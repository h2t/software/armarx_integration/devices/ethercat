/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Armar6RT::ArmarXObjects::Armar6IceKinematicUnit
 * @author     Markus Swarowsky ( markus dot swarowsky at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>

#include <devices/ethercat/hand/common/AbstractHandUnitControllerWrapper.h>


namespace devices::ethercat::hand::common
{

    TYPEDEF_PTRS_SHARED(AbstractHand);
    class AbstractHand
    {

    public:
        virtual ~AbstractHand() = default;

        virtual std::string getHandDeviceName() const = 0;
        virtual AbstractHandUnitControllerWrapperPtr
        createHandUnitControllerWrapper(armarx::RobotUnit* robotUnit,
                                        const std::string& handName) const = 0;
    };

} // namespace devices::ethercat::hand::common
