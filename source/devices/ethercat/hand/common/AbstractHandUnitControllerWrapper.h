#pragma once


#include <RobotAPI/components/units/RobotUnit/util.h>
#include <RobotAPI/interface/objectpose/object_pose_types.h>


namespace devices::ethercat::hand::common
{

    TYPEDEF_PTRS_SHARED(AbstractHandUnitControllerWrapper);
    class AbstractHandUnitControllerWrapper
    {

    public:
        virtual ~AbstractHandUnitControllerWrapper() = default;

        virtual void setShape(const std::string& shape) = 0;
        virtual std::string describeHandState() const = 0;
        virtual void setJointAngles(const armarx::NameValueMap& targetJointAngles) = 0;
        virtual std::map<std::string, float> getActualJointValues() = 0;
    };

} // namespace devices::ethercat::hand::common
