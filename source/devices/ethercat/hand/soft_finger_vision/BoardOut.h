﻿#pragma once

#include <armarx/control/ethercat/AbstractSlave.h>
#include <armarx/control/ethercat/DeviceFactory.h>

namespace devices::ethercat::hand::finger_vision_soft
{
    struct BoardOut_motor_value
    {
        std::int32_t pos;
        std::int32_t vel;
    } __attribute__((__packed__));

    struct BoardOut_diagnostics
    {
        std::uint32_t sensor_frame_counter;
        std::uint16_t main_update_rate;
        std::uint16_t fpga_update_rate;
    } __attribute__((__packed__));

    struct BoardOut_cnn_data
    {
        std::uint32_t finger_number;
        std::array<std::uint32_t, 199> data;
    } __attribute__((__packed__));

    /// @brief PDO mapping sensorB->master
    struct BoardOut
    {
        BoardOut_motor_value motor_thumb;
        BoardOut_motor_value motor_index;
        BoardOut_motor_value motor_other;
        std::uint32_t        temperature;
        BoardOut_diagnostics diagnostics;
        BoardOut_cnn_data    cnn_data;
    } __attribute__((__packed__));
    static_assert(sizeof(BoardOut) == 836);
}
