/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Armar6RT::ArmarXObjects::KITSensorizedSoftFingerHandV1NJointController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>

#include <devices/ethercat/hand/finger_vision_soft/sensor_value/Motor.h>
#include <devices/ethercat/hand/finger_vision_soft/sensor_value/Hand.h>

#include "Identities.h"

namespace devices::ethercat::hand::finger_vision_soft
{
    struct Sensors
    {
        using MotorIdentity  = identity::MotorIdentity;
        using FingerIdentity = identity::FingerIdentity;

        identity::MotorArray<const MotorSensorValue*> motor;

        const HandSensorValue* hand = nullptr;

        Sensors() = default;
        Sensors(Sensors&&) = default;
        Sensors(const Sensors&) = default;
        Sensors& operator=(Sensors&&) = default;
        Sensors& operator=(const Sensors&) = default;

        Sensors(const std::string& hand_name, NJointController& parent)
        {
            init(parent, hand_name, hand);
            init(parent, hand_name + ".motor.thumb",   motor.thumb());
            init(parent, hand_name + ".motor.index",   motor.index());
            init(parent, hand_name + ".motor.other",   motor.other());
        }

    private:
        template<class T>
        void init(NJointController& parent, const std::string& name, T const*& ptr)
        {
            const SensorValueBase* sv = parent.useSensorValue(name);
            ARMARX_CHECK_NOT_NULL(sv) << VAROUT(name);
            ptr = sv->asA<T>();
            ARMARX_CHECK_NOT_NULL(ptr) << VAROUT(name);
        }
    };

}
