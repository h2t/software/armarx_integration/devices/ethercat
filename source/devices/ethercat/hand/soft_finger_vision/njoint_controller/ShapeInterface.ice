#pragma once

#include <RobotAPI/interface/units/RobotUnit/NJointController.ice>

module armarx
{
    struct KITFingerVisionSoftHandV1JointValues
    {
        float thumbJointValue;
        float indexJointValue;
        float otherJointValue;
    };

    interface NJointKITFingerVisionSoftHandV1ShapeControllerInterface extends NJointControllerInterface
    {
        void setTargets(float lrm, float index, float thumb);
        void setTargetsWithPwm(float lrm, float index, float thumb, float lrmRelativePWM, float indexRelativePWM, float thumbRelativePWM);
        ["cpp:const"] KITFingerVisionSoftHandV1JointValues getJointValues();
    };
};

