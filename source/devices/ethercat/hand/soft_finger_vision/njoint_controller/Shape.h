#pragma once

#include <atomic>

#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>

#include "robot_devices/ethercat/hand/finger_vision_soft/sensor_value/Motor.h"
#include <devices/ethercat/hand/finger_vision_soft/njoint_controller/Shape.h>

#include "../joint_controller/Pwm.h"

namespace devices::ethercat::hand::finger_vision_soft
{
    class NJointShapeControllerConfig;
    using NJointShapeControllerConfigPtr = IceUtil::Handle<NJointShapeControllerConfig>;

    class NJointShapeControllerConfig : virtual public armarx::NJointControllerConfig
    {
    public:
        NJointShapeControllerConfig(std::string const& deviceName):
            deviceName(deviceName)
        {}

        const std::string deviceName;
    };

    class NJointShapeController;
    using NJointShapeControllerPtr = IceInternal::Handle<NJointShapeController>;

    class NJointShapeController :
        public armarx::NJointController,
        public NJointKITFingerVisionSoftHandV1ShapeControllerInterface

    {
    public:
        using ConfigPtrT = NJointShapeControllerConfigPtr;
        NJointShapeController(
            armarx::RobotUnitPtr prov,
            NJointShapeControllerConfigPtr config,
            const VirtualRobot::RobotPtr& r);

    public:
        std::string getClassName(const Ice::Current&) const override
        {
            return "NJointKITFingerVisionSoftHandV1ShapeController";
        }

        void stopMotion();

        void readConfig(
            const armarx::RapidXmlReaderNode& configNode,
            armarx::DefaultRapidXmlReaderNode defaultConfigurationNod);

        int16_t getOtherPwm();
        int16_t getIndexPwm();
        int16_t getThumbPwm();

        float getOtherTarget() const;
        float getIndexTarget() const;
        float getThumbTarget() const;

        float getOtherJointValue() const;
        float getIndexJointValue() const;
        float getThumbJointValue() const;

        bool isControlEnabled() const;

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

    public:
        armarx::WidgetDescription::StringWidgetDictionary getFunctionDescriptions(const Ice::Current&) const override;
        void callDescribedFunction(const std::string& name, const armarx::StringVariantBaseMap& valueMap, const Ice::Current&) override;

        void setTargets(float lrm, float index, float thumb,
                        const Ice::Current& = Ice::emptyCurrent) override;
        void setTargetsWithPwm(float lrm, float index, float thumb,
                               float lrmRelativePWM, float indexRelativePWM, float thumbRelativePWM,
                               const Ice::Current& = Ice::emptyCurrent) override;
        KITFingerVisionSoftHandV1JointValues getJointValues(const Ice::Current&) const override;
    private:
        struct FingerData
        {
            const MotorSensorValue*       motorSensor;
            armarx::ControlTarget1DoFActuatorPWM* pwmTarget;
            std::atomic<float>            jointValue;
            std::atomic<int16_t>          lastPwm;
            int16_t                       pwmLimit = 0;
            float                         Kp        = 0;
            float                         Ki        = 0;
            float                         Kd        = 0;
        };
        FingerData otherData;
        FingerData indexData;
        FingerData thumbData;

        struct FingersTargetData
        {
            struct TargetData
            {
                int16_t maxPwm = 0;
                float   target  = 0;
            };
            TargetData index;
            TargetData thumb;
            TargetData other;
            bool enableControl = false;
        };
        mutable std::recursive_mutex                   targetBufWriteMutex;
        armarx::WriteBufferedTripleBuffer<FingersTargetData> targetBuf;
        mutable std::recursive_mutex                   lastTargetBufReadMutex;
        armarx::TripleBuffer<FingersTargetData>              lastTargetBuf;

    };
}
