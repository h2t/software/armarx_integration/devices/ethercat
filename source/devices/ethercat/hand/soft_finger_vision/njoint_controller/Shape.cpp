#include "Shape.h"

#include <boost/algorithm/clamp.hpp>

#include <RobotAPI/libraries/core/math/MathUtils.h>

#include "../joint_controller/Pwm.h"

//init
namespace devices::ethercat::hand::finger_vision_soft
{
    armarx::NJointControllerRegistration<NJointShapeController>
    registrationControllerNJointShapeController(
        "NJointKITFingerVisionSoftHandV1ShapeController");

    NJointShapeController::NJointShapeController(armarx::RobotUnitPtr prov,
            NJointShapeControllerConfigPtr config,
            const VirtualRobot::RobotPtr&)
    {
        ARMARX_TRACE;
        ARMARX_CHECK_EXPRESSION(prov);
        const auto init = [&](auto & data, const std::string & suffix)
        {
            const auto name = config->deviceName + ".motor." + suffix;
            ControlTargetBase* ct = useControlTarget(name, ControlModes::PWM1DoF);
            ARMARX_CHECK_NOT_NULL(ct) << " control target " << name
                                      << " control mode " << ControlModes::PWM1DoF
                                      << ARMARX_STREAM_PRINTER
            {
                const auto ptr = peekControlDevice(name);
                if (ptr)
                {
                    out << "\nthese are the available controllers: "
                        << ptr->getJointControllerToTargetTypeNameMap();
                }
                else
                {
                    out << "\nthere is no control device with this name";
                }
            };

            ConstSensorDevicePtr sensor = peekSensorDevice(name);
            ARMARX_CHECK_NOT_NULL(sensor) << "device name: " << name;

            data.motor_sensor = sensor->getSensorValue()->asA<MotorSensorValue>();
            ARMARX_CHECK_NOT_NULL(data.motor_sensor) << "device name: " << name;
            data.pwm_target  = ct->asA<armarx::ControlTarget1DoFActuatorPWM>();
            ARMARX_CHECK_NOT_NULL(data.pwm_target) << "device name: " << name;
        };
        init(otherData, "other");
        init(indexData, "index");
        init(thumbData, "thumb");
    }
    void NJointShapeController::readConfig(
        const armarx::RapidXmlReaderNode& configNode,
        armarx::DefaultRapidXmlReaderNode defaultConfigurationNode)
    {
        ARMARX_TRACE;
        ARMARX_DEBUG << "readConfig got cfg:\n" << VAROUT(configNode.getPath())
                     << "\n" << VAROUT(defaultConfigurationNode.getPathsString());

        auto configNodeAll =
            defaultConfigurationNode
            .add_node_at_end(configNode)
            .first_node("Motor");

        const auto init = [&](auto & data, auto name)
        {
            auto cfg = configNodeAll
                       .first_node("default")
                       .add_node_at_end(configNodeAll.first_node(name))
                       .first_node("ctrl_shape");
            cfg.attribute_as("Kp", data.Kp);
            cfg.attribute_as("Ki", data.Ki);
            cfg.attribute_as("Kd", data.Kd);
            cfg.attribute_as("max_pwm", data.pwm_limit);
        };
        init(thumbData, "thumb");
        init(indexData, "index");
        init(otherData, "other");
    }
}

//getter
namespace devices::ethercat::hand::finger_vision_soft
{
    int16_t NJointShapeController::getOtherPwm()
    {
        return otherData.last_pwm;
    }
    int16_t NJointShapeController::getIndexPwm()
    {
        return indexData.last_pwm;
    }
    int16_t NJointShapeController::getThumbPwm()
    {
        return thumbData.last_pwm;
    }

    float NJointShapeController::getOtherTarget() const
    {
        std::lock_guard g{lastTargetBufReadMutex};
        return lastTargetBuf.getUpToDateReadBuffer().other.target;
    }
    float NJointShapeController::getIndexTarget() const
    {
        std::lock_guard g{lastTargetBufReadMutex};
        return lastTargetBuf.getUpToDateReadBuffer().index.target;
    }
    float NJointShapeController::getThumbTarget() const
    {
        std::lock_guard g{lastTargetBufReadMutex};
        return lastTargetBuf.getUpToDateReadBuffer().thumb.target;
    }

    float NJointShapeController::getOtherJointValue() const
    {
        return otherData.joint_value;
    }
    float NJointShapeController::getIndexJointValue() const
    {
        return indexData.joint_value;
    }
    float NJointShapeController::getThumbJointValue() const
    {
        return thumbData.joint_value;
    }

    bool NJointShapeController::isControlEnabled() const
    {
        std::lock_guard g{lastTargetBufReadMutex};
        return lastTargetBuf.getUpToDateReadBuffer().enable_control;
    }

    KITFingerVisionSoftHandV1JointValues NJointShapeController::getJointValues(const Ice::Current&) const
    {
        KITFingerVisionSoftHandV1JointValues values;
        values.otherJointValue = getOtherJointValue();
        values.indexJointValue = getIndexJointValue();
        values.thumbJointValue = getThumbJointValue();
        return values;
    }
}

//setter
namespace devices::ethercat::hand::finger_vision_soft
{
    void NJointShapeController::stopMotion()
    {
        std::lock_guard g{targetBufWriteMutex};
        targetBuf.getWriteBuffer().enable_control = false;
        targetBuf.commitWrite();
    }
    void NJointShapeController::setTargets(
        float lrm, float index, float thumb, const Ice::Current&)
    {
        setTargetsWithPwm(lrm, index, thumb, 1, 1, 1);
    }

    void NJointShapeController::setTargetsWithPwm(
        float lrm, float index, float thumb,
        float lrmRelativePWM, float indexRelativePWM, float thumbRelativePWM,
        const Ice::Current&)
    {
        std::lock_guard g{targetBufWriteMutex};
        auto& buf = targetBuf.getWriteBuffer();

        buf.enable_control = true;
        buf.index.target  = armarx::math::MathUtils::LimitMinMax(0.f, 2.f, thumb);
        buf.thumb.target  = armarx::math::MathUtils::LimitMinMax(0.f, 2.f, index);
        buf.other.target  = armarx::math::MathUtils::LimitMinMax(0.f, 2.f, lrm);
        buf.index.max_pwm = static_cast<std::int16_t>(indexData.pwm_limit * boost::algorithm::clamp(indexRelativePWM, 0, 1));
        buf.thumb.max_pwm = static_cast<std::int16_t>(thumbData.pwm_limit * boost::algorithm::clamp(thumbRelativePWM, 0, 1));
        buf.other.max_pwm = static_cast<std::int16_t>(otherData.pwm_limit * boost::algorithm::clamp(lrmRelativePWM, 0, 1));
        targetBuf.commitWrite();
    }
}

//control
namespace devices::ethercat::hand::finger_vision_soft
{
    void NJointShapeController::rtRun(
        const IceUtil::Time& sensorValuesTimestamp,
        const IceUtil::Time& timeSinceLastIteration)
    {
        otherData.joint_value = otherData.motor_sensor->position;
        indexData.joint_value = indexData.motor_sensor->position;
        thumbData.joint_value = thumbData.motor_sensor->position;

        const auto& buf = targetBuf.getUpToDateReadBuffer();
        lastTargetBuf.getWriteBuffer() = buf;
        lastTargetBuf.commitWrite();


        if (buf.enable_control)
        {
            auto calc = [](auto & buff, auto & data)
            {
                const float err = buff.target - data.motor_sensor->position;
                data.pwm_target->pwm = boost::algorithm::clamp(
                                           static_cast<std::int16_t>(data.Kp * buff.max_pwm * err),
                                           -buff.max_pwm, buff.max_pwm);
            };
            calc(buf.other, otherData);
            calc(buf.index, indexData);
            calc(buf.thumb, thumbData);
        }
        else
        {
            otherData.pwm_target->pwm = 0;
            indexData.pwm_target->pwm = 0;
            thumbData.pwm_target->pwm = 0;
        }
        otherData.last_pwm = otherData.pwm_target->pwm;
        indexData.last_pwm = indexData.pwm_target->pwm;
        thumbData.last_pwm = thumbData.pwm_target->pwm;
    }
}

//gui
namespace devices::ethercat::hand::finger_vision_soft
{
    armarx::WidgetDescription::StringWidgetDictionary NJointShapeController::getFunctionDescriptions(const Ice::Current&) const
    {
        using namespace armarx::WidgetDescription;
        VBoxLayoutPtr vbox = new VBoxLayout;
        const auto add = [&](const std::string & name, const auto value)
        {
            HBoxLayoutPtr hbox = new HBoxLayout;
            vbox->children.emplace_back(hbox);
            //val
            {
                hbox->children.emplace_back(new Label(false, name));
                FloatSliderPtr slider = new FloatSlider;
                slider->name = name + "_val";
                slider->min = 0;
                slider->defaultValue = value;
                slider->max = 1;
                hbox->children.emplace_back(slider);
            }
            //pwm
            {
                hbox->children.emplace_back(new Label(false, "max pwm"));
                FloatSliderPtr slider = new FloatSlider;
                slider->name = name + "_pwm";
                slider->min = 0;
                slider->defaultValue = 1;
                slider->max = 1;
                hbox->children.emplace_back(slider);
            }
        };
        add("other", getOtherJointValue());
        add("index", getIndexJointValue());
        add("thumb", getThumbJointValue());
        return {{"Targets", vbox}};
    }

    void NJointShapeController::callDescribedFunction(const std::string& name, const StringVariantBaseMap& valueMap, const Ice::Current&)
    {
        if (name == "Targets")
        {
            setTargetsWithPwm(
                valueMap.at("other_val")->getFloat(),
                valueMap.at("index_val")->getFloat(),
                valueMap.at("thumb_val")->getFloat(),

                valueMap.at("other_pwm")->getFloat(),
                valueMap.at("index_pwm")->getFloat(),
                valueMap.at("thumb_pwm")->getFloat());
        }
        else
        {
            ARMARX_WARNING << "Unknown function name called: " << name;
        }
    }
}
