#pragma once

#include <RobotAPI/interface/units/RobotUnit/NJointController.ice>

module armarx
{
    module KITFingerVisionSoftHand
    {
        module V1
        {
            module NJointGraspOnDetectionControllerConfigElements
            {
                struct PID
                {
                    float p = 1;
                    float i = 0;
                    float d = 0;
                };
                struct MotorCfg
                {
                    PID   pos_pid;
                    float pos_close = 0.5;
                    float pos_open  = 0.1;
                    short pwm_max   = 1000;
                    short pwm_open  = -300;
                };
            };

            class NJointGraspOnDetectionControllerConfig extends NJointControllerConfig
            {
                string          hand_name;

                NJointGraspOnDetectionControllerConfigElements::MotorCfg motor_other;
                NJointGraspOnDetectionControllerConfigElements::MotorCfg motor_index;
                NJointGraspOnDetectionControllerConfigElements::MotorCfg motor_thumb;

                bool  close_full_force               = false;

                bool  detect_on_little               = false;
                bool  detect_on_ring                 = false;
                bool  detect_on_middle               = false;
                bool  detect_on_index                = true;
                bool  detect_on_thumb                = true;

                long  detection_threshold_per_finger = 50;
                long  detection_threshold_total      = 200;
                short finger_count_threahold         = 1;

                long object_id = 0;
            };
            interface NJointGraspOnDetectionControllerInterface extends NJointControllerInterface
            {
                void setAutofire(bool fire);
                bool detected();
                void start();
                void reset();
                void configure(NJointGraspOnDetectionControllerConfig cfg);
            };
        };
    };
};

