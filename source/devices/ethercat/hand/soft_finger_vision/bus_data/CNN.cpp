#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include "CNN.h"

namespace devices::ethercat::hand::finger_vision_soft
{
    CNNControlData::CNNControlData(BoardIn_cnn_target* target) :
        target{target}
    {
        ARMARX_TRACE;
        ARMARX_CHECK_NOT_NULL(target);
    }

    void CNNControlData::rtReadSensorValues(
        const IceUtil::Time& /*sensorValuesTimestamp*/,
        const IceUtil::Time& /*timeSinceLastIteration*/)
    {}

    void CNNControlData::rtWriteTargetValues(
        const IceUtil::Time& /*sensorValuesTimestamp*/,
        const IceUtil::Time& /*timeSinceLastIteration*/)
    {}

    void CNNControlData::setId(int16 id)
    {
        target->object_id = id;
    }
}
