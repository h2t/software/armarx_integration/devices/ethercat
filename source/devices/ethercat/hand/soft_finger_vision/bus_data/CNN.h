#pragma once

#include <memory>

#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>

#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <RobotAPI/components/units/RobotUnit/Devices/ControlDevice.h>
#include <RobotAPI/components/units/RobotUnit/Devices/SensorDevice.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <RobotAPI/interface/units/KinematicUnitInterfaceStdOverloads.h>
#include <RobotAPI/libraries/core/Pose.h>

#include <armarx/control/ethercat/AbstractData.h>

#include <armarx/control/rt_filters/MedianFilteredLinearConvertedValue.h>

#include <devices/ethercat/common/Elmo/ElmoData.h>

#include "../Slave.h"
#include "../SensorValue/Motor.h"

namespace devices::ethercat::hand::finger_vision_soft
{
    class CNNControlData : public AbstractData
    {

    public:
        CNNControlData(BoardIn_cnn_target* target);

        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration) override;
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                 const IceUtil::Time& timeSinceLastIteration) override;

        void setId(int16 id);
    private:
        BoardIn_cnn_target* target;
    };
    using CNNControlDataPtr =
        std::shared_ptr<CNNControlData>;
}
