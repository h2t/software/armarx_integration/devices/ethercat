#include <boost/algorithm/clamp.hpp>

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include "Hand.h"

namespace devices::ethercat::hand::finger_vision_soft
{
    HandSensorData::HandSensorData(
        const RapidXmlReaderNode& node,
        DefaultRapidXmlReaderNode defaultConfigurationNode,
        BoardOut* sensorAll,
        const std::string& configName
    ):
        sensor{sensorAll}
    {
        ARMARX_TRACE;
        ARMARX_CHECK_NOT_NULL(sensorAll);
        auto cfg =
            defaultConfigurationNode
            .add_node_at_end(node)
            .first_node("Hand");
        ARMARX_DEBUG << "configName " << configName << "\nall config paths:\n" << cfg.getChildPaths();

        //hand_shutdown_threshold
        {
            const auto node = cfg.first_node("hand_shutdown_threshold");
            node.attribute_as("shutdown_temp", motor_temp_threshold_shutdown);
            node.attribute_as("restart_temp",  motor_temp_threshold_restart);
        }
        //sensor_hand_temp
        {
            const auto node = cfg.first_node("sensor_hand_temp");
            node.attribute_as("a", temp_correction_a);
            node.attribute_as("b", temp_correction_b);
            node.attribute_as("c", temp_correction_c);

            temperature = RtMedianFilter{node.attribute_as<std::size_t>("median_filter_width")};
        }
    }

    void HandSensorData::rtReadSensorValues(
        const IceUtil::Time& /*sensorValuesTimestamp*/,
        const IceUtil::Time& /*timeSinceLastIteration*/)
    {}

    void HandSensorData::rtWriteTargetValues(
        const IceUtil::Time& /*sensorValuesTimestamp*/,
        const IceUtil::Time& /*timeSinceLastIteration*/)
    {}

    void HandSensorData::updateSensorValueStruct(
        HandSensorValue& data)
    {
        data.frame_counter    = sensor->diagnostics.sensor_frame_counter;
        data.update_rate_main = sensor->diagnostics.main_update_rate;
        data.update_rate_fpga = sensor->diagnostics.fpga_update_rate;
        data.raw_temperature  = sensor->temperature;
        // a*exp(b*x)+c
        data.temperature = temperature.update(
                               temp_correction_a *
                               std::exp(data.raw_temperature * temp_correction_b) +
                               temp_correction_c);
        data.cnn_data = sensor->cnn_data.data;
        //update popcount
        std::size_t pops = 0;
        {
            // due to the old compiler we do not have popcount -> use
            // https://stackoverflow.com/a/109025
            //
            // benchmark:
            // Run on (8 X 3800 MHz CPU s)
            // CPU Caches:
            //   L1 Data 32K (x4)
            //   L1 Instruction 32K (x4)
            //   L2 Unified 256K (x4)
            //   L3 Unified 6144K (x1)
            // Load Average: 1.57, 1.78, 2.39
            // ***WARNING*** CPU scaling is enabled, the benchmark real time
            //       measurements may be noisy and will incur extra overhead.
            // ***WARNING*** Library was built as DEBUG. Timings may be affected.
            // ---------------------------------------------------------------------
            // Benchmark                           Time             CPU   Iterations
            // ---------------------------------------------------------------------
            // popcount<199>/manual_time         880 ns         4542 ns       808996
            for (auto i : data.cnn_data)
            {
                i = i - ((i >> 1) & 0x55555555);
                i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
                pops += (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
            }
        }
        switch (sensor->cnn_data.finger_number % 5)
        {
            case 0:
                data.cnn_data_popcount_little_frame_counter = data.frame_counter;
                data.cnn_data_popcount_little = pops;
                ++data.cnn_data_popcount_little_number_of_images;
                data.cnn_data_finger_number = 0;
                break;
            case 3:
                data.cnn_data_popcount_ring_frame_counter = data.frame_counter;
                data.cnn_data_popcount_ring = pops;
                ++data.cnn_data_popcount_ring_number_of_images;
                data.cnn_data_finger_number = 1;
                break;
            case 4:
                data.cnn_data_popcount_middle_frame_counter = data.frame_counter;
                data.cnn_data_popcount_middle = pops;
                ++data.cnn_data_popcount_middle_number_of_images;
                data.cnn_data_finger_number = 2;
                break;
            case 2:
                data.cnn_data_popcount_index_frame_counter = data.frame_counter;
                data.cnn_data_popcount_index = pops;
                ++data.cnn_data_popcount_index_number_of_images;
                data.cnn_data_finger_number = 3;
                break;
            case 1:
                data.cnn_data_popcount_thumb_frame_counter = data.frame_counter;
                data.cnn_data_popcount_thumb = pops;
                ++data.cnn_data_popcount_thumb_number_of_images;
                data.cnn_data_finger_number = 4;
                break;
            default:
                data.cnn_data_finger_number = ~0;
                ++data.cnn_data_popcount_error_number_of_images;
                ARMARX_WARNING_S << "KITFingerVisionSoftHand::V1 invalid finger id "
                                 << sensor->cnn_data.finger_number;
        }
    }
}
