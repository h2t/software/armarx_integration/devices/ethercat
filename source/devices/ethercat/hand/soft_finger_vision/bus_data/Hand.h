#pragma once

#include <memory>

#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>

#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <RobotAPI/components/units/RobotUnit/Devices/ControlDevice.h>
#include <RobotAPI/components/units/RobotUnit/Devices/SensorDevice.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <RobotAPI/interface/units/KinematicUnitInterfaceStdOverloads.h>
#include <RobotAPI/libraries/core/Pose.h>
#include <armarx/control/ethercat/AbstractData.h>

#include <armarx/control/rt_filters/AverageFilter.h>
#include <armarx/control/rt_filters/FirFilter.h>
#include <armarx/control/rt_filters/RtMedianFilter.h>




#include "../Slave.h"
#include "../SensorValue/Hand.h"

namespace devices::ethercat::hand::finger_vision_soft
{
    class HandSensorData;
    using HandSensorDataPtr =
        std::shared_ptr<HandSensorData>;

    class HandSensorData : public AbstractData
    {
    public:
        HandSensorData(
            const RapidXmlReaderNode& node,
            DefaultRapidXmlReaderNode defaultConfigurationNode,
            BoardOut* sensorAll,
            const std::string& configName);

        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration) override;
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                 const IceUtil::Time& timeSinceLastIteration) override;

        void updateSensorValueStruct(HandSensorValue& data);

        float rtGetMotorTemperatureShutdownThreshold() const
        {
            return motor_temp_threshold_shutdown;
        }

        float rtGetMotorTemperatureRestartThreshold() const
        {
            return motor_temp_threshold_restart;
        }

    private:
        const BoardOut* sensor;
        float           motor_temp_threshold_shutdown;
        float           motor_temp_threshold_restart;
        // a*exp(b*x)+c
        float           temp_correction_a;
        float           temp_correction_b;
        float           temp_correction_c;
        RtMedianFilter  temperature;
    };
}
