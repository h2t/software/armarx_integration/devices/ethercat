#pragma once

#include <VirtualRobot/Nodes/RobotNode.h>

#include <armarx/control/ethercat/AbstractDevice.h>
#include <armarx/control/ethercat/SlaveIdentifier.h>

#include "bus_data/Hand.h"
#include "bus_data/Motor.h"
#include "bus_data/CNN.h"
#include "Slave.h"

#include <devices/ethercat/hand/common/AbstractHand.h>

namespace devices::ethercat::hand::finger_vision_soft::detail
{

    using MotorControlDevicePtr =
        std::shared_ptr<class MotorControlDevice>;

    using HandSensorDevicePtr =
        std::shared_ptr<class HandSensorDevice>;

    using CNNControlDevicePtr =
        std::shared_ptr<class CNNControlDevice>;
}

namespace devices::ethercat::hand::finger_vision_soft
{
    using FunctionalDevicePtr =
        std::shared_ptr<class FunctionalDevice>;

    class FunctionalDevice :
        public AbstractDevice,
        public AbstractHand
    {
    public:
        FunctionalDevice(RapidXmlReaderNode jointNode,
                         armarx::DefaultRapidXmlReaderNode defaultConfigurationNode,
                         const VirtualRobot::RobotPtr& robot);
        ~FunctionalDevice() = default;
        void init(SlavePtr handSlave);
        void initData();

        SlaveIdentifier getSlaveIdentifier() const;
        std::string getDeviceName() const;
    public:
        AbstractHandUnitControllerWrapperPtr createHandUnitControllerWrapper(
            RobotUnit* robotUnit, const std::string& handName,
            RapidXmlReaderNode configNode) const override;
        std::string getHandConfigNodeName() const override;
        std::string getHandDeviceName() const override;

        void addDevicesTo(const KITFingerVisionSoftHandV1FactoryPtr& f);

    private:
        const std::string deviceName;
        RapidXmlReaderNode configNode;
        VirtualRobot::RobotPtr robot;

        ///data objects for sensor and control
        MotorControlDataPtr otherControlDataPtr;
        MotorControlDataPtr indexControlDataPtr;
        MotorControlDataPtr thumbControlDataPtr;

        CNNControlDataPtr   cnnControlDataPtr;

        HandSensorDataPtr   handSensorDataPtr;

        ///device objects for sensor and control
        detail::MotorControlDevicePtr otherControlDevicePtr;
        detail::MotorControlDevicePtr indexControlDevicePtr;
        detail::MotorControlDevicePtr thumbControlDevicePtr;

        detail::HandSensorDevicePtr   handSensorDevicePtr;

        detail::CNNControlDevicePtr   cnnControlDevicePtr;
        ///bus devices - serial form config and pointer to actual bus slave
        const SlaveIdentifier slaveIdentifier;

        SlavePtr handSlave;

        IceUtil::Time lastReadUpdate, lastWriteUpdate;
    };

}
