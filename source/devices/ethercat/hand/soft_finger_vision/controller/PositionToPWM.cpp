#include <boost/algorithm/clamp.hpp>

#include "PositionToPWM.h"

namespace devices::ethercat::hand::finger_vision_soft::Controllers
{
    PositionToPWM::PositionToPWM(float p, float i, float d) :
        PID{p, i, d}
    {
        PID.threadSafe = false;
    }

    void PositionToPWM::reset()
    {
        PID.reset();
    }

    int16_t PositionToPWM::calculate(float curr_pos, float targ_pos, float dt, int16_t maxPWM)
    {
        PID.update(dt, curr_pos, targ_pos);
        return boost::algorithm::clamp(
                   static_cast<std::int16_t>(PID.getControlValue() * maxPWM),
                   -maxPWM, maxPWM);
    }
}
