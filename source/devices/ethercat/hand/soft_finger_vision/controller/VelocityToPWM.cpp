#include <boost/algorithm/clamp.hpp>

#include "VelocityToPWM.h"

namespace devices::ethercat::hand::finger_vision_soft::Controllers
{
    VelocityToPWM::VelocityToPWM(float p, float i, float d) :
        pos_ctrl{p, i, d}
    {}

    void VelocityToPWM::reset(float pos)
    {
        pos_ctrl.reset();
        virtual_pos_target =  pos;
    }

    int16_t VelocityToPWM::calculate(float curr_pos, float velocity, float dt, int16_t maxPWM)
    {
        virtual_pos_target = boost::algorithm::clamp(
                                 virtual_pos_target + velocity * dt,
                                 0, 1);
        return pos_ctrl.calculate(curr_pos, virtual_pos_target, dt, maxPWM);
    }
}
