#include "Motor.h"

armarx::SensorValueBase::SensorValueInfo<MotorSensorValue>
devices::ethercat::hand::finger_vision_soft::MotorSensorValue::GetClassMemberInfo()
{
    using T = MotorSensorValue;
    SensorValueInfo<T> svi;
    svi.addMemberVariable(&T::raw_position, "raw_position");
    svi.addMemberVariable(&T::raw_velocity, "raw_velocity");
    svi.addMemberVariable(&T::pwm_max, "motor_pwm_max");
    svi.addBaseClass<SensorValue1DoFActuatorPosition>();
    svi.addBaseClass<SensorValue1DoFActuatorVelocity>();
    svi.addBaseClass<SensorValue1DoFMotorPWM>();
    return svi;
}
