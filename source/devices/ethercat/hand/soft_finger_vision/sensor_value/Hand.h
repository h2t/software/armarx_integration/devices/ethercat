#pragma once

#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>

namespace devices::ethercat::hand::finger_vision_soft
{
    class HandSensorValue : virtual public armarx::SensorValueBase
    {
    public:
        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION

        float         temperature;
        std::uint32_t raw_temperature;

        std::uint32_t frame_counter;
        std::uint16_t update_rate_main;
        std::uint16_t update_rate_fpga;

        std::uint32_t cnn_data_finger_number;
        std::array<std::uint32_t, 199> cnn_data;

        std::uint32_t cnn_data_popcount_little;
        std::uint32_t cnn_data_popcount_ring;
        std::uint32_t cnn_data_popcount_middle;
        std::uint32_t cnn_data_popcount_index;
        std::uint32_t cnn_data_popcount_thumb;

        std::uint32_t cnn_data_popcount_little_frame_counter;
        std::uint32_t cnn_data_popcount_ring_frame_counter;
        std::uint32_t cnn_data_popcount_middle_frame_counter;
        std::uint32_t cnn_data_popcount_index_frame_counter;
        std::uint32_t cnn_data_popcount_thumb_frame_counter;

        std::uint32_t cnn_data_popcount_little_number_of_images = 0;
        std::uint32_t cnn_data_popcount_ring_number_of_images   = 0;
        std::uint32_t cnn_data_popcount_middle_number_of_images = 0;
        std::uint32_t cnn_data_popcount_index_number_of_images  = 0;
        std::uint32_t cnn_data_popcount_thumb_number_of_images  = 0;
        std::uint32_t cnn_data_popcount_error_number_of_images  = 0;

        static SensorValueInfo<HandSensorValue> GetClassMemberInfo();
    };

}  // namespace devices::ethercat::hand::finger_vision_soft
