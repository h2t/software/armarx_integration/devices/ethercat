
#include "Hand.h"

namespace devices::ethercat::hand::finger_vision_soft
{

    armarx::SensorValueBase::SensorValueInfo<HandSensorValue>
    HandSensorValue::GetClassMemberInfo()
    {
        using T = HandSensorValue;
        SensorValueInfo<T> svi;
        svi.addMemberVariable(&T::temperature, "temperature");
        svi.addMemberVariable(&T::raw_temperature, "raw_temperature");

        svi.addMemberVariable(&T::frame_counter, "frame_counter");
        svi.addMemberVariable(&T::update_rate_main, "update_rate_main");
        svi.addMemberVariable(&T::update_rate_fpga, "update_rate_fpga");

        svi.addMemberVariable(&T::cnn_data_finger_number, "cnn_data_finger_number");
        svi.addMemberVariable(&T::cnn_data, "cnn_data");

        svi.addMemberVariable(&T::cnn_data_popcount_little, "cnn_data_popcount_little");
        svi.addMemberVariable(&T::cnn_data_popcount_ring, "cnn_data_popcount_ring");
        svi.addMemberVariable(&T::cnn_data_popcount_middle, "cnn_data_popcount_middle");
        svi.addMemberVariable(&T::cnn_data_popcount_index, "cnn_data_popcount_index");
        svi.addMemberVariable(&T::cnn_data_popcount_thumb, "cnn_data_popcount_thumb");

        svi.addMemberVariable(&T::cnn_data_popcount_little_frame_counter,
                              "cnn_data_popcount_little_frame_counter");
        svi.addMemberVariable(&T::cnn_data_popcount_ring_frame_counter,
                              "cnn_data_popcount_ring_frame_counter");
        svi.addMemberVariable(&T::cnn_data_popcount_middle_frame_counter,
                              "cnn_data_popcount_middle_frame_counter");
        svi.addMemberVariable(&T::cnn_data_popcount_index_frame_counter,
                              "cnn_data_popcount_index_frame_counter");
        svi.addMemberVariable(&T::cnn_data_popcount_thumb_frame_counter,
                              "cnn_data_popcount_thumb_frame_counter");

        svi.addMemberVariable(&T::cnn_data_popcount_little_number_of_images,
                              "cnn_data_popcount_little_number_of_images");
        svi.addMemberVariable(&T::cnn_data_popcount_ring_number_of_images,
                              "cnn_data_popcount_ring_number_of_images");
        svi.addMemberVariable(&T::cnn_data_popcount_middle_number_of_images,
                              "cnn_data_popcount_middle_number_of_images");
        svi.addMemberVariable(&T::cnn_data_popcount_index_number_of_images,
                              "cnn_data_popcount_index_number_of_images");
        svi.addMemberVariable(&T::cnn_data_popcount_thumb_number_of_images,
                              "cnn_data_popcount_thumb_number_of_images");
        svi.addMemberVariable(&T::cnn_data_popcount_error_number_of_images,
                              "cnn_data_popcount_error_number_of_images");
        return svi;
    }
} // namespace devices::ethercat::hand::finger_vision_soft
