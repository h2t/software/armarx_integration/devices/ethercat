#pragma once

#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>

namespace devices::ethercat::hand::finger_vision_soft
{
    class MotorSensorValue :
        virtual public armarx::SensorValue1DoFActuatorPosition,
        virtual public armarx::SensorValue1DoFActuatorVelocity,
        virtual public armarx::SensorValue1DoFMotorPWM
    {
    public:
        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION

        std::int32_t  raw_position;
        std::int32_t  raw_velocity;
        std::uint32_t pwm_max;

        static SensorValueInfo<MotorSensorValue> GetClassMemberInfo();
    };

}  // namespace devices::ethercat::hand::finger_vision_soft
