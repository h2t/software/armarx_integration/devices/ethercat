#include "Pwm.h"

namespace devices::ethercat::hand::finger_vision_soft
{
    JointPwmController::JointPwmController(
        MotorControlDataPtr data,
        FunctionalDevicePtr hand) :
        data(data),
        hand(hand)
    {
        ARMARX_CHECK_NOT_NULL(data);
        ARMARX_CHECK_NOT_NULL(hand);
    }

    ControlTargetBase* JointPwmController::getControlTarget()
    {
        return &target;
    }

    void JointPwmController::rtRun(
        const IceUtil::Time&, const IceUtil::Time&)
    {
        data->setCommand(target.pwm);
    }

    void JointPwmController::rtPreActivateController()
    {}

    void JointPwmController::rtPostDeactivateController()
    {
        data->setCommand(0);
    }
}
