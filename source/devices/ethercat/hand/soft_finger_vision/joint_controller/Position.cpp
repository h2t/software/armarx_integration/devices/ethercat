#include <RobotAPI/libraries/core/math/MathUtils.h>

#include "../FunctionalDevice.h"
#include "Position.h"

namespace devices::ethercat::hand::finger_vision_soft
{
    JointPositionController::JointPositionController(
        MotorControlDataPtr data,
        FunctionalDevicePtr hand,
        ControlTargetBase::ControlDeviceAccessToken token) :
        data(data),
        hand(hand),
        ctrl(data->getPosCtrlKp(), data->getPosCtrlKi(), data->getPosCtrlKd())
    {
        ARMARX_CHECK_NOT_NULL(data);
        ARMARX_CHECK_NOT_NULL(hand);
        target.setPWMLimits(data->getMaxPWM(), data->getPosCtrlDefaultMaxPWM(), token);
    }

    ControlTargetBase* JointPositionController::getControlTarget()
    {
        return &target;
    }

    void JointPositionController::rtRun(
        const IceUtil::Time& /*sensorValuesTimestamp*/,
        const IceUtil::Time& timeSinceLastIteration)
    {
        ARMARX_CHECK_EXPRESSION(target.isValid());
        last_pwm = ctrl.calculate(
                       data->getRelativeEncoderValue(),
                       target.position,
                       timeSinceLastIteration.toSecondsDouble(),
                       std::min<std::int16_t>(data->getMaxPWM(), target.maxPWM)
                   );
        data->setCommand(last_pwm);
    }

    void JointPositionController::rtPreActivateController()
    {
        ctrl.reset();
    }

    void JointPositionController::rtPostDeactivateController()
    {
        data->setCommand(last_pwm);
    }
}
