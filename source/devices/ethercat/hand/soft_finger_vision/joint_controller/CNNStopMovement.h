#pragma once

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>

namespace devices::ethercat::hand::finger_vision_soft
{
    class JointCNNStopMovementController : public JointController
    {
    public:
        JointCNNStopMovementController();
        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;
        ControlTargetBase* getControlTarget() override;
        void rtPreActivateController() override;
    private:
        DummyControlTargetStopMovement target;
    };
    using JointCNNStopMovementControllerPtr = std::shared_ptr<JointCNNStopMovementController>;
}
