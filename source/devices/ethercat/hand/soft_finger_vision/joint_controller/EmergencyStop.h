#pragma once

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>

#include "../bus_data/Motor.h"

namespace devices::ethercat::hand::finger_vision_soft
{
    class JointEmergencyStopController : public JointController
    {
    public:
        JointEmergencyStopController(
            MotorControlDataPtr dataPtr);
    private:
        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        ControlTargetBase* getControlTarget() override;

        void rtPreActivateController() override;
    private:
        DummyControlTargetEmergencyStop target;
        MotorControlDataPtr dataPtr;
    };
    using JointEmergencyStopControllerPtr =
        std::shared_ptr<JointEmergencyStopController>;
}
