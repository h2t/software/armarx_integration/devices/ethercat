#pragma once

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/libraries/core/PIDController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>

#include "../bus_data/Motor.h"
#include "../FunctionalDevice.h"
#include "../controller/VelocityToPWM.h"
#include "../joint_controller/Pwm.h"

namespace devices::ethercat::hand::finger_vision_soft
{
    class JointVelocityController : public JointController
    {
    public:
        JointVelocityController(
            MotorControlDataPtr data,
            FunctionalDevicePtr hand,
            ControlTargetBase::ControlDeviceAccessToken token);
        ControlTargetBase* getControlTarget() override;
        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;
        void rtPreActivateController() override;
        void rtPostDeactivateController() override;
    private:
        MotorControlDataPtr                           data;
        FunctionalDevicePtr                           hand;
        ControlTarget1DoFActuatorVelocityWithPWMLimit target;
        Controllers::VelocityToPWM                    ctrl;
        std::int16_t                                  last_pwm;
    };
    using JointVelocityControllerPtr =
        std::shared_ptr<JointVelocityController>;
}
