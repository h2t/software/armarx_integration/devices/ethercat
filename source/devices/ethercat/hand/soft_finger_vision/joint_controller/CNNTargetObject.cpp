#include "CNNTargetObject.h"

namespace devices::ethercat::hand::finger_vision_soft
{
    JointCNNTargetObjectController::JointCNNTargetObjectController(
        CNNControlDataPtr data) :
        data(data)
    {
        ARMARX_CHECK_NOT_NULL(data);
    }

    ControlTargetBase* JointCNNTargetObjectController::getControlTarget()
    {
        return &target;
    }

    void JointCNNTargetObjectController::rtRun(
        const IceUtil::Time&, const IceUtil::Time&)
    {
        data->setId(target.object_id);
    }

    void JointCNNTargetObjectController::rtPreActivateController()
    {}

    void JointCNNTargetObjectController::rtPostDeactivateController()
    {
        data->setId(0);
    }
}
