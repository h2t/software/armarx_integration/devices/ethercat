#include "CNNStopMovement.h"

namespace devices::ethercat::hand::finger_vision_soft
{
    JointCNNStopMovementController::JointCNNStopMovementController()
    {}

    void JointCNNStopMovementController::rtRun(
        const IceUtil::Time&, const IceUtil::Time&)
    {
    }

    ControlTargetBase* JointCNNStopMovementController::getControlTarget()
    {
        return &target;
    }

    void JointCNNStopMovementController::rtPreActivateController()
    {
    }
}
