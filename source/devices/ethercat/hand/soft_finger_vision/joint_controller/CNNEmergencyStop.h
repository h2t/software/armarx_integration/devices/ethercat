#pragma once

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>

namespace devices::ethercat::hand::finger_vision_soft
{
    class JointCNNEmergencyStopController : public JointController
    {
    public:
        JointCNNEmergencyStopController();
    private:
        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        ControlTargetBase* getControlTarget() override;

        void rtPreActivateController() override;
    private:
        DummyControlTargetEmergencyStop target;
    };
    
    using JointCNNEmergencyStopControllerPtr =
        std::shared_ptr<JointCNNEmergencyStopController>;

}
