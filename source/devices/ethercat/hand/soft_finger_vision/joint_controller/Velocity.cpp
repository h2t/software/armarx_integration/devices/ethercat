#include <boost/algorithm/clamp.hpp>

#include <RobotAPI/libraries/core/math/MathUtils.h>

#include "../FunctionalDevice.h"
#include "Velocity.h"

namespace devices::ethercat::hand::finger_vision_soft
{
    JointVelocityController::JointVelocityController(
        MotorControlDataPtr data,
        FunctionalDevicePtr hand,
        ControlTargetBase::ControlDeviceAccessToken token) :
        data(data),
        hand(hand),
        ctrl(data->getVelCtrlKp(), data->getVelCtrlKi(), data->getVelCtrlKd())
    {
        ARMARX_CHECK_NOT_NULL(data);
        ARMARX_CHECK_NOT_NULL(hand);
        target.setPWMLimits(data->getMaxPWM(), data->getVelCtrlDefaultMaxPWM(), token);
    }

    ControlTargetBase* JointVelocityController::getControlTarget()
    {
        return &target;
    }

    void JointVelocityController::rtRun(
        const IceUtil::Time& /*sensorValuesTimestamp*/,
        const IceUtil::Time& timeSinceLastIteration)
    {
        ARMARX_CHECK_EXPRESSION(target.isValid());

        last_pwm = ctrl.calculate(
                       data->getRelativeEncoderValue(),
                       target.velocity,
                       timeSinceLastIteration.toSecondsDouble(),
                       std::min<std::int16_t>(data->getMaxPWM(), target.maxPWM)
                   );
        data->setCommand(last_pwm);
    }

    void JointVelocityController::rtPreActivateController()
    {
        ctrl.reset(data->getRelativeEncoderValue());
    }

    void JointVelocityController::rtPostDeactivateController()
    {
        data->setCommand(last_pwm);
    }
}
