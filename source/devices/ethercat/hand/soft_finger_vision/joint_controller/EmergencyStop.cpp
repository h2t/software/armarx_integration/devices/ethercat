#include "EmergencyStop.h"

namespace devices::ethercat::hand::finger_vision_soft
{
    JointEmergencyStopController::JointEmergencyStopController(
        MotorControlDataPtr dataPtr) :
        dataPtr(dataPtr)
    {}
    void JointEmergencyStopController::rtRun(
        const IceUtil::Time&, const IceUtil::Time&)
    {
        dataPtr->setCommand(0);
    }

    ControlTargetBase* JointEmergencyStopController::getControlTarget()
    {
        return &target;
    }

    void JointEmergencyStopController::rtPreActivateController()
    {
        dataPtr->setCommand(0);
    }
}
