#pragma once

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>

#include "../bus_data/Motor.h"
#include "../FunctionalDevice.h"

namespace devices::ethercat::hand::finger_vision_soft
{
    using FunctionalDevicePtr = std::shared_ptr<class FunctionalDevice>;
    class JointPwmController : public JointController
    {
    public:
        JointPwmController(
            MotorControlDataPtr data,
            FunctionalDevicePtr hand);
        ControlTargetBase* getControlTarget() override;
        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;
        void rtPreActivateController() override;
        void rtPostDeactivateController() override;
    private:
        MotorControlDataPtr          data;
        FunctionalDevicePtr          hand;
        ControlTarget1DoFActuatorPWM target;
    };
    using JointPwmControllerPtr = std::shared_ptr<JointPwmController>;
}
