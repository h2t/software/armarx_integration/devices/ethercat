#pragma once

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>

#include "../bus_data/CNN.h"
#include "../control_target/CNNObjectId.h"
#include "../FunctionalDevice.h"

namespace devices::ethercat::hand::finger_vision_soft
{
    using FunctionalDevicePtr = std::shared_ptr<class FunctionalDevice>;
    class JointCNNTargetObjectController : public JointController
    {
    public:
        JointCNNTargetObjectController(CNNControlDataPtr data);
        ControlTargetBase* getControlTarget() override;
        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;
        void rtPreActivateController() override;
        void rtPostDeactivateController() override;
    private:
        CNNControlDataPtr        data;
        ControlTargetCNNObjectId target;
    };
    using JointCNNTargetObjectControllerPtr = std::shared_ptr<JointCNNTargetObjectController>;
}
