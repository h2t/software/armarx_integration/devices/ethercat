#include "StopMovement.h"

namespace devices::ethercat::hand::finger_vision_soft
{
    JointStopMovementController::JointStopMovementController(
        MotorControlDataPtr dataPtr) :
        dataPtr(dataPtr)
    {}

    void JointStopMovementController::rtRun(
        const IceUtil::Time&, const IceUtil::Time&)
    {
        dataPtr->setCommand(0);
    }

    ControlTargetBase* JointStopMovementController::getControlTarget()
    {
        return &target;
    }

    void JointStopMovementController::rtPreActivateController()
    {
        dataPtr->setCommand(0);
    }
}
