#pragma once

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>

#include "../bus_data/Motor.h"

namespace devices::ethercat::hand::finger_vision_soft
{
    class JointStopMovementController : public JointController
    {
    public:
        JointStopMovementController(
            MotorControlDataPtr dataPtr);
        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;
        ControlTargetBase* getControlTarget() override;
        void rtPreActivateController() override;
    private:
        DummyControlTargetStopMovement target;
        MotorControlDataPtr dataPtr;
    };
    using JointStopMovementControllerPtr = std::shared_ptr<JointStopMovementController>;
}
