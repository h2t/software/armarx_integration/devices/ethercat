#pragma once

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>

#include "../bus_data/Motor.h"
#include "../FunctionalDevice.h"
#include "../controller/PositionToPWM.h"
#include "../joint_controller/Pwm.h"

namespace devices::ethercat::hand::finger_vision_soft
{
    class JointPositionController : public JointController
    {
    public:
        JointPositionController(
            MotorControlDataPtr data,
            FunctionalDevicePtr hand,
            ControlTargetBase::ControlDeviceAccessToken token);
        ControlTargetBase* getControlTarget() override;
        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;
        void rtPreActivateController() override;
        void rtPostDeactivateController() override;
    private:
        MotorControlDataPtr                           data;
        FunctionalDevicePtr                           hand;
        ControlTarget1DoFActuatorPositionWithPWMLimit target;
        Controllers::PositionToPWM                    ctrl;
        std::int16_t                                  last_pwm;
    };
    using JointPositionControllerPtr = std::shared_ptr<JointPositionController>;
}
