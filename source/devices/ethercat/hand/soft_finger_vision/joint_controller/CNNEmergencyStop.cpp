#include "CNNEmergencyStop.h"

namespace devices::ethercat::hand::finger_vision_soft
{
    JointCNNEmergencyStopController::JointCNNEmergencyStopController()
    {}
    void JointCNNEmergencyStopController::rtRun(
        const IceUtil::Time&, const IceUtil::Time&)
    {
    }

    ControlTargetBase* JointCNNEmergencyStopController::getControlTarget()
    {
        return &target;
    }

    void JointCNNEmergencyStopController::rtPreActivateController()
    {
    }
}
