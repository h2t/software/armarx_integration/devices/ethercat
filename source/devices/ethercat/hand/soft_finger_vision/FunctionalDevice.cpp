#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/util/StringHelperTemplates.h>
#include <ArmarXCore/core/ManagedIceObject.h>

#include "joint_controller/CNNEmergencyStop.h"
#include "joint_controller/CNNStopMovement.h"
#include "joint_controller/CNNTargetObject.h"
#include "joint_controller/EmergencyStop.h"
#include "joint_controller/Position.h"
#include "joint_controller/Velocity.h"
#include "joint_controller/Pwm.h"
#include "joint_controller/StopMovement.h"

#include "njoint_controller/Shape.h"

//HandSensorDevice
namespace devices::ethercat::hand::finger_vision_soft::detail
{
    class HandSensorDevice : public SensorDevice
    {
        friend class FunctionalDevice;
    public:
        HandSensorDevice(const std::string& deviceName) :
            DeviceBase(deviceName), SensorDevice(deviceName)
        {
            ARMARX_DEBUG << "    ctor HandSensorDevice " << deviceName;
        }

        const HandSensorValue* getSensorValue() const override
        {
            return &sensorValue;
        }

        void init(const FunctionalDevicePtr& handPtr,
                  const HandSensorDataPtr& dataPtr)
        {
            ARMARX_DEBUG << "    init HandSensorDevice " << getDeviceName();
            ARMARX_CHECK_NOT_NULL(handPtr);
            ARMARX_CHECK_NOT_NULL(dataPtr);
            hand = handPtr;
            data = dataPtr;
        }
        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration) override
        {
            data->rtReadSensorValues(sensorValuesTimestamp, timeSinceLastIteration);
            data->updateSensorValueStruct(sensorValue);
        }
    private:
        FunctionalDevicePtr hand;
        HandSensorDataPtr data;
        HandSensorValue sensorValue;
    };
}

//MotorControlDevice
namespace devices::ethercat::hand::finger_vision_soft::detail
{
    class MotorControlDevice :
        public SensorDevice, public ControlDevice
    {
        friend class FunctionalDevice;
    public:
        MotorControlDevice(const std::string& deviceName) :
            DeviceBase(deviceName), SensorDevice(deviceName), ControlDevice(deviceName)
        {
            ARMARX_DEBUG << "    ctor MotorControlDevice " << deviceName;
        }

        const SensorValueBase* getSensorValue() const override
        {
            return &sensorValue;
        }

        void init(const FunctionalDevicePtr& handPtr,
                  const HandSensorDevicePtr& devHandPtr,
                  const HandSensorDataPtr&   dataHandPtr,
                  const MotorControlDataPtr& dataMotorPtr)
        {
            ARMARX_TRACE;
            ARMARX_DEBUG << "    init MotorControlDevice " << getDeviceName();
            ARMARX_CHECK_NOT_NULL(handPtr);
            ARMARX_CHECK_NOT_NULL(dataMotorPtr);
            ARMARX_CHECK_NOT_NULL(dataHandPtr);
            ARMARX_CHECK_NOT_NULL(devHandPtr);
            hand = handPtr;
            dataMotor = dataMotorPtr;
            dataHand = dataHandPtr;
            handSensorTemp = &(devHandPtr->getSensorValue()->temperature);

            ctrls.at(0).reset(new JointStopMovementController(dataMotor));
            ctrls.at(1).reset(new JointEmergencyStopController(dataMotor));
            ctrls.at(2).reset(new JointPwmController(dataMotor, hand));
            ctrls.at(3).reset(new JointPositionController(dataMotor, hand, getControlTargetAccessToken()));
            ctrls.at(4).reset(new JointVelocityController(dataMotor, hand, getControlTargetAccessToken()));

            for (const auto& ctrl : ctrls)
            {
                addJointController(ctrl.get());
            }
        }
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                 const IceUtil::Time& timeSinceLastIteration) override
        {
            if (motorIsShutDown)
            {
                motorIsShutDown = *handSensorTemp < dataHand->rtGetMotorTemperatureRestartThreshold();
            }
            motorIsShutDown = *handSensorTemp >= dataHand->rtGetMotorTemperatureShutdownThreshold();

            if (motorIsShutDown)
            {
                if (!firstRun)
                {
                    ARMARX_ERROR << deactivateSpam(5) << getDeviceName()
                                 << ": Hand Motor Temperature too high (threshold shut down: "
                                 << dataHand->rtGetMotorTemperatureShutdownThreshold()
                                 << ", threshold restart: "
                                 << dataHand->rtGetMotorTemperatureRestartThreshold()
                                 << ") - stopping motor - motor temperature: "
                                 << *handSensorTemp  << " °C";
                }
                dataMotor->setCommand(0);
            }
            dataMotor->rtWriteTargetValues(sensorValuesTimestamp, timeSinceLastIteration);
            firstRun = false;
        }
        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration) override
        {
            dataMotor->rtReadSensorValues(sensorValuesTimestamp, timeSinceLastIteration);
            dataMotor->updateSensorValueStruct(sensorValue);
        }
    private:
        FunctionalDevicePtr hand;

        MotorControlDataPtr dataMotor;
        HandSensorDataPtr dataHand;
        const float* handSensorTemp;
        MotorSensorValue sensorValue;

        std::array<std::unique_ptr<JointController>, 5> ctrls;

        bool motorIsShutDown = false;
        bool firstRun = true;
    };
}

//CNNControlDevice
namespace devices::ethercat::hand::finger_vision_soft::detail
{
    class CNNControlDevice :
        public ControlDevice
    {
        friend class FunctionalDevice;
    public:
        CNNControlDevice(const std::string& deviceName) :
            DeviceBase(deviceName), ControlDevice(deviceName)
        {
            ARMARX_DEBUG << "    ctor CNNControlDevice " << deviceName;
        }

        void init(const CNNControlDataPtr& dataPtr)
        {
            ARMARX_TRACE;
            ARMARX_DEBUG << "    init CNNControlDevice " << getDeviceName();
            ARMARX_CHECK_NOT_NULL(dataPtr);
            data = dataPtr;

            ctrls.at(0).reset(new JointCNNStopMovementController());
            ctrls.at(1).reset(new JointCNNEmergencyStopController());
            ctrls.at(2).reset(new JointCNNTargetObjectController(data));

            for (const auto& ctrl : ctrls)
            {
                addJointController(ctrl.get());
            }
        }
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                 const IceUtil::Time& timeSinceLastIteration) override
        {
            data->rtWriteTargetValues(sensorValuesTimestamp, timeSinceLastIteration);
        }
    private:
        CNNControlDataPtr data;

        std::array<std::unique_ptr<JointController>, 3> ctrls;
    };
}


//ControllerWrapper
namespace devices::ethercat::hand::finger_vision_soft
{
    TYPEDEF_PTRS_SHARED(ControllerWrapper);
    class ControllerWrapper : public AbstractHandUnitControllerWrapper
    {
    public:
        NJointShapeControllerPtr controller;

        void setShape(const std::string& shape) override
        {
            ARMARX_CHECK_EXPRESSION(controller);
            if (!controller->isControllerActive())
            {
                controller->activateController();
            }

            if (armarx::Contains(shape, "relax", true))
            {
                controller->stopMotion();
            }
            else if (armarx::Contains(shape, "half open", true))
            {
                controller->setTargets(0.5f, 0.5f, 0.5f);
            }
            else if (armarx::Contains(shape, "open", true))
            {
                controller->setTargets(0, 0, 0);
            }
            else if (armarx::Contains(shape, "close", true))
            {
                controller->setTargets(1, 1, 1);
            }
            else
            {
                ARMARX_ERROR << "shape '" << shape << "' not supported.";
            }
        }
        std::string describeHandState() const override
        {
            std::stringstream ss;
            ss.precision(3);
            ss << std::fixed;
            ss << controller->getIndexJointValue() << " " << controller->getIndexTarget() << " " << controller->getIndexPwm() << " / ";
            ss << controller->getOtherJointValue() << " " << controller->getOtherTarget() << " " << controller->getOtherPwm() << " / ";
            ss << controller->getThumbJointValue() << " " << controller->getThumbTarget() << " " << controller->getThumbPwm();
            ss << " " << (controller->isControlEnabled() ? "active" : "disabled");
            return ss.str();
        }

        void setJointAngles(const NameValueMap& targetJointAngles) override
        {
            ARMARX_CHECK_EXPRESSION(controller);
            if (!controller->isControllerActive())
            {
                controller->activateController();
            }

            float indexTarget = controller->getIndexTarget();
            float lmfTarget   = controller->getOtherTarget();
            float thumbTarget = controller->getThumbTarget();
            for (const auto& [name, value] : targetJointAngles)
            {
                if (name == "index")
                {
                    indexTarget = value;
                }
                else if (name == "other")
                {
                    lmfTarget = value;
                }
                else if (name == "thumb")
                {
                    thumbTarget = value;
                }
                else
                {
                    ARMARX_WARNING << "Invalid HandJointName '" << name << "', ignoring.";
                }
            }
            controller->setTargets(indexTarget, lmfTarget, thumbTarget);
        }

        std::map<std::string, float> getActualJointValues() override
        {
            NameValueMap jointValues;
            jointValues["index"] = controller->getIndexJointValue();
            jointValues["other"] = controller->getOtherJointValue();
            jointValues["thumb"] = controller->getThumbJointValue();
            return jointValues;
        }
    };
}

//FunctionalDevice
namespace devices::ethercat::hand::finger_vision_soft
{
    FunctionalDeviceFactory::SubClassRegistry
    FunctionalDevice::registry(
        "KITFingerVisionSoftHandV1",
        &FunctionalDeviceFactory::createInstance<FunctionalDevice>);

    FunctionalDevice::FunctionalDevice(
        RapidXmlReaderNode jointNode,
        DefaultRapidXmlReaderNode defaultConfigurationNode,
        VirtualRobot::RobotPtr const& robot
    ):
        AbstractFunctionalDevice(
            defaultConfigurationNode
            .first_node("KITFingerVisionSoftHandV1DefaultConfiguration")
            .add_node_at_end(jointNode)),
        deviceName(jointNode.attribute_value("name")),
        configNode(jointNode),
        robot(robot),
        slaveIdentifier(jointNode)
    {

    }

    void FunctionalDevice::init(SlavePtr slave)
    {
        ARMARX_TRACE;
        ARMARX_DEBUG << "init " << deviceName;
        ARMARX_CHECK_NOT_NULL(slave);
        //check if the sensorBoard is the correct one
        if (slave->getSlaveIdentifier().ProductID != slaveIdentifier.ProductID)
        {
            std::stringstream reason;
            reason << "Invalid Serial Number for SensorBoard for arm joint : "
                   << deviceName << "!\n"
                   << "product ID in config: " << slaveIdentifier.ProductID << " "
                   << "product ID from bus:  " << slave->getSlaveIdentifier().ProductID;
            throw LocalException(reason.str());
        }

        handSlave = slave;
        //create devices
        {
            using MDevT = detail::MotorControlDevice;
            using HDevT = detail::HandSensorDevice;
            using CNNDevT = detail::CNNControlDevice;

            otherControlDevicePtr = armarx::make_shared<MDevT  >(deviceName + ".motor.other");
            indexControlDevicePtr = armarx::make_shared<MDevT  >(deviceName + ".motor.index");
            thumbControlDevicePtr = armarx::make_shared<MDevT  >(deviceName + ".motor.thumb");
            handSensorDevicePtr   = armarx::make_shared<HDevT  >(deviceName);
            cnnControlDevicePtr   = armarx::make_shared<CNNDevT>(deviceName + ".cnn");
        }
    }

    void FunctionalDevice::initData()
    {
        ARMARX_TRACE;
        ARMARX_DEBUG << "initData " << deviceName;
        auto hand = std::dynamic_pointer_cast<FunctionalDevice>(shared_from_this());

        //init data devices
        {
            //init ctrl
            {
                auto* ctrlAll = handSlave->getInputsPtr();
                auto* snsAll = handSlave->getOutputsPtr();
                auto init = [&](auto & sd, auto * cf, auto * sf, auto name)
                {
                    ARMARX_TRACE_LITE;
                    sd = std::make_shared<MotorControlData>(
                             configNode, getNode(),
                             cf, snsAll, sf,
                             name);
                };
                init(otherControlDataPtr, &ctrlAll->other, &snsAll->motor_other, "other");
                init(indexControlDataPtr, &ctrlAll->index, &snsAll->motor_index, "index");
                init(thumbControlDataPtr, &ctrlAll->thumb, &snsAll->motor_thumb, "thumb");
            }
            //hand
            {
                handSensorDataPtr = std::make_shared<HandSensorData>(
                                        configNode, getNode(), handSlave->getOutputsPtr(), "Hand");
            }
            //cnn
            {
                cnnControlDataPtr = std::make_shared<CNNControlData>(&(handSlave->getInputsPtr()->cnn));
            }
        }
        //init devices
        {
            handSensorDevicePtr  ->init(hand, handSensorDataPtr);

            otherControlDevicePtr->init(hand, handSensorDevicePtr, handSensorDataPtr, otherControlDataPtr);
            indexControlDevicePtr->init(hand, handSensorDevicePtr, handSensorDataPtr, indexControlDataPtr);
            thumbControlDevicePtr->init(hand, handSensorDevicePtr, handSensorDataPtr, thumbControlDataPtr);

            cnnControlDevicePtr->init(cnnControlDataPtr);
        }
    }

    SlaveIdentifier FunctionalDevice::getSlaveIdentifier() const
    {
        return slaveIdentifier;
    }

    AbstractHandUnitControllerWrapperPtr FunctionalDevice::createHandUnitControllerWrapper(
        RobotUnit* robotUnit,
        const std::string& handName,
        RapidXmlReaderNode configNode) const
    {
        ARMARX_TRACE;
        ControllerWrapperPtr wrapper(new ControllerWrapper());
        NJointShapeControllerConfigPtr cfg =
            new NJointShapeControllerConfig(deviceName);
        ARMARX_INFO << "createHandUnitControllerWrapper " << VAROUT(deviceName);
        wrapper->controller = NJointShapeControllerPtr::dynamicCast(
                                  robotUnit->createNJointController("NJointKITFingerVisionSoftHandV1ShapeController",
                                          handName + "_NJointKITFingerVisionSoftHandV1ShapeController",
                                          cfg, false, true));
        ARMARX_TRACE;
        DefaultRapidXmlReaderNode defaultConfigurationNode
        {
            configNode
            .parent_node()
            .first_node("DefaultConfiguration")
            .first_node("KITFingerVisionSoftHandV1DefaultConfiguration")
        };
        wrapper->controller->readConfig(configNode, defaultConfigurationNode);
        return wrapper;
    }

    std::string FunctionalDevice::getHandConfigNodeName() const
    {
        return "KITFingerVisionSoftHandV1";
    }

    std::string FunctionalDevice::getDeviceName() const
    {
        return deviceName;
    }

    std::string FunctionalDevice::getHandDeviceName() const
    {
        return deviceName;
    }

    void FunctionalDevice::addDevicesTo(
        const KITFingerVisionSoftHandV1FactoryPtr& f)
    {
        ARMARX_TRACE;

        f->addControlDevice(otherControlDevicePtr);
        f->addSensorDevice(otherControlDevicePtr);

        f->addControlDevice(indexControlDevicePtr);
        f->addSensorDevice(indexControlDevicePtr);

        f->addControlDevice(thumbControlDevicePtr);
        f->addSensorDevice(thumbControlDevicePtr);

        f->addSensorDevice(handSensorDevicePtr);

        f->addControlDevice(cnnControlDevicePtr);
    }
}
