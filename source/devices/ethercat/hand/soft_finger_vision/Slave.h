#pragma once

#include <armarx/control/ethercat/AbstractSlave.h>
#include <armarx/control/ethercat/DeviceFactory.h>

#include <memory>
#include <thread>
#include <atomic>

#include "BoardIn.h"
#include "BoardOut.h"

namespace devices::ethercat::hand::finger_vision_soft
{
    const std::uint32_t H2T_RIGHT_PRODUCT_CODE = 0x42424243;
    const std::uint32_t H2T_LEFT_PRODUCT_CODE  = 0x43434344;

    using SlavePtr = std::shared_ptr<class Slave>;

    class Slave :
        public armarx::control::ethercat::AbstractSlaveWithInputOutput<BoardIn, BoardOut>
    {

    public:
        Slave(const armarx::control::ethercat::SlaveIdentifier slaveIdentifier, u_int16_t slaveNumber);

        void doMappings() override;
        void prepareForOp() override;
        void finishPreparingForOp() override;
        bool prepare() override;
        void execute() override;
        bool shutdown() override;

        /**
         * @see AbstractSlave::hasError()
         * @return
         */
        bool hasError() override;

        void prepareForSafeOp() override;
        void finishPreparingForSafeOp() override;
    };

    class KITFingerVisionSoftHandV1Factory : public armarx::control::ethercat::DeviceFactory
    {
    public:
        KITFingerVisionSoftHandV1Factory() {}
        static std::string getName()
        {
            return "KITFingerVisionSoftHandV1Factory";
        }
    private:
        static SubClassRegistry registry;
        static SharedPointerType createInstance(FactoryArgs args);
    };
    using KITFingerVisionSoftHandV1FactoryPtr = std::shared_ptr<KITFingerVisionSoftHandV1Factory>;

}  // namespace devices::ethercat::hand::finger_vision_soft
