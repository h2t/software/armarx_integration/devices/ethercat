/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::ActuatorVelocityTarget
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTargetBase.h>
#include <ArmarXCore/observers/variant/Variant.h>

namespace devices::ethercat::hand::finger_vision_soft::ControlModes
{
    //'normal' actor modes
    static const std::string CNNObjectId = "ControlMode_CNNObjectId";
}
namespace devices::ethercat::hand::finger_vision_soft
{
    class ControlTargetCNNObjectId: public ControlTargetBase
    {
    public:
        std::int16_t object_id = 0;
        ControlTargetCNNObjectId() = default;
        ControlTargetCNNObjectId(const ControlTargetCNNObjectId&) = default;
        ControlTargetCNNObjectId(ControlTargetCNNObjectId&&) = default;
        ControlTargetCNNObjectId(std::int16_t val) : object_id{val} {}
        ControlTargetCNNObjectId& operator=(const ControlTargetCNNObjectId&) = default;
        ControlTargetCNNObjectId& operator=(ControlTargetCNNObjectId&&) = default;
        ControlTargetCNNObjectId& operator=(std::int16_t val)
        {
            object_id = val;
            return *this;
        }
        virtual const std::string& getControlMode() const override
        {
            return ControlModes::CNNObjectId;
        }
        virtual void reset() override
        {
            object_id = 0;
        }
        virtual bool isValid() const override
        {
            return true;
        }
        DETAIL_ControlTargetBase_DEFAULT_METHOD_IMPLEMENTATION
        static ControlTargetInfo<ControlTargetCNNObjectId> GetClassMemberInfo()
        {
            ControlTargetInfo<ControlTargetCNNObjectId> cti;
            cti.addMemberVariable(&ControlTargetCNNObjectId::object_id, "object_id");
            return cti;
        }
    };
}
