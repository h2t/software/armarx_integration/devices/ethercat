#pragma once

#include <armarx/control/ethercat/AbstractSlave.h>
#include <armarx/control/ethercat/DeviceFactory.h>

namespace devices::ethercat::hand::finger_vision_soft
{
    struct BoardIn_motor_target
    {
        int16_t pwm;
    } __attribute__((__packed__));

    struct BoardIn_cnn_target
    {
        int16_t object_id;
    } __attribute__((__packed__));

    /// @brief PDO mapping master->sensorB
    struct BoardIn
    {
        BoardIn_motor_target thumb;
        BoardIn_motor_target index;
        BoardIn_motor_target other;
        BoardIn_cnn_target   cnn;
    } __attribute__((__packed__));
    static_assert(sizeof(BoardIn) == (64) / 8);
}
