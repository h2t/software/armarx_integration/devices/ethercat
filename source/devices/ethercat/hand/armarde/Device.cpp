/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Armar6RT::ArmarXObjects::Armar6IceKinematicUnit
 * @author     Markus Swarowsky ( markus dot swarowsky at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Device.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/util/StringHelperTemplates.h>

#include <devices/ethercat/common/h2t_devices.h>
#include <devices/ethercat/hand/armarde/joint_controller/PWM.h>
#include <devices/ethercat/hand/armarde/joint_controller/Position.h>
#include <devices/ethercat/hand/armarde/njoint_controller/Shape.h>


namespace devices::ethercat::hand::armarde
{

    Device::Device(armarx::control::hardware_config::DeviceConfig& hwConfig,
                   VirtualRobot::RobotPtr const& robot) :
        DeviceBase(hwConfig.getName()),
        DeviceInterface(hwConfig.getName()),
        robot(robot),
        fingersDataPtr(nullptr),
        indexDataPtr(nullptr),
        thumbDataPtr(nullptr),
        thumbRotationDataPtr(nullptr),
        slaveIdentifier(hwConfig.getOnlySlaveConfig().getIdentifier()),
        handSlave(nullptr),
        fingersDataConfig(hwConfig, hwConfig.getSubdeviceConfig("Fingers")),
        indexDataConfig(hwConfig, hwConfig.getSubdeviceConfig("Index")),
        thumbDataConfig(hwConfig, hwConfig.getSubdeviceConfig("Thumb")),
        thumbRotationDataConfig(hwConfig, hwConfig.getSubdeviceConfig("ThumbRotation"))
    {
        motorTemperatureShutdownThreshold = hwConfig.getFloat("motorTemperatureShutdownThreshold");
    }


    Device::~Device() = default;


    armarx::control::ethercat::DeviceInterface::TryAssignResult
    Device::tryAssign(armarx::control::ethercat::SlaveInterface& slave)
    {
        using Result = DeviceInterface::TryAssignResult;

        Result result = ethercat::common::tryAssignLegacyH2TDevice(
            slave, handSlave, slaveIdentifier.productCode);
        if (result != Result::unknown)
        {
            return result;
        }

        return result;
    }


    armarx::control::ethercat::DeviceInterface::AllAssignedResult
    Device::onAllAssigned()
    {
        using Result = DeviceInterface::AllAssignedResult;

        if (handSlave)
        {
            thumbDevice.reset(new RobotUnitDevice(getDeviceName() + "Thumb"));
            fingersDevice.reset(new RobotUnitDevice(getDeviceName() + "Fingers"));
            indexDevice.reset(new RobotUnitDevice(getDeviceName() + "Index"));
            thumbRotationDevice.reset(new RobotUnitDevice(getDeviceName() + "ThumbRotation"));

            // Also add both devices to subDevices
            subDevices.push_back(thumbDevice);
            subDevices.push_back(fingersDevice);
            subDevices.push_back(indexDevice);
            subDevices.push_back(thumbRotationDevice);

            return Result::ok;
        }

        return Result::slavesMissing;
    }


    std::string
    Device::getClassName() const
    {
        return "ArmardeHand";
    }


    void
    Device::postSwitchToSafeOp()
    {
        if (!fingersDataPtr && !thumbDataPtr && !indexDataPtr && !thumbRotationDataPtr)
        {
            fingersDataPtr = std::make_unique<Data>(fingersDataConfig,
                                                    handSlave->getOutputsPtr(),
                                                    handSlave->getInputsPtr(),
                                                    MotorFingerType::Fingers);
            thumbDataPtr = std::make_unique<Data>(thumbDataConfig,
                                                  handSlave->getOutputsPtr(),
                                                  handSlave->getInputsPtr(),
                                                  MotorFingerType::Thumb);
            indexDataPtr = std::make_unique<Data>(indexDataConfig,
                                                  handSlave->getOutputsPtr(),
                                                  handSlave->getInputsPtr(),
                                                  MotorFingerType::Index);
            thumbRotationDataPtr = std::make_unique<Data>(thumbRotationDataConfig,
                                                          handSlave->getOutputsPtr(),
                                                          handSlave->getInputsPtr(),
                                                          MotorFingerType::ThumbRotation);

            thumbDevice->init(this, thumbDataPtr.get());
            fingersDevice->init(this, fingersDataPtr.get());
            indexDevice->init(this, indexDataPtr.get());
            thumbRotationDevice->init(this, thumbRotationDataPtr.get());
        }

        ARMARX_CHECK_NOT_NULL(fingersDataPtr);
        ARMARX_CHECK_NOT_NULL(thumbDataPtr);
        ARMARX_CHECK_NOT_NULL(indexDataPtr);
        ARMARX_CHECK_NOT_NULL(thumbRotationDataPtr);
    }


    armarx::control::ethercat::SlaveIdentifier
    Device::getSlaveIdentifier() const
    {
        return slaveIdentifier;
    }


    const Device::RobotUnitDevicePtr&
    Device::getFingersDevice() const
    {
        return fingersDevice;
    }


    const Device::RobotUnitDevicePtr&
    Device::getThumbDevice() const
    {
        return thumbDevice;
    }


    const Device::RobotUnitDevicePtr&
    Device::getIndexDevice() const
    {
        return indexDevice;
    }


    const Device::RobotUnitDevicePtr&
    Device::getThumbRotationDevice() const
    {
        return thumbRotationDevice;
    }


    void
    Device::rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration)
    {
        ARMARX_CHECK_EXPRESSION(false);
        // called from multiple robotunit devices
        if ((sensorValuesTimestamp - lastWriteUpdate).toMicroSeconds() == 0)
        {
            return;
        }
        lastWriteUpdate = sensorValuesTimestamp;

        if (!fingersDataPtr || !indexDataPtr || !thumbDataPtr || !thumbRotationDataPtr)
        {
            DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                   "ArmardeHand has no data set, switch to SafeOP before");
        }

        if (fingersDataPtr->getMotorTemperature() > motorTemperatureShutdownThreshold ||
            indexDataPtr->getMotorTemperature() > motorTemperatureShutdownThreshold ||
            thumbDataPtr->getMotorTemperature() > motorTemperatureShutdownThreshold)
        {

            DEVICE_ERROR(rtGetDeviceName(),
                         "Hand Motor Temperature too high (threshold: %f) - stopping motors - "
                         "fingers motor temperature: %f°C index motor temperature: %f°C thumb motor temperature: %f°C",
                         motorTemperatureShutdownThreshold,
                         fingersDataPtr->getMotorTemperature(),
                         indexDataPtr->getMotorTemperature(),
                         thumbDataPtr->getMotorTemperature())
                .deactivateSpam(5);
            fingersDataPtr->setMotorPwmValue(0);
            indexDataPtr->setMotorPwmValue(0);
            thumbDataPtr->setMotorPwmValue(0);
            thumbRotationDataPtr->setMotorPwmValue(0);
        }

        fingersDataPtr->rtWriteTargetValues(sensorValuesTimestamp, timeSinceLastIteration);
        indexDataPtr->rtWriteTargetValues(sensorValuesTimestamp, timeSinceLastIteration);
        thumbDataPtr->rtWriteTargetValues(sensorValuesTimestamp, timeSinceLastIteration);
        thumbRotationDataPtr->rtWriteTargetValues(sensorValuesTimestamp, timeSinceLastIteration);
    }


    TYPEDEF_PTRS_SHARED(HandControllerWrapperArmarde);


    class HandControllerWrapperArmarde : public common::AbstractHandUnitControllerWrapper
    {

    public:
        ShapeControllerPtr controller;

        void
        setShape(const std::string& shape) override
        {
            ARMARX_CHECK_EXPRESSION(controller);
            if (!controller->isControllerActive())
            {
                controller->activateController();
            }

            if (armarx::Contains(shape, "relax", true))
            {
                controller->stopMotion();
            }
            else if (armarx::Contains(shape, "half open", true))
            {
                controller->setTargets(0.5f, 0.5f, 0.5f, 0.5f);
            }
            else if (armarx::Contains(shape, "open", true))
            {
                controller->setTargets(0, 0, 0, 0);
            }
            else if (armarx::Contains(shape, "close", true))
            {
                controller->setTargets(1, 1, 1, 1);
            }
            else
            {
                ARMARX_ERROR << "shape '" << shape << "' not supported.";
            }
        }

        std::string
        describeHandState() const override
        {
            std::stringstream ss;
            ss.precision(3);
            ss << std::fixed;
            ss << controller->getFingersJointValue() << " " << controller->getFingersTarget() << " "
               << controller->getFingersPwm();
            ss << " / ";
            ss << controller->getIndexJointValue() << " " << controller->getIndexTarget() << " "
               << controller->getIndexPwm();
            ss << " / ";
            ss << controller->getThumbJointValue() << " " << controller->getThumbTarget() << " "
               << controller->getThumbPwm();
            ss << " / ";
            ss << controller->getThumbRotationJointValue() << " " << controller->getThumbRotationTarget() << " "
               << controller->getThumbRotationPwm();
            ss << " / ";
            ss << " " << (controller->isControlEnabled() ? "active" : "disabled");
            return ss.str();
        }

        void
        setJointAngles(const armarx::NameValueMap& targetJointAngles) override
        {
            ARMARX_CHECK_EXPRESSION(controller);
            if (!controller->isControllerActive())
            {
                controller->activateController();
            }

            float fingersTarget = controller->getFingersTarget();
            float indexTarget = controller->getIndexTarget();
            float thumbTarget = controller->getThumbJointValue();
            float thumbRotationTarget = controller->getThumbRotationTarget();
            for (const auto& pair : targetJointAngles)
            {
                if (pair.first == "Fingers")
                {
                    fingersTarget = pair.second;
                }
                else if (pair.first == "Index")
                {
                    indexTarget = pair.second;
                }
                else if (pair.first == "Thumb")
                {
                    thumbTarget = pair.second;
                }
                else if (pair.first == "ThumbRotation")
                {
                    thumbRotationTarget = pair.second;
                }
                else
                {
                    ARMARX_WARNING << "Invalid HandJointName '" << pair.first << "', ignoring.";
                }
            }
            controller->setTargets(fingersTarget, indexTarget, thumbTarget, thumbRotationTarget);
        }

        std::map<std::string, float>
        getActualJointValues() override
        {
            armarx::NameValueMap jointValues;
            jointValues["Fingers"] = controller->getFingersJointValue();
            jointValues["Index"] = controller->getIndexJointValue();
            jointValues["Thumb"] = controller->getThumbJointValue();
            jointValues["ThumbRotation"] = controller->getThumbRotationJointValue();
            return jointValues;
        }
    };


    common::AbstractHandUnitControllerWrapperPtr
    Device::createHandUnitControllerWrapper(armarx::RobotUnit* robotUnit,
                                            const std::string& handName) const
    {
        HandControllerWrapperArmardePtr wrapper(new HandControllerWrapperArmarde());
        ShapeControllerConfigPtr cfg = new ShapeControllerConfig(getDeviceName());
        wrapper->controller = ShapeControllerPtr::dynamicCast(
            robotUnit->createNJointController("NJointArmardeHandShapeController",
                                              handName + "_NJointArmardeHandShapeController",
                                              cfg,
                                              false,
                                              true));
        wrapper->controller->setConfigs(fingersDataConfig, indexDataConfig, thumbDataConfig, thumbRotationDataConfig);
        return wrapper;
    }


    Device::RobotUnitDevice::RobotUnitDevice(const std::string& deviceName) :
        DeviceBase(deviceName),
        DeviceInterface::SubDeviceInterface(deviceName),
        ControlDevice(deviceName),
        SensorDevice(deviceName)
    {
    }


    void
    Device::RobotUnitDevice::init(Device* hand, Data* dataPtr)
    {
        this->hand = hand;
        this->dataPtr = dataPtr;
        stopMovementController.reset(new StopMovementController(dataPtr));
        emergencyController.reset(new EmergencyStopController(dataPtr));
        shapeController.reset(new PWMController(dataPtr, hand));
        positionController.reset(new PositionController(dataPtr, hand));

        addJointController(emergencyController.get());
        addJointController(stopMovementController.get());
        addJointController(shapeController.get());
        addJointController(positionController.get());
    }


    void
    Device::RobotUnitDevice::rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                                const IceUtil::Time& timeSinceLastIteration)
    {
//        ARMARX_INFO << rtGetDeviceName() << "Read sensor values ...";
        if (dataPtr == nullptr)
        {
            DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                   "Joint has no data set, switch to SafeOP before");
        }

        dataPtr->rtReadSensorValues(sensorValuesTimestamp, timeSinceLastIteration);
        dataPtr->updateSensorValueStruct(sensorValue);
    }


    void
    Device::RobotUnitDevice::rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                                 const IceUtil::Time& timeSinceLastIteration)
    {
        if (dataPtr->getMotorTemperature() > hand->motorTemperatureShutdownThreshold)
        {
            DEVICE_ERROR(rtGetDeviceName(),
                         "Hand Motor Temperature too high (threshold: %f) - stopping motors - "
                         "motor temperature: %f°C",
                         hand->motorTemperatureShutdownThreshold,
                         dataPtr->getMotorTemperature())
                .deactivateSpam(5);
            dataPtr->setMotorPwmValue(0);
        }

        dataPtr->rtWriteTargetValues(sensorValuesTimestamp, timeSinceLastIteration);
    }


    std::string
    Device::getHandDeviceName() const
    {
        return getDeviceName();
    }

} // namespace devices::ethercat::hand::armarde
