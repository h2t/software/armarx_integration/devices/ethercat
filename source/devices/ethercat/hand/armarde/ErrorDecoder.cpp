/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Jan Hausberg (jan dot hausberg at kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "ErrorDecoder.h"

namespace devices::ethercat::hand::armarde
{
    ErrorDecoder::ErrorDecoder(SlaveOut* outputs, SlaveIn* inputs) :
        outputs(outputs), inputs(inputs)
    {
    }

    bool
    ErrorDecoder::getEmergencyStopStatus() const
    {
        ARMARX_CHECK_NOT_NULL(outputs);
        return (0x80 & outputs->statusBits) ? true : false;
    }

    bool
    ErrorDecoder::motorErrorDetected(armarx::control::ethercat::SlaveInterface* slave,
                            bool printError) const
    {
        std::uint8_t errors = outputs->statusBits;

        if (printError)
        {
            if (errors & MotorStatus::ErrorFingers)
            {
                SLAVE_ERROR(
                    slave->getSlaveIdentifier(),
                    "MotorFingers error.")
                    .deactivateSpam(1);
            }
            if (errors & MotorStatus::ErrorIndex)
            {
                SLAVE_ERROR(
                    slave->getSlaveIdentifier(),
                    "MotorIndex error.")
                    .deactivateSpam(1);
            }
            if (errors & MotorStatus::ErrorThumb)
            {
                SLAVE_ERROR(
                    slave->getSlaveIdentifier(),
                    "MotorThumb error.")
                    .deactivateSpam(1);
            }
            if (errors & MotorStatus::ErrorThumbRotation)
            {
                SLAVE_ERROR(
                    slave->getSlaveIdentifier(),
                    "MotorThumbRotation error.")
                    .deactivateSpam(1);
            }
        }

        // Return true iff there are errors
        return (errors &
                (MotorStatus::ErrorFingers |
                 MotorStatus::ErrorIndex |
                 MotorStatus::ErrorThumb |
                 MotorStatus::ErrorThumbRotation))
                   ? true
                   : false;
    }

    bool
    ErrorDecoder::absoluteEncoderErrorDetected(armarx::control::ethercat::SlaveInterface* slave,
                                      bool printError) const
    {
        std::uint8_t errors = outputs->absoluteEncoderStatusBits;

        std::uint8_t numBytes = (errors & AbsoluteEncoderStatus::NumBytes) * 2 + 2;

        if (printError)
        {
            if (errors & AbsoluteEncoderStatus::BurstMode)
            {
                SLAVE_WARNING(
                    slave->getSlaveIdentifier(),
                    "Absolute Encoder: Burst mode is active, although only Single Measurement mode should be active.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::WocMode)
            {
                SLAVE_WARNING(
                    slave->getSlaveIdentifier(),
                    "Absolute Encoder: Wake-Up on Change mode is active, although only Single Measurement mode should be active.")
                    .deactivateSpam(1);
            }
            if (!(errors & AbsoluteEncoderStatus::SmMode))
            {
                SLAVE_WARNING(
                    slave->getSlaveIdentifier(),
                    "Absolute Encoder: Single Measurement mode is not set.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::Error)
            {
                SLAVE_ERROR(
                    slave->getSlaveIdentifier(),
                    "Absolute Encoder: A command has been rejected or an uncorrectable error is "
                    "detected in the memory.")
                    .deactivateSpam(1);
            }
            if (numBytes != 6)
            {
                SLAVE_ERROR(
                    slave->getSlaveIdentifier(),
                    "Absolute Encoder: Number of response data bytes is %d instead of 6.", numBytes)
                    .deactivateSpam(1);
            }
        }

        // Return true if there are errors
        return (errors & AbsoluteEncoderStatus::Error ? true : false) || numBytes != 6;
    }

} // namespace devices::ethercat::hand::armarde
