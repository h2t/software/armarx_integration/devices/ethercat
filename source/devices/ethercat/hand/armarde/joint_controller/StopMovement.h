#pragma once


// armarx
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>

// robot_devices
#include <devices/ethercat/hand/armarde/Data.h>


namespace devices::ethercat::hand::armarde
{

    using StopMovementControllerPtr = std::shared_ptr<class StopMovementController>;


    class StopMovementController : public armarx::JointController
    {

    public:

        StopMovementController(Data* dataPtr);

    private:

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

        /**
         * Returns the Target for this controller, but as this is the Emergency controller it will ignored.
         * As this controller will just break
         * @return is type VelocityTarget but it will return a nullptr, because it won't be possible to set a target
         */
        armarx::ControlTargetBase* getControlTarget() override;

        void rtPreActivateController() override;

    private:

        armarx::DummyControlTargetStopMovement target;
        Data* dataPtr;

    };

} // namespace armarx
