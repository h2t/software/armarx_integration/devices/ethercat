#include <devices/ethercat/hand/armarde/joint_controller/StopMovement.h>


namespace devices::ethercat::hand::armarde
{

    StopMovementController::StopMovementController(Data* dataPtr) :
        dataPtr(dataPtr)
    {

    }


    void StopMovementController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {

    }


    armarx::ControlTargetBase* StopMovementController::getControlTarget()
    {
        return &target;
    }


    void StopMovementController::rtPreActivateController()
    {
        ARMARX_RT_LOGF_INFO("Stopping hand!");
        dataPtr->setMotorPwmValue(0);
    }

} // namespace armarx
