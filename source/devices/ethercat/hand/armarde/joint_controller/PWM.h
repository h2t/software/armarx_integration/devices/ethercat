#pragma once


// armarx
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>

// robot_devices
#include <devices/ethercat/hand/armarde/Device.h>
#include <devices/ethercat/hand/armarde/Data.h>


namespace devices::ethercat::hand::armarde
{

    namespace ctrl_modes
    {
        static const std::string ArmardeHandPwm = "ControlMode_HandPwm";
    }


    class PWMControlTarget :
        public armarx::ControlTargetBase
    {

    public:

        std::int16_t pwm_motor;

        const std::string& getControlMode() const override
        {
            return ctrl_modes::ArmardeHandPwm;
        }

        void reset() override
        {
            pwm_motor = std::numeric_limits<int16_t>::max();
        }

        bool isValid() const override
        {
            return std::abs(pwm_motor) < 256 ;
        }

        static ControlTargetInfo<PWMControlTarget> GetClassMemberInfo()
        {
            ControlTargetInfo<PWMControlTarget> cti;
            cti.addMemberVariable(&PWMControlTarget::pwm_motor, "pwm_motor");
            return cti;
        }

        DETAIL_ControlTargetBase_DEFAULT_METHOD_IMPLEMENTATION

    };


    class PWMController :
        public armarx::JointController
    {

    public:

        PWMController(Data* data, Device* hand);

        // JointController interface

    public:

        armarx::ControlTargetBase* getControlTarget() override;

    protected:

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        void rtPreActivateController() override;
        void rtPostDeactivateController() override;

    private:

        Data* data;
        Device* hand;

        PWMControlTarget target;

    };


    using PWMControllerPtr = std::shared_ptr<PWMController>;

} // namespace armarx
