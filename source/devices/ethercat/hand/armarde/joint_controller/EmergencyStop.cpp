#include <devices/ethercat/hand/armarde/joint_controller/EmergencyStop.h>


namespace devices::ethercat::hand::armarde
{

    EmergencyStopController::EmergencyStopController(Data* dataPtr) :
        dataPtr(dataPtr)
    {

    }


    void EmergencyStopController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {

    }


    armarx::ControlTargetBase* EmergencyStopController::getControlTarget()
    {
        return &target;
    }


    void EmergencyStopController::rtPreActivateController()
    {
        ARMARX_INFO << "Stopping hand!";
        dataPtr->setMotorPwmValue(0);
    }

} // namespace armarx
