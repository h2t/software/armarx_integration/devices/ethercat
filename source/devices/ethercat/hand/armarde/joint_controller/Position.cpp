#include <devices/ethercat/hand/armarde/joint_controller/Position.h>


// armarx
#include <RobotAPI/libraries/core/math/MathUtils.h>


namespace devices::ethercat::hand::armarde
{

    PositionController::PositionController(Data* data, Device* hand) :
        data(data),
        hand(hand),
        PID(data->getKp(), 0.0f, 0.0f)

    {
        // thumbPID.threadSafe = false;
        PID.threadSafe = false;
    }


    armarx::ControlTargetBase* PositionController::getControlTarget()
    {
        return &target;
    }


    void PositionController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        if (target.isValid())
        {
            int maxPWM = std::min<int>(data->getMaxPWM(), target.maxPWM);

            PID.update(timeSinceLastIteration.toSecondsDouble(), data->getMotorPosition(), target.position);
            std::int16_t TargetPWM = armarx::math::MathUtils::LimitMinMax(-maxPWM, maxPWM, int(PID.getControlValue() * maxPWM));
            data->setMotorPwmValue(TargetPWM);
        }
    }


    void PositionController::rtPreActivateController()
    {
    }


    void PositionController::rtPostDeactivateController()
    {
        data->setMotorPwmValue(0);
    }

} // namespace armarx
