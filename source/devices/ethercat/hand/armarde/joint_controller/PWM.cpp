#include <devices/ethercat/hand/armarde/joint_controller/PWM.h>


namespace devices::ethercat::hand::armarde
{

    PWMController::PWMController(Data* data, Device* hand) :
        data(data),
        hand(hand)
    {

    }


    armarx::ControlTargetBase* PWMController::getControlTarget()
    {
        return &target;
    }


    void PWMController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        if (target.isValid())
        {
            data->setMotorPwmValue(target.pwm_motor);
        }
    }


    void PWMController::rtPreActivateController()
    {
    }


    void PWMController::rtPostDeactivateController()
    {
        data->setMotorPwmValue(0);
    }

} // namespace armarx
