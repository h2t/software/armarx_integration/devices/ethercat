/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Jan Hausberg (jan dot hausberg at kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <armarx/control/ethercat/SlaveInterface.h>

#include <devices/ethercat/hand/armarde/SlaveIO.h>

namespace armarx::control::ethercat
{
    class SlaveInterface;
}

namespace devices::ethercat::hand::armarde
{

    enum MotorStatus : std::uint8_t
    {
        // clang-format off
        ErrorFingers =       0b01000000,
        ErrorIndex   =       0b00100000,
        ErrorThumb   =       0b00010000,
        ErrorThumbRotation = 0b00001000
        // clang-format on
    };

    enum AbsoluteEncoderStatus : std::uint8_t
    {
        // clang-format off
        BurstMode =            0b10000000,
        WocMode =              0b01000000,
        SmMode =               0b00100000,
        Error =                0b00010000,
        SingleErrorCorrected = 0b00001000,
        Reset =                0b00000100,
        NumBytes =             0b00000011
        // clang-format on
    };

    class ErrorDecoder
    {
    public:
        ErrorDecoder(SlaveOut* outputs, SlaveIn* inputs);

        bool getEmergencyStopStatus() const;

        bool motorErrorDetected(armarx::control::ethercat::SlaveInterface* slave,
                                bool printError = false) const;

        bool absoluteEncoderErrorDetected(armarx::control::ethercat::SlaveInterface* slave,
                                          bool printError = false) const;

    private:
        SlaveOut* outputs;
        SlaveIn* inputs;
    };
} // namespace devices::ethercat::hand::armarde
