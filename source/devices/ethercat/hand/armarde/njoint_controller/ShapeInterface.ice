/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::NJointControllerInterface
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// armarx
#include <RobotAPI/interface/units/RobotUnit/NJointController.ice>


module devices { module ethercat { module hand { module armarde
{

    struct JointValues
    {
        float thumbJointValue;
        float fingersJointValue;
        float indexJointValue;
        float thumbRotationJointValue;
    };


    interface ShapeControllerInterface extends armarx::NJointControllerInterface
    {
        void setTargets(float fingers, float index, float thumb, float thumbRotation);
        void setTargetsWithPwm(float fingers, float index, float thumb, float thumbRotation,
                               float fingersRelativeMaxPwm, float indexRelativeMaxPwm, float thumbRelativeMaxPwm,
                               float thumbRotationRelativeMaxPwm);
        JointValues getJointValues();
    };

};};};};
