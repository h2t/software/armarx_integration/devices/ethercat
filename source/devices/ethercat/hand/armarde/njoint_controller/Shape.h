#pragma once


// armarx
#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>

// robot_devices
#include <devices/ethercat/hand/armarde/Data.h>
#include <devices/ethercat/hand/armarde/joint_controller/PWM.h>
#include <devices/ethercat/hand/armarde/njoint_controller/Shape.h>
#include <devices/ethercat/hand/armarde/njoint_controller/ShapeInterface.h>


namespace devices::ethercat::hand::armarde
{

    using ShapeControllerConfigPtr = IceUtil::Handle<class ShapeControllerConfig>;


    class ShapeControllerConfig :
        virtual public armarx::NJointControllerConfig
    {

    public:

        ShapeControllerConfig(std::string const& deviceName):
            deviceName(deviceName)
        {}

        const std::string deviceName;

    };


    using ShapeControllerPtr = IceInternal::Handle<class ShapeController>;


    class ShapeController :
        public armarx::NJointController,
        public ShapeControllerInterface

    {

    public:

        using ConfigPtrT = ShapeControllerConfigPtr;
        ShapeController(armarx::RobotUnitPtr prov, ShapeControllerConfigPtr config, const VirtualRobot::RobotPtr& r);

        // NJointControllerInterface interface

    public:

        std::string getClassName(const Ice::Current&) const override
        {
            return "NJointArmardeHandShapeController";
        }

        void stopMotion();

        void setConfigs(const DataConfig& fingersConfig, const DataConfig& indexConfig, const DataConfig& thumbConfig, const DataConfig& thumbRotationConfig);

        std::int16_t getFingersPwm();
        std::int16_t getIndexPwm();
        std::int16_t getThumbPwm();
        std::int16_t getThumbRotationPwm();

        float getFingersTarget();
        float getIndexTarget();
        float getThumbTarget();
        float getThumbRotationTarget();

        float getFingersJointValue() const;
        float getIndexJointValue() const;
        float getThumbJointValue() const;
        float getThumbRotationJointValue() const;

        bool isControlEnabled();

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

    private:

        const SensorDataValue* thumbSensorValues;
        const SensorDataValue* fingersSensorValues;
        const SensorDataValue* indexSensorValues;
        const SensorDataValue* thumbRotationSensorValues;
        PWMControlTarget* thumbControlTarget;
        PWMControlTarget* fingersControlTarget;
        PWMControlTarget* indexControlTarget;
        PWMControlTarget* thumbRotationControlTarget;

        std::int16_t fingersPwm = 0;
        std::int16_t indexPwm = 0;
        std::int16_t thumbPwm = 0;
        std::int16_t thumbRotationPwm = 0;

        std::int16_t fingersMaxPwm = 1.0;
        std::int16_t indexMaxPwm = 1.0;
        std::int16_t thumbMaxPwm = 1.0;
        std::int16_t thumbRotationMaxPwm = 1.0;
        std::int16_t fingersPwmLimit = 0;
        std::int16_t indexPwmLimit = 0;
        std::int16_t thumbPwmLimit = 0;
        std::int16_t thumbRotationPwmLimit = 0;

        float fingersTarget = 0;
        float indexTarget = 0;
        float thumbTarget = 0;
        float thumbRotationTarget = 0;

        float actualFingersJointValue = 0;
        float actualIndexJointValue = 0;
        float actualThumbJointValue = 0;
        float actualThumbRotationJointValue = 0;

        float KpFingers = 0;
        float KpIndex = 0;
        float KpThumb = 0;
        float KpThumbRotation = 0;

        bool enableControl = false;

    public:

        armarx::WidgetDescription::StringWidgetDictionary getFunctionDescriptions(const Ice::Current&) const override;
        void callDescribedFunction(const std::string& name, const armarx::StringVariantBaseMap& valueMap, const Ice::Current&) override;

        // NJointArmardeHandShapeControllerInterface interface

    public:

        void setTargets(float fingers, float index, float thumb, float thumbRotation, const Ice::Current& = Ice::emptyCurrent) override;
        void setTargetsWithPwm(float fingers, float index, float thumb, float thumbRotation,
                               float fingersRelativeMaxPwm, float indexRelativeMaxPwm, float thumbRelativeMaxPwm,
                               float thumbRotationRelativeMaxPwm, const Ice::Current& = Ice::emptyCurrent) override;
        JointValues getJointValues(const Ice::Current&) override;

        // NJointControllerBase interface

    protected:

        void rtPreActivateController() override;

    };
}
