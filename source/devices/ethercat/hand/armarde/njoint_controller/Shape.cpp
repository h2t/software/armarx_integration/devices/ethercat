#include <devices/ethercat/hand/armarde/njoint_controller/Shape.h>


// armarx
#include <RobotAPI/libraries/core/math/MathUtils.h>

// robot_devices
#include <devices/ethercat/hand/armarde/Data.h>
#include <devices/ethercat/hand/armarde/joint_controller/PWM.h>


namespace devices::ethercat::hand::armarde
{

    armarx::NJointControllerRegistration<ShapeController> registrationControllerNJointArmardeHandShapeController("NJointArmardeHandShapeController");


    ShapeController::ShapeController(armarx::RobotUnitPtr prov,
                                     ShapeControllerConfigPtr config,
                                     const VirtualRobot::RobotPtr& r)
    {
        ARMARX_CHECK_EXPRESSION(prov);
        auto thumbDeviceName = config->deviceName + "Thumb";
        armarx::ControlTargetBase* ct = useControlTarget(thumbDeviceName, ctrl_modes::ArmardeHandPwm);
        ARMARX_CHECK_EXPRESSION(ct);
        armarx::ConstSensorDevicePtr thumbSensor = peekSensorDevice(thumbDeviceName);
        ARMARX_CHECK_EXPRESSION(thumbSensor) << "device name: " << thumbDeviceName;

        thumbSensorValues = thumbSensor->getSensorValue()->asA<SensorDataValue>();
        ARMARX_CHECK_EXPRESSION(thumbSensorValues);
        ARMARX_CHECK_EXPRESSION(ct->asA<PWMControlTarget>());
        thumbControlTarget = ct->asA<PWMControlTarget>();

        auto fingersDeviceName = config->deviceName + "Fingers";
        ct = useControlTarget(fingersDeviceName, ctrl_modes::ArmardeHandPwm);
        ARMARX_CHECK_EXPRESSION(ct);
        armarx::ConstSensorDevicePtr fingersSensor = peekSensorDevice(fingersDeviceName);
        ARMARX_CHECK_EXPRESSION(fingersSensor) << "device name: " << fingersDeviceName;

        fingersSensorValues = fingersSensor->getSensorValue()->asA<SensorDataValue>();
        ARMARX_CHECK_EXPRESSION(fingersSensorValues);
        ARMARX_CHECK_EXPRESSION(ct->asA<PWMControlTarget>());
        fingersControlTarget = ct->asA<PWMControlTarget>();

        auto indexDeviceName = config->deviceName + "Index";
        ct = useControlTarget(indexDeviceName, ctrl_modes::ArmardeHandPwm);
        ARMARX_CHECK_EXPRESSION(ct);
        armarx::ConstSensorDevicePtr indexSensor = peekSensorDevice(indexDeviceName);
        ARMARX_CHECK_EXPRESSION(indexSensor) << "device name: " << indexDeviceName;

        indexSensorValues = indexSensor->getSensorValue()->asA<SensorDataValue>();
        ARMARX_CHECK_EXPRESSION(indexSensorValues);
        ARMARX_CHECK_EXPRESSION(ct->asA<PWMControlTarget>());
        indexControlTarget = ct->asA<PWMControlTarget>();

        auto thumbRotationDeviceName = config->deviceName + "ThumbRotation";
        ct = useControlTarget(thumbRotationDeviceName, ctrl_modes::ArmardeHandPwm);
        ARMARX_CHECK_EXPRESSION(ct);
        armarx::ConstSensorDevicePtr thumbRotationSensor = peekSensorDevice(thumbRotationDeviceName);
        ARMARX_CHECK_EXPRESSION(thumbRotationSensor) << "device name: " << thumbRotationDeviceName;

        thumbRotationSensorValues = thumbRotationSensor->getSensorValue()->asA<SensorDataValue>();
        ARMARX_CHECK_EXPRESSION(thumbRotationSensorValues);
        ARMARX_CHECK_EXPRESSION(ct->asA<PWMControlTarget>());
        thumbRotationControlTarget = ct->asA<PWMControlTarget>();
    }


    void ShapeController::rtPreActivateController()
    {
        this->fingersTarget = fingersSensorValues->position;
        this->indexTarget = indexSensorValues->position;
        this->thumbTarget = thumbSensorValues->position;
        this->thumbRotationTarget = thumbRotationSensorValues->position;
    }


    void ShapeController::stopMotion()
    {
        enableControl = false;
    }


    void ShapeController::setConfigs(const DataConfig& fingersConfig, const DataConfig& indexConfig, const DataConfig& thumbConfig, const DataConfig& thumbRotationConfig)
    {
        fingersMaxPwm = fingersConfig.maxPwm;
        indexMaxPwm = indexConfig.maxPwm;
        thumbMaxPwm = thumbConfig.maxPwm;
        thumbRotationMaxPwm = thumbRotationConfig.maxPwm;
        KpFingers = fingersConfig.Kp;
        KpIndex = indexConfig.Kp;
        KpThumb = thumbConfig.Kp;
        KpThumbRotation = thumbRotationConfig.Kp;
        fingersPwmLimit = fingersMaxPwm;
        indexPwmLimit = indexMaxPwm;
        thumbPwmLimit = thumbMaxPwm;
        thumbRotationPwmLimit = thumbRotationMaxPwm;
    }


    std::int16_t ShapeController::getFingersPwm()
    {
        return fingersPwm;
    }


    std::int16_t ShapeController::getIndexPwm()
    {
        return indexPwm;
    }


    std::int16_t ShapeController::getThumbPwm()
    {
        return thumbPwm;
    }


    std::int16_t ShapeController::getThumbRotationPwm()
    {
        return thumbRotationPwm;
    }


    float ShapeController::getFingersTarget()
    {
        return fingersTarget;
    }


    float ShapeController::getIndexTarget()
    {
        return indexTarget;
    }


    float ShapeController::getThumbTarget()
    {
        return thumbTarget;
    }


    float ShapeController::getThumbRotationTarget()
    {
        return thumbRotationTarget;
    }


    float ShapeController::getFingersJointValue() const
    {
        return actualFingersJointValue;
    }


    float ShapeController::getIndexJointValue() const
    {
        return actualIndexJointValue;
    }


    float ShapeController::getThumbJointValue() const
    {
        return actualThumbJointValue;
    }


    float ShapeController::getThumbRotationJointValue() const
    {
        return actualThumbRotationJointValue;
    }


    bool ShapeController::isControlEnabled()
    {
        return enableControl;
    }


    void ShapeController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {

        actualFingersJointValue = fingersSensorValues->position;
        actualIndexJointValue = indexSensorValues->position;
        actualThumbJointValue = thumbSensorValues->position;
        actualThumbRotationJointValue = thumbRotationSensorValues->position;

        if (enableControl)
        {
            float fingersError = fingersTarget - actualFingersJointValue;
            float indexError = indexTarget - actualIndexJointValue;
            float thumbError = thumbTarget - actualThumbJointValue;
            float thumbRotationError = thumbRotationTarget - actualThumbRotationJointValue;

            fingersPwm = armarx::math::MathUtils::LimitMinMax(-fingersMaxPwm, fingersMaxPwm, (int)(KpFingers * fingersMaxPwm * fingersError));
            indexPwm = armarx::math::MathUtils::LimitMinMax(-indexMaxPwm, indexMaxPwm, (int)(KpFingers * indexMaxPwm * indexError));
            thumbPwm = armarx::math::MathUtils::LimitMinMax(-thumbMaxPwm, thumbMaxPwm, (int)(KpThumb * thumbMaxPwm * thumbError));
            thumbRotationPwm = armarx::math::MathUtils::LimitMinMax(-thumbRotationMaxPwm, thumbRotationMaxPwm, (int)(KpFingers * thumbRotationMaxPwm * thumbRotationError));
        }
        else
        {
            fingersPwm = 0;
            indexPwm = 0;
            thumbPwm = 0;
            thumbRotationPwm = 0;
        }


        fingersControlTarget->pwm_motor = fingersPwm;
        indexControlTarget->pwm_motor = indexPwm;
        thumbControlTarget->pwm_motor = thumbPwm;
        thumbRotationControlTarget->pwm_motor = thumbRotationPwm;
    }


    armarx::WidgetDescription::StringWidgetDictionary ShapeController::getFunctionDescriptions(const Ice::Current&) const
    {
        using namespace armarx::WidgetDescription;
        HBoxLayoutPtr hbox = new HBoxLayout;
        {
            hbox->children.emplace_back(new Label(false, "Fingers"));
            FloatSliderPtr slider = new FloatSlider;
            slider->name = "Fingers";
            slider->min = 0;
            slider->defaultValue = getFingersJointValue();
            slider->max = 1;
            hbox->children.emplace_back(slider);
        }
        {
            hbox->children.emplace_back(new Label(false, "Index"));
            FloatSliderPtr slider = new FloatSlider;
            slider->name = "Index";
            slider->min = 0;
            slider->defaultValue = getIndexJointValue();
            slider->max = 1;
            hbox->children.emplace_back(slider);
        }
        {
            hbox->children.emplace_back(new Label(false, "Thumb"));
            FloatSliderPtr slider = new FloatSlider;
            slider->name = "Thumb";
            slider->min = 0;
            slider->defaultValue = getThumbJointValue();
            slider->max = 1;
            hbox->children.emplace_back(slider);
        }
        {
            hbox->children.emplace_back(new Label(false, "ThumbRotation"));
            FloatSliderPtr slider = new FloatSlider;
            slider->name = "ThumbRotation";
            slider->min = 0;
            slider->defaultValue = getThumbRotationJointValue();
            slider->max = 1;
            hbox->children.emplace_back(slider);
        }
        {
            hbox->children.emplace_back(new Label(false, "FingersMaxPwm"));
            FloatSliderPtr slider = new FloatSlider;
            slider->name = "FingersMaxPwm";
            slider->min = 0;
            slider->defaultValue = 1;
            slider->max = 1;
            hbox->children.emplace_back(slider);
        }
        {
            hbox->children.emplace_back(new Label(false, "IndexMaxPwm"));
            FloatSliderPtr slider = new FloatSlider;
            slider->name = "IndexMaxPwm";
            slider->min = 0;
            slider->defaultValue = 1;
            slider->max = 1;
            hbox->children.emplace_back(slider);
        }
        {
            hbox->children.emplace_back(new Label(false, "ThumbMaxPwm"));
            FloatSliderPtr slider = new FloatSlider;
            slider->name = "ThumbMaxPwm";
            slider->min = 0;
            slider->defaultValue = 1;
            slider->max = 1;
            hbox->children.emplace_back(slider);
        }
        {
            hbox->children.emplace_back(new Label(false, "ThumbRotationMaxPwm"));
            FloatSliderPtr slider = new FloatSlider;
            slider->name = "ThumbRotationMaxPwm";
            slider->min = 0;
            slider->defaultValue = 1;
            slider->max = 1;
            hbox->children.emplace_back(slider);
        }

        return {{"Targets", hbox}};
    }


    void ShapeController::callDescribedFunction(const std::string& name, const armarx::StringVariantBaseMap& valueMap, const Ice::Current&)
    {
        if (name == "Targets")
        {
            float fingersValue = valueMap.at("Fingers")->getFloat();
            float indexValue = valueMap.at("Index")->getFloat();
            float thumbValue = valueMap.at("Thumb")->getFloat();
            float thumbRotationValue = valueMap.at("ThumbRotation")->getFloat();
            float fingersMaxPwm = valueMap.at("FingersMaxPwm")->getFloat();
            float indexMaxPwm = valueMap.at("IndexMaxPwm")->getFloat();
            float thumbMaxPwm = valueMap.at("ThumbMaxPwm")->getFloat();
            float thumbRotationMaxPwm = valueMap.at("ThumbRotationMaxPwm")->getFloat();
            setTargetsWithPwm(fingersValue, indexValue, thumbValue, thumbRotationValue,
                              fingersMaxPwm, indexMaxPwm, thumbMaxPwm, thumbRotationMaxPwm);
        }
        else
        {
            ARMARX_WARNING << "Unknown function name called: " << name;
        }
    }


    void ShapeController::setTargets(float fingers, float index, float thumb, float thumbRotation, const Ice::Current&)
    {
        setTargetsWithPwm(fingers, index, thumb, thumbRotation, 1, 1, 1, 1);
    }


    void ShapeController::setTargetsWithPwm(float fingers, float index, float thumb, float thumbRotation,
                                            float fingersRelativeMaxPwm, float indexRelativeMaxPwm, float thumbRelativeMaxPwm,
                                            float thumbRotationRelativeMaxPwm, const Ice::Current&)
    {
        this->fingersTarget = armarx::math::MathUtils::LimitMinMax(0.f, 2.f, fingers);
        this->indexTarget = armarx::math::MathUtils::LimitMinMax(0.f, 2.f, index);
        this->thumbTarget = armarx::math::MathUtils::LimitMinMax(0.f, 2.f, thumb);
        this->thumbRotationTarget = armarx::math::MathUtils::LimitMinMax(0.f, 2.f, thumbRotation);
        this->fingersMaxPwm = armarx::math::MathUtils::LimitMinMax(0, fingersPwmLimit, (int)(fingersPwmLimit * fingersRelativeMaxPwm));
        this->indexMaxPwm = armarx::math::MathUtils::LimitMinMax(0, indexPwmLimit, (int)(indexPwmLimit * indexRelativeMaxPwm));
        this->thumbMaxPwm = armarx::math::MathUtils::LimitMinMax(0, thumbPwmLimit, (int)(thumbPwmLimit * thumbRelativeMaxPwm));
        this->thumbRotationMaxPwm = armarx::math::MathUtils::LimitMinMax(0, thumbRotationPwmLimit, (int)(thumbRotationPwmLimit * thumbRotationRelativeMaxPwm));
        enableControl = true;
    }


    JointValues ShapeController::getJointValues(const Ice::Current&)
    {
        JointValues values;
        values.fingersJointValue = actualFingersJointValue;
        values.indexJointValue = actualIndexJointValue;
        values.thumbJointValue = actualThumbJointValue;
        values.thumbRotationJointValue = actualThumbRotationJointValue;
        return values;
    }

} // namespace armarx
