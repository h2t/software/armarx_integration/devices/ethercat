/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <memory>

// armarx
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <armarx/control/ethercat/DataInterface.h>
#include <armarx/control/rt_filters/AverageFilter.h>
#include <armarx/control/rt_filters/FirFilter.h>

#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <RobotAPI/components/units/RobotUnit/Devices/ControlDevice.h>
#include <RobotAPI/components/units/RobotUnit/Devices/SensorDevice.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <RobotAPI/interface/units/KinematicUnitInterfaceStdOverloads.h>
#include <RobotAPI/libraries/core/Pose.h>

// robot_devices
#include <devices/ethercat/hand/armarde/SlaveIO.h>
#include <devices/ethercat/hand/armarde/Config.h>
#include <devices/ethercat/hand/armarde/ErrorDecoder.h>


namespace devices::ethercat::hand::armarde
{

    class SensorDataValue :
        virtual public armarx::SensorValue1DoFRealActuator,
        virtual public armarx::SensorValue1DoFMotorPWM
    {

    public:

        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION

        std::uint16_t mainUpdateRate;
        bool emergencyStopStatus;
        bool motorError;
        /* motorTemperature */

        float absoluteEncoderZAngle;
        std::uint8_t absoluteEncoderStatusBits;

        /* position */

        std::array<std::uint16_t, 16> tofDepth;

        /* motorPWM */

        static SensorValueInfo<SensorDataValue> GetClassMemberInfo()
        {
            SensorValueInfo<SensorDataValue> svi;
            svi.addBaseClass<SensorValue1DoFMotorPWM>();
            svi.addBaseClass<SensorValue1DoFRealActuator>();
            svi.addMemberVariable(&SensorDataValue::mainUpdateRate, "mainUpdateRate");
            svi.addMemberVariable(&SensorDataValue::emergencyStopStatus, "emergencyStopStatus");
            svi.addMemberVariable(&SensorDataValue::motorError, "motorError");
            svi.addMemberVariable(&SensorDataValue::absoluteEncoderZAngle, "absoluteEncoderZAngle");
            svi.addMemberVariable(&SensorDataValue::absoluteEncoderStatusBits, "absoluteEncoderStatusBits");
            svi.addMemberVariable(&SensorDataValue::tofDepth, "tofDepth");

            return svi;
        }

    };


    enum MotorFingerType : std::uint8_t
    {
        Fingers = 0,
        Index = 1,
        Thumb = 2,
        ThumbRotation = 3
    };


    class Data :
        public armarx::control::ethercat::DataInterface
    {

    public:

        Data(const DataConfig& config,
             SlaveOut* sensorOUT, SlaveIn* sensorIN, MotorFingerType motorFingerType);

        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

        void updateSensorValueStruct(SensorDataValue& data);

        float getKp() const;

        unsigned int getMaxPWM() const;

        std::uint16_t getMainUpdateRate() const;

        bool getEmergencyStopStatus() const;

        bool getMotorError() const;

        float getMotorTemperature() const;

        float getAbsoluteEncoderZValue() const;

        std::uint8_t getAbsoluteEncoderSatusBits() const;

        float getMotorPosition() const;

        std::array<std::uint16_t, 16> getTofDepth() const;

        void setMotorPwmValue(std::int16_t pwm);

    private:

        MotorFingerType motorFingerType;
        unsigned int maxPWM;
        float kp;

        SlaveOut* sensorOUT;
        SlaveIn* sensorIN;

        ErrorDecoder errorDecoder;

        std::uint16_t mainUpdateRate;
        bool emergencyStopStatus;
        bool motorError;
        armarx::control::ethercat::LinearConvertedValue<std::int8_t> motorTemperature;

        armarx::control::ethercat::LinearConvertedValue<std::int16_t> absoluteEncoderZValue;
        std::uint8_t absoluteEncoderStatusBits;

        armarx::control::ethercat::LinearConvertedValue<std::int32_t> motorPosition;

        std::array<std::uint16_t, 16> tofDepth;

        // target values in etherCAT buffer
        std::int16_t* motorPwmPtr = NULL;

        // target values set from outside
        std::int16_t motorPwmValue;

    };

}
