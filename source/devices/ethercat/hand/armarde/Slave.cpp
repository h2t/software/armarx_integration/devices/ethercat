#include "Slave.h"

#include <iostream>

#include <ArmarXCore/core/logging/Logging.h>
#include <armarx/control/ethercat/Bus.h>

#include <devices/ethercat/common/h2t_devices.h>


namespace devices::ethercat::hand::armarde
{

    Slave::Slave(const armarx::control::ethercat::SlaveIdentifier& slaveIdentifier) :
        SlaveInterfaceWithIO(slaveIdentifier)
    {
        inputs = nullptr;
        outputs = nullptr;

        setTag(this->slaveIdentifier.getName());
    }


    void
    Slave::execute()
    {
    }


    bool
    Slave::prepareForRun()
    {
        ARMARX_INFO << deactivateSpam(5) << "ArmardeHandSlave " << getSlaveIdentifier()
                    << " is ready";
        return true;
    }


    bool
    Slave::shutdown()
    {
        return true;
    }


    void
    Slave::doMappings()
    {
    }


    bool
    Slave::hasError()
    {
        return errorDecoder.absoluteEncoderErrorDetected(this, true) || errorDecoder.motorErrorDetected(this, true);
    }


    void
    Slave::prepareForSafeOp()
    {
    }


    void
    Slave::finishPreparingForSafeOp()
    {
    }


    void
    Slave::prepareForOp()
    {
        errorDecoder = ErrorDecoder(getOutputsPtr(), getInputsPtr());
    }


    void
    Slave::finishPreparingForOp()
    {
    }


    bool
    Slave::isEmergencyStopActive() const
    {
        return errorDecoder.getEmergencyStopStatus();
    }


    bool
    Slave::recoverFromEmergencyStop()
    {
        return true;
    }


    std::string
    Slave::getDefaultName()
    {
        return "ArmardeHand";
    }


    bool
    Slave::isSlaveIdentifierAccepted(const armarx::control::ethercat::SlaveIdentifier& sid)
    {
        const std::vector<std::uint32_t> validProductCodes{ArmardeHandLeft, ArmardeHandRight};

        const bool productCodeMatches =
            std::find(validProductCodes.begin(), validProductCodes.end(), sid.productCode) !=
            validProductCodes.end();

        return sid.vendorID == devices::ethercat::common::H2TVendorId and productCodeMatches;
    }

} // namespace devices::ethercat::hand::armarde
