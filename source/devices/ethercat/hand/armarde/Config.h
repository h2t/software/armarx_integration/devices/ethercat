#pragma once

#include <armarx/control/hardware_config/DeviceConfig.h>

namespace devices::ethercat::hand::armarde
{
    namespace hwconfig = armarx::control::hardware_config;
    struct DataConfig
    {
        DataConfig(hwconfig::DeviceConfig& deviceConfig, hwconfig::DeviceConfigBase& subDeviceConfig)
            : motorTemperature(deviceConfig.getLinearConfig("motorTemperature")),
              absoluteEncoderZValue(deviceConfig.getLinearConfig("absoluteEncoderZValue")),
              motorPosition(subDeviceConfig.getLinearConfig("motorPosition")),
              maxPwm(subDeviceConfig.getUint("maxPwm")),
              Kp(subDeviceConfig.getFloat("Kp"))
        {

        }
        hwconfig::types::LinearConfig motorTemperature;
        hwconfig::types::LinearConfig absoluteEncoderZValue;
        hwconfig::types::LinearConfig motorPosition;
        std::uint32_t maxPwm;
        float Kp;
    };
}
