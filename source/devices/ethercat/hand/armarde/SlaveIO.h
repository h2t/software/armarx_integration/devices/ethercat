#pragma once


// SDT/STL
#include <cstdint>


namespace devices::ethercat::hand::armarde
{

    /**
     * @brief PDO mapping sensorB->master
     */
    struct SlaveOut
    {

        // Board Diagnostic
        std::uint16_t mainUpdateRate;
        std::uint8_t statusBits;
        std::int8_t	temperatureFingers;
        std::int8_t	temperatureIndex;
        std::int8_t	temperatureThumb;
        std::uint16_t pad1;

        // Absolute Encoder
        std::int16_t absoluteEncoderZAngle;
        std::uint8_t absoluteEncoderStatusBits;
        std::uint8_t pad2;

        // Motor Sensing
        std::int32_t motorFingersPosition;
        std::int32_t motorIndexPosition;
        std::int32_t motorThumbPosition;
        std::int32_t motorThumbRotationPosition;

        // ToF Depth
        std::uint16_t tofDepth[16];

    } __attribute__((__packed__));


    /**
     * @brief PDO mapping master->sensorB
     */
    struct SlaveIn
    {
        // Motor PWM
        std::int16_t motorFingersPwmValue;
        std::int16_t motorIndexPwmValue;
        std::int16_t motorThumbPwmValue;
        std::int16_t motorThumbRotationPwmValue;

    } __attribute__((__packed__));

}
