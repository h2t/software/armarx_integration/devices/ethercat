/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, butgetActualTorque
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <devices/ethercat/hand/armarde/Data.h>


// armarx
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace devices::ethercat::hand::armarde
{

    Data::Data(const DataConfig& config,
               SlaveOut* sensorOUT, SlaveIn* sensorIN,
               MotorFingerType motorFingerType) :
        motorFingerType(motorFingerType),
        sensorOUT(sensorOUT),
        sensorIN(sensorIN),
        errorDecoder(sensorOUT, sensorIN)
    {
        maxPWM = config.maxPwm;
        kp = config.Kp;
        ARMARX_CHECK_EXPRESSION(sensorOUT);

        switch (motorFingerType)
        {
            case MotorFingerType::Fingers:
                motorTemperature.init(&sensorOUT->temperatureFingers, config.motorTemperature);
                motorPosition.init(&sensorOUT->motorFingersPosition, config.motorPosition);
                motorPwmPtr = &sensorIN->motorFingersPwmValue;
            break;
            case MotorFingerType::Index:
                motorTemperature.init(&sensorOUT->temperatureIndex, config.motorTemperature);
                motorPosition.init(&sensorOUT->motorIndexPosition, config.motorPosition);
                motorPwmPtr = &sensorIN->motorIndexPwmValue;
                break;
            case MotorFingerType::Thumb:
                motorTemperature.init(&sensorOUT->temperatureThumb, config.motorTemperature);
                motorPosition.init(&sensorOUT->motorThumbPosition, config.motorPosition);
                motorPwmPtr = &sensorIN->motorThumbPwmValue;
                break;
            default:
                motorPosition.init(&sensorOUT->motorThumbRotationPosition, config.motorPosition);
                motorPwmPtr = &sensorIN->motorThumbRotationPwmValue;
                break;
        }

        absoluteEncoderZValue.init(&sensorOUT->absoluteEncoderZAngle, config.absoluteEncoderZValue);
    }


    void Data::rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        mainUpdateRate = sensorOUT->mainUpdateRate;
        emergencyStopStatus = errorDecoder.getEmergencyStopStatus();
        motorError = sensorOUT->statusBits &
                (MotorStatus::ErrorFingers |
                 MotorStatus::ErrorIndex |
                 MotorStatus::ErrorThumb |
                 MotorStatus::ErrorThumbRotation) ? true : false;
        absoluteEncoderStatusBits = sensorOUT->absoluteEncoderStatusBits;

//        ARMARX_INFO << "Data::rtReadSensorValues";
        memcpy(tofDepth.data(), sensorOUT->tofDepth,  16 * sizeof(std::uint16_t));

        if (motorFingerType != MotorFingerType::ThumbRotation)
        {
            motorTemperature.read();
        }
        absoluteEncoderZValue.read();
        motorPosition.read();
    }


    void Data::rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        *motorPwmPtr = motorPwmValue;
    }


    void Data::updateSensorValueStruct(SensorDataValue& data)
    {
        data.mainUpdateRate = mainUpdateRate;
        data.emergencyStopStatus = emergencyStopStatus;
        data.motorError = motorError;
        data.motorTemperature = motorFingerType != MotorFingerType::ThumbRotation ? motorTemperature.value : 0;
        data.absoluteEncoderZAngle = absoluteEncoderZValue.value;
        data.absoluteEncoderStatusBits = absoluteEncoderStatusBits;
        data.position = motorFingerType != MotorFingerType::ThumbRotation ? motorPosition.value : absoluteEncoderZValue.value;
        data.tofDepth = tofDepth;
        data.motorPWM = motorPwmValue;
    }


    float Data::getKp() const
    {
        return kp;
    }


    unsigned int Data::getMaxPWM() const
    {
        return maxPWM;
    }


    std::uint16_t Data::getMainUpdateRate() const
    {
        return mainUpdateRate;
    }

    bool Data::getEmergencyStopStatus() const
    {
        return emergencyStopStatus;
    }


    bool Data::getMotorError() const
    {
        return motorError;
    }


    float Data::getMotorTemperature() const
    {
        if (motorFingerType == MotorFingerType::ThumbRotation)
        {
            return 0;
        }
        return motorTemperature.value;
    }


    float Data::getAbsoluteEncoderZValue() const
    {
        return absoluteEncoderZValue.value;
    }


    std::uint8_t Data::getAbsoluteEncoderSatusBits() const
    {
        return absoluteEncoderStatusBits;
    }


    float Data::getMotorPosition() const
    {
        if (motorFingerType == MotorFingerType::ThumbRotation)
        {
            return absoluteEncoderZValue.value;
        }
        return motorPosition.value;
    }


    std::array<std::uint16_t, 16> Data::getTofDepth() const
    {
        return tofDepth;
    }


    void Data::setMotorPwmValue(std::int16_t pwm)
    {
        motorPwmValue = pwm;
    }

}
