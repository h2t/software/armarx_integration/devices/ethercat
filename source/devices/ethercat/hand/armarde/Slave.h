#pragma once


#include <atomic>
#include <cstdint>
#include <memory>
#include <thread>

#include <armarx/control/ethercat/SlaveInterface.h>

#include <devices/ethercat/hand/armarde/SlaveIO.h>
#include <devices/ethercat/hand/armarde/ErrorDecoder.h>


namespace devices::ethercat::hand::armarde
{

    static constexpr std::uint32_t ArmardeHandLeft = 0x2200;
    static constexpr std::uint32_t ArmardeHandRight = 0x2201;

    class Slave : public armarx::control::ethercat::SlaveInterfaceWithIO<SlaveIn, SlaveOut>
    {

    public:
        Slave(const armarx::control::ethercat::SlaveIdentifier& slaveIdentifier);

        /**
         * @see AbstractSlave::doMappings
         */
        void doMappings() override;

        void prepareForOp() override;

        void finishPreparingForOp() override;

        /**
         * @see AbstractSlave::prepare()
         */
        bool prepareForRun() override;

        /**
         * @see AbstractSlave::execute()
         */
        void execute() override;

        /**
         * @see AbstractSlave::shutdown()
         */
        bool shutdown() override;

        /**
         * @see AbstractSlave::hasError()
         * @return
         */
        bool hasError() override;

        void prepareForSafeOp() override;

        void finishPreparingForSafeOp() override;

        bool isEmergencyStopActive() const override;

        bool recoverFromEmergencyStop() override;

        static std::string getDefaultName();

        static bool
        isSlaveIdentifierAccepted(const armarx::control::ethercat::SlaveIdentifier& sid);

    private:
        ErrorDecoder errorDecoder{nullptr, nullptr};
    };

} // namespace devices::ethercat::hand::armarde
