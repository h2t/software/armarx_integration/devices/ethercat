#include <boost/algorithm/clamp.hpp>

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include "Hand.h"

namespace devices::ethercat::hand::soft_sensorized_finger::bus_data
{
    HandSensorData::HandSensorData(
        const armarx::RapidXmlReaderNode& node,
        armarx::DefaultRapidXmlReaderNode defaultConfigurationNode,
        BoardOut* sensorAll,
        const std::string& configName)
    {
        ARMARX_TRACE;
        ARMARX_CHECK_NOT_NULL(sensorAll);
        auto cfg =
            defaultConfigurationNode
            .add_node_at_end(node)
            .first_node("Hand");
        ARMARX_DEBUG << "configName " << configName << "\nall config paths:\n" << cfg.getChildPaths();

        sensor_frame_counter = &sensorAll->sensor_frame_counter;
        update_rate_main     = &sensorAll->update_rate_main;
        update_rate_fpga     = &sensorAll->update_rate_fpga;
        raw_temperature      = &sensorAll->temperature;
        //hand_shutdown_threshold
        {
            const auto node = cfg.first_node("hand_shutdown_threshold");
            node.attribute_as("shutdown_temp", motor_temp_threshold_shutdown);
            node.attribute_as("restart_temp",  motor_temp_threshold_restart);
        }
        //sensor_hand_temp
        {
            const auto node = cfg.first_node("sensor_hand_temp");
            node.attribute_as("a", temp_correction_a);
            node.attribute_as("b", temp_correction_b);
            node.attribute_as("c", temp_correction_c);

            temperature = armarx::control::rt_filters::RtMedianFilter{node.attribute_as<std::size_t>("median_filter_width")};
        }
    }

    void HandSensorData::rtReadSensorValues(
        const IceUtil::Time& /*sensorValuesTimestamp*/,
        const IceUtil::Time& /*timeSinceLastIteration*/)
    {}

    void HandSensorData::rtWriteTargetValues(
        const IceUtil::Time& /*sensorValuesTimestamp*/,
        const IceUtil::Time& /*timeSinceLastIteration*/)
    {}

    void HandSensorData::updateSensorValueStruct(
        HandSensorValue& data)
    {
        data.frame_counter    = *sensor_frame_counter;
        data.update_rate_main = *update_rate_main;
        data.update_rate_fpga = *update_rate_fpga;
        data.raw_temperature  = *raw_temperature;
        // a*exp(b*x)+c
        data.temperature = temperature.update(
                               temp_correction_a *
                               std::exp(data.raw_temperature * temp_correction_b) +
                               temp_correction_c);
    }
}
