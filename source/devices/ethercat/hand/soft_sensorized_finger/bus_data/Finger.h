#pragma once


#include <memory>

#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>

#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <RobotAPI/components/units/RobotUnit/Devices/ControlDevice.h>
#include <RobotAPI/components/units/RobotUnit/Devices/SensorDevice.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <RobotAPI/interface/units/KinematicUnitInterfaceStdOverloads.h>
#include <RobotAPI/libraries/core/Pose.h>
#include <armarx/control/ethercat/AbstractData.h>

#include <armarx/control/rt_filters/MedianFilteredLinearConvertedValue.h>

#include "../Slave.h"
#include "../sensor_value/Finger.h"

namespace devices::ethercat::hand::soft_sensorized_finger::bus_data
{
    class FingerSensorData;
    using FingerSensorDataPtr =
        std::shared_ptr<FingerSensorData>;

    class FingerSensorData : public armarx::control::ethercat::AbstractData
    {
    public:
        FingerSensorData(
            const armarx::RapidXmlReaderNode& node,
            armarx::DefaultRapidXmlReaderNode defaultConfigurationNode,
            BoardOut* sensorAll,
            BoardOut_finger_value* sensorFinger,
            const std::string& configName);

        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

        void updateSensorValueStruct(FingerSensorValue& data);
    private:
        template<class T>
        using Conv = armarx::control::ethercat::LinearConvertedValue<T>;
        template<class T>
        using ConvFiltered = armarx::control::rt_filters::MedianFilteredLinearConvertedValue<T>;

        BoardOut_finger_value*                            sensor_finger;
        std::uint32_t*                                    sensor_frame_counter;
        ConvFiltered<std::uint16_t>                       joint_encoder_prox_temp;
        ConvFiltered<std::uint16_t>                       joint_encoder_dist_temp;
        ConvFiltered<std::int16_t>                        pressure_prox_l_temp;
        ConvFiltered<std::int16_t>                        pressure_prox_r_temp;
        ConvFiltered<std::int16_t>                        pressure_dist_joint_temp;
        ConvFiltered<std::int16_t>                        pressure_dist_tip_temp;
        ConvFiltered<std::uint16_t>                       shear_force_dist_joint_temp;
        ConvFiltered<std::uint16_t>                       shear_force_dist_tip_temp;

        std::array<ConvFiltered<std::int16_t>, 3>         joint_encoder_prox;
        std::array<ConvFiltered<std::int16_t>, 3>         joint_encoder_dist;

        ConvFiltered<u_int32_t>                           pressure_prox_l;
        ConvFiltered<u_int32_t>                           pressure_prox_r;
        ConvFiltered<u_int32_t>                           pressure_dist_joint;
        ConvFiltered<u_int32_t>                           pressure_dist_tip;

        std::array<ConvFiltered<std::int16_t>, 3>         shear_force_dist_joint;
        std::array<ConvFiltered<std::int16_t>, 3>         shear_force_dist_tip;

        ConvFiltered<std::uint16_t>                       proximity;

        std::array<std::array<Conv<std::int16_t>, 3>, 17> accelerometer_fifo;
    };
}
