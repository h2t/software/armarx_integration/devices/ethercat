#include "Motor.h"


#include <boost/algorithm/clamp.hpp>

// armarx
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>


namespace devices::ethercat::hand::soft_sensorized_finger::bus_data
{
    MotorControlData::MotorControlData(
        const armarx::RapidXmlReaderNode& node,
        armarx::DefaultRapidXmlReaderNode defaultConfigurationNode,
        BoardIn_motor_target* target,
        BoardOut* sensorAll,
        BoardOut_motor_value* sensorMotor,
        const std::string& configName
    ) :
        target{target},
        sensor_motor{sensorMotor}
    {
        ARMARX_TRACE;

        ARMARX_CHECK_NOT_NULL(target);
        ARMARX_CHECK_NOT_NULL(sensorAll);
        ARMARX_CHECK_NOT_NULL(sensor_motor);
        {
            ARMARX_TRACE;

            auto configNodeAll =
                defaultConfigurationNode
                .add_node_at_end(node)
                .first_node("Motor");

            auto cfg = configNodeAll
                       .first_node("default")
                       .add_node_at_end(configNodeAll.first_node(configName));

            //low level pwm
            {
                auto cfg_pwm = cfg.first_node("ctrl_low_level_pwm");
                cfg_pwm.attribute_as("max",    pwm_max);
                cfg_pwm.attribute_as("factor", pwm_factor);
                ARMARX_CHECK_GREATER_EQUAL(pwm_max, 0);
            }
            //ctrl_position
            {
                auto cfg_ctrl = cfg.first_node("ctrl_position");
                cfg_ctrl.attribute_as("Kp", pos_ctrl_Kp);
                cfg_ctrl.attribute_as("Ki", pos_ctrl_Ki);
                cfg_ctrl.attribute_as("Kd", pos_ctrl_Kd);
                cfg_ctrl.attribute_as("default_max_pwm", pos_ctrl_default_max_pwm);
                ARMARX_CHECK_GREATER_EQUAL(pos_ctrl_Kp, 0);
                ARMARX_CHECK_GREATER_EQUAL(pos_ctrl_Ki, 0);
                ARMARX_CHECK_GREATER_EQUAL(pos_ctrl_Kd, 0);
                ARMARX_CHECK_GREATER_EQUAL(pos_ctrl_default_max_pwm, 0);
            }
            //ctrl_velocity
            {
                auto cfg_ctrl = cfg.first_node("ctrl_velocity");
                cfg_ctrl.attribute_as("Kp", vel_ctrl_Kp);
                cfg_ctrl.attribute_as("Ki", vel_ctrl_Ki);
                cfg_ctrl.attribute_as("Kd", vel_ctrl_Kd);
                cfg_ctrl.attribute_as("default_max_pwm", vel_ctrl_default_max_pwm);
                ARMARX_CHECK_GREATER_EQUAL(vel_ctrl_Kp, 0);
                ARMARX_CHECK_GREATER_EQUAL(vel_ctrl_Ki, 0);
                ARMARX_CHECK_GREATER_EQUAL(vel_ctrl_Kd, 0);
                ARMARX_CHECK_GREATER_EQUAL(vel_ctrl_default_max_pwm, 0);
            }

            sensor_frame_counter = &sensorAll->sensor_frame_counter;
            velocity         .init(&sensorMotor->velocity,          cfg.first_node("sensor_motor_velocity"));
            relative_position.init(&sensorMotor->relative_position, cfg.first_node("sensor_motor_relative_position"));
        }
    }

    void MotorControlData::rtReadSensorValues(
        const IceUtil::Time& /*sensorValuesTimestamp*/,
        const IceUtil::Time& /*timeSinceLastIteration*/)
    {
        velocity         .read();
        relative_position.read();
    }

    void MotorControlData::rtWriteTargetValues(
        const IceUtil::Time& /*sensorValuesTimestamp*/,
        const IceUtil::Time& /*timeSinceLastIteration*/)
    {
        target->pwm = target_pwm_motor;
    }

    void MotorControlData::setCommand(int16 pwm)
    {
        const auto pwm_clamped = boost::algorithm::clamp<std::int64_t>(
                                     pwm, -pwm_max, pwm_max);
        target_pwm_motor = pwm_clamped;
    }

    std::int16_t MotorControlData::getMaxPWM() const
    {
        return pwm_max;
    }

    void MotorControlData::updateSensorValueStruct(
        MotorSensorValue& data)
    {
        data.frame_counter     = *sensor_frame_counter;
        data.velocity          = velocity.value;
        data.position          = relative_position.value;
        data.raw_velocity      = sensor_motor->velocity;
        data.raw_position      = sensor_motor->relative_position;
        data.motorPWM          = target_pwm_motor;
        data.pwm_max           = pwm_max;
    }
}
