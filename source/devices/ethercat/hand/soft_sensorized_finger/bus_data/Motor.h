#pragma once


#include <cstdint>
#include <memory>
#include <osal.h>

#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>

#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <RobotAPI/components/units/RobotUnit/Devices/ControlDevice.h>
#include <RobotAPI/components/units/RobotUnit/Devices/SensorDevice.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <RobotAPI/interface/units/KinematicUnitInterfaceStdOverloads.h>
#include <RobotAPI/libraries/core/Pose.h>
#include <armarx/control/ethercat/AbstractData.h>

#include <armarx/control/rt_filters/MedianFilteredLinearConvertedValue.h>

#include "../Slave.h"
#include "../sensor_value/Motor.h"
#include "robot_devices/ethercat/hand/soft_sensorized_finger/sensor_value/Motor.h"

namespace devices::ethercat::hand::soft_sensorized_finger::bus_data
{
    class MotorControlData : public armarx::control::ethercat::AbstractData
    {

    public:
        MotorControlData(
            const armarx::RapidXmlReaderNode& node,
            armarx::DefaultRapidXmlReaderNode defaultConfigurationNode,
            BoardIn_motor_target* target,
            BoardOut* sensorAll,
            BoardOut_motor_value* sensorMotor,
            const std::string& configName
        );

        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

        void setCommand(std::int16_t pwm);
        int16_t getMaxPWM() const;

        float getPosCtrlKp() const
        {
            return pos_ctrl_Kp;
        }
        float getPosCtrlKi() const
        {
            return pos_ctrl_Ki;
        }
        float getPosCtrlKd() const
        {
            return pos_ctrl_Kd;
        }
        std::int16_t getPosCtrlDefaultMaxPWM() const
        {
            return pos_ctrl_default_max_pwm;
        }

        float getVelCtrlKp() const
        {
            return vel_ctrl_Kp;
        }
        float getVelCtrlKi() const
        {
            return vel_ctrl_Ki;
        }
        float getVelCtrlKd() const
        {
            return vel_ctrl_Kd;
        }
        std::int16_t getVelCtrlDefaultMaxPWM() const
        {
            return vel_ctrl_default_max_pwm;
        }

        float getRelativeEncoderValue() const
        {
            return relative_position.value;
        }

        void updateSensorValueStruct(MotorSensorValue& data);
    private:
        BoardIn_motor_target*       target;
        const BoardOut_motor_value* sensor_motor;
        std::uint32_t*                                               sensor_frame_counter;
        armarx::control::rt_filters::MedianFilteredLinearConvertedValue<int32_t>                  velocity;
        armarx::control::rt_filters::MedianFilteredLinearConvertedValue<int32_t>                  relative_position;

        float        pos_ctrl_Kp = 10;
        float        pos_ctrl_Ki =  1;
        float        pos_ctrl_Kd =  0;
        std::int16_t pos_ctrl_default_max_pwm;

        float        vel_ctrl_Kp = 10;
        float        vel_ctrl_Ki =  1;
        float        vel_ctrl_Kd =  0;
        std::int16_t vel_ctrl_default_max_pwm;

        std::int16_t pwm_max;
        int32_t      pwm_factor;
        // target values set from outside
        int16_t target_pwm_motor;
    };
    using MotorControlDataPtr =
        std::shared_ptr<MotorControlData>;
}
