#include <boost/algorithm/clamp.hpp>

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include "Finger.h"
#include "robot_devices/ethercat/hand/soft_sensorized_finger/sensor_value/Finger.h"

namespace devices::ethercat::hand::soft_sensorized_finger::bus_data
{
    FingerSensorData::FingerSensorData(
        const armarx::RapidXmlReaderNode& node,
        armarx::DefaultRapidXmlReaderNode defaultConfigurationNode,
        BoardOut* sensorAll,
        BoardOut_finger_value* sensorFinger,
        const std::string& configName
    ):
        sensor_finger{sensorFinger}
    {
        ARMARX_TRACE;
        ARMARX_CHECK_NOT_NULL(sensorAll);
        ARMARX_CHECK_NOT_NULL(sensorFinger);
        auto configNodeAll =
            defaultConfigurationNode
            .add_node_at_end(node)
            .first_node("Finger");
        auto cfg = configNodeAll
                   .first_node("default")
                   .add_node_at_end(configNodeAll.first_node(configName));
        ARMARX_DEBUG << "configName " << configName << "\nall config paths:\n" << cfg.getChildPaths();

        sensor_frame_counter              = &sensorAll->sensor_frame_counter;
        joint_encoder_prox_temp           .init(&sensorFinger->joint_encoder_prox    .temperature, cfg.first_node("sensor_joint_encoder_prox_temp"));
        joint_encoder_dist_temp           .init(&sensorFinger->joint_encoder_dist    .temperature, cfg.first_node("sensor_joint_encoder_dist_temp"));
        pressure_prox_l_temp              .init(&sensorFinger->pressure_prox_l       .temperature, cfg.first_node("sensor_pressure_prox_l_temp"));
        pressure_prox_r_temp              .init(&sensorFinger->pressure_prox_r       .temperature, cfg.first_node("sensor_pressure_prox_r_temp"));
        pressure_dist_joint_temp          .init(&sensorFinger->pressure_dist_joint   .temperature, cfg.first_node("sensor_pressure_dist_joint_temp"));
        pressure_dist_tip_temp            .init(&sensorFinger->pressure_dist_tip     .temperature, cfg.first_node("sensor_pressure_dist_tip_temp"));
        shear_force_dist_joint_temp       .init(&sensorFinger->shear_force_dist_joint.temperature, cfg.first_node("sensor_shear_force_dist_joint_temp"));
        shear_force_dist_tip_temp         .init(&sensorFinger->shear_force_dist_tip  .temperature, cfg.first_node("sensor_shear_force_dist_tip_temp"));

        joint_encoder_prox.at(0)          .init(&sensorFinger->joint_encoder_prox.xyz.at(0),       cfg.first_node("sensor_joint_encoder_prox_x"));
        joint_encoder_prox.at(1)          .init(&sensorFinger->joint_encoder_prox.xyz.at(1),       cfg.first_node("sensor_joint_encoder_prox_y"));
        joint_encoder_prox.at(2)          .init(&sensorFinger->joint_encoder_prox.xyz.at(2),       cfg.first_node("sensor_joint_encoder_prox_z"));

        joint_encoder_dist.at(0)          .init(&sensorFinger->joint_encoder_dist.xyz.at(0),       cfg.first_node("sensor_joint_encoder_dist_x"));
        joint_encoder_dist.at(1)          .init(&sensorFinger->joint_encoder_dist.xyz.at(1),       cfg.first_node("sensor_joint_encoder_dist_y"));
        joint_encoder_dist.at(2)          .init(&sensorFinger->joint_encoder_dist.xyz.at(2),       cfg.first_node("sensor_joint_encoder_dist_z"));

        pressure_prox_l                   .init(&sensorFinger->pressure_prox_l    .value,          cfg.first_node("sensor_pressure_prox_l"));
        pressure_prox_r                   .init(&sensorFinger->pressure_prox_r    .value,          cfg.first_node("sensor_pressure_prox_r"));
        pressure_dist_joint               .init(&sensorFinger->pressure_dist_joint.value,          cfg.first_node("sensor_pressure_dist_joint"));
        pressure_dist_tip                 .init(&sensorFinger->pressure_dist_tip  .value,          cfg.first_node("sensor_pressure_dist_tip"));

        shear_force_dist_joint.at(0)      .init(&sensorFinger->shear_force_dist_joint.xyz.at(0),   cfg.first_node("sensor_shear_force_dist_joint_x"));
        shear_force_dist_joint.at(1)      .init(&sensorFinger->shear_force_dist_joint.xyz.at(1),   cfg.first_node("sensor_shear_force_dist_joint_y"));
        shear_force_dist_joint.at(2)      .init(&sensorFinger->shear_force_dist_joint.xyz.at(2),   cfg.first_node("sensor_shear_force_dist_joint_z"));

        shear_force_dist_tip.at(0)        .init(&sensorFinger->shear_force_dist_tip.xyz.at(0),     cfg.first_node("sensor_shear_force_dist_tip_x"));
        shear_force_dist_tip.at(1)        .init(&sensorFinger->shear_force_dist_tip.xyz.at(1),     cfg.first_node("sensor_shear_force_dist_tip_y"));
        shear_force_dist_tip.at(2)        .init(&sensorFinger->shear_force_dist_tip.xyz.at(2),     cfg.first_node("sensor_shear_force_dist_tip_z"));

        proximity                         .init(&sensorFinger->proximity.value,                    cfg.first_node("sensor_proximity"));
        for (std::size_t i = 0; i < 17; ++i)
        {
            accelerometer_fifo.at(i).at(0).init(&sensorFinger->accelerometer.fifo.at(i).at(0),     cfg.first_node("sensor_accelerometer_x"));
            accelerometer_fifo.at(i).at(1).init(&sensorFinger->accelerometer.fifo.at(i).at(1),     cfg.first_node("sensor_accelerometer_y"));
            accelerometer_fifo.at(i).at(2).init(&sensorFinger->accelerometer.fifo.at(i).at(2),     cfg.first_node("sensor_accelerometer_z"));
        }
    }

    void read_converter(auto& c)
    {
        c.read();
    }
    template<class T, std::size_t N>
    void read_converter(std::array<T, N>& cs)
    {
        for (auto& c : cs)
        {
            read_converter(c);
        }
    }

    void FingerSensorData::rtReadSensorValues(
        const IceUtil::Time& /*sensorValuesTimestamp*/,
        const IceUtil::Time& /*timeSinceLastIteration*/)
    {
        read_converter(joint_encoder_prox_temp);
        read_converter(joint_encoder_dist_temp);
        read_converter(pressure_prox_l_temp);
        read_converter(pressure_prox_r_temp);
        read_converter(pressure_dist_joint_temp);
        read_converter(pressure_dist_tip_temp);
        read_converter(shear_force_dist_joint_temp);
        read_converter(shear_force_dist_tip_temp);
        read_converter(joint_encoder_prox);
        read_converter(joint_encoder_dist);
        read_converter(pressure_prox_l);
        read_converter(pressure_prox_r);
        read_converter(pressure_dist_joint);
        read_converter(pressure_dist_tip);
        read_converter(shear_force_dist_joint);
        read_converter(shear_force_dist_tip);
        read_converter(proximity);
        read_converter(accelerometer_fifo);
    }

    void FingerSensorData::rtWriteTargetValues(
        const IceUtil::Time& /*sensorValuesTimestamp*/,
        const IceUtil::Time& /*timeSinceLastIteration*/)
    {}

    void FingerSensorData::updateSensorValueStruct(
        FingerSensorValue& data)
    {
        data.frame_counter                   = *sensor_frame_counter;

        data.joint_encoder_prox.x()          = joint_encoder_prox.at(0)    .value;
        data.joint_encoder_prox.y()          = joint_encoder_prox.at(1)    .value;
        data.joint_encoder_prox.z()          = joint_encoder_prox.at(2)    .value;
        data.joint_encoder_prox_temp         = joint_encoder_prox_temp     .value;
        data.raw_joint_encoder_prox          = sensor_finger->joint_encoder_prox.xyz;
        data.raw_joint_encoder_prox_temp     = sensor_finger->joint_encoder_prox.temperature;

        data.joint_encoder_dist.x()          = joint_encoder_dist.at(0)    .value;
        data.joint_encoder_dist.y()          = joint_encoder_dist.at(1)    .value;
        data.joint_encoder_dist.z()          = joint_encoder_dist.at(2)    .value;
        data.joint_encoder_dist_temp         = joint_encoder_dist_temp     .value;
        data.raw_joint_encoder_dist          = sensor_finger->joint_encoder_dist.xyz;
        data.raw_joint_encoder_dist_temp     = sensor_finger->joint_encoder_dist.temperature;

        data.pressure_prox_l                 = pressure_prox_l             .value;
        data.pressure_prox_l_temp            = pressure_prox_l_temp        .value;
        data.raw_pressure_prox_l             = sensor_finger->pressure_prox_l.value;
        data.raw_pressure_prox_l_temp        = sensor_finger->pressure_prox_l.temperature;

        data.pressure_prox_r                 = pressure_prox_r             .value;
        data.pressure_prox_r_temp            = pressure_prox_r_temp        .value;
        data.raw_pressure_prox_r             = sensor_finger->pressure_prox_r.value;
        data.raw_pressure_prox_r_temp        = sensor_finger->pressure_prox_r.temperature;

        data.pressure_dist_joint             = pressure_dist_joint         .value;
        data.pressure_dist_joint_temp        = pressure_dist_joint_temp    .value;
        data.raw_pressure_dist_joint         = sensor_finger->pressure_dist_joint.value;
        data.raw_pressure_dist_joint_temp    = sensor_finger->pressure_dist_joint.temperature;

        data.pressure_dist_tip               = pressure_dist_tip           .value;
        data.pressure_dist_tip_temp          = pressure_dist_tip_temp      .value;
        data.raw_pressure_dist_tip           = sensor_finger->pressure_dist_tip.value;
        data.raw_pressure_dist_tip_temp      = sensor_finger->pressure_dist_tip.temperature;

        data.shear_force_dist_joint.x()      = shear_force_dist_joint.at(0).value;
        data.shear_force_dist_joint.y()      = shear_force_dist_joint.at(1).value;
        data.shear_force_dist_joint.z()      = shear_force_dist_joint.at(2).value;
        data.shear_force_dist_joint_temp     = shear_force_dist_joint_temp .value;
        data.raw_shear_force_dist_joint      = sensor_finger->shear_force_dist_joint.xyz;
        data.raw_shear_force_dist_joint_temp = sensor_finger->shear_force_dist_joint.temperature;

        data.shear_force_dist_tip.x()        = shear_force_dist_tip.at(0)  .value;
        data.shear_force_dist_tip.y()        = shear_force_dist_tip.at(1)  .value;
        data.shear_force_dist_tip.z()        = shear_force_dist_tip.at(2)  .value;
        data.shear_force_dist_tip_temp       = shear_force_dist_tip_temp   .value;
        data.raw_shear_force_dist_tip        = sensor_finger->shear_force_dist_tip.xyz;
        data.raw_shear_force_dist_tip_temp   = sensor_finger->shear_force_dist_tip.temperature;

        data.proximity                       = proximity                   .value;
        data.raw_proximity                   = sensor_finger->proximity.value;

        data.raw_accelerometer_fifo          = sensor_finger->accelerometer.fifo;
        data.accelerometer_fifo_length       = sensor_finger->accelerometer.fifo_length;
        for (std::size_t i = 0; i < data.accelerometer_fifo.size(); ++i)
        {
            data.accelerometer_fifo.at(i).x() = accelerometer_fifo.at(i).at(0).value;
            data.accelerometer_fifo.at(i).y() = accelerometer_fifo.at(i).at(1).value;
            data.accelerometer_fifo.at(i).z() = accelerometer_fifo.at(i).at(2).value;
        }
    }
}
