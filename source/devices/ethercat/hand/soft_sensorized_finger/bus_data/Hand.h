#pragma once


#include <memory>

#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>

#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <RobotAPI/components/units/RobotUnit/Devices/ControlDevice.h>
#include <RobotAPI/components/units/RobotUnit/Devices/SensorDevice.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <RobotAPI/interface/units/KinematicUnitInterfaceStdOverloads.h>
#include <RobotAPI/libraries/core/Pose.h>
#include <armarx/control/ethercat/AbstractData.h>

#include <armarx/control/rt_filters/AverageFilter.h>
#include <armarx/control/rt_filters/FirFilter.h>
#include <armarx/control/rt_filters/RtMedianFilter.h>

//#include <devices/ethercat/common/elmo/gold/Data.h>

#include "../Slave.h"
#include "../sensor_value/Hand.h"

namespace devices::ethercat::hand::soft_sensorized_finger::bus_data
{
    class HandSensorData;
    using HandSensorDataPtr =
        std::shared_ptr<HandSensorData>;

    class HandSensorData : public armarx::control::ethercat::AbstractData
    {
    public:
        HandSensorData(
            const armarx::RapidXmlReaderNode& node,
            armarx::DefaultRapidXmlReaderNode defaultConfigurationNode,
            BoardOut* sensorAll,
            const std::string& configName);

        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

        void updateSensorValueStruct(HandSensorValue& data);

        float rtGetMotorTemperatureShutdownThreshold() const
        {
            return motor_temp_threshold_shutdown;
        }

        float rtGetMotorTemperatureRestartThreshold() const
        {
            return motor_temp_threshold_restart;
        }

    private:
        std::uint32_t* sensor_frame_counter;
        std::uint32_t* raw_temperature;
        std::uint16_t* update_rate_main;
        std::uint16_t* update_rate_fpga;
        float          motor_temp_threshold_shutdown;
        float          motor_temp_threshold_restart;
        // a*exp(b*x)+c
        float          temp_correction_a;
        float          temp_correction_b;
        float          temp_correction_c;
        armarx::control::rt_filters::RtMedianFilter temperature;
    };
}
