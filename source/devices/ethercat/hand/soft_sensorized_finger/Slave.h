#pragma once

#include <memory>
#include <thread>
#include <atomic>

#include <armarx/control/ethercat/AbstractSlave.h>
// #include <armarx/control/ethercat/DeviceFactory.h>

#include "BoardIn.h"
#include "BoardOut.h"

namespace devices::ethercat::hand::soft_sensorized_finger
{
    const std::uint32_t H2T_RIGHT_PRODUCT_CODE = 0x42424242;
    const std::uint32_t H2T_LEFT_PRODUCT_CODE  = 0x43434343;

    class Slave;
    using SlavePtr = std::shared_ptr<Slave>;

    class Slave :
        public armarx::control::ethercat::AbstractSlaveWithInputOutput<BoardIn, BoardOut>
    {

    public:
        Slave(const armarx::control::ethercat::SlaveIdentifier slaveIdentifier, u_int16_t slaveNumber);

        void doMappings() override;
        void prepareForOp() override;
        void finishPreparingForOp() override;
        bool prepare() override;
        void execute() override;
        bool shutdown() override;

        /**
         * @see AbstractSlave::hasError()
         * @return
         */
        bool hasError() override;

        void prepareForSafeOp() override;
        void finishPreparingForSafeOp() override;
    };

    // class KITSensorizedSoftFingerHandV1Factory : public DeviceFactory
    // {
    // public:
    //     KITSensorizedSoftFingerHandV1Factory() {}
    //     static std::string getName()
    //     {
    //         return "KITSensorizedSoftFingerHandV1Factory";
    //     }
    // private:
    //     static armarx::SubClassRegistry registry;
    //     static armarx::SharedPointerType createInstance(FactoryArgs args);
    // };
    // using KITSensorizedSoftFingerHandV1FactoryPtr = std::shared_ptr<KITSensorizedSoftFingerHandV1Factory>;

}
