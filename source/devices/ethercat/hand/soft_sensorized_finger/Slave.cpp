#include <iostream>

#include <ethercat.h>

#include <ArmarXCore/core/logging/Logging.h>

// #include <armarx/control/ethercat/DeviceContainer.h>
#include <armarx/control/ethercat/AbstractSlave.h>

#include "FunctionalDevice.h"
#include "Slave.h"

namespace devices::ethercat::hand::soft_sensorized_finger
{
    Slave::Slave(const armarx::control::ethercat::SlaveIdentifier slaveIdentifier,
                 uint16_t slaveNumber) :
        armarx::control::ethercat::AbstractSlaveWithInputOutput(slaveIdentifier, slaveNumber)
    {
        inputs = nullptr;
        outputs = nullptr;
        setTag(slaveIdentifier.name); // TODO: in getDefaultName
    }

    bool
    Slave::prepare()
    {
        ARMARX_INFO << deactivateSpam(5) << "Slave " << getSlaveIdentifier() << " is ready";
        return true;
    }

    bool
    Slave::shutdown()
    {
        return true;
    }

    bool
    Slave::hasError()
    {
        return false;
    }

    void
    Slave::execute()
    {
    }
    void
    Slave::doMappings()
    {
    }
    void
    Slave::prepareForSafeOp()
    {
    }
    void
    Slave::finishPreparingForSafeOp()
    {
    }
    void
    Slave::prepareForOp()
    {
    }
    void
    Slave::finishPreparingForOp()
    {
    }
} // namespace devices::ethercat::hand::soft_sensorized_finger
namespace devices::ethercat::hand::soft_sensorized_finger
{
    /**
     * register this class in the super class factory
     */
    KITSensorizedSoftFingerHandV1Factory::SubClassRegistry
        KITSensorizedSoftFingerHandV1Factory::registry(
            KITSensorizedSoftFingerHandV1Factory::getName(),
            &KITSensorizedSoftFingerHandV1Factory::createInstance);

    KITSensorizedSoftFingerHandV1Factory::SharedPointerType
    KITSensorizedSoftFingerHandV1Factory::createInstance(FactoryArgs args)
    {
        ARMARX_TRACE;
        auto [etherCAT, slaveIdx, deviceContainer] = args;
        ARMARX_CHECK_EXPRESSION(etherCAT);

        const auto eep_id = ec_slave[slaveIdx].eep_id;
        ARMARX_DEBUG << "checking\n"
                     << "ec_slave[slaveIdx].eep_id =                        = " << eep_id << "\n"
                     << "H2T_KITSensorizedSoftFingerHandV1_RIGHT_PRODUCT_CODE = "
                     << H2T_RIGHT_PRODUCT_CODE << "\n"
                     << "H2T_KITSensorizedSoftFingerHandV1_LEFT_PRODUCT_CODE  = "
                     << H2T_LEFT_PRODUCT_CODE << "\n";
        if (eep_id == H2T_RIGHT_PRODUCT_CODE || eep_id == H2T_LEFT_PRODUCT_CODE)
        {
            ARMARX_DEBUG << ".\nCandidate for hand - eep_id " << eep_id << " slaveIdx " << slaveIdx;
            uint32 serialNumber = 0;
            {
                auto success = etherCAT->getSerialNumber(slaveIdx, serialNumber);
                ARMARX_CHECK_EXPRESSION(success)
                    << "called etherCAT->getSerialNumber with " << VAROUT(slaveIdx);
            }
            const auto fdevs = deviceContainer->getAllFunctionalDevices();
            ARMARX_DEBUG << ".\nnow checking " << fdevs.size() << " functional devices";
            for (auto& dev : fdevs)
            {
                ARMARX_CHECK_NOT_NULL(dev);
                ARMARX_DEBUG << ".\nfunctional device of class: " << dev->getClassName()
                             << "\ntype = " << boost::core::demangle(typeid(*dev).name());
                ARMARX_TRACE;
                auto hand = std::dynamic_pointer_cast<FunctionalDevice>(dev);
                if (hand)
                {
                    ARMARX_DEBUG << "Found hand in config: " << hand->getSlaveIdentifier().ProductID
                                 << " serial number of slave " << (serialNumber);
                }
                else
                {
                    ARMARX_DEBUG << "discarded functional device of type " << dev->getClassName();
                }
                if (hand && hand->getSlaveIdentifier().ProductID == eep_id)
                {
                    uint32_t vendorID;
                    auto success = etherCAT->getVendorID(slaveIdx, vendorID);
                    if (!success)
                    {
                        return SharedPointerType();
                    }
                    ARMARX_IMPORTANT << "Found SensorizedSoftFingerHandV1 with product code "
                                     << eep_id << " and serial number " << serialNumber;
                    auto handSlaveId =
                        SlaveIdentifier(vendorID, eep_id, serialNumber, hand->getDeviceName());
                    auto slave = std::make_shared<Slave>(handSlaveId, slaveIdx);
                    hand->init(slave);
                    auto objFac = std::make_shared<KITSensorizedSoftFingerHandV1Factory>();
                    objFac->addSlave(slave);
                    hand->addDevicesTo(objFac);
                    return objFac;
                }
            }
        }
        return SharedPointerType();
    }
} // namespace devices::ethercat::hand::soft_sensorized_finger
