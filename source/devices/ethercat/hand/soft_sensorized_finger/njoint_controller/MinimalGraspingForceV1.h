
#pragma once


#include "RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h"
#include <devices/ethercat/hand/soft_sensorized_finger/controller/CloseAndHold.h>
#include <devices/ethercat/hand/soft_sensorized_finger/utility/Sensors.h>

#include "minimal_grasping_force_v1/GraspPhaseControllerData.h"

namespace devices::ethercat::hand::soft_sensorized_finger::njoint_controller
{
    class MinimalGraspingForceV1_NJointController :
        virtual public armarx::NJointController,
        virtual public NJointMinimalGraspingForceV1ControllerInterface
    {
    public:
        using ConfigPtrT = NJointMinimalGraspingForceV1ControllerConfigPtr;
    private:
        using MotorIdentity = Sensors::MotorIdentity;
        using FingerIdentity = Sensors::FingerIdentity;
        template<class T>
        using FingerArray = identity::FingerArray<T>;
        template<class T>
        using MotorArray = identity::MotorArray<T>;
        FingerArray<FingerSensor> _finger_sensors; //0 = little, 1 = ring, 2 = middle, 3 = index, 4 = thumb

        GraspPhaseControllerData _grasp_phase_controller_data;

        //create
    public:
        static armarx::WidgetDescription::WidgetPtr GenerateConfigDescription(
            const VirtualRobot::RobotPtr&,
            const std::map<std::string, armarx::ConstControlDevicePtr>&,
            const std::map<std::string, armarx::ConstSensorDevicePtr>&);

        static ConfigPtrT
        GenerateConfigFromVariants(const armarx::StringVariantBaseMap& values);

        MinimalGraspingForceV1_NJointController(
            armarx::RobotUnitPtr robotUnit,
            const ConfigPtrT& config,
            const VirtualRobot::RobotPtr&);

        //managemant
    public:
        std::string getClassName(const Ice::Current& = Ice::emptyCurrent) const override;

        //remote function calls
    public:
        armarx::WidgetDescription::StringWidgetDictionary getFunctionDescriptions(const Ice::Current& = Ice::emptyCurrent) const override;
        void callDescribedFunction(const std::string& name, const armarx::StringVariantBaseMap& valueMap, const Ice::Current& = Ice::emptyCurrent) override;
        //rt control
    public:
        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        void rtPreActivateController() override;
        void rtPostDeactivateController() override;

        //publish
    public:
        void onPublishActivation(const armarx::DebugDrawerInterfacePrx&, const armarx::DebugObserverInterfacePrx&) override;
        void onPublishDeactivation(const armarx::DebugDrawerInterfacePrx&, const armarx::DebugObserverInterfacePrx&) override;
        void onPublish(const armarx::SensorAndControl&, const armarx::DebugDrawerInterfacePrx&, const armarx::DebugObserverInterfacePrx&) override;

    protected:
        //data from ice -> RT
        struct DataIceToRT
        {
            struct control_data
            {
                bool active = false;
                float o = 0;
                float i = 0;
                float t = 0;
            };
            control_data pos_control;
            control_data pwm_control;
            NJointMinimalGraspingForceV1ControllerConfig cfg;
        };
        mutable std::recursive_mutex           _ice_to_rt_buffer_write_mutex;
        armarx::WriteBufferedTripleBuffer<DataIceToRT> _ice_to_rt_buffer;

        std::atomic_bool _clicked_open{false};
        std::atomic_bool _clicked_close{false};
        std::atomic_bool _clicked_stop{false};
        std::atomic_bool _clicked_recalibrate{false};
        std::atomic_bool _clicked_start_placing{false};

        //data from RT -> publish
        struct DataRTToPublish
        {
            FingerArray<FingerSensor> normalized_finger_sensors;
            bool updating_sensors = false;
            bool resetting_controller = false;
            bool run_controller = false;

            MotorArray<MotorGraspPhaseControllerData::controller_state_data> motor_controller_data;

            float last_fpga_dt = 0;

            float active_shear_avg_len_xy = 0;
            float active_shear_avg_len_z = 0;
            float active_shear_avg_ratio = 0;
            std::size_t active_shear_num = 0;
            float used_shear_avg_ratio = 0;

            std::size_t open_signal_finger_count = 0;
            bool        open_signal              = false;
        };
        armarx::WriteBufferedTripleBuffer<DataRTToPublish> _rt_to_ice;
        mutable std::recursive_mutex               _rt_to_ice_read_mutex;


        //sensors
        Sensors _sensors;

        //ctrl target
        armarx::ControlTarget1DoFActuatorPWM*        _target_pwm_thumb = nullptr;
        armarx::ControlTarget1DoFActuatorPWM*        _target_pwm_index = nullptr;
        armarx::ControlTarget1DoFActuatorPWM*        _target_pwm_other = nullptr;

        armarx::ControlTarget1DoFActuatorPWM*
        target_pwm(MotorIdentity id) const
        {
            return identity::Select(
                       id, _target_pwm_other, _target_pwm_index, _target_pwm_thumb);
        }

    public:
        void closeMGF(const Ice::Current& = Ice::emptyCurrent) override;
        void openMGF(const Ice::Current& = Ice::emptyCurrent) override;
        void startPlacing(const Ice::Current& = Ice::emptyCurrent) override;
        void overridePWM(bool active, Ice::Float o, Ice::Float i, Ice::Float t, const Ice::Current& = Ice::emptyCurrent) override;
        GraspPhase::Phase currentGraspPhase(const Ice::Current& = Ice::emptyCurrent) override;
        void setConfig(const NJointMinimalGraspingForceV1ControllerConfigPtr& cfg, const Ice::Current&) override;
        void recalibrate(const Ice::Current& = Ice::emptyCurrent) override;
        NJointMinimalGraspingForceV1ControllerDebugData getDebugData(const Ice::Current& = Ice::emptyCurrent) override;
    };
}
namespace armarx::detail
{
    static_assert(
        hasGenerateConfigDescription< devices::ethercat::hand::soft_sensorized_finger::njoint_controller::MinimalGraspingForceV1_NJointController>::value);
    static_assert(
        hasGenerateConfigFromVariants< devices::ethercat::hand::soft_sensorized_finger::njoint_controller::MinimalGraspingForceV1_NJointController>::value);
}
