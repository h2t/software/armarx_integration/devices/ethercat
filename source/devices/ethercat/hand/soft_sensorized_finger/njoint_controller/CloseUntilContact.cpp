/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    robot_devices::ArmarXObjects::KITSensorizedSoftFingerHandV1NJointController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>

#include "CloseUntilContact.h"

//bouler
namespace devices::ethercat::hand::soft_sensorized_finger::njoint_controller
{
    armarx::NJointControllerRegistration<KITSensorizedSoftFingerHandV1NJointController>
    registrationControllerKITSensorizedSoftFingerHandV1NJointControllers("KITSensorizedSoftFingerHandV1NJointController");

    KITSensorizedSoftFingerHandV1NJointController::KITSensorizedSoftFingerHandV1NJointController(
        armarx::RobotUnitPtr robotUnit,
        const KITSensorizedSoftFingerHandV1NJointControllerConfigPtr& config,
        const VirtualRobot::RobotPtr&)
    {
        ARMARX_CHECK_NOT_NULL(config);
        //setup params
        {
            _force_thresh_hold_start_thumb = config->thumb_holding_start_force_threshold;
            _force_thresh_hold_start_index = config->index_holding_start_force_threshold;
            _force_thresh_hold_start_other = config->other_holding_start_force_threshold;

            _force_thresh_hold_stop_thumb = config->thumb_holding_stop_force_threshold;
            _force_thresh_hold_stop_index = config->index_holding_stop_force_threshold;
            _force_thresh_hold_stop_other = config->other_holding_stop_force_threshold;

            _holding_mode_thumb = config->thumb_holding_mode;
            _holding_mode_index = config->index_holding_mode;
            _holding_mode_other = config->other_holding_mode;

            _finger_vel_thumb = 0;
            _finger_vel_index = 0;
            _finger_vel_other = 0;

            _finger_vel_thumb_trg = config->thumb_velocity;
            _finger_vel_index_trg = config->index_velocity;
            _finger_vel_other_trg = config->other_velocity;

            _max_pwm_thumb = config->thumb_max_pwm;
            _max_pwm_index = config->index_max_pwm;
            _max_pwm_other = config->other_max_pwm;

            Controllers::CloseAndHoldParams params_thumb;
            Controllers::CloseAndHoldParams params_index;
            Controllers::CloseAndHoldParams params_other;

            params_thumb.p = config->thumb_p;
            params_index.p = config->index_p;
            params_other.p = config->other_p;

            params_thumb.i = config->thumb_i;
            params_index.i = config->index_i;
            params_other.i = config->other_i;

            params_thumb.d = config->thumb_d;
            params_index.d = config->index_d;
            params_other.d = config->other_d;

            params_thumb.holding_start_force_threshold = config->thumb_holding_start_force_threshold;
            params_index.holding_start_force_threshold = config->index_holding_start_force_threshold;
            params_other.holding_start_force_threshold = config->other_holding_start_force_threshold;

            params_thumb.holding_stop_force_threshold = config->thumb_holding_stop_force_threshold;
            params_index.holding_stop_force_threshold = config->index_holding_stop_force_threshold;
            params_other.holding_stop_force_threshold = config->other_holding_stop_force_threshold;

            _ctrl_thumb = Controllers::CloseAndHold(params_thumb);
            _ctrl_index = Controllers::CloseAndHold(params_index);
            _ctrl_other = Controllers::CloseAndHold(params_other);
        }
        //get ctrl
        {
            armarx::ControlTargetBase* ct_thumb = useControlTarget(config->hand_name + ".motor.thumb", armarx::ControlModes::PWM1DoF);
            armarx::ControlTargetBase* ct_index = useControlTarget(config->hand_name + ".motor.index", armarx::ControlModes::PWM1DoF);
            armarx::ControlTargetBase* ct_other = useControlTarget(config->hand_name + ".motor.other", armarx::ControlModes::PWM1DoF);
            ARMARX_CHECK_EXPRESSION(ct_thumb->isA<armarx::ControlTarget1DoFActuatorPWM>());
            ARMARX_CHECK_EXPRESSION(ct_index->isA<armarx::ControlTarget1DoFActuatorPWM>());
            ARMARX_CHECK_EXPRESSION(ct_other->isA<armarx::ControlTarget1DoFActuatorPWM>());

            _target_pwm_thumb = ct_thumb->asA<armarx::ControlTarget1DoFActuatorPWM>();
            _target_pwm_index = ct_index->asA<armarx::ControlTarget1DoFActuatorPWM>();
            _target_pwm_other = ct_other->asA<armarx::ControlTarget1DoFActuatorPWM>();
        }
        //get sens
        _sensors = Sensors(config->hand_name, *this);
    }

    std::string KITSensorizedSoftFingerHandV1NJointController::getClassName(const Ice::Current&) const
    {
        return "KITSensorizedSoftFingerHandV1NJointController";
    }

    armarx::WidgetDescription::StringWidgetDictionary KITSensorizedSoftFingerHandV1NJointController::getFunctionDescriptions(const Ice::Current&) const
    {
        using namespace armarx::WidgetDescription;
        VBoxLayoutPtr set_params = new VBoxLayout;
        {
            const auto line = [&](const std::string & name, auto & start, auto & stop, auto & vel, auto & pwm, auto & mode)
            {
                HBoxLayoutPtr line = new HBoxLayout;
                set_params->children.emplace_back(line);

                line->children.emplace_back(new Label(false, name));

                {
                    line->children.emplace_back(new Label(false, "hold start"));
                    FloatSpinBoxPtr e = new FloatSpinBox;
                    e->name = name + "start";
                    e->min = 0;
                    e->defaultValue = start;
                    e->max = 1;
                    line->children.emplace_back(e);
                }
                {
                    line->children.emplace_back(new Label(false, "hold stop"));
                    FloatSpinBoxPtr e = new FloatSpinBox;
                    e->name = name + "stop";
                    e->min = 0;
                    e->defaultValue = stop;
                    e->max = 1;
                    line->children.emplace_back(e);
                }
                {
                    line->children.emplace_back(new Label(false, "close vel"));
                    FloatSpinBoxPtr e = new FloatSpinBox;
                    e->name = name + "vel";
                    e->min = 0;
                    e->defaultValue = vel;
                    e->max = 5;
                    line->children.emplace_back(e);
                }
                {
                    line->children.emplace_back(new Label(false, "max pwm"));
                    IntSpinBoxPtr e = new IntSpinBox;
                    e->name = name + "pwm";
                    e->min = 0;
                    e->defaultValue = pwm;
                    e->max = 500;
                    line->children.emplace_back(e);
                }
                {
                    line->children.emplace_back(new Label(false, "hold mode"));
                    StringComboBoxPtr e = new StringComboBox;
                    e->name = name + "hold";
                    e->defaultIndex = (mode == CAHCtrl::HoldingMode::velocity ? 0 : 1);
                    e->options.emplace_back("vel");
                    e->options.emplace_back("pwm");
                    line->children.emplace_back(e);
                }
            };
            line("thumb", _force_thresh_hold_start_thumb, _force_thresh_hold_stop_thumb, _finger_vel_thumb_trg, _max_pwm_thumb, _holding_mode_thumb);
            line("index", _force_thresh_hold_start_index, _force_thresh_hold_stop_index, _finger_vel_index_trg, _max_pwm_index, _holding_mode_index);
            line("other", _force_thresh_hold_start_other, _force_thresh_hold_stop_other, _finger_vel_other_trg, _max_pwm_other, _holding_mode_other);
        }
        return {{"SetParams", set_params}, {"Open", nullptr}, {"Close", nullptr}, {"Relax", nullptr}};
    }

    void KITSensorizedSoftFingerHandV1NJointController::callDescribedFunction(const std::string& name, const armarx::StringVariantBaseMap& valueMap, const Ice::Current&)
    {
        if (name == "SetParams")
        {
            const auto read = [&](const std::string & name, auto & start, auto & stop, auto & vel, auto & pwm, auto & mode)
            {
                start = valueMap.at(name + "start")->getFloat();
                stop = valueMap.at(name + "stop")->getFloat();
                vel = valueMap.at(name + "vel")->getFloat();
                pwm = valueMap.at(name + "pwm")->getInt();
                mode = (valueMap.at(name + "hold")  ->getString() == "vel") ? CAHCtrl::HoldingMode::velocity : CAHCtrl::HoldingMode::pwm;
            };
            read("thumb", _force_thresh_hold_start_thumb, _force_thresh_hold_stop_thumb, _finger_vel_thumb_trg, _max_pwm_thumb, _holding_mode_thumb);
            read("index", _force_thresh_hold_start_index, _force_thresh_hold_stop_index, _finger_vel_index_trg, _max_pwm_index, _holding_mode_index);
            read("other", _force_thresh_hold_start_other, _force_thresh_hold_stop_other, _finger_vel_other_trg, _max_pwm_other, _holding_mode_other);

        }
        else if (name == "Open")
        {
            _finger_vel_thumb = -_finger_vel_thumb_trg;
            _finger_vel_index = -_finger_vel_index_trg;
            _finger_vel_other = -_finger_vel_other_trg;
        }
        else if (name == "Close")
        {
            _finger_vel_thumb = _finger_vel_thumb_trg;
            _finger_vel_index = _finger_vel_index_trg;
            _finger_vel_other = _finger_vel_other_trg;
        }
        else if (name == "Stop")
        {
            _finger_vel_thumb = 0;
            _finger_vel_index = 0;
            _finger_vel_other = 0;
        }
        else
        {
            ARMARX_WARNING << "Unknown function name called: " << name;
        }
    }

    armarx::WidgetDescription::WidgetPtr
    KITSensorizedSoftFingerHandV1NJointController::GenerateConfigDescription(
        const VirtualRobot::RobotPtr&,
        const std::map<std::string, armarx::ConstControlDevicePtr>&,
        const std::map<std::string, armarx::ConstSensorDevicePtr>&)
    {
        using namespace armarx::WidgetDescription;

        VBoxLayoutPtr set_params = new VBoxLayout;
        {
            HBoxLayoutPtr line = new HBoxLayout;
            set_params->children.emplace_back(line);

            line->children.emplace_back(new Label(false, "hand name"));
            LineEditPtr e = new LineEdit;
            e->name = "hand";
            e->defaultValue = "RightHand";
            line->children.emplace_back(e);
        }
        const auto line = [&](const std::string & name)
        {
            HBoxLayoutPtr line = new HBoxLayout;
            set_params->children.emplace_back(line);

            line->children.emplace_back(new Label(false, name));

            {
                line->children.emplace_back(new Label(false, "p"));
                FloatSpinBoxPtr e = new FloatSpinBox;
                e->name = name + "p";
                e->min = 0;
                e->defaultValue = 10;
                e->max = 1000;
                line->children.emplace_back(e);
            }
            {
                line->children.emplace_back(new Label(false, "i"));
                FloatSpinBoxPtr e = new FloatSpinBox;
                e->name = name + "i";
                e->min = 0;
                e->defaultValue = 0;
                e->max = 1000;
                line->children.emplace_back(e);
            }
            {
                line->children.emplace_back(new Label(false, "d"));
                FloatSpinBoxPtr e = new FloatSpinBox;
                e->name = name + "d";
                e->min = 0;
                e->defaultValue = 0;
                e->max = 1000;
                line->children.emplace_back(e);
            }
            {
                line->children.emplace_back(new Label(false, "hold start"));
                FloatSpinBoxPtr e = new FloatSpinBox;
                e->name = name + "start";
                e->min = 0;
                e->defaultValue = 0.05;
                e->max = 1;
                line->children.emplace_back(e);
            }
            {
                line->children.emplace_back(new Label(false, "hold stop"));
                FloatSpinBoxPtr e = new FloatSpinBox;
                e->name = name + "stop";
                e->min = 0;
                e->defaultValue = 0.025;
                e->max = 1;
                line->children.emplace_back(e);
            }
            {
                line->children.emplace_back(new Label(false, "close vel"));
                FloatSpinBoxPtr e = new FloatSpinBox;
                e->name = name + "vel";
                e->min = 0;
                e->defaultValue = 0.5;
                e->max = 5;
                line->children.emplace_back(e);
            }
            {
                line->children.emplace_back(new Label(false, "max pwm"));
                IntSpinBoxPtr e = new IntSpinBox;
                e->name = name + "pwm";
                e->min = 0;
                e->defaultValue = 512;
                e->max = 1000;
                line->children.emplace_back(e);
            }
            {
                line->children.emplace_back(new Label(false, "hold mode"));
                StringComboBoxPtr e = new StringComboBox;
                e->name = name + "hold";
                e->defaultIndex = 0;
                e->options.emplace_back("vel");
                e->options.emplace_back("pwm");
                line->children.emplace_back(e);
            }
        };
        line("thumb");
        line("index");
        line("other");
        return set_params;
    }

    KITSensorizedSoftFingerHandV1NJointControllerConfigPtr
    KITSensorizedSoftFingerHandV1NJointController::GenerateConfigFromVariants(const armarx::StringVariantBaseMap& valueMap)
    {
        KITSensorizedSoftFingerHandV1NJointControllerConfigPtr cfg = new KITSensorizedSoftFingerHandV1NJointControllerConfig;

        cfg->hand_name = valueMap.at("hand")->getString();

        const auto read = [&](const std::string & name, auto & p, auto & i, auto & d, auto & start, auto & stop, auto & pwm, auto & vel, auto & mode)
        {
            p     = valueMap.at(name + "p")    ->getFloat();
            i     = valueMap.at(name + "i")    ->getFloat();
            d     = valueMap.at(name + "d")    ->getFloat();
            start = valueMap.at(name + "start")->getFloat();
            stop  = valueMap.at(name + "stop") ->getFloat();
            vel   = valueMap.at(name + "vel")  ->getFloat();
            pwm   = valueMap.at(name + "pwm")  ->getInt();
            mode = (valueMap.at(name + "hold")  ->getString() == "vel") ? CAHCtrl::HoldingMode::velocity : CAHCtrl::HoldingMode::pwm;
        };
        read("thumb", cfg->thumb_p, cfg->thumb_i, cfg->thumb_d,
             cfg->thumb_holding_start_force_threshold,
             cfg->thumb_holding_stop_force_threshold,
             cfg->thumb_max_pwm, cfg->thumb_velocity,
             cfg->thumb_holding_mode);

        read("index", cfg->index_p, cfg->index_i, cfg->index_d,
             cfg->index_holding_start_force_threshold,
             cfg->index_holding_stop_force_threshold,
             cfg->index_max_pwm, cfg->index_velocity,
             cfg->index_holding_mode);

        read("other", cfg->other_p, cfg->other_i, cfg->other_d,
             cfg->other_holding_start_force_threshold,
             cfg->other_holding_stop_force_threshold,
             cfg->other_max_pwm, cfg->other_velocity,
             cfg->other_holding_mode);
        return cfg;
    }
}

namespace devices::ethercat::hand::soft_sensorized_finger::njoint_controller
{
    void KITSensorizedSoftFingerHandV1NJointController::rtRun(
        const IceUtil::Time& sensorValuesTimestamp,
        const IceUtil::Time& timeSinceLastIteration)
    {
        const auto dt = timeSinceLastIteration.toSecondsDouble();

        const float thumb_force = _sensors.finger.thumb()->pressure_dist_tip;
        const float index_force = _sensors.finger.index()->pressure_dist_tip;
        const float other_force = _sensors.finger.ring()->pressure_dist_tip;

        const float thumb_pos = _sensors.motor.thumb()->position;
        const float index_pos = _sensors.motor.index()->position;
        const float other_pos = _sensors.motor.other()->position;

        _ctrl_thumb.cfg.holding_start_force_threshold = _force_thresh_hold_start_thumb.load();
        _ctrl_index.cfg.holding_start_force_threshold = _force_thresh_hold_start_index.load();
        _ctrl_other.cfg.holding_start_force_threshold = _force_thresh_hold_start_other.load();

        _ctrl_thumb.cfg.holding_stop_force_threshold = _force_thresh_hold_stop_thumb.load();
        _ctrl_index.cfg.holding_stop_force_threshold = _force_thresh_hold_stop_index.load();
        _ctrl_other.cfg.holding_stop_force_threshold = _force_thresh_hold_stop_other.load();

        _target_pwm_thumb->pwm = _ctrl_thumb.calculate(thumb_force, thumb_pos, _finger_vel_thumb, dt, _max_pwm_thumb);
        _target_pwm_index->pwm = _ctrl_index.calculate(index_force, index_pos, _finger_vel_index, dt, _max_pwm_index);
        _target_pwm_other->pwm = _ctrl_other.calculate(other_force, other_pos, _finger_vel_other, dt, _max_pwm_other);
    }

    void KITSensorizedSoftFingerHandV1NJointController::rtPreActivateController()
    {
        _ctrl_thumb.reset(_sensors.motor.thumb()->position);
        _ctrl_index.reset(_sensors.motor.index()->position);
        _ctrl_other.reset(_sensors.motor.other()->position);
    }
    void KITSensorizedSoftFingerHandV1NJointController::rtPostDeactivateController()
    {}
}
