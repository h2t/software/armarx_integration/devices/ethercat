#pragma once

#include <RobotAPI/interface/units/RobotUnit/NJointController.ice>

module armarx
{
    module KITSensorizedSoftFingerHand
    {
        module V1
        {
            module GraspPhase
            {
                enum Phase
                {
                    FREELY_MOVING    = 1,
                    BARRIER_PRE_LLH  = 2,
                    LOAD_LIFT_HOLD   = 3,
                    UNLOAD           = 4,
                    BARRIER_PRE_OPEN = 5,
                    OPEN             = 6
                };
            };

            module NJointMinimalGraspingForceV1ControllerConfigElements
            {
                struct PID
                {
                    float p;
                    float i;
                    float d;
                };
                struct MotorCfg
                {
                    PID          pid_pos;
                    PID          pid_pressure;
                    PID          pid_shear;
                    PID          pid_unload;

                    float        pid_force_forward_p; //start at 200
                    float        pid_force_limit;
                    PID          pid_force;

                    short max_pwm                  = 1000;
                    short pwm_open                 = -200;
                    short pwm_freely_moving        = 1000;
                    short min_pwm_close_to_contact = 200;
                    float target_pressure          = 30;
                    float target_shear             = 1.3;

                    long free_phase_start_moving_delay_msec = 0;

                    float gross_slip_pos_step  = 0.1;
                    float gross_slip_threshold = 0.1;
                    float encoder_delta_for_movent = 0.05;

                    short gross_slip_pwm_step = 25;
                    float force_ctrl_pressure_factor = 1;
                };
            };

            class NJointMinimalGraspingForceV1ControllerConfig extends NJointControllerConfig
            {
                string          hand_name;

                float          place_fft_energy_threshold = 100000;
                int          place_finger_trigger_count = 5;
                long min_load_lift_hold_duration = 2000000;
                NJointMinimalGraspingForceV1ControllerConfigElements::MotorCfg motor_index;
                NJointMinimalGraspingForceV1ControllerConfigElements::MotorCfg motor_other;
                NJointMinimalGraspingForceV1ControllerConfigElements::MotorCfg motor_thumb;
                float pwm_scaling = 1;
                bool always_execute_controllers_depending_on_fpga = false;
                bool load_lift_hold_pressure_relative_pwm = true;
                bool load_lift_hold_shear_relative_pwm = true;
                bool use_shear_xy_offset = true;
                bool control_pressure_on_other = true;
                long min_contacts_for_shear_control = 1;
            };

            struct NJointMinimalGraspingForceV1ControllerDebugDataMotor
            {
                GraspPhase::Phase grasp_phase = GraspPhase::Phase::FREELY_MOVING;

                float  squish_distance                                       = 0;
                float  last_shear_pressure_position                          = 0.0f;
                float  pos_ctrl_target                                       = 0.0f;
                float  normal_force_controller_position_delta                = 0.0f;
                float  shear_force_controller_position_delta                 = 0.0f;
                float  shear_force_min_xy_len                                = 0.0f;
                float  shear_force_min_z_len                                 = 0.0f;
                float  shear_force_min_ratio                                 = 0.0f;
                float  shear_force_avg_ratio                                 = 0.0f;
                float  shear_force_used_ratio                                = 0.0f;
                long   shear_force_used_ratio_num                            = 0;
                double slip_detection_value                                  = 0.0f;
                float  shear_force_ratio_number_of_active_sensors            = 0.0f;
                float  maximum_pressure                                      = 0;
                float  pressure_on_start_unload                              = 0;

                float  target_shear                                          = 0.0f;
                float  target_pressure                                       = 0.0f;

                bool   contact_detection_flag                                = false;
                long   gross_slip_count                                      = 0;
                bool   finger_did_close                                      = false;

                float  monotonic_shear_pressure_control_last_pwm             = 0;
                float  monotonic_shear_pressure_control_last_pwm_pressure    = 0;
                float  monotonic_shear_pressure_control_last_pwm_shear       = 0;
                float  monotonic_shear_pressure_control_last_pwm_slip        = 0;
                float  monotonic_shear_pressure_control_last_delta_pressure  = 0;
                float  monotonic_shear_pressure_control_last_delta_shear     = 0;

                float  target_pressure_rate_of_change                        = 0;
                float  pressure_rate_of_change                               = 0;
                float  unload_phase_controller_normal_force_via_pwm_last_pwm = 0;
            };

            struct NJointMinimalGraspingForceV1ControllerDebugDataFingerNF
            {
                long  current                             = 0;
                float continuous_mean                     = 0;
                float current_mean_free_scaled            = 0;
                float current_continuous_mean_free_scaled = 0;
            };
            struct NJointMinimalGraspingForceV1ControllerDebugDataFingerShear
            {
                float len_xy                                = 0;
                float len_z                                 = 0;
                float ratio                                 = 0;

                long  x_current                             = 0;
                float x_continuous_mean                     = 0;
                float x_current_mean_free_scaled            = 0;
                float x_current_continuous_mean_free_scaled = 0;

                long  y_current                             = 0;
                float y_continuous_mean                     = 0;
                float y_current_mean_free_scaled            = 0;
                float y_current_continuous_mean_free_scaled = 0;

                long  z_current                             = 0;
                float z_continuous_mean                     = 0;
                float z_current_mean_free_scaled            = 0;
                float z_current_continuous_mean_free_scaled = 0;
            };
            struct NJointMinimalGraspingForceV1ControllerDebugDataFingerAcc
            {
                double shear_force_criterium     = 0.0f;
                double fft_contact_detect_energy = 0.0f;
            };

            struct NJointMinimalGraspingForceV1ControllerDebugDataFinger
            {
                float fit_prox_x   = 0;
                float fit_dist_x   = 0;
                float fit_prox_z   = 0;
                float fit_dist_z   = 0;
                float fit_prox_avg = 0;
                float fit_dist_avg = 0;
                NJointMinimalGraspingForceV1ControllerDebugDataFingerNF nf_prox_l;
                NJointMinimalGraspingForceV1ControllerDebugDataFingerNF nf_prox_r;
                NJointMinimalGraspingForceV1ControllerDebugDataFingerNF nf_dist_joint;
                NJointMinimalGraspingForceV1ControllerDebugDataFingerNF nf_dist_tip;
                NJointMinimalGraspingForceV1ControllerDebugDataFingerShear shear_force_dist_joint;
                NJointMinimalGraspingForceV1ControllerDebugDataFingerShear shear_force_dist_tip;
                NJointMinimalGraspingForceV1ControllerDebugDataFingerAcc accelerometer;
            };

            struct NJointMinimalGraspingForceV1ControllerDebugData
            {
                NJointMinimalGraspingForceV1ControllerDebugDataMotor motor_other;
                NJointMinimalGraspingForceV1ControllerDebugDataMotor motor_index;
                NJointMinimalGraspingForceV1ControllerDebugDataMotor motor_thumb;

                NJointMinimalGraspingForceV1ControllerDebugDataFinger finger_little;
                NJointMinimalGraspingForceV1ControllerDebugDataFinger finger_ring;
                NJointMinimalGraspingForceV1ControllerDebugDataFinger finger_middle;
                NJointMinimalGraspingForceV1ControllerDebugDataFinger finger_index;
                NJointMinimalGraspingForceV1ControllerDebugDataFinger finger_thumb;

                GraspPhase::Phase grasp_phase = GraspPhase::Phase::FREELY_MOVING;
                long  sensor_time_stamp_us_ice             = 0;
                long  time_stamp_us_std                    = 0;

                bool  updating_sensors                     = false;
                bool  resetting_controller                 = false;
                bool  run_controller                       = false;

                float all_shear_avg_len_xy                 = 0;
                float all_shear_avg_len_z                  = 0;
                float all_shear_avg_ratio                  = 0;

                float last_fpga_dt                         = 0;

                float finger_active_shear_avg_len_xy       = 0;
                float finger_active_shear_avg_len_z        = 0;
                float finger_active_shear_avg_ratio        = 0;
                long  finger_active_shear_num              = 0;

                float active_shear_avg_len_xy              = 0;
                float active_shear_avg_len_z               = 0;
                float active_shear_avg_ratio               = 0;
                long  active_shear_num                     = 0;
                float used_shear_avg_ratio                 = 0;

                float negative_z_shear_avg_len_xy          = 0;
                float negative_z_shear_avg_len_z           = 0;
                float negative_z_shear_avg_ratio           = 0;
                long  negative_z_shear_num                 = 0;

                float all_shear_avg_len_z_pressure_sensors = 0.0f;
                float all_shear_avg_ratio_pressure_sensors = 0.0f;

                long  open_signal_finger_count             = 0;
                bool  open_signal                          = false;
            };

            interface NJointMinimalGraspingForceV1ControllerInterface extends NJointControllerInterface
            {
                void closeMGF();
                void openMGF();
                void startPlacing();
                void overridePWM(bool active, float pwmo, float pwmi, float pwmt);
                GraspPhase::Phase currentGraspPhase();
                void setConfig(NJointMinimalGraspingForceV1ControllerConfig cfg);
                void recalibrate();
                NJointMinimalGraspingForceV1ControllerDebugData getDebugData();
            };
        };
    };
};

