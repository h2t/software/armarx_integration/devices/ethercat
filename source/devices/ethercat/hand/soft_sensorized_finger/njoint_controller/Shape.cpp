#include <boost/algorithm/clamp.hpp>
#include "ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h"

#include "RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h"
#include <RobotAPI/libraries/core/math/MathUtils.h>

#include "../joint_controller/Pwm.h"
#include "Shape.h"

//init
namespace devices::ethercat::hand::soft_sensorized_finger::njoint_controller
{
    armarx::NJointControllerRegistration<NJointShapeController>
    registrationControllerNJointShapeController(
        "NJointKITSensorizedSoftFingerHandV1ShapeController");

    NJointShapeController::NJointShapeController(armarx::RobotUnitPtr prov,
            NJointShapeControllerConfigPtr config,
            const VirtualRobot::RobotPtr&)
    {
        ARMARX_TRACE;
        ARMARX_CHECK_EXPRESSION(prov);
        const auto init = [&](auto & data, const std::string & suffix)
        {
            const auto name = config->deviceName + ".motor." + suffix;
            armarx::ControlTargetBase* ct = useControlTarget(name, armarx::ControlModes::PWM1DoF);
            ARMARX_CHECK_NOT_NULL(ct) << " control target " << name
                                      << " control mode " << armarx::ControlModes::PWM1DoF
                                      << ARMARX_STREAM_PRINTER
            {
                const auto ptr = peekControlDevice(name);
                if (ptr)
                {
                    out << "\nthese are the available controllers: "
                        << ptr->getJointControllerToTargetTypeNameMap();
                }
                else
                {
                    out << "\nthere is no control device with this name";
                }
            };

            armarx::ConstSensorDevicePtr sensor = peekSensorDevice(name);
            ARMARX_CHECK_NOT_NULL(sensor) << "device name: " << name;

            data.motor_sensor = sensor->getSensorValue()->asA<MotorSensorValue>();
            ARMARX_CHECK_NOT_NULL(data.motor_sensor) << "device name: " << name;
            data.pwm_target  = ct->asA<armarx::ControlTarget1DoFActuatorPWM>();
            ARMARX_CHECK_NOT_NULL(data.pwm_target) << "device name: " << name;
        };
        init(_other_data, "other");
        init(_index_data, "index");
        init(_thumb_data, "thumb");
    }
    void NJointShapeController::readConfig(
        const armarx::RapidXmlReaderNode& configNode,
        armarx::DefaultRapidXmlReaderNode defaultConfigurationNode)
    {
        ARMARX_TRACE;
        ARMARX_DEBUG << "readConfig got cfg:\n" << VAROUT(configNode.getPath())
                     << "\n" << VAROUT(defaultConfigurationNode.getPathsString());

        auto configNodeAll =
            defaultConfigurationNode
            .add_node_at_end(configNode)
            .first_node("Motor");

        const auto init = [&](auto & data, auto name)
        {
            auto cfg = configNodeAll
                       .first_node("default")
                       .add_node_at_end(configNodeAll.first_node(name))
                       .first_node("ctrl_shape");
            cfg.attribute_as("Kp", data.Kp);
            cfg.attribute_as("Ki", data.Ki);
            cfg.attribute_as("Kd", data.Kd);
            cfg.attribute_as("max_pwm", data.pwm_limit);
        };
        init(_thumb_data, "thumb");
        init(_index_data, "index");
        init(_other_data, "other");
    }
}

//getter
namespace devices::ethercat::hand::soft_sensorized_finger::njoint_controller
{
    int16_t NJointShapeController::getOtherPwm()
    {
        return _other_data.last_pwm;
    }
    int16_t NJointShapeController::getIndexPwm()
    {
        return _index_data.last_pwm;
    }
    int16_t NJointShapeController::getThumbPwm()
    {
        return _thumb_data.last_pwm;
    }

    float NJointShapeController::getOtherTarget() const
    {
        std::lock_guard g{_last_target_buf_read_mutex};
        return _last_target_buf.getUpToDateReadBuffer().other.target;
    }
    float NJointShapeController::getIndexTarget() const
    {
        std::lock_guard g{_last_target_buf_read_mutex};
        return _last_target_buf.getUpToDateReadBuffer().index.target;
    }
    float NJointShapeController::getThumbTarget() const
    {
        std::lock_guard g{_last_target_buf_read_mutex};
        return _last_target_buf.getUpToDateReadBuffer().thumb.target;
    }

    float NJointShapeController::getOtherJointValue() const
    {
        return _other_data.joint_value;
    }
    float NJointShapeController::getIndexJointValue() const
    {
        return _index_data.joint_value;
    }
    float NJointShapeController::getThumbJointValue() const
    {
        return _thumb_data.joint_value;
    }

    bool NJointShapeController::isControlEnabled() const
    {
        std::lock_guard g{_last_target_buf_read_mutex};
        return _last_target_buf.getUpToDateReadBuffer().enable_control;
    }

    JointValues
    NJointShapeController::getJointValues(const Ice::Current&) const
    {
        JointValues values;
        values.otherJointValue  = getOtherJointValue();
        values.indexJointValue  = getIndexJointValue();
        values.thumbJointValue  = getThumbJointValue();
        return values;
    }
}

//setter
namespace devices::ethercat::hand::soft_sensorized_finger::njoint_controller
{
    void NJointShapeController::stopMotion()
    {
        std::lock_guard g{_target_buf_write_mutex};
        _target_buf.getWriteBuffer().enable_control = false;
        _target_buf.commitWrite();
    }
    void NJointShapeController::setTargets(
        float lrm, float index, float thumb, const Ice::Current&)
    {
        setTargetsWithPwm(lrm, index, thumb, 1, 1, 1);
    }

    void NJointShapeController::setTargetsWithPwm(
        float lrm, float index, float thumb,
        float lrmRelativePWM, float indexRelativePWM, float thumbRelativePWM,
        const Ice::Current&)
    {
        std::lock_guard g{_target_buf_write_mutex};
        auto& buf = _target_buf.getWriteBuffer();

        buf.enable_control = true;
        buf.index.target  = armarx::math::MathUtils::LimitMinMax(0.f, 2.f, thumb);
        buf.thumb.target  = armarx::math::MathUtils::LimitMinMax(0.f, 2.f, index);
        buf.other.target  = armarx::math::MathUtils::LimitMinMax(0.f, 2.f, lrm);
        buf.index.max_pwm = static_cast<std::int16_t>(_index_data.pwm_limit * boost::algorithm::clamp(indexRelativePWM, 0, 1));
        buf.thumb.max_pwm = static_cast<std::int16_t>(_thumb_data.pwm_limit * boost::algorithm::clamp(thumbRelativePWM, 0, 1));
        buf.other.max_pwm = static_cast<std::int16_t>(_other_data.pwm_limit * boost::algorithm::clamp(lrmRelativePWM, 0, 1));
        _target_buf.commitWrite();
    }
}

//control
namespace devices::ethercat::hand::soft_sensorized_finger::njoint_controller
{
    void NJointShapeController::rtRun(
        const IceUtil::Time& sensorValuesTimestamp,
        const IceUtil::Time& timeSinceLastIteration)
    {
        _other_data.joint_value = _other_data.motor_sensor->position;
        _index_data.joint_value = _index_data.motor_sensor->position;
        _thumb_data.joint_value = _thumb_data.motor_sensor->position;

        const auto& buf = _target_buf.getUpToDateReadBuffer();
        _last_target_buf.getWriteBuffer() = buf;
        _last_target_buf.commitWrite();

        if (buf.enable_control)
        {
            auto calc = [](auto & buff, auto & data)
            {
                const float err = buff.target - data.motor_sensor->position;
                data.pwm_target->pwm = boost::algorithm::clamp(
                                           static_cast<std::int16_t>(data.Kp * buff.max_pwm * err),
                                           -buff.max_pwm, buff.max_pwm);
            };
            calc(buf.other, _other_data);
            calc(buf.index, _index_data);
            calc(buf.thumb, _thumb_data);
        }
        else
        {
            _other_data.pwm_target->pwm = 0;
            _index_data.pwm_target->pwm = 0;
            _thumb_data.pwm_target->pwm = 0;
        }
        _other_data.last_pwm = _other_data.pwm_target->pwm;
        _index_data.last_pwm = _index_data.pwm_target->pwm;
        _thumb_data.last_pwm = _thumb_data.pwm_target->pwm;
    }
}

//gui
namespace devices::ethercat::hand::soft_sensorized_finger::njoint_controller
{
    armarx::WidgetDescription::StringWidgetDictionary NJointShapeController::getFunctionDescriptions(const Ice::Current&) const
    {
        using namespace armarx::WidgetDescription;
        VBoxLayoutPtr vbox = new VBoxLayout;
        const auto add = [&](const std::string & name, const auto value)
        {
            HBoxLayoutPtr hbox = new HBoxLayout;
            vbox->children.emplace_back(hbox);
            //val
            {
                hbox->children.emplace_back(new Label(false, name));
                FloatSliderPtr slider = new FloatSlider;
                slider->name = name + "_val";
                slider->min = 0;
                slider->defaultValue = value;
                slider->max = 1;
                hbox->children.emplace_back(slider);
            }
            //pwm
            {
                hbox->children.emplace_back(new Label(false, "max pwm"));
                FloatSliderPtr slider = new FloatSlider;
                slider->name = name + "_pwm";
                slider->min = 0;
                slider->defaultValue = 1;
                slider->max = 1;
                hbox->children.emplace_back(slider);
            }
        };
        add("other", getOtherJointValue());
        add("index", getIndexJointValue());
        add("thumb", getThumbJointValue());
        return {{"Targets", vbox}};

    }

    void NJointShapeController::callDescribedFunction(const std::string& name, const StringVariantBaseMap& valueMap, const Ice::Current&)
    {
        if (name == "Targets")
        {
            setTargetsWithPwm(
                valueMap.at("other_val")->getFloat(),
                valueMap.at("index_val")->getFloat(),
                valueMap.at("thumb_val")->getFloat(),

                valueMap.at("other_pwm")->getFloat(),
                valueMap.at("index_pwm")->getFloat(),
                valueMap.at("thumb_pwm")->getFloat());
        }
        else
        {
            ARMARX_WARNING << "Unknown function name called: " << name;
        }
    }
}
