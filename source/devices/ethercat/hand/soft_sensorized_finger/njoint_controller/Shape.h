#pragma once

#include <atomic>
#include "ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h"
#include "ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h"

#include "RobotAPI/interface/units/RobotUnit/RobotUnitInterface.h"
#include "RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h"
#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>

#include "robot_devices/ethercat/hand/soft_sensorized_finger/sensor_value/Motor.h"
#include <devices/ethercat/hand/soft_sensorized_finger/njoint_controller/Shape.h>

#include "../joint_controller/Pwm.h"

namespace devices::ethercat::hand::soft_sensorized_finger::njoint_controller
{
    using NJointShapeControllerConfigPtr = IceUtil::Handle<class NJointShapeControllerConfig>;

    class NJointShapeControllerConfig : virtual public armarx::NJointControllerConfig
    {
    public:
        NJointShapeControllerConfig(std::string const& deviceName):
            deviceName(deviceName)
        {}

        const std::string deviceName;
    };

    class NJointShapeController;
    using NJointShapeControllerPtr = IceInternal::Handle<NJointShapeController>;

    class NJointShapeController :
        public armarx::NJointController,
        public armarx::NJointControllerInterface
    {
    public:
        using ConfigPtrT = NJointShapeControllerConfigPtr;
        NJointShapeController(
            armarx::RobotUnitPtr prov,
            NJointShapeControllerConfigPtr config,
            const VirtualRobot::RobotPtr& r);

        // NJointControllerInterface interface
    public:
        std::string getClassName(const Ice::Current&) const override
        {
            return "NJointKITSensorizedSoftFingerHandV1ShapeController";
        }

        void stopMotion();

        void readConfig(
            const armarx::RapidXmlReaderNode& configNode,
            armarx::DefaultRapidXmlReaderNode defaultConfigurationNod);

        int16_t getOtherPwm();
        int16_t getIndexPwm();
        int16_t getThumbPwm();

        float getOtherTarget() const;
        float getIndexTarget() const;
        float getThumbTarget() const;

        float getOtherJointValue() const;
        float getIndexJointValue() const;
        float getThumbJointValue() const;

        bool isControlEnabled() const;

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

        //interface
    public:
        armarx::WidgetDescription::StringWidgetDictionary getFunctionDescriptions(const Ice::Current&) const override;
        void callDescribedFunction(const std::string& name, const armarx::StringVariantBaseMap& valueMap, const Ice::Current&) override;

        void setTargets(float lrm, float index, float thumb,
                        const Ice::Current& = Ice::emptyCurrent) override;
        void setTargetsWithPwm(float lrm, float index, float thumb,
                               float lrmRelativePWM, float indexRelativePWM, float thumbRelativePWM,
                               const Ice::Current& = Ice::emptyCurrent) override;
        JointValues getJointValues(const Ice::Current&) const override;
    private:
        struct finger_data
        {
            const MotorSensorValue* motor_sensor;
            armarx::ControlTarget1DoFActuatorPWM*                        pwm_target;
            std::atomic<float>                                   joint_value;
            std::atomic<int16_t>                                 last_pwm;
            int16_t                                              pwm_limit = 0;
            float                                                Kp        = 0;
            float                                                Ki        = 0;
            float                                                Kd        = 0;
        };
        finger_data _other_data;
        finger_data _index_data;
        finger_data _thumb_data;

        struct fingers_target_data
        {
            struct target_data
            {
                int16_t max_pwm          = 0;
                float   target           = 0;
            };
            target_data index;
            target_data thumb;
            target_data other;
            bool enable_control = false;
        };
        mutable std::recursive_mutex                   _target_buf_write_mutex;
        armarx::WriteBufferedTripleBuffer<fingers_target_data> _target_buf;
        mutable std::recursive_mutex                   _last_target_buf_read_mutex;
        armarx::TripleBuffer<fingers_target_data>              _last_target_buf;

    };

}  // namespace devices::ethercat::hand::soft_sensorized_finger::njoint_controller
