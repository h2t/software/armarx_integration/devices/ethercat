#pragma once

#include <RobotAPI/interface/units/RobotUnit/NJointController.ice>

module armarx
{
    module KITSensorizedSoftFingerHand
	{
		module V1
		{
		    struct JointValues
		    {
			float thumbJointValue;
			float indexJointValue;
			float otherJointValue;
		    };

		    interface NJointShapeControllerInterface extends NJointControllerInterface
		    {
			void setTargets(float lrm, float index, float thumb);
			void setTargetsWithPwm(float lrm, float index, float thumb, float lrmRelativePWM, float indexRelativePWM, float thumbRelativePWM);
                        ["cpp:const"] JointValues getJointValues();
		    };
		};
	};
};
