#pragma once

#include "MeanFreeSensor.h"

namespace devices::ethercat::hand::soft_sensorized_finger::njoint_controller
{
    using NormalForceSensor = MeanFreeValue<uint32_t>;
}
