#pragma once

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/libraries/core/PIDController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>

#include "../bus_data/Motor.h"
#include "../FunctionalDevice.h"
#include "../controller/VelocityToPWM.h"
#include "../joint_controller/Pwm.h"
#include "robot_devices/ethercat/hand/soft_sensorized_finger/joint_controller/Pwm.h"

namespace devices::ethercat::hand::soft_sensorized_finger::joint_controller
{
    class VelocityController : public armarx::JointController
    {
    public:
        VelocityController(
            bus_data::MotorControlDataPtr data,
            FunctionalDevicePtr hand,
            armarx::ControlTargetBase::ControlDeviceAccessToken token);
        armarx::ControlTargetBase* getControlTarget() override;
        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;
        void rtPreActivateController() override;
        void rtPostDeactivateController() override;
    private:
        bus_data::MotorControlDataPtr                           data;
        FunctionalDevicePtr                           hand;
        armarx::ControlTarget1DoFActuatorVelocityWithPWMLimit target;
        controller::VelocityToPWM                    ctrl;
        std::int16_t                                  last_pwm;
    };
    using VelocityControllerPtr =
        std::shared_ptr<VelocityController>;
}
