#include "StopMovement.h"

namespace devices::ethercat::hand::soft_sensorized_finger::joint_controller
{
    StopMovementController::StopMovementController(
        bus_data::MotorControlDataPtr dataPtr) :
        dataPtr(dataPtr)
    {}

    void StopMovementController::rtRun(
        const IceUtil::Time&, const IceUtil::Time&)
    {
        dataPtr->setCommand(0);
    }

    armarx::ControlTargetBase* StopMovementController::getControlTarget()
    {
        return &target;
    }

    void StopMovementController::rtPreActivateController()
    {
        dataPtr->setCommand(0);
    }
}
