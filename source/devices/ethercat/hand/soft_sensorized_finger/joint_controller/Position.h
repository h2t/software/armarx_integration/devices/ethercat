#pragma once

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>

#include "../bus_data/Motor.h"
#include "../FunctionalDevice.h"
#include "../controller/PositionToPWM.h"
#include "../joint_controller/Pwm.h"

namespace devices::ethercat::hand::soft_sensorized_finger::joint_controller
{
    class PositionController : public armarx::JointController
    {
    public:
        PositionController(
            bus_data::MotorControlDataPtr data,
            FunctionalDevicePtr hand,
            armarx::ControlTargetBase::ControlDeviceAccessToken token);
        armarx::ControlTargetBase* getControlTarget() override;
        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;
        void rtPreActivateController() override;
        void rtPostDeactivateController() override;

    private:
        bus_data::MotorControlDataPtr        data;
        FunctionalDevicePtr                        hand;
        armarx::ControlTarget1DoFActuatorPositionWithPWMLimit           target;
        controller::PositionToPWM ctrl;
        std::int16_t                                            last_pwm;
    };

    using PositionControllerPtr =
        std::shared_ptr<PositionController>;
}  // namespace devices::ethercat::hand::soft_sensorized_finger::joint_controller
