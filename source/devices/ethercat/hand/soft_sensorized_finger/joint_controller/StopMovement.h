#pragma once

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>

#include "robot_devices/ethercat/hand/soft_sensorized_finger/bus_data/Motor.h"

namespace devices::ethercat::hand::soft_sensorized_finger::joint_controller
{
    class StopMovementController : public armarx::JointController
    {
    public:
        StopMovementController(
            bus_data::MotorControlDataPtr dataPtr);
        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;
        armarx::ControlTargetBase* getControlTarget() override;
        void rtPreActivateController() override;
    private:
        armarx::DummyControlTargetStopMovement target;
        bus_data::MotorControlDataPtr dataPtr;
    };

    using StopMovementControllerPtr =
        std::shared_ptr<StopMovementController>;
}
