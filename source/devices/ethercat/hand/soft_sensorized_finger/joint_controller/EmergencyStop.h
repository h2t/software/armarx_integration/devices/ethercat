#pragma once

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>

#include "../bus_data/Motor.h"

namespace devices::ethercat::hand::soft_sensorized_finger::joint_controller
{
    class EmergencyStopController : public armarx::JointController
    {
    public:
        EmergencyStopController(
            bus_data::MotorControlDataPtr dataPtr);
    private:
        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        armarx::ControlTargetBase* getControlTarget() override;

        void rtPreActivateController() override;
    private:
        armarx::DummyControlTargetEmergencyStop target;
        bus_data::MotorControlDataPtr dataPtr;
    };

    using JointEmergencyStopControllerPtr =
        std::shared_ptr<EmergencyStopController>;

}  // namespace devices::ethercat::hand::soft_sensorized_finger::joint_controller
