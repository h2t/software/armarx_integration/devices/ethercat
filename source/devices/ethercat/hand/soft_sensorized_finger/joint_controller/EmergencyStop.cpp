#include "EmergencyStop.h"

namespace devices::ethercat::hand::soft_sensorized_finger::joint_controller
{
    EmergencyStopController::EmergencyStopController(
        bus_data::MotorControlDataPtr dataPtr) :
        dataPtr(dataPtr)
    {}
    void EmergencyStopController::rtRun(
        const IceUtil::Time&, const IceUtil::Time&)
    {
        dataPtr->setCommand(0);
    }

    armarx::ControlTargetBase* EmergencyStopController::getControlTarget()
    {
        return &target;
    }

    void EmergencyStopController::rtPreActivateController()
    {
        dataPtr->setCommand(0);
    }
}
