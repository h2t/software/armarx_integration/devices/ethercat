#include "Pwm.h"

namespace devices::ethercat::hand::soft_sensorized_finger::joint_controller
{
    PwmController::PwmController(
        bus_data::MotorControlDataPtr data,
        FunctionalDevicePtr hand) :
        data(data),
        hand(hand)
    {
        ARMARX_CHECK_NOT_NULL(data);
        ARMARX_CHECK_NOT_NULL(hand);
    }

    armarx::ControlTargetBase* PwmController::getControlTarget()
    {
        return &target;
    }

    void PwmController::rtRun(
        const IceUtil::Time&, const IceUtil::Time&)
    {
        data->setCommand(target.pwm);
    }

    void PwmController::rtPreActivateController()
    {}

    void PwmController::rtPostDeactivateController()
    {
        data->setCommand(0);
    }
}
