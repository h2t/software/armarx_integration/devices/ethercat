#include <RobotAPI/libraries/core/math/MathUtils.h>

#include "../FunctionalDevice.h"
#include "Position.h"

namespace devices::ethercat::hand::soft_sensorized_finger::joint_controller
{
    PositionController::PositionController(
        bus_data::MotorControlDataPtr data,
        FunctionalDevicePtr hand,
        armarx::ControlTargetBase::ControlDeviceAccessToken token) :
        data(data),
        hand(hand),
        ctrl(data->getPosCtrlKp(), data->getPosCtrlKi(), data->getPosCtrlKd())
    {
        ARMARX_CHECK_NOT_NULL(data);
        ARMARX_CHECK_NOT_NULL(hand);
        target.setPWMLimits(data->getMaxPWM(), data->getPosCtrlDefaultMaxPWM(), token);
    }

    armarx::ControlTargetBase* PositionController::getControlTarget()
    {
        return &target;
    }

    void PositionController::rtRun(
        const IceUtil::Time& /*sensorValuesTimestamp*/,
        const IceUtil::Time& timeSinceLastIteration)
    {
        ARMARX_CHECK_EXPRESSION(target.isValid());
        last_pwm = ctrl.calculate(
                       data->getRelativeEncoderValue(),
                       target.position,
                       timeSinceLastIteration.toSecondsDouble(),
                       std::min<std::int16_t>(data->getMaxPWM(), target.maxPWM)
                   );
        data->setCommand(last_pwm);
    }

    void PositionController::rtPreActivateController()
    {
        ctrl.reset();
    }

    void PositionController::rtPostDeactivateController()
    {
        data->setCommand(last_pwm);
    }
}
