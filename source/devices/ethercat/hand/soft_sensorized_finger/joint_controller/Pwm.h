#pragma once

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>

#include "../bus_data/Motor.h"
#include "../FunctionalDevice.h"

namespace devices::ethercat::hand::soft_sensorized_finger::joint_controller
{
    class PwmController : public armarx::JointController
    {
    public:
        PwmController(
            bus_data::MotorControlDataPtr data,
            FunctionalDevicePtr hand);
        armarx::ControlTargetBase* getControlTarget() override;
        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;
        void rtPreActivateController() override;
        void rtPostDeactivateController() override;
    private:
        bus_data::MotorControlDataPtr data;
        FunctionalDevicePtr hand;
        armarx::ControlTarget1DoFActuatorPWM target;
    };
    using JointPwmControllerPtr = std::shared_ptr<PwmController>;
}
