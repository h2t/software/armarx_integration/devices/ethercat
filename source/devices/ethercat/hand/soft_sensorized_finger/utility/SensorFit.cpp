#include "SensorFit.h"

namespace Eigen
{
    using Vector5d = Matrix<double, 5, 1>;
}

namespace devices::ethercat::hand::soft_sensorized_finger
{
    using JENC = SensorFit::JointEncoder;
    Eigen::Vector3f JENC::thumb_prox_fit_joint_angle_data_1_poly(
        float x, float y, float z)
    {
        static const Eigen::Vector4d cx
        {
            -3.0149985997869457e+001,
                +3.0128853346157572e-002,
                -3.2319616750818926e-006,
                +1.1794986395837135e-010
            };
        static const Eigen::Vector4d cy
        {
            -3.2882006618854462e+001,
                -3.7483225185537128e-001,
                -4.9745054331382045e-004,
                -2.2910312387766286e-007
            };
        static const Eigen::Vector4d cz
        {
            -1.8987002102600119e+001,
                -3.4080416074681805e-002,
                -4.6260163969121740e-006,
                -2.1003723936766469e-010
            };
        return
        {
            calc_poly(cx, x),
            calc_poly(cy, y),
            calc_poly(cz, z)
        };
    }

    Eigen::Vector3f JENC::thumb_prox_fit_joint_angle_data_2_poly(
        float x, float y, float z)
    {
        static const Eigen::Vector5d cx = []
        {
            Eigen::Vector5d v;
            v << -5.5568017768730435e+001,
              +6.8157052557135417e-002,
              -1.7125128969521229e-005,
              +2.1952213287357551e-009,
              -1.3510392732948656e-013,
              +3.1680494194422713e-018;
            return v;
        }();
        static const Eigen::Vector5d cy = []
        {
            Eigen::Vector5d v;
            v << +1.7150983498520592e+001,
              +3.0178399682480750e-001,
              -5.1024537014353864e-004,
              +1.8904540254959379e-006,
              -7.4790300984104864e-009,
              +8.8962636945820215e-012;
            return v;
        }();
        static const Eigen::Vector5d cz = []
        {
            Eigen::Vector5d v;
            v << -3.8214284045175326e+001,
              - 6.8665155192354554e-002,
              - 2.0092443959013517e-005,
              - 2.9100269843583112e-009,
              - 1.9795457264104989e-013,
              - 5.0630697241822937e-018;
            return v;
        }();
        return
        {
            calc_poly(cx, x),
            calc_poly(cy, y),
            calc_poly(cz, z)
        };
    }
}

namespace devices::ethercat::hand::soft_sensorized_finger
{
    using JENC = SensorFit::JointEncoder;
    Eigen::Vector3f JENC::data_3_fit_joint_angle_pwlin_thumb_prox(std::int64_t x, std::int64_t y, std::int64_t z)
    {
        static const std::array<piecewise_linear_function<std::int64_t, float>, 3> fit = []
        {
            const std::map<std::int64_t, float> vals_thumb_prox_x
            {
                {966,  -4.80f},
                {998,  -3.60f},
                {1058,  -1.10f},
                {1158,   3.80f},
                {1272,   8.10f},
                {1353,  11.10f},
                {1468,  14.90f},
                {1580,  18.40f},
                {1717,  21.90f},
                {1901,  26.10f},
                {2140,  30.20f},
                {2425,  35.50f},
                {2922,  41.80f},
                {3529,  47.60f},
                {4523,  55.30f},
                {5849,  60.80f},
                {7855,  68.20f},
                {8770,  70.90f},
                {9420,  72.20f},
                {11343,  75.80f},
                {13091,  77.80f},
                {14162,  80.00f}
            };
            const std::map<std::int64_t, float> vals_thumb_prox_y
            {
                {-67, -4.80f},
                {-57, -3.60f},
                {-55, -1.10f},
                {-32,  3.80f},
                {-24,  8.10f},
                {-28, 11.10f},
                {-8, 14.90f},
                {-2, 18.40f},
                {14, 21.90f},
                {44, 26.10f},
                {37, 30.20f},
                {73, 35.50f},
                {89, 41.80f},
                {119, 47.60f},
                {146, 55.30f},
                {195, 60.80f},
                {238, 68.20f},
                {288, 70.90f},
                {323, 72.20f},
                {389, 75.80f},
                {416, 77.80f},
                {308, 80.00f}
            };
            const std::map<std::int64_t, float> vals_thumb_prox_z
            {
                {-627, -4.80f},
                {-665, -3.60f},
                {-687, -1.10f},
                {-749,  3.80f},
                {-840,  8.10f},
                {-903, 11.10f},
                {-993, 14.90f},
                {-1093, 18.40f},
                {-1208, 21.90f},
                {-1366, 26.10f},
                {-1585, 30.20f},
                {-1845, 35.50f},
                {-2316, 41.80f},
                {-2887, 47.60f},
                {-3863, 55.30f},
                {-5210, 60.80f},
                {-7310, 68.20f},
                {-8155, 70.90f},
                {-8909, 72.20f},
                {-10828, 75.80f},
                {-12380, 77.80f},
                {-13366, 80.00f}
            };
            std::array<piecewise_linear_function<std::int64_t, float>, 3> f;
            f.at(0).reset(vals_thumb_prox_x);
            f.at(1).reset(vals_thumb_prox_y);
            f.at(2).reset(vals_thumb_prox_z);
            return f;
        }();
        return
        {
            fit.at(0).calc_lin_search_with_cache(x),
            fit.at(1).calc_lin_search_with_cache(y),
            fit.at(2).calc_lin_search_with_cache(z)
        };
    }
    Eigen::Vector3f JENC::data_3_fit_joint_angle_pwlin_little_dist(std::int64_t x, std::int64_t y, std::int64_t z)
    {
        static const std::array<piecewise_linear_function<std::int64_t, float>, 3> fit = []
        {
            static const std::map<std::int64_t, float> vals_little_dist_x
            {
                {943,  1.20f},
                {1163, 10.20f},
                {1256, 12.20f},
                {1351, 17.70f},
                {1506, 21.50f},
                {1798, 27.70f},
                {2242, 34.50f},
                {2716, 41.20f},
                {3202, 45.30f},
                {4075, 52.10f},
                {5203, 59.00f},
                {6631, 64.90f},
                {8769, 71.80f},
                {10550, 75.40f},
                {13041, 79.20f}
            };
            static const std::map<std::int64_t, float> vals_little_dist_y
            {
                {-188,  1.20},
                {-206, 10.20},
                {-219, 12.20},
                {-219, 17.70},
                {-225, 21.50},
                {-209, 27.70},
                {-194, 34.50},
                {-211, 41.20},
                {-221, 45.30},
                {-304, 52.10},
                {-491, 59.00},
                {-753, 64.90},
                {-1088, 71.80},
                {-1432, 75.40},
                {-1948, 79.20}
            };
            static const std::map<std::int64_t, float> vals_little_dist_z
            {
                { -496,  1.20f},
                { -564, 10.20f},
                { -597, 12.20f},
                { -612, 17.70f},
                { -653, 21.50f},
                { -721, 27.70f},
                { -836, 34.50f},
                { -971, 41.20f},
                { -1106, 45.30f},
                { -1396, 52.10f},
                { -1807, 59.00f},
                { -2406, 64.90f},
                { -3567, 71.80f},
                { -4649, 75.40f},
                { -6236, 79.20f}
            };

            std::array<piecewise_linear_function<std::int64_t, float>, 3> f;
            f.at(0).reset(vals_little_dist_x);
            f.at(1).reset(vals_little_dist_y);
            f.at(2).reset(vals_little_dist_z);
            return f;
        }();
        return
        {
            fit.at(0).calc_lin_search_with_cache(x),
            fit.at(1).calc_lin_search_with_cache(y),
            fit.at(2).calc_lin_search_with_cache(z)
        };
    }
}
