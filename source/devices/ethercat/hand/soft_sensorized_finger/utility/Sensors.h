/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    robot_devices::ArmarXObjects::KITSensorizedSoftFingerHandV1NJointController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>

#include <devices/ethercat/hand/soft_sensorized_finger/sensor_value/Motor.h>
#include <devices/ethercat/hand/soft_sensorized_finger/sensor_value/Finger.h>
#include <devices/ethercat/hand/soft_sensorized_finger/sensor_value/Hand.h>

#include "Identities.h"

namespace devices::ethercat::hand::soft_sensorized_finger
{
    struct Sensors
    {
        using MotorIdentity  = identity::MotorIdentity;
        using FingerIdentity = identity::FingerIdentity;

        identity::MotorArray<const MotorSensorValue*> motor;

        struct Finger
        {
            const FingerSensorValue* sensor = nullptr;
            const FingerSensorValue* operator->() const
            {
                return sensor;
            }
            const FingerSensorValue& operator*() const
            {
                return *sensor;
            }
            template<class T>
            auto fit_joint_encoder_dist(T fnc, bool invert_values)
            {
                std::int16_t factor = invert_values ? -1 : 1;
                return fnc(factor * sensor->raw_joint_encoder_dist.at(0),
                           factor * sensor->raw_joint_encoder_dist.at(1),
                           factor * sensor->raw_joint_encoder_dist.at(2));
            }
            template<class T>
            auto fit_joint_encoder_prox(T fnc, bool invert_values)
            {
                std::int16_t factor = invert_values ? -1 : 1;
                return fnc(factor * sensor->raw_joint_encoder_prox.at(0),
                           factor * sensor->raw_joint_encoder_prox.at(1),
                           factor * sensor->raw_joint_encoder_prox.at(2));
            }
        };

        identity::FingerArray<Finger> finger;

        const HandSensorValue* hand = nullptr;

        Sensors() = default;
        Sensors(Sensors&&) = default;
        Sensors(const Sensors&) = default;
        Sensors& operator=(Sensors&&) = default;
        Sensors& operator=(const Sensors&) = default;

        Sensors(const std::string& hand_name, armarx::NJointController& parent)
        {
            init(parent, hand_name, hand);
            init(parent, hand_name + ".motor.thumb",   motor.thumb());
            init(parent, hand_name + ".motor.index",   motor.index());
            init(parent, hand_name + ".motor.other",   motor.other());
            init(parent, hand_name + ".finger.thumb",  finger.thumb().sensor);
            init(parent, hand_name + ".finger.index",  finger.index().sensor);
            init(parent, hand_name + ".finger.middle", finger.middle().sensor);
            init(parent, hand_name + ".finger.ring",   finger.ring().sensor);
            init(parent, hand_name + ".finger.little", finger.little().sensor);
        }
    private:
        template<class T>
        void init(armarx::NJointController& parent, const std::string& name, T const*& ptr)
        {
            const armrax::SensorValueBase* sv = parent.useSensorValue(name);
            ARMARX_CHECK_NOT_NULL(sv) << VAROUT(name);
            ptr = sv->asA<T>();
            ARMARX_CHECK_NOT_NULL(ptr) << VAROUT(name);
        }
    };

}
