/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    robot_devices::ArmarXObjects::KITSensorizedSoftFingerHandV1NJointController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Eigen/Core>

#include <vector>
#include <map>
#include <limits>
#include <algorithm>

namespace devices::ethercat::hand::soft_sensorized_finger
{
    template<class X, class Y>
    struct piecewise_linear_function_entry
    {
        piecewise_linear_function_entry(X lower_bound = 0, Y a = 0, Y b = 0) :
            lower_bound{lower_bound}, a{a}, b{b}
        {}
        X lower_bound = 0;
        Y a           = 0;
        Y b           = 0;
    };

    template<class X, class Y>
    bool operator<(const X l, const piecewise_linear_function_entry<X, Y>& r)
    {
        return l < r.lower_bound;
    }
    template<class X, class Y>
    bool operator<(const piecewise_linear_function_entry<X, Y> l, const piecewise_linear_function_entry<X, Y>& r)
    {
        return l.lower_bound < r.lower_bound;
    }
    template<class X, class Y>
    bool operator<(const piecewise_linear_function_entry<X, Y> l, const X r)
    {
        return l.lower_bound < r;
    }

    template<class X, class Y>
    class piecewise_linear_function
    {
        //without   local:  lin < 64   ->  bin
        //with (16) local:  lin < 1024 ->  bin
    private:
        std::vector<piecewise_linear_function_entry<X, Y>> _coefficients;
        mutable std::size_t _last;
    public:
        piecewise_linear_function() = default;
        piecewise_linear_function(const std::map<X, Y>& values)
        {
            reset(values);
        }
        piecewise_linear_function(std::initializer_list<std::pair<const X, Y>> values) :
            piecewise_linear_function(std::map<X, Y>(std::move(values)))
        {
        }

        void reset(const std::map<X, Y>& points)
        {
            static constexpr X minimal =
                std::is_floating_point_v<X> ?
                -std::numeric_limits<X>::infinity() :
                std::numeric_limits<X>::lowest();
            _coefficients.clear();
            _last = 0;
            switch (points.size())
            {
                case 0:
                    throw std::invalid_argument{"no points given for piecewise_linear_function"};
                case 1:
                {
                    _coefficients.emplace_back(minimal, static_cast<Y>(0), points.begin()->second);
                }
                break;
                default:
                {
                    _coefficients.reserve(points.size() - 1);
                    auto i0 = points.begin();
                    auto i1 = points.begin();
                    ++i1;
                    for (; i1 != points.end(); i0 = i1, ++i1)
                    {
                        Y a = (i1->second - i0->second) / (static_cast<Y>(i1->first) - static_cast<Y>(i0->first));
                        Y b = i0->second - static_cast<Y>(i0->first) * a;
                        _coefficients.emplace_back(i0->first, a, b);
                    }
                    _coefficients.front().lower_bound = minimal;
                }
            }
        }
        void reset(std::initializer_list<std::pair<const X, Y>> l)
        {
            reset(std::map<X, Y>(std::move(l)));
        }

    private:
        std::size_t bin_search(auto f, auto l, X x) const
        {
            auto it = std::upper_bound(f, l, x);
            //cant be begin, since begin is min
            if (it == _coefficients.end())
            {
                return _coefficients.size() - 1;
            }
            return std::distance(_coefficients.begin(), it) - 1;
        }
    public:
        Y calc_at(X x, std::size_t i) const
        {
            const auto& coeff = _coefficients.at(i);
            return coeff.a * static_cast<Y>(x) + coeff.b;
        }
        Y calc_lin_search(X x) const
        {
            std::size_t i = 0;
            for (; i + 1 < _coefficients.size(); ++i)
            {
                if (_coefficients.at(i + 1).lower_bound > x)
                {
                    break;
                }
            }
            return calc_at(x, i);
        }
        Y calc_lin_search_with_cache(X x) const
        {
            if (_last + 1 < _coefficients.size())
            {
                //not last
                if (x < _coefficients.at(_last).lower_bound)
                {
                    //search down
                    for (; x < _coefficients.at(_last).lower_bound; --_last);
                }
                else
                {
                    //search up
                    for (; _last + 1 < _coefficients.size() && x > _coefficients.at(_last + 1).lower_bound ; ++_last);
                }
            }
            else
            {
                //last
                for (; x < _coefficients.at(_last).lower_bound; --_last);
            }
            return calc_at(x, _last);
        }
        Y calc_bin_search(X x) const
        {
            return calc_at(x, bin_search(_coefficients.begin(), _coefficients.end(), x));
        }
        Y calc_bin_search_with_cache(X x) const
        {
            if (_last + 1 < _coefficients.size())
            {
                //not last
                if (x < _coefficients.at(_last).lower_bound)
                {
                    //search down
                    _last = bin_search(_coefficients.begin(), _coefficients.begin() + _last, x);
                }
                else
                {
                    _last = bin_search(_coefficients.begin() + _last, _coefficients.end(), x);
                }
            }
            else
            {
                if (x < _coefficients.back().lower_bound)
                {
                    _last = bin_search(_coefficients.begin(), --_coefficients.end(), x);
                }
            }
            return calc_at(x, _last);
        }
    };
}

namespace devices::ethercat::hand::soft_sensorized_finger
{
    inline float calc_poly(const auto& coeffs, double v)
    {
        double acc = 0;
        for (int i = 0; i <  coeffs.cols(); ++i)
        {
            acc += std::pow(v, i) * coeffs(i);
        }
        return static_cast<float>(acc);
    }
    struct SensorFit
    {
        struct JointEncoder
        {
            static Eigen::Vector3f thumb_prox_fit_joint_angle_data_1_poly(float x, float y, float z);
            static Eigen::Vector3f thumb_prox_fit_joint_angle_data_2_poly(float x, float y, float z);

            static Eigen::Vector3f data_3_fit_joint_angle_pwlin_thumb_prox(std::int64_t x, std::int64_t y, std::int64_t z);
            static Eigen::Vector3f data_3_fit_joint_angle_pwlin_little_dist(std::int64_t x, std::int64_t y, std::int64_t z);
        };
    };
}
