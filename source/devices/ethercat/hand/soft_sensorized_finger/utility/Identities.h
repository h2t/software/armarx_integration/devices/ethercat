/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    robot_devices::ArmarXObjects::KITSensorizedSoftFingerHandV1NJointController
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

namespace devices::ethercat::hand::soft_sensorized_finger::identity
{
    enum class MotorIdentity : std::size_t
    {
        index = 0,
        other = 1,
        thumb = 2
    };
    template<class T>
    static decltype(auto) Select(MotorIdentity id,
                                 T& other,
                                 T& index,
                                 T& thumb)
    {
        switch (id)
        {
            case MotorIdentity::index:
                return index;
            case MotorIdentity::other:
                return other;
            case MotorIdentity::thumb:
                return thumb;
        };
        ARMARX_CHECK_EXPRESSION(!"unreachable code!");
    }
    enum class FingerIdentity : std::size_t
    {
        little = 0,
        ring   = 1,
        middle = 2,
        index  = 3,
        thumb  = 4
    };
    template<class T>
    static decltype(auto) Select(FingerIdentity id,
                                 T& little,
                                 T& ring,
                                 T& middle,
                                 T& index,
                                 T& thumb)
    {
        switch (id)
        {
            case FingerIdentity::thumb:
                return thumb;
            case FingerIdentity::index:
                return index;
            case FingerIdentity::middle:
                return middle;
            case FingerIdentity::ring:
                return ring;
            case FingerIdentity::little:
                return little;
        };
        ARMARX_CHECK_EXPRESSION(!"unreachable code!");
    }

    template<class ET, class VT, std::size_t N>
    class EnumIndexedArray : public std::array<VT, N>
    {
    public:
        using enum_t = ET;
        using value_t = VT;

        using std::array<VT, N>::at;

        value_t& at(enum_t id)
        {
            return at(static_cast<std::size_t>(id));
        }
        const value_t& at(enum_t id) const
        {
            return at(static_cast<std::size_t>(id));
        }

        const value_t& operator()(enum_t id) const
        {
            return at(id);
        }
        value_t& operator()(enum_t id)
        {
            return at(id);
        }
    };

    template<class T>
    class FingerArray : public EnumIndexedArray<FingerIdentity, T, 5>
    {
    public:
        // *INDENT-OFF*
              T& little()       { return this->at(FingerIdentity::little); }
        const T& little() const { return this->at(FingerIdentity::little); }

              T& ring  ()       { return this->at(FingerIdentity::ring  ); }
        const T& ring  () const { return this->at(FingerIdentity::ring  ); }

              T& middle()       { return this->at(FingerIdentity::middle); }
        const T& middle() const { return this->at(FingerIdentity::middle); }

              T& index ()       { return this->at(FingerIdentity::index ); }
        const T& index () const { return this->at(FingerIdentity::index ); }

              T& thumb ()       { return this->at(FingerIdentity::thumb ); }
        const T& thumb () const { return this->at(FingerIdentity::thumb ); }
        // *INDENT-ON*
    };

    template<class T>
    class MotorArray : public EnumIndexedArray<MotorIdentity, T, 3>
    {
    public:
        // *INDENT-OFF*
              T& other()       { return this->at(MotorIdentity::other); }
        const T& other() const { return this->at(MotorIdentity::other); }

              T& index()       { return this->at(MotorIdentity::index); }
        const T& index() const { return this->at(MotorIdentity::index); }

              T& thumb()       { return this->at(MotorIdentity::thumb); }
        const T& thumb() const { return this->at(MotorIdentity::thumb); }
        // *INDENT-ON*
    };
}
