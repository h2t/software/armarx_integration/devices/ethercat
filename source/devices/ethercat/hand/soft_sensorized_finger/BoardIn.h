#pragma once


namespace devices::ethercat::hand::soft_sensorized_finger
{
    struct BoardIn_motor_target
    {
        int16_t pwm;
    } __attribute__((__packed__));

    /// @brief PDO mapping master->sensorB
    struct BoardIn
    {
        BoardIn_motor_target thumb;
        BoardIn_motor_target index;
        BoardIn_motor_target other;
        uint16_t             padding;
    } __attribute__((__packed__));

    static_assert(sizeof(BoardIn) == (64) / 8);

}  // namespace devices::ethercat::hand::soft_sensorized_finger
