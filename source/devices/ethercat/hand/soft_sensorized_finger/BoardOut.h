﻿#pragma once

#include <array>

namespace devices::ethercat::hand::soft_sensorized_finger
{
    struct BoardOut_joint_encoder_value
    {
        std::array<std::int16_t, 3> xyz;
        std::uint16_t               temperature;
    } __attribute__((__packed__));

    struct BoardOut_pressure_value
    {
        std::uint32_t value;
        std::int16_t temperature;
        std::int16_t  padding;
    } __attribute__((__packed__));

    struct BoardOut_shear_force_value
    {
        std::array<std::int16_t, 3> xyz;
        std::uint16_t               temperature;
    } __attribute__((__packed__));

    struct BoardOut_proximity_value
    {
        std::uint16_t value;
        std::int16_t  padding;
    } __attribute__((__packed__));

    struct BoardOut_accelerometer_value
    {
        std::uint16_t fifo_length;
        std::array<std::array<std::int16_t, 3>, 17> fifo;
    } __attribute__((__packed__));

    struct BoardOut_finger_value
    {
        BoardOut_joint_encoder_value joint_encoder_prox;
        BoardOut_joint_encoder_value joint_encoder_dist;

        BoardOut_pressure_value      pressure_prox_l;
        BoardOut_pressure_value      pressure_prox_r;
        BoardOut_pressure_value      pressure_dist_joint;
        BoardOut_pressure_value      pressure_dist_tip;

        BoardOut_shear_force_value   shear_force_dist_joint;
        BoardOut_shear_force_value   shear_force_dist_tip;

        BoardOut_proximity_value     proximity;

        BoardOut_accelerometer_value accelerometer;
    } __attribute__((__packed__));

    struct BoardOut_motor_value
    {
        std::int32_t           relative_position;
        std::int32_t           velocity;
    } __attribute__((__packed__));

    /// @brief PDO mapping sensorB->master
    struct BoardOut
    {
        BoardOut_motor_value  motor_thumb;
        BoardOut_motor_value  motor_index;
        BoardOut_motor_value  motor_other;

        std::uint32_t         temperature;

        std::uint32_t         sensor_frame_counter;
        std::uint16_t         update_rate_main;
        std::uint16_t         update_rate_fpga;

        BoardOut_finger_value little;
        BoardOut_finger_value ring;
        BoardOut_finger_value middle;
        BoardOut_finger_value index;
        BoardOut_finger_value thumb;

    } __attribute__((__packed__));

    static_assert(sizeof(BoardOut) == (7168) / 8);

}  // namespace devices::ethercat::hand::soft_sensorized_finger
