#pragma once

#include "VelocityToPWM.h"

namespace devices::ethercat::hand::soft_sensorized_finger::controller
{
    struct CloseAndHoldParams
    {
        float p = 0;
        float i = 0;
        float d = 0;
        float holding_start_force_threshold = 0;
        float holding_stop_force_threshold  = 0;
    };
    struct CloseAndHold
    {
        CloseAndHold(const CloseAndHoldParams& p);

        CloseAndHold() = default;
        CloseAndHold(CloseAndHold&&) = default;
        CloseAndHold(const CloseAndHold&) = default;
        CloseAndHold& operator=(CloseAndHold&&) = default;
        CloseAndHold& operator=(const CloseAndHold&) = default;

        void reset(float pos);
        std::int16_t calculate(
            float force,
            float curr_pos,
            float velocity,
            float dt,
            std::int16_t maxPWM);

        enum class HoldingMode
        {
            pwm,
            velocity
        };

        struct Configuration
        {
            float holding_start_force_threshold = 0;
            float holding_stop_force_threshold  = 0;

            HoldingMode holding_mode = HoldingMode::velocity;

            float holding_velocity = 0;
            std::int16_t holding_pwm = 100;
        };
        Configuration cfg;
        struct State
        {
            VelocityToPWM vel_ctrl;
            bool          holding                       = false;
            std::int16_t  last_pwm                      = 0;
        };
        State state;
    };
}
