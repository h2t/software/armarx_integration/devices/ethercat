#include <boost/algorithm/clamp.hpp>

#include "CloseAndHold.h"

namespace devices::ethercat::hand::soft_sensorized_finger::controller
{
    CloseAndHold::CloseAndHold(const CloseAndHoldParams& p)
    {

        cfg.holding_start_force_threshold = p.holding_start_force_threshold;
        cfg.holding_stop_force_threshold  = p.holding_stop_force_threshold;
        state.vel_ctrl = VelocityToPWM{p.p, p.i, p.d};
    }

    void CloseAndHold::reset(float pos)
    {
        state.vel_ctrl.reset(pos);
    }

    int16_t CloseAndHold::calculate(
        float force,
        float curr_pos,
        float velocity,
        float dt,
        std::int16_t maxPWM)
    {
        if (force <= cfg.holding_stop_force_threshold)
        {
            state.holding = false;
        }
        else if (!state.holding)
        {
            state.holding = force >= cfg.holding_start_force_threshold;
        }

        if (velocity < 0)
        {
            // open the hand
            return state.vel_ctrl.calculate(curr_pos, velocity, dt, maxPWM);
        }

        std::int16_t out_pwm = 0;

        switch (cfg.holding_mode)
        {
            case HoldingMode::pwm:
            {
                out_pwm = (state.holding && velocity >= 0) ?
                          cfg.holding_pwm :
                          state.vel_ctrl.calculate(curr_pos, velocity, dt, maxPWM);
            }
            break;
            case HoldingMode::velocity:
            {
                out_pwm = state.vel_ctrl.calculate(
                              curr_pos,
                              state.holding ? cfg.holding_velocity : velocity,
                              dt,
                              maxPWM);
            }
            break;
        };
        return out_pwm;
    }
}
