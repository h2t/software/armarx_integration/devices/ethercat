#pragma once

#include <RobotAPI/libraries/core/PIDController.h>

namespace devices::ethercat::hand::soft_sensorized_finger::controller
{
    struct PositionToPWM
    {
        PositionToPWM(float p, float i = 0, float d = 0);

        PositionToPWM() = default;
        PositionToPWM(PositionToPWM&&) = default;
        PositionToPWM(const PositionToPWM&) = default;
        PositionToPWM& operator=(PositionToPWM&&) = default;
        PositionToPWM& operator=(const PositionToPWM&) = default;

        void reset();
        std::int16_t calculate(float curr_pos, float targ_pos, float dt, std::int16_t maxPWM);
        armarx::PIDController PID;
    };
}
