#pragma once

#include "PositionToPWM.h"

namespace devices::ethercat::hand::soft_sensorized_finger::controller
{
    struct VelocityToPWM
    {
        VelocityToPWM(float p, float i = 0, float d = 0);

        VelocityToPWM() = default;
        VelocityToPWM(VelocityToPWM&&) = default;
        VelocityToPWM(const VelocityToPWM&) = default;
        VelocityToPWM& operator=(VelocityToPWM&&) = default;
        VelocityToPWM& operator=(const VelocityToPWM&) = default;

        void reset(float pos);
        std::int16_t calculate(float curr_pos, float velocity, float dt, std::int16_t maxPWM);
        PositionToPWM pos_ctrl;
        float         virtual_pos_target = 0;
    };
}
