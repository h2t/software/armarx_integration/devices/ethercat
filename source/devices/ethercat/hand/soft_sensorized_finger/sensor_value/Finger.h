#pragma once

#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>

namespace devices::ethercat::hand::soft_sensorized_finger
{
    class FingerSensorValue : virtual public armarx::SensorValueBase
    {
    public:
        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION

        std::uint32_t                   frame_counter;

        Eigen::Vector3f                 joint_encoder_prox;
        float                           joint_encoder_prox_temp;
        std::array<std::int16_t, 3>     raw_joint_encoder_prox;
        std::uint16_t                   raw_joint_encoder_prox_temp;

        Eigen::Vector3f                 joint_encoder_dist;
        float                           joint_encoder_dist_temp;
        std::array<std::int16_t, 3>     raw_joint_encoder_dist;
        std::uint16_t                   raw_joint_encoder_dist_temp;

        float                           pressure_prox_l;
        float                           pressure_prox_l_temp;
        std::uint32_t                   raw_pressure_prox_l;
        std::uint16_t                   raw_pressure_prox_l_temp;

        float                           pressure_prox_r;
        float                           pressure_prox_r_temp;
        std::uint32_t                   raw_pressure_prox_r;
        std::uint16_t                   raw_pressure_prox_r_temp;

        float                           pressure_dist_joint;
        float                           pressure_dist_joint_temp;
        std::uint32_t                   raw_pressure_dist_joint;
        std::uint16_t                   raw_pressure_dist_joint_temp;

        float                           pressure_dist_tip;
        float                           pressure_dist_tip_temp;
        std::uint32_t                   raw_pressure_dist_tip;
        std::uint16_t                   raw_pressure_dist_tip_temp;

        Eigen::Vector3f                 shear_force_dist_joint;
        float                           shear_force_dist_joint_temp;
        std::array<std::int16_t, 3>     raw_shear_force_dist_joint;
        std::uint16_t                   raw_shear_force_dist_joint_temp;

        Eigen::Vector3f                 shear_force_dist_tip;
        float                           shear_force_dist_tip_temp;
        std::array<std::int16_t, 3>     raw_shear_force_dist_tip;
        std::uint16_t                   raw_shear_force_dist_tip_temp;

        float                           proximity;
        std::uint16_t                   raw_proximity;

        std::array<Eigen::Vector3f, 17>             accelerometer_fifo;
        std::array<std::array<std::int16_t, 3>, 17> raw_accelerometer_fifo;
        std::uint16_t                               accelerometer_fifo_length;

        static SensorValueInfo<FingerSensorValue> GetClassMemberInfo()
        {
            using T = FingerSensorValue;
            SensorValueInfo<T> svi;
            svi.addMemberVariable(&T::frame_counter,                    "frame_counter");

            svi.addMemberVariable(&T::joint_encoder_prox,               "joint_encoder_prox");
            svi.addMemberVariable(&T::joint_encoder_prox_temp,          "joint_encoder_prox_temp");
            svi.addMemberVariable(&T::raw_joint_encoder_prox,           "raw_joint_encoder_prox");
            svi.addMemberVariable(&T::raw_joint_encoder_prox_temp,      "raw_joint_encoder_prox_temp");

            svi.addMemberVariable(&T::joint_encoder_dist,               "joint_encoder_dist");
            svi.addMemberVariable(&T::joint_encoder_dist_temp,          "joint_encoder_dist_temp");
            svi.addMemberVariable(&T::raw_joint_encoder_dist,           "raw_joint_encoder_dist");
            svi.addMemberVariable(&T::raw_joint_encoder_dist_temp,      "raw_joint_encoder_dist_temp");

            svi.addMemberVariable(&T::pressure_prox_l,                  "pressure_prox_l");
            svi.addMemberVariable(&T::pressure_prox_l_temp,             "pressure_prox_l_temp");
            svi.addMemberVariable(&T::raw_pressure_prox_l,              "raw_pressure_prox_l");
            svi.addMemberVariable(&T::raw_pressure_prox_l_temp,         "raw_pressure_prox_l_temp");

            svi.addMemberVariable(&T::pressure_prox_r,                  "pressure_prox_r");
            svi.addMemberVariable(&T::pressure_prox_r_temp,             "pressure_prox_r_temp");
            svi.addMemberVariable(&T::raw_pressure_prox_r,              "raw_pressure_prox_r");
            svi.addMemberVariable(&T::raw_pressure_prox_r_temp,         "raw_pressure_prox_r_temp");

            svi.addMemberVariable(&T::pressure_dist_joint,              "pressure_dist_joint");
            svi.addMemberVariable(&T::pressure_dist_joint_temp,         "pressure_dist_joint_temp");
            svi.addMemberVariable(&T::raw_pressure_dist_joint,          "raw_pressure_dist_joint");
            svi.addMemberVariable(&T::raw_pressure_dist_joint_temp,     "raw_pressure_dist_joint_temp");

            svi.addMemberVariable(&T::pressure_dist_tip,                "pressure_dist_tip");
            svi.addMemberVariable(&T::pressure_dist_tip_temp,           "pressure_dist_tip_temp");
            svi.addMemberVariable(&T::raw_pressure_dist_tip,            "raw_pressure_dist_tip");
            svi.addMemberVariable(&T::raw_pressure_dist_tip_temp,       "raw_pressure_dist_tip_temp");

            svi.addMemberVariable(&T::shear_force_dist_joint,           "shear_force_dist_joint");
            svi.addMemberVariable(&T::shear_force_dist_joint_temp,      "shear_force_dist_joint_temp");
            svi.addMemberVariable(&T::raw_shear_force_dist_joint,       "raw_shear_force_dist_joint");
            svi.addMemberVariable(&T::raw_shear_force_dist_joint_temp,  "raw_shear_force_dist_joint_temp");

            svi.addMemberVariable(&T::shear_force_dist_tip,             "shear_force_dist_tip");
            svi.addMemberVariable(&T::shear_force_dist_tip_temp,        "shear_force_dist_tip_temp");
            svi.addMemberVariable(&T::raw_shear_force_dist_tip,         "raw_shear_force_dist_tip");
            svi.addMemberVariable(&T::raw_shear_force_dist_tip_temp,    "raw_shear_force_dist_tip_temp");

            svi.addMemberVariable(&T::proximity,                        "proximity");
            svi.addMemberVariable(&T::raw_proximity,                    "raw_proximity");

            svi.addMemberVariable(&T::accelerometer_fifo,               "accelerometer_fifo");
            svi.addMemberVariable(&T::raw_accelerometer_fifo,           "raw_accelerometer_fifo");
            svi.addMemberVariable(&T::accelerometer_fifo_length,        "accelerometer_fifo_length");
            return svi;
        }
    };
}
