#pragma once

#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>

namespace devices::ethercat::hand::soft_sensorized_finger
{
    class MotorSensorValue :
        virtual public armarx::SensorValue1DoFActuatorPosition,
        virtual public armarx::SensorValue1DoFActuatorVelocity,
        virtual public armarx::SensorValue1DoFMotorPWM
    {
    public:
        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION

        std::uint32_t frame_counter;
        std::int32_t  raw_position;
        std::int32_t  raw_velocity;
        std::uint32_t pwm_max;

        static SensorValueInfo<MotorSensorValue> GetClassMemberInfo()
        {
            using T = MotorSensorValue;
            SensorValueInfo<T> svi;
            svi.addMemberVariable(&T::frame_counter,    "frame_counter");

            svi.addMemberVariable(&T::raw_position,      "raw_position");
            svi.addMemberVariable(&T::raw_velocity,      "raw_velocity");

            svi.addMemberVariable(&T::pwm_max,           "motor_pwm_max");

            svi.addBaseClass<SensorValue1DoFActuatorPosition>();
            svi.addBaseClass<SensorValue1DoFActuatorVelocity>();
            svi.addBaseClass<SensorValue1DoFMotorPWM>();
            return svi;
        }
    };
}
