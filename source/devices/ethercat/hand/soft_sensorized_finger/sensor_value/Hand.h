#pragma once

#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>

namespace devices::ethercat::hand::soft_sensorized_finger
{
    class HandSensorValue : virtual public armarx::SensorValueBase
    {
    public:
        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION

        float         temperature;
        std::uint32_t raw_temperature;
        std::uint32_t frame_counter;
        std::uint16_t update_rate_main;
        std::uint16_t update_rate_fpga;

        float         relative_position;
        float         velocity;

        static SensorValueInfo<HandSensorValue> GetClassMemberInfo()
        {
            using T = HandSensorValue;
            SensorValueInfo<T> svi;
            svi.addMemberVariable(&T::temperature,      "temperature");
            svi.addMemberVariable(&T::raw_temperature,  "raw_temperature");
            svi.addMemberVariable(&T::frame_counter,    "frame_counter");
            svi.addMemberVariable(&T::update_rate_main, "update_rate_main");
            svi.addMemberVariable(&T::update_rate_fpga, "update_rate_fpga");
            return svi;
        }
    };
}
