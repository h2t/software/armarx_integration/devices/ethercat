#pragma once


// Simox
#include <VirtualRobot/Nodes/RobotNode.h>

// armarx
#include <armarx/control/ethercat/AbstractDevice.h>
#include <armarx/control/ethercat/SlaveIdentifier.h>
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>

#include <devices/ethercat/hand/common/AbstractHand.h>
#include <devices/ethercat/hand/common/AbstractHandUnitControllerWrapper.h>
#include "bus_data/Hand.h"
#include "bus_data/Finger.h"
#include "bus_data/Motor.h"
#include "Slave.h"


namespace devices::ethercat::hand::soft_sensorized_finger::detail
{
    using FingerSensorDevicePtr =
        std::shared_ptr<class FingerSensorDevice>;

    using MotorControlDevicePtr =
        std::shared_ptr<class MotorControlDevice>;

    using HandSensorDevicePtr =
        std::shared_ptr<class HandSensorDevice>;
}

namespace devices::ethercat::hand::soft_sensorized_finger
{
    class FunctionalDevice :
        public armarx::control::ethercat::AbstractDevice,
        public common::AbstractHand
    {

    public:
        FunctionalDevice(armarx::RapidXmlReaderPtr jointNode,
                         armarx::DefaultRapidXmlReaderNode defaultConfigurationNode,
                         const VirtualRobot::RobotPtr& robot);
        ~FunctionalDevice() = default;
        void init(SlavePtr handSlave);
        void initData();

        armarx::control::ethercat::SlaveIdentifier getSlaveIdentifier() const;
        std::string getDeviceName() const;
    public:
        common::AbstractHandUnitControllerWrapperPtr createHandUnitControllerWrapper(
            armarx::RobotUnit* robotUnit, const std::string& handName,
            armarx::RapidXmlReaderPtr configNode) const override;
        std::string getHandConfigNodeName() const override;
        std::string getHandDeviceName() const override;

        //void addDevicesTo(const KITSensorizedSoftFingerHandV1FactoryPtr& f);

    private:
        const std::string deviceName;
        armarx::RapidXmlReaderPtr configNode;
        VirtualRobot::RobotPtr robot;

        ///data objects for sensor and control
        bus_data::FingerSensorDataPtr littleSensorDataPtr;
        bus_data::FingerSensorDataPtr ringSensorDataPtr;
        bus_data::FingerSensorDataPtr middleSensorDataPtr;
        bus_data::FingerSensorDataPtr indexSensorDataPtr;
        bus_data::FingerSensorDataPtr thumbSensorDataPtr;

        bus_data::MotorControlDataPtr otherControlDataPtr;
        bus_data::MotorControlDataPtr indexControlDataPtr;
        bus_data::MotorControlDataPtr thumbControlDataPtr;

        bus_data::HandSensorDataPtr   handSensorDataPtr;

        ///device objects for sensor and control
        detail::FingerSensorDevicePtr littleSensorDevicePtr;
        detail::FingerSensorDevicePtr ringSensorDevicePtr;
        detail::FingerSensorDevicePtr middleSensorDevicePtr;
        detail::FingerSensorDevicePtr indexSensorDevicePtr;
        detail::FingerSensorDevicePtr thumbSensorDevicePtr;

        detail::MotorControlDevicePtr otherControlDevicePtr;
        detail::MotorControlDevicePtr indexControlDevicePtr;
        detail::MotorControlDevicePtr thumbControlDevicePtr;

        detail::HandSensorDevicePtr   handSensorDevicePtr;
        ///bus devices - serial form config and pointer to actual bus slave
        const armarx::control::ethercat::SlaveIdentifier slaveIdentifier;

        SlavePtr handSlave;

        IceUtil::Time lastReadUpdate, lastWriteUpdate;
    };

}
