/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Armar6RT::ArmarXObjects::Armar6IceKinematicUnit
 * @author     Markus Swarowsky ( markus dot swarowsky at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Device.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/util/StringHelperTemplates.h>

#include <devices/ethercat/common/h2t_devices.h>
#include <devices/ethercat/hand/armar6_v2/joint_controller/PWM.h>
#include <devices/ethercat/hand/armar6_v2/joint_controller/Position.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/Shape.h>


namespace devices::ethercat::hand::armar6_v2
{

    Device::Device(armarx::control::hardware_config::DeviceConfig& hwConfig,
                   VirtualRobot::RobotPtr const& robot) :
        DeviceBase(hwConfig.getName()),
        DeviceInterface(hwConfig.getName()),
        robot(robot),
        fingersDataPtr(nullptr),
        thumbDataPtr(nullptr),
        slaveIdentifier(hwConfig.getOnlySlaveConfig().getIdentifier()),
        handSlave(nullptr) /*,
        thumbDevice(nullptr),
        fingersDevice(nullptr)*/,
        thumbDataConfig(hwConfig, hwConfig.getSubdeviceConfig("Thumb")),
        fingersDataConfig(hwConfig, hwConfig.getSubdeviceConfig("Fingers"))

    {
        motorTemperatureShutdownThreshold = hwConfig.getFloat("motorTemperatureShutdownThreshold");
    }


    Device::~Device() = default;


    armarx::control::ethercat::DeviceInterface::TryAssignResult
    Device::tryAssign(armarx::control::ethercat::SlaveInterface& slave)
    {
        using Result = DeviceInterface::TryAssignResult;

        Result result = ethercat::common::tryAssignLegacyH2TDevice(
            slave, handSlave, slaveIdentifier.productCode);
        if (result != Result::unknown)
        {
            return result;
        }

        return result;
    }


    armarx::control::ethercat::DeviceInterface::AllAssignedResult
    Device::onAllAssigned()
    {
        using Result = DeviceInterface::AllAssignedResult;

        if (handSlave)
        {
            thumbDevice.reset(new RobotUnitDevice(getDeviceName() + "Thumb"));
            fingersDevice.reset(new RobotUnitDevice(getDeviceName() + "Fingers"));

            // Also add both devices to subDevices
            subDevices.push_back(thumbDevice);
            subDevices.push_back(fingersDevice);

            return Result::ok;
        }

        return Result::slavesMissing;
    }


    std::string
    Device::getClassName() const
    {
        return "KITHand_V2";
    }


    void
    Device::postSwitchToSafeOp()
    {
        if (!fingersDataPtr && !thumbDataPtr)
        {
            fingersDataPtr = std::make_unique<Data>(fingersDataConfig,
                                                    handSlave->getOutputsPtr(),
                                                    handSlave->getInputsPtr(),
                                                    false);
            thumbDataPtr = std::make_unique<Data>(thumbDataConfig,
                                                  handSlave->getOutputsPtr(),
                                                  handSlave->getInputsPtr(),
                                                  true);

            thumbDevice->init(this, thumbDataPtr.get());
            fingersDevice->init(this, fingersDataPtr.get());
        }

        ARMARX_CHECK_NOT_NULL(fingersDataPtr);
        ARMARX_CHECK_NOT_NULL(thumbDataPtr);
    }


    armarx::control::ethercat::SlaveIdentifier
    Device::getSlaveIdentifier() const
    {
        return slaveIdentifier;
    }


    const Device::RobotUnitDevicePtr&
    Device::getFingersDevice() const
    {
        return fingersDevice;
    }


    const Device::RobotUnitDevicePtr&
    Device::getThumbDevice() const
    {
        return thumbDevice;
    }


    //armarx::KITHandV2Data* KITHandV2::getData() const
    //{
    //    if (fingersData* == nullptr)
    //    {
    //        throw LocalException("KITHandV2 has no data set, switch to SafeOP before");
    //    }
    //    return fingersData*;
    //}


    void
    Device::rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration)
    {
        ARMARX_CHECK_EXPRESSION(false);
        // called from multiple robotunit devices (thumbs/fingers)
        if ((sensorValuesTimestamp - lastWriteUpdate).toMicroSeconds() == 0)
        {
            return;
        }
        lastWriteUpdate = sensorValuesTimestamp;

        if (!fingersDataPtr)
        {
            DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                   "KITHandV2 has no data set, switch to SafeOP before");
        }

        if (fingersDataPtr->getMotorTemperature() > motorTemperatureShutdownThreshold ||
            fingersDataPtr->getMotorTemperature() > motorTemperatureShutdownThreshold)
        {

            DEVICE_ERROR(rtGetDeviceName(),
                         "Hand Motor Temperature too high (threshold: %f) - stopping motors - "
                         "finger motor temperature: %f°C thumb motor temperature: %f°C",
                         motorTemperatureShutdownThreshold,
                         fingersDataPtr->getMotorTemperature(),
                         fingersDataPtr->getMotorTemperature())
                .deactivateSpam(5);
            fingersDataPtr->setCommand(0);
        }

        fingersDataPtr->rtWriteTargetValues(sensorValuesTimestamp, timeSinceLastIteration);
    }


    TYPEDEF_PTRS_SHARED(HandControllerWrapperV2);


    class HandControllerWrapperV2 : public common::AbstractHandUnitControllerWrapper
    {

    public:
        ShapeControllerPtr controller;

        void
        setShape(const std::string& shape) override
        {
            ARMARX_CHECK_EXPRESSION(controller);
            if (!controller->isControllerActive())
            {
                controller->activateController();
            }

            if (armarx::Contains(shape, "relax", true))
            {
                controller->stopMotion();
            }
            else if (armarx::Contains(shape, "half open", true))
            {
                controller->setTargets(0.5f, 0.5f);
            }
            else if (armarx::Contains(shape, "open", true))
            {
                controller->setTargets(0, 0);
            }
            else if (armarx::Contains(shape, "close", true))
            {
                controller->setTargets(1, 1);
            }
            else
            {
                ARMARX_ERROR << "shape '" << shape << "' not supported.";
            }
        }

        std::string
        describeHandState() const override
        {
            std::stringstream ss;
            ss.precision(3);
            ss << std::fixed;
            ss << controller->getFingersJointValue() << " " << controller->getFingersTarget() << " "
               << controller->getFingersPwm();
            ss << " / ";
            ss << controller->getThumbJointValue() << " " << controller->getThumbTarget() << " "
               << controller->getThumbPwm();
            ss << " " << (controller->isControlEnabled() ? "active" : "disabled");
            return ss.str();
        }

        void
        setJointAngles(const armarx::NameValueMap& targetJointAngles) override
        {
            ARMARX_CHECK_EXPRESSION(controller);
            if (!controller->isControllerActive())
            {
                controller->activateController();
            }

            float fingersTarget = controller->getFingersTarget();
            float thumbTarget = controller->getThumbJointValue();
            for (const auto& pair : targetJointAngles)
            {
                if (pair.first == "Fingers")
                {
                    fingersTarget = pair.second;
                }
                else if (pair.first == "Thumb")
                {
                    thumbTarget = pair.second;
                }
                else
                {
                    ARMARX_WARNING << "Invalid HandJointName '" << pair.first << "', ignoring.";
                }
            }
            controller->setTargets(fingersTarget, thumbTarget);
        }

        std::map<std::string, float>
        getActualJointValues() override
        {
            armarx::NameValueMap jointValues;
            jointValues["Fingers"] = controller->getFingersJointValue();
            jointValues["Thumb"] = controller->getThumbJointValue();
            return jointValues;
        }
    };


    common::AbstractHandUnitControllerWrapperPtr
    Device::createHandUnitControllerWrapper(armarx::RobotUnit* robotUnit,
                                            const std::string& handName) const
    {
        HandControllerWrapperV2Ptr wrapper(new HandControllerWrapperV2());
        ShapeControllerConfigPtr cfg = new ShapeControllerConfig(getDeviceName());
        wrapper->controller = ShapeControllerPtr::dynamicCast(
            robotUnit->createNJointController("NJointKITHandV2ShapeController",
                                              handName + "_NJointKITHandV2ShapeController",
                                              cfg,
                                              false,
                                              true));
        wrapper->controller->setConfigs(fingersDataConfig, thumbDataConfig);
        return wrapper;
    }


    //    std::string
    //    Device::getDeviceName() const
    //    {
    //        return name;
    //    }


    Device::RobotUnitDevice::RobotUnitDevice(const std::string& deviceName) :
        DeviceBase(deviceName),
        DeviceInterface::SubDeviceInterface(deviceName),
        ControlDevice(deviceName),
        SensorDevice(deviceName)
    {
    }


    void
    Device::RobotUnitDevice::init(Device* hand, Data* dataPtr)
    {
        this->hand = hand;
        this->dataPtr = dataPtr;
        stopMovementController.reset(new StopMovementController(dataPtr));
        emergencyController.reset(new EmergencyStopController(dataPtr));
        shapeController.reset(new PWMController(dataPtr, hand));
        positionController.reset(new PositionController(dataPtr, hand));

        addJointController(emergencyController.get());
        addJointController(stopMovementController.get());
        addJointController(shapeController.get());
        addJointController(positionController.get());
    }


    void
    Device::RobotUnitDevice::rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                                const IceUtil::Time& timeSinceLastIteration)
    {
        dataPtr->rtReadSensorValues(sensorValuesTimestamp, timeSinceLastIteration);
        dataPtr->updateSensorValueStruct(sensorValue);
    }


    void
    Device::RobotUnitDevice::rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                                 const IceUtil::Time& timeSinceLastIteration)
    {
        if (dataPtr->getMotorTemperature() > hand->motorTemperatureShutdownThreshold)
        {
            DEVICE_ERROR(rtGetDeviceName(),
                         "Hand Motor Temperature too high (threshold: %f) - stopping motors - "
                         "motor temperature: %f°C",
                         hand->motorTemperatureShutdownThreshold,
                         dataPtr->getMotorTemperature())
                .deactivateSpam(5);
            dataPtr->setCommand(0);
        }

        dataPtr->rtWriteTargetValues(sensorValuesTimestamp, timeSinceLastIteration);
    }


    std::string
    Device::getHandDeviceName() const
    {
        return getDeviceName();
    }

} // namespace devices::ethercat::hand::armar6_v2
