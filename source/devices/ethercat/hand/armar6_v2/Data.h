/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <memory>

// armarx
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <armarx/control/ethercat/DataInterface.h>
#include <armarx/control/rt_filters/AverageFilter.h>
#include <armarx/control/rt_filters/FirFilter.h>

#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <RobotAPI/components/units/RobotUnit/Devices/ControlDevice.h>
#include <RobotAPI/components/units/RobotUnit/Devices/SensorDevice.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <RobotAPI/interface/units/KinematicUnitInterfaceStdOverloads.h>
#include <RobotAPI/libraries/core/Pose.h>

// robot_devices
#include <devices/ethercat/common/elmo/gold/Data.h>
#include <devices/ethercat/hand/armar6_v2/SlaveIO.h>
#include <devices/ethercat/hand/armar6_v2/Config.h>


namespace devices::ethercat::hand::armar6_v2
{

    class SensorDataValue :
        public armarx::SensorValue1DoFActuatorPosition,
        public armarx::SensorValue1DoFActuatorMotorTemperature
    {

    public:

        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION

        float target_pwm_motor;
        float direction_motor;

        float actual_pwm;
        float actual_direction;

        float motorCurrent;

        static SensorValueInfo<SensorDataValue> GetClassMemberInfo()
        {
            SensorValueInfo<SensorDataValue> svi;
            svi.addBaseClass<SensorValue1DoFActuatorPosition>();
            svi.addBaseClass<SensorValue1DoFActuatorMotorTemperature>();
            svi.addMemberVariable(&SensorDataValue::target_pwm_motor, "target_pwm_motor");
            svi.addMemberVariable(&SensorDataValue::direction_motor, "direction_motor");
            svi.addMemberVariable(&SensorDataValue::actual_pwm, "actual_pwm");
            svi.addMemberVariable(&SensorDataValue::actual_direction, "actual_direction");
            svi.addMemberVariable(&SensorDataValue::motorCurrent, "motorCurrent");

            return svi;
        }

    };


    class Data :
        public armarx::control::ethercat::DataInterface
    {

    public:

        Data(const DataConfig& config,
             SlaveOut* sensorOUT, SlaveIn* sensorIN, bool isThumb);

        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        void setCommand(std::int16_t pwm);

        void updateSensorValueStruct(SensorDataValue& data);

        float getMotorTemperature() const;
        float getRelativeEncoderValue() const;

        unsigned int getMaxPWM() const;

        float getKp() const;

    private:

        bool isThumb;
        unsigned int maxPWM;
        float kp;

        SlaveOut* sensorOUT;
        SlaveIn* sensorIN;

        armarx::control::ethercat::LinearConvertedValue<std::uint16_t> motorTemperature;
        armarx::control::ethercat::LinearConvertedValue<std::int32_t> relativeEncoder;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> motorCurrent;

        // target values in etherCAT buffer
        std::uint8_t* target_pwm_ptr = NULL;
        std::uint8_t* direction_motor_ptr = NULL;

        // target values set from outside
        std::uint8_t target_pwm_motor;
        std::uint8_t direction_motor;

        // sensor values in etherCAT buffer
        std::uint8_t* actual_pwm_ptr = NULL;
        std::uint8_t* actual_direction_ptr = NULL;

    };

}
