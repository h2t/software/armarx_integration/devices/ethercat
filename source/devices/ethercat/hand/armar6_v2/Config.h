#pragma once

#include <armarx/control/hardware_config/DeviceConfig.h>

namespace devices::ethercat::hand::armar6_v2
{
    namespace hwconfig = armarx::control::hardware_config;
    struct DataConfig
    {
        DataConfig(hwconfig::DeviceConfig& deviceConfig, hwconfig::DeviceConfigBase& subDeviceConfig)
            : motorTemperature(deviceConfig.getLinearConfig("motorTemperature")),
              relativeEncoder(subDeviceConfig.getLinearConfig("relativeEncoder")),
              motorCurrent(subDeviceConfig.getLinearConfig("motorCurrent")),
              maxPwm(subDeviceConfig.getUint("maxPwm")),
              Kp(subDeviceConfig.getFloat("Kp"))
        {

        }
        hwconfig::types::LinearConfig motorTemperature;
        hwconfig::types::LinearConfig relativeEncoder;
        hwconfig::types::LinearConfig motorCurrent;
        std::uint32_t maxPwm;
        float Kp;
    };
}
