#pragma once


#include <atomic>
#include <cstdint>
#include <memory>
#include <thread>

#include <armarx/control/ethercat/SlaveInterface.h>

#include <devices/ethercat/hand/armar6_v2/SlaveIO.h>


namespace devices::ethercat::hand::armar6_v2
{

    static constexpr std::uint32_t KITHandV2Left = 0x404;
    static constexpr std::uint32_t KITHandV2Right = 0x403;

    class Slave : public armarx::control::ethercat::SlaveInterfaceWithIO<SlaveIn, SlaveOut>
    {

    public:
        Slave(const armarx::control::ethercat::SlaveIdentifier& slaveIdentifier);

        /**
         * @see AbstractSlave::doMappings
         */
        void doMappings() override;

        void prepareForOp() override;

        void finishPreparingForOp() override;

        /**
         * @see AbstractSlave::prepare()
         */
        bool prepareForRun() override;

        /**
         * @see AbstractSlave::execute()
         */
        void execute() override;

        /**
         * @see AbstractSlave::shutdown()
         */
        bool shutdown() override;

        /**
         * @see AbstractSlave::hasError()
         * @return
         */
        bool hasError() override;

        void prepareForSafeOp() override;

        void finishPreparingForSafeOp() override;

        static std::string getDefaultName();

        static bool
        isSlaveIdentifierAccepted(const armarx::control::ethercat::SlaveIdentifier& sid);
    };

} // namespace devices::ethercat::hand::armar6_v2
