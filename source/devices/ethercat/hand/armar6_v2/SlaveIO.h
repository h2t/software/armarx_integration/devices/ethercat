#pragma once


// SDT/STL
#include <cstdint>


namespace devices::ethercat::hand::armar6_v2
{

    /**
     * @brief PDO mapping sensorB->master
     */
    struct SlaveOut
    {

        //Actual motor state
        std::uint8_t pwm_finger_motor;
        std::uint8_t pwm_thumb_motor;
        std::uint8_t direction_finger_motor;
        std::uint8_t direction_thumb_motor;

        // motor temperature
        std::uint16_t motor_fingers_temperature;
        std::uint16_t motor_thumb_temperature;

        // relative encoder values
        std::int32_t relative_encoder_value_fingers;
        std::int32_t relative_encoder_value_thumb;

        // motor current values
        std::int16_t motor_current_value_fingers;
        std::int16_t motor_current_value_thumb;

    } __attribute__((__packed__));


    /**
     * @brief PDO mapping master->sensorB
     */
    struct SlaveIn
    {
        // motor state
        std::uint8_t pwm_finger_state;
        std::uint8_t pwm_thumb_state;
        std::uint8_t direction_finger_motor;
        std::uint8_t direction_thumb_motor;

    } __attribute__((__packed__));

}
