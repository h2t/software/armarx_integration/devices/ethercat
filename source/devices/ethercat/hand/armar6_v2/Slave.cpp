#include "Slave.h"

#include <iostream>

#include <ArmarXCore/core/logging/Logging.h>
#include <armarx/control/ethercat/Bus.h>

#include <devices/ethercat/common/h2t_devices.h>


namespace devices::ethercat::hand::armar6_v2
{

    Slave::Slave(const armarx::control::ethercat::SlaveIdentifier& slaveIdentifier) :
        SlaveInterfaceWithIO(slaveIdentifier)
    {
        inputs = nullptr;
        outputs = nullptr;

        setTag(this->slaveIdentifier.getName());
    }


    void
    Slave::execute()
    {
    }


    bool
    Slave::prepareForRun()
    {
        ARMARX_INFO << deactivateSpam(5) << "KITHandV2Slave " << getSlaveIdentifier()
                    << " is ready";
        return true;
    }


    bool
    Slave::shutdown()
    {
        return true;
    }


    void
    Slave::doMappings()
    {
    }


    bool
    Slave::hasError()
    {
        return false;
    }


    void
    Slave::prepareForSafeOp()
    {
    }


    void
    Slave::finishPreparingForSafeOp()
    {
    }


    void
    Slave::prepareForOp()
    {
    }


    void
    Slave::finishPreparingForOp()
    {
    }


    std::string
    Slave::getDefaultName()
    {
        return "Armar6HandV2";
    }


    bool
    Slave::isSlaveIdentifierAccepted(const armarx::control::ethercat::SlaveIdentifier& sid)
    {
        // For the old KIT hands (used in Armar6), the serial number could not be encoded on the chip.
        // The product code was used instead.

        const std::vector<std::uint32_t> validProductCodes{KITHandV2Left, KITHandV2Right};

        const bool productCodeMatches =
            std::find(validProductCodes.begin(), validProductCodes.end(), sid.productCode) !=
            validProductCodes.end();

        return sid.vendorID == devices::ethercat::common::H2TVendorId and sid.serialNumber == 0 and
               productCodeMatches;
    }

} // namespace devices::ethercat::hand::armar6_v2
