#include <devices/ethercat/hand/armar6_v2/njoint_controller/Shape.h>


// armarx
#include <RobotAPI/libraries/core/math/MathUtils.h>

// robot_devices
#include <devices/ethercat/hand/armar6_v2/Data.h>
#include <devices/ethercat/hand/armar6_v2/joint_controller/PWM.h>


namespace devices::ethercat::hand::armar6_v2
{

    armarx::NJointControllerRegistration<ShapeController> registrationControllerNJointKITHandV2ShapeController("NJointKITHandV2ShapeController");


    ShapeController::ShapeController(armarx::RobotUnitPtr prov,
                                     ShapeControllerConfigPtr config,
                                     const VirtualRobot::RobotPtr& r)
    {
        ARMARX_CHECK_EXPRESSION(prov);
        auto thumbDeviceName = config->deviceName + "Thumb";
        armarx::ControlTargetBase* ct = useControlTarget(thumbDeviceName, ctrl_modes::HandV2Pwm);
        ARMARX_CHECK_EXPRESSION(ct);
        armarx::ConstSensorDevicePtr thumbSensor = peekSensorDevice(thumbDeviceName);
        ARMARX_CHECK_EXPRESSION(thumbSensor) << "device name: " << thumbDeviceName;

        thumbSensorValues = thumbSensor->getSensorValue()->asA<SensorDataValue>();
        ARMARX_CHECK_EXPRESSION(thumbSensorValues);
        ARMARX_CHECK_EXPRESSION(ct->asA<PWMControlTarget>());
        thumbControlTarget = ct->asA<PWMControlTarget>();

        auto fingersDeviceName = config->deviceName + "Fingers";
        ct = useControlTarget(fingersDeviceName, ctrl_modes::HandV2Pwm);
        ARMARX_CHECK_EXPRESSION(ct);
        armarx::ConstSensorDevicePtr fingersSensor = peekSensorDevice(fingersDeviceName);
        ARMARX_CHECK_EXPRESSION(fingersSensor) << "device name: " << fingersDeviceName;

        fingersSensorValues = fingersSensor->getSensorValue()->asA<SensorDataValue>();
        ARMARX_CHECK_EXPRESSION(fingersSensorValues);
        ARMARX_CHECK_EXPRESSION(ct->asA<PWMControlTarget>());
        fingersControlTarget = ct->asA<PWMControlTarget>();
    }


    void ShapeController::rtPreActivateController()
    {
        this->fingersTarget = fingersSensorValues->position;
        this->thumbTarget = thumbSensorValues->position;
    }


    void ShapeController::stopMotion()
    {
        enableControl = false;
    }


    void ShapeController::setConfigs(const DataConfig& fingersConfig, const DataConfig& thumbConfig)
    {
        fingersMaxPwm = fingersConfig.maxPwm;
        thumbMaxPwm = thumbConfig.maxPwm;
        KpFingers = fingersConfig.Kp;
        KpThumb = thumbConfig.Kp;
        fingersPwmLimit = fingersMaxPwm;
        thumbPwmLimit = thumbMaxPwm;
    }


    /*bool NJointKITHandV2ShapeController::hasQueuedTarget()
    {
        return targetFingerState != queuedFingerState || targetThumbState != queuedThumbState;
    }*/


    std::int16_t ShapeController::getFingersPwm()
    {
        return fingersPwm;
    }


    std::int16_t ShapeController::getThumbPwm()
    {
        return thumbPwm;
    }


    float ShapeController::getFingersTarget()
    {
        return fingersTarget;
    }


    float ShapeController::getThumbTarget()
    {
        return thumbTarget;
    }


    float ShapeController::getFingersJointValue() const
    {
        return actualFingersJointValue;
    }


    float ShapeController::getThumbJointValue() const
    {
        return actualThumbJointValue;
    }


    bool ShapeController::isControlEnabled()
    {
        return enableControl;
    }


    void ShapeController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {

        actualFingersJointValue = fingersSensorValues->position;
        actualThumbJointValue = thumbSensorValues->position;

        if (enableControl)
        {
            float fingersError = fingersTarget - actualFingersJointValue;
            float thumbError = thumbTarget - actualThumbJointValue;

            fingersPwm = armarx::math::MathUtils::LimitMinMax(-fingersMaxPwm, fingersMaxPwm, (int)(KpFingers * fingersMaxPwm * fingersError));
            thumbPwm = armarx::math::MathUtils::LimitMinMax(-thumbMaxPwm, thumbMaxPwm, (int)(KpThumb * thumbMaxPwm * thumbError));
        }
        else
        {
            fingersPwm = 0;
            thumbPwm = 0;
        }


        fingersControlTarget->pwm_motor = fingersPwm;
        thumbControlTarget->pwm_motor = thumbPwm;
    }


    armarx::WidgetDescription::StringWidgetDictionary ShapeController::getFunctionDescriptions(const Ice::Current&) const
    {
        using namespace armarx::WidgetDescription;
        HBoxLayoutPtr hbox = new HBoxLayout;
        {
            hbox->children.emplace_back(new Label(false, "Fingers"));
            FloatSliderPtr slider = new FloatSlider;
            slider->name = "Fingers";
            slider->min = 0;
            slider->defaultValue = getFingersJointValue();
            slider->max = 1;
            hbox->children.emplace_back(slider);
        }
        {
            hbox->children.emplace_back(new Label(false, "Thumb"));
            FloatSliderPtr slider = new FloatSlider;
            slider->name = "Thumb";
            slider->min = 0;
            slider->defaultValue = getThumbJointValue();
            slider->max = 1;
            hbox->children.emplace_back(slider);
        }
        {
            hbox->children.emplace_back(new Label(false, "FingersMaxPwm"));
            FloatSliderPtr slider = new FloatSlider;
            slider->name = "FingersMaxPwm";
            slider->min = 0;
            slider->defaultValue = 1;
            slider->max = 1;
            hbox->children.emplace_back(slider);
        }
        {
            hbox->children.emplace_back(new Label(false, "ThumbMaxPwm"));
            FloatSliderPtr slider = new FloatSlider;
            slider->name = "ThumbMaxPwm";
            slider->min = 0;
            slider->defaultValue = 1;
            slider->max = 1;
            hbox->children.emplace_back(slider);
        }

        return {{"Targets", hbox}};
    }


    void ShapeController::callDescribedFunction(const std::string& name, const armarx::StringVariantBaseMap& valueMap, const Ice::Current&)
    {
        if (name == "Targets")
        {
            float fingersValue = valueMap.at("Fingers")->getFloat();
            float thumbValue = valueMap.at("Thumb")->getFloat();
            float fingersMaxPwm = valueMap.at("FingersMaxPwm")->getFloat();
            float thumbMaxPwm = valueMap.at("ThumbMaxPwm")->getFloat();
            setTargetsWithPwm(fingersValue, thumbValue, fingersMaxPwm, thumbMaxPwm);
        }
        else
        {
            ARMARX_WARNING << "Unknown function name called: " << name;
        }
    }


    void ShapeController::setTargets(float fingers, float thumb, const Ice::Current&)
    {
        setTargetsWithPwm(fingers, thumb, 1, 1);
    }


    void ShapeController::setTargetsWithPwm(float fingers, float thumb, float fingersRelativeMaxPwm, float thumbRelativeMaxPwm, const Ice::Current&)
    {
        this->fingersTarget = armarx::math::MathUtils::LimitMinMax(0.f, 2.f, fingers);
        this->thumbTarget = armarx::math::MathUtils::LimitMinMax(0.f, 2.f, thumb);
        this->fingersMaxPwm = armarx::math::MathUtils::LimitMinMax(0, fingersPwmLimit, (int)(fingersPwmLimit * fingersRelativeMaxPwm));
        this->thumbMaxPwm = armarx::math::MathUtils::LimitMinMax(0, thumbPwmLimit, (int)(thumbPwmLimit * thumbRelativeMaxPwm));
        enableControl = true;
    }


    JointValues ShapeController::getJointValues(const Ice::Current&)
    {
        JointValues values;
        values.fingersJointValue = actualFingersJointValue;
        values.thumbJointValue = actualThumbJointValue;
        return values;
    }

} // namespace armarx
