#pragma once


// armarx
#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>

// robot_devices
#include <devices/ethercat/hand/armar6_v2/Data.h>
#include <devices/ethercat/hand/armar6_v2/joint_controller/PWM.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/Shape.h>
#include <devices/ethercat/hand/armar6_v2/njoint_controller/ShapeInterface.h>


namespace devices::ethercat::hand::armar6_v2
{

    using ShapeControllerConfigPtr = IceUtil::Handle<class ShapeControllerConfig>;


    class ShapeControllerConfig :
        virtual public armarx::NJointControllerConfig
    {

    public:

        ShapeControllerConfig(std::string const& deviceName):
            deviceName(deviceName)
        {}

        const std::string deviceName;

    };


    using ShapeControllerPtr = IceInternal::Handle<class ShapeController>;


    class ShapeController :
        public armarx::NJointController,
        public ShapeControllerInterface

    {

    public:

        using ConfigPtrT = ShapeControllerConfigPtr;
        ShapeController(armarx::RobotUnitPtr prov, ShapeControllerConfigPtr config, const VirtualRobot::RobotPtr& r);

        // NJointControllerInterface interface

    public:

        std::string getClassName(const Ice::Current&) const override
        {
            return "NJointKITHandV2ShapeController";
        }

        void stopMotion();

        void setConfigs(const DataConfig& fingersConfig, const DataConfig& thumbConfig);

        std::int16_t getFingersPwm();
        std::int16_t getThumbPwm();

        float getFingersTarget();
        float getThumbTarget();

        float getFingersJointValue() const;
        float getThumbJointValue() const;

        bool isControlEnabled();

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

    private:

        const SensorDataValue* thumbSensorValues;
        const SensorDataValue* fingersSensorValues;
        PWMControlTarget* thumbControlTarget;
        PWMControlTarget* fingersControlTarget;

        std::int16_t fingersPwm = 0;
        std::int16_t thumbPwm = 0;

        std::int16_t fingersMaxPwm = 1.0;
        std::int16_t thumbMaxPwm = 1.0;
        std::int16_t fingersPwmLimit = 0;
        std::int16_t thumbPwmLimit = 0;

        float fingersTarget = 0;
        float thumbTarget = 0;

        float actualFingersJointValue = 0;
        float actualThumbJointValue = 0;

        float KpFingers = 0;
        float KpThumb = 0;

        bool enableControl = false;

    public:

        armarx::WidgetDescription::StringWidgetDictionary getFunctionDescriptions(const Ice::Current&) const override;
        void callDescribedFunction(const std::string& name, const armarx::StringVariantBaseMap& valueMap, const Ice::Current&) override;

        // NJointKITHandV2ShapeControllerInterface interface

    public:

        void setTargets(float fingers, float thumb, const Ice::Current& = Ice::emptyCurrent) override;
        void setTargetsWithPwm(float fingers, float thumb, float fingersRelativeMaxPwm, float thumbRelativeMaxPwm, const Ice::Current& = Ice::emptyCurrent) override;
        JointValues getJointValues(const Ice::Current&) override;

        // NJointControllerBase interface

    protected:

        void rtPreActivateController() override;

    };
}
