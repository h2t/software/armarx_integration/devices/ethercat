/*
     * This file is part of ArmarX.
     *
     * ArmarX is free software; you can redistribute it and/or modify
     * it under the terms of the GNU General Public License version 2 as
     * published by the Free Software Foundation.
     *
     * ArmarX is distributed in the hope that it will be useful, but
     * WITHOUT ANY WARRANTY; without even the implied warranty of
     * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
     * GNU General Public License for more details.
     *
     * You should have received a copy of the GNU General Public License
     * along with this program. If not, see <http://www.gnu.org/licenses/>.
     *
     * @package    Armar6RT::ArmarXObjects::Armar6IceKinematicUnit
     * @author     Markus Swarowsky ( markus dot swarowsky at student dot kit dot edu )
     * @date       2017
     * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
     *             GNU General Public License
     */


#pragma once


#include <VirtualRobot/Nodes/RobotNode.h>

#include <armarx/control/ethercat/DeviceInterface.h>
#include <armarx/control/ethercat/SlaveIdentifier.h>

#include <devices/ethercat/hand/armar6_v2/Config.h>
#include <devices/ethercat/hand/armar6_v2/Data.h>
#include <devices/ethercat/hand/armar6_v2/Slave.h>
#include <devices/ethercat/hand/armar6_v2/joint_controller/EmergencyStop.h>
#include <devices/ethercat/hand/armar6_v2/joint_controller/StopMovement.h>
#include <devices/ethercat/hand/common/AbstractHand.h>


namespace devices::ethercat::hand::armar6_v2
{

    using PWMControllerPtr = std::shared_ptr<class PWMController>;
    using PositionControllerPtr = std::shared_ptr<class PositionController>;

    class Device :
        // public ControlDevice,
        // public SensorDevice,
        public armarx::control::ethercat::DeviceInterface,
        public common::AbstractHand
    {

    public:
        class RobotUnitDevice :
            public DeviceInterface::SubDeviceInterface,
            public armarx::ControlDevice,
            public armarx::SensorDevice
        {

            friend class Device;

            // SensorDevice interface

        public:
            RobotUnitDevice(const std::string& deviceName);
            const armarx::SensorValueBase*
            getSensorValue() const override
            {
                return &sensorValue;
            }

            void init(Device* hand, Data* dataPtr);

        protected:
            Device* hand;
            Data* dataPtr;
            EmergencyStopControllerPtr emergencyController;
            StopMovementControllerPtr stopMovementController;
            PWMControllerPtr shapeController;
            PositionControllerPtr positionController;
            /// The data object for copying to non-rt part
            SensorDataValue sensorValue;

            // SensorDevice interface

        public:
            void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                    const IceUtil::Time& timeSinceLastIteration) override;

            // ControlDevice interface

        public:
            void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                     const IceUtil::Time& timeSinceLastIteration) override;
        };

        using RobotUnitDevicePtr = std::shared_ptr<RobotUnitDevice>;

        Device(armarx::control::hardware_config::DeviceConfig& hwConfig,
               const VirtualRobot::RobotPtr& robot);

        ~Device() override;

        // DeviceInterface interface
        TryAssignResult tryAssign(armarx::control::ethercat::SlaveInterface& slave) override;
        AllAssignedResult onAllAssigned() override;
        std::string getClassName() const override;
        void postSwitchToSafeOp() override;

        // ControlDevice interface
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                 const IceUtil::Time& timeSinceLastIteration);

        armarx::control::ethercat::SlaveIdentifier getSlaveIdentifier() const;
        //        std::string getDeviceName() const;
        const RobotUnitDevicePtr& getThumbDevice() const;
        const RobotUnitDevicePtr& getFingersDevice() const;

    public:
        common::AbstractHandUnitControllerWrapperPtr
        createHandUnitControllerWrapper(armarx::RobotUnit* robotUnit,
                                        const std::string& handName) const override;
        std::string getHandDeviceName() const override;

    private:
        VirtualRobot::RobotPtr robot;

        ///The data and target object
        std::unique_ptr<Data> fingersDataPtr;
        std::unique_ptr<Data> thumbDataPtr;

        ///bus devices - serial form config and pointer to actual bus slave
        const armarx::control::ethercat::SlaveIdentifier slaveIdentifier;

        Slave* handSlave;

        RobotUnitDevicePtr thumbDevice, fingersDevice;
        IceUtil::Time lastReadUpdate, lastWriteUpdate;

        float motorTemperatureShutdownThreshold;

        const DataConfig thumbDataConfig, fingersDataConfig;
    };

} // namespace devices::ethercat::hand::armar6_v2
