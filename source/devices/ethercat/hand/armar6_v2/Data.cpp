/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, butgetActualTorque
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <devices/ethercat/hand/armar6_v2/Data.h>


// armarx
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


namespace devices::ethercat::hand::armar6_v2
{

    Data::Data(const DataConfig& config,
               SlaveOut* sensorOUT, SlaveIn* sensorIN,
               bool isThumb) :
        isThumb(isThumb),
        sensorOUT(sensorOUT),
        sensorIN(sensorIN)
    {
        maxPWM = config.maxPwm;
        ARMARX_INFO << "maxPwmTag : " << maxPWM;
        kp = config.Kp;
        ARMARX_CHECK_EXPRESSION(sensorOUT);

        motorTemperature.init(isThumb ? &sensorOUT->motor_thumb_temperature : &sensorOUT->motor_fingers_temperature, config.motorTemperature);

        relativeEncoder.init(isThumb ? &sensorOUT->relative_encoder_value_thumb : &sensorOUT->relative_encoder_value_fingers, config.relativeEncoder);

        motorCurrent.init(isThumb ? &sensorOUT->motor_current_value_thumb : &sensorOUT->motor_current_value_fingers, config.motorCurrent);

        actual_pwm_ptr = isThumb ? &sensorOUT->pwm_thumb_motor : &sensorOUT->pwm_finger_motor;
        actual_direction_ptr = isThumb ? &sensorOUT->direction_thumb_motor :  &sensorOUT->direction_finger_motor;

        direction_motor_ptr = isThumb ? &sensorIN->direction_thumb_motor : &sensorIN->direction_finger_motor;
        target_pwm_ptr = isThumb ? &sensorIN->pwm_thumb_state : &sensorIN->pwm_finger_state;
    }


    void Data::rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        motorTemperature.read();
        relativeEncoder.read();
        motorCurrent.read();

        if (isThumb)
        {
            // ARMARX_INFO << deactivateSpam(1) << "relativeEncoder: " << relativeEncoder.value << " actual pwm: " << (int)*actual_pwm_ptr;
        }
    }


    void Data::rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        *direction_motor_ptr = direction_motor;
        *target_pwm_ptr = target_pwm_motor;

        if (isThumb)
        {
            // ARMARX_INFO << deactivateSpam(1) << "target ethercat: " << (int)*target_pwm_ptr << " target from outside: " << (int)target_pwm_motor;
        }
    }


    void Data::setCommand(std::int16_t pwm)
    {
        direction_motor = pwm > 0 ? 1 : 0;
        target_pwm_motor = std::abs(pwm);
    }


    void Data::updateSensorValueStruct(SensorDataValue& data)
    {
        ARMARX_CHECK_EXPRESSION(actual_pwm_ptr);
        ARMARX_CHECK_EXPRESSION(actual_direction_ptr);
        data.motorTemperature = motorTemperature.value;
        data.target_pwm_motor = target_pwm_motor;
        data.direction_motor = direction_motor;
        data.actual_pwm = *actual_pwm_ptr;
        data.actual_direction = *actual_direction_ptr;
        data.position = relativeEncoder.value;
        data.motorCurrent = motorCurrent.value;
    }


    float Data::getMotorTemperature() const
    {
        return motorTemperature.value;
    }


    float Data::getRelativeEncoderValue() const
    {
        return relativeEncoder.value;
    }


    unsigned int Data::getMaxPWM() const
    {
        return maxPWM;
    }


    float Data::getKp() const
    {
        return kp;
    }

}
