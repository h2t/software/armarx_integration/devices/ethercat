#include <devices/ethercat/hand/armar6_v2/joint_controller/StopMovement.h>


namespace devices::ethercat::hand::armar6_v2
{

    StopMovementController::StopMovementController(Data* dataPtr) :
        dataPtr(dataPtr)
    {

    }


    void StopMovementController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {

    }


    armarx::ControlTargetBase* StopMovementController::getControlTarget()
    {
        return &target;
    }


    void StopMovementController::rtPreActivateController()
    {
        ARMARX_RT_LOGF_INFO("Stopping hand!");
        dataPtr->setCommand(0);
    }

} // namespace armarx
