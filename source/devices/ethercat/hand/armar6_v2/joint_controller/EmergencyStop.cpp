#include <devices/ethercat/hand/armar6_v2/joint_controller/EmergencyStop.h>


namespace devices::ethercat::hand::armar6_v2
{

    EmergencyStopController::EmergencyStopController(Data* dataPtr) :
        dataPtr(dataPtr)
    {

    }


    void EmergencyStopController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {

    }


    armarx::ControlTargetBase* EmergencyStopController::getControlTarget()
    {
        return &target;
    }


    void EmergencyStopController::rtPreActivateController()
    {
        ARMARX_INFO << "Stopping hand!";
        dataPtr->setCommand(0);
    }

} // namespace armarx
