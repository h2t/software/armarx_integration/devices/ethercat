#include <devices/ethercat/hand/armar6_v2/joint_controller/Position.h>


// armarx
#include <RobotAPI/libraries/core/math/MathUtils.h>


namespace devices::ethercat::hand::armar6_v2
{

    PositionController::PositionController(Data* data, Device* hand) :
        data(data),
        hand(hand),
        PID(data->getKp(), 0.0f, 0.0f)

    {
        // thumbPID.threadSafe = false;
        PID.threadSafe = false;
    }


    armarx::ControlTargetBase* PositionController::getControlTarget()
    {
        return &target;
    }


    void PositionController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        if (target.isValid())
        {
            int maxPWM = std::min<int>(data->getMaxPWM(), target.maxPWM);

            // thumbPID.update(timeSinceLastIteration.toSecondsDouble(), data->getRelativeEncoderValue(), target.TargetPosition);
            PID.update(timeSinceLastIteration.toSecondsDouble(), data->getRelativeEncoderValue(), target.position);
            // ARMARX_IMPORTANT << "rtRun: " << VAROUT(target.pwm_finger_motor) << " " << VAROUT(target.pwm_thumb_motor);
            std::int16_t TargetPWM = armarx::math::MathUtils::LimitMinMax(-maxPWM, maxPWM, int(PID.getControlValue() * maxPWM));
            // int16 thumbTargetPWM = math::MathUtils::LimitMinMax(-target.maxThumbPWM, target.maxThumbPWM, int(thumbPID.getControlValue() * target.maxThumbPWM));
            // ARMARX_RT_LOGF_INFO("target pwm: %d, target.maxPWM: %d, target.position: %.2f, current position: %.2f kp: %.2f", TargetPWM, maxPWM, target.position, data->getRelativeEncoderValue(), PID.Kp).deactivateSpam(0.5);
            data->setCommand(TargetPWM);
        }
    }


    void PositionController::rtPreActivateController()
    {
    }


    void PositionController::rtPostDeactivateController()
    {
        data->setCommand(0);
    }

} // namespace armarx
