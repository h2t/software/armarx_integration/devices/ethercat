#pragma once


// armarx
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>

// robot_devices
#include <devices/ethercat/hand/armar6_v2/Data.h>


namespace devices::ethercat::hand::armar6_v2
{

    using EmergencyStopControllerPtr = std::shared_ptr<class EmergencyStopController>;


    class EmergencyStopController : public armarx::JointController
    {

    public:

        EmergencyStopController(Data* dataPtr);

    private:

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

        /**
         * Returns the Target for this controller, but as this is the Emergency controller it will ignored.
         * As this controller will just break
         * @return is type VelocityTarget but it will return a nullptr, because it won't be possible to set a target
         */
        armarx::ControlTargetBase* getControlTarget() override;

        void rtPreActivateController() override;

    private:

        armarx::DummyControlTargetEmergencyStop target;
        Data* dataPtr;

    };

} // namespace
