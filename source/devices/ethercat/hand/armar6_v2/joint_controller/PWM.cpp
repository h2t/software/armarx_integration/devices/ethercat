#include <devices/ethercat/hand/armar6_v2/joint_controller/PWM.h>


namespace devices::ethercat::hand::armar6_v2
{

    PWMController::PWMController(Data* data, Device* hand) :
        data(data),
        hand(hand)
    {

    }


    armarx::ControlTargetBase* PWMController::getControlTarget()
    {
        return &target;
    }


    void PWMController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        if (target.isValid())
        {
            // ARMARX_RT_LOGF_INFO("encoder: %.2f: target pwm: %d", data->getRelativeEncoderValue(), target.pwm_motor).deactivateSpam(1);
            data->setCommand(target.pwm_motor);
            // data->setCommand(target.pwm_finger_motor, target.pwm_thumb_motor);
        }
    }


    void PWMController::rtPreActivateController()
    {
    }


    void PWMController::rtPostDeactivateController()
    {
        data->setCommand(0);
    }

} // namespace armarx
