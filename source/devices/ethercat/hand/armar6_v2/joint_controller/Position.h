#pragma once


// armarx
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/libraries/core/PIDController.h>

// robot_devices
#include <devices/ethercat/hand/armar6_v2/Device.h>
#include <devices/ethercat/hand/armar6_v2/Data.h>


namespace devices::ethercat::hand::armar6_v2
{

    namespace ctrl_modes
    {
        // static const std::string HandV2PositionControl = "ControlMode_HandPositionControl";
    }

    class PositionControlTarget :
        public armarx::ControlTarget1DoFActuatorPosition
    {

    public:

        std::int16_t maxPWM;

        void reset() override
        {
            maxPWM = 255;
            ControlTarget1DoFActuatorPosition::reset();
        }

        bool isValid() const override
        {
            return std::abs(maxPWM) < 256 && ControlTarget1DoFActuatorPosition::isValid();
        }

        static ControlTargetInfo<PositionControlTarget> GetClassMemberInfo()
        {
            ControlTargetInfo<PositionControlTarget> cti;
            cti.addBaseClass<ControlTarget1DoFActuatorPosition>();
            cti.addMemberVariable(&PositionControlTarget::maxPWM, "maxPWM");
            return cti;
        }

        DETAIL_ControlTargetBase_DEFAULT_METHOD_IMPLEMENTATION

    };


    class PositionController :
        public armarx::JointController
    {

    public:

        PositionController(Data* data, Device* hand);

        // JointController interface

    public:

        armarx::ControlTargetBase* getControlTarget() override;

    protected:

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        void rtPreActivateController() override;
        void rtPostDeactivateController() override;

    private:

        Data* data;
        Device* hand;
        armarx::PIDController PID;
        PositionControlTarget target;

    };


    using PositionControllerPtr = std::shared_ptr<PositionController>;

} // namespace armarx
