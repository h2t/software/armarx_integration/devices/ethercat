#include <devices/ethercat/hand/armar6_v1/njoint_controller/Shape.h>


#include <devices/ethercat/hand/armar6_v1/Data.h>
#include <devices/ethercat/hand/armar6_v1/joint_controller/PWM.h>


namespace devices::ethercat::hand::armar6_v1::njoint_controller
{

    armarx::NJointControllerRegistration<ShapeController> registrationControllerNJointKITHandShapeController("NJointKITHandShapeController");


    ShapeController::ShapeController(armarx::RobotUnitPtr prov,
                                     ShapeControllerConfigPtr config,
                                     const VirtualRobot::RobotPtr& r)
    {
        ARMARX_CHECK_EXPRESSION(prov);

        armarx::ControlTargetBase* ct = useControlTarget(config->deviceName, joint_controller::ctrl_modes::HandPWM);
        ARMARX_CHECK_EXPRESSION(ct);
        sensorValues = useSensorValue<SensorDataValue>(config->deviceName);
        ARMARX_CHECK_EXPRESSION(sensorValues) << "device name: " << config->deviceName;
        ARMARX_CHECK_EXPRESSION(ct->asA<joint_controller::HandPWMControlTarget>());
        controlTarget = ct->asA<joint_controller::HandPWMControlTarget>();

        transitionMatrix(ShapeClosed, ShapeClosed) = CommandFullClose;
        transitionMatrix(ShapeClosed, ShapeHalfOpen) = CommandHalfOpen;
        transitionMatrix(ShapeClosed, ShapeOpen) = CommandFullOpen;
        //transitionMatrix(ShapeClosed, ShapeDelta) = CommandDelta;

        transitionMatrix(ShapeHalfOpen, ShapeClosed) = CommandFullClose;
        transitionMatrix(ShapeHalfOpen, ShapeHalfOpen) = CommandNone;
        transitionMatrix(ShapeHalfOpen, ShapeOpen) = CommandHalfOpen;
        //transitionMatrix(ShapeHalfOpen, ShapeDelta) = CommandDelta;

        transitionMatrix(ShapeOpen, ShapeClosed) = CommandFullClose;
        transitionMatrix(ShapeOpen, ShapeHalfOpen) = CommandHalfClose;
        transitionMatrix(ShapeOpen, ShapeOpen) = CommandNone;
        //transitionMatrix(ShapeOpen, ShapeDelta) = CommandIgnore;

        //transitionMatrix(ShapeDelta, ShapeClosed) = CommandFullClose;
        //transitionMatrix(ShapeDelta, ShapeHalfOpen) = CommandHalfClose;
        //transitionMatrix(ShapeDelta, ShapeOpen) = CommandIgnore;
        //transitionMatrix(ShapeDelta, ShapeDelta) = CommandDelta;

        lastStart = armarx::rtNow();

        actualFingerState = ShapeOpen;
        actualThumbState = ShapeOpen;
        queuedFingerState = ShapeOpen;
        queuedThumbState = ShapeOpen;
        targetFingerState = ShapeOpen;
        targetThumbState = ShapeOpen;

        handBusy = false;
    }


    void ShapeController::setShapeCommand(Command command)
    {
        this->command = command;
        ARMARX_INFO << deactivateSpam(1, KITHandCommandToString(command)) << "new shape command: " << KITHandCommandToString(command);
    }


    void ShapeController::queueTargetShape(ShapeController::HandShapeState fingerState, ShapeController::HandShapeState thumbState)
    {
        ARMARX_IMPORTANT << "queueTargetShape: " << VAROUT(fingerState) << " " << VAROUT(thumbState);
        this->queuedFingerState = fingerState;
        this->queuedThumbState = thumbState;
        this->hasQueuedTarget = true;
    }


    void ShapeController::queueDeltaShapeCommand(ShapeController::DeltaShape fingerDelta, ShapeController::DeltaShape thumbDelta)
    {
        this->queuedFingerDelta = fingerDelta;
        this->queuedThumbDelta = thumbDelta;
        this->hasQueuedDelta = true;
    }


    void ShapeController::stopMotion()
    {
        this->hasQueuedDelta = false;
        this->hasQueuedTarget = false;
        fingerCommandSeq = fingerCommandSequences.None;
        thumbCommandSeq = thumbCommandSequences.None;
        handBusy = false;
    }


    /*bool NJointKITHandShapeController::isShaping()
    {
        return actualFingerState != targetFingerState || actualThumbState != targetThumbState;
    }*/


    void ShapeController::setConfigs(const ShapeConfig& fingersConfig, const ShapeConfig& thumbConfig)
    {
        fingerCommandSequences.fromShapeConfig(fingersConfig);
        thumbCommandSequences.fromShapeConfig(thumbConfig);

        fingerCommandSeq = fingerCommandSequences.None;
        thumbCommandSeq = thumbCommandSequences.None;
    }


    /*bool NJointKITHandShapeController::hasQueuedTarget()
    {
        return targetFingerState != queuedFingerState || targetThumbState != queuedThumbState;
    }*/


    ShapeController::HandShapeSubCommand ShapeController::getShapeSubCommand(ShapeController::HandShapeState currentState, ShapeController::HandShapeState targetState)
    {
        //        ARMARX_IMPORTANT << VAROUT(currentState) << " " << VAROUT(targetState);
        HandShapeSubCommand command = (HandShapeSubCommand)transitionMatrix(currentState, targetState);
        return command;
    }


    ShapeController::HandShapeState ShapeController::getActualThumbState()
    {
        return actualThumbState;
    }


    ShapeController::HandShapeState ShapeController::getActualFingerState()
    {
        return actualFingerState;
    }


    std::string ShapeController::HandShapeStateToString(ShapeController::HandShapeState state)
    {
        switch (state)
        {
            case ShapeOpen:
                return "Open";
            case ShapeHalfOpen:
                return "HalfOpen";
            case ShapeClosed:
                return "Closed";
            default:
                return "unknown";
        }
    }


    std::int16_t ShapeController::getFingerPwm()
    {
        return fingerPwm;
    }


    std::int16_t ShapeController::getThumbPwm()
    {
        return thumbPwm;
    }


    void ShapeController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        //controlTarget->shape = static_cast<u_int8_t>(command.operator armarx::KITHandCommand());

        if (!handBusy && hasQueuedTarget)
        {
            //ARMARX_IMPORTANT << "move queued state to target state";
            hasQueuedTarget = false;

            targetFingerState = queuedFingerState;
            targetThumbState = queuedThumbState;

            lastStart = sensorValuesTimestamp;
            handBusy = true;
            fingerCommandSeq = fingerCommandSequences.getCommandSequence(getShapeSubCommand(actualFingerState, targetFingerState));
            thumbCommandSeq = thumbCommandSequences.getCommandSequence(getShapeSubCommand(actualThumbState, targetThumbState));
            //ARMARX_IMPORTANT << VAROUT(fingerCommandSeq.activePwm) << " " << VAROUT(thumbCommandSeq.activePwm);
        }

        if (!handBusy && hasQueuedDelta)
        {
            hasQueuedDelta = false;
            lastStart = sensorValuesTimestamp;
            handBusy = true;
            fingerCommandSeq = fingerCommandSequences.getDeltaSequence(actualFingerState, queuedFingerDelta);
            thumbCommandSeq = thumbCommandSequences.getDeltaSequence(actualThumbState, queuedThumbDelta);
        }

        if (handBusy)
        {
            int deltaMs = (sensorValuesTimestamp - lastStart).toMilliSeconds();
            fingerCommandSeq.setPwm(deltaMs, fingerPwm);
            thumbCommandSeq.setPwm(deltaMs, thumbPwm);
            //ARMARX_IMPORTANT << "handBusy: " << VAROUT(fingerPwm) << " " << VAROUT(thumbPwm);
            if (fingerCommandSeq.isDone(deltaMs) && thumbCommandSeq.isDone(deltaMs))
            {
                handBusy = false;
                actualFingerState = targetFingerState;
                actualThumbState = targetThumbState;
                ARMARX_DEBUG_S << "shape completed. fingerPwm=" << fingerPwm << " thumbPwm=" << thumbPwm;
            }
        }
        else
        {
            fingerPwm = fingerCommandSeq.holdPwm;
            thumbPwm = thumbCommandSeq.holdPwm;
        }

        controlTarget->pwm_finger_motor = fingerPwm;
        controlTarget->pwm_thumb_motor = thumbPwm;
    }


    void ShapeController::CommandSequence::setPwm(int deltaMs, std::int16_t& pwm)
    {
        if (deltaMs > duration + delay)
        {
            pwm = holdPwm;
        }
        else if (deltaMs > delay)
        {
            pwm = activePwm;
        }
    }


    bool ShapeController::CommandSequence::isDone(int deltaMs)
    {
        return deltaMs > duration + delay;
    }


    void ShapeController::addSlider(std::vector<armarx::WidgetDescription::WidgetPtr>& children, std::string prefix, std::string name, std::int16_t defaultValue, std::int16_t min, std::int16_t max) const
    {
        using namespace armarx::WidgetDescription;
        VBoxLayoutPtr vbox = new VBoxLayout;
        vbox->children.emplace_back(new Label(false, name));
        FloatSliderPtr slider = new FloatSlider;
        slider->name = prefix + "_" + name;
        slider->min = min;
        slider->defaultValue = defaultValue;
        slider->max = max;
        vbox->children.emplace_back(slider);
        children.emplace_back(vbox);
    }


    armarx::WidgetDescription::WidgetPtr ShapeController::createSequenceSliders(const std::string& name, std::int16_t min, std::int16_t max, CommandSequence fingerCommandSeq, CommandSequence thumbCommandSeq) const
    {
        using namespace armarx::WidgetDescription;
        //VBoxLayoutPtr vbox = new VBoxLayout;
        HBoxLayoutPtr hbox1 = new HBoxLayout;
        //HBoxLayoutPtr hbox2 = new HBoxLayout;
        GroupBoxPtr groupBox = new GroupBox(false, name, hbox1);
        addSlider(hbox1->children, name, "fingerActivePwm", fingerCommandSeq.activePwm, min, max);
        addSlider(hbox1->children, name, "fingerDuration", fingerCommandSeq.duration, 0, 2000);
        if (name.find("Close") != std::string::npos)
        {
            addSlider(hbox1->children, name, "fingerHoldPwm", fingerCommandSeq.holdPwm, min, max);
        }

        addSlider(hbox1->children, name, "thumbDelay", thumbCommandSeq.delay, 0, 2000);
        addSlider(hbox1->children, name, "thumbActivePwm", thumbCommandSeq.activePwm, min, max);
        addSlider(hbox1->children, name, "thumbDuration", thumbCommandSeq.duration, 0, 2000);
        if (name.find("Close") != std::string::npos)
        {
            addSlider(hbox1->children, name, "thumbHoldPwm", thumbCommandSeq.holdPwm, min, max);
        }

        //vbox->children.emplace_back(hbox1);
        //vbox->children.emplace_back(hbox2);

        return groupBox;
    }


    void ShapeController::readSequence(const armarx::StringVariantBaseMap& valueMap, const std::string& name, CommandSequence& fingerCommandSeq, CommandSequence& thumbCommandSeq) const
    {
        fingerCommandSeq.activePwm = valueMap.at(name + "_" + "fingerActivePwm")->getFloat();
        fingerCommandSeq.duration = valueMap.at(name + "_" + "fingerDuration")->getFloat();
        if (name.find("Close") != std::string::npos)
        {
            fingerCommandSeq.holdPwm = valueMap.at(name + "_" + "fingerHoldPwm")->getFloat();
        }

        thumbCommandSeq.delay = valueMap.at(name + "_" + "thumbDelay")->getFloat();
        thumbCommandSeq.activePwm = valueMap.at(name + "_" + "thumbActivePwm")->getFloat();
        thumbCommandSeq.duration = valueMap.at(name + "_" + "thumbDuration")->getFloat();
        if (name.find("Close") != std::string::npos)
        {
            thumbCommandSeq.holdPwm = valueMap.at(name + "_" + "thumbHoldPwm")->getFloat();
        }
    }


    armarx::WidgetDescription::StringWidgetDictionary ShapeController::getFunctionDescriptions(const Ice::Current&) const
    {
        using namespace armarx::WidgetDescription;
        VBoxLayoutPtr vboxLayout = new VBoxLayout;
        vboxLayout->children.emplace_back(createSequenceSliders("FullClose", 0, 255, fingerCommandSequences.FullClose, thumbCommandSequences.FullClose));
        vboxLayout->children.emplace_back(createSequenceSliders("HalfClose", 0, 255, fingerCommandSequences.HalfClose, thumbCommandSequences.HalfClose));
        vboxLayout->children.emplace_back(createSequenceSliders("HalfOpen", -255, 0, fingerCommandSequences.HalfOpen, thumbCommandSequences.HalfOpen));
        vboxLayout->children.emplace_back(createSequenceSliders("FullOpen", -255, 0, fingerCommandSequences.FullOpen, thumbCommandSequences.FullOpen));
        return {{"HandControllerConfig", vboxLayout}};
    }


    void ShapeController::callDescribedFunction(const std::string& name, const armarx::StringVariantBaseMap& valueMap, const Ice::Current&)
    {
        if (name == "HandControllerConfig")
        {
            readSequence(valueMap, "FullClose", fingerCommandSequences.FullClose, thumbCommandSequences.FullClose);
            readSequence(valueMap, "HalfClose", fingerCommandSequences.HalfClose, thumbCommandSequences.HalfClose);
            readSequence(valueMap, "HalfOpen", fingerCommandSequences.HalfOpen, thumbCommandSequences.HalfOpen);
            readSequence(valueMap, "FullOpen", fingerCommandSequences.FullOpen, thumbCommandSequences.FullOpen);

            ARMARX_IMPORTANT << "fingerHoldPwm: " << fingerCommandSequences.FullClose.holdPwm;
        }
        else
        {
            ARMARX_WARNING << "Unknown function name called: " << name;
        }
    }


    void ShapeController::CommandSequenceConfig::fromShapeConfig(const ShapeConfig& shapeConfig)
    {

        FullClose = shapeConfig.fullClose;
        HalfClose = shapeConfig.halfClose;
        None = shapeConfig.none;
        HalfOpen = shapeConfig.halfOpen;
        FullOpen = shapeConfig.fullOpen;
        DeltaOpen = shapeConfig.deltaOpen;
        DeltaClose = shapeConfig.deltaClose;
    }


    ShapeController::CommandSequence ShapeController::CommandSequenceConfig::getCommandSequence(ShapeController::HandShapeSubCommand cmd)
    {
        switch (cmd)
        {
            case CommandFullClose:
                return FullClose;
            case CommandHalfClose:
                return HalfClose;
            case CommandNone:
                return None;
            case CommandHalfOpen:
                return HalfOpen;
            case CommandFullOpen:
                return FullOpen;
            default:
                return CommandSequence(0, 0, 0, 0);
        }
    }


    ShapeController::CommandSequence ShapeController::CommandSequenceConfig::getDeltaSequence(HandShapeState currentState, ShapeController::DeltaShape cmd)
    {
        switch (cmd)
        {
            case DeltaShape::DeltaClose:
                return DeltaClose;
            case DeltaShape::DeltaNone:
                return None;
            case DeltaShape::DeltaOpen:
                return currentState == ShapeOpen ? None : DeltaOpen;
            default:
                return CommandSequence(0, 0, 0, 0);
        }
    }

} // namespace armarx
