#pragma once


// armarx
#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>

// robot_devices
#include <devices/ethercat/hand/armar6_v1/Data.h>
#include <devices/ethercat/hand/armar6_v1/joint_controller/PWM.h>


namespace devices::ethercat::hand::armar6_v1::njoint_controller
{

    using ShapeControllerConfigPtr = IceUtil::Handle<class ShapeControllerConfig>;
    using ShapeControllerPtr = IceUtil::Handle<class ShapeController>;


    class ShapeControllerConfig :
        virtual public armarx::NJointControllerConfig
    {

    public:

        ShapeControllerConfig(std::string const& deviceName):
            deviceName(deviceName)
        {}

        const std::string deviceName;

    };


    class ShapeController :
        public armarx::NJointController
    {

    public:

        using ConfigPtrT = ShapeControllerConfigPtr;
        ShapeController(armarx::RobotUnitPtr prov, ShapeControllerConfigPtr config, const VirtualRobot::RobotPtr& r);

        enum HandShapeState : int { ShapeClosed = 0, ShapeHalfOpen = 1, ShapeOpen = 2, ShapeDelta = 3 };
        enum HandShapeSubCommand : int { CommandFullClose = -2, CommandHalfClose = -1, CommandNone = 0, CommandHalfOpen = 1, CommandFullOpen = 2 };
        enum DeltaShape : int {DeltaClose = -1, DeltaNone = 0, DeltaOpen = 1};

        struct CommandSequence
        {
            std::int16_t delay;
            std::int16_t activePwm;
            std::int16_t duration;
            std::int16_t holdPwm;

            CommandSequence(std::int16_t delay, std::int16_t activePwm, std::int16_t duration, std::int16_t holdPwm)
                : delay(delay), activePwm(activePwm), duration(duration), holdPwm(holdPwm)
            {}

            CommandSequence(CommandSequenceConfig sequence)
                : delay(sequence.delay), activePwm(sequence.activePwm), duration(sequence.duration), holdPwm(sequence.holdPwm)
            {}

            CommandSequence()
                : CommandSequence(0, 0, 0, 0)
            {}

            void setPwm(int deltaMs, std::int16_t& pwm);
            bool isDone(int deltaMs);
        };

        struct CommandSequenceConfig
        {
            CommandSequence FullClose;
            CommandSequence HalfClose;
            CommandSequence None;
            CommandSequence HalfOpen;
            CommandSequence FullOpen;
            CommandSequence DeltaOpen;
            CommandSequence DeltaClose;

            void fromShapeConfig(const ShapeConfig& shapeConfig);

            CommandSequence getCommandSequence(HandShapeSubCommand cmd);
            CommandSequence getDeltaSequence(HandShapeState currentState, DeltaShape cmd);
        };

        // NJointControllerInterface interface

    public:

        std::string getClassName(const Ice::Current&) const override
        {
            return "NJointKITHandShapeController";
        }

        void setShapeCommand(Command command);
        void queueTargetShape(HandShapeState fingerState, HandShapeState thumbState);
        void queueDeltaShapeCommand(DeltaShape fingerDelta, DeltaShape thumbDelta);
        void stopMotion();

        //bool isShaping();
        //bool hasQueuedTarget();

        void setConfigs(const ShapeConfig& fingersConfig, const ShapeConfig& thumbConfig);

        HandShapeSubCommand getShapeSubCommand(HandShapeState currentState, HandShapeState targetState);

        HandShapeState getActualThumbState();
        HandShapeState getActualFingerState();
        static std::string HandShapeStateToString(HandShapeState state);
        std::int16_t getFingerPwm();
        std::int16_t getThumbPwm();

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

    private:

        std::atomic<Command> command;
        const SensorDataValue* sensorValues;
        joint_controller::HandPWMControlTarget* controlTarget;

        Eigen::Matrix3i transitionMatrix;

        HandShapeState actualThumbState = ShapeOpen;
        HandShapeState actualFingerState = ShapeOpen;
        HandShapeState targetThumbState = ShapeOpen;
        HandShapeState targetFingerState = ShapeOpen;
        HandShapeState queuedThumbState = ShapeOpen;
        HandShapeState queuedFingerState = ShapeOpen;
        DeltaShape queuedFingerDelta = DeltaNone;
        DeltaShape queuedThumbDelta = DeltaNone;

        CommandSequence thumbCommandSeq;
        CommandSequence fingerCommandSeq;
        IceUtil::Time lastStart;
        bool handBusy = false;
        std::int16_t fingerPwm = 0;
        std::int16_t thumbPwm = 0;
        bool hasQueuedTarget = false;
        bool hasQueuedDelta = false;

        CommandSequenceConfig fingerCommandSequences;
        CommandSequenceConfig thumbCommandSequences;

        armarx::WidgetDescription::WidgetPtr createSequenceSliders(const std::string& name, std::int16_t min, std::int16_t max, CommandSequence fingerCommandSeq, CommandSequence thumbCommandSeq) const;
        void readSequence(const armarx::StringVariantBaseMap& valueMap, const std::string& name, CommandSequence& fingerCommandSeq, CommandSequence& thumbCommandSeq) const;

    public:

        void addSlider(std::vector<armarx::WidgetDescription::WidgetPtr>& children, std::string prefix, std::string name, std::int16_t defaultValue, std::int16_t min, std::int16_t max) const;
        armarx::WidgetDescription::StringWidgetDictionary getFunctionDescriptions(const Ice::Current&) const override;
        void callDescribedFunction(const std::string& name, const armarx::StringVariantBaseMap& valueMap, const Ice::Current&) override;

    };

}
