#pragma once


#include <armarx/control/ethercat/SlaveInterface.h>

#include <devices/ethercat/hand/armar6_v1/SlaveIO.h>


#define H2T_KITHAND_LEFT_PRODUCT_CODE 0x401
#define H2T_KITHAND_RIGHT_PRODUCT_CODE 0x402


namespace devices::ethercat::hand::armar6_v1
{

    class Slave :
        public armarx::control::ethercat::SlaveInterfaceWithIO<SlaveIn, SlaveOut>
    {

    public:

        Slave(const armarx::control::ethercat::SlaveIdentifier& slaveIdentifier);

        /**
         * @see AbstractSlave::doMappings
         */
        void doMappings() override;

        void prepareForOp() override;

        void finishPreparingForOp() override;

        /**
         * @see AbstractSlave::prepare()
         */
        bool prepareForRun() override;

        /**
         * @see AbstractSlave::execute()
         */
        void execute() override;

        /**
         * @see AbstractSlave::shutdown()
         */
        bool shutdown() override;

        /**
         * @see AbstractSlave::hasError()
         * @return
         */
        bool hasError() override;

        void prepareForSafeOp() override;
        void finishPreparingForSafeOp() override;

        static std::string getDefaultName();

        static bool
        isSlaveIdentifierAccepted(const armarx::control::ethercat::SlaveIdentifier& sid);
    };

}
