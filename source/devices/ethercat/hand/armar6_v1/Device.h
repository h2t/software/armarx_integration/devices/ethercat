/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Armar6RT::ArmarXObjects::Armar6IceKinematicUnit
 * @author     Markus Swarowsky ( markus dot swarowsky at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <memory>

#include <VirtualRobot/Nodes/RobotNode.h>

#include <armarx/control/ethercat/DeviceInterface.h>
#include <armarx/control/ethercat/SlaveIdentifier.h>
#include <armarx/control/hardware_config/DeviceConfig.h>

#include <devices/ethercat/hand/armar6_v1/Data.h>
#include <devices/ethercat/hand/armar6_v1/Slave.h>
#include <devices/ethercat/hand/common/AbstractHand.h>


namespace devices::ethercat::hand::armar6_v1
{

    namespace joint_controller
    {
        class EmergencyStop;
        class PWM;
        class StopMovement;
    } // namespace joint_controller


    class Device :
        public armarx::ControlDevice,
        public armarx::SensorDevice,
        public armarx::control::ethercat::DeviceInterface,
        public common::AbstractHand
    {

    public:
        Device(armarx::control::hardware_config::DeviceConfig& hwConfig,
               const VirtualRobot::RobotPtr& robot);

        ~Device() override;

        // DeviceInterface interface
        TryAssignResult tryAssign(armarx::control::ethercat::SlaveInterface& slave) override;
        AllAssignedResult onAllAssigned() override;
        std::string getClassName() const override;
        void postSwitchToSafeOp() override;

        // SensorDevice interface
        const armarx::SensorValueBase* getSensorValue() const override;
        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration) override;

        // ControlDevice interface
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                 const IceUtil::Time& timeSinceLastIteration) override;

        // AbstractHand interface
        std::string getHandDeviceName() const override;

        Data* getData() const;

        armarx::control::ethercat::SlaveIdentifier getSlaveIdentifier() const;

    private:
        void updateSensorValueStruct();

        VirtualRobot::RobotNodePtr robotNode;

        ///The data and target object
        std::unique_ptr<Data> data;
        /// The data object for copying to non-rt part
        SensorDataValue sensorValue;

        /// Whether or not this device should be started on the bus

        ///bus devices - serial form config and pointer to actual bus slave
        const armarx::control::ethercat::SlaveIdentifier slaveIdentifier;

        Slave* handSlave;

        std::unique_ptr<joint_controller::EmergencyStop> emergencyController;
        std::unique_ptr<joint_controller::StopMovement> stopMovementController;
        std::unique_ptr<joint_controller::PWM> shapeController;

        float motorTemperatureShutdownThreshold;

        DataConfig dataConfig;
        const ShapeConfig fingersConfig;
        const ShapeConfig thumbConfig;

        // AbstractHand interface

    public:
        common::AbstractHandUnitControllerWrapperPtr
        createHandUnitControllerWrapper(armarx::RobotUnit* robotUnit,
                                        const std::string& handName) const override;
    };
} // namespace devices::ethercat::hand::armar6_v1
