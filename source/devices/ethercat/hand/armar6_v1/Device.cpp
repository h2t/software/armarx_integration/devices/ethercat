/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Armar6RT::ArmarXObjects::Armar6IceKinematicUnit
 * @author     Markus Swarowsky ( markus dot swarowsky at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Device.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/util/StringHelperTemplates.h>

#include <devices/ethercat/common/h2t_devices.h>
#include <devices/ethercat/hand/armar6_v1/joint_controller/EmergencyStop.h>
#include <devices/ethercat/hand/armar6_v1/joint_controller/PWM.h>
#include <devices/ethercat/hand/armar6_v1/joint_controller/StopMovement.h>
#include <devices/ethercat/hand/armar6_v1/njoint_controller/Shape.h>


namespace devices::ethercat::hand::armar6_v1
{

    Device::Device(armarx::control::hardware_config::DeviceConfig& hwConfig,
                   VirtualRobot::RobotPtr const& robot) :
        DeviceBase(hwConfig.getName()),
        ControlDevice(hwConfig.getName()),
        SensorDevice(hwConfig.getName()),
        DeviceInterface(hwConfig.getName()),
        robotNode(robot->getRobotNode(hwConfig.getName())),
        data(nullptr),
        slaveIdentifier(hwConfig.getOnlySlaveConfig().getIdentifier()),
        dataConfig(hwConfig),
        fingersConfig(hwConfig.getControllerConfig("FingerShape")),
        thumbConfig(hwConfig.getControllerConfig("ThumbShape"))
    {
        motorTemperatureShutdownThreshold = hwConfig.getFloat("motorTemperatureShutdownThreshold");
    }


    Device::~Device() = default;


    void
    Device::postSwitchToSafeOp()
    {
        if (!data)
        {
            data.reset(new Data(dataConfig,
                                handSlave->getOutputsPtr(),
                                handSlave->getInputsPtr()));

            stopMovementController.reset(new joint_controller::StopMovement(data.get()));
            emergencyController.reset(new joint_controller::EmergencyStop(data.get()));
            shapeController.reset(new joint_controller::PWM(data.get(), this));

            addJointController(emergencyController.get());
            addJointController(stopMovementController.get());
            addJointController(shapeController.get());
        }

        ARMARX_CHECK_NOT_NULL(data);
    }


    void
    Device::updateSensorValueStruct()
    {
        sensorValue.motorTemperatureFingers = data->getMotorTemperatureFingers();
        sensorValue.motorTemperatureThumb = data->getMotorTemperatureThumb();
        sensorValue.target_pwm_finger_motor = data->getTargetPWMFingerMotor();
        sensorValue.target_pwm_thumb_motor = data->getTargetPWMThumbMotor();
        sensorValue.direction_finger_motor = data->getFingerMotorDirection();
        sensorValue.direction_thumb_motor = data->getThumbMotorDirection();
        sensorValue.imuQuaternion = data->getImutQuaternion();
        sensorValue.linearAcceleration = data->getLinearAcceleration();
        sensorValue.accelerometerTemperature = data->getAccelerometerTemperature();
        sensorValue.actual_finger_pwm = data->getActualFingerPwm();
        sensorValue.actual_finger_direction = data->getActualFingerDirection();
        sensorValue.actual_thumb_pwm = data->getActualThumbPwm();
        sensorValue.actual_thumb_direction = data->getActualThumbDirection();
    }


    armarx::control::ethercat::SlaveIdentifier
    Device::getSlaveIdentifier() const
    {
        return slaveIdentifier;
    }


    std::string
    Device::getHandDeviceName() const
    {
        return getDeviceName();
    }


    armarx::control::ethercat::DeviceInterface::TryAssignResult
    Device::tryAssign(armarx::control::ethercat::SlaveInterface& slave)
    {
        using Result = DeviceInterface::TryAssignResult;

        Result result = ethercat::common::tryAssignLegacyH2TDevice(
            slave, handSlave, slaveIdentifier.productCode);
        if (result != Result::unknown)
        {
            return result;
        }

        return result;
    }


    armarx::control::ethercat::DeviceInterface::AllAssignedResult
    Device::onAllAssigned()
    {
        using Result = DeviceInterface::AllAssignedResult;

        if (handSlave)
        {
            return Result::ok;
        }

        return Result::slavesMissing;
    }


    std::string
    Device::getClassName() const
    {
        return "KITHand_V1";
    }


    Data*
    Device::getData() const
    {
        if (not data)
        {
            DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                   "KITHand has no data set, switch to SafeOP before");
        }
        return data.get();
    }


    void
    Device::rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                               const IceUtil::Time& timeSinceLastIteration)
    {
        if (not data)
        {
            DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                   "KITHand has no data set, switch to SafeOP before");
        }
        data->rtReadSensorValues(sensorValuesTimestamp, timeSinceLastIteration);
        //    dataPtr->setCommand(eCLOSE_HOLD_5SEC_OPEN);

        updateSensorValueStruct();
    }


    void
    Device::rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration)
    {
        if (not data)
        {
            throw armarx::LocalException("KITHand has no data set, call switch to SafeOP before");
        }

        if (data->getMotorTemperatureFingers() > motorTemperatureShutdownThreshold ||
            data->getMotorTemperatureThumb() > motorTemperatureShutdownThreshold)
        {
            ARMARX_ERROR << deactivateSpam(5) << this->getDeviceName()
                         << ": Hand Motor Temperature too high (threshold: "
                         << motorTemperatureShutdownThreshold
                         << ") - stopping motors - finger motor temperature: "
                         << data->getMotorTemperatureFingers()
                         << " °C thumb motor temperature: " << data->getMotorTemperatureThumb()
                         << " °C";
            data->setCommand(Command::STOP, 0, 0);
        }

        data->rtWriteTargetValues(sensorValuesTimestamp, timeSinceLastIteration);
    }


    const armarx::SensorValueBase*
    Device::getSensorValue() const
    {
        return &sensorValue;
    }


    TYPEDEF_PTRS_SHARED(HandControllerWrapperV1);


    class HandControllerWrapperV1 : public common::AbstractHandUnitControllerWrapper
    {

    public:
        njoint_controller::ShapeControllerPtr controller;

        void
        setShape(const std::string& shape) override
        {
            using njoint_controller::ShapeController;

            ARMARX_CHECK_EXPRESSION(controller);
            controller->activateController();
            if (armarx::Contains(shape, "relax", true))
            {
                controller->stopMotion();
            }
            else if (armarx::Contains(shape, "delta open", true))
            {
                controller->queueDeltaShapeCommand(ShapeController::DeltaOpen,
                                                   ShapeController::DeltaOpen);
            }
            else if (armarx::Contains(shape, "delta fingers open", true))
            {
                controller->queueDeltaShapeCommand(ShapeController::DeltaOpen,
                                                   ShapeController::DeltaNone);
            }
            else if (armarx::Contains(shape, "delta Thumb open", true))
            {
                controller->queueDeltaShapeCommand(ShapeController::DeltaNone,
                                                   ShapeController::DeltaOpen);
            }
            else if (armarx::Contains(shape, "delta close", true))
            {
                controller->queueDeltaShapeCommand(ShapeController::DeltaClose,
                                                   ShapeController::DeltaClose);
            }
            else if (armarx::Contains(shape, "delta fingers close", true))
            {
                controller->queueDeltaShapeCommand(ShapeController::DeltaClose,
                                                   ShapeController::DeltaNone);
            }
            else if (armarx::Contains(shape, "delta thumb close", true))
            {
                controller->queueDeltaShapeCommand(ShapeController::DeltaNone,
                                                   ShapeController::DeltaClose);
            }
            else if (armarx::Contains(shape, "half open", true))
            {
                controller->queueTargetShape(ShapeController::ShapeHalfOpen,
                                             ShapeController::ShapeHalfOpen);
            }
            else if (armarx::Contains(shape, "open", true))
            {
                controller->queueTargetShape(ShapeController::ShapeOpen,
                                             ShapeController::ShapeOpen);
            }
            else if (armarx::Contains(shape, "close", true))
            {
                controller->queueTargetShape(ShapeController::ShapeClosed,
                                             ShapeController::ShapeClosed);
            }
            else
            {
                ARMARX_ERROR << "shape '" << shape << "' not supported.";
            }
        }

        std::string
        describeHandState() const override
        {
            std::stringstream ss;
            ss << njoint_controller::ShapeController::HandShapeStateToString(
                controller->getActualFingerState());
            ss << " ";
            ss << controller->getFingerPwm();
            ss << " / ";
            ss << njoint_controller::ShapeController::HandShapeStateToString(
                controller->getActualThumbState());
            ss << " ";
            ss << controller->getThumbPwm();

            return ss.str();
        }

        void
        setJointAngles(const armarx::NameValueMap& targetJointAngles) override
        {
            ARMARX_WARNING
                << deactivateSpam()
                << "setJointAngles() called, but KITHandV1 does not support joint angles!";
        }

        std::map<std::string, float>
        getActualJointValues() override
        {
            ARMARX_WARNING
                << deactivateSpam()
                << "getCurrentJointValues() called, but KITHandV1 does not support joint angles!";
            return armarx::NameValueMap();
        }
    };


    common::AbstractHandUnitControllerWrapperPtr
    Device::createHandUnitControllerWrapper(armarx::RobotUnit* robotUnit,
                                            const std::string& handName) const
    {
        HandControllerWrapperV1Ptr wrapper(new HandControllerWrapperV1());
        njoint_controller::ShapeControllerConfigPtr cfg =
            new njoint_controller::ShapeControllerConfig(getDeviceName());
        wrapper->controller = njoint_controller::ShapeControllerPtr::dynamicCast(
            robotUnit->createNJointController("NJointKITHandShapeController",
                                              handName + "_NJointKITHandShapeController",
                                              cfg,
                                              false,
                                              true));
        wrapper->controller->setConfigs(fingersConfig, thumbConfig);
        return wrapper;
    }

} // namespace devices::ethercat::hand::armar6_v1
