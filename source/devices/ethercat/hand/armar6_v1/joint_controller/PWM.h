#pragma once


#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>

#include <devices/ethercat/hand/armar6_v1/Data.h>
#include <devices/ethercat/hand/armar6_v1/Device.h>


namespace devices::ethercat::hand::armar6_v1::joint_controller
{

    namespace ctrl_modes
    {
        static const std::string HandPWM = "ControlMode_HandPwm";
    }


    class HandPWMControlTarget : public armarx::ControlTargetBase
    {

    public:
        std::int16_t pwm_finger_motor;
        std::int16_t pwm_thumb_motor;

        const std::string&
        getControlMode() const override
        {
            return ctrl_modes::HandPWM;
        }

        void
        reset() override
        {
            pwm_finger_motor = pwm_thumb_motor = std::numeric_limits<int16_t>::max();
        }

        bool
        isValid() const override
        {
            return std::abs(pwm_finger_motor) < 256 && std::abs(pwm_thumb_motor) < 256;
        }

        static ControlTargetInfo<HandPWMControlTarget>
        GetClassMemberInfo()
        {
            ControlTargetInfo<HandPWMControlTarget> cti;
            cti.addMemberVariable(&HandPWMControlTarget::pwm_finger_motor, "pwm_finger_motor");
            cti.addMemberVariable(&HandPWMControlTarget::pwm_thumb_motor, "pwm_thumb_motor");
            return cti;
        }

        DETAIL_ControlTargetBase_DEFAULT_METHOD_IMPLEMENTATION
    };


    class PWM : public armarx::JointController
    {

    public:
        PWM(Data* data, Device* hand);

        // JointController interface

    public:
        armarx::ControlTargetBase* getControlTarget() override;

    protected:
        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;
        void rtPreActivateController() override;
        void rtPostDeactivateController() override;

    private:
        Data* data;
        Device* hand;
        HandPWMControlTarget target;
    };

} // namespace devices::ethercat::hand::armar6_v1::joint_controller
