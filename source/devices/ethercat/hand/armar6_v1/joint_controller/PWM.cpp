#include "PWM.h"


namespace devices::ethercat::hand::armar6_v1::joint_controller
{

    PWM::PWM(Data* data, Device* hand) : data(data), hand(hand)
    {
    }


    armarx::ControlTargetBase*
    PWM::getControlTarget()
    {
        return &target;
    }


    void
    PWM::rtRun(const IceUtil::Time& sensorValuesTimestamp,
               const IceUtil::Time& timeSinceLastIteration)
    {
        if (target.isValid())
        {
            //ARMARX_IMPORTANT << "rtRun: " << VAROUT(target.pwm_finger_motor) << " " << VAROUT(target.pwm_thumb_motor);
            data->setCommand(static_cast<Command>(Command::PWM),
                             target.pwm_finger_motor,
                             target.pwm_thumb_motor);
        }
    }


    void
    PWM::rtPreActivateController()
    {
    }


    void
    PWM::rtPostDeactivateController()
    {
        data->setCommand(Command::STOP, 0, 0);
    }

} // namespace devices::ethercat::hand::armar6_v1::joint_controller
