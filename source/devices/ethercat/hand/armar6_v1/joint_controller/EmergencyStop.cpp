#include "EmergencyStop.h"


namespace devices::ethercat::hand::armar6_v1::joint_controller
{

    EmergencyStop::EmergencyStop(Data* dataPtr) : dataPtr(dataPtr)
    {
    }


    void
    EmergencyStop::rtRun(const IceUtil::Time& sensorValuesTimestamp,
                         const IceUtil::Time& timeSinceLastIteration)
    {
    }


    armarx::ControlTargetBase*
    EmergencyStop::getControlTarget()
    {
        return &target;
    }


    void
    EmergencyStop::rtPreActivateController()
    {
        ARMARX_INFO << "Stopping hand!";
        dataPtr->setCommand(Command::STOP, 0, 0);
    }

} // namespace devices::ethercat::hand::armar6_v1::joint_controller
