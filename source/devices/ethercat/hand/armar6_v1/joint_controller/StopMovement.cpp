#include "StopMovement.h"


namespace devices::ethercat::hand::armar6_v1::joint_controller
{

    StopMovement::StopMovement(Data* dataPtr) : dataPtr(dataPtr)
    {
    }


    void
    StopMovement::rtRun(const IceUtil::Time& sensorValuesTimestamp,
                        const IceUtil::Time& timeSinceLastIteration)
    {
    }


    armarx::ControlTargetBase*
    StopMovement::getControlTarget()
    {
        return &target;
    }


    void
    StopMovement::rtPreActivateController()
    {
        ARMARX_INFO << "Stopping hand!";
        dataPtr->setCommand(Command::STOP, 0, 0);
    }

} // namespace devices::ethercat::hand::armar6_v1::joint_controller
