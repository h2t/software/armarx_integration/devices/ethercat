/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <memory>

// armarx
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <armarx/control/ethercat/DataInterface.h>
#include <armarx/control/rt_filters/AverageFilter.h>
#include <armarx/control/rt_filters/FirFilter.h>
#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <RobotAPI/components/units/RobotUnit/Devices/ControlDevice.h>
#include <RobotAPI/components/units/RobotUnit/Devices/SensorDevice.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <RobotAPI/interface/units/KinematicUnitInterfaceStdOverloads.h>
#include <RobotAPI/libraries/core/Pose.h>

// robot_devices
#include <devices/ethercat/hand/armar6_v1/Slave.h>
#include <devices/ethercat/hand/armar6_v1/Config.h>


namespace devices::ethercat::hand::armar6_v1
{

    class SensorDataValue : public armarx::SensorValueBase
    {

    public:

        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION
        float motorTemperatureFingers;
        float motorTemperatureThumb;
        float target_pwm_finger_motor;
        float target_pwm_thumb_motor;

        float direction_finger_motor;
        float direction_thumb_motor;

        float actual_finger_pwm;
        float actual_finger_direction;
        float actual_thumb_pwm;
        float actual_thumb_direction;

        Eigen::Quaternionf imuQuaternion;
        Eigen::Vector3f linearAcceleration;

        float accelerometerTemperature;

        static SensorValueInfo<SensorDataValue> GetClassMemberInfo()
        {
            SensorValueInfo<SensorDataValue> svi;
            svi.addMemberVariable(&SensorDataValue::motorTemperatureFingers, "motorTemperatureFingers");
            svi.addMemberVariable(&SensorDataValue::motorTemperatureThumb, "motorTemperatureThumb");
            svi.addMemberVariable(&SensorDataValue::target_pwm_finger_motor, "target_pwm_finger_motor");
            svi.addMemberVariable(&SensorDataValue::target_pwm_thumb_motor, "target_pwm_thumb_motor");
            svi.addMemberVariable(&SensorDataValue::direction_finger_motor, "direction_finger_motor");
            svi.addMemberVariable(&SensorDataValue::direction_thumb_motor, "direction_thumb_motor");
            svi.addMemberVariable(&SensorDataValue::imuQuaternion, "imuQuaternion");
            svi.addMemberVariable(&SensorDataValue::linearAcceleration, "linearAcceleration");
            svi.addMemberVariable(&SensorDataValue::accelerometerTemperature, "accelerometerTemperature");
            svi.addMemberVariable(&SensorDataValue::actual_finger_pwm, "actual_finger_pwm");
            svi.addMemberVariable(&SensorDataValue::actual_finger_direction, "actual_finger_direction");
            svi.addMemberVariable(&SensorDataValue::actual_thumb_pwm, "actual_thumb_pwm");
            svi.addMemberVariable(&SensorDataValue::actual_thumb_direction, "actual_thumb_direction");
            return svi;
        }

    };


    enum class Command : std::uint8_t
    {
        STOP = 0x00,
        CLOSE_HAND = 0x10,
        HALF_CLOSE_HAND = 0x11,
        CLOSE_FINGERS = 0x15,
        CLOSE_THUMB = 0x16,
        OPEN_HAND = 0x20,
        HALF_OPEN_HAND = 0x21,
        OPEN_FINGERS = 0x25,
        OPEN_THUMB = 0x2A,
        CLOSE_HOLD_5SEC_OPEN = 0x40,
        PWM = 0xFF
    };


    inline std::string KITHandCommandToString(Command command)
    {
        switch (command)
        {
            case Command::STOP:
                return "STOP";
            case Command::CLOSE_HAND:
                return "CLOSE_HAND";
            case Command::HALF_CLOSE_HAND:
                return "HALF_CLOSE_HAND";
            case Command::CLOSE_FINGERS:
                return "CLOSE_FINGERS";
            case Command::CLOSE_THUMB:
                return "CLOSE_THUMB";
            case Command::OPEN_HAND:
                return "OPEN_HAND";
            case Command::HALF_OPEN_HAND:
                return "HALF_OPEN_HAND";
            case Command::OPEN_FINGERS:
                return "OPEN_FINGERS";
            case Command::OPEN_THUMB:
                return "OPEN_THUMB";
            case Command::CLOSE_HOLD_5SEC_OPEN:
                return "CLOSE_HOLD_5SEC_OPEN";
            case Command::PWM:
                return "PWM";
            default:
                return "Unknown";
        };
    }


    class Data :
        public armarx::control::ethercat::DataInterface
    {

    public:

        Data(DataConfig& config,
             SlaveOut* sensorOUT, SlaveIn* sensorIN);

        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        void setCommand(Command command, std::int16_t fingerPwm, std::int16_t thumbPwm);

        float getMotorTemperatureFingers() const;

        float getMotorTemperatureThumb() const;
        float getTargetPWMFingerMotor() const;
        float getTargetPWMThumbMotor() const;
        float getFingerMotorDirection() const;
        float getThumbMotorDirection() const;
        const Eigen::Quaternionf& getImutQuaternion() const;
        const Eigen::Vector3f& getLinearAcceleration() const;
        float getAccelerometerTemperature() const;

        std::uint8_t getActualFingerPwm() const;
        std::uint8_t getActualFingerDirection() const;
        std::uint8_t getActualThumbPwm() const;
        std::uint8_t getActualThumbDirection() const;

    private:

        SlaveOut* sensorOUT;
        SlaveIn* sensorIN;
        std::atomic<std::uint8_t> command;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> motorTemperatureFingers;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> motorTemperatureThumb;

        std::uint8_t target_pwm_finger_motor;
        std::uint8_t target_pwm_thumb_motor;
        std::uint8_t direction_finger_motor;
        std::uint8_t direction_thumb_motor;

        armarx::control::ethercat::LinearConvertedValue<std::int16_t> imuQuatW;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> imuQuatX;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> imuQuatY;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> imuQuatZ;
        Eigen::Quaternionf imuQuaternion;
        Eigen::Vector3f linearAcceleration;

        armarx::control::ethercat::LinearConvertedValue<std::int16_t> linearAccX;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> linearAccY;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> linearAccZ;
        armarx::control::ethercat::LinearConvertedValue<std::uint8_t> accelerometerTemperature;

    };

}
