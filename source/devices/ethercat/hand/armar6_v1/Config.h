#pragma once

#include <armarx/control/hardware_config/DeviceConfig.h>

namespace devices::ethercat::hand::armar6_v1
{
    namespace hwconfig = armarx::control::hardware_config;
    struct DataConfig
    {
        DataConfig(hwconfig::DeviceConfig& hwConfig)
            : motorTemperature(hwConfig.getLinearConfig("motorTemperature")),
              imuQuaternion(hwConfig.getLinearConfig("imuQuaternion")),
              accelerometer(hwConfig.getLinearConfig("accelerometer"))
        {

        }
        hwconfig::types::LinearConfig motorTemperature;
        hwconfig::types::LinearConfig imuQuaternion;
        hwconfig::types::LinearConfig accelerometer;
    };

    struct CommandSequenceConfig
    {
        CommandSequenceConfig(std::string name, hwconfig::Config& cfg)
            : delay(cfg.getInt(name + "Delay")),
              activePwm(cfg.getInt(name + "ActivePwm")),
              duration(cfg.getInt(name + "Duration")),
              holdPwm(cfg.getInt(name + "HoldPwm"))
        {
        }
        std::int16_t delay;
        std::int16_t activePwm;
        std::int16_t duration;
        std::int16_t holdPwm;
    };

    struct ShapeConfig
    {
        ShapeConfig(hwconfig::Config& controllerConfig)
            : fullClose("FullClose", controllerConfig),
              halfClose("HalfClose", controllerConfig),
              none("None", controllerConfig),
              halfOpen("HalfOpen", controllerConfig),
              fullOpen("FullOpen", controllerConfig),
              deltaOpen("DeltaOpen", controllerConfig),
              deltaClose("DeltaClose", controllerConfig)
        {

        }
        CommandSequenceConfig fullClose;
        CommandSequenceConfig halfClose;
        CommandSequenceConfig none;
        CommandSequenceConfig halfOpen;
        CommandSequenceConfig fullOpen;
        CommandSequenceConfig deltaOpen;
        CommandSequenceConfig deltaClose;
    };
}
