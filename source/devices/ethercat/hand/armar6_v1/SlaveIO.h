#pragma once


// STD/STL
#include <cstdint>


namespace devices::ethercat::hand::armar6_v1
{

    /**
     * @brief PDO mapping sensorB->master
     */
    struct SlaveOut
    {
        //Actual motor state
        std::uint8_t pwm_finger_motor;
        std::uint8_t pwm_thumb_motor;
        std::uint8_t direction_finger_motor;
        std::uint8_t direction_thumb_motor;

        // accelerometer (linear acceleration)
        std::int16_t x;
        std::int16_t y;
        std::int16_t z;
        std::uint8_t accelerometerTemperature;
        std::uint8_t pad_1;

        // motor temperature
        std::int16_t motor_fingers_temperature;
        std::int16_t motor_thumb_temperature;

        // IMU 0
        std::int16_t IMU0QuaternionW;
        std::int16_t IMU0QuaternionX;
        std::int16_t IMU0QuaternionY;
        std::int16_t IMU0QuaternionZ;

        // IMU 1 - not used
        std::int16_t IMU1QuaternionW;
        std::int16_t IMU1QuaternionX;
        std::int16_t IMU1QuaternionY;
        std::int16_t IMU1QuaternionZ;

    } __attribute__((__packed__));


    /**
     * @brief PDO mapping master->sensorB
     */
    struct SlaveIn
    {
        // motor state
        std::uint8_t pwm_finger_state;
        std::uint8_t pwm_thumb_state;
        std::uint8_t direction_finger_motor;
        std::uint8_t direction_thumb_motor;

        // command register
        std::uint8_t command;
        std::int16_t pad1;
        std::int8_t pad2;

        // hand controller - not used
        std::int16_t target_angle;
        std::int16_t pad3;

    } __attribute__((__packed__));

}
