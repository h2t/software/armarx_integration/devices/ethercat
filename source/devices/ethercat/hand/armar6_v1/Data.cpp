/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, butgetActualTorque
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Data.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>

namespace devices::ethercat::hand::armar6_v1
{

    Data::Data(DataConfig& config,
               SlaveOut* sensorOUT, SlaveIn* sensorIN) :
        sensorOUT(sensorOUT),
        sensorIN(sensorIN)
    {
        command = 0;

        ARMARX_CHECK_EXPRESSION(sensorOUT);
        motorTemperatureFingers.init(&sensorOUT->motor_fingers_temperature, config.motorTemperature);
        motorTemperatureThumb.init(&sensorOUT->motor_thumb_temperature, config.motorTemperature);

        /*target_pwm_finger_motor.init(&sensorOUT->pwm_finger_motor, conversionNode.first_node("pwmMotor"));
        target_pwm_thumb_motor.init(&sensorOUT->pwm_thumb_motor, conversionNode.first_node("pwmMotor"));
        direction_finger_motor.init(&sensorOUT->direction_finger_motor, conversionNode.first_node("motorDirection"));
        direction_thumb_motor.init(&sensorOUT->direction_thumb_motor, conversionNode.first_node("motorDirection"));*/

        imuQuatW.init(&sensorOUT->IMU0QuaternionW, config.imuQuaternion);
        imuQuatX.init(&sensorOUT->IMU0QuaternionX, config.imuQuaternion);
        imuQuatY.init(&sensorOUT->IMU0QuaternionY, config.imuQuaternion);
        imuQuatZ.init(&sensorOUT->IMU0QuaternionZ, config.imuQuaternion);

        linearAccX.init(&sensorOUT->x, config.accelerometer);
        linearAccY.init(&sensorOUT->y, config.accelerometer);
        linearAccZ.init(&sensorOUT->z, config.accelerometer);

        accelerometerTemperature.init(&sensorOUT->accelerometerTemperature, config.accelerometer);
    }


    void Data::rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        motorTemperatureFingers.read();
        motorTemperatureThumb.read();

        imuQuatW.read();
        imuQuatX.read();
        imuQuatY.read();
        imuQuatZ.read();
        imuQuaternion.w() = imuQuatW.value;
        imuQuaternion.x() = imuQuatX.value;
        imuQuaternion.y() = imuQuatY.value;
        imuQuaternion.z() = imuQuatZ.value;

        linearAccX.read();
        linearAccY.read();
        linearAccZ.read();
        linearAcceleration.x() = linearAccX.value;
        linearAcceleration.y() = linearAccY.value;
        linearAcceleration.z() = linearAccZ.value;

        accelerometerTemperature.read();
    }


    void Data::rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        sensorIN->command = command;
        sensorIN->direction_finger_motor = direction_finger_motor;
        sensorIN->direction_thumb_motor = direction_thumb_motor;
        sensorIN->pwm_finger_state = target_pwm_finger_motor;
        sensorIN->pwm_thumb_state = target_pwm_thumb_motor;
    }


    void Data::setCommand(Command command, std::int16_t fingerPwm, std::int16_t thumbPwm)
    {
        this->command = static_cast<std::uint8_t>(command);
        this->direction_finger_motor = fingerPwm > 0 ? 1 : 0;
        this->direction_thumb_motor = thumbPwm > 0 ? 1 : 0;
        this->target_pwm_finger_motor = std::abs(fingerPwm);
        this->target_pwm_thumb_motor = std::abs(thumbPwm);
    }


    float Data::getMotorTemperatureFingers() const
    {
        return motorTemperatureFingers.value;
    }


    float Data::getMotorTemperatureThumb() const
    {
        return motorTemperatureThumb.value;
    }


    float Data::getTargetPWMFingerMotor() const
    {
        return target_pwm_finger_motor;
    }


    float Data::getTargetPWMThumbMotor() const
    {
        return target_pwm_thumb_motor;
    }


    float Data::getFingerMotorDirection() const
    {
        return direction_finger_motor;
    }


    float Data::getThumbMotorDirection() const
    {
        return direction_thumb_motor;
    }


    const Eigen::Quaternionf& Data::getImutQuaternion() const
    {
        return imuQuaternion;
    }


    const Eigen::Vector3f& Data::getLinearAcceleration() const
    {
        return linearAcceleration;
    }


    float Data::getAccelerometerTemperature() const
    {
        return accelerometerTemperature.value;
    }


    std::uint8_t Data::getActualFingerPwm() const
    {
        return sensorOUT->pwm_finger_motor;
    }


    std::uint8_t Data::getActualFingerDirection() const
    {
        return sensorOUT->direction_finger_motor;
    }


    std::uint8_t Data::getActualThumbPwm() const
    {
        return sensorOUT->pwm_thumb_motor;
    }


    std::uint8_t Data::getActualThumbDirection() const
    {
        return sensorOUT->direction_thumb_motor;
    }

}
