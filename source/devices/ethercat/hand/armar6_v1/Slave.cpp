#include "Slave.h"

#include <iostream>

#include <ethercat.h>

#include <ArmarXCore/core/logging/Logging.h>
#include <armarx/control/ethercat/Bus.h>

namespace devices::ethercat::hand::armar6_v1
{

    Slave::Slave(const armarx::control::ethercat::SlaveIdentifier& slaveIdentifier) :
        SlaveInterfaceWithIO(slaveIdentifier)
    {
        inputs = nullptr;
        outputs = nullptr;

        setTag(this->slaveIdentifier.getName());
    }


    void
    Slave::execute()
    {
    }


    bool
    Slave::prepareForRun()
    {
        ARMARX_INFO << deactivateSpam(5) << "KITHandSlave " << getSlaveIdentifier() << " is ready";
        return true;
    }


    bool
    Slave::shutdown()
    {
        return true;
    }


    void
    Slave::doMappings()
    {
    }


    bool
    Slave::hasError()
    {
        return false;
    }


    void
    Slave::prepareForSafeOp()
    {
    }


    void
    Slave::finishPreparingForSafeOp()
    {
    }

    void
    Slave::prepareForOp()
    {
    }


    void
    Slave::finishPreparingForOp()
    {
    }

    std::string
    Slave::getDefaultName()
    {
        return "Armar6HandV1";
    }

    bool
    Slave::isSlaveIdentifierAccepted(const armarx::control::ethercat::SlaveIdentifier& sid)
    {
        // TODO: Implement.
        return false;
    }
} // namespace devices::ethercat::hand::armar6_v1
