/*
     * This file is part of ArmarX.
     *
     * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
     * Karlsruhe Institute of Technology (KIT), all rights reserved.
     *
     * ArmarX is free software; you can redistribute it and/or modify
     * it under the terms of the GNU General Public License version 2 as
     * published by the Free Software Foundation.
     *
     * ArmarX is distributed in the hope that it will be useful, butgetActualTorque
     * WITHOUT ANY WARRANTY; without even the implied warranty of
     * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
     * GNU General Public License for more details.
     *
     * You should have received a copy of the GNU General Public License
     * along with this program. If not, see <http://www.gnu.org/licenses/>.
     *
     * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
     * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
     *             GNU General Public License
     */

#include "Data.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <devices/ethercat/common/elmo/gold/Slave.h>
#include <devices/ethercat/common/sensor_board/armar6/Slave.h>

namespace devices::ethercat::sensor_actor_unit::armar6
{

    Data::Data(common::elmo::gold::ElmoConfig elmoConfig,
               common::elmo::gold::SlaveOut* elmo_out,
               common::elmo::gold::SlaveIn* elmo_in,
               common::sensor_board::armar6::SensorBoardConfig sensorBoardConfig,
               common::sensor_board::armar6::SlaveOut* sensorOUT) :
        common::elmo::gold::Data(elmoConfig,
                                 elmo_out,
                                 elmo_in),
        sensorOUT(sensorOUT)
    {
        ARMARX_CHECK_EXPRESSION(sensorOUT);
        position.init(&rawABSEncoderTicks, sensorBoardConfig.position);
        torque.init(&sensorOUT->TorqueValue, sensorBoardConfig.actualTorque);
        torqueSensorTemperature.init(&sensorOUT->TorqueSensorTemperature,
                                     sensorBoardConfig.torqueTemperature,
                                     std::nan("1"),
                                     false);

        motorTemperature.init(&sensorOUT->MotorTemperature, sensorBoardConfig.motorTemperature);
        gearTemperature.init(&sensorOUT->GearboxTemperature, sensorBoardConfig.gearTemperature);
        dieTemperature.init(&sensorOUT->DieTemperature, sensorBoardConfig.gearTemperature);
        elmoTemperature.init(&sensorOUT->ElmoTemperature, sensorBoardConfig.elmoTemperature);

        imuQuatW.init(&sensorOUT->IMUQuaternionW, sensorBoardConfig.imuQuaternion);
        imuQuatX.init(&sensorOUT->IMUQuaternionX, sensorBoardConfig.imuQuaternion);
        imuQuatY.init(&sensorOUT->IMUQuaternionY, sensorBoardConfig.imuQuaternion);
        imuQuatZ.init(&sensorOUT->IMUQuaternionZ, sensorBoardConfig.imuQuaternion);

        imuAccelerationX.init(&sensorOUT->IMUVector1[0], sensorBoardConfig.imuAcceleration);
        imuAccelerationY.init(&sensorOUT->IMUVector1[1], sensorBoardConfig.imuAcceleration);
        imuAccelerationZ.init(&sensorOUT->IMUVector1[2], sensorBoardConfig.imuAcceleration);

        imuGyroX.init(&sensorOUT->IMUVector2[0], sensorBoardConfig.imuGyro);
        imuGyroY.init(&sensorOUT->IMUVector2[1], sensorBoardConfig.imuGyro);
        imuGyroZ.init(&sensorOUT->IMUVector2[2], sensorBoardConfig.imuGyro);
    }

    void
    Data::rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                             const IceUtil::Time& timeSinceLastIteration)
    {
        //    ARMARX_IMPORTANT << "1";
        //    float lastActualVelocity = velocity.value;
        common::elmo::gold::Data::rtReadSensorValues(sensorValuesTimestamp, timeSinceLastIteration);
        convertRawAbsEncoderValue();
        //    ARMARX_IMPORTANT << "2";
        //    ARMARX_IMPORTANT << "3";
        position.read();
        // keep value between center-180 and center+180 deg
        position.value = shiftAngleToLimits(position.value);
        torque.read();

        torqueSensorTemperature.read();
        motorTemperature.read();
        gearTemperature.read();
        dieTemperature.read();
        elmoTemperature.read();

        imuQuatW.read();
        imuQuatX.read();
        imuQuatY.read();
        imuQuatZ.read();

        imuAccelerationX.read();
        imuAccelerationY.read();
        imuAccelerationZ.read();

        imuGyroX.read();
        imuGyroY.read();
        imuGyroZ.read();

        imuQuat.w() = imuQuatW.value;
        imuQuat.x() = imuQuatX.value;
        imuQuat.y() = imuQuatY.value;
        imuQuat.z() = imuQuatZ.value;

        imuAcceleration(0) = imuAccelerationX.value;
        imuAcceleration(1) = imuAccelerationY.value;
        imuAcceleration(2) = imuAccelerationZ.value;

        imuGyro(0) = imuGyroX.value;
        imuGyro(1) = imuGyroY.value;
        imuGyro(2) = imuGyroZ.value;

        //    ARMARX_INFO << deactivateSpam(1) << "length: " << imuAcceleration.norm();
    }

    const float&
    Data::getActualPosition()
    {
        return position.value;
    }

    const float&
    Data::getActualTorque()
    {
        return torque.value;
    }

    const float&
    Data::getTorqueTemperature()
    {
        return torqueSensorTemperature.value;
    }

    const float&
    Data::getActualMotorTemperature()
    {
        return motorTemperature.value;
    }

    const float&
    Data::getActualElmoTemperature()
    {
        return elmoTemperature.value;
    }

    const float&
    Data::getActualGearTemperature()
    {
        return gearTemperature.value;
    }

    const float&
    Data::getActualDieTemperature()
    {
        return dieTemperature.value;
    }

    const float&
    Data::getActualTorqueSensorTemperature() const
    {
        return torqueSensorTemperature.value;
    }

    void
    Data::setTorqueFilter(const armarx::control::rt_filters::FirFilter& value)
    {
        torqueFilter = value;
    }

    const Eigen::Quaternionf&
    Data::getImuQuaternion()
    {
        return imuQuat;
    }

    const Eigen::Vector3f&
    Data::getImuAcceleration() const
    {
        return imuAcceleration;
    }

    const Eigen::Vector3f&
    Data::getImuGyro() const
    {
        return imuGyro;
    }

    float
    Data::getSensorBoardUpdateRate() const
    {
        return sensorOUT->SensorboardUpdateRate;
    }

    float
    Data::getI2CUpdateRate() const
    {
        return sensorOUT->I2CUpdateRate;
    }

    void
    Data::convertRawAbsEncoderValue()
    {
        rawABSEncoderTicks =
            ((static_cast<std::uint32_t>(sensorOUT->RawABSEncoderValueBytes[0]) << 24 |
              static_cast<std::uint32_t>(sensorOUT->RawABSEncoderValueBytes[1]) << 16 |
              static_cast<std::uint32_t>(sensorOUT->RawABSEncoderValueBytes[2]) << 8 |
              static_cast<std::uint32_t>(sensorOUT->RawABSEncoderValueBytes[3])) &
             0xFFFFF000) >>
            12;

        //    rawABSEncoderTicks = sensorOUT->RawABSEncoderValue & 0xFFFFF000 >> 12;
    }

    //float JointData::getGravityTorque() const
    //{
    //    return gravityTorque;
    //}

    //void JointData::setGravityTorque(float value)
    //{
    //    gravityTorque = value;
    //}

    std::int32_t
    Data::getTorqueTicks()
    {
        return torque.getRaw();
    }


    std::uint32_t
    Data::getAbsoluteEncoderTicks()
    {
        return rawABSEncoderTicks;
    }

} // namespace devices::ethercat::sensor_actor_unit::armar6
