/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <memory>

// armarx
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValueIMU.h>
#include <armarx/control/ethercat/DataInterface.h>
#include <armarx/control/rt_filters/FirFilter.h>

// robot_devices
#include <devices/ethercat/common/elmo/gold/Data.h>
#include <devices/ethercat/common/sensor_board/armar6/Config.h>


namespace devices::ethercat::common::sensor_board::armar6
{
    class SlaveOut;
}


namespace devices::ethercat::sensor_actor_unit::armar6
{

    class SensorValueArmar6Actuator :
        virtual public armarx::SensorValue1DoFRealActuatorWithStatus,
        virtual public armarx::SensorValueIMU,
        virtual public armarx::SensorValue1DoFActuatorFilteredVelocity
    {

    public:
        SensorValueArmar6Actuator() = default;

        SensorValueArmar6Actuator(const SensorValueArmar6Actuator&) = default;

        SensorValueArmar6Actuator& operator=(const SensorValueArmar6Actuator&) = default;

        SensorValueArmar6Actuator(SensorValueArmar6Actuator&& o)
        {
            *this = o;
        }

        SensorValueArmar6Actuator& operator=(SensorValueArmar6Actuator&& o)
        {
            *this = o;
            return *this;
        }

        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION

        float gearTemperature = 0.0f;
        float dieTemperature = 0.0f;
        float relativePosition = 0.0f;
        float torqueSensorTemperature = 0.0f;
        float sensorBoardUpdateRate = 0.0f;
        float I2CUpdateRate = 0.0f;
        float elmoTemperature = 0.0f;
        float currentTarget = 0.0f;
        float positionTarget = 0.0f;
        float velocityTarget = 0.0f;
        float maxTargetCurrent = 0.0f;
        float gravityCompensatedTorque = 0.0f;
        std::uint32_t absoluteEncoderTicks = 0;
        std::int32_t torqueTicks = 0;

        static SensorValueInfo<SensorValueArmar6Actuator> GetClassMemberInfo()
        {
            SensorValueInfo<SensorValueArmar6Actuator> svi;
            svi.addBaseClass<SensorValue1DoFRealActuatorWithStatus>();
            svi.addBaseClass<SensorValueIMU>();
            svi.addBaseClass<SensorValue1DoFActuatorFilteredVelocity>();
            svi.addMemberVariable(&SensorValueArmar6Actuator::gearTemperature, "gearTemperature");
            svi.addMemberVariable(&SensorValueArmar6Actuator::dieTemperature, "dieTemperature");
            svi.addMemberVariable(&SensorValueArmar6Actuator::relativePosition, "relativePosition");
            svi.addMemberVariable(&SensorValueArmar6Actuator::torqueSensorTemperature, "torqueSensorTemperature");
            svi.addMemberVariable(&SensorValueArmar6Actuator::I2CUpdateRate, "I2CUpdateRate");
            svi.addMemberVariable(&SensorValueArmar6Actuator::sensorBoardUpdateRate, "sensorBoardUpdateRate");
            svi.addMemberVariable(&SensorValueArmar6Actuator::elmoTemperature, "elmoTemperature");
            svi.addMemberVariable(&SensorValueArmar6Actuator::currentTarget, "currentTarget");
            svi.addMemberVariable(&SensorValueArmar6Actuator::positionTarget, "positionTarget");
            svi.addMemberVariable(&SensorValueArmar6Actuator::velocityTarget, "velocityTarget");
            svi.addMemberVariable(&SensorValueArmar6Actuator::maxTargetCurrent, "maxTargetCurrent");
            svi.addMemberVariable(&SensorValueArmar6Actuator::absoluteEncoderTicks, "absoluteEncoderTicks");
            svi.addMemberVariable(&SensorValueArmar6Actuator::torqueTicks, "torqueTicks");
            svi.addMemberVariable(&SensorValueArmar6Actuator::gravityCompensatedTorque, "gravityCompensatedTorque");
            return svi;
        }

    };


    class Data;
    using DataPtr = std::shared_ptr<Data>;


    class Data :
        public common::elmo::gold::Data
    {

    public:

        Data(common::elmo::gold::ElmoConfig elmoConfig,
             common::elmo::gold::SlaveOut* elmo_out,
             common::elmo::gold::SlaveIn* elmo_in,
             common::sensor_board::armar6::SensorBoardConfig sensorBoardConfig,
             common::sensor_board::armar6::SlaveOut* sensorOUT);

        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

        const float& getActualPosition();

        const float& getActualTorque();
        const float& getTorqueTemperature();

        const float& getActualMotorTemperature();
        const float& getActualElmoTemperature();

        const float& getActualGearTemperature();
        const float& getActualDieTemperature();

        const float& getActualTorqueSensorTemperature() const;

        void setTorqueFilter(const armarx::control::rt_filters::FirFilter& value);

        const Eigen::Quaternionf& getImuQuaternion();

        const Eigen::Vector3f& getImuAcceleration() const;

        const Eigen::Vector3f& getImuGyro() const;

        float getSensorBoardUpdateRate() const;
        float getI2CUpdateRate() const;

        // float getGravityTorque() const;
        // void setGravityTorque(float value);

        std::int32_t getTorqueTicks();

        std::uint32_t getAbsoluteEncoderTicks();

    private:

        void convertRawAbsEncoderValue();

        common::sensor_board::armar6::SlaveOut* sensorOUT;

        std::uint32_t rawABSEncoderTicks;
        armarx::control::ethercat::LinearConvertedValue<std::uint32_t> position;
        armarx::control::ethercat::LinearConvertedValue<std::int32_t> torque;
        armarx::control::ethercat::LinearConvertedValue<std::int32_t> torqueSensorTemperature;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> motorTemperature;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> gearTemperature;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> dieTemperature;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> elmoTemperature;

        // armarx::control::ethercat::LinearConvertedValue<Eigen::Matrix<int16, 3, 1> > imuAcceleration;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> imuAccelerationX;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> imuAccelerationY;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> imuAccelerationZ;

        armarx::control::ethercat::LinearConvertedValue<std::int16_t> imuGyroX;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> imuGyroY;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> imuGyroZ;

        armarx::control::ethercat::LinearConvertedValue<std::int16_t> imuQuatW;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> imuQuatX;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> imuQuatY;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> imuQuatZ;

        Eigen::Quaternionf imuQuat;
        Eigen::Vector3f imuAcceleration;
        Eigen::Vector3f imuGyro;

        armarx::control::rt_filters::FirFilter torqueFilter;
        float gravityTorque;

    };

}  // namespace devices::ethercat::sensor_actor_unit::v1
