#include "ActiveImpedance.h"


#include <devices/ethercat/sensor_actor_unit/armar6/Data.h>
#include <devices/ethercat/sensor_actor_unit/armar6/Device.h>


namespace devices::ethercat::sensor_actor_unit::armar6::joint_controller
{
    ActiveImpedanceController::ActiveImpedanceController(Device* joint,
            DataPtr jointData,
            armarx::control::joint_controller::TorqueControllerConfigurationPtr torqueConfigData) : JointController(),
        target(),
        joint(joint),
        pid(),
        targetFilter(100)
    {
        dataPtr = jointData;

        pid.proportional_gain = torqueConfigData->pid_proportional_gain; //
        pid.integral_gain = torqueConfigData->pid_integral_gain; //
        pid.derivative_gain = torqueConfigData->pid_derivative_gain; //
        //    pid.windup_guard = torqueConfigData->pid_windup_guard;
        pid.max_value = torqueConfigData->pid_max_value;
        //    pid.min_value = torqueConfigData->pid_min_value;
        //    pid.dis = torqueConfigData->pid_dis;
        //    pid.Kd = torqueConfigData->Kd;
        //    pid.inertia = torqueConfigData->inertia;
        //    pid.scalePI = torqueConfigData->scalePI;
        //    pid.scaleP = torqueConfigData->scaleP;
        //    pid.actuatorType = torqueConfigData->actuatorType;

        //  coefficients for Lowpass FIR-Filter
        filter.setImpulseResponse(
        {
            0.0073f, 0.0011f, 0.0012f, 0.0013f, 0.0014f, 0.0015f, 0.0016f, 0.0017f, 0.0018f, 0.0019f, 0.0021f, 0.0022f, 0.0023f,
            0.0024f, 0.0026f, 0.0027f, 0.0028f, 0.0029f, 0.0031f, 0.0032f, 0.0034f, 0.0035f, 0.0036f, 0.0038f, 0.0039f, 0.0041f,
            0.0043f, 0.0044f, 0.0046f, 0.0047f, 0.0049f, 0.0050f, 0.0052f, 0.0054f, 0.0055f, 0.0057f, 0.0058f, 0.0060f, 0.0062f,
            0.0063f, 0.0065f, 0.0066f, 0.0068f, 0.0070f, 0.0071f, 0.0073f, 0.0074f, 0.0076f, 0.0077f, 0.0078f, 0.0080f, 0.0081f,
            0.0083f, 0.0084f, 0.0085f, 0.0087f, 0.0088f, 0.0089f, 0.0090f, 0.0091f, 0.0092f, 0.0093f, 0.0094f, 0.0095f, 0.0096f,
            0.0097f, 0.0098f, 0.0098f, 0.0099f, 0.0100f, 0.0100f, 0.0101f, 0.0101f, 0.0102f, 0.0102f, 0.0102f, 0.0103f, 0.0103f,
            0.0103f, 0.0103f, 0.0103f, 0.0103f, 0.0103f, 0.0103f, 0.0102f, 0.0102f, 0.0102f, 0.0101f, 0.0101f, 0.0100f, 0.0100f,
            0.0099f, 0.0098f, 0.0098f, 0.0097f, 0.0096f, 0.0095f, 0.0094f, 0.0093f, 0.0092f, 0.0091f, 0.0090f, 0.0089f, 0.0088f,
            0.0087f, 0.0085f, 0.0084f, 0.0083f, 0.0081f, 0.0080f, 0.0078f, 0.0077f, 0.0076f, 0.0074f, 0.0073f, 0.0071f, 0.0070f,
            0.0068f, 0.0066f, 0.0065f, 0.0063f, 0.0062f, 0.0060f, 0.0058f, 0.0057f, 0.0055f, 0.0054f, 0.0052f, 0.0050f, 0.0049f,
            0.0047f, 0.0046f, 0.0044f, 0.0043f, 0.0041f, 0.0039f, 0.0038f, 0.0036f, 0.0035f, 0.0034f, 0.0032f, 0.0031f, 0.0029f,
            0.0028f, 0.0027f, 0.0026f, 0.0024f, 0.0023f, 0.0022f, 0.0021f, 0.0019f, 0.0018f, 0.0017f, 0.0016f, 0.0015f, 0.0014f,
            0.0013f, 0.0012f, 0.0011f, 0.0073f
        });

        filterVel.setImpulseResponse(
        {
            0.1744f, 0.0205f, 0.0215f, 0.0225f, 0.0235f, 0.0244f, 0.0252f, 0.0259f, 0.0265f, 0.0270f, 0.0274f, 0.0276f, 0.0278f,
            0.0282f, 0.0282f, 0.0278f, 0.0276f, 0.0274f, 0.0270f, 0.0265f, 0.0259f, 0.0252f, 0.0244f, 0.0235f, 0.0225f, 0.0215f,
            0.0205f, 0.1744f
        });

    }

    void ActiveImpedanceController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        float setPosition = target.position;
        float setSpringConst = target.kp;
        float actualPosition = dataPtr->getActualPosition();
        float actualTorque = dataPtr->getActualTorque();
        auto sensorValue = joint->getSensorValue()->asA<armarx::SensorValue1DoFGravityTorque>();
        ARMARX_CHECK_EXPRESSION(sensorValue);
        float gravity = sensorValue->gravityTorque;
        float actualVelocity = filterVel.update(dataPtr->getActualVelocity());

        double dt = timeSinceLastIteration.toSecondsDouble();

        float error = setPosition - actualPosition;
        if (dataPtr->isLimitLess())
        {
            error = armarx::math::MathUtils::angleModPI(error);
        }

        float desTorque = error * setSpringConst;

        float actualTorqueF = filter.update(actualTorque);

        /*float Kd = 30;

        if ((setSpringConst > 100))
        {
            Kd = 40;
        }

        if ((setSpringConst > 200))
        {
            Kd = 60;
        }

        pid.proportional_gain = 0.0;

        if (pid.actuatorType == 1)
        {
            pid.integral_gain = 0.95; //(-0.003721 * pow(Kd, 2.0) + 0.43429 * Kd - 1.4408) * (scalePI * 0.5);
        }
        else
        {
            pid.integral_gain = 1.1 * pid.scalePI; //0.5
            pid.proportional_gain = pid.scaleP; // 0.02
        }*/

        float desVelocity = 0.55; //4.*0 std::abs(error);

        if (desVelocity > 0.55)
        {
            desVelocity = 0.55;
        }

        if (std::abs(error) < 0.2)
        {
            desVelocity = 0.45;
        }

        if (std::abs(error) < 0.15)
        {
            desVelocity = 0.35;
        }

        if (std::abs(error) < 0.1)
        {
            desVelocity = 0.25;
        }

        if (std::abs(error) < 0.05)
        {
            desVelocity = 0.15;
        }

        if (std::abs(error) < 0.01)
        {
            desVelocity = 0.1;
        }

        float Kd = (-gravity + desTorque + actualTorqueF) / std::max(0.0001f, std::abs(desVelocity));

        ARMARX_RT_LOGF_INFO("Kd: %.2f", Kd).deactivateSpam(0.15);
        ARMARX_RT_LOGF_INFO("actualTorqueF: %.2f", actualTorqueF).deactivateSpam(0.15);

        Kd = fabs(Kd);

        Kd = std::min(3000000.f, Kd);

        //if (std::abs(error)<0.05){
        //    Kd=20;
        //}

        if (pid.actuatorType == 1)
        {
            pid.integral_gain = 1.0 * pid.scalePI;
            pid.proportional_gain = 0.1 * pid.scalePI;
            if (std::abs(Kd) < 10)
            {
                pid.proportional_gain = 0.15 * pid.scalePI;
            }
        }
        else if (pid.actuatorType == 2)
        {
            pid.integral_gain = 2.5 * pid.scalePI;
            pid.proportional_gain = 0.22;
            if (std::abs(Kd) < 15)
            {
                pid.proportional_gain = 0.1 * pid.scalePI;
            }
        }

        else if (pid.actuatorType == 3)
        {
            pid.integral_gain = 1.5 * pid.scalePI;
            pid.proportional_gain = 0.12;
        }

        if (pid.integral_gain < 0)
        {
            pid.integral_gain = 0.001;
            pid.proportional_gain = 0.0;
        }

        if (std::abs(desVelocity) < 0.005)
        {
            //adjustedTargetVelocity = 0.0f;
            pid.integral_gain = 0.1;
            pid.proportional_gain = 0.0;
        }

        if (std::abs(Kd) > 200)
        {
            pid.integral_gain = pid.integral_gain / 2.0;
            pid.proportional_gain = pid.proportional_gain / 2.0;
        }

        if (std::abs(Kd) > 500)
        {
            pid.integral_gain = pid.integral_gain / 2.5;
            pid.proportional_gain = pid.proportional_gain / 2.5;
        }

        if (std::abs(Kd) > 1000)
        {
            pid.integral_gain = pid.integral_gain / 3.0;
            pid.proportional_gain = pid.proportional_gain / 3.0;
        }

        if (std::abs(Kd) > 3000)
        {
            pid.integral_gain = pid.integral_gain / 4.0;
            pid.proportional_gain = pid.proportional_gain / 4.0;
        }

        if (std::abs(Kd) > 6000)
        {
            pid.integral_gain = 0.001;
            pid.proportional_gain = 0.0;
        }

        /*Kd = fabs(Kd);

        Kd = std::min(3000000.f, Kd);

        ARMARX_RT_LOGF_INFO("Kd: %.2f", Kd).deactivateSpam(0.1);
        ARMARX_RT_LOGF_INFO("desTorque: %.2f", desTorque).deactivateSpam(0.1);
        ARMARX_RT_LOGF_INFO("desVelocity: %.2f", desVelocity).deactivateSpam(0.1);

        if (pid.actuatorType == 1)
        {
            pid.integral_gain = 2.3 * pid.scalePI;
            pid.proportional_gain = 0.13 * pid.scalePI;
            if (std::abs(Kd) < 10)
            {
                pid.proportional_gain = 0.03 * pid.scalePI;
            }
        }
        else if (pid.actuatorType == 2)
        {
            pid.integral_gain = 2.5 * pid.scalePI;
            pid.proportional_gain = 0.22;
            if (std::abs(Kd) < 15)
            {
                pid.proportional_gain = 0.1 * pid.scalePI;
            }
        }

        else if (pid.actuatorType == 3)
        {
            pid.integral_gain = 2.5 * pid.scalePI;
            pid.proportional_gain = 0.22;
        }

        if (pid.integral_gain < 0)
        {
            pid.integral_gain = 0.001;
            pid.proportional_gain = 0.0;
        }

        if (std::abs(desVelocity) < 0.005)
        {
            //adjustedTargetVelocity = 0.0f;
            pid.integral_gain = 0.1;
            pid.proportional_gain = 0.0;
        }

        if (std::abs(Kd) > 1500)
        {
            Kd = 1500;
        }

        if (std::abs(Kd) > 300)
        {
            pid.integral_gain = 0.0000001;
            pid.proportional_gain = 0.0;
        }*/

        float setTorque = (-gravity + desTorque - Kd * actualVelocity);

        double CurrError = setTorque + actualTorqueF;

        pid.update(CurrError, dt);
        //pid.update(desTorque - Kd * actualVelocity, dt);

        float current = -(pid.control) * 1000.0;

        dataPtr->setTargetCurrent(current);
    }


    armarx::ControlTargetBase* ActiveImpedanceController::getControlTarget()
    {
        return &target;
    }


    armarx::control::rt_filters::FirFilter ActiveImpedanceController::getFilter() const
    {
        return filter;
    }


    void ActiveImpedanceController::rtPreActivateController()
    {
        joint->switchControlMode(armarx::eTorqueControl);
        dataPtr->setTargetCurrent(0.0f);
    }


    ActiveImpedanceControllerConfigurationPtr ActiveImpedanceControllerConfiguration::CreateTorqueConfigDataFromXml(armarx::DefaultRapidXmlReaderNode node)
    {
        ActiveImpedanceControllerConfiguration configData;
        ARMARX_CHECK_EXPRESSION(node.is_valid());
        configData.pid_proportional_gain = (double) node.first_node("pid_proportional_gain").value_as_float();
        configData.pid_integral_gain = (double) node.first_node("pid_integral_gain").value_as_float();
        configData.pid_derivative_gain = (double) node.first_node("pid_derivative_gain").value_as_float();
        configData.pid_windup_guard = (double) node.first_node("pid_windup_guard").value_as_float();
        configData.pid_max_value = (double) node.first_node("pid_max_value").value_as_float();
        configData.pid_min_value = (double) node.first_node("pid_min_value").value_as_float();
        configData.pid_dis = (double) node.first_node("pid_dis").value_as_float();
        configData.Kd = (double) node.first_node("Kd").value_as_float();
        configData.inertia = (double) node.first_node("inertia").value_as_float();

        auto v = node.first_node("firFilterImpulseResponse").value_as_string();
        for (auto& elem  : armarx::Split(v, ",", true))
        {
            configData.firFilterImpulseResponse.push_back(armarx::toFloat(elem));
        }

        return std::make_shared<ActiveImpedanceControllerConfiguration>(configData);
    }


    ActiveImpedanceControllerConfiguration::ActiveImpedanceControllerConfiguration()
    {
    }

}
