#include "VelocityTorque.h"


#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
//#include <RobotAPI/libraries/core/math/MathUtils.h>

#include <devices/ethercat/sensor_actor_unit/armar6/Data.h>
#include <devices/ethercat/sensor_actor_unit/armar6/Device.h>


namespace devices::ethercat::sensor_actor_unit::armar6::joint_controller
{
    VelocityTorqueController::VelocityTorqueController(Device* joint,
            DataPtr jointData,
            armarx::control::joint_controller::TorqueControllerConfigurationPtr torqueConfigData)
        : JointController(),
          target(),
          joint(joint),
          pid(),
          targetFilter(100)
    {
        defaultMaxTorque = torqueConfigData->maxTorque;

        dataPtr = jointData;

        pid.proportional_gain = torqueConfigData->pid_proportional_gain; //
        pid.integral_gain = torqueConfigData->pid_integral_gain; //
        pid.derivative_gain = torqueConfigData->pid_derivative_gain; //
        pid.windup_guard = torqueConfigData->pid_windup_guard;
        pid.max_value = torqueConfigData->pid_max_value;
        pid.min_value = torqueConfigData->pid_min_value;
        pid.dis = torqueConfigData->pid_dis;
        pid.Kd = torqueConfigData->Kd;
        pid.inertia = torqueConfigData->inertia;
        pid.scalePI = torqueConfigData->scalePI;
        pid.maxTorque = torqueConfigData->maxTorque;
        pid.actuatorType = torqueConfigData->actuatorType;

        //  coefficients for low-pass FIR-Filter for torque
        filter.setImpulseResponse(
        {
            0.0071f, 0.0007f, 0.0007f, 0.0008f, 0.0008f, 0.0008f, 0.0009f, 0.0009f, 0.0010f, 0.0010f, 0.0010f, 0.0011f, 0.0011f,
            0.0012f, 0.0012f, 0.0012f, 0.0013f, 0.0013f, 0.0014f, 0.0014f, 0.0015f, 0.0015f, 0.0016f, 0.0016f, 0.0017f, 0.0017f,
            0.0018f, 0.0018f, 0.0019f, 0.0020f, 0.0020f, 0.0021f, 0.0021f, 0.0022f, 0.0022f, 0.0023f, 0.0023f, 0.0024f, 0.0025f,
            0.0025f, 0.0026f, 0.0026f, 0.0027f, 0.0028f, 0.0028f, 0.0029f, 0.0030f, 0.0030f, 0.0031f, 0.0031f, 0.0032f, 0.0033f,
            0.0033f, 0.0034f, 0.0035f, 0.0035f, 0.0036f, 0.0036f, 0.0037f, 0.0038f, 0.0038f, 0.0039f, 0.0040f, 0.0040f, 0.0041f,
            0.0041f, 0.0042f, 0.0043f, 0.0043f, 0.0044f, 0.0045f, 0.0045f, 0.0046f, 0.0046f, 0.0047f, 0.0048f, 0.0048f, 0.0049f,
            0.0049f, 0.0050f, 0.0050f, 0.0051f, 0.0051f, 0.0052f, 0.0052f, 0.0053f, 0.0053f, 0.0054f, 0.0054f, 0.0055f, 0.0055f,
            0.0056f, 0.0056f, 0.0057f, 0.0057f, 0.0058f, 0.0058f, 0.0058f, 0.0059f, 0.0059f, 0.0060f, 0.0060f, 0.0060f, 0.0061f,
            0.0061f, 0.0061f, 0.0061f, 0.0062f, 0.0062f, 0.0062f, 0.0063f, 0.0063f, 0.0063f, 0.0063f, 0.0063f, 0.0064f, 0.0064f,
            0.0064f, 0.0064f, 0.0064f, 0.0064f, 0.0064f, 0.0064f, 0.0064f, 0.0064f, 0.0064f, 0.0064f, 0.0064f, 0.0064f, 0.0064f,
            0.0064f, 0.0064f, 0.0064f, 0.0064f, 0.0064f, 0.0064f, 0.0064f, 0.0064f, 0.0064f, 0.0063f, 0.0063f, 0.0063f, 0.0063f,
            0.0063f, 0.0062f, 0.0062f, 0.0062f, 0.0061f, 0.0061f, 0.0061f, 0.0061f, 0.0060f, 0.0060f, 0.0060f, 0.0059f, 0.0059f,
            0.0058f, 0.0058f, 0.0058f, 0.0057f, 0.0057f, 0.0056f, 0.0056f, 0.0055f, 0.0055f, 0.0054f, 0.0054f, 0.0053f, 0.0053f,
            0.0052f, 0.0052f, 0.0051f, 0.0051f, 0.0050f, 0.0050f, 0.0049f, 0.0049f, 0.0048f, 0.0048f, 0.0047f, 0.0046f, 0.0046f,
            0.0045f, 0.0045f, 0.0044f, 0.0043f, 0.0043f, 0.0042f, 0.0041f, 0.0041f, 0.0040f, 0.0040f, 0.0039f, 0.0038f, 0.0038f,
            0.0037f, 0.0036f, 0.0036f, 0.0035f, 0.0035f, 0.0034f, 0.0033f, 0.0033f, 0.0032f, 0.0031f, 0.0031f, 0.0030f, 0.0030f,
            0.0029f, 0.0028f, 0.0028f, 0.0027f, 0.0026f, 0.0026f, 0.0025f, 0.0025f, 0.0024f, 0.0023f, 0.0023f, 0.0022f, 0.0022f,
            0.0021f, 0.0021f, 0.0020f, 0.0020f, 0.0019f, 0.0018f, 0.0018f, 0.0017f, 0.0017f, 0.0016f, 0.0016f, 0.0015f, 0.0015f,
            0.0014f, 0.0014f, 0.0013f, 0.0013f, 0.0012f, 0.0012f, 0.0012f, 0.0011f, 0.0011f, 0.0010f, 0.0010f, 0.0010f, 0.0009f,
            0.0009f, 0.0008f, 0.0008f, 0.0008f, 0.0007f, 0.0007f, 0.0071f
        });

        //  coefficients for low-pass FIR-Filter for velocity
        filter_vel.setImpulseResponse(
        {
            0.1744f, 0.0205f, 0.0215f, 0.0225f, 0.0235f, 0.0244f, 0.0252f, 0.0259f, 0.0265f, 0.0270f, 0.0274f, 0.0276f, 0.0278f,
            0.0282f, 0.0282f, 0.0278f, 0.0276f, 0.0274f, 0.0270f, 0.0265f, 0.0259f, 0.0252f, 0.0244f, 0.0235f, 0.0225f, 0.0215f,
            0.0205f, 0.1744f
        });

        velCtrl.acceleration = 4.5;
        velCtrl.deceleration = 4.5;
        velCtrl.maxDt = 0.003;
        velCtrl.maxV = 0.6;
        velCtrl.directSetVLimit = 0.005;
        //    controller.positionLimitHiHard = dataPtr->getHardLimitHi();
        velCtrl.positionLimitHiSoft = dataPtr->getSoftLimitHi();
        //    controller.positionLimitLoHard = dataPtr->getHardLimitLo();
        velCtrl.positionLimitLoSoft = dataPtr->getSoftLimitLo();

    }


    void VelocityTorqueController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        float maxTorque = target.maxTorque < 0 ? defaultMaxTorque : target.maxTorque;
        pid.maxTorque = maxTorque;

        velCtrl.currentPosition = dataPtr->getActualPosition();
        velCtrl.currentV = lastSetVelocity;
        velCtrl.dt = timeSinceLastIteration.toSecondsDouble();
        velCtrl.targetV = target.velocity;

        float adjustedTargetVelocity = velCtrl.run();
        lastSetVelocity = adjustedTargetVelocity;

        adjustedTargetVelocity = filter_vel.update(target.velocity);
        //adjustedTargetVelocity = target.velocity;

        float actualVelocity = dataPtr->getActualVelocity();

        //float actualPosition = dataPtr->getActualPosition();

        /*float setPosition = 0.0;
        float setSpringConst = (120 / (PI / 2));
        maxTorque = (setPosition - actualPosition) * setSpringConst;
        adjustedTargetVelocity = 10.0 * std::abs(setPosition - actualPosition);
        if (adjustedTargetVelocity > 0.35)
        {
            adjustedTargetVelocity = 0.35;
        }
        if (adjustedTargetVelocity < 0.05)
        {
            adjustedTargetVelocity = 0.05;
        }

        ARMARX_INFO << deactivateSpam(0.3) << "maxTorque: " << maxTorque;
        ARMARX_INFO << deactivateSpam(0.3) << "adjustedTargetVelocity: " << adjustedTargetVelocity;*/

        auto sensorValue = joint->getSensorValue()->asA<armarx::SensorValue1DoFGravityTorque>();
        ARMARX_CHECK_EXPRESSION(sensorValue);
        float gravity = sensorValue->gravityTorque;
        float actualTorque = dataPtr->getActualTorque();
        std::string JointName = joint->getJointName();
        double dt = timeSinceLastIteration.toSecondsDouble();

        pid.findAcc(actualVelocity, dt);

        if (adjustedTargetVelocity < 0)
        {
            maxTorque = -maxTorque;
        }

        if (adjustedTargetVelocity == 0)
        {
            maxTorque = 0.0;
        }

        if (pid.actuatorType == 1)
        {
            actualTorque = filter.update(actualTorque);
        }

        //double actualTorque_hp = hp_filter.update(actualTorque) - gravity; //- (gravity - pid.acc * pid.inertia));

        //double actualTorque_lp = lp_filter.update(actualTorque) - gravity; //- (gravity)); //- pid.acc * pid.inertia));

        //double diff = actualTorque_lp - actualTorque_hp;

        float T_st = (1 / (((0.3 / 0.001) + 1)));
        float maxTorque_f = ((T_st * ((actualTorque - pid.old_pos)) + pid.old_pos));
        pid.old_pos = maxTorque_f;

        if (pid.status)
        {
            adjustedTargetVelocity = 0.0;
        }

        float Kd = (-gravity + maxTorque + actualTorque) / std::max(0.0001f, std::abs(adjustedTargetVelocity));

        //ARMARX_RT_LOGF_INFO("Kd: %.2f", Kd).deactivateSpam(0.3);

        Kd = fabs(Kd);

        Kd = std::min(3000000.f, Kd);

        if (pid.actuatorType == 1)
        {
            pid.integral_gain = 2.3 * pid.scalePI;
            pid.proportional_gain = 0.15 * pid.scalePI;
            if (std::abs(Kd) < 10)
            {
                pid.proportional_gain = 0.15 * pid.scalePI;
            }
        }
        else if (pid.actuatorType == 2)
        {
            pid.integral_gain = 2.5 * pid.scalePI;
            pid.proportional_gain = 0.22;
            if (std::abs(Kd) < 15)
            {
                pid.proportional_gain = 0.1 * pid.scalePI;
            }
        }
        else if (pid.actuatorType == 3)
        {
            pid.integral_gain = 1.5 * pid.scalePI;
            pid.proportional_gain = 0.12;
        }

        if (pid.integral_gain < 0)
        {
            pid.integral_gain = 0.001;
            pid.proportional_gain = 0.0;
        }

        if (std::abs(adjustedTargetVelocity) < 0.005)
        {
            //adjustedTargetVelocity = 0.0f;
            pid.integral_gain = 0.1;
            pid.proportional_gain = 0.0;

        }

        if (std::abs(Kd) > 200)
        {
            pid.integral_gain = pid.integral_gain / 2.0;
            pid.proportional_gain = pid.proportional_gain / 2.0;
        }

        if (std::abs(Kd) > 500)
        {
            pid.integral_gain = pid.integral_gain / 2.5;
            pid.proportional_gain = pid.proportional_gain / 2.5;
        }

        if (std::abs(Kd) > 1000)
        {
            pid.integral_gain = pid.integral_gain / 3.0;
            pid.proportional_gain = pid.proportional_gain / 3.0;
        }

        /*if (std::abs(Kd) > 3000)
        {
            pid.integral_gain = pid.integral_gain / 4.0;
            pid.proportional_gain = pid.proportional_gain / 4.0;
        }

        if (std::abs(Kd) > 6000)
        {
            pid.integral_gain = pid.integral_gain / 6.0;
            pid.proportional_gain = pid.proportional_gain / 6.0;
        }*/

        if (std::abs(Kd) > 12000)
        {
            pid.integral_gain = 0.005;
            pid.proportional_gain = 0.0;
            if (pid.actuatorType == 1)
            {
                pid.integral_gain = 0.001;
                pid.proportional_gain = 0.0;
            }
        }

        //    ARMARX_RT_LOGF_INFO("pid.integral_gain: %.4f", pid.integral_gain).deactivateSpam(0.2);
        //   ARMARX_RT_LOGF_INFO("Kd: %.4f", Kd).deactivateSpam(0.2);

        //gravity = gravity + pid.acc * pid.inertia;

        float setTorque = (-gravity + maxTorque - Kd * actualVelocity);

        double CurrError = setTorque + actualTorque;

        pid.update(CurrError, dt);

        float current = -(pid.control) * 1000.0;


        // send the current value to Elmo
        dataPtr->setTargetCurrent(current);

        //ARMARX_RT_LOGF_INFO("Target current: %.2f", current).deactivateSpam(0.3);
    }


    armarx::ControlTargetBase* VelocityTorqueController::getControlTarget()
    {
        return &target;
    }


    armarx::control::rt_filters::FirFilter VelocityTorqueController::getFilter() const
    {
        return filter;
    }


    void VelocityTorqueController::rtPreActivateController()
    {
        joint->switchControlMode(armarx::eTorqueControl);
        dataPtr->setTargetCurrent(0.0f);
        lastSetVelocity = dataPtr->getActualVelocity();
        target.velocity = lastSetVelocity;
    }


    VelocityTorqueControllerConfigurationPtr VelocityTorqueControllerConfiguration::CreateTorqueConfigDataFromXml(armarx::DefaultRapidXmlReaderNode node)
    {
        VelocityTorqueControllerConfiguration configData;
        ARMARX_CHECK_EXPRESSION(node.is_valid());
        configData.pid_proportional_gain = (double) node.first_node("pid_proportional_gain").value_as_float();
        configData.pid_integral_gain = (double) node.first_node("pid_integral_gain").value_as_float();
        configData.pid_derivative_gain = (double) node.first_node("pid_derivative_gain").value_as_float();
        configData.pid_windup_guard = (double) node.first_node("pid_windup_guard").value_as_float();
        configData.pid_max_value = (double) node.first_node("pid_max_value").value_as_float();
        configData.pid_min_value = (double) node.first_node("pid_min_value").value_as_float();
        configData.pid_dis = (double) node.first_node("pid_dis").value_as_float();
        configData.Kd = (double) node.first_node("Kd").value_as_float();
        configData.inertia = (double) node.first_node("inertia").value_as_float();
        configData.maxTorque = (double) node.first_node("maxTorque").value_as_float();
        configData.actuatorType = (double) node.first_node("actuatorType").value_as_float();

        auto v = node.first_node("firFilterImpulseResponse").value_as_string();
        for (auto& elem  : armarx::Split(v, ",", true))
        {
            configData.firFilterImpulseResponse.push_back(armarx::toFloat(elem));
        }

        return std::make_shared<VelocityTorqueControllerConfiguration>(configData);
    }


    VelocityTorqueControllerConfiguration::VelocityTorqueControllerConfiguration()
    {

    }

}
