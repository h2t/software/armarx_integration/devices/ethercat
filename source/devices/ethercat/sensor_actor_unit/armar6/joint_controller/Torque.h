#pragma once


// armarx
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <armarx/control/rt_filters/AverageFilter.h>
#include <armarx/control/rt_filters/FirFilter.h>
#include <armarx/control/joint_controller/Torque.h>


namespace devices::ethercat::sensor_actor_unit::armar6
{
    class Device;
    using DataPtr = std::shared_ptr<class Data>;
}


namespace devices::ethercat::sensor_actor_unit::armar6::joint_controller
{
    using TorqueControllerPtr = std::shared_ptr<class TorqueController>;


    class TorqueController :
        public armarx::JointController
    {

    public:

        TorqueController(Device* joint, DataPtr jointData, armarx::control::joint_controller::TorqueControllerConfigurationPtr torqueConfigData);

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        armarx::ControlTargetBase* getControlTarget() override;

        armarx::control::rt_filters::FirFilter getFilter() const;

    protected:

        void rtPreActivateController() override;

    private:

        armarx::ControlTarget1DoFActuatorTorque target;
        DataPtr dataPtr;
        Device* joint;
        armarx::control::joint_controller::TorqueController torqueController;

    };

}
