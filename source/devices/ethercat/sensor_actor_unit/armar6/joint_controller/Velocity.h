#pragma once


// armarx
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/BasicControllers.h>
#include <armarx/control/hardware_config/Config.h>


namespace devices::ethercat::sensor_actor_unit::armar6
{
    class Device;
    using DataPtr = std::shared_ptr<class Data>;
}


namespace devices::ethercat::sensor_actor_unit::armar6::joint_controller
{
    using VelocityControllerPtr = std::shared_ptr<class VelocityController>;

    inline void FillRampedVelocityControllerConfiguration(armarx::RampedAccelerationVelocityControllerConfiguration& config, armarx::control::hardware_config::Config& hwConfig)
    {
        config.maxVelocityRad = hwConfig.getFloat("maxVelocityRad");
        config.maxDecelerationRad = hwConfig.getFloat("maxDecelerationRad");
        config.jerk = hwConfig.getFloat("jerk");
        config.maxDt = hwConfig.getFloat("maxDt");
        config.directSetVLimit = hwConfig.getFloat("directSetVLimit");
    }

    inline armarx::RampedAccelerationVelocityControllerConfigurationPtr CreateRampedVelocityControllerConfiguration(armarx::control::hardware_config::Config& hwConfig)
    {
        armarx::RampedAccelerationVelocityControllerConfigurationPtr config(new armarx::RampedAccelerationVelocityControllerConfiguration());
        FillRampedVelocityControllerConfiguration(*config, hwConfig);
        return config;
    }

    class VelocityController : public armarx::JointController
    {

    public:

        VelocityController(Device* joint,
                           DataPtr jointData,
                           armarx::RampedAccelerationVelocityControllerConfigurationPtr velocityControllerConfigDataPtr);

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override ;
        armarx::ControlTargetBase* getControlTarget() override;

        void rtPreActivateController() override;

    private:

        armarx::ControlTarget1DoFActuatorVelocity target;
        armarx::VelocityControllerWithRampedAccelerationAndPositionBounds controller;
        DataPtr dataPtr;
        Device* joint;
        double lastTargetVelocity = 0.0;
        double lastTargetAcceleration = 0.0;
        bool limitless;
        //DEBUG
        bool setStart;
        IceUtil::Time start;

    };

}
