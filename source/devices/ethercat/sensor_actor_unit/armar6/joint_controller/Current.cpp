#include "Current.h"


#include <devices/ethercat/sensor_actor_unit/armar6/Data.h>
#include <devices/ethercat/sensor_actor_unit/armar6/Device.h>


namespace devices::ethercat::sensor_actor_unit::armar6::joint_controller
{

    CurrentController::CurrentController(Device* joint, DataPtr jointData) : JointController(),
        target(),
        joint(joint)
    {
        dataPtr = jointData;
    }


    void CurrentController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        if (target.isValid())
        {
            dataPtr->setTargetCurrent(target.current);
        }
    }


    armarx::ControlTargetBase* CurrentController::getControlTarget()
    {
        return &target;
    }


    void CurrentController::rtPreActivateController()
    {
        joint->switchControlMode(armarx::eTorqueControl);
        dataPtr->setTargetCurrent(0.0f);
    }

}
