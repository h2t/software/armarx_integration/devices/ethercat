#pragma once


// armarx
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>


namespace devices::ethercat::sensor_actor_unit::armar6
{
    class Device;
    using DataPtr = std::shared_ptr<class Data>;
}


namespace devices::ethercat::sensor_actor_unit::armar6::joint_controller
{
    using JointCurrentControllerPtr = std::shared_ptr<class CurrentController>;


    class CurrentController : public armarx::JointController
    {

    public:

        CurrentController(Device* joint, DataPtr jointData);

    protected:

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override ;
        void rtPreActivateController() override;
        armarx::ControlTargetBase* getControlTarget() override;

    private:

        armarx::ControlTarget1DoFActuatorCurrent target;
        DataPtr dataPtr;
        Device* joint;

    };

}
