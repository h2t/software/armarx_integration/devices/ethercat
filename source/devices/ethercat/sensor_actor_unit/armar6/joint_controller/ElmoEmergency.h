#pragma once


// armarx
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>

#include <devices/ethercat/common/elmo/gold/Slave.h>


namespace devices::ethercat::sensor_actor_unit::armar6::joint_controller
{
    template <typename DataTypePtr>
    class ElmoEmergencyController : public armarx::JointController
    {

    public:
        ElmoEmergencyController(devices::ethercat::common::elmo::gold::Slave* elmo,
                                DataTypePtr data) :
            setStart(true)
        {
            dataPtr = data;
            this->elmo = elmo;
        }

        void
        rtRun(const IceUtil::Time& sensorValuesTimestamp,
              const IceUtil::Time& timeSinceLastIteration) override
        {
            //TODO activate Motor break.

            dataPtr->setTargetVelocity(0);

            //    if (setStart)
            //    {
            //        start = std::chrono::high_resolution_clock::now();
            //        setStart = false;
            //    }

            /*
            auto now = std::chrono::high_resolution_clock::now();
            if (std::chrono::duration_cast<std::chrono::milliseconds>((now - start)).count() < 5000)
            {
            }
            else
            {
            armarx::DS402State  currentState = elmo->getState();
            if (currentState != armarx::DS402State::QUICK_STOP_ACTIVE
                && currentState != armarx::DS402State::SWITCH_ON_DISABLED)
            {
                elmo->activateQuickStop();
                elmo->switchMode(armarx::ElmoControlMode::DEFAULT_MODE);
            }

            }*/
        }
        /**
         * Returns the Target for this controller, but as this is the Emergency controller it will ignored.
         * As this controller will just break
         * @return is type VelocityTarget but it will return a nullptr, because it won't be possible to set a target
         */
        armarx::ControlTargetBase*
        getControlTarget() override
        {
            return &target;
        }

    private:
        devices::ethercat::common::elmo::gold::Slave* elmo;
        armarx::DummyControlTargetEmergencyStop target;
        DataTypePtr dataPtr;
        //DEBUG
        bool setStart;
        std::chrono::high_resolution_clock::time_point start;

        // JointController interface
    protected:
        void
        rtPreActivateController() override
        {
            ARMARX_IMPORTANT << "Activating Emergency Stop controller for "
                             << elmo->getSlaveIdentifier().getName();
            elmo->switchMode(devices::ethercat::common::elmo::gold::ElmoControlMode::VELOCITY);
            dataPtr->setTargetDeceleration(6);
            dataPtr->setMaxTargetCurrent(dataPtr->getMaxDefaultTargetCurrent() / 2);
        }

        void
        rtPostDeactivateController() override
        {
            dataPtr->resetMaxTargetCurrent();
        }
    };
} // namespace devices::ethercat::sensor_actor_unit::armar6::joint_controller
