#include "Torque.h"


#include <devices/ethercat/sensor_actor_unit/armar6/Data.h>
#include <devices/ethercat/sensor_actor_unit/armar6/Device.h>


namespace devices::ethercat::sensor_actor_unit::armar6::joint_controller
{

    TorqueController::TorqueController(Device* joint,
                                       DataPtr jointData,
                                       armarx::control::joint_controller::TorqueControllerConfigurationPtr torqueConfigData) : JointController(),
        target(),
        joint(joint),
        torqueController(torqueConfigData)
    {
        dataPtr = jointData;
    }


    void TorqueController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        if (target.isValid())
        {
            auto sensorValue = joint->getSensorValue()->asA<armarx::SensorValue1DoFGravityTorque>();
            ARMARX_CHECK_EXPRESSION(sensorValue);
            float gravity = sensorValue->gravityTorque;

            // std::string jointName = joint->getJointName();

            float actualTorque = dataPtr->getActualTorque();

            float actualPosition = dataPtr->getActualPosition();

            float targetTorque = target.torque;

            //get data from the elmo
            float actualVelocity = dataPtr->getActualVelocity();
            float current = torqueController.update(timeSinceLastIteration, joint->getJointName(), gravity, actualTorque, targetTorque, actualVelocity, actualPosition);
            // send the current value to Elmo

            dataPtr->setTargetCurrent(current);
        }
    }


    armarx::ControlTargetBase* TorqueController::getControlTarget()
    {
        return &target;
    }


    armarx::control::rt_filters::FirFilter TorqueController::getFilter() const
    {
        return torqueController.getFilter();
    }


    void TorqueController::rtPreActivateController()
    {
        joint->switchControlMode(armarx::eTorqueControl);
        dataPtr->setTargetCurrent(0.0f);
        // torqueController.reset();
    }

}
