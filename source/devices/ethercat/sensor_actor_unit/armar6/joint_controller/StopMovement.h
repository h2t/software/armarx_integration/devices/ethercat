#pragma once


// armarx
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/util/RtLogging.h>

#include <devices/ethercat/common/elmo/gold/Slave.h>


namespace devices::ethercat::sensor_actor_unit::armar6::joint_controller
{

    template <typename DataTypePtr>
    class StopMovementController : public armarx::JointController
    {

    public:
        StopMovementController(devices::ethercat::common::elmo::gold::Slave* elmo,
                               DataTypePtr dataPtr,
                               float deceleration = 6.0f) :
            dataPtr(dataPtr), elmo(elmo), deceleration(std::fabs(deceleration))
        {
        }

        void
        rtRun(const IceUtil::Time& sensorValuesTimestamp,
              const IceUtil::Time& timeSinceLastIteration) override
        {
            dataPtr->setTargetVelocity(0);
        }
        /**
         * Returns the Target for this controller, but as this is the Emergency controller it will ignored.
         * As this controller will just break
         * @return is type VelocityTarget but it will return a nullptr, because it won't be possible to set a target
         */
        armarx::ControlTargetBase*
        getControlTarget() override
        {
            return &target;
        }

    private:
        armarx::DummyControlTargetStopMovement target;
        DataTypePtr dataPtr;
        devices::ethercat::common::elmo::gold::Slave* elmo;
        float deceleration;

        // JointController interface
    protected:
        void
        rtPreActivateController() override
        {
            ARMARX_RT_LOGF_IMPORTANT("Activating Stop Movement controller for %s",
                                     elmo->getSlaveIdentifier().getName().c_str());

            elmo->switchMode(devices::ethercat::common::elmo::gold::ElmoControlMode::VELOCITY);
            dataPtr->setTargetDeceleration(deceleration);
        }
    };
} // namespace devices::ethercat::sensor_actor_unit::armar6::joint_controller
