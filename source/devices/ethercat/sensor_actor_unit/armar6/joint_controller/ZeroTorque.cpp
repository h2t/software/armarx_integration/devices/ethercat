/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "ZeroTorque.h"


#include <devices/ethercat/sensor_actor_unit/armar6/Data.h>
#include <devices/ethercat/sensor_actor_unit/armar6/Device.h>


namespace devices::ethercat::sensor_actor_unit::armar6::joint_controller
{

    ZeroTorqueController::ZeroTorqueController(Device* joint,
            DataPtr jointData,
            armarx::control::joint_controller::VelocityManipulatingTorqueControllerConfigurationPtr torqueConfigData) :
        target(),
        joint(joint),
        torqueController(torqueConfigData)
    {
        dataPtr = jointData;
    }


    void ZeroTorqueController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        if (target.isValid())
        {
            auto sensorValue = joint->getSensorValue()->asA<armarx::SensorValue1DoFGravityTorque>();
            ARMARX_CHECK_EXPRESSION(sensorValue);
            float gravity = sensorValue->gravityTorque;

            float actualTorque = dataPtr->getActualTorque();

            float actualPosition = dataPtr->getActualPosition();

            float targetTorque = target.torque;

            //get data from the elmo
            float actualVelocity = dataPtr->getActualVelocity();
            float current = torqueController.update(sensorValuesTimestamp, timeSinceLastIteration, gravity, actualTorque, targetTorque, actualVelocity, actualPosition);
            // send the current value to Elmo
            dataPtr->setTargetCurrent(current);
        }
    }


    armarx::ControlTargetBase* ZeroTorqueController::getControlTarget()
    {
        return &target;
    }


    void ZeroTorqueController::rtPreActivateController()
    {
        joint->switchControlMode(armarx::eTorqueControl);
        dataPtr->setTargetCurrent(0.0f);
        torqueController.reset();
    }
}
