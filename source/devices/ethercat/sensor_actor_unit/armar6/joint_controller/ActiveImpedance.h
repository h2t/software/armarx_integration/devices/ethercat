#pragma once


// armarx
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <armarx/control/rt_filters/AverageFilter.h>
#include <armarx/control/rt_filters/FirFilter.h>
#include <armarx/control/joint_controller/Torque.h>


namespace devices::ethercat::sensor_actor_unit::armar6
{
    class Device;
    using DataPtr = std::shared_ptr<class Data>;
}


namespace devices::ethercat::sensor_actor_unit::armar6::joint_controller
{

    using ActiveImpedanceControllerConfigurationPtr = std::shared_ptr<class ActiveImpedanceControllerConfiguration>;


    class ActiveImpedanceControllerConfiguration
    {

    public:

        ActiveImpedanceControllerConfiguration();
        static ActiveImpedanceControllerConfigurationPtr CreateTorqueConfigDataFromXml(armarx::DefaultRapidXmlReaderNode node);
        double pid_proportional_gain;
        double pid_integral_gain;
        double pid_derivative_gain;
        double pid_windup_guard;
        double pid_max_value;
        double pid_min_value;
        double pid_dis;
        double Kd;
        double inertia;
        std::vector<float> firFilterImpulseResponse;

    };


    using ActiveImpedanceControllerPtr = std::shared_ptr<class ActiveImpedanceController>;


    class ActiveImpedanceController : public armarx::JointController
    {

    public:

        ActiveImpedanceController(Device* joint,
                                  DataPtr jointData,
                                  armarx::control::joint_controller::TorqueControllerConfigurationPtr torqueConfigData);

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        armarx::ControlTargetBase* getControlTarget() override;

        armarx::control::rt_filters::FirFilter getFilter() const;

    private:

        armarx::ActiveImpedanceControlTarget target;
        DataPtr dataPtr;
        Device* joint;

        armarx::control::joint_controller::TorquePID pid;
        armarx::control::rt_filters::FirFilter filter;
        armarx::control::rt_filters::FirFilter filterVel;
        armarx::control::rt_filters::RtAverageFilter targetFilter;

        // JointController interface

    protected:

        void rtPreActivateController() override;

    };

}
