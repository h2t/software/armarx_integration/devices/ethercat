/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <memory>

#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValueIMU.h>
#include <armarx/control/ethercat/DataInterface.h>
#include <armarx/control/rt_filters/AverageFilter.h>

#include <devices/ethercat/common/elmo/gold/Data.h>
#include <devices/ethercat/common/elmo/gold/Config.h>
#include <devices/ethercat/common/sensor_board/armar7de/Data.h>
#include <devices/ethercat/common/sensor_board/armar7de/Config.h>


namespace devices::ethercat::common::sensor_board::armar7de
{
    class SlaveOut;
}


namespace devices::ethercat::sensor_actor_unit::armar7de
{

    class SensorValueArmar7deActuator :
        virtual public armarx::SensorValue1DoFRealActuatorWithStatus,
        virtual public armarx::SensorValueIMU,
        virtual public armarx::SensorValue1DoFActuatorFilteredVelocity
    {

    public:
        SensorValueArmar7deActuator() = default;

        SensorValueArmar7deActuator(const SensorValueArmar7deActuator&) = default;

        SensorValueArmar7deActuator& operator=(const SensorValueArmar7deActuator&) = default;

        SensorValueArmar7deActuator(SensorValueArmar7deActuator&& o)
        {
            *this = o;
        }

        SensorValueArmar7deActuator&
        operator=(SensorValueArmar7deActuator&& o)
        {
            *this = o;
            return *this;
        }

        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION

            float relativePosition = 0.0f;

        float sensorBoardUpdateRate = 0.0f;
        float currentTarget = 0.0f;
        float positionTarget = 0.0f;
        float velocityTarget = 0.0f;
        float maxTargetCurrent = 0.0f;
        float gravityCompensatedTorque = 0.0f;
        std::int32_t torqueTicks = 0;
        float voltage = 0.0f;
        std::uint32_t absEncoderTicks = 0;

        std::uint8_t accelerationAccuracy = 0;
        std::uint8_t orientationAccuracy = 0;

        float torqueAdcTemperature = 0.0f;
        float absoluteEncoderTemperature = 0.0f;

        armarx::control::rt_filters::RtAverageFilter motorTempFilter;

        // error flags
        bool motorTemperatureAdcError = false;
        std::uint8_t relativeEncoderStatus = 0;
        bool emergencyStopStatus = false;
        std::uint16_t absoluteEncoderDetailedStatus = 0;
        bool imuCommunicationError = false;

        // debugging fields
        std::uint16_t motorTemperatureValueErrorCounter = 0;


        static SensorValueInfo<SensorValueArmar7deActuator>
        GetClassMemberInfo()
        {
            SensorValueInfo<SensorValueArmar7deActuator> svi;
            svi.addBaseClass<SensorValue1DoFRealActuatorWithStatus>();
            svi.addBaseClass<SensorValueIMU>();
            svi.addBaseClass<SensorValue1DoFActuatorFilteredVelocity>();
            svi.addMemberVariable(&SensorValueArmar7deActuator::relativePosition,
                                  "relativePosition");
            svi.addMemberVariable(&SensorValueArmar7deActuator::torqueAdcTemperature,
                                  "torqueAdcTemperature");
            svi.addMemberVariable(&SensorValueArmar7deActuator::sensorBoardUpdateRate,
                                  "sensorBoardUpdateRate");
            svi.addMemberVariable(&SensorValueArmar7deActuator::currentTarget, "currentTarget");
            svi.addMemberVariable(&SensorValueArmar7deActuator::positionTarget, "positionTarget");
            svi.addMemberVariable(&SensorValueArmar7deActuator::velocityTarget, "velocityTarget");
            svi.addMemberVariable(&SensorValueArmar7deActuator::maxTargetCurrent,
                                  "maxTargetCurrent");
            svi.addMemberVariable(&SensorValueArmar7deActuator::torqueTicks, "torqueTicks");
            svi.addMemberVariable(&SensorValueArmar7deActuator::gravityCompensatedTorque,
                                  "gravityCompensatedTorque");
            svi.addMemberVariable(&SensorValueArmar7deActuator::voltage, "voltage");
            svi.addMemberVariable(&SensorValueArmar7deActuator::absEncoderTicks,
                                  "absoluteEncoderTicks");
            svi.addMemberVariable(&SensorValueArmar7deActuator::absoluteEncoderTemperature,
                                  "absoluteEncoderTemperature");
            svi.addMemberVariable(&SensorValueArmar7deActuator::accelerationAccuracy,
                                  "accelerationAccuracy");
            svi.addMemberVariable(&SensorValueArmar7deActuator::orientationAccuracy,
                                  "orientationAccuracy");
            svi.addMemberVariable(&SensorValueArmar7deActuator::motorTemperatureAdcError,
                                  "motorTemperatureError");
            svi.addMemberVariable(&SensorValueArmar7deActuator::motorTemperatureValueErrorCounter,
                                  "motorTemperatureValueErrorCounter");
            svi.addMemberVariable(&SensorValueArmar7deActuator::relativeEncoderStatus,
                                  "relativeEncoderStatus");
            svi.addMemberVariable(&SensorValueArmar7deActuator::emergencyStopStatus,
                                  "emergencyStopStatus");
            svi.addMemberVariable(&SensorValueArmar7deActuator::absoluteEncoderDetailedStatus,
                                  "absoluteEncoderDetailedStatus");
            svi.addMemberVariable(&SensorValueArmar7deActuator::imuCommunicationError,
                                  "imuCommunicationError");
            return svi;
        }
    };


    class Data;
    using DataPtr = std::shared_ptr<Data>;


    class Data :
        public virtual common::elmo::gold::Data,
        public virtual common::sensor_board::armar7de::Data
    {

    public:
        Data(common::elmo::gold::ElmoConfig elmoConfig,
             common::elmo::gold::SlaveOut* elmo_out,
             common::elmo::gold::SlaveIn* elmo_in,
             common::sensor_board::armar7de::SensorBoardConfig sensorConfig,
             common::sensor_board::armar7de::SlaveOut* sensor_out,
             common::sensor_board::armar7de::SlaveIn* sensor_in);

        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration) override;

        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                 const IceUtil::Time& timeSinceLastIteration) override;
    };

} // namespace devices::ethercat::sensor_actor_unit::armar7de
