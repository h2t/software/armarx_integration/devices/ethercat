/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Armar6RT::ArmarXObjects::Armar6IceKinematicUnit
 * @author     Markus Swarowsky ( markus dot swarowsky at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Device.h"

#include <VirtualRobot/Robot.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/util/StringHelperTemplates.h>
#include <armarx/control/ethercat/ErrorReporting.h>
#include <armarx/control/joint_controller/ControllerConfiguration.h>
#include <armarx/control/joint_controller/Torque.h>

#include "joint_controller/ActiveImpedance.h"
#include "joint_controller/CurrentPassThrough.h"
#include "joint_controller/ElmoEmergency.h"
#include "joint_controller/Position.h"
#include "joint_controller/StopMovement.h"
#include "joint_controller/Torque.h"
#include "joint_controller/Velocity.h"
#include "joint_controller/VelocityTorque.h"
#include "joint_controller/ZeroTorque.h"
#include <devices/ethercat/common/elmo/gold/Slave.h>
#include <devices/ethercat/common/h2t_devices.h>
#include <devices/ethercat/common/sensor_board/armar6/Slave.h>


namespace devices::ethercat::sensor_actor_unit::armar7de
{

    Device::Device(hwconfig::DeviceConfig& config,
                   const VirtualRobot::RobotPtr& robot) :
        DeviceBase(config.getName()),
        ControlDevice(config.getName()),
        SensorDevice(config.getName()),
        DeviceInterface(config.getName()),
        deviceName{config.getName()},
        robotNode(robot->getRobotNode(deviceName)),
        dataPtr(nullptr),
        elmoIdentifier(config.getSlaveConfig("Elmo").getIdentifier()),
        sensorBoardIdentifier(config.getSlaveConfig("SensorBoard7de").getIdentifier()),
        elmoPtr(nullptr),
        sensorBoardPtr(nullptr),
        sensorBoardConfig(config.getSlaveConfig("SensorBoard7de")),
        elmoConfig(config.getSlaveConfig("Elmo"))
    {
        using namespace armarx::control::joint_controller;

        ARMARX_CHECK_NOT_NULL(robotNode);

        torqueControllerConfigDataPtr =
            TorqueControllerConfiguration::CreateTorqueConfigData(
                config.getControllerConfig("Torque"),
                this->robotNode->isLimitless(),
                this->robotNode->getJointLimitLow(),
                this->robotNode->getJointLimitHigh());
        zeroTorqueControllerConfigDataPtr =
            VelocityManipulatingTorqueControllerConfiguration::CreateTorqueConfigData(
                config.getControllerConfig("ZeroTorque"),
                this->robotNode->isLimitless(),
                this->robotNode->getJointLimitLow(),
                this->robotNode->getJointLimitHigh());
        positionControllerConfigDataPtr =
            PositionControllerConfiguration::CreatePositionControllerConfigData(
                config.getControllerConfig("Position"));
        velocityControllerConfigDataPtr = joint_controller::CreateRampedVelocityControllerConfiguration(
                    config.getControllerConfig("Velocity"));

        enabled = config.getBool("Enabled");
        ARMARX_VERBOSE << "Joint " << getDeviceName() << " enabled: " << enabled;
    }


    armarx::control::ethercat::DeviceInterface::TryAssignResult
    Device::tryAssign(armarx::control::ethercat::SlaveInterface& slave)
    {
        namespace ecat = armarx::control::ethercat;
        using Result = DeviceInterface::TryAssignResult;

        Result result = ecat::tryAssignUsingSerialNo(slave, elmoPtr, elmoIdentifier.serialNumber);
        if (result != Result::unknown)
        {
            return result;
        }

        result = tryAssignUsingSerialNo(slave, sensorBoardPtr, sensorBoardIdentifier.serialNumber);
        if (result != Result::unknown)
        {
            return result;
        }

        return result;
    }


    armarx::control::ethercat::DeviceInterface::AllAssignedResult
    Device::onAllAssigned()
    {
        using Result = DeviceInterface::AllAssignedResult;

        if (elmoPtr and sensorBoardPtr)
        {
            // Initialize Elmo.
            {
                // Set the config parameters for the slaves.
                std::uint32_t ratedTorque = elmoConfig.ratedTorque;
                std::uint32_t ratedCurrent = elmoConfig.ratedCurrent;
                std::uint16_t maxCurrent = static_cast<std::uint16_t>(elmoConfig.maxCurrent);
                elmoPtr->setRatedTorque(ratedTorque);
                elmoPtr->setRatedCurrent(ratedCurrent);
                elmoPtr->setMaxRatedCurrent(static_cast<std::uint16_t>(
                    static_cast<double>(maxCurrent) / (static_cast<double>(ratedCurrent) * 0.001)));

                // Some Elmos have a different value for the STO (Emergency Stop) when
                // active/inactive.
                elmoPtr->setSTO_OnState(elmoConfig.stoOnState);

                ARMARX_DEBUG << "Elmo outputs: " << elmoPtr->getOutputsPtr()
                             << "; Elmo inputs: " << elmoPtr->getInputsPtr();
            }

            // Initialize sensor board.
            {
                // TODO: Maybe set all at once
                sensorBoardPtr->setTorquePgaAndSps(sensorBoardConfig.torquePgaAndSps);
                sensorBoardPtr->setTorqueAdcTempFrequency(sensorBoardConfig.torqueAdcTempFrequency);
                sensorBoardPtr->setDisableImuCalibAccel(sensorBoardConfig.disableCalibAccel ? 1 : 0);
                sensorBoardPtr->setDisableImuCalibGyro(sensorBoardConfig.disableCalibGyro ? 1 : 0);
                sensorBoardPtr->setDisableImuCalibMag(sensorBoardConfig.disableCalibMag ? 1 : 0);
                sensorBoardPtr->setLedStripCount(sensorBoardConfig.ledStripCount);
            }

            return Result::ok;
        }

        return Result::slavesMissing;
    }

    std::string
    Device::getClassName() const
    {
        return "SensorActorUnit_Armar7de";
    }

    void
    Device::postSwitchToSafeOp()
    {
        if (!dataPtr)
        {
            dataPtr = std::make_shared<Data>(elmoConfig,
                                             elmoPtr->getOutputsPtr(),
                                             elmoPtr->getInputsPtr(),
                                             sensorBoardConfig,
                                             sensorBoardPtr->getOutputsPtr(),
                                             sensorBoardPtr->getInputsPtr());
            //with data set we can create the rest of the joint controller...
            //            torqueController = std::make_shared<joint_controller::TorqueController>(
            //                this, dataPtr, torqueControllerConfigDataPtr);
            //            zeroTorqueController = std::make_shared<joint_controller::ZeroTorqueController>(
            //                this, dataPtr, zeroTorqueControllerConfigDataPtr);
            velocityTorqueController = std::make_shared<joint_controller::VelocityTorqueController>(
                this, dataPtr, torqueControllerConfigDataPtr);
            activeImpedanceController =
                std::make_shared<joint_controller::ActiveImpedanceController>(
                    this, dataPtr, torqueControllerConfigDataPtr);
            currentController =
                std::make_shared<joint_controller::CurrentPassThroughController>(this, dataPtr);
            positionController = std::make_shared<joint_controller::PositionController>(
                this, dataPtr, positionControllerConfigDataPtr);
            velocityController = std::make_shared<joint_controller::VelocityController>(
                this, dataPtr, velocityControllerConfigDataPtr);
            stopMovementController =
                std::make_shared<joint_controller::StopMovementController<DataPtr>>(elmoPtr,
                                                                                    dataPtr);
            elmoEmergencyController =
                std::make_shared<joint_controller::ElmoEmergencyController<DataPtr>>(elmoPtr,
                                                                                     dataPtr);
            addJointController(elmoEmergencyController.get());
            addJointController(stopMovementController.get());
            //addJointController(torqueController.get());
            //addJointController(zeroTorqueController.get());
            addJointController(velocityTorqueController.get());
            addJointController(velocityController.get());

            addJointController(activeImpedanceController.get());
            addJointController(currentController.get());
            addJointController(positionController.get());
        }
        ARMARX_CHECK_NOT_NULL(dataPtr);
    }

    void
    Device::postSwitchToOp()
    {
    }


    void
    Device::updateSensorValueStruct()
    {
        sensorValue.position = dataPtr->getActualPosition();
        sensorValue.velocity = dataPtr->getActualVelocity();
        sensorValue.filteredvelocity = dataPtr->getActualFilteredVelocity();
        sensorValue.acceleration = dataPtr->getActualAcceleration();
        sensorValue.torque = dataPtr->getActualTorque();

        // sensorValue.gravityTorque is set in armarx::control::ethercat::RTUnit.cpp
        sensorValue.gravityCompensatedTorque = sensorValue.torque - sensorValue.gravityTorque;
        sensorValue.motorTemperature =
            sensorValue.motorTempFilter.update(dataPtr->getMotorTemperature());
        sensorValue.motorCurrent = dataPtr->getActualCurrent();
        sensorValue.status = dataPtr->getStatus();
        sensorValue.relativePosition = dataPtr->getActualRelativePosition();
        sensorValue.torqueAdcTemperature = dataPtr->getTorqueAdcTemperature();
        sensorValue.orientation = dataPtr->getImuOrientation();
        sensorValue.linearAcceleration = dataPtr->getImuAcceleration();


        /*
         * TODO: angularVelocity has to be calculated from other imu values
         */
        //sensorValue.angularVelocity = dataPtr->getImuGyro();
        sensorValue.angularVelocity = Eigen::Vector3f();
        sensorValue.sensorBoardUpdateRate = dataPtr->getMainUpdateRate();

        sensorValue.currentTarget = dataPtr->getTargetCurrent();
        sensorValue.positionTarget = dataPtr->getTargetPosition();
        sensorValue.velocityTarget = dataPtr->getTargetVelocity();
        sensorValue.maxTargetCurrent = dataPtr->getMaxTargetCurrent();
        sensorValue.torqueTicks = dataPtr->getTorqueTicks();

        sensorValue.voltage = dataPtr->getVoltage();
        sensorValue.absEncoderTicks = dataPtr->getAbsoluteEncoderTicks();
        sensorValue.absoluteEncoderTemperature = dataPtr->getAbsoluteEncoderTemperature();
        sensorValue.accelerationAccuracy = dataPtr->getImuAccelerationAccuracy();
        sensorValue.orientationAccuracy = dataPtr->getImuOrientationAccuracy();
        sensorValue.motorTemperatureAdcError = dataPtr->hasMotorTemperatureError();
        sensorValue.relativeEncoderStatus = dataPtr->getRelativeEncoderStatus();
        sensorValue.emergencyStopStatus = dataPtr->getEmergencyStopStatus();
        sensorValue.absoluteEncoderDetailedStatus = dataPtr->getAbsoluteEncoderDetailedStatus();
        sensorValue.imuCommunicationError = dataPtr->hasImuError();

        sensorValue.motorTemperatureValueErrorCounter +=
            dataPtr->hasTemperatureBitshiftOccured(Data::TemperatureType::MotorAdcTemperature) ? 1
                                                                                               : 0;
    }


    DataPtr
    Device::getData() const
    {
        if (dataPtr == nullptr)
        {
            DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                   "Joint has no data set, switch to SafeOP before");
        }

        return dataPtr;
    }


    const std::string&
    Device::getJointName() const
    {
        return getDeviceName();
    }


    armarx::control::ethercat::SlaveIdentifier
    Device::getElmoIdentifier() const
    {
        return elmoIdentifier;
    }


    armarx::control::ethercat::SlaveIdentifier
    Device::getSensorBoardIdentifier() const
    {
        return sensorBoardIdentifier;
    }


    bool
    Device::switchControlMode(armarx::ControlMode newMode)
    {
        using ElmoControlMode = common::elmo::gold::ElmoControlMode;

        ElmoControlMode modeToSet = ElmoControlMode::DEFAULT_MODE;

        switch (newMode)
        {
            case armarx::eVelocityControl:
                modeToSet = ElmoControlMode::VELOCITY;
                break;
            case armarx::eTorqueControl:
                modeToSet = ElmoControlMode::CURRENT;
                break;
            case armarx::ePositionVelocityControl:
                modeToSet = ElmoControlMode::DEFAULT_MODE;
                break;
            case armarx::ePositionControl:
                modeToSet = ElmoControlMode::POSITION;
                break;
            case armarx::eDisabled:
                modeToSet = ElmoControlMode::DEFAULT_MODE;
                break;
            case armarx::eUnknown:
                DEVICE_ERROR(rtGetDeviceName(), "unknown control mode set for this joint!");
                break;
        }

        return elmoPtr->switchMode(modeToSet);
    }

    void
    Device::rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                               const IceUtil::Time& timeSinceLastIteration)
    {
        if (dataPtr == nullptr)
        {
            DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                   "Joint has no data set, switch to SafeOP before");
        }
        dataPtr->rtReadSensorValues(sensorValuesTimestamp, timeSinceLastIteration);

        // Absolute encoder temperature readings.
        {
            const uint8_t absoluteEncoderTemperature = dataPtr->getAbsoluteEncoderTemperature();
            if (absoluteEncoderTemperature > 90)
            {
                DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                       "Absolute encoder temperature is extremely "
                                       "high! Reading %u °C. Shutting down.",
                                       absoluteEncoderTemperature);
            }
            else if (absoluteEncoderTemperature > 80)
            {
                DEVICE_ERROR(rtGetDeviceName(),
                             "Motor temperature is very high! Reading %u °C.",
                             absoluteEncoderTemperature)
                    .deactivateSpam(1);
            }
            else if (absoluteEncoderTemperature > 70)
            {
                DEVICE_WARNING(rtGetDeviceName(),
                               "Motor temperature is high! Reading %u °C.",
                               absoluteEncoderTemperature)
                    .deactivateSpam(1);
            }
        }

        // Motor temperature readings.
        {
            const float motorTemperature = dataPtr->getMotorTemperature();
            if (motorTemperature > 90)
            {
                DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                       "Motor temperature is extremely high! Reading %f °C. Shutting down.",
                                       motorTemperature);
            }
            else if (motorTemperature > 80)
            {
                DEVICE_ERROR(rtGetDeviceName(),
                             "Motor temperature is very high! Reading %f °C.",
                             motorTemperature)
                    .deactivateSpam(1);
            }
            else if (motorTemperature > 70)
            {
                DEVICE_WARNING(rtGetDeviceName(),
                               "Motor temperature is high! Reading %f °C.",
                               motorTemperature)
                    .deactivateSpam(1);
            }
        }

        // Torque temperature readings.
        {
            const float torqueTemperature = dataPtr->getTorqueAdcTemperature();
            if (torqueTemperature > 90)
            {
                DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                       "Torque temperature is extremely high! "
                                       "Reading %f °C. Shuttding down.",
                                       torqueTemperature);
            }
            else if (torqueTemperature > 80)
            {
                DEVICE_ERROR(rtGetDeviceName(),
                             "Torque temperature is very high! Reading %f °C.",
                             torqueTemperature)
                    .deactivateSpam(1);
            }
            else if (torqueTemperature > 70)
            {
                DEVICE_WARNING(rtGetDeviceName(),
                               "Torque temperature is high! Reading %f °C.",
                               torqueTemperature)
                    .deactivateSpam(1);
            }
        }

        updateSensorValueStruct();
    }


    void
    Device::rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration)
    {
        if (dataPtr == nullptr)
        {
            DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                   "Joint has no data set, switch to SafeOP before");
        }
        dataPtr->rtWriteTargetValues(sensorValuesTimestamp, timeSinceLastIteration);
    }


    const armarx::SensorValueBase*
    Device::getSensorValue() const
    {
        return &sensorValue;
    }

} // namespace devices::ethercat::sensor_actor_unit::armar7de
