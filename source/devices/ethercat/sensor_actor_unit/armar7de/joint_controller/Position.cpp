#include "Position.h"


// armarx
#include <RobotAPI/components/units/RobotUnit/BasicControllers.h>

#include <devices/ethercat/sensor_actor_unit/armar7de/Data.h>
#include <devices/ethercat/sensor_actor_unit/armar7de/Device.h>


namespace devices::ethercat::sensor_actor_unit::armar7de::joint_controller
{

    PositionController::PositionController(Device* joint,
                                           DataPtr jointPtr,
                                           armarx::control::joint_controller::PositionControllerConfigurationPtr config) : JointController(),
        target(),
        joint(joint)
    {
        dataPtr = jointPtr;

        maxVelocityRad = config->maxVelocityRad;
        maxAccelerationRad = config->maxAccelerationRad;
        maxDecelerationRad = config->maxDecelerationRad;
        p = config->p;
        limitlessController.positionPeriodLo = dataPtr->getSoftLimitLo();
        limitlessController.positionPeriodHi = dataPtr->getSoftLimitHi();
        limitlessController.maxDt = 0.003f;
        limitlessController.maxV = maxVelocityRad;
        //    controller.pControlPosErrorLimit = 0;
        limitlessController.pid->Kp = p;
        limitlessController.acceleration = maxAccelerationRad;
        limitlessController.deceleration = maxDecelerationRad;
        limitlessController.accuracy = config->accuracy;
        positionController.positionLimitLo = dataPtr->getSoftLimitLo();
        positionController.positionLimitHi = dataPtr->getSoftLimitHi();
        positionController.maxDt = 0.003f;
        positionController.pControlVelLimit = 0.005f;
        positionController.maxV = maxVelocityRad;
        //    controller.pControlPosErrorLimit = 0;
        positionController.pid->Kp = p;
        positionController.acceleration = maxAccelerationRad;
        positionController.deceleration = maxDecelerationRad;
        positionController.accuracy = config->accuracy;

        limitlessJoint = dataPtr->isLimitLess();
    }


    void PositionController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        if (target.isValid())
        {
            //ARMARX_IMPORTANT << deactivateSpam(1) << "pos tar:" << target.position;
            //        dataPtr->setTargetAngle(target.position);
            //ARMARX_IMPORTANT << deactivateSpam(2) << target.position;

            //        float newVelocity = positionThroughVelocityControlWithAccelerationBounds(
            //                                timeSinceLastIteration.toSecondsDouble(),
            //                                0.003,
            //                                currentVelocity,
            //                                maxVelocityRad,
            //                                maxAccelerationRad,
            //                                maxDecelerationRad,
            //                                dataPtr->getActualPosition(),
            //                                target.position,
            //                                p
            //                            );
            float newVelocity = 0;
            if (limitlessJoint)
            {
                limitlessController.currentV = currentVelocity;
                limitlessController.dt = timeSinceLastIteration.toSecondsDouble();
                limitlessController.currentPosition = dataPtr->getActualPosition();
                limitlessController.targetPosition = target.position;
                newVelocity = limitlessController.run();
            }
            else
            {
                float currentPosition = dataPtr->getActualPosition();
                float targetPosition = std::clamp(target.position,
                                                  std::min(currentPosition, positionController.positionLimitLo), // lo or current position
                                                  std::max(currentPosition, positionController.positionLimitHi)); // hi or current position
                positionController.currentV = currentVelocity;
                positionController.dt = timeSinceLastIteration.toSecondsDouble();
                positionController.currentPosition = currentPosition;
                positionController.targetPosition = targetPosition;
                newVelocity = positionController.run();
            }
            newVelocity = std::clamp(newVelocity, -maxVelocityRad, maxVelocityRad);
            currentVelocity = newVelocity;
            dataPtr->setTargetVelocity(newVelocity);
            //        ARMARX_INFO << deactivateSpam(5) << "Setting velocity target to " << newVelocity << " for position target " << target.position
            //                    << " current position: " << dataPtr->getActualPosition() << VAROUT(dataPtr->getSoftLimitLo());
        }
        else
        {
            ARMARX_ERROR << "invalid target set for a joint";
        }
    }


    armarx::ControlTargetBase* PositionController::getControlTarget()
    {
        return &target;
    }


    void PositionController::rtPreActivateController()
    {
        currentVelocity = dataPtr->getActualVelocity();
        joint->switchControlMode(armarx::eVelocityControl);
        dataPtr->setTargetAcceleration(maxAccelerationRad);
        dataPtr->setTargetDeceleration(maxDecelerationRad);
        //    ARMARX_INFO << "Setting acc to " << maxAccelerationRad;
        //    ARMARX_IMPORTANT << "Activating position controller in " << (dataPtr->isLimitLess() ? "limitless mode" : "in mode with position bounds");
    }

}
