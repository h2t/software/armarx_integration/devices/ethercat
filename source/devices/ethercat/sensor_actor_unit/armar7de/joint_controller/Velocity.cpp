#include "Velocity.h"


// robot_devices
#include <devices/ethercat/sensor_actor_unit/armar7de/Data.h>
#include <devices/ethercat/sensor_actor_unit/armar7de/Device.h>


namespace devices::ethercat::sensor_actor_unit::armar7de::joint_controller
{

    VelocityController::VelocityController(Device* joint,
                                           DataPtr jointData,
                                           armarx::RampedAccelerationVelocityControllerConfigurationPtr velocityControllerConfigDataPtr)
        : JointController(), target(), joint(joint), setStart(true)
    {
        dataPtr = jointData;
        limitless = dataPtr->isLimitLess();

        controller.maxV = velocityControllerConfigDataPtr->maxVelocityRad;
        controller.deceleration = velocityControllerConfigDataPtr->maxDecelerationRad;
        controller.jerk = velocityControllerConfigDataPtr->jerk;
        controller.maxDt = velocityControllerConfigDataPtr->maxDt;
        controller.directSetVLimit = velocityControllerConfigDataPtr->directSetVLimit;

        if (limitless)
        {
            controller.positionLimitHiSoft = M_PI * 1e8; // some unreachable number
            controller.positionLimitLoSoft = -M_PI * 1e8;
        }
        else
        {
            controller.positionLimitHiSoft = dataPtr->getSoftLimitHi();
            controller.positionLimitLoSoft = dataPtr->getSoftLimitLo();
        }
    }


    void VelocityController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        if (target.isValid())
        {
            //        ARMARX_IMPORTANT << deactivateSpam(1) << "vel target: " <<  target.velocity;
            //send the velocity to the joint

            controller.currentPosition = dataPtr->getActualPosition();
            controller.currentV = lastTargetVelocity;//dataPtr->getActualVelocity();
            controller.currentAcc = lastTargetAcceleration;
            controller.dt = timeSinceLastIteration.toSecondsDouble();
            controller.targetV = target.velocity;
            auto r = controller.run();
            lastTargetAcceleration = r.acceleration;
            float newVel = r.velocity;
            dataPtr->setTargetAcceleration(std::max(controller.deceleration, std::abs(r.acceleration * 1.1)));
            dataPtr->setTargetDeceleration(std::max(controller.deceleration, std::abs(r.acceleration * 1.1)));
            //        if (r.acceleration != 0.0)
            //        {
            //            dataPtr->setTargetAcceleration(std::abs(r.acceleration * 1.1));
            //            dataPtr->setTargetDeceleration(std::abs(r.acceleration * 1.1));
            //        }
            //        else
            //        {
            //            dataPtr->setTargetAcceleration(controller.deceleration);
            //            dataPtr->setTargetDeceleration(controller.deceleration);
            //        }
            //        ARMARX_INFO /*<< deactivateSpam(0.1)*/
            //        //<< VAROUT(limitless) << controller.positionLimitHiSoft << ", " << controller.positionLimitLoSoft << ", "
            //                << VAROUT(newVel) <<
            //                VAROUT(r.acceleration) << VAROUT(lastTargetVelocity);
            if (std::isnan(newVel))
            {
                //            ARMARX_INFO << deactivateSpam(0.1) << "NAN velocity target!";
                newVel = 0;
            }

            // simple sanity check
            if ((dataPtr->getActualPosition() > static_cast<float>(controller.positionLimitHiSoft) && target.velocity > 0)
                || (dataPtr->getActualPosition() < static_cast<float>(controller.positionLimitLoSoft) && target.velocity < 0))
            {
                newVel = 0;
                //            ARMARX_INFO << deactivateSpam(0.1) << "Out of limits - Breaking now at " << dataPtr->getActualPosition() << " current: " << dataPtr->getActualCurrent();
                dataPtr->setTargetDeceleration(9000);
            }

            dataPtr->setTargetVelocity(newVel);
            lastTargetVelocity = newVel;
        }
        else
        {
            ARMARX_ERROR << "invalid target set for elmo";
        }
    }


    armarx::ControlTargetBase* VelocityController::getControlTarget()
    {
        return &target;
    }


    void VelocityController::rtPreActivateController()
    {
        joint->switchControlMode(armarx::eVelocityControl);
        float actualVelocity = dataPtr->getActualVelocity();
        lastTargetVelocity = actualVelocity;
        lastTargetAcceleration = 0.0; // dataPtr->getActualAcceleration(); <-- using actual acceleration is dangerous due to high noise on acceleration!

        if (actualVelocity < 1e-2)
        {
            actualVelocity = 0;
        }

        ARMARX_RT_LOGF_VERBOSE("Setting target velocity to %.2f, current acceleration: %.2f", actualVelocity, dataPtr->getActualAcceleration());
        target.velocity = actualVelocity;
        dataPtr->setTargetVelocity(actualVelocity);
        start = armarx::rtNow();
    }

}
