#pragma once


// armarx
#include <armarx/control/joint_controller/ControllerConfiguration.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/BasicControllers.h>


namespace devices::ethercat::sensor_actor_unit::armar7de
{
    class Device;
    using DataPtr = std::shared_ptr<class Data>;
}


namespace devices::ethercat::sensor_actor_unit::armar7de::joint_controller
{
    using PositionControllerPtr = std::shared_ptr<class PositionController>;


    class PositionController : public armarx::JointController
    {

    public:

        PositionController(Device* joint, DataPtr jointPtr, armarx::control::joint_controller::PositionControllerConfigurationPtr config);

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        armarx::ControlTargetBase* getControlTarget() override;
        void rtPreActivateController() override;

    private:

        armarx::ControlTarget1DoFActuatorPosition target;
        armarx::PositionThroughVelocityControllerWithAccelerationBoundsAndPeriodicPosition limitlessController;
        armarx::PositionThroughVelocityControllerWithAccelerationAndPositionBounds positionController;
        DataPtr dataPtr;
        Device* joint;
        float maxVelocityRad;
        float maxAccelerationRad;
        float maxDecelerationRad;
        float p;
        float currentVelocity;
        bool limitlessJoint;

    };
}
