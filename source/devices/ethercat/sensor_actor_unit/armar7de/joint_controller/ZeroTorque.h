/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// armarx
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <armarx/control/joint_controller/VelocityManipulatingTorque.h>


namespace devices::ethercat::sensor_actor_unit::armar7de
{
    class Device;
    using DataPtr = std::shared_ptr<class Data>;
}


namespace devices::ethercat::sensor_actor_unit::armar7de::joint_controller
{
    using ZeroTorqueControllerPtr = std::shared_ptr<class ZeroTorqueController>;

    class ZeroTorqueController : public armarx::JointController
    {

    public:

        ZeroTorqueController(Device* joint,
                             DataPtr jointData,
                             armarx::control::joint_controller::VelocityManipulatingTorqueControllerConfigurationPtr torqueConfigData);

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        armarx::ControlTargetBase* getControlTarget() override;
        // JointController interface

    protected:

        void rtPreActivateController() override;

    private:

        armarx::ControlTarget1DoFActuatorZeroTorque target;
        DataPtr dataPtr;
        Device* joint;
        armarx::control::joint_controller::VelocityManipulatingTorqueController torqueController;

    };

}
