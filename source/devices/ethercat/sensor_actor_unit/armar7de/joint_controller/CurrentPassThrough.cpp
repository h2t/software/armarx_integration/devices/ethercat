#include "CurrentPassThrough.h"


#include <devices/ethercat/sensor_actor_unit/armar7de/Data.h>
#include <devices/ethercat/sensor_actor_unit/armar7de/Device.h>


namespace devices::ethercat::sensor_actor_unit::armar7de::joint_controller
{

    armarx::ControlTargetBase* CurrentPassThroughController::getControlTarget()
    {
        return &target;
    }


    CurrentPassThroughController::CurrentPassThroughController(Device* joint, DataPtr jointData) :
        dataPtr(jointData),
        joint(joint)
    {

    }


    void CurrentPassThroughController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        if (target.isValid())
        {
            if (target.current > 100)
            {
                throw armarx::LocalException() << "Got huge value for target current: " << target.current << " Expecting Ampere not Milliampere!";
            }

            dataPtr->setTargetCurrent(target.current * 1000);
        }
    }


    void CurrentPassThroughController::rtPreActivateController()
    {
        joint->switchControlMode(armarx::eTorqueControl);
        dataPtr->setTargetCurrent(0.0f);
    }

}  // namespace robot_devices::ethercat::sensor_actor_unit::v1::joint_controller
