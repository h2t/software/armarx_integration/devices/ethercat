#pragma once


// armarx
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <RobotAPI/components/units/RobotUnit/BasicControllers.h>
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <armarx/control/rt_filters/AverageFilter.h>
#include <armarx/control/rt_filters/FirFilter.h>
#include <armarx/control/joint_controller/Torque.h>


namespace devices::ethercat::sensor_actor_unit::armar7de
{
    class Device;
    using DataPtr = std::shared_ptr<class Data>;
}


namespace devices::ethercat::sensor_actor_unit::armar7de::joint_controller
{
    using VelocityTorqueControllerConfigurationPtr = std::shared_ptr<class VelocityTorqueControllerConfiguration>;


    class VelocityTorqueControllerConfiguration
    {

    public:

        VelocityTorqueControllerConfiguration();
        static VelocityTorqueControllerConfigurationPtr CreateTorqueConfigDataFromXml(armarx::DefaultRapidXmlReaderNode node);
        double pid_proportional_gain;
        double pid_integral_gain;
        double pid_derivative_gain;
        double pid_windup_guard;
        double pid_max_value;
        double pid_min_value;
        double pid_dis;
        double Kd;
        double inertia;
        double maxTorque;
        double scalePI;
        double actuatorType;
        bool status;
        std::vector<float> firFilterImpulseResponse;

    };


    using VelocityTorqueControllerPtr = std::shared_ptr<class VelocityTorqueController>;


    class VelocityTorqueController : public armarx::JointController
    {

    public:

        VelocityTorqueController(Device* joint,
                                 DataPtr jointData,
                                 armarx::control::joint_controller::TorqueControllerConfigurationPtr torqueConfigData);

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        armarx::ControlTargetBase* getControlTarget() override;

        armarx::control::rt_filters::FirFilter getFilter() const;

    protected:

        void rtPreActivateController() override;

    private:

        float defaultMaxTorque;
        armarx::ControlTarget1DoFActuatorTorqueVelocity target;
        DataPtr dataPtr;
        Device* joint;
        armarx::VelocityControllerWithAccelerationAndPositionBounds velCtrl;
        float lastSetVelocity = 0;
        armarx::control::joint_controller::TorquePID pid;
        armarx::control::rt_filters::FirFilter filter;
        armarx::control::rt_filters::FirFilter filter_vel;
        armarx::control::rt_filters::FirFilter hp_filter;
        armarx::control::rt_filters::FirFilter lp_filter;
        armarx::control::rt_filters::RtAverageFilter targetFilter;

    };

}  // namespace robot_devices::ethercat::sensor_actor_unit::v1::joint_controller
