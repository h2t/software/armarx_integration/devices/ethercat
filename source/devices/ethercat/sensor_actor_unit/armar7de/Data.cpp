/*
     * This file is part of ArmarX.
     *
     * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
     * Karlsruhe Institute of Technology (KIT), all rights reserved.
     *
     * ArmarX is free software; you can redistribute it and/or modify
     * it under the terms of the GNU General Public License version 2 as
     * published by the Free Software Foundation.
     *
     * ArmarX is distributed in the hope that it will be useful, butgetActualTorque
     * WITHOUT ANY WARRANTY; without even the implied warranty of
     * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
     * GNU General Public License for more details.
     *
     * You should have received a copy of the GNU General Public License
     * along with this program. If not, see <http://www.gnu.org/licenses/>.
     *
     * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
     * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
     *             GNU General Public License
     */

#include "Data.h"

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <devices/ethercat/common/elmo/gold/Slave.h>
#include <devices/ethercat/common/sensor_board/armar6/Slave.h>

namespace devices::ethercat::sensor_actor_unit::armar7de
{

    Data::Data(common::elmo::gold::ElmoConfig elmoConfig,
               common::elmo::gold::SlaveOut* elmo_out,
               common::elmo::gold::SlaveIn* elmo_in,
               common::sensor_board::armar7de::SensorBoardConfig sensorConfig,
               common::sensor_board::armar7de::SlaveOut* sensor_out,
               common::sensor_board::armar7de::SlaveIn* sensor_in) :
        common::elmo::gold::Data(elmoConfig,
                                 elmo_out,
                                 elmo_in),
        common::sensor_board::armar7de::Data(sensorConfig,
                                             sensor_out,
                                             sensor_in)
    {
    }

    void
    Data::rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                             const IceUtil::Time& timeSinceLastIteration)
    {
        common::elmo::gold::Data::rtReadSensorValues(sensorValuesTimestamp, timeSinceLastIteration);
        common::sensor_board::armar7de::Data::rtReadSensorValues(sensorValuesTimestamp,
                                                                 timeSinceLastIteration);
    }

    void
    Data::rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                              const IceUtil::Time& timeSinceLastIteration)
    {
        common::elmo::gold::Data::rtWriteTargetValues(sensorValuesTimestamp,
                                                      timeSinceLastIteration);
        common::sensor_board::armar7de::Data::rtWriteTargetValues(sensorValuesTimestamp,
                                                                  timeSinceLastIteration);
    }

} // namespace devices::ethercat::sensor_actor_unit::armar7de
