/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Armar6RT::ArmarXObjects::Armar6IceKinematicUnit
 * @author     Markus Swarowsky ( markus dot swarowsky at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <VirtualRobot/Nodes/RobotNode.h>

#include <RobotAPI/components/units/RobotUnit/BasicControllers.h>
#include <RobotAPI/components/units/RobotUnit/Devices/ControlDevice.h>
#include <RobotAPI/components/units/RobotUnit/Devices/SensorDevice.h>
#include <armarx/control/ethercat/DeviceInterface.h>
#include <armarx/control/ethercat/SlaveIdentifier.h>

#include <devices/ethercat/common/sensor_board/armar7de/Slave.h>
#include <devices/ethercat/common/sensor_board/armar7de/Config.h>
#include <devices/ethercat/common/elmo/gold/Config.h>
#include <devices/ethercat/sensor_actor_unit/armar7de/Data.h>


namespace armarx::control::joint_controller
{
    using TorqueControllerConfigurationPtr = std::shared_ptr<class TorqueControllerConfiguration>;
    using VelocityManipulatingTorqueControllerConfigurationPtr =
        std::shared_ptr<class VelocityManipulatingTorqueControllerConfiguration>;
    using PositionControllerConfigurationPtr =
        std::shared_ptr<class PositionControllerConfiguration>;
} // namespace armarx::control::joint_controller


namespace devices::ethercat::sensor_actor_unit::armar7de
{

    namespace joint_controller
    {
        using VelocityControllerPtr = std::shared_ptr<class VelocityController>;
        using PositionControllerPtr = std::shared_ptr<class PositionController>;
        using TorqueControllerPtr = std::shared_ptr<class TorqueController>;
        using ZeroTorqueControllerPtr = std::shared_ptr<class ZeroTorqueController>;
        using CurrentPassThroughControllerPtr = std::shared_ptr<class CurrentPassThroughController>;
        using ActiveImpedanceControllerPtr = std::shared_ptr<class ActiveImpedanceController>;
        using VelocityTorqueControllerPtr = std::shared_ptr<class VelocityTorqueController>;
        template <class Type>
        class StopMovementController;
        template <class Type>
        class ElmoEmergencyController;
    } // namespace joint_controller

    namespace hwconfig = armarx::control::hardware_config;


    class Device :
        public armarx::ControlDevice,
        public armarx::SensorDevice,
        public armarx::control::ethercat::DeviceInterface
    {

    public:
        Device(hwconfig::DeviceConfig& config,
               VirtualRobot::RobotPtr const& robot);
        ~Device() override
        {
        }

        DeviceInterface::TryAssignResult
        tryAssign(armarx::control::ethercat::SlaveInterface& slave) override;

        DeviceInterface::AllAssignedResult onAllAssigned() override;

        std::string getClassName() const override;

        void postSwitchToSafeOp() override;

        void postSwitchToOp() override;

        // SensorDevice interface
        const armarx::SensorValueBase* getSensorValue() const override;
        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration) override;

        // ControlDevice interface
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                 const IceUtil::Time& timeSinceLastIteration) override;

        DataPtr getData() const;

        const std::string& getJointName() const;

        armarx::control::ethercat::SlaveIdentifier getElmoIdentifier() const;

        armarx::control::ethercat::SlaveIdentifier getSensorBoardIdentifier() const;

        bool switchControlMode(armarx::ControlMode newMode);

        bool
        isEnabled() const
        {
            return this->enabled;
        }

    private:
        void updateSensorValueStruct();

        std::string deviceName;

        VirtualRobot::RobotNodePtr robotNode;

        ///The data and target object
        DataPtr dataPtr;
        /// The data object for copying to non-rt part
        SensorValueArmar7deActuator sensorValue;

        /// Whether or not this device should be started on the bus
        bool enabled = false;

        ///bus devices - serial form config and pointer to actual bus slave
        const armarx::control::ethercat::SlaveIdentifier elmoIdentifier;

        const armarx::control::ethercat::SlaveIdentifier sensorBoardIdentifier;

        common::elmo::gold::Slave* elmoPtr;
        common::sensor_board::armar7de::Slave* sensorBoardPtr;

        ///the joint controller
        joint_controller::TorqueControllerPtr torqueController;
        joint_controller::ZeroTorqueControllerPtr zeroTorqueController;
        joint_controller::VelocityTorqueControllerPtr velocityTorqueController;
        joint_controller::ActiveImpedanceControllerPtr activeImpedanceController;
        joint_controller::CurrentPassThroughControllerPtr currentController;
        joint_controller::PositionControllerPtr positionController;
        joint_controller::VelocityControllerPtr velocityController;
        std::shared_ptr<joint_controller::ElmoEmergencyController<DataPtr>> elmoEmergencyController;
        std::shared_ptr<joint_controller::StopMovementController<DataPtr>> stopMovementController;

        ///the config data for jointTorqueController
        armarx::control::joint_controller::TorqueControllerConfigurationPtr
            torqueControllerConfigDataPtr;
        armarx::control::joint_controller::VelocityManipulatingTorqueControllerConfigurationPtr
            zeroTorqueControllerConfigDataPtr;
        armarx::control::joint_controller::PositionControllerConfigurationPtr
            positionControllerConfigDataPtr;
        armarx::RampedAccelerationVelocityControllerConfigurationPtr
            velocityControllerConfigDataPtr;

        /// the config data for the slaves
        common::sensor_board::armar7de::SensorBoardConfig sensorBoardConfig;
        common::elmo::gold::ElmoConfig elmoConfig;
    };

} // namespace devices::ethercat::sensor_actor_unit::armar7de
