#pragma once


// STD/STL
#include <cstdint>


namespace devices::ethercat::ft_sensor_board::armar6
{

    /**
     * @brief PDO mapping sensorB->master
     */
    struct SlaveOut
    {
        std::int32_t dms_adc_1;
        std::int32_t dms_adc_2;
        std::int32_t dms_adc_3;
        std::int32_t dms_adc_4;
        std::int32_t dms_adc_5;
        std::int32_t dms_adc_6;
        std::int32_t adc_1_to_3_temperature;
        std::int32_t adc_4_to_6_temperature;

        std::uint8_t  dms_adc_crc_1;
        std::uint8_t  dms_adc_crc_2;
        std::uint8_t  dms_adc_crc_3;
        std::uint8_t  dms_adc_crc_4;
        std::uint8_t  dms_adc_crc_5;
        std::uint8_t  dms_adc_crc_6;

        std::uint8_t  adc_temperature_crc_1_to_3;
        std::uint8_t  adc_temperature_crc_4_to_6;

        std::uint16_t update_rate;
        std::int16_t pad16;

    } __attribute__((__packed__));


    /**
     * @brief PDO mapping master->sensorB
     */
    struct SlaveIn
    {
        //Test LED
        std::uint8_t LEDSwitch;
        //24 bits padding
        std::uint16_t pad1;
        std::uint8_t pad2;
    } __attribute__((__packed__));

}
