/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Armar6RT::ArmarXObjects::Armar6IceKinematicUnit
 * @author     Markus Swarowsky ( markus dot swarowsky at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Device.h"

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/RobotNodeSet.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/util/StringHelperTemplates.h>

#include <devices/ethercat/common/h2t_devices.h>


namespace devices::ethercat::ft_sensor_board::armar6
{

    Device::Device(armarx::control::hardware_config::DeviceConfig& hwConfig,
                   const VirtualRobot::RobotPtr& robot) :
        DeviceBase(hwConfig.getName()),
        SensorDevice(hwConfig.getName()),
        DeviceInterface(hwConfig.getName()),
        dataPtr(nullptr),
        slaveIdentifier(hwConfig.getOnlySlaveConfig().getIdentifier()),
        ftSlave(nullptr),
        config(hwConfig)
    {
        this->sensorNode = robot->getSensor(getDeviceName());
        ARMARX_CHECK_EXPRESSION(this->sensorNode) << getDeviceName();
        this->robot = sensorNode->getRobotNode()->getRobot();
        auto dynamicRoot =
            robot->getRobotNode(hwConfig.getString("FirstRobotNodeNameAfterSensor"));
        auto parent = sensorNode->getParent();
        massSum = 0;
        Eigen::Vector3f averageCoM;
        std::set<VirtualRobot::SceneObjectPtr> allChildren;
        std::vector<std::string> nodeNames;

        auto children = dynamicRoot->getChildren();
        while (!children.empty())
        {
            auto child = children.back();
            if (!allChildren.count(child))
            {
                allChildren.insert(child);
                auto newChildren = child->getChildren();
                children.pop_back();
                children.insert(children.end(), newChildren.begin(), newChildren.end());
            }
        }

        for (auto& child : allChildren)
        {
            if (dynamic_cast<VirtualRobot::RobotNode*>(child.get()))
            {
                nodeNames.push_back(child->getName());
            }
        }

        nodeNames.push_back(dynamicRoot->getName());
        ARMARX_DEBUG << "CoM nodes: " << nodeNames;

        VirtualRobot::RobotNodeSetPtr handNodeSet = VirtualRobot::RobotNodeSet::createRobotNodeSet(
            robot, "handBodiesSet", nodeNames, dynamicRoot->getName());
        massSum = handNodeSet->getMass();
        ARMARX_INFO << getDeviceName() << " Hand mass: " << massSum;
        gravityVec = Eigen::Vector3f(0, 0, 9.81f * massSum);

        //    comLocationLocal = Eigen::Matrix4f::Identity();
        //    comLocationLocal.block<3, 1>(0, 3) = handNodeSet->getCoM();
        //    ARMARX_INFO << parent->getName() << " CoM Global: " << comLocationLocal << " parent global: " << parent->getGlobalPose().block<3, 1>(0, 3);
        comLocationLocal = sensorNode->toLocalCoordinateSystemVec(handNodeSet->getCoM());
        Eigen::Vector3f comLocationLocalJeff =
            parent->getGlobalPose().block<3, 3>(0, 0).transpose() *
            (handNodeSet->getCoM() - sensorNode->getGlobalPose().block<3, 1>(0, 3));

        transformationToParent =
            sensorNode->getTransformationTo(sensorNode->getParent()).block<3, 3>(0, 0).transpose();
        comLocationLocal = transformationToParent * comLocationLocal;
        ARMARX_INFO << getDeviceName() << VAROUT(comLocationLocal) << VAROUT(comLocationLocalJeff);
        //    comLocationLocal << -0.0044873, -0.00233698, 0.0989811;
        //    comLocationLocal *= 1000;
        //    massSum = 1.11726;
        ARMARX_INFO << getDeviceName() << VAROUT(transformationToParent);
        gravityOffsetVecTorque = Eigen::Vector3f::Zero();
        gravityOffsetVecForce = Eigen::Vector3f::Zero();
    }


    Device::~Device() = default;


    armarx::control::ethercat::DeviceInterface::TryAssignResult
    Device::tryAssign(armarx::control::ethercat::SlaveInterface& slave)
    {
        using Result = DeviceInterface::TryAssignResult;

        Result result =
            common::tryAssignLegacyH2TDevice(slave, ftSlave, slaveIdentifier.productCode);
        if (result != Result::unknown)
        {
            return result;
        }

        return result;
    }


    armarx::control::ethercat::DeviceInterface::AllAssignedResult
    Device::onAllAssigned()
    {
        using Result = DeviceInterface::AllAssignedResult;

        if (ftSlave)
        {
            return Result::ok;
        }

        return Result::slavesMissing;
    }

    std::string
    Device::getClassName() const
    {
        return "FTSensorBoard_Armar6";
    }


    void
    Device::postSwitchToSafeOp()
    {
        if (!dataPtr)
        {
            ARMARX_INFO << "Creating FTSensorData for device " << getDeviceName()
                        << " of slave with id " << ftSlave->getSlaveNumber()
                        << "\nOutputPtrs = " << ftSlave->getOutputsPtr()
                        << "\nOutputPtrs = " << ftSlave->getInputsPtr();
            dataPtr = std::make_unique<Data>(
                config, ftSlave->getOutputsPtr(), ftSlave->getInputsPtr());
        }
        ARMARX_CHECK_NOT_NULL(dataPtr);
    }


    Eigen::Vector3f
    changeDirectionFrame(const Eigen::Vector3f direction,
                         const VirtualRobot::SceneObjectPtr& currentFrame,
                         const VirtualRobot::SceneObjectPtr& newFrame)
    {
        Eigen::Matrix4f toNewFrame = currentFrame->getTransformationTo(newFrame);
        toNewFrame(0, 3) = 0; // only rotation is important for direction
        toNewFrame(1, 3) = 0;
        toNewFrame(2, 3) = 0;

        return toNewFrame.block(0, 0, 3, 3).inverse() * direction;
    }


    Eigen::Vector3f
    Device::changeDirectionFrameToGlobal(const Eigen::Vector3f direction,
                                         const VirtualRobot::SceneObjectPtr& currentFrame)
    {
        Eigen::Matrix4f toNewFrame =
            robot->getGlobalPose() * currentFrame->getTransformationFrom(robot->getRootNode());

        toNewFrame(0, 3) = 0; // only rotation is important for direction
        toNewFrame(1, 3) = 0;
        toNewFrame(2, 3) = 0;

        return toNewFrame.block(0, 0, 3, 3).inverse() * direction;
    }


    Eigen::Vector3f
    Device::changeDirectionFrameFromGlobal(const Eigen::Vector3f& globalDirection,
                                           const VirtualRobot::SceneObjectPtr& newFrame)
    {
        Eigen::Matrix4f toNewFrame =
            robot->getGlobalPose() * newFrame->getTransformationFrom(robot->getRootNode());

        toNewFrame(0, 3) = 0; // only rotation is important for direction
        toNewFrame(1, 3) = 0;
        toNewFrame(2, 3) = 0;

        return toNewFrame.block(0, 0, 3, 3).inverse() * globalDirection;
    }


    void
    Device::updateSensorValueStruct()
    {
        //    auto parent = sensorNode->getParent();
        sensorValue.force = dataPtr->getForces();
        sensorValue.torque = dataPtr->getTorques();

        sensorValue.force = transformationToParent * sensorValue.force;
        sensorValue.torque = transformationToParent * sensorValue.torque;

        //    ARMARX_INFO << deactivateSpam(1) << VAROUT(dataPtr->getForces()) << VAROUT(sensorValue.force);

        sensorValue.adc_1_to_3_temperature = dataPtr->getADC1to3Temperature();
        sensorValue.adc_4_to_6_temperature = dataPtr->getADC4to6Temperature();

        sensorValue.rawForceTorque = dataPtr->getRawValues().cast<float>();
        sensorValue.updateRate = static_cast<int>(dataPtr->getUpdateRate());
        Eigen::Vector3f localGravityForceVec =
            sensorNode->getParent()->getGlobalPose().block<3, 3>(0, 0).transpose() * gravityVec;
        Eigen::Vector3f localGravityTorqueVec =
            (comLocationLocal * 0.001).cross(localGravityForceVec);

        if (startTime.toMicroSeconds() == 0)
        {
            startTime = IceUtil::Time::now();
        }

        // get force / torque offset
        if ((IceUtil::Time::now() - startTime).toSecondsDouble() >
                1.0 // wait a moment after start up since the controllers shaked the robot durting start up
            && gravityOffsetVecForce.norm() < 0.001f) // if value is not set yet, it is zero
        {
            // this offset is used to set the force value to the value it should measure (based on joint angles and dynamic model). This assumes that nothing is in the hands or touching the hands after start up
            gravityOffsetVecForce = sensorValue.force + localGravityForceVec;
            gravityOffsetVecTorque = sensorValue.torque + localGravityTorqueVec;
        }

        // eliminate force / torque offset and hand gravitational force from raw data (now all data are in the local frame of the sensorNode's parent)
        sensorValue.force -= gravityOffsetVecForce;
        sensorValue.torque -= gravityOffsetVecTorque;
        sensorValue.gravityCompensatedForce = sensorValue.force + localGravityForceVec;
        sensorValue.gravityCompensatedTorque = sensorValue.torque + localGravityTorqueVec;

        //    auto parent = sensorNode->getParent();
        //    Eigen::Matrix4f parentGlobalPose = parent->getGlobalPose();
        //    Eigen::Matrix3f globalParentOrientation = parentGlobalPose.block<3, 3>(0, 0);

        //    Eigen::Vector3f localGravityForceVec = globalParentOrientation.transpose() * gravityVec;
        //    Eigen::Matrix4f sensorPose = sensorNode->getGlobalPose();
        //    Eigen::Vector2f sensorPosition = sensorPose.block<2, 1>(0, 3);
        //    Eigen::Matrix4f localCoMPose = Eigen::Matrix4f::Identity();
        //    localCoMPose.block<3, 1>(0, 3) = comLocationLocal;
        //    Eigen::Vector3f comPositionGlobal3D = (parentGlobalPose * localCoMPose).block<3, 1>(0, 3);
        //    Eigen::Vector2f comPositionGlobal = comPositionGlobal3D.head(2);

        //    Eigen::Vector3f forceGlobal = changeDirectionFrameToGlobal(sensorValue.force, parent);

        //    Eigen::Matrix3f parentPoseGlobalInv = globalParentOrientation.transpose();
        //    float wrench = (comPositionGlobal - sensorPosition).norm();
        //    ARMARX_INFO << deactivateSpam(1, getDeviceName())  << getDeviceName() << "\n" << "\n"
        //                << "\n" << VAROUT(sensorPose)
        //                << "\n" << VAROUT(comLocationLocal)
        //                << "\n" << VAROUT(parentGlobalPose)
        //                << "\n" << VAROUT(localCoMPose)
        //                << "\n" << VAROUT(comPositionGlobal3D)
        //                << "\n" << VAROUT(comPositionGlobal)

        //                << "\n" << VAROUT(sensorPosition)
        //                << "\n" << VAROUT(wrench);

        //    Eigen::Vector3f localGravityForceVec = globalParentOrientation.transpose() * gravityVec;
        //    Eigen::Vector3f localGravityTorqueVec = comLocationLocal.cross(localGravityForceVec);
        //    Eigen::Vector3f calculatedGravityTorque = wrench * 0.001f * gravityVec;

        //    if (startTime.toMicroSeconds() == 0)
        //    {
        //        startTime = IceUtil::Time::now();
        //    }
        //    if ((IceUtil::Time::now() - startTime).toSecondsDouble() > 1.0 // wait a moment after start up since the controllers shaked the robot durting start up
        //            && gravityOffsetVecForce.norm() < 0.001) // if value is not set yet, it is zero
        //    {
        //        // this offset is used to set the force value to the value it should measure (based on joint angles and dynamic model). This assumes that nothing is in the hands or touching the hands after start up
        //        gravityOffsetVecForce = sensorValue.force + localGravityForceVec;

        //        //        gravityOffsetVecTorque = sensorValue.torque + parentPoseGlobalInv * calculatedGravityTorque;
        //        gravityOffsetVecTorque = sensorValue.torque + localGravityTorqueVec;

        //    }
        //    // eliminate force / torque offset and hand gravitational force from raw data
        //    sensorValue.force -= gravityOffsetVecForce;
        //    sensorValue.torque -= gravityOffsetVecTorque;
        //    sensorValue.gravityCompensatedForce = sensorValue.force + localGravityForceVec;
        //    sensorValue.gravityCompensatedTorque = sensorValue.torque + localGravityTorqueVec;

        //    Eigen::Vector3f forceGlobal = globalParentOrientation * (sensorValue.force);
        //    Eigen::Vector3f compensatedForceLocalGlobal = forceGlobal + gravityVec;
        //    ARMARX_INFO << deactivateSpam(1, getDeviceName()) << getDeviceName() << ":  " << VAROUT(sensorValue.force) << "\n" << VAROUT(forceGlobal) << "\n" << VAROUT(gravityOffsetVec) << "\n" << VAROUT(gravityVec) << "\n" << VAROUT(compensatedForceLocalGlobal);
        //    sensorValue.gravityCompensatedForce = parentPoseGlobalInv * compensatedForceLocalGlobal;

        //    ARMARX_INFO << deactivateSpam(1) << VAROUT(forceGlobal) << VAROUT(gravityVec);
        //    Eigen::Vector3f torqueGlobal = globalParentOrientation * sensorValue.torque;
        //    Eigen::Vector3f gravityTorqueGlobal = wrench * 0.001f * gravityVec;
        //    sensorValue.gravityCompensatedTorque = torqueGlobal + gravityTorqueGlobal;
        //    ARMARX_INFO << deactivateSpam(1, getDeviceName())  << VAROUT(wrench) << VAROUT(calculatedGravityTorque) << "\n" << VAROUT(dataPtr->getTorques())
        //                "\n" << VAROUT(sensorValue.torque) <<
        //                "\n" << VAROUT(gravityOffsetVecTorque) <<
        //                "\n" << VAROUT(gravityTorqueGlobal) <<
        //                "\n" << VAROUT(torqueGlobal);
        //    ARMARX_INFO << deactivateSpam(1) << sensorNode->getName() << VAROUT(wrench) << VAROUT(sensorValue.gravityCompensatedTorque) << VAROUT(comPositionGlobal) << VAROUT(sensorPosition) << VAROUT(parentGlobalPose) << VAROUT(comLocationLocal);
        //    sensorValue.gravityCompensatedTorque = parentPoseGlobalInv * sensorValue.gravityCompensatedTorque;
    }


    std::string
    Device::getReportingFrame() const
    {
        return sensorNode->getParent()->getName();
    }


    armarx::control::ethercat::SlaveIdentifier
    Device::getSlaveIdentifier() const
    {
        return slaveIdentifier;
    }


    Data*
    Device::getData() const
    {
        if (not dataPtr)
        {
            DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                   "FTSensor has no data set, switch to SafeOP before");
        }

        return dataPtr.get();
    }


    void
    Device::rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                               const IceUtil::Time& timeSinceLastIteration)
    {
        if (not dataPtr)
        {
            DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                   "FTSensor has no data set, switch to SafeOP before");
        }

        dataPtr->rtReadSensorValues(sensorValuesTimestamp, timeSinceLastIteration);

        updateSensorValueStruct();
    }


    const armarx::SensorValueBase*
    Device::getSensorValue() const
    {
        return &sensorValue;
    }

} // namespace devices::ethercat::ft_sensor_board::armar6
