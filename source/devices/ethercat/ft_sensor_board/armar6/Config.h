#pragma once

#include <armarx/control/hardware_config/DeviceConfig.h>

namespace devices::ethercat::ft_sensor_board::armar6
{
    namespace hwconfig = armarx::control::hardware_config;
    struct Config
    {
        Config(hwconfig::DeviceConfig& hwConfig)
            : adcTemperature(hwConfig.getLinearConfig("adcTemperature")),
              voltageFactor(hwConfig.getFloat("voltageFactor")),
              ftCalibrationMatrix(hwConfig.getMatrix<float, 6, 6>("FTCalibrationMatrix")),
              channelOrder(hwConfig.getMatrix<std::int32_t, 6, 1>("ChannelOrder")),
              offsetList(hwConfig.getIntList("Offset"))
        {

        }

        hwconfig::types::LinearConfig adcTemperature;
        float voltageFactor;
        Eigen::Matrix<float, 6, 6> ftCalibrationMatrix;
        Eigen::Matrix<std::int32_t, 6, 1> channelOrder;
        std::list<std::int32_t> offsetList;
    };
}
