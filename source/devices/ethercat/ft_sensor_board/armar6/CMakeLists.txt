armarx_add_library(ft_sensor_board_armar6
    SOURCES
        Slave.cpp
        Device.cpp
        Data.cpp
    HEADERS
        Config.h
        SlaveIO.h
        Slave.h
        Device.h
        Data.h
    DEPENDENCIES
        Simox::VirtualRobot
        armarx_control::ethercat
        armarx_control::rt_filters
        devices_ethercat::common_sensor_board_armar6
    DEPENDENCIES_LEGACY
        SOEM
)

target_compile_options(ft_sensor_board_armar6
    PRIVATE
        -Wall -Wextra -Wpedantic -Werror
)

if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    if(CMAKE_CXX_COMPILER_VERSION VERSION_GREATER_EQUAL "9")
        target_compile_options(ft_sensor_board_armar6
            PRIVATE
                -Wno-error=address-of-packed-member
        )
    endif()
endif()
