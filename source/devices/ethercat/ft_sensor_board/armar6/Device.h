/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Armar6RT::ArmarXObjects::Armar6IceKinematicUnit
 * @author     Markus Swarowsky ( markus dot swarowsky at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <VirtualRobot/Nodes/RobotNode.h>

#include <RobotAPI/components/units/RobotUnit/Devices/SensorDevice.h>
#include <armarx/control/ethercat/DeviceInterface.h>
#include <armarx/control/ethercat/SlaveIdentifier.h>

#include <devices/ethercat/ft_sensor_board/armar6/Data.h>
#include <devices/ethercat/ft_sensor_board/armar6/Slave.h>
#include <devices/ethercat/ft_sensor_board/armar6/Config.h>


namespace devices::ethercat::ft_sensor_board::armar6
{

    using JointFTSensorShapeControllerPtr = std::shared_ptr<class JointFTSensorShapeController>;


    class Device : public armarx::SensorDevice, public armarx::control::ethercat::DeviceInterface
    {

    public:
        Device(armarx::control::hardware_config::DeviceConfig& hwConfig,
               const VirtualRobot::RobotPtr& robot);

        ~Device() override;

        TryAssignResult tryAssign(armarx::control::ethercat::SlaveInterface& slave) override;

        AllAssignedResult onAllAssigned() override;

        std::string getClassName() const override;

        void postSwitchToSafeOp() override;

        // SensorDevice interface
        const armarx::SensorValueBase* getSensorValue() const override;
        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration) override;

        Data* getData() const;

        armarx::control::ethercat::SlaveIdentifier getSlaveIdentifier() const;

    private:
        void updateSensorValueStruct();
        VirtualRobot::SensorPtr sensorNode;
        VirtualRobot::RobotPtr robot;

        ///The data and target object
        std::unique_ptr<Data> dataPtr;
        /// The data object for copying to non-rt part
        FTSensorDataValue sensorValue;

        ///bus devices - serial form config and pointer to actual bus slave
        const armarx::control::ethercat::SlaveIdentifier slaveIdentifier;
        bool firstRun = true;
        IceUtil::Time startTime;
        Slave* ftSlave;
        Eigen::Vector3f comLocationLocal;
        float massSum;
        Eigen::Matrix3f transformationToParent;
        Eigen::Vector3f gravityVec;
        Eigen::Vector3f gravityOffsetVecForce, gravityOffsetVecTorque;
        // SensorDevice interface

        Config config;

    public:
        std::string getReportingFrame() const override;
        Eigen::Vector3f
        changeDirectionFrameToGlobal(const Eigen::Vector3f direction,
                                     const VirtualRobot::SceneObjectPtr& currentFrame);
        Eigen::Vector3f
        changeDirectionFrameFromGlobal(const Eigen::Vector3f& globalDirection,
                                       const VirtualRobot::SceneObjectPtr& newFrame);
    };

} // namespace devices::ethercat::ft_sensor_board::armar6
