#include "Slave.h"

#include <armarx/control/ethercat/Bus.h>

#include <devices/ethercat/common/h2t_devices.h>
#include <devices/ethercat/common/sensor_board/armar6/ADS1263CRCHelper.h>


namespace devices::ethercat::ft_sensor_board::armar6
{

    Slave::Slave(const armarx::control::ethercat::SlaveIdentifier slaveIdentifier) :
        SlaveInterfaceWithIO(slaveIdentifier)
    {
        setTag(this->slaveIdentifier.getName());
    }


    void
    Slave::execute()
    {
        auto checkCRCHelper = [&, this](std::int32_t sensorField,
                                        std::uint8_t expectedCRC,
                                        const std::string& sensorFieldName)
        {
            // TODO: Move ADS1263CRCHelper somewhere in common or so.
            auto crc = common::sensor_board::armar6::ADS1263CRCHelper::CalculateCRC8_Temperature(
                sensorField);
            if (crc != expectedCRC)
            {
                SLAVE_WARNING(getSlaveIdentifier(),
                              "CRC of %s is wrong: expected value: %d calculated value: %d",
                              sensorFieldName,
                              static_cast<std::uint16_t>(expectedCRC),
                              static_cast<std::uint16_t>(crc))
                    .deactivateSpam(10);
            }
        };
        checkCRCHelper(outputs->adc_1_to_3_temperature,
                       outputs->adc_temperature_crc_1_to_3,
                       "adc_1_to_3_temperature");
        checkCRCHelper(outputs->adc_4_to_6_temperature,
                       outputs->adc_temperature_crc_4_to_6,
                       "adc_4_to_6_temperature");
    }


    bool
    Slave::prepareForRun()
    {
        return true;
    }


    bool
    Slave::shutdown()
    {
        return true;
    }


    void
    Slave::doMappings()
    {
    }


    bool
    Slave::hasError()
    {
        return false;
    }


    void
    Slave::prepareForSafeOp()
    {
    }


    void
    Slave::finishPreparingForSafeOp()
    {
    }


    void
    Slave::prepareForOp()
    {
    }


    void
    Slave::finishPreparingForOp()
    {
    }


    std::string
    Slave::getDefaultName()
    {
        return "Armar6FTSensorBoard";
    }


    bool
    Slave::isSlaveIdentifierAccepted(const armarx::control::ethercat::SlaveIdentifier& sid)
    {
        // For the old ft-sensor-boards (used in Armar6), the serial number could not be encoded on the chip.
        // The product code was used instead.

        const std::vector<std::uint32_t> validProductCodes{H2T_FTSENSOR_PRODUCT_CODE_LEFT,
                                                           H2T_FTSENSOR_PRODUCT_CODE_RIGHT,
                                                           H2T_FTSENSOR_PRODUCT_CODE_RIGHT_2};

        const bool productCodeMatches =
            std::find(validProductCodes.begin(), validProductCodes.end(), sid.productCode) !=
            validProductCodes.end();

        return sid.vendorID == devices::ethercat::common::H2TVendorId and sid.serialNumber == 0 and
               productCodeMatches;
    }

} // namespace devices::ethercat::ft_sensor_board::armar6
