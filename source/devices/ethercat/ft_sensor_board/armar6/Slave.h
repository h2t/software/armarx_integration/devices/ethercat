#pragma once


// armarx
#include <armarx/control/ethercat/SlaveInterface.h>

#include <devices/ethercat/ft_sensor_board/armar6/SlaveIO.h>


namespace devices::ethercat::ft_sensor_board::armar6
{
    static constexpr std::uint32_t H2T_FTSENSOR_PRODUCT_CODE_LEFT       = 0x501;
    static constexpr std::uint32_t H2T_FTSENSOR_PRODUCT_CODE_RIGHT      = 0x502;
    static constexpr std::uint32_t H2T_FTSENSOR_PRODUCT_CODE_RIGHT_2    = 0x503;

    class Slave :
        public armarx::control::ethercat::SlaveInterfaceWithIO<SlaveIn, SlaveOut>
    {

    public:

        Slave(const armarx::control::ethercat::SlaveIdentifier slaveIdentifier);

        /**
         * @see AbstractSlave::doMappings
         */
        void doMappings() override;

        void prepareForOp() override;

        void finishPreparingForOp() override;

        /**
         * @see AbstractSlave::prepare()
         */
        bool prepareForRun() override;

        /**
         * @see AbstractSlave::execute()
         */
        void execute() override;

        /**
         * @see AbstractSlave::shutdown()
         */
        bool shutdown() override;

        /**
         * @see AbstractSlave::hasError()
         * @return
         */
        bool hasError() override;

        void prepareForSafeOp() override;

        void finishPreparingForSafeOp() override;

        static std::string getDefaultName();

        static bool isSlaveIdentifierAccepted(const armarx::control::ethercat::SlaveIdentifier& sid);

    };

}
