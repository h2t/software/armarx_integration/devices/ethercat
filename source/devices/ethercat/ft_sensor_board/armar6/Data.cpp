/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, butgetActualTorque
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Data.h"


// armarx
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/util/StringHelperTemplates.h>


namespace devices::ethercat::ft_sensor_board::armar6
{

    Data::Data(Config& config,
               SlaveOut* sensorOUT, SlaveIn* sensorIN) :
        sensorOUT(sensorOUT),
        sensorIN(sensorIN)
    {
        ARMARX_CHECK_EXPRESSION(sensorOUT);
        adc_1_to_3_temperature.init(&sensorOUT->adc_1_to_3_temperature, config.adcTemperature, 0, false, "adc_1_to_3_temperature");
        adc_4_to_6_temperature.init(&sensorOUT->adc_4_to_6_temperature, config.adcTemperature, 0, false, "adc_4_to_6_temperature");

        ticksToVoltFactor = config.voltageFactor;

        conversionMatrix = config.ftCalibrationMatrix;
        //    conversionMatrix /= 160; // 32 * 5
        ARMARX_DEBUG << VAROUT(conversionMatrix);

        channelOrder = config.channelOrder;
        ARMARX_DEBUG << VAROUT(channelOrder);
        ARMARX_CHECK_EQUAL(channelOrder.size(), 6);

        std::vector<int> offsetList{config.offsetList.begin(), config.offsetList.end()};
        ARMARX_DEBUG << VAROUT(offsetList);
        ARMARX_CHECK_EQUAL(offsetList.size(), 6);
        offset << offsetList[0], offsetList[1], offsetList[2], offsetList[3], offsetList[4], offsetList[5];
        ARMARX_DEBUG << VAROUT(offset);

        filters.resize(6, armarx::control::rt_filters::RtMedianFilter(10));
    }


    void Data::rtReadSensorValues(const IceUtil::Time& /*sensorValuesTimestamp*/, const IceUtil::Time& /*timeSinceLastIteration*/)
    {
        adc_1_to_3_temperature.read();
        adc_4_to_6_temperature.read();

        Eigen::Vector6i permutation = channelOrder;
#ifdef FT_CALIB
        //rawForceTorque << sensorOUT->dms_adc_1, sensorOUT->dms_adc_2, sensorOUT->dms_adc_3, sensorOUT->dms_adc_4, sensorOUT->dms_adc_5, sensorOUT->dms_adc_6;
        //    ARMARX_INFO << "sensorOUT: " << sensorOUT->dms_adc_1 << " " << sensorOUT->dms_adc_2 << " " << sensorOUT->dms_adc_3 << " " << sensorOUT->dms_adc_4 << " " << sensorOUT->dms_adc_5 << " " << sensorOUT->dms_adc_6;
        Ice::IntSeq bestPermutation;
        int permutationsTried = 0;

        float min_score = 10000000;
        Eigen::Vector3f bestForceVec;
        //    float min_z_distance_to_0 = 10000000;
        //#define FT_CALIB
        do
        {
#endif

            Eigen::Vector6i rawForceTorqueUnordered;
            rawForceTorqueUnordered[0] = sensorOUT->dms_adc_1;
            rawForceTorqueUnordered[1] = sensorOUT->dms_adc_2;
            rawForceTorqueUnordered[2] = sensorOUT->dms_adc_3;
            rawForceTorqueUnordered[3] = sensorOUT->dms_adc_4;
            rawForceTorqueUnordered[4] = sensorOUT->dms_adc_5;
            rawForceTorqueUnordered[5] = sensorOUT->dms_adc_6;
            if (firstRun)
            {
                firstRun = false;
                //            offset = rawForceTorque;
            }
            rawForceTorque -= offset;

            rawForceTorque[permutation[0]] = rawForceTorqueUnordered[0];
            rawForceTorque[permutation[1]] = rawForceTorqueUnordered[1];
            rawForceTorque[permutation[2]] = rawForceTorqueUnordered[2];
            rawForceTorque[permutation[3]] = rawForceTorqueUnordered[3];
            rawForceTorque[permutation[4]] = rawForceTorqueUnordered[4];
            rawForceTorque[permutation[5]] = rawForceTorqueUnordered[5];

            //ARMARX_INFO << VAROUT(rawForceTorque);
            //    ARMARX_IMPORTANT << VAROUT(rawForceTorque);
            forceTorqueValues = conversionMatrix * (rawForceTorque.cast<float>() * ticksToVoltFactor);
#ifdef FT_CALIB
            forces = forceTorqueValues.block<3, 1>(0, 0);
            torques = forceTorqueValues.block<3, 1>(3, 0);
#else
            forces << filters.at(0).update(forceTorqueValues(0)), filters.at(1).update(forceTorqueValues(1)), filters.at(2).update(forceTorqueValues(2));
            torques << filters.at(3).update(forceTorqueValues(3)), filters.at(4).update(forceTorqueValues(4)), filters.at(5).update(forceTorqueValues(5));
#endif

#ifdef FT_CALIB
            float x_y_distance = fabs(10 - (forces(0) + forces(1)));
            float score = x_y_distance + fabs(forces(2)) + fabs(torques(2));
            if (score < min_score)
            {
                min_score = score;
                bestPermutation = permutation;
                bestForceVec = forces;
            }
            permutationsTried++;
        }
        while (boost::range::next_permutation(permutation));
        ARMARX_INFO << deactivateSpam(1) << VAROUT(min_score) << VAROUT(bestPermutation) << VAROUT(permutationsTried);
        forces = bestForceVec;
#endif
        //    ARMARX_IMPORTANT << "filters applied";
    }


    const Eigen::Vector3f& Data::getForces() const
    {
        return forces;
    }


    const Eigen::Vector3f& Data::getTorques() const
    {
        return torques;
    }


    float Data::getADC1to3Temperature() const
    {
        return adc_1_to_3_temperature.value;
    }


    float Data::getADC4to6Temperature() const
    {
        return adc_4_to_6_temperature.value;
    }


    float Data::getUpdateRate() const
    {
        return sensorOUT->update_rate / 3;
    }


    const Eigen::Vector6i& Data::getRawValues() const
    {
        return rawForceTorque;
    }

}
