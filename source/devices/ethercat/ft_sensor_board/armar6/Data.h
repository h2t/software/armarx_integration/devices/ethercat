/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// armarx
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValueForceTorque.h>
#include <armarx/control/ethercat/DataInterface.h>
#include <armarx/control/rt_filters/RtMedianFilter.h>

// robot_devices
#include <devices/ethercat/ft_sensor_board/armar6/Slave.h>
#include <devices/ethercat/ft_sensor_board/armar6/Config.h>


namespace Eigen
{
    using Matrix6f = Matrix<float, 6, 6>;
    using Vector6f = Matrix<float, 6, 1>;
    using Vector6i = Matrix<int, 6, 1>;
}


namespace devices::ethercat::ft_sensor_board::armar6
{

    class FTSensorDataValue :
        public armarx::SensorValueForceTorque
    {

    public:

        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION
        float adc_1_to_3_temperature;
        float adc_4_to_6_temperature;
        Eigen::Vector6f rawForceTorque;
        int updateRate;
        static SensorValueInfo<FTSensorDataValue> GetClassMemberInfo()
        {
            SensorValueInfo<FTSensorDataValue> svi;
            svi.addBaseClass<SensorValueForceTorque>();
            svi.addMemberVariable(&FTSensorDataValue::adc_1_to_3_temperature, "adc_1_to_3_temperature");
            svi.addMemberVariable(&FTSensorDataValue::adc_4_to_6_temperature, "adc_4_to_6_temperature");
            svi.addMemberVariable(&FTSensorDataValue::rawForceTorque, "rawForceTorque")
            .setVariantReportFunction(
                [](const IceUtil::Time & timestamp, const FTSensorDataValue * ptr)
            {
                return std::map<std::string, VariantBasePtr>
                {
                    {"adc_raw_1_to_3", {new armarx::TimedVariant {armarx::Vector3((Eigen::Vector3f) ptr->rawForceTorque.block<3, 1>(0, 0)), timestamp}}},
                    {"adc_raw_4_to_6", {new armarx::TimedVariant {armarx::Vector3((Eigen::Vector3f) ptr->rawForceTorque.block<3, 1>(3, 0)), timestamp}}}
                };
            }
            );
            return svi;
        }

    };


    class Data :
        public armarx::control::ethercat::DataInterface
    {

    public:

        Data(Config& config,
             SlaveOut* sensorOUT, SlaveIn* sensorIN);

        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override {}

        const Eigen::Vector3f& getForces() const;
        const Eigen::Vector3f& getTorques() const;
        float getADC1to3Temperature() const;
        float getADC4to6Temperature() const;
        float getUpdateRate() const;
        const Eigen::Vector6i& getRawValues() const;

    private:

        SlaveOut* sensorOUT;
        SlaveIn* sensorIN;

        armarx::control::ethercat::LinearConvertedValue<std::int32_t> adc_1_to_3_temperature;
        armarx::control::ethercat::LinearConvertedValue<std::int32_t> adc_4_to_6_temperature;

        Eigen::Matrix6f conversionMatrix;
        Eigen::Vector6i offset;
        float ticksToVoltFactor;
        Eigen::Vector6i rawForceTorque;
        Eigen::Vector6f forceTorqueValues;
        Eigen::Vector3f forces;
        Eigen::Vector3f torques;
        Eigen::Vector6i channelOrder;
        std::vector<armarx::control::rt_filters::RtMedianFilter> filters;
        bool firstRun = true;
        Eigen::Vector6f gravityOffset;

    };

}
