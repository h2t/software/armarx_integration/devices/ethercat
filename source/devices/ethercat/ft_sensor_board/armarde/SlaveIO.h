/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Jan Hausberg (jan dot hausberg at kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <cstdint>


namespace devices::ethercat::ft_sensor_board::armarde
{
    /*
     * Here belong the PDO mappings for ethercat.
     * The attribute packed is important.
     */

    /**
     * @brief PDO mapping sensorB->master
     */
    struct SlaveOut
    {
        // Object Entry 0x6000
        // Board Diagnostics
        std::int32_t mainCouter;
        std::int32_t mainUpdateRate;
        std::int32_t adc1UpdateRate;
        std::int32_t adc2UpdateRate;

        // Object Entry 0x6001
        // FTAdc
        std::int32_t dmsAdc1Value1;
        std::int32_t dmsAdc1Value2;
        std::int32_t dmsAdc1Value3;
        std::int32_t dmsAdc2Value4;
        std::int32_t dmsAdc2Value5;
        std::int32_t dmsAdc2Value6;
        std::int32_t temperatureAdc1;
        std::int32_t supplyVoltageShunt;
        std::int32_t supplyCurrentShunt;
        std::int32_t temperatureAdc2;
        std::uint8_t dmsAdc1Crc1;
        std::uint8_t dmsAdc1Crc2;
        std::uint8_t dmsAdc1Crc3;
        std::uint8_t dmsAdc2Crc4;
        std::uint8_t dmsAdc2Crc5;
        std::uint8_t dmsAdc2Crc6;
        std::uint8_t temperatureAdc1Crc;
        std::uint8_t supplyVoltageShuntCrc;
        std::uint8_t supplyCurrentShuntCrc;
        std::uint8_t temperatureAdc2Crc;
        std::int16_t pad16;


    } __attribute__((__packed__));

    /**
     * @brief PDO mapping master->sensorB
     */
    struct SlaveIn
    {
    } __attribute__((__packed__));


} // namespace devices::ethercat::ft_sensor_board::armarde
