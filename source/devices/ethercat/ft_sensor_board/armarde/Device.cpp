/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Armar6RT::ArmarXObjects::Armar6IceKinematicUnit
 * @author     Markus Swarowsky ( markus dot swarowsky at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Device.h"

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/RobotNodeSet.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/util/StringHelperTemplates.h>

//#include <devices/ethercat/ft_sensor_board/armarde/Slave.h>


namespace devices::ethercat::ft_sensor_board::armarde
{

    Device::Device(armarx::control::hardware_config::DeviceConfig& hwConfig,
                   const VirtualRobot::RobotPtr& robot) :
        DeviceBase(hwConfig.getName()),
        SensorDevice(hwConfig.getName()),
        DeviceInterface(hwConfig.getName()),
        dataPtr(nullptr),
        slavePtr(nullptr),
        slaveIdentifier(hwConfig.getOnlySlaveConfig().getIdentifier()),
        dataConfig(hwConfig)
    {
        this->sensorNode = robot->getSensor(getDeviceName());
        ARMARX_CHECK_EXPRESSION(this->sensorNode) << getDeviceName();
        this->robot = sensorNode->getRobotNode()->getRobot();
        auto dynamicRoot = robot->getRobotNode(dataConfig.firstRobotNodeNameAfterSensor);
        auto parent = sensorNode->getParent();
        massSum = 0;
        Eigen::Vector3f averageCoM;
        std::set<VirtualRobot::SceneObjectPtr> allChildren;
        std::vector<std::string> nodeNames;

        auto children = dynamicRoot->getChildren();
        while (!children.empty())
        {
            auto child = children.back();
            if (!allChildren.count(child))
            {
                allChildren.insert(child);
                auto newChildren = child->getChildren();
                children.pop_back();
                children.insert(children.end(), newChildren.begin(), newChildren.end());
            }
        }

        for (auto& child : allChildren)
        {
            if (dynamic_cast<VirtualRobot::RobotNode*>(child.get()))
            {
                nodeNames.push_back(child->getName());
            }
        }

        nodeNames.push_back(dynamicRoot->getName());
        ARMARX_DEBUG << "CoM nodes: " << nodeNames;

        VirtualRobot::RobotNodeSetPtr handNodeSet = VirtualRobot::RobotNodeSet::createRobotNodeSet(
            robot, "handBodiesSet", nodeNames, dynamicRoot->getName());
        massSum = handNodeSet->getMass();
        ARMARX_INFO << getDeviceName() << " Hand mass: " << massSum;
        gravityVec = Eigen::Vector3f(0, 0, 9.81f * massSum);

        //    comLocationLocal = Eigen::Matrix4f::Identity();
        //    comLocationLocal.block<3, 1>(0, 3) = handNodeSet->getCoM();
        //    ARMARX_INFO << parent->getName() << " CoM Global: " << comLocationLocal << " parent global: " << parent->getGlobalPose().block<3, 1>(0, 3);
        comLocationLocal = sensorNode->toLocalCoordinateSystemVec(handNodeSet->getCoM());
        Eigen::Vector3f comLocationLocalJeff =
            parent->getGlobalPose().block<3, 3>(0, 0).transpose() *
            (handNodeSet->getCoM() - sensorNode->getGlobalPose().block<3, 1>(0, 3));

        transformationToParent =
            sensorNode->getTransformationTo(sensorNode->getParent()).block<3, 3>(0, 0).transpose();
        comLocationLocal = transformationToParent * comLocationLocal;
        ARMARX_INFO << getDeviceName() << VAROUT(comLocationLocal) << VAROUT(comLocationLocalJeff);
        //    comLocationLocal << -0.0044873, -0.00233698, 0.0989811;
        //    comLocationLocal *= 1000;
        //    massSum = 1.11726;
        ARMARX_INFO << getDeviceName() << VAROUT(transformationToParent);
        gravityOffsetVecTorque = Eigen::Vector3f::Zero();
        gravityOffsetVecForce = Eigen::Vector3f::Zero();
    }


    armarx::control::ethercat::DeviceInterface::TryAssignResult
    Device::tryAssign(armarx::control::ethercat::SlaveInterface& slave)
    {
        namespace ecat = armarx::control::ethercat;
        using Result = DeviceInterface::TryAssignResult;

        Result result = ecat::tryAssignUsingSerialNo(slave, slavePtr, slaveIdentifier.serialNumber);

        return result;
    }


    armarx::control::ethercat::DeviceInterface::AllAssignedResult
    Device::onAllAssigned()
    {
        using Result = DeviceInterface::AllAssignedResult;

        if (slavePtr)
        {
            return Result::ok;
        }

        return Result::slavesMissing;
    }

    std::string
    Device::getClassName() const
    {
        return "FTSensorBoardArmarde";
    }

    void
    Device::postSwitchToSafeOp()
    {
        if (!dataPtr)
        {
            ARMARX_INFO << "Creating FTSensorData for device " << getDeviceName()
                        << " of slave with id " << slavePtr->getSlaveNumber()
                        << "\nOutputPtrs = " << slavePtr->getOutputsPtr();
            dataPtr = std::make_shared<Data>(dataConfig, slavePtr->getOutputsPtr());
        }
        ARMARX_CHECK_NOT_NULL(dataPtr);
    }

    void
    Device::postSwitchToOp()
    {
    }


    Eigen::Vector3f
    changeDirectionFrame(const Eigen::Vector3f direction,
                         const VirtualRobot::SceneObjectPtr& currentFrame,
                         const VirtualRobot::SceneObjectPtr& newFrame)
    {
        Eigen::Matrix4f toNewFrame = currentFrame->getTransformationTo(newFrame);
        toNewFrame(0, 3) = 0; // only rotation is important for direction
        toNewFrame(1, 3) = 0;
        toNewFrame(2, 3) = 0;

        return toNewFrame.block(0, 0, 3, 3).inverse() * direction;
    }


    Eigen::Vector3f
    Device::changeDirectionFrameToGlobal(const Eigen::Vector3f direction,
                                         const VirtualRobot::SceneObjectPtr& currentFrame)
    {
        Eigen::Matrix4f toNewFrame =
            robot->getGlobalPose() * currentFrame->getTransformationFrom(robot->getRootNode());

        toNewFrame(0, 3) = 0; // only rotation is important for direction
        toNewFrame(1, 3) = 0;
        toNewFrame(2, 3) = 0;

        return toNewFrame.block(0, 0, 3, 3).inverse() * direction;
    }


    Eigen::Vector3f
    Device::changeDirectionFrameFromGlobal(const Eigen::Vector3f& globalDirection,
                                           const VirtualRobot::SceneObjectPtr& newFrame)
    {
        Eigen::Matrix4f toNewFrame =
            robot->getGlobalPose() * newFrame->getTransformationFrom(robot->getRootNode());

        toNewFrame(0, 3) = 0; // only rotation is important for direction
        toNewFrame(1, 3) = 0;
        toNewFrame(2, 3) = 0;

        return toNewFrame.block(0, 0, 3, 3).inverse() * globalDirection;
    }


    void
    Device::updateSensorValueStruct()
    {      
        sensorValue.force = dataPtr->getForces();
        sensorValue.torque = dataPtr->getTorques();

        sensorValue.force = transformationToParent * sensorValue.force;
        sensorValue.torque = transformationToParent * sensorValue.torque;

        //    ARMARX_INFO << deactivateSpam(1) << VAROUT(dataPtr->getForces()) << VAROUT(sensorValue.force);

        sensorValue.mainCounter = dataPtr->getMainCounter();
        sensorValue.mainUpdateRate = dataPtr->getMainUpdateRate();
        sensorValue.adc1UpdateRate = dataPtr->getAdc1UpdateRate();
        sensorValue.adc2UpdateRate = dataPtr->getAdc2UpdateRate();

        sensorValue.temperatureAdc1 = dataPtr->getTemperatureAdc1();
        sensorValue.supplyVotageShunt = dataPtr->getSupplyVoltageShunt();
        sensorValue.supplyCurrentShunt = dataPtr->getSupplyCurrentShunt();
        sensorValue.temperatureAdc2 = dataPtr->getTemperatureAdc2();

        sensorValue.rawForceTorque = dataPtr->getRawValues().cast<float>();
        Eigen::Vector3f localGravityForceVec =
            sensorNode->getParent()->getGlobalPose().block<3, 3>(0, 0).transpose() * gravityVec;
        Eigen::Vector3f localGravityTorqueVec =
            (comLocationLocal * 0.001).cross(localGravityForceVec);

        if (startTime.toMicroSeconds() == 0)
        {
            startTime = IceUtil::Time::now();
        }

        // get force / torque offset
        if ((IceUtil::Time::now() - startTime).toSecondsDouble() >
                1.0 // wait a moment after start up since the controllers shaked the robot durting start up
            && gravityOffsetVecForce.norm() < 0.001f) // if value is not set yet, it is zero
        {
            // this offset is used to set the force value to the value it should measure (based on joint angles and dynamic model). This assumes that nothing is in the hands or touching the hands after start up
            gravityOffsetVecForce = sensorValue.force + localGravityForceVec;
            gravityOffsetVecTorque = sensorValue.torque + localGravityTorqueVec;
        }

        // eliminate force / torque offset and hand gravitational force from raw data (now all data are in the local frame of the sensorNode's parent)
        sensorValue.force -= gravityOffsetVecForce;
        sensorValue.torque -= gravityOffsetVecTorque;
        sensorValue.gravityCompensatedForce = sensorValue.force + localGravityForceVec;
        sensorValue.gravityCompensatedTorque = sensorValue.torque + localGravityTorqueVec;

        sensorValue.temperature = dataPtr->getTemperature();
    }


    std::string
    Device::getReportingFrame() const
    {
        return sensorNode->getParent()->getName();
    }


    std::shared_ptr<Data>
    Device::getData() const
    {
        if (dataPtr == nullptr)
        {
            throw armarx::LocalException("Joint has no data set, call initData(...) before");
        }

        return dataPtr;
    }


    const std::string&
    Device::getJointName() const
    {
        return getDeviceName();
    }


    void
    Device::rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                               const IceUtil::Time& timeSinceLastIteration)
    {
        if (dataPtr == nullptr)
        {
            throw armarx::LocalException("Joint has no data set, call initData(...) before");
        }
        dataPtr->rtReadSensorValues(sensorValuesTimestamp, timeSinceLastIteration);

        updateSensorValueStruct();
    }


    const armarx::SensorValueBase*
    Device::getSensorValue() const
    {
        return &sensorValue;
    }

} // namespace devices::ethercat::ft_sensor_board::armarde
