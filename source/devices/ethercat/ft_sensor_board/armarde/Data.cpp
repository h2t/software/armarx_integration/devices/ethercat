/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Jan Hausberg (jan dot hausberg at kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Data.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/util/StringHelperTemplates.h>

namespace devices::ethercat::ft_sensor_board::armarde
{
    Data::Data(const DataConfig& config, SlaveOut* outputs) :
        outputs(outputs)
    {
        ARMARX_CHECK_EXPRESSION(outputs);

        ticksToVoltFactor = config.ticksToVoltFactor;

        conversionMatrix = config.conversionMatrix;
        //    conversionMatrix /= 160; // 32 * 5
        ARMARX_DEBUG << VAROUT(conversionMatrix);

        channelOrder = config.channelOrder;
        ARMARX_DEBUG << VAROUT(channelOrder);
        ARMARX_CHECK_EQUAL(channelOrder.size(), 6);

        std::vector<int> offsetList(config.offsetList.begin(), config.offsetList.end());
        ARMARX_DEBUG << VAROUT(offsetList);
        ARMARX_CHECK_EQUAL(offsetList.size(), 6);
        offset << offsetList[0], offsetList[1], offsetList[2], offsetList[3], offsetList[4], offsetList[5];
        ARMARX_DEBUG << VAROUT(offset);

        filters.resize(6, armarx::control::rt_filters::RtMedianFilter(10));

        temperature.init(&(outputs->supplyCurrentShunt), config.temperature);
    }

    std::vector<int> Data::readIntList(std::string str)
    {
        std::vector<std::string> parts = armarx::Split(str, "\t \n", true, true);
        std::vector<int> result;
        for (const std::string& s : parts)
        {
            result.push_back(atoi(s.c_str()));
        }
        return result;
    }

    void
    Data::rtReadSensorValues(const IceUtil::Time& /*sensorValuesTimestamp*/,
                             const IceUtil::Time& /*timeSinceLastIteration*/)
    {
        mainCounter = outputs->mainCouter;
        mainUpdateRate = outputs->mainUpdateRate;
        adc1UpdateRate = outputs->adc1UpdateRate;
        adc2UpdateRate = outputs->adc2UpdateRate;

        supplyVotageShunt = outputs->supplyVoltageShunt;
        temperatureAdc1 = getTemperatureAdcInDeg(outputs->temperatureAdc1);
        temperatureAdc2 = getTemperatureAdcInDeg(outputs->temperatureAdc2);
        supplyCurrentShunt = outputs->supplyCurrentShunt;

        Eigen::Vector6i permutation = channelOrder;

        Eigen::Vector6i rawForceTorqueUnordered;
        rawForceTorqueUnordered[0] = outputs->dmsAdc1Value1;
        rawForceTorqueUnordered[1] = outputs->dmsAdc1Value2;
        rawForceTorqueUnordered[2] = outputs->dmsAdc1Value3;
        rawForceTorqueUnordered[3] = outputs->dmsAdc2Value4;
        rawForceTorqueUnordered[4] = outputs->dmsAdc2Value5;
        rawForceTorqueUnordered[5] = outputs->dmsAdc2Value6;
        if (firstRun)
        {
            firstRun = false;
            //            offset = rawForceTorque;
        }
        rawForceTorque -= offset;

        rawForceTorque[permutation[0]] = rawForceTorqueUnordered[0];
        rawForceTorque[permutation[1]] = rawForceTorqueUnordered[1];
        rawForceTorque[permutation[2]] = rawForceTorqueUnordered[2];
        rawForceTorque[permutation[3]] = rawForceTorqueUnordered[3];
        rawForceTorque[permutation[4]] = rawForceTorqueUnordered[4];
        rawForceTorque[permutation[5]] = rawForceTorqueUnordered[5];

        //ARMARX_INFO << VAROUT(rawForceTorque);
        //    ARMARX_IMPORTANT << VAROUT(rawForceTorque);
        forceTorqueValues = conversionMatrix * (rawForceTorque.cast<float>() * ticksToVoltFactor);
        forces << filters.at(0).update(forceTorqueValues(0)), filters.at(1).update(forceTorqueValues(1)), filters.at(2).update(forceTorqueValues(2));
        torques << filters.at(3).update(forceTorqueValues(3)), filters.at(4).update(forceTorqueValues(4)), filters.at(5).update(forceTorqueValues(5));

        //    ARMARX_IMPORTANT << "filters applied";

        temperature.read();
    }

    void
    Data::rtWriteTargetValues(const IceUtil::Time& /*sensorValuesTimestamp*/,
                              const IceUtil::Time& /*timeSinceLastIteration*/)
    {
    }

    std::int32_t
    Data::getMainCounter() const
    {
        return mainCounter;
    }

    std::int32_t
    Data::getMainUpdateRate() const
    {
        return mainUpdateRate;
    }

    std::int32_t
    Data::getAdc1UpdateRate() const
    {
        return adc1UpdateRate;
    }
    std::int32_t
    Data::getAdc2UpdateRate() const
    {
        return adc2UpdateRate;
    }

    float
    Data::getTemperatureAdc1() const
    {
        return temperatureAdc1;
    }

    std::int32_t
    Data::getSupplyVoltageShunt() const
    {
        return supplyVotageShunt;
    }

    std::int32_t
    Data::getSupplyCurrentShunt() const
    {
        return supplyCurrentShunt;
    }

    float
    Data::getTemperatureAdc2() const
    {
        return temperatureAdc2;
    }

    const Eigen::Vector3f&
    Data::getForces() const
    {
        return forces;
    }


    const Eigen::Vector3f&
    Data::getTorques() const
    {
        return torques;
    }

    const Eigen::Vector6i&
    Data::getRawValues() const
    {
        return rawForceTorque;
    }

    float
    Data::getTemperature() const
    {
        return temperature.value;
    }

    float
    Data::getTemperatureAdcInDeg(std::int32_t rawValue) const
    {
        std::int32_t valueSigned = rawValue << 8;
        valueSigned = valueSigned / 256;
        //From Datasheet: Temperature (°C) = [(Temperature Reading (μV) – 122,400) / 420 μV/°C] + 25°C

        float tempUv = static_cast<float>(valueSigned) * 5000000.0f / static_cast<float>(0x1000000); //multiplied with 2*ref voltage (full-scale ADC-range = +-Vref, in µV) and divided by ADC-resolution (24 bit)
        float tempDegC = (tempUv - 122400.0f) / 420.0f + 25.0f;

        return tempDegC;
    }

} // namespace devices::ethercat::ft_sensor_board::armarde
