/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Jan Hausberg (jan dot hausberg at kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValueForceTorque.h>
#include <armarx/control/ethercat/DataInterface.h>
#include <armarx/control/rt_filters/RtMedianFilter.h>

#include <devices/ethercat/ft_sensor_board/armarde/SlaveIO.h>
#include <devices/ethercat/ft_sensor_board/armarde/Config.h>

namespace Eigen
{
    using Matrix6f = Matrix<float, 6, 6>;
    using Vector6f = Matrix<float, 6, 1>;
    using Vector6i = Matrix<int, 6, 1>;
}

namespace devices::ethercat::ft_sensor_board::armarde
{
    /**
     * @brief This class contains all sensor data, that should be given to armarx.
     * If, for example, some data should be displayed in the gui, then it is necessary that it
     * is included in this class.
     */
    class FTSensorDataValue : virtual public armarx::SensorValueForceTorque
    /*
     * Inherit from each class in RobotAPI/components/units/RobotUnit/SensorValues that makes sense.
     * E.g. for a sensor actor unit armarx::SensorValue1DoFRealActuatorWithStatus
     * In that case there are already attributes defined. Do not forget to update them as well!
     */
    {
    public:
        FTSensorDataValue() = default;

        FTSensorDataValue(const FTSensorDataValue&) = default;

        FTSensorDataValue& operator=(const FTSensorDataValue&) = default;

        FTSensorDataValue(FTSensorDataValue&& o)
        {
            *this = o;
        }

        FTSensorDataValue&
        operator=(FTSensorDataValue&& o)
        {
            *this = o;
            return *this;
        }

        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION

        std::int32_t mainCounter;
        std::int32_t mainUpdateRate;
        std::int32_t adc1UpdateRate;
        std::int32_t adc2UpdateRate;

        float temperatureAdc1;
        std::int32_t supplyVotageShunt;
        std::int32_t supplyCurrentShunt;
        float temperatureAdc2;

        Eigen::Vector6f rawForceTorque;
        float temperature;


        static SensorValueInfo<FTSensorDataValue>
        GetClassMemberInfo()
        {
            SensorValueInfo<FTSensorDataValue> svi;

            svi.addBaseClass<SensorValueForceTorque>();

            svi.addMemberVariable(&FTSensorDataValue::mainCounter, "mainCounter");
            svi.addMemberVariable(&FTSensorDataValue::mainUpdateRate, "mainUpdateRate");
            svi.addMemberVariable(&FTSensorDataValue::adc1UpdateRate, "adc1UpdateRate");
            svi.addMemberVariable(&FTSensorDataValue::adc2UpdateRate, "adc2UpdateRate");

            svi.addMemberVariable(&FTSensorDataValue::temperatureAdc1, "temperatureAdc1");
            svi.addMemberVariable(&FTSensorDataValue::supplyVotageShunt, "supplyVotageShunt");
            svi.addMemberVariable(&FTSensorDataValue::supplyCurrentShunt, "supplyCurrentShunt");
            svi.addMemberVariable(&FTSensorDataValue::temperatureAdc2, "temperatureAdc2");

            svi.addMemberVariable(&FTSensorDataValue::rawForceTorque, "rawForceTorque")
            .setVariantReportFunction(
                [](const IceUtil::Time & timestamp, const FTSensorDataValue * ptr)
            {
                return std::map<std::string, VariantBasePtr>
                {
                    {"adc_raw_1_to_3", {new armarx::TimedVariant {armarx::Vector3((Eigen::Vector3f) ptr->rawForceTorque.block<3, 1>(0, 0)), timestamp}}},
                    {"adc_raw_4_to_6", {new armarx::TimedVariant {armarx::Vector3((Eigen::Vector3f) ptr->rawForceTorque.block<3, 1>(3, 0)), timestamp}}}
                };
            }
            );
            svi.addMemberVariable(&FTSensorDataValue::temperature, "temperature");

            return svi;
        }
    };


    class Data : public armarx::control::ethercat::DataInterface
    {
    public:
        Data(const DataConfig& config, SlaveOut* outputs);

        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration) override;
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                 const IceUtil::Time& timeSinceLastIteration) override;

        std::int32_t getMainCounter() const;
        std::int32_t getMainUpdateRate() const;
        std::int32_t getAdc1UpdateRate() const;
        std::int32_t getAdc2UpdateRate() const;

        float getTemperatureAdc1() const;
        std::int32_t getSupplyVoltageShunt() const;
        std::int32_t getSupplyCurrentShunt() const;
        float getTemperatureAdc2() const;

        const Eigen::Vector3f& getForces() const;
        const Eigen::Vector3f& getTorques() const;
        const Eigen::Vector6i& getRawValues() const;
        float getTemperature() const;

    private:
        SlaveOut* outputs;

        std::int32_t mainCounter;
        std::int32_t mainUpdateRate;
        std::int32_t adc1UpdateRate;
        std::int32_t adc2UpdateRate;

        float temperatureAdc1;
        std::int32_t supplyVotageShunt;
        std::int32_t supplyCurrentShunt;
        float temperatureAdc2;

        Eigen::Matrix6f conversionMatrix;
        Eigen::Vector6i offset;
        float ticksToVoltFactor;
        Eigen::Vector6i rawForceTorque;
        Eigen::Vector6f forceTorqueValues;
        Eigen::Vector3f forces;
        Eigen::Vector3f torques;
        Eigen::Vector6i channelOrder;
        std::vector<armarx::control::rt_filters::RtMedianFilter> filters;
        bool firstRun = true;
        armarx::control::ethercat::LinearConvertedValue<std::int32_t> temperature;

        std::vector<int> readIntList(std::string str);

        float getTemperatureAdcInDeg(std::int32_t rawValue) const;

    };

    using DataPtr = std::shared_ptr<Data>;
} // namespace devices::ethercat::ft_sensor_board::armarde
