#pragma once

#include <armarx/control/hardware_config/DeviceConfig.h>

namespace devices::ethercat::ft_sensor_board::armarde
{
    struct DataConfig
    {
        DataConfig(armarx::control::hardware_config::DeviceConfig& hwConfig)
            : firstRobotNodeNameAfterSensor(hwConfig.getString("FirstRobotNodeNameAfterSensor")),
              ticksToVoltFactor(hwConfig.getFloat("ticksToVoltFactor")),
              conversionMatrix(hwConfig.getMatrix<float, 6, 6>("FTCalibrationMatrix")),
              channelOrder(hwConfig.getMatrix<int, 6, 1>("ChannelOrder")),
              offsetList(hwConfig.getIntList("Offset")),
              temperature(hwConfig.getLinearConfig("temperature"))
        {
        }

        std::string firstRobotNodeNameAfterSensor;
        float ticksToVoltFactor;
        Eigen::Matrix<float, 6, 6> conversionMatrix;
        Eigen::Matrix<int, 6, 1> channelOrder;
        std::list<int> offsetList;
        armarx::control::hardware_config::types::LinearConfig temperature;
    };
}
