/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Jan Hausberg (jan dot hausberg at kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <devices/ethercat/ft_sensor_board/armarde/SlaveIO.h>

namespace armarx::control::ethercat
{
    class SlaveInterface;
}

namespace devices::ethercat::ft_sensor_board::armarde
{

    class ErrorDecoder
    {
    public:
        ErrorDecoder(SlaveOut* outputs);

        std::uint8_t calcDmsAdc1Crc1() const;

        std::uint8_t calcDmsAdc1Crc2() const;

        std::uint8_t calcDmsAdc1Crc3() const;

        std::uint8_t calcDmsAdc2Crc4() const;

        std::uint8_t calcDmsAdc2Crc5() const;

        std::uint8_t calcDmsAdc2Crc6() const;

        std::uint8_t calcTemperatureAdc1Crc() const;

        std::uint8_t calcSupplyVoltageShuntCrc() const;

        std::uint8_t calcSupplyCurrentShuntCrc() const;

        std::uint8_t calcTemperatureAdc2Crc() const;

    private:
        SlaveOut* outputs;

        std::uint8_t calculateCrc8ADS1263(std::uint8_t* bytes, std::size_t numBytes) const;
    };
} // namespace devices::ethercat::ft_sensor_board::armarde
