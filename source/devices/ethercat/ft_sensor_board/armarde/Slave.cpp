/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Jan Hausberg (jan dot hausberg at kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Slave.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <armarx/control/ethercat/Bus.h>

#include <devices/ethercat/common/h2t_devices.h>


namespace devices::ethercat::ft_sensor_board::armarde
{
    Slave::Slave(const armarx::control::ethercat::SlaveIdentifier slaveIdentifier_) :
        SlaveInterfaceWithIO(slaveIdentifier_),
        errorDecoder(nullptr),
        bus(armarx::control::ethercat::Bus::getBus())
    {
        setTag(this->slaveIdentifier.getName());
    }


    void
    Slave::execute()
    {
    }


    bool
    Slave::prepareForRun()
    {
        return true;
    }


    bool
    Slave::shutdown()
    {
        return true;
    }


    void
    Slave::doMappings()
    {
    }


    bool
    Slave::hasError()
    {
        // Check for all errors and return false if there are no errors
        bool hasError = false;

        std::uint8_t crc = errorDecoder.calcDmsAdc1Crc1();
        if (crc != outputs->dmsAdc1Crc1)
        {
            SLAVE_WARNING(getSlaveIdentifier(),
                          "DMS ADC 1 CRC 1 failed. Expected: %#02X, but got: %#02X",
                          outputs->dmsAdc1Crc1,
                          crc)
                .deactivateSpam(1);
            hasError = true;
        }

        crc = errorDecoder.calcDmsAdc1Crc2();
        if (crc != outputs->dmsAdc1Crc2)
        {
            SLAVE_WARNING(getSlaveIdentifier(),
                          "DMS ADC 1 CRC 2 failed. Expected: %#02X, but got: %#02X",
                          outputs->dmsAdc1Crc2,
                          crc)
                .deactivateSpam(1);
            hasError = true;
        }

        crc = errorDecoder.calcDmsAdc1Crc3();
        if (crc != outputs->dmsAdc1Crc3)
        {
            SLAVE_WARNING(getSlaveIdentifier(),
                          "DMS ADC 1 CRC 3 failed. Expected: %#02X, but got: %#02X",
                          outputs->dmsAdc1Crc3,
                          crc)
                .deactivateSpam(1);
            hasError = true;
        }

        crc = errorDecoder.calcDmsAdc2Crc4();
        if (crc != outputs->dmsAdc2Crc4)
        {
            SLAVE_WARNING(getSlaveIdentifier(),
                          "DMS ADC 2 CRC 4 failed. Expected: %#02X, but got: %#02X",
                          outputs->dmsAdc2Crc4,
                          crc)
                .deactivateSpam(1);
            hasError = true;
        }

        crc = errorDecoder.calcDmsAdc2Crc5();
        if (crc != outputs->dmsAdc2Crc5)
        {
            SLAVE_WARNING(getSlaveIdentifier(),
                          "DMS ADC 2 CRC 5 failed. Expected: %#02X, but got: %#02X",
                          outputs->dmsAdc2Crc5,
                          crc)
                .deactivateSpam(1);
            hasError = true;
        }

        crc = errorDecoder.calcDmsAdc2Crc6();
        if (crc != outputs->dmsAdc2Crc6)
        {
            SLAVE_WARNING(getSlaveIdentifier(),
                          "DMS ADC 2 CRC 6 failed. Expected: %#02X, but got: %#02X",
                          outputs->dmsAdc2Crc6,
                          crc)
                .deactivateSpam(1);
            hasError = true;
        }

        crc = errorDecoder.calcTemperatureAdc1Crc();
        if (crc != outputs->temperatureAdc1Crc)
        {
            SLAVE_WARNING(getSlaveIdentifier(),
                          "Temperature ADC 1 CRC failed. Expected: %#02X, but got: %#02X",
                          outputs->temperatureAdc1Crc,
                          crc)
                .deactivateSpam(1);
            hasError = true;
        }

        crc = errorDecoder.calcTemperatureAdc2Crc();
        if (crc != outputs->temperatureAdc2Crc)
        {
            SLAVE_WARNING(getSlaveIdentifier(),
                          "Temperature ADC 2 CRC failed. Expected: %#02X, but got: %#02X",
                          outputs->temperatureAdc2Crc,
                          crc)
                .deactivateSpam(1);
            hasError = true;
        }

        crc = errorDecoder.calcSupplyVoltageShuntCrc();
        if (crc != outputs->supplyVoltageShuntCrc)
        {
            SLAVE_WARNING(getSlaveIdentifier(),
                          "Supply-Voltage Shunt CRC failed. Expected: %#02X, but got: %#02X",
                          outputs->supplyVoltageShuntCrc,
                          crc)
                .deactivateSpam(1);
            hasError = true;
        }

        crc = errorDecoder.calcSupplyCurrentShuntCrc();
        if (crc != outputs->supplyCurrentShuntCrc)
        {
            SLAVE_WARNING(getSlaveIdentifier(),
                          "Supply-Current Shunt CRC failed. Expected: %#02X, but got: %#02X",
                          outputs->supplyCurrentShuntCrc,
                          crc)
                .deactivateSpam(1);
            hasError = true;
        }

        return hasError;
    }


    void
    Slave::prepareForSafeOp()
    {
    }


    void
    Slave::finishPreparingForSafeOp()
    {
    }


    void
    Slave::prepareForOp()
    {
        // Here the inputs and outputs pointers are initialized
        errorDecoder = ErrorDecoder(getOutputsPtr());
    }


    void
    Slave::finishPreparingForOp()
    {
    }

    bool
    Slave::isEmergencyStopActive() const
    {
        // This needs to be implemented, if the slave supports emergency stop detection.
        return false;
    }

    bool
    Slave::recoverFromEmergencyStop()
    {
        return true;
    }

    bool
    Slave::isSlaveIdentifierAccepted(const armarx::control::ethercat::SlaveIdentifier& sid)
    {
        // Product Code is different for each type of slave
        // The Slave Identifier can for example be obtained using EtherKITten
        const std::uint32_t correctProductCode = 12304;
        return sid.vendorID == common::H2TVendorId and sid.productCode == correctProductCode;
    }

    std::string
    Slave::getDefaultName()
    {
        return "FTSensorBoardArmarde";
    }


} // namespace devices::ethercat::ft_sensor_board::armarde
