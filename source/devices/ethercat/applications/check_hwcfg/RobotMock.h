#pragma once

#include <VirtualRobot/Robot.h>
#include "SensorMock.h"

namespace devices::ethercat {
    class RobotMock : public VirtualRobot::Robot
    {
    public:
        RobotMock() : VirtualRobot::Robot("Mock")
        {
        }

        void setRootNode(VirtualRobot::RobotNodePtr node) override
        {
        }

        VirtualRobot::RobotNodePtr getRootNode() const override
        {
            return {};
        }

        void registerRobotNode(VirtualRobot::RobotNodePtr node) override
        {

        }

        void deregisterRobotNode(VirtualRobot::RobotNodePtr node) override
        {

        }

        bool hasRobotNode(VirtualRobot::RobotNodePtr node) const override
        {
            return robotNode == node;
        }

        bool hasRobotNode(const std::string& robotNodeName) const override
        {
            return robotNodeName == rnName;
        }

        void setRobotNode(std::string name, VirtualRobot::RobotNodePtr robotNode)
        {
            this->rnName = name;
            this->robotNode = robotNode;
        }

        VirtualRobot::RobotNodePtr getRobotNode(const std::string& robotNodeName) const override
        {
            if (robotNodeName == rnName)
            {
                return robotNode;
            }
            else
            {
                return {};
            }
        }

        void getRobotNodes(std::vector< VirtualRobot::RobotNodePtr >& storeNodes, bool clearVector = true) const  override
        {

        }

        void registerRobotNodeSet(VirtualRobot::RobotNodeSetPtr nodeSet) override
        {

        }

        void deregisterRobotNodeSet(VirtualRobot::RobotNodeSetPtr nodeSet) override
        {

        }

        VirtualRobot::RobotNodeSetPtr getRobotNodeSet(const std::string& nodeSetName) const override
        {
            return {};
        }

        void getRobotNodeSets(std::vector<VirtualRobot::RobotNodeSetPtr>& storeNodeSet) const override
        {

        }

        void registerEndEffector(VirtualRobot::EndEffectorPtr endEffector) override
        {

        }

        bool hasEndEffector(const std::string& endEffectorName) const override
        {
            return false;
        }

        VirtualRobot::EndEffectorPtr getEndEffector(const std::string& endEffectorName) const override
        {
            return {};
        }

        void getEndEffectors(std::vector<VirtualRobot::EndEffectorPtr>& storeEEF) const override
        {

        }

        virtual bool hasRobotNodeSet(const std::string& name) const override
        {
            return false;
        }

        virtual void setGlobalPose(const Eigen::Matrix4f& globalPose, bool applyValues) override
        {

        }

        virtual std::shared_ptr<VirtualRobot::Sensor> getSensor(const std::string& name) const override
        {
            return std::make_shared<SensorMock>(std::weak_ptr(robotNode), "name");
        }

        void init(VirtualRobot::RobotPtr ptr)
        {
            self = ptr;
        }

        VirtualRobot::RobotPtr self;
        VirtualRobot::RobotNodePtr robotNode;
        std::string rnName = "";
    };
}
