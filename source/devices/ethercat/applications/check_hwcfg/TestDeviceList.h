#pragma once

#include <map>
#include <functional>
#include <armarx/control/hardware_config/DeviceConfig.h>
#include <VirtualRobot/Robot.h>

namespace devices::ethercat {
    class TestDeviceList
    {
    public:
        void registerDevice(std::string typeName, std::function<void(std::shared_ptr<armarx::control::hardware_config::DeviceConfig>&)> testFunc)
        {
            m[typeName] = testFunc;
        }

        void testDevice(std::shared_ptr<armarx::control::hardware_config::DeviceConfig>& conf)
        {
            auto it = m.find(conf->getType());
            if (it == m.end())
            {
                std::cout << "Device with name '" << conf->getName() << "' and type '" << conf->getType() << "' not tested" << std::endl << std::endl;
                return;
            }
            (it->second)(conf);
        }

    private:
        std::map<std::string, std::function<void(std::shared_ptr<armarx::control::hardware_config::DeviceConfig>&)>> m;
    };
}
