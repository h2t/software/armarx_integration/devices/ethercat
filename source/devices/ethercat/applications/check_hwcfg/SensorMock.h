#pragma once

#include <VirtualRobot/Nodes/Sensor.h>

namespace devices::ethercat {
    class SensorMock : public VirtualRobot::Sensor
    {
    public:
        SensorMock(std::weak_ptr<VirtualRobot::GraspableSensorizedObject> node,
                   const std::string& name) : VirtualRobot::Sensor(node, name)
        {

        }

        std::shared_ptr<VirtualRobot::Sensor> _clone(const std::shared_ptr<VirtualRobot::GraspableSensorizedObject> node, const std::shared_ptr<VirtualRobot::VisualizationNode> visualizationModel, float scaling) override
        {
            return {};
        }
    };
}
