#include <armarx/control/ethercat/RTUnit.h>
#include <armarx/control/hardware_config/ConfigParser.h>

// Devices armarde
#include <devices/ethercat/sensor_actor_unit/armar7de/Device.h>
#include <devices/ethercat/head_board/armar7de/Device.h>
#include <devices/ethercat/platform/armar6_mecanum/Device.h>
#include <devices/ethercat/torso/armar6_prismatic/Device.h>
#include <devices/ethercat/power_board/Device.h>

// Devices armar6
#include <devices/ethercat/sensor_actor_unit/armar6/Device.h>
#include <devices/ethercat/platform/armar6_mecanum/Device.h>
#include <devices/ethercat/torso/armar6_prismatic/Device.h>
#include <devices/ethercat/hand/armar6_v2/Device.h>
#include <devices/ethercat/ft_sensor_board/armar6/Device.h>

#include "RobotMock.h"
#include "RobotNodeMock.h"
#include "TestDeviceList.h"

using namespace armarx::control::hardware_config;
using namespace armarx;
using namespace devices::ethercat;

MultiNodeRapidXMLReader
readHardwareConfigFile(std::string filePath)
{
    ARMARX_TRACE;
    if (!ArmarXDataPath::getAbsolutePath(filePath, filePath))
    {
        throw LocalException("could not find file path: ") << filePath;
    }
    ARMARX_CHECK(std::filesystem::is_regular_file(filePath))
        << "The bus config file `" << filePath << "` does not exist!";

    ARMARX_INFO_S << "Read the hw config from " << filePath;

    //reading the config for the Bus and create all the robot objects in the robot container
    auto busConfigFilePathDir = std::filesystem::path(filePath).parent_path();

    ARMARX_TRACE;
    ARMARX_INFO_S << "Reading file `" << filePath << "`";
    auto rapidXmlReaderPtr = RapidXmlReader::FromFile(filePath);
    ARMARX_CHECK_NOT_NULL(rapidXmlReaderPtr) << "Failed to read `" << filePath << "`";

    ARMARX_TRACE;
    auto rootNode = rapidXmlReaderPtr->getRoot("HardwareConfig");
    ARMARX_CHECK(rootNode.is_valid());

    MultiNodeRapidXMLReader multiNode({rootNode});
    for (RapidXmlReaderNode& includeNode : rootNode.nodes("include"))
    {
        ARMARX_CHECK(includeNode.has_attribute("file"))
            << "File `" << filePath
            << "` contains invalid `include` tag. The `file` attribute is missing.";
        const auto relPath = includeNode.attribute_value("file");

        std::filesystem::path filepath = (busConfigFilePathDir / relPath);
        if (!std::filesystem::exists(filepath))
        {
            std::string absPath;
            if (!ArmarXDataPath::getAbsolutePath(relPath, absPath))
            {
                throw LocalException("Could not find config file at path ") << relPath;
            }
        }

        std::cout << "Reading included file at: " << filepath << std::endl;

        ARMARX_TRACE;
        auto includedNode = RapidXmlReader::FromFile(filepath.string());
        ARMARX_CHECK(includedNode->getRoot("HardwareConfig").is_valid())
            << "Invalid root node for XML file `" << filepath.string() << "`";
        multiNode.addNode(includedNode->getRoot("HardwareConfig"));
    }
    return multiNode;
}

template<typename T>
inline void testDevice(std::shared_ptr<DeviceConfig>& conf, VirtualRobot::RobotPtr const& robot)
{
    std::cout << "Testing device with name '" << conf->getName() << "' and type '" << conf->getType() << "': ";
    try {
        T dev(*conf, robot);

        std::vector<std::string> itemsNotReadErrors;
        conf->checkAllItemsRead(itemsNotReadErrors);

        if (itemsNotReadErrors.size() == 0)
        {
            std::cout << "No issues found" << std::endl;
        } else {
            std::cout << "Errors from items defined only in XML:" << std::endl;
            for (auto& s : itemsNotReadErrors)
            {
                std::cout << s << std::endl;
            }
        }


    }
    catch (const std::exception& ex)
    {
        std::cout << "An exception was thrown: " << ex.what() << std::endl;
    }

    std::cout << std::endl;
}

template<typename T>
inline void testDeviceWithDefaultMock(std::shared_ptr<DeviceConfig>& conf)
{
    std::shared_ptr<RobotMock> robot = std::make_shared<RobotMock>();
    robot->init(std::dynamic_pointer_cast<VirtualRobot::Robot>(robot));
    robot->setRobotNode(conf->getName(), std::make_shared<RobotNodeMock>(robot, conf->getName(), 3, 10));
    testDevice<T>(conf, robot);
}

enum TestDevices
{
    ArmarDE,
    Armar6,
    None
};

int
main(int argc, char* argv[])
{
    bool printStructure = false;
    bool printWarnings = false;
    std::string filePath = "";
    TestDevices testDevices = TestDevices::None;

    for (int i = 1; i < argc; i++)
    {
        std::string t = argv[i];
        if (t[0] == '-')
        {
            if (t == "--print-structure" || t == "-p")
            {
                printStructure = true;
            }
            else if (t == "--print-warnings" || t == "-w")
            {
                printWarnings = true;
            }
            else if (t.substr(0, 15) == "--test-devices=")
            {
                std::string devicesStr = t.substr(15);
                if (devicesStr == "armarde")
                {
                    testDevices = TestDevices::ArmarDE;
                }
                else if (devicesStr == "armar6")
                {
                    testDevices = TestDevices::Armar6;
                }
                else
                {
                    std::cout << "Testing  devices for robot '" << devicesStr << "' not supported" << std::endl;
                    return 1;
                }
            }
            else
            {
                std::cout << "Unknown parameter: " << t << std::endl;
                return 1;
            }
        }
        else
        {
            filePath = t;
        }
    }

    if (filePath == "")
    {
        std::cout << "Input file is missing. Usage: check_hwcfg <HardwareConfigFile>" << std::endl;
        std::cout << "Options:" << std::endl;
        std::cout << "--print-structure         -p      Print the parsed data" << std::endl;
        std::cout << "--test-devices=<robot>    -t=<r>  Test devices by specifying a robot" << std::endl;
    }
    filePath = argv[1];

    armarx::MultiNodeRapidXMLReader documentNode;
    try
    {
        documentNode = readHardwareConfigFile(filePath);
    }
    catch (const rapidxml::parse_error& err)
    {
        std::cout << "Cannot read file." << std::endl;
        std::cout << "Exception information:" << std::endl;
        std::cout << err.what() << std::endl;
        std::cout << std::endl;
        std::cout << "Extracted error position:" << std::endl;

        char* posCStr = err.where<char>();
        size_t len = strlen(posCStr);
        char shortenedStr[1000] = {0};
        strncpy(shortenedStr, posCStr, 999);
        std::cout << shortenedStr << std::endl;

        return 1;
    }
    catch (const LocalException& err)
    {
        std::cout << err.what() << std::endl;
    }


    std::cout << std::endl;
    std::cout << "Results:" << std::endl;

    ConfigParser parser(documentNode);

    bool fileOK = false;
    try
    {
        parser.parse();
        std::cout << "File looks fine." << std::endl;
        fileOK = true;
    }
    catch (const ParserError& error)
    {
        std::cout << "Errors occured during parsing:" << std::endl;
        std::cout << error.what() << std::endl;
    }

    HardwareConfig& config = parser.getHardwareConfig();

    if (printStructure)
    {
        std::cout << std::endl;
        std::cout << "Structure: " << std::endl;
        std::cout << config << std::endl;
    }

    if (printWarnings)
    {
        std::cout << std::endl;
        std::cout << "Warnings:" << std::endl;
        for (auto& s : parser.getWarnings())
        {
            std::cout << s << std::endl;
            std::cout << std::endl;
        }
    }

    if (testDevices != TestDevices::None)
    {
        TestDeviceList devs;

        // Register Devices according to selected robot
        if (testDevices == TestDevices::ArmarDE)
        {
            devs.registerDevice("Joint", testDeviceWithDefaultMock<sensor_actor_unit::armar7de::Device>);
            devs.registerDevice("HeadBoardArmar7de", testDeviceWithDefaultMock<head_board::armar7de::Device>);
            devs.registerDevice("HolonomicPlatform", testDeviceWithDefaultMock<platform::armar6_mecanum::Device>);
            devs.registerDevice("PrismaticJoint", testDeviceWithDefaultMock<torso::armar6_prismatic::Device>);
            devs.registerDevice("PowerBoard", testDeviceWithDefaultMock<power_board::Device>);
        }
        else if (testDevices == TestDevices::Armar6)
        {
            devs.registerDevice("Joint", testDeviceWithDefaultMock<sensor_actor_unit::armar6::Device>);
            devs.registerDevice("HeadBoardArmar7de", testDeviceWithDefaultMock<head_board::armar7de::Device>);
            devs.registerDevice("HolonomicPlatform", testDeviceWithDefaultMock<platform::armar6_mecanum::Device>);
            devs.registerDevice("PrismaticJoint", testDeviceWithDefaultMock<torso::armar6_prismatic::Device>);
            devs.registerDevice("KITHandV2", testDeviceWithDefaultMock<hand::armar6_v2::Device>);

            // Disabled, because not yet testable - produces segfault
            // devs.registerDevice("ForceTorqueSensor", testDeviceWithDefaultMock<ft_sensor_board::armar6::Device>);
        }

        std::cout << std::endl;
        std::cout << "Attributes matching code?" << std::endl;
        if (!fileOK)
        {
            std::cout << "Warning: File contains errors. Results may be inaccurate!" << std::endl;
        }

        std::shared_ptr<RobotMock> robot = std::make_shared<RobotMock>();

        for (auto& pair : config.deviceConfigs)
        {
            auto& conf = pair.second;

            devs.testDevice(conf);
        }
    }

    return 0;
}
