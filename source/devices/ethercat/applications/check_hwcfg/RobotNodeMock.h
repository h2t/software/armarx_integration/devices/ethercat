#pragma once

#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/VirtualRobot.h>

namespace devices::ethercat {
    class RobotNodeMock : public VirtualRobot::RobotNode
    {
    public:
        RobotNodeMock(VirtualRobot::RobotWeakPtr rob,
                  const std::string& name,
                  float jointLimitLo,
                  float jointLimitHi)
            : VirtualRobot::RobotNode(rob, name, jointLimitLo, jointLimitHi)
        {
        }

        VirtualRobot::RobotNodePtr _clone(const VirtualRobot::RobotPtr newRobot, const VirtualRobot::VisualizationNodePtr visualizationModel, const VirtualRobot::CollisionModelPtr collisionModel, VirtualRobot::CollisionCheckerPtr colChecker, float scaling) override
        {
            return {};
        }

        std::string _toXML(const std::string& modelPath) override
        {
            return "";
        }
    };
}
