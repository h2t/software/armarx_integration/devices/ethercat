#pragma once


// armarx
#include <armarx/control/ethercat/SlaveInterface.h>


namespace devices::ethercat::ethercat_hub
{

    class Slave :
        public armarx::control::ethercat::SlaveInterface
    {

    public:

        Slave(const armarx::control::ethercat::SlaveIdentifier slaveIdentifier);

        // AbstractSlave interface
    public:

        void doMappings() override
        {
        }

        bool prepareForRun() override
        {
            return true;
        }

        void execute() override
        {
        }

        bool shutdown() override
        {
            return true;
        }

        void setInputPDO(void* ptr) override
        {
        }

        void setOutputPDO(void* ptr) override
        {
        }

        void prepareForOp() override
        {
        }

        bool hasError() override
        {
            return false;
        }

        // AbstractSlave interface
    public:

        static std::string getDefaultName();

        static bool isSlaveIdentifierAccepted(const armarx::control::ethercat::SlaveIdentifier& sid);

    };

} // namespace armarx
