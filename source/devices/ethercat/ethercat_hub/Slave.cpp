
#include "Slave.h"


// SOEM
#include <ethercat.h>

// armarx
#include <armarx/control/ethercat/Bus.h>
#include <devices/ethercat/common/h2t_devices.h>


namespace devices::ethercat::ethercat_hub
{
    static constexpr std::uint32_t EtherCatHub_ProductCode = 0x10;

    Slave::Slave(const armarx::control::ethercat::SlaveIdentifier slaveIdentifier) :
        SlaveInterface(slaveIdentifier)
    {
        setTag(this->slaveIdentifier.getName ());
    }


    std::string
    Slave::getDefaultName()
    {
        return "EtherCATHub";
    }

    bool
    Slave::isSlaveIdentifierAccepted(const armarx::control::ethercat::SlaveIdentifier& sid)
    {
        if (sid.vendorID == common::H2TVendorId && sid.productCode == EtherCatHub_ProductCode)
        {
            return true;
        }

        return false;
    }

} // namespace devices::ethercat::ethercat_hub
