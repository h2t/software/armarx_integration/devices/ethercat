/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <memory>

#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <RobotAPI/components/units/RobotUnit/Devices/ControlDevice.h>
#include <RobotAPI/components/units/RobotUnit/Devices/SensorDevice.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <RobotAPI/interface/units/KinematicUnitInterface.h>
#include <armarx/control/ethercat/DataInterface.h>
#include <armarx/control/rt_filters/AverageFilter.h>

#include <devices/ethercat/common/elmo/gold/Data.h>
#include <devices/ethercat/common/elmo/gold/Slave.h>
#include <devices/ethercat/common/sensor_board/armar6/Slave.h>

namespace devices::ethercat::torso::armar6_prismatic
{

    class TorsoJointSensorValue :
        virtual public armarx::SensorValue1DoFActuatorPosition,
        virtual public armarx::SensorValue1DoFActuatorVelocity,
        virtual public armarx::SensorValue1DoFActuatorCurrent,
        virtual public armarx::SensorValue1DoFActuatorStatus
    {

    public:

        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION
        static SensorValueInfo<TorsoJointSensorValue> GetClassMemberInfo()
        {
            SensorValueInfo<TorsoJointSensorValue> svi;
            svi.addBaseClass<SensorValue1DoFActuatorPosition>();
            svi.addBaseClass<SensorValue1DoFActuatorVelocity>();
            svi.addBaseClass<SensorValue1DoFActuatorCurrent>();
            svi.addBaseClass<SensorValue1DoFActuatorStatus>();
            return svi;
        }

    };


    using TorsoPrismaticJointDataPtr = std::shared_ptr<class TorsoPrismaticJointData>;

}
