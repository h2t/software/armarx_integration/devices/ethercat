/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Armar6RT::ArmarXObjects::Armar6IceKinematicUnit
 * @author     Markus Swarowsky ( markus dot swarowsky at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <VirtualRobot/Nodes/RobotNode.h>

#include <armarx/control/ethercat/DeviceInterface.h>

#include <devices/ethercat/sensor_actor_unit/armar6/Data.h>
#include <devices/ethercat/sensor_actor_unit/armar6/joint_controller/ElmoEmergency.h>
#include <devices/ethercat/sensor_actor_unit/armar6/joint_controller/StopMovement.h>
#include <devices/ethercat/torso/armar6_prismatic/Data.h>
#include <devices/ethercat/torso/armar6_prismatic/joint_controller/Emergency.h>
#include <devices/ethercat/torso/armar6_prismatic/joint_controller/Position.h>
#include <devices/ethercat/torso/armar6_prismatic/joint_controller/Velocity.h>


namespace devices::ethercat::torso::armar6_prismatic
{

    class Device :
        public armarx::ControlDevice,
        public armarx::SensorDevice,
        public armarx::control::ethercat::DeviceInterface
    {

    public:
        Device(armarx::control::hardware_config::DeviceConfig& hwConfig,
               const VirtualRobot::RobotPtr& robot);

        // DeviceInterface interface
        TryAssignResult tryAssign(armarx::control::ethercat::SlaveInterface& slave) override;
        AllAssignedResult onAllAssigned() override;
        std::string getClassName() const override;
        void postSwitchToSafeOp() override;

        // SensorDevice interface
        const armarx::SensorValueBase* getSensorValue() const override;
        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration) override;

        // ControlDevice interface
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                 const IceUtil::Time& timeSinceLastIteration) override;

        common::elmo::gold::Data* getData() const;

        std::string getTorsoPrismaticJointName() const;

        std::uint32_t getElmoSerial() const;

        std::uint16_t getElmoSlaveNumber() const;

        bool switchControlMode(armarx::ControlMode newMode);

    private:
        void updateSensorValueStruct();

        // The data and target object.
        std::unique_ptr<common::elmo::gold::Data> dataPtr;
        // The data object for copying to non-rt part.
        TorsoJointSensorValue sensorValue;

        VirtualRobot::RobotNodePtr robotNode;

        // Bus devices - serial form config and pointer to actual bus slave.
        const armarx::control::ethercat::SlaveIdentifier elmoIdentifier;

        common::elmo::gold::Slave* elmoPtr;

        common::elmo::gold::ElmoConfig elmoConfig;

        // The joint controller.
        joint_controller::PositionControllerPtr torsoPositionController;
        joint_controller::VelocityControllerPtr torsoVelocityController;
        joint_controller::EmergencyControllerPtr elmoEmergencyController;
        std::shared_ptr<sensor_actor_unit::armar6::joint_controller::StopMovementController<
            common::elmo::gold::Data*>>
            stopMovementController;

        armarx::control::joint_controller::PositionControllerConfigurationPtr
            positionControllerConfigDataPtr;
        armarx::control::joint_controller::VelocityControllerConfigurationPtr
            velocityControllerConfigDataPtr;
    };
} // namespace devices::ethercat::torso::armar6_prismatic
