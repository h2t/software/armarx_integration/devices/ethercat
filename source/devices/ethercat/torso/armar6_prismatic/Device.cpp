/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Armar6RT::ArmarXObjects::Armar6IceKinematicUnit
 * @author     Markus Swarowsky ( markus dot swarowsky at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Device.h"

#include <VirtualRobot/Robot.h>

#include <ArmarXCore/core/logging/Logging.h>


namespace devices::ethercat::torso::armar6_prismatic
{

    Device::Device(armarx::control::hardware_config::DeviceConfig& hwConfig,
                   VirtualRobot::RobotPtr const& robot) :
        DeviceBase(hwConfig.getName()),
        ControlDevice(hwConfig.getName()),
        SensorDevice(hwConfig.getName()),
        DeviceInterface(hwConfig.getName()),
        dataPtr(nullptr),
        robotNode(robot->getRobotNode(hwConfig.getName())),
        elmoIdentifier(hwConfig.getSlaveConfig("Elmo").getIdentifier()),
        elmoPtr(nullptr),
        elmoConfig(hwConfig.getSlaveConfig("Elmo")),
        positionControllerConfigDataPtr(
            armarx::control::joint_controller::PositionControllerConfiguration::
                CreatePositionControllerConfigData(hwConfig.getControllerConfig("Position"))),
        velocityControllerConfigDataPtr(
            armarx::control::joint_controller::VelocityControllerConfiguration::
                CreateVelocityControllerConfigData(hwConfig.getControllerConfig("Velocity")))
    {
    }

    armarx::control::ethercat::DeviceInterface::TryAssignResult
    Device::tryAssign(armarx::control::ethercat::SlaveInterface& slave)
    {
        namespace ecat = armarx::control::ethercat;
        using Result = DeviceInterface::TryAssignResult;

        Result result = ecat::tryAssignUsingSerialNo(slave, elmoPtr, elmoIdentifier.serialNumber);
        if (result != Result::unknown)
        {
            return result;
        }

        return result;
    }

    armarx::control::ethercat::DeviceInterface::AllAssignedResult
    Device::onAllAssigned()
    {
        if (elmoPtr)
        {
            // Set the config parameters for the slaves.
            elmoPtr->setConfiguration(elmoConfig);

            return AllAssignedResult::ok;
        }

        return AllAssignedResult::slavesMissing;
    }

    std::string
    Device::getClassName() const
    {
        return "PrismaticJoint_Armar6";
    }


    void
    Device::postSwitchToSafeOp()
    {
        if (!dataPtr)
        {
            dataPtr = std::make_unique<common::elmo::gold::Data>(elmoConfig,
                                                                 elmoPtr->getOutputsPtr(),
                                                                 elmoPtr->getInputsPtr());

            //with data set we can create the rest of the joint controller...
            torsoPositionController = std::make_shared<joint_controller::PositionController>(
                this, dataPtr.get(), positionControllerConfigDataPtr);
            torsoVelocityController = std::make_shared<joint_controller::VelocityController>(
                this, dataPtr.get(), velocityControllerConfigDataPtr);
            stopMovementController.reset(
                new sensor_actor_unit::armar6::joint_controller::StopMovementController<
                    common::elmo::gold::Data*>(elmoPtr, dataPtr.get(), 200));

            elmoEmergencyController.reset(
                new joint_controller::EmergencyController(elmoPtr, dataPtr.get()));

            addJointController(elmoEmergencyController.get());
            addJointController(stopMovementController.get());
            addJointController(torsoPositionController.get());
            addJointController(torsoVelocityController.get());
        }

        ARMARX_CHECK_NOT_NULL(dataPtr);
    }


    void
    Device::updateSensorValueStruct()
    {
        sensorValue.position = dataPtr->getActualRelativePosition();
        if (sensorValue.position < -1000 || sensorValue.position > 1000)
        {
            DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                   "!!!ATTENTION!!! The torso joint position sensor has an unusual "
                                   "value: %d - probably something is wrong with the sensor!",
                                   sensorValue.position);
        }
        sensorValue.velocity = dataPtr->getActualVelocity();

        //    sensorValue.acceleration = dataPtr->getActualAcceleration();
        //    sensorValue.torque = dataPtr->getActualTorque();
        //    sensorValue.motorTemperature = dataPtr->getActualMotorTemperature();
        //    sensorValue.gearTemperature = dataPtr->getActualGearTemperature();
        sensorValue.motorCurrent = dataPtr->getActualCurrent();
        sensorValue.status = dataPtr->getStatus();
    }


    const armarx::SensorValueBase*
    Device::getSensorValue() const
    {
        return &sensorValue;
    }


    common::elmo::gold::Data*
    Device::getData() const
    {
        if (not dataPtr)
        {
            DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                   "Torso has no data set, switch to SafeOP before");
        }
        return dataPtr.get();
    }


    std::uint32_t
    Device::getElmoSerial() const
    {
        return elmoIdentifier.serialNumber;
    }


    bool
    Device::switchControlMode(armarx::ControlMode newMode)
    {
        common::elmo::gold::ElmoControlMode modeToSet =
            common::elmo::gold::ElmoControlMode::DEFAULT_MODE;

        switch (newMode)
        {
            case armarx::eVelocityControl:
                modeToSet = common::elmo::gold::ElmoControlMode::VELOCITY;
                break;
            case armarx::eTorqueControl:
                modeToSet = common::elmo::gold::ElmoControlMode::CURRENT;
                break;
            case armarx::ePositionVelocityControl:
                modeToSet = common::elmo::gold::ElmoControlMode::DEFAULT_MODE;
                break;
            case armarx::ePositionControl:
                modeToSet = common::elmo::gold::ElmoControlMode::POSITION;
                break;
            case armarx::eDisabled:
                modeToSet = common::elmo::gold::ElmoControlMode::DEFAULT_MODE;
                break;
            case armarx::eUnknown:
                DEVICE_ERROR(rtGetDeviceName(), "unknown control mode set for this joint!");
                break;
        }

        return elmoPtr->switchMode(modeToSet);
    }


    void
    Device::rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                               const IceUtil::Time& timeSinceLastIteration)
    {
        if (dataPtr == nullptr)
        {
            DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                   "Torso has no data set, switch to SafeOP before");
        }
        dataPtr->rtReadSensorValues(sensorValuesTimestamp, timeSinceLastIteration);
        updateSensorValueStruct();
    }


    void
    Device::rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration)
    {
        if (dataPtr == nullptr)
        {
            DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                   "Torso has no data set, switch to SafeOP before");
        }
        dataPtr->rtWriteTargetValues(sensorValuesTimestamp, timeSinceLastIteration);
    }


    std::uint16_t
    Device::getElmoSlaveNumber() const
    {
        if (elmoPtr == nullptr)
        {
            DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                   "Torso isn't initialized, assign slaves before");
        }
        return elmoPtr->getSlaveNumber();
    }

} // namespace devices::ethercat::torso::armar6_prismatic
