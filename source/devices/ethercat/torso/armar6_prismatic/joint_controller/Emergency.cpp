#include "Emergency.h"


namespace devices::ethercat::torso::armar6_prismatic::joint_controller
{

    EmergencyController::EmergencyController(common::elmo::gold::Slave* elmo,
                                             common::elmo::gold::Data* data) :
        setStart(true)
    {
        this->dataPtr = data;
        this->elmo = elmo;
    }


    armarx::ControlTargetBase*
    EmergencyController::getControlTarget()
    {
        return &target;
    }


    void
    EmergencyController::rtRun(const IceUtil::Time& sensorValuesTimestamp,
                               const IceUtil::Time& timeSinceLastIteration)
    {
        dataPtr->setTargetVelocity(0.0f);
        //TODO activate Motor break.
        if (setStart)
        {
            start = std::chrono::high_resolution_clock::now();
            setStart = false;
        }

        //    armarx::DS402State  currentState = elmo->getState();
        //    ARMARX_INFO << deactivateSpam(1) << "Current State: " << DS402StateToString(currentState);

        /*
        auto now = std::chrono::high_resolution_clock::now();
        if (std::chrono::duration_cast<std::chrono::milliseconds>((now - start)).count() < 5000)
        {
        }
        else
        {
            armarx::DS402State  currentState = elmo->getState();
            if (currentState != armarx::DS402State::QUICK_STOP_ACTIVE
                && currentState != armarx::DS402State::SWITCH_ON_DISABLED)
            {
                elmo->activateQuickStop();
                elmo->switchMode(armarx::ElmoControlMode::DEFAULT_MODE);
            }

        }*/
    }


    const std::string&
    EmergencyController::getControlMode() const
    {
        return armarx::ControlModes::EmergencyStop;
    }


    void
    EmergencyController::rtPreActivateController()
    {
        ARMARX_IMPORTANT << "Activating Emergency Stop controller for "
                         << elmo->getSlaveIdentifier().getName();
        elmo->switchMode(common::elmo::gold::ElmoControlMode::VELOCITY);
        dataPtr->setTargetDeceleration(300);
        //    elmo->activateQuickStop();
    }


    void
    EmergencyController::rtPostDeactivateController()
    {
        //    elmo->decativateQuickStop();
    }

} // namespace devices::ethercat::torso::armar6_prismatic::joint_controller
