#pragma once


// STD/STL
#include <memory>

// armarx
#include <armarx/control/joint_controller/ControllerConfiguration.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>

// robot_devices
#include <devices/ethercat/common/elmo/gold/Data.h>
#include <devices/ethercat/sensor_actor_unit/armar6/joint_controller/Position.h>


namespace devices::ethercat::torso::armar6_prismatic
{
    class Device;
}


namespace devices::ethercat::torso::armar6_prismatic::joint_controller
{

    using PositionControllerPtr = std::shared_ptr<class PositionController>;

    class PositionController : public armarx::JointController
    {

    public:

        PositionController(Device* jointPtr, common::elmo::gold::Data* torsoDataPtr, armarx::control::joint_controller::PositionControllerConfigurationPtr config);

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        armarx::ControlTargetBase* getControlTarget() override;
        void rtPreActivateController() override;

    private:

        Device* jointPtr;
        armarx::ControlTarget1DoFActuatorPosition target;
        common::elmo::gold::Data*  torsoData;

        armarx::PositionThroughVelocityControllerWithAccelerationAndPositionBounds positionController;
        float maxVelocityRad;
        float maxAccelerationRad;
        float maxDecelerationRad;
        float p;
        float currentVelocity;

    };

}  // namespace devices::ethercat::torso::armar6_prismatic::joint_controller
