#pragma once


// STD/STL
#include <memory>
#include <chrono>

// armarx
#include <armarx/control/joint_controller/ControllerConfiguration.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
//#include <RobotAPI/components/units/RobotUnit/Targets/ActuatorVelocityTarget.h>

// robot_devices
#include <devices/ethercat/common/elmo/gold/Slave.h>
#include <devices/ethercat/common/elmo/gold/Data.h>


namespace devices::ethercat::torso::armar6_prismatic
{
    class Device;
}


namespace devices::ethercat::torso::armar6_prismatic::joint_controller
{

    using VelocityControllerPtr = std::shared_ptr<class VelocityController>;


    class VelocityController : public armarx::JointController
    {

    public:

        VelocityController(Device* jointPtr, common::elmo::gold::Data* jointData, armarx::control::joint_controller::VelocityControllerConfigurationPtr config);

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        armarx::ControlTargetBase* getControlTarget() override;

        void rtPreActivateController() override;

    private:

        armarx::ControlTarget1DoFActuatorVelocity target;
        common::elmo::gold::Data* dataPtr;
        Device* jointPtr;
        //DEBUG
        bool setStart;
        std::chrono::high_resolution_clock::time_point start;
        float positionLimitHi = 0;
        float positionLimitLo = 0;
        armarx::control::joint_controller::VelocityControllerConfiguration config;

    };

}  // namespace devices::ethercat::torso::armar6_prismatic::joint_controller
