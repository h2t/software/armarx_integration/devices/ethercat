#include "Velocity.h"

// STD/STL
#include <chrono>

// Boost
#include <boost/algorithm/clamp.hpp>

// armarx
#include <ArmarXCore/core/logging/Logging.h>

// robot_devices
#include <devices/ethercat/torso/armar6_prismatic/Device.h>


namespace devices::ethercat::torso::armar6_prismatic::joint_controller
{

    VelocityController::VelocityController(Device* jointPtr, common::elmo::gold::Data* jointData,
                                           armarx::control::joint_controller::VelocityControllerConfigurationPtr config) : JointController(),
        target(), setStart(true),
        config(*config)

    {
        this->jointPtr = jointPtr;
        dataPtr = jointData;
        positionLimitHi = dataPtr->getSoftLimitHi();
        positionLimitLo = dataPtr->getSoftLimitLo();
    }

    void VelocityController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        if (target.isValid())
        {
            dataPtr->setTargetDeceleration(config.maxDecelerationRad);
            //ARMARX_IMPORTANT << "vel target: " <<  target.velocity;
            //send the velocity to the joint
            float newVel = target.velocity;

            newVel = boost::algorithm::clamp(newVel, -config.maxVelocityRad, config.maxVelocityRad);

            if ((dataPtr->getActualRelativePosition() > positionLimitHi && target.velocity > 0)
                || (dataPtr->getActualRelativePosition() < positionLimitLo && target.velocity < 0))
            {
                newVel = 0;
                //                ARMARX_INFO /*<< deactivateSpam(1)*/ << "Breaking now at " << dataPtr->getActualPosition() << " current: " << dataPtr->getActualCurrent();
                dataPtr->setTargetDeceleration(1000);
            }

            dataPtr->setTargetVelocity(newVel);
        }
        else
        {
            ARMARX_ERROR << "invalid target set for elmo";
        }
    }


    armarx::ControlTargetBase*
    VelocityController::getControlTarget()
    {
        return &target;
    }


    void VelocityController::rtPreActivateController()
    {
        jointPtr->switchControlMode(armarx::eVelocityControl);
        dataPtr->setTargetAcceleration(config.maxAccelerationRad);
        dataPtr->setTargetDeceleration(config.maxDecelerationRad);
        ARMARX_INFO << VAROUT(config.maxVelocityRad) << VAROUT(config.maxAccelerationRad) << VAROUT(config.maxDecelerationRad);
    }

}
