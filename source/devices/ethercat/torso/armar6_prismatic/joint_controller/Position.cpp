#include "Position.h"


// Boost
#include <boost/algorithm/clamp.hpp>

// armarx
#include <ArmarXCore/core/logging/Logging.h>

// robot_devices
#include <devices/ethercat/torso/armar6_prismatic/Device.h>


namespace devices::ethercat::torso::armar6_prismatic::joint_controller
{

    PositionController::PositionController(Device* jointPtr, common::elmo::gold::Data* torsoDataPtr, armarx::control::joint_controller::PositionControllerConfigurationPtr config) : JointController(),
        target()
    {
        this->jointPtr = jointPtr;
        this->torsoData = torsoDataPtr;

        maxVelocityRad = config->maxVelocityRad;
        maxAccelerationRad = config->maxAccelerationRad;
        maxDecelerationRad = config->maxDecelerationRad;
        p = config->p;

        positionController.positionLimitLo = torsoData->getSoftLimitLo();
        positionController.positionLimitHi = torsoData->getSoftLimitHi();
        positionController.maxDt = 0.003;
        positionController.maxV = maxVelocityRad;
        positionController.pControlPosErrorLimit = 1.0f;
        positionController.pControlVelLimit = 0.1f;
        positionController.pid->Kp = p;
        positionController.acceleration = maxAccelerationRad;
        positionController.deceleration = maxDecelerationRad;
        positionController.accuracy = config->accuracy;
    }


    void PositionController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        if (target.isValid())
        {
            //TODO implement position logic
            //        dataPtr->setTargetPosition(target.position);
            //ARMARX_IMPORTANT << deactivateSpam(2) << target.position;

            float newVelocity = 0;
            float currentPosition = torsoData->getActualRelativePosition();
            float targetPosition = boost::algorithm::clamp(target.position,
                                   std::min(currentPosition, positionController.positionLimitLo), // lo or current position
                                   std::max(currentPosition, positionController.positionLimitHi)); // hi or current position
            positionController.currentV = currentVelocity;
            positionController.dt = timeSinceLastIteration.toSecondsDouble();
            positionController.currentPosition = currentPosition;
            positionController.targetPosition = targetPosition;
            newVelocity = positionController.run();
            newVelocity = armarx::math::MathUtils::LimitTo(newVelocity, maxVelocityRad);

            currentVelocity = newVelocity;
            //        target.targetVelocity = newVelocity;
            //        ARMARX_IMPORTANT << deactivateSpam(1) << "pos tar:" << target.position << VAROUT(newVelocity);

            torsoData->setTargetVelocity(newVelocity);
        }
        else
        {
            ARMARX_ERROR << "invalid target set for a joint";
        }
    }


    armarx::ControlTargetBase* PositionController::getControlTarget()
    {
        return &target;
    }


    void PositionController::rtPreActivateController()
    {
        currentVelocity = torsoData->getActualVelocity();
        jointPtr->switchControlMode(armarx::eVelocityControl);
        torsoData->setTargetAcceleration(maxAccelerationRad);
        torsoData->setTargetDeceleration(maxDecelerationRad);
        ARMARX_INFO << "Setting acc to " << maxAccelerationRad << " and dec to " << maxDecelerationRad;
    }

}
