#pragma once


// STD/STL
#include <memory>

// armarx
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>

// robot_devices
#include <devices/ethercat/common/elmo/gold/Slave.h>
#include <devices/ethercat/common/elmo/gold/Data.h>


namespace devices::ethercat::torso::armar6_prismatic::joint_controller
{

    using EmergencyControllerPtr = std::shared_ptr<class EmergencyController>;


    class EmergencyController : public armarx::JointController
    {

    public:

        EmergencyController(common::elmo::gold::Slave* elmo, common::elmo::gold::Data* data);

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        /**
         * Returns the Target for this controller, but as this is the Emergency controller it will ignored.
         * As this controller will just break
         * @return is type VelocityTarget but it will return a nullptr, because it won't be possible to set a target
         */
        armarx::ControlTargetBase* getControlTarget() override;

        const std::string& getControlMode() const override;

    private:

        common::elmo::gold::Slave* elmo;
        common::elmo::gold::Data* dataPtr;
        armarx::DummyControlTargetEmergencyStop target;
        //DEBUG
        bool setStart;
        std::chrono::high_resolution_clock::time_point start;

        // JointController interface

    protected:

        void rtPreActivateController() override;
        void rtPostDeactivateController() override;

    };

}
