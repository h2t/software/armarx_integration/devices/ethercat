/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Armar6RT::ArmarXObjects::Armar6IceKinematicUnit
 * @author     Markus Swarowsky ( markus dot swarowsky at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Device.h"

#include <VirtualRobot/Robot.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/util/StringHelperTemplates.h>

#include "joint_controller/EmergencyStop.h"
#include "joint_controller/LedColor.h"
#include "joint_controller/LedEmergencyStop.h"
#include "joint_controller/LedStopMovement.h"
#include "joint_controller/PWM.h"
#include "joint_controller/PWMPosition.h"
#include "joint_controller/PWMVelocity.h"
#include "joint_controller/StopMovement.h"
#include "joint_controller/ZeroTorque.h"
#include <devices/ethercat/head_board/armar7de/Slave.h>


namespace devices::ethercat::head_board::armar7de
{

    Device::Device(hwconfig::DeviceConfig& hwConfig,
                   const VirtualRobot::RobotPtr& robot) :
        DeviceBase(hwConfig.getName()),
        ControlDevice(hwConfig.getName()),
        SensorDevice(hwConfig.getName()),
        DeviceInterface(hwConfig.getName()),
        robotNode(robot->getRobotNode(hwConfig.getName())),
        dataPtr(nullptr),
        slaveIdentifier(hwConfig.getOnlySlaveConfig().getIdentifier()),
        slavePtr(nullptr),
        ledControlDataPtr(nullptr),
        ledDevice(nullptr),
        config(hwConfig),
        dataConfig(hwConfig)
    {

        ARMARX_CHECK_NOT_NULL(robotNode);

        positionControllerCfg = joint_controller::PWMPositionControllerConfiguration::
            CreatePWMPositionControllerConfigData(hwConfig.getControllerConfig("PWMPosition"));
        // TODO: Why not the controller in joint_controllers? Why is there another one?
        velocityControllerCfg =
            PWMVelocityControllerConfiguration::CreatePWMVelocityControllerConfigData(hwConfig.getControllerConfig("PWMVelocity"));
        zeroTorqueControllerCfg =
            joint_controller::PWMZeroTorqueControllerConfiguration::CreateConfigData(hwConfig.getControllerConfig("PWMZeroTorque"));
    }


    armarx::control::ethercat::DeviceInterface::TryAssignResult
    Device::tryAssign(armarx::control::ethercat::SlaveInterface& slave)
    {
        namespace ecat = armarx::control::ethercat;
        using Result = DeviceInterface::TryAssignResult;

        Result result = ecat::tryAssignUsingSerialNo(slave, slavePtr, slaveIdentifier.serialNumber);

        return result;
    }


    armarx::control::ethercat::DeviceInterface::AllAssignedResult
    Device::onAllAssigned()
    {
        using Result = DeviceInterface::AllAssignedResult;

        if (slavePtr)
        {
            // Initialize head board.
            {
                slavePtr->setConfig(config);
            }

            ledDevice.reset(new LedDevice(getDeviceName() + "Led"));
            subDevices.push_back(ledDevice);

            return Result::ok;
        }

        return Result::slavesMissing;
    }

    std::string
    Device::getClassName() const
    {
        return "HeadBoardArmar7de";
    }

    void
    Device::postSwitchToSafeOp()
    {
        if (!dataPtr)
        {
            dataPtr = std::make_shared<Data>(
                dataConfig, slavePtr->getOutputsPtr(), slavePtr->getInputsPtr());
            //with data set we can create the joint controllers...

            emergencyController.reset(new joint_controller::EmergencyStopController(dataPtr));
            stopMovementController.reset(new joint_controller::StopMovementController(dataPtr));
            zeroTorqueController.reset(new joint_controller::PWMZeroTorqueController(
                getDeviceName(), this, dataPtr, zeroTorqueControllerCfg));
            velocityController.reset(new joint_controller::PWMVelocityController(
                getDeviceName(), this, dataPtr, velocityControllerCfg));
            positionController.reset(new joint_controller::PWMPositionController(
                getDeviceName(), this, dataPtr, positionControllerCfg));
            pwmController.reset(
                new joint_controller::PWMController(getDeviceName(), this, dataPtr));

            addJointController(emergencyController.get());
            addJointController(stopMovementController.get());
            addJointController(zeroTorqueController.get());
            addJointController(velocityController.get());
            addJointController(positionController.get());
            addJointController(pwmController.get());

            ledControlDataPtr =
                std::make_shared<LedControlData>(dataPtr->getLedRgbStripControlPtr());
            ledDevice->init(this, ledControlDataPtr);
        }
        ARMARX_CHECK_NOT_NULL(dataPtr);
        ARMARX_CHECK_NOT_NULL(ledControlDataPtr);
    }

    void
    Device::postSwitchToOp()
    {
    }


    void
    Device::updateSensorValueStruct()
    {
        sensorValue.position = dataPtr->getAbsoluteEncoderAngleInRad();
        sensorValue.velocity = static_cast<float>(dataPtr->getVelocity());
        sensorValue.acceleration = static_cast<float>(dataPtr->getAcceleration());
        sensorValue.filteredAcceleration = dataPtr->getFilteredAcceleration();
        sensorValue.torque = 0;
        sensorValue.inverseDynamicsTorque = 0;
        sensorValue.motorCurrent = 0;
        sensorValue.motorTemperature = dataPtr->getExternalTemperature();
        sensorValue.motorPWM = dataPtr->getMotorPwmValue();

        sensorValue.absoluteEncoderDetailedStatus = dataPtr->getAbsoluteEncoderDetailedStatus();
        sensorValue.absoluteEncoderTemperature = dataPtr->getAbsoluteEncoderTemperature();
        sensorValue.absoluteEncoderTicks = dataPtr->getAbsoluteEncoderTicks();
        sensorValue.absoluteEncoderCrc = dataPtr->getAbsoluteEncoderCrc();

        sensorValue.relativePosition = dataPtr->getRelativeEncoderPositionInRad();
        sensorValue.relativeTicks = dataPtr->getRelativeEncoderTicks();

        sensorValue.onBoardTemparature = dataPtr->getOnBoardTemperature();

        sensorValue.mainUpdateRate = dataPtr->getMainUpdateRate();

        // Status Bits
        sensorValue.emergencyStopStatus = dataPtr->getEmergencyStopStatus();
        sensorValue.aksim1ConnectedAtStartup = dataPtr->getAksIm1ConnectedAtStartup();
        sensorValue.aksim2ConnectedAtStartup = dataPtr->getAksIm2ConnectedAtStartup();
        sensorValue.activeAbsoluteEncoder = dataPtr->getActiveAbsoluteEncoder();
        sensorValue.onBoardTemperatureError = dataPtr->getOnBoardTemperatureError();
        sensorValue.externalTemperatureError = dataPtr->getExternalTemperatureError();
        sensorValue.onBoardTemperatureLimitReached = dataPtr->getOnBoardTemperatureLimitReached();
        sensorValue.externalTemperatureLimitReached = dataPtr->getExternalTemperatureLimitReached();
    }


    std::shared_ptr<Data>
    Device::getData() const
    {
        if (dataPtr == nullptr)
        {
            DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                   "Joint has no data set, switch to SafeOP before");
        }

        return dataPtr;
    }


    const std::string&
    Device::getJointName() const
    {
        return getDeviceName();
    }


    armarx::control::ethercat::SlaveIdentifier
    Device::getSlaveIdentifier() const
    {
        return slaveIdentifier;
    }


    const Device::LedDevicePtr&
    Device::getLedDevice() const
    {
        return ledDevice;
    }


    void
    Device::rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                               const IceUtil::Time& timeSinceLastIteration)
    {
        if (dataPtr == nullptr)
        {
            DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                   "Joint has no data set, switch to SafeOP before");
        }
        dataPtr->rtReadSensorValues(sensorValuesTimestamp, timeSinceLastIteration);

        if (dataPtr->getAbsoluteEncoderTemperature() > 70)
        {
            DEVICE_WARNING(rtGetDeviceName(),
                           "Absolute Encoder Temperature is high! %f°C",
                           dataPtr->getAbsoluteEncoderTemperature())
                .deactivateSpam(10);
        }
        if (dataPtr->getExternalTemperatureLimitReached())
        {
            DEVICE_WARNING(rtGetDeviceName(),
                           "External Temperature is high! %f°C",
                           dataPtr->getAbsoluteEncoderTemperature())
                .deactivateSpam(10);
        }
        if (dataPtr->getOnBoardTemperatureLimitReached())
        {
            DEVICE_WARNING(rtGetDeviceName(),
                           "On-Board Temperature is high! %f°C",
                           dataPtr->getAbsoluteEncoderTemperature())
                .deactivateSpam(10);
        }

        updateSensorValueStruct();
    }


    void
    Device::rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration)
    {
        if (dataPtr == nullptr)
        {
            DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                   "Joint has no data set, call switch to SafeOP before");
        }
        dataPtr->rtWriteTargetValues(sensorValuesTimestamp, timeSinceLastIteration);

        ledControlDataPtr->rtWriteTargetValues(sensorValuesTimestamp, timeSinceLastIteration);
    }


    const armarx::SensorValueBase*
    Device::getSensorValue() const
    {
        return &sensorValue;
    }


    std::uint16_t
    Device::getSlaveNumber() const
    {
        return slavePtr->getSlaveNumber();
    }


    float
    Device::getSoftLimitHi() const
    {
        return robotNode->getJointLimitHigh();
    }

    float
    Device::getSoftLimitLo() const
    {
        return robotNode->getJointLimitLo();
    }

    bool
    Device::isLimitless() const
    {
        return robotNode->isLimitless();
    }


    Device::LedDevice::LedDevice(const std::string& deviceName) :
        DeviceBase(deviceName),
        ControlDevice(deviceName),
        DeviceInterface::SubDeviceInterface(deviceName)
    {
    }

    void
    Device::LedDevice::init(Device* device, LedControlDataPtr ledControlDataPtr)
    {
        this->device = device;
        this->ledControlDataPtr = ledControlDataPtr;

        emergencyController.reset(new joint_controller::LedEmergencyStopController());
        stopMovementController.reset(new joint_controller::LedStopMovementController());
        colorController.reset(new joint_controller::LedColorController(ledControlDataPtr));

        addJointController(emergencyController.get());
        addJointController(stopMovementController.get());
        addJointController(colorController.get());
    }

    void
    Device::LedDevice::rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                           const IceUtil::Time& timeSinceLastIteration)
    {
        if (ledControlDataPtr == nullptr)
        {
            DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                   "LED has no data set, call switch to SafeOP before");
        }

        ledControlDataPtr->rtWriteTargetValues(sensorValuesTimestamp, timeSinceLastIteration);
    }

} // namespace devices::ethercat::head_board::armar7de
