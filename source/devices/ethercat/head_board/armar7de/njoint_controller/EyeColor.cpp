#include <devices/ethercat/head_board/armar7de/njoint_controller/EyeColor.h>


namespace devices::ethercat::head_board::armar7de
{

    armarx::NJointControllerRegistration<EyeColorController> registrationControllerNJointKITHandV2ShapeController("NJointHeadBoardArmar7DEEyeColorController");


    EyeColorController::EyeColorController(armarx::RobotUnit* prov,
                                     EyeColorControllerConfigPtr config,
                                     const VirtualRobot::RobotPtr& r)
    {
        ARMARX_CHECK_EXPRESSION(prov);
        auto deviceName = config->deviceName + "Led";
        ARMARX_INFO << VAROUT(deviceName);
        armarx::ControlTargetBase* ct = useControlTarget(deviceName, ctrl_modes::HeadLedColor);
        ARMARX_CHECK_EXPRESSION(ct);

        ARMARX_CHECK_EXPRESSION(ct->asA<ControlTargetHeadLedColor>());
        controlTarget = ct->asA<ControlTargetHeadLedColor>();
    }


    void EyeColorController::rtPreActivateController()
    {
    }


    void EyeColorController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {

        controlTarget->r.fill(r);
        controlTarget->g.fill(g);
        controlTarget->b.fill(b);
    }

     armarx::WidgetDescription::WidgetPtr EyeColorController::GenerateConfigDescription(
        const VirtualRobot::RobotPtr&,
        const std::map<std::string, armarx::ConstControlDevicePtr>&,
        const std::map<std::string, armarx::ConstSensorDevicePtr>&)
    {
         using namespace armarx::WidgetDescription;
         HBoxLayoutPtr layout = new HBoxLayout;

         LabelPtr label = new Label;
         label->text = "control device name";
         layout->children.emplace_back(label);

         LineEditPtr edit = new LineEdit;
         edit->name = "deviceName";
         edit->defaultValue = "Neck_1_Yaw";
         layout->children.emplace_back(edit);
         return layout;
    }

    EyeColorControllerConfigPtr EyeColorController::GenerateConfigFromVariants(const armarx::StringVariantBaseMap& values)
    {
//        ARMARX_INFO_S << VAROUT(values);
        return new EyeColorControllerConfig {values.at("deviceName")->getString()};
//        return new EyeColorControllerConfig {"Neck_1_Yaw"};
    }


    armarx::WidgetDescription::StringWidgetDictionary EyeColorController::getFunctionDescriptions(const Ice::Current&) const
    {
        using namespace armarx::WidgetDescription;
        HBoxLayoutPtr hbox = new HBoxLayout;
        {
            hbox->children.emplace_back(new Label(false, "r"));
            IntSliderPtr slider = new IntSlider;
            slider->name = "r";
            slider->min = 0;
            slider->defaultValue = r;
            slider->max = 255;
            hbox->children.emplace_back(slider);
        }
        {
            hbox->children.emplace_back(new Label(false, "g"));
            IntSliderPtr slider = new IntSlider;
            slider->name = "g";
            slider->min = 0;
            slider->defaultValue = g;
            slider->max = 255;
            hbox->children.emplace_back(slider);
        }
        {
            hbox->children.emplace_back(new Label(false, "b"));
            IntSliderPtr slider = new IntSlider;
            slider->name = "b";
            slider->min = 0;
            slider->defaultValue = b;
            slider->max = 255;
            hbox->children.emplace_back(slider);
        }

        return {{"Targets", hbox}};
    }


    void EyeColorController::callDescribedFunction(const std::string& name, const armarx::StringVariantBaseMap& valueMap, const Ice::Current&)
    {
        if (name == "Targets")
        {
            std::uint8_t rValue = valueMap.at("r")->getInt();
            std::uint8_t gValue = valueMap.at("g")->getInt();
            std::uint8_t bValue = valueMap.at("b")->getInt();
            setTargets(rValue, gValue, bValue);
        }
        else
        {
            ARMARX_WARNING << "Unknown function name called: " << name;
        }
    }


    void EyeColorController::setTargets(int r, int g, int b, const Ice::Current&)
    {
        this->r = r;
        this->g = g;
        this->b = b;
    }

} // namespace armarx
