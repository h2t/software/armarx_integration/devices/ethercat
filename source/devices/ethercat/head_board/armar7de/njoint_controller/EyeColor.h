#pragma once


// armarx
#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>

// robot_devices
#include <devices/ethercat/head_board/armar7de/ControlTargets.h>
#include <devices/ethercat/head_board/armar7de/njoint_controller/EyeColor.h>
#include <devices/ethercat/head_board/armar7de/njoint_controller/EyeColorInterface.h>


namespace devices::ethercat::head_board::armar7de
{

    using EyeColorControllerConfigPtr = IceUtil::Handle<class EyeColorControllerConfig>;


    class EyeColorControllerConfig :
        virtual public armarx::NJointControllerConfig
    {

    public:

        EyeColorControllerConfig(std::string const& deviceName):
            deviceName(deviceName)
        {}

        const std::string deviceName;

    };


    using EyeColorControllerPtr = IceInternal::Handle<class EyeColorController>;


    class EyeColorController :
        public armarx::NJointController,
        public EyeColorControllerInterface

    {

    public:

        using ConfigPtrT = EyeColorControllerConfigPtr;
        EyeColorController(armarx::RobotUnit* prov, EyeColorControllerConfigPtr config, const VirtualRobot::RobotPtr& r);

        // NJointControllerInterface interface

    public:

        std::string getClassName(const Ice::Current&) const override
        {
            return "NJointHeadBoardArmar7DEEyeColorController";
        }

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

        static armarx::WidgetDescription::WidgetPtr GenerateConfigDescription(
            const VirtualRobot::RobotPtr&,
            const std::map<std::string, armarx::ConstControlDevicePtr>&,
            const std::map<std::string, armarx::ConstSensorDevicePtr>&);

        static EyeColorControllerConfigPtr GenerateConfigFromVariants(const armarx::StringVariantBaseMap& values);

    private:

        ControlTargetHeadLedColor* controlTarget;

        std::uint8_t r = 0;
        std::uint8_t g = 0;
        std::uint8_t b = 0;

    public:

        armarx::WidgetDescription::StringWidgetDictionary getFunctionDescriptions(const Ice::Current&) const override;
        void callDescribedFunction(const std::string& name, const armarx::StringVariantBaseMap& valueMap, const Ice::Current&) override;

        // NJointHeadBoardArmar7DEEyeColorControllerInterface interface

    public:

        void setTargets(int r, int g, int b, const Ice::Current& = Ice::emptyCurrent) override;

        // NJointControllerBase interface

    protected:

        void rtPreActivateController() override;

    };
}
