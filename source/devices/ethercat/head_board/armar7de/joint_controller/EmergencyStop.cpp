#include "EmergencyStop.h"


// armarx
#include <RobotAPI/libraries/core/PIDController.h>

// robot_devices
#include <devices/ethercat/head_board/armar7de/Data.h>


namespace devices::ethercat::head_board::armar7de::joint_controller
{

    EmergencyStopController::EmergencyStopController(DataPtr dataPtr) : dataPtr(dataPtr)
    {
        pid.reset(new armarx::PIDController(0, 5000, 0));
        pid->maxIntegral = 0.1;
    }


    void
    EmergencyStopController::rtRun(const IceUtil::Time& /*sensorValuesTimestamp*/,
                                   const IceUtil::Time& timeSinceLastIteration)
    {
        pid->update(timeSinceLastIteration.toSecondsDouble(),
                    static_cast<double>(dataPtr->getVelocity()),
                    0.0);
        double targetPwm = pid->getControlValue();
        if (std::isnan(targetPwm))
        {
            targetPwm = 0.0;
        }

//        ARMARX_INFO << "EmergencyStop";
//        ARMARX_INFO << VAROUT(dataPtr->getVelocity());
//        ARMARX_INFO << VAROUT(targetPwm);
        targetPwm = 0.0f;

        dataPtr->setMotorPwmValue(static_cast<std::int32_t>(targetPwm));
        //        lastPWM *= 0.9997;
        //        dataPtr->setTargetPWM(lastPWM);
    }


    armarx::ControlTargetBase*
    EmergencyStopController::getControlTarget()
    {
        return &target;
    }


    void
    EmergencyStopController::rtPreActivateController()
    {
        pid->reset();
        //        ARMARX_INFO << "Stopping gripper!";
        lastPWM = static_cast<float>(dataPtr->getMotorPwmValue());
    }

} // namespace devices::ethercat::head_board::armar7de::joint_controller
