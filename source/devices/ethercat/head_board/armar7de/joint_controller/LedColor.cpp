#include "LedColor.h"

namespace devices::ethercat::head_board::armar7de::joint_controller
{
    LedColorController::LedColorController(
        LedControlDataPtr data) :
        data(data)
    {
        ARMARX_CHECK_NOT_NULL(data);
    }

    armarx::ControlTargetBase* LedColorController::getControlTarget()
    {
        return &target;
    }

    void LedColorController::rtRun(
        const IceUtil::Time&, const IceUtil::Time&)
    {
        std::array<std::uint16_t, 20> ledRgbStripControl;
        for (size_t i = 0; i < ledRgbStripControl.size(); ++i)
        {
            ledRgbStripControl[i] = ((target.r[i] & 0b11111000) << 8) | ((target.g[i] & 0b11111100) << 3) | (target.b[i] >> 3);
        }
        data->setLedRgbStripControl(ledRgbStripControl);
    }
}
