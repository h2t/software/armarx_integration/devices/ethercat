#pragma once


// armarx
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTargetBase.h>
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>

// robot_devices
#include <devices/ethercat/head_board/armar7de/ControlTargets.h>


namespace devices::ethercat::head_board::armar7de
{
    using DataPtr = std::shared_ptr<class Data>;
}

namespace devices::ethercat::head_board::armar7de
{
    class Device;
}

namespace devices::ethercat::head_board::armar7de::joint_controller
{
    using PWMControllerPtr = std::shared_ptr<class PWMController>;

    class PWMController : public armarx::JointController
    {

    public:
        PWMController(const std::string&, Device*, DataPtr jointData);

        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;
        armarx::ControlTargetBase* getControlTarget() override;

    protected:
        ControlTargetHeadPWM target;
        DataPtr dataPtr;
    };

} // namespace devices::ethercat::head_board::armar7de::joint_controller
