#pragma once

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>

namespace devices::ethercat::head_board::armar7de::joint_controller
{
    class LedStopMovementController : public armarx::JointController
    {
    public:
        LedStopMovementController();
        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;
        armarx::ControlTargetBase* getControlTarget() override;
        void rtPreActivateController() override;
    private:
        armarx::DummyControlTargetStopMovement target;
    };
    using LedStopMovementControllerPtr = std::shared_ptr<LedStopMovementController>;
} // namespace devices::ethercat::head_board::armar7de::joint_controller
