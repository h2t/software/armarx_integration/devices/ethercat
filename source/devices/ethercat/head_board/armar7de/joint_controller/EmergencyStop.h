#pragma once


// armarx
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>


namespace armarx
{
    using PIDControllerPtr = std::shared_ptr<class PIDController>;
}


namespace devices::ethercat::head_board::armar7de
{
    using DataPtr = std::shared_ptr<class Data>;
}


namespace devices::ethercat::head_board::armar7de::joint_controller
{

    using EmergencyStopControllerPtr = std::shared_ptr<class EmergencyStopController>;


    class EmergencyStopController : public armarx::JointController
    {

    public:
        EmergencyStopController(DataPtr dataPtr);

    private:
        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;
        /**
         * Returns the Target for this controller, but as this is the Emergency controller it will ignored.
         * As this controller will just break
         * @return is type VelocityTarget but it will return a nullptr, because it won't be possible to set a target
         */
        armarx::ControlTargetBase* getControlTarget() override;

        void rtPreActivateController() override;

    private:
        armarx::DummyControlTargetEmergencyStop target;
        DataPtr dataPtr;
        armarx::PIDControllerPtr pid;
        float lastPWM = 0.0f;
    };

} // namespace devices::ethercat::head_board::armar7de::joint_controller
