#include "LedEmergencyStop.h"

namespace devices::ethercat::head_board::armar7de::joint_controller
{
    LedEmergencyStopController::LedEmergencyStopController()
    {}
    void LedEmergencyStopController::rtRun(
        const IceUtil::Time&, const IceUtil::Time&)
    {
    }

    armarx::ControlTargetBase* LedEmergencyStopController::getControlTarget()
    {
        return &target;
    }

    void LedEmergencyStopController::rtPreActivateController()
    {
    }
} // namespace devices::ethercat::head_board::armar7de::joint_controller
