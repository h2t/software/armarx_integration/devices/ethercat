#pragma once


// armarx
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/libraries/core/PIDController.h>


namespace devices::ethercat::head_board::armar7de
{
    using DataPtr = std::shared_ptr<class Data>;
}


namespace devices::ethercat::head_board::armar7de::joint_controller
{

    using StopMovementControllerPtr = std::shared_ptr<class StopMovementController>;


    class StopMovementController : public armarx::JointController
    {

    public:
        StopMovementController(DataPtr dataPtr);

    private:
        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;
        /**
         * Returns the Target for this controller, but as this is the Emergency controller it will ignored.
         * As this controller will just break
         * @return is type VelocityTarget but it will return a nullptr, because it won't be possible to set a target
         */
        armarx::ControlTargetBase* getControlTarget() override;

        void rtPreActivateController() override;

    private:
        armarx::DummyControlTargetStopMovement target;
        DataPtr dataPtr;
        armarx::PIDControllerPtr pid;
    };

} // namespace devices::ethercat::head_board::armar7de::joint_controller
