#pragma once


// armarx
#include <RobotAPI/components/units/RobotUnit/BasicControllers.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <armarx/control/hardware_config/Config.h>


namespace devices::ethercat::head_board::armar7de
{
    using DataPtr = std::shared_ptr<class Data>;
}

namespace devices::ethercat::head_board::armar7de
{
    class Device;
}


namespace devices::ethercat::head_board::armar7de::joint_controller
{

    using PWMZeroTorqueControllerConfigurationPtr =
        std::shared_ptr<class PWMZeroTorqueControllerConfiguration>;
    using PWMZeroTorqueControllerConfigurationCPtr =
        std::shared_ptr<const class PWMZeroTorqueControllerConfiguration>;


    class PWMZeroTorqueControllerConfiguration
    {

    public:
        PWMZeroTorqueControllerConfiguration()
        {
        }
        static PWMZeroTorqueControllerConfigurationCPtr
        CreateConfigData(armarx::control::hardware_config::Config& hwConfig);
        float feedforwardVelocityToPWMFactor;
        float PWMDeadzone;
    };


    using JointPWMZeroTorqueControllerPtr = std::shared_ptr<class PWMZeroTorqueController>;


    class PWMZeroTorqueController : public armarx::JointController
    {

    public:
        PWMZeroTorqueController(const std::string& deviceName,
                                Device* board,
                                DataPtr jointData,
                                PWMZeroTorqueControllerConfigurationCPtr config);
        ~PWMZeroTorqueController() noexcept(true);

        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;
        armarx::ControlTargetBase* getControlTarget() override;

        void rtPreActivateController() override;
        void rtPostDeactivateController() override;

    private:
        PWMZeroTorqueControllerConfigurationCPtr config;
        armarx::ControlTarget1DoFActuatorZeroTorque target;

        std::atomic<double> lastTargetVelocity;
        bool isLimitless;

        DataPtr dataPtr;
        Device* board;
        const std::string deviceName;
    };

} // namespace devices::ethercat::head_board::armar7de::joint_controller
