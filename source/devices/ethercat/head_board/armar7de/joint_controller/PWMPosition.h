#pragma once


// armarx
#include <ArmarXCore/core/services/tasks/ThreadPool.h>
#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <RobotAPI/components/units/RobotUnit/BasicControllers.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <armarx/control/hardware_config/Config.h>


namespace armarx
{
    using PIDControllerPtr = std::shared_ptr<class PIDController>;
}

namespace devices::ethercat::head_board::armar7de
{
    using DataPtr = std::shared_ptr<class Data>;
}

namespace devices::ethercat::head_board::armar7de
{
    class Device;
}

namespace devices::ethercat::head_board::armar7de::joint_controller
{
    using PWMPositionControllerConfigurationPtr =
        std::shared_ptr<class PWMPositionControllerConfiguration>;
    using PWMPositionControllerConfigurationCPtr =
        std::shared_ptr<const PWMPositionControllerConfiguration>;

    class PWMPositionControllerConfiguration
    {

    public:
        PWMPositionControllerConfiguration()
        {
        }
        static PWMPositionControllerConfigurationCPtr
        CreatePWMPositionControllerConfigData(armarx::control::hardware_config::Config& hwConfig);
        float maxVelocityRad;
        float maxAccelerationRad;
        float maxDecelerationRad;
        float maxDt;
        float p;
        float i;
        float d;
        float maxIntegral;
        float feedforwardVelocityToPWMFactor;
        float feedforwardTorqueToPWMFactor;
        float PWMDeadzone;
        float velocityUpdatePercent;
        float conditionalIntegralErrorTreshold;
        bool feedForwardMode;
    };


    using PWMPositionControllerPtr = std::shared_ptr<class PWMPositionController>;


    class PWMPositionController : public armarx::JointController
    {

    public:
        PWMPositionController(
            const std::string& deviceName,
            Device* board,
            DataPtr jointData,
            PWMPositionControllerConfigurationCPtr positionControllerConfigDataPtr);
        ~PWMPositionController() noexcept(true);

        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;
        armarx::ControlTargetBase* getControlTarget() override;

        void rtPreActivateController() override;
        void rtPostDeactivateController() override;

        armarx::StringVariantBaseMap
        publish(const armarx::DebugDrawerInterfacePrx& draw,
                const armarx::DebugObserverInterfacePrx& observer) const override;

    protected:
        PWMPositionControllerConfigurationCPtr config;
        //        PWMVelocityController controller;
        //        PositionThroughVelocityControllerWithAccelerationAndPositionBounds posController;
        armarx::MinJerkPositionController posController;
        armarx::PIDControllerPtr pidPosController;
        armarx::ControlTarget1DoFActuatorPosition target;

        std::atomic<double> lastTargetVelocity, targetPWM;
        bool isLimitless;

        DataPtr dataPtr;
        Device* board;
        const std::string deviceName;
        mutable armarx::RemoteGuiInterfacePrx remoteGui;
        bool stopRequested = false;
        mutable armarx::ThreadPool::Handle threadHandle;
        const armarx::SensorValue1DoFActuator* sensorValue;
    };

} // namespace devices::ethercat::head_board::armar7de::joint_controller
