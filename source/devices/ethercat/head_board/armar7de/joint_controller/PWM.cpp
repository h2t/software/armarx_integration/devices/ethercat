#include "PWM.h"


// robot_devices
#include <devices/ethercat/head_board/armar7de/Data.h>
#include <devices/ethercat/head_board/armar7de/Device.h>

namespace devices::ethercat::head_board::armar7de::joint_controller
{

    PWMController::PWMController(const std::string&, Device*, DataPtr jointData) :
        dataPtr(jointData)
    {
    }


    void
    PWMController::rtRun(const IceUtil::Time&, const IceUtil::Time&)
    {
//        ARMARX_INFO << "PWMController";

        if (target.isValid())
        {
            dataPtr->setMotorPwmValue(target.pwm);
        }
        else
        {
            ARMARX_ERROR << "invalid target set for actor";
        }
    }


    armarx::ControlTargetBase*
    PWMController::getControlTarget()
    {
        return &target;
    }

} // namespace devices::ethercat::head_board::armar7de::joint_controller
