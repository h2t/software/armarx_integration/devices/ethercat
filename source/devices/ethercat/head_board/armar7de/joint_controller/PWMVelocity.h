#pragma once

// armarx
#include <ArmarXCore/core/services/tasks/ThreadPool.h>
#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <RobotAPI/components/units/RobotUnit/BasicControllers.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>

// robot_devices
#include <devices/ethercat/head_board/armar7de/PWMVelocity.h>


namespace armarx
{
    using PIDControllerPtr = std::shared_ptr<class PIDController>;
}


namespace devices::ethercat::head_board::armar7de
{
    using DataPtr = std::shared_ptr<class Data>;
}


namespace devices::ethercat::head_board::armar7de
{
    class Device;
    using PWMVelocityControllerConfigurationCPtr =
        std::shared_ptr<const class PWMVelocityControllerConfiguration>;
} // namespace devices::ethercat::head_board::armar7de

namespace devices::ethercat::head_board::armar7de::joint_controller
{

    class PWMVelocityController : public armarx::JointController
    {

    public:
        PWMVelocityController(
            const std::string& deviceName,
            Device* board,
            DataPtr jointData,
            PWMVelocityControllerConfigurationCPtr velocityControllerConfigDataPtr);
        ~PWMVelocityController() noexcept(true);

        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;
        armarx::ControlTargetBase* getControlTarget() override;

        armarx::StringVariantBaseMap
        publish(const armarx::DebugDrawerInterfacePrx& draw,
                const armarx::DebugObserverInterfacePrx& observer) const override;

        void rtPreActivateController() override;
        void rtPostDeactivateController() override;

    protected:
        PWMVelocityControllerConfigurationCPtr config;
        devices::ethercat::head_board::armar7de::PWMVelocityController controller;
        armarx::VelocityControllerWithRampedAccelerationAndPositionBounds velController;

        armarx::ControlTarget1DoFActuatorVelocity target;

        std::atomic<float> lastTargetVelocity, lastTargetAcceleration;
        bool isLimitless;

        DataPtr dataPtr;
        Device* board;
        const std::string deviceName;
        mutable armarx::RemoteGuiInterfacePrx remoteGui;
        bool stopRequested = false;
        mutable armarx::ThreadPool::Handle threadHandle;
        const armarx::SensorValue1DoFActuator* sensorValue;
    };

} // namespace devices::ethercat::head_board::armar7de::joint_controller
