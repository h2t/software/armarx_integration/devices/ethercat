#pragma once

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>

#include "../LedControlData.h"
#include "../ControlTargets.h"

namespace devices::ethercat::head_board::armar7de::joint_controller
{
    class LedColorController : public armarx::JointController
    {
    public:
        LedColorController(LedControlDataPtr data);
        armarx::ControlTargetBase* getControlTarget() override;
        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;
    private:
        LedControlDataPtr data;
        ControlTargetHeadLedColor target;
    };
    using LedColorControllerPtr = std::shared_ptr<LedColorController>;
} // namespace devices::ethercat::head_board::armar7de::joint_controller
