#include "StopMovement.h"


// robot_devices
#include <devices/ethercat/head_board/armar7de/Data.h>


namespace devices::ethercat::head_board::armar7de::joint_controller
{

    StopMovementController::StopMovementController(DataPtr dataPtr) : dataPtr(dataPtr)
    {
        pid.reset(new armarx::PIDController(0, 5000, 0));
        pid->maxIntegral = 0.1;
    }


    void
    StopMovementController::rtRun(const IceUtil::Time& sensorValuesTimestamp,
                                  const IceUtil::Time& timeSinceLastIteration)
    {
        pid->update(timeSinceLastIteration.toSecondsDouble(), dataPtr->getVelocity(), 0.0);
        float targetPwm = pid->getControlValue();

        if (std::isnan(targetPwm))
        {
            targetPwm = 0.0f;
        }

        // ARMARX_INFO << "StopMovement";
//        ARMARX_INFO << VAROUT(dataPtr->getVelocity());
//        ARMARX_INFO << VAROUT(targetPwm);
        targetPwm = 0.0f;

        dataPtr->setMotorPwmValue(targetPwm);
    }


    armarx::ControlTargetBase*
    StopMovementController::getControlTarget()
    {
        return &target;
    }


    void
    StopMovementController::rtPreActivateController()
    {
        pid->reset();
    }

} // namespace devices::ethercat::head_board::armar7de::joint_controller
