/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "PWMVelocity.h"

#include <ArmarXCore/observers/filters/rtfilters/AverageFilter.h>

namespace devices::ethercat::head_board::armar7de
{

    PWMVelocityController::PWMVelocityController(
        PWMVelocityControllerConfigurationCPtr velocityControllerConfigDataPtr) :
        config(velocityControllerConfigDataPtr)
    {
        pid.reset(new armarx::PIDController(velocityControllerConfigDataPtr->p,
                                            velocityControllerConfigDataPtr->i,
                                            velocityControllerConfigDataPtr->d));
        pid->pdOutputFilter.reset(new armarx::rtfilters::AverageFilter(10));
        pid->maxIntegral = velocityControllerConfigDataPtr->maxIntegral;
        pid->conditionalIntegralErrorTreshold =
            velocityControllerConfigDataPtr->conditionalIntegralErrorTreshold;
        pid->threadSafe = false;
    }


    double
    PWMVelocityController::run(IceUtil::Time const& deltaT,
                               double currentVelocity,
                               double targetVelocity,
                               double gravityTorque)
    {
        double targetPWM = 0;
        if (!this->config->feedForwardMode)
        {
            lastActualVelocity = lastActualVelocity * (1.0 - config->velocityUpdatePercent) +
                                 currentVelocity * config->velocityUpdatePercent;
            pid->update(deltaT.toSecondsDouble(), lastActualVelocity, targetVelocity);
            targetPWM = pid->getControlValue();
        }
        float torqueFF = config->feedforwardTorqueToPWMFactor * -gravityTorque;
        //        ARMARX_INFO << deactivateSpam(1) << VAROUT(torqueFF);
        targetPWM += torqueFF;

        //feed forward
        if (std::abs(targetVelocity) > 0.001 && std::abs(currentVelocity) < 0.0001f)
        {
            targetPWM +=
                config->PWMDeadzone * armarx::math::MathUtils::Sign(targetVelocity); // deadzone
        }
        targetPWM +=
            config->feedforwardVelocityToPWMFactor * targetVelocity; // approx. feedforward vel

        //            ARMARX_RT_LOGF_INFO("target velocity: %.3f, current velocity: %.3f, target pwm: %d, kp: %.3f ki: %f, kd: %f, max acc: %.3f",
        //                                target.velocity, dataPtr->getVelocity(), targetPWM, pid->Kp, pid->Ki, pid->Kd, controller.acceleration).deactivateSpam(1);

        return targetPWM;
    }


    void
    PWMVelocityController::reset(double currentVelocity)
    {
        lastActualVelocity = currentVelocity;
        pid->reset();
    }


    PWMVelocityControllerConfigurationCPtr
    PWMVelocityControllerConfiguration::CreatePWMVelocityControllerConfigData(
        armarx::control::hardware_config::Config& hwConfig)
    {
        PWMVelocityControllerConfigurationPtr configData(new PWMVelocityControllerConfiguration());

        // Initialize inherited members
        configData->maxVelocityRad = hwConfig.getFloat("maxVelocityRad");
        configData->maxDecelerationRad = hwConfig.getFloat("maxDecelerationRad");
        configData->jerk = hwConfig.getFloat("jerk");
        configData->maxDt = hwConfig.getFloat("maxDt");
        configData->directSetVLimit = hwConfig.getFloat("directSetVLimit");

        // Initialize remaining members
        configData->p = hwConfig.getFloat("p");
        configData->i = hwConfig.getFloat("i");
        configData->d = hwConfig.getFloat("d");
        configData->maxIntegral = hwConfig.getFloat("maxIntegral");
        configData->feedforwardVelocityToPWMFactor =
            hwConfig.getFloat("feedforwardVelocityToPWMFactor");
        configData->feedforwardTorqueToPWMFactor =
            hwConfig.getFloat("feedforwardTorqueToPWMFactor");
        configData->PWMDeadzone = hwConfig.getFloat("PWMDeadzone");
        configData->velocityUpdatePercent =
            hwConfig.getFloat("velocityUpdatePercent");
        configData->conditionalIntegralErrorTreshold =
            hwConfig.getFloat("conditionalIntegralErrorTreshold");
        configData->feedForwardMode = hwConfig.getBool("FeedForwardMode");

        return configData;
    }

} // namespace devices::ethercat::head_board::armar7de
