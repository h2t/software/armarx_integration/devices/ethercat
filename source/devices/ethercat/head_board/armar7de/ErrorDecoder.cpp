#include "ErrorDecoder.h"

namespace devices::ethercat::head_board::armar7de
{
    ErrorDecoder::ErrorDecoder(SlaveOut* outputs, SlaveIn* inputs) :
        outputs(outputs), inputs(inputs)
    {
    }

    std::uint16_t
    ErrorDecoder::getAbsoluteEncoderStatus() const
    {
        return outputs->absoluteEncoderStatus;
    }

    bool
    ErrorDecoder::absoluteEncoderErrorDetected(armarx::control::ethercat::SlaveInterface* slave,
                                               bool printError) const
    {
        std::uint16_t errors = getAbsoluteEncoderStatus();

        if (printError)
        {
            if (errors & AbsoluteEncoderStatus::ErrorAccelerationError)
            {
                SLAVE_ERROR(
                    slave->getSlaveIdentifier(),
                    "Absolute encoder: Acceleration error. The position "
                    "data changed too fast. A stray magnetic field is present or metal particles "
                    "are present between the readhead and the ring")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::ErrorEncoderNotConfiguredProperly)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Absolute encoder: ERROR - encoder not "
                            "configured properly")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::ErrorMagneticPatternError)
            {
                SLAVE_ERROR(
                    slave->getSlaveIdentifier(),
                    "Absolute encoder: Magnetic pattern error. A stray "
                    "magnetic field is present or metal particles are present between the readhead "
                    "and the ring or radial positioning between the readhead and the ring is out "
                    "of tolerances.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::ErrorMagneticSensor)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Absolute encoder: Magnetic sensor. "
                            "Cycle power to the encoder.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::ErrorMultiturnCounterMismatch)
            {
                SLAVE_ERROR(
                    slave->getSlaveIdentifier(),
                    "Absolute encoder: Multiturn counter mismatch. Encoder "
                    "was rotated for more than ±90° during power-down. Cycle the power to clear "
                    "this error or apply new multiturn counter value.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::ErrorPositionNotValid)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Absolute encoder: If bit is set, "
                            "position is not valid.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::ErrorPowerSupplyError)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Absolute encoder: Power supply error. The readhead "
                            "power supply voltage is out of specified range.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::ErrorSensorReadingError)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Absolute encoder: Sensor reading error, probably "
                            "caused by electrical interference, ground loop or RFI.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::ErrorSignalAmplitudeTooHigh)
            {
                SLAVE_ERROR(
                    slave->getSlaveIdentifier(),
                    "Absolute encoder: Signal amplitude too high. The "
                    "readhead is too close to the ring or an external magnetic field is present.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::ErrorSignalLost)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Absolute encoder: Signal lost. The readhead is out of "
                            "alignment with the ring or the ring is damaged.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::ErrorSystemError)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Absolute encoder: System error. "
                            "Malfunction inside the circuitry or inconsistent calibration "
                            "data is detected. To reset the System error bit try to cycle "
                            "the power supply while the rise time is shorter than 20 ms.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::WarningEncoderNearOperationalLimits)
            {
                SLAVE_WARNING(
                    slave->getSlaveIdentifier(),
                    "Absolute encoder: If bit is set, encoder is near "
                    "operational limits. Position is valid. Resolution and / or accuracy might be "
                    "lower than specified.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::WarningSignalAmplitudeLow)
            {
                SLAVE_WARNING(slave->getSlaveIdentifier(),
                              "Absolute encoder: Signal amplitude low. The distance "
                              "between the readhead and the ring is too large.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::WarningSignalAmplitudeTooHigh)
            {
                SLAVE_WARNING(
                    slave->getSlaveIdentifier(),
                    "Absolute encoder: Signal amplitude too high. The "
                    "readhead is too close to the ring or an external magnetic field is present.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::WarningSignalAmplitudeTooHigh2)
            {
                SLAVE_WARNING(
                    slave->getSlaveIdentifier(),
                    "Absolute encoder: Signal amplitude too high. The "
                    "readhead is too close to the ring or an external magnetic field is present.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::WarningTemperature)
            {
                SLAVE_WARNING(slave->getSlaveIdentifier(),
                              "Absolute encoder: Temperature. The "
                              "readhead temperature is out of specified range.")
                    .deactivateSpam(1);
            }
        }

        // Return true iff there are errors
        return (errors &
                (AbsoluteEncoderStatus::ErrorAccelerationError |
                 AbsoluteEncoderStatus::ErrorEncoderNotConfiguredProperly |
                 AbsoluteEncoderStatus::ErrorMagneticPatternError |
                 AbsoluteEncoderStatus::ErrorMagneticSensor |
                 AbsoluteEncoderStatus::ErrorMultiturnCounterMismatch |
                 AbsoluteEncoderStatus::ErrorPositionNotValid |
                 AbsoluteEncoderStatus::ErrorPowerSupplyError |
                 AbsoluteEncoderStatus::ErrorSensorReadingError |
                 AbsoluteEncoderStatus::ErrorSignalAmplitudeTooHigh |
                 AbsoluteEncoderStatus::ErrorSignalLost | AbsoluteEncoderStatus::ErrorSystemError))
                   ? true
                   : false;
    }

    bool
    ErrorDecoder::getEmergencyStopStatus() const
    {
        ARMARX_CHECK_NOT_NULL(outputs);
        return (0x8000 & outputs->statusBits) ? true : false;
    }

    std::uint8_t
    ErrorDecoder::calcAbsoluteEncoderCrc() const
    {
        std::uint8_t raw_byte1 = (outputs->absoluteEncoderValue >> 12) & 0xff;
        std::uint8_t raw_byte2 = (outputs->absoluteEncoderValue >> 4) & 0xff;
        std::uint8_t raw_byte3 = ((outputs->absoluteEncoderValue << 4) & 0xf0) |
                                 ((outputs->absoluteEncoderStatus >> 6) & 0x0f);
        std::uint8_t raw_byte4 = ((outputs->absoluteEncoderStatus << 2) & 0xff) | 0x03;
        std::uint32_t raw_bytes = static_cast<std::uint32_t>(raw_byte1) << 24 |
                                  static_cast<std::uint32_t>(raw_byte2) << 16 |
                                  static_cast<std::uint32_t>(raw_byte3) << 8 |
                                  static_cast<std::uint32_t>(raw_byte4);
        return crc8_4B(raw_bytes);
    }

    // use this function to calculate CRC from 32-bit number
    std::uint8_t
    ErrorDecoder::crc8_4B(std::uint32_t bb) const
    {
        // Taken from Appendix 1 in https://gitlab.com/h2t/hardware/armar/armar-7/armar-7.dc-motordriver/-/blob/master/Doku/MHAD01_09.pdf
        // poly = 0x97
        static std::uint8_t tableCRC[256] = {
            0x00, 0x97, 0xB9, 0x2E, 0xE5, 0x72, 0x5C, 0xCB, 0x5D, 0xCA, 0xE4, 0x73, 0xB8, 0x2F,
            0x01, 0x96, 0xBA, 0x2D, 0x03, 0x94, 0x5F, 0xC8, 0xE6, 0x71, 0xE7, 0x70, 0x5E, 0xC9,
            0x02, 0x95, 0xBB, 0x2C, 0xE3, 0x74, 0x5A, 0xCD, 0x06, 0x91, 0xBF, 0x28, 0xBE, 0x29,
            0x07, 0x90, 0x5B, 0xCC, 0xE2, 0x75, 0x59, 0xCE, 0xE0, 0x77, 0xBC, 0x2B, 0x05, 0x92,
            0x04, 0x93, 0xBD, 0x2A, 0xE1, 0x76, 0x58, 0xCF, 0x51, 0xC6, 0xE8, 0x7F, 0xB4, 0x23,
            0x0D, 0x9A, 0x0C, 0x9B, 0xB5, 0x22, 0xE9, 0x7E, 0x50, 0xC7, 0xEB, 0x7C, 0x52, 0xC5,
            0x0E, 0x99, 0xB7, 0x20, 0xB6, 0x21, 0x0F, 0x98, 0x53, 0xC4, 0xEA, 0x7D, 0xB2, 0x25,
            0x0B, 0x9C, 0x57, 0xC0, 0xEE, 0x79, 0xEF, 0x78, 0x56, 0xC1, 0x0A, 0x9D, 0xB3, 0x24,
            0x08, 0x9F, 0xB1, 0x26, 0xED, 0x7A, 0x54, 0xC3, 0x55, 0xC2, 0xEC, 0x7B, 0xB0, 0x27,
            0x09, 0x9E, 0xA2, 0x35, 0x1B, 0x8C, 0x47, 0xD0, 0xFE, 0x69, 0xFF, 0x68, 0x46, 0xD1,
            0x1A, 0x8D, 0xA3, 0x34, 0x18, 0x8F, 0xA1, 0x36, 0xFD, 0x6A, 0x44, 0xD3, 0x45, 0xD2,
            0xFC, 0x6B, 0xA0, 0x37, 0x19, 0x8E, 0x41, 0xD6, 0xF8, 0x6F, 0xA4, 0x33, 0x1D, 0x8A,
            0x1C, 0x8B, 0xA5, 0x32, 0xF9, 0x6E, 0x40, 0xD7, 0xFB, 0x6C, 0x42, 0xD5, 0x1E, 0x89,
            0xA7, 0x30, 0xA6, 0x31, 0x1F, 0x88, 0x43, 0xD4, 0xFA, 0x6D, 0xF3, 0x64, 0x4A, 0xDD,
            0x16, 0x81, 0xAF, 0x38, 0xAE, 0x39, 0x17, 0x80, 0x4B, 0xDC, 0xF2, 0x65, 0x49, 0xDE,
            0xF0, 0x67, 0xAC, 0x3B, 0x15, 0x82, 0x14, 0x83, 0xAD, 0x3A, 0xF1, 0x66, 0x48, 0xDF,
            0x10, 0x87, 0xA9, 0x3E, 0xF5, 0x62, 0x4C, 0xDB, 0x4D, 0xDA, 0xF4, 0x63, 0xA8, 0x3F,
            0x11, 0x86, 0xAA, 0x3D, 0x13, 0x84, 0x4F, 0xD8, 0xF6, 0x61, 0xF7, 0x60, 0x4E, 0xD9,
            0x12, 0x85, 0xAB, 0x3C};


        std::uint8_t crc;
        std::uint32_t t;
        t = (bb >> 24) & 0x000000FF;
        crc = ((bb >> 16) & 0x000000FF);
        t = crc ^ tableCRC[t];
        crc = ((bb >> 8) & 0x000000FF);
        t = crc ^ tableCRC[t];
        crc = (bb & 0x000000FF);
        t = crc ^ tableCRC[t];
        crc = tableCRC[t];
        return crc;
    }

} // namespace devices::ethercat::head_board::armar7de
