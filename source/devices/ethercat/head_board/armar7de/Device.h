/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Armar6RT::ArmarXObjects::Armar6IceKinematicUnit
 * @author     Markus Swarowsky ( markus dot swarowsky at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <VirtualRobot/Nodes/RobotNode.h>

#include <RobotAPI/components/units/RobotUnit/BasicControllers.h>
#include <RobotAPI/components/units/RobotUnit/Devices/ControlDevice.h>
#include <RobotAPI/components/units/RobotUnit/Devices/SensorDevice.h>
#include <armarx/control/ethercat/DeviceInterface.h>
#include <armarx/control/ethercat/SlaveIdentifier.h>

#include <devices/ethercat/head_board/armar7de/Config.h>
#include <devices/ethercat/head_board/armar7de/Data.h>
#include <devices/ethercat/head_board/armar7de/Slave.h>

#include <devices/ethercat/head_board/armar7de/LedControlData.h>


namespace devices::ethercat::head_board::armar7de
{

    namespace joint_controller
    {
        using StopMovementControllerPtr = std::shared_ptr<class StopMovementController>;
        using EmergencyStopControllerPtr = std::shared_ptr<class EmergencyStopController>;
        using PWMVelocityControllerPtr = std::shared_ptr<class PWMVelocityController>;
        using PWMPositionControllerPtr = std::shared_ptr<class PWMPositionController>;
        using PWMZeroTorqueControllerPtr = std::shared_ptr<class PWMZeroTorqueController>;
        using ParallelGripperPositionControllerPtr =
            std::shared_ptr<class ParallelGripperPositionController>;
        using ParallelGripperVelocityControllerPtr =
            std::shared_ptr<class ParallelGripperVelocityController>;
        using PWMControllerPtr = std::shared_ptr<class PWMController>;
        using LedStopMovementControllerPtr = std::shared_ptr<class LedStopMovementController>;
        using LedEmergencyStopControllerPtr = std::shared_ptr<class LedEmergencyStopController>;
        using LedColorControllerPtr = std::shared_ptr<class LedColorController>;
        using PWMPositionControllerConfigurationCPtr =
            std::shared_ptr<const class PWMPositionControllerConfiguration>;
        using PWMZeroTorqueControllerConfigurationCPtr =
            std::shared_ptr<const class PWMZeroTorqueControllerConfiguration>;
    } // namespace joint_controller

    namespace hwconfig = armarx::control::hardware_config;
    using PWMVelocityControllerConfigurationCPtr =
        std::shared_ptr<const class PWMVelocityControllerConfiguration>;


    class Device :
        public armarx::ControlDevice,
        public armarx::SensorDevice,
        public armarx::control::ethercat::DeviceInterface
    {

    public:
        class LedDevice :
                public armarx::ControlDevice,
                public armarx::control::ethercat::DeviceInterface::SubDeviceInterface
        {
            friend class Device;

        public:
            LedDevice(const std::string& deviceName);

            void init(Device* device, LedControlDataPtr ledControlDataPtr);

        protected:
            Device* device;
            LedControlDataPtr ledControlDataPtr;
            joint_controller::LedEmergencyStopControllerPtr emergencyController;
            joint_controller::LedStopMovementControllerPtr stopMovementController;
            joint_controller::LedColorControllerPtr colorController;

            // ControlDevice interface
            void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                     const IceUtil::Time& timeSinceLastIteration) override;
        };

        using LedDevicePtr = std::shared_ptr<LedDevice>;

        Device(hwconfig::DeviceConfig& hwConfig,
               VirtualRobot::RobotPtr const& robot);
        ~Device() override
        {
        }

        DeviceInterface::TryAssignResult
        tryAssign(armarx::control::ethercat::SlaveInterface& slave) override;

        DeviceInterface::AllAssignedResult onAllAssigned() override;

        std::string getClassName() const override;

        void postSwitchToSafeOp() override;

        void postSwitchToOp() override;

        // SensorDevice interface
        const armarx::SensorValueBase* getSensorValue() const override;
        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration) override;

        // ControlDevice interface
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                 const IceUtil::Time& timeSinceLastIteration) override;

        std::shared_ptr<Data> getData() const;

        const std::string& getJointName() const;

        armarx::control::ethercat::SlaveIdentifier getSlaveIdentifier() const;

        std::uint16_t getSlaveNumber() const;

        bool switchControlMode(armarx::ControlMode newMode);

        float getSoftLimitHi() const;

        float getSoftLimitLo() const;

        bool isLimitless() const;


        const LedDevicePtr& getLedDevice() const;

    private:
        void updateSensorValueStruct();

        VirtualRobot::RobotNodePtr robotNode;

        ///The data and target object
        std::shared_ptr<Data> dataPtr;
        /// The data object for copying to non-rt part
        HeadBoardSensorValue sensorValue;

        const armarx::control::ethercat::SlaveIdentifier slaveIdentifier;

        Slave* slavePtr;

        ///the joint controller
        joint_controller::EmergencyStopControllerPtr emergencyController;
        joint_controller::StopMovementControllerPtr stopMovementController;
        joint_controller::PWMVelocityControllerPtr velocityController;
        joint_controller::PWMPositionControllerPtr positionController;
        joint_controller::PWMZeroTorqueControllerPtr zeroTorqueController;
        joint_controller::PWMControllerPtr pwmController;

        /// Controller configuration
        joint_controller::PWMPositionControllerConfigurationCPtr positionControllerCfg;
        // TODO: Why not the controller in joint_controllers? Why is there another one?
        PWMVelocityControllerConfigurationCPtr velocityControllerCfg;
        joint_controller::PWMZeroTorqueControllerConfigurationCPtr zeroTorqueControllerCfg;

        LedControlDataPtr ledControlDataPtr;
        LedDevicePtr ledDevice;

        HeadBoardConfig config;
        HeadBoardDataConfig dataConfig;
    };

} // namespace devices::ethercat::head_board::armar7de
