#pragma once


#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTargetBase.h>


namespace devices::ethercat::head_board::armar7de
{

    namespace ctrl_modes
    {
        static const std::string HeadPWM = "ControlMode_HeadPWM";
        static const std::string HeadLedColor = "ControlMode_HeadLEDColor";
    }


    class ControlTargetHeadPWM : public armarx::ControlTargetBase
    {

    public:
        std::int32_t pwm = 0;
        ControlTargetHeadPWM() = default;
        ControlTargetHeadPWM(const ControlTargetHeadPWM&) = default;
        ControlTargetHeadPWM(ControlTargetHeadPWM&&) = default;
        ControlTargetHeadPWM(std::int32_t val) : pwm{val}
        {
        }
        ControlTargetHeadPWM& operator=(const ControlTargetHeadPWM&) = default;
        ControlTargetHeadPWM& operator=(ControlTargetHeadPWM&&) = default;

        ControlTargetHeadPWM&
        operator=(std::int32_t val)
        {
            pwm = val;
            return *this;
        }

        const std::string&
        getControlMode() const override
        {
            return ctrl_modes::HeadPWM;
        }

        void
        reset() override
        {
            pwm = std::numeric_limits<int>::min();
        }

        bool
        isValid() const override
        {
            return pwm != std::numeric_limits<int>::min();
        }

        DETAIL_ControlTargetBase_DEFAULT_METHOD_IMPLEMENTATION

            static ControlTargetInfo<ControlTargetHeadPWM>
            GetClassMemberInfo()
        {
            ControlTargetInfo<ControlTargetHeadPWM> cti;
            cti.addMemberVariable(&ControlTargetHeadPWM::pwm, "pwm");
            return cti;
        }
    };


    class ControlTargetHeadLedColor : public armarx::ControlTargetBase
    {

    public:
        std::array<std::uint8_t, 20> r;
        std::array<std::uint8_t, 20> g;
        std::array<std::uint8_t, 20> b;
        ControlTargetHeadLedColor() = default;
        ControlTargetHeadLedColor(const ControlTargetHeadLedColor&) = default;
        ControlTargetHeadLedColor(ControlTargetHeadLedColor&&) = default;
        ControlTargetHeadLedColor(std::array<std::uint8_t, 20> r, std::array<std::uint8_t, 20> g, std::array<std::uint8_t, 20> b)
        {
            this->r = r;
            this->g = g;
            this->b = b;
        }
        ControlTargetHeadLedColor& operator=(const ControlTargetHeadLedColor&) = default;
        ControlTargetHeadLedColor& operator=(ControlTargetHeadLedColor&&) = default;

        const std::string&
        getControlMode() const override
        {
            return ctrl_modes::HeadLedColor;
        }

        void
        reset() override
        {
            r.fill(0);
            g.fill(0);
            b.fill(0);
        }

        bool
        isValid() const override
        {
            return true;
        }

        DETAIL_ControlTargetBase_DEFAULT_METHOD_IMPLEMENTATION

            static ControlTargetInfo<ControlTargetHeadLedColor>
            GetClassMemberInfo()
        {
            ControlTargetInfo<ControlTargetHeadLedColor> cti;
            cti.addMemberVariable(&ControlTargetHeadLedColor::r, "r");
            cti.addMemberVariable(&ControlTargetHeadLedColor::g, "g");
            cti.addMemberVariable(&ControlTargetHeadLedColor::b, "b");
            return cti;
        }
    };

} // namespace devices::ethercat::head_board::armar7de
