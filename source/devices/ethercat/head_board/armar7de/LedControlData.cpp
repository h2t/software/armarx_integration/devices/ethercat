#include "LedControlData.h"

namespace devices::ethercat::head_board::armar7de
{
    LedControlData::LedControlData(std::uint16_t* target) :
        target{target}
    {
        ARMARX_TRACE;
        ARMARX_CHECK_NOT_NULL(target);
    }

    void
    LedControlData::rtReadSensorValues(
        const IceUtil::Time& /*sensorValuesTimestamp*/,
        const IceUtil::Time& /*timeSinceLastIteration*/)
    {}

    void
    LedControlData::rtWriteTargetValues(
        const IceUtil::Time& /*sensorValuesTimestamp*/,
        const IceUtil::Time& /*timeSinceLastIteration*/)
    {
        std::copy(std::begin(ledRgbStripControl),
                  std::end(ledRgbStripControl),
                  target);
    }

    void
    LedControlData::setLedRgbStripControl(std::array<std::uint16_t, 20> value)
    {
        ledRgbStripControl = value;
    }
} // namespace devices::ethercat::head_board::armar7de
