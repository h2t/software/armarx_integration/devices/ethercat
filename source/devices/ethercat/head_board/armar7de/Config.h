#pragma once

#include <armarx/control/hardware_config/DeviceConfig.h>

namespace devices::ethercat::head_board::armar7de
{
    namespace hwconfig = armarx::control::hardware_config;
    struct HeadBoardConfig
    {
        HeadBoardConfig(hwconfig::DeviceConfig& hwConfig)
            : alwaysUseAksIm1(hwConfig.getUint("alwaysUseAksIM_1")),
              onBoardTemperatureLimit(hwConfig.getUint("onBoardTemperatureLimit")),
              externalTemperatureLimit(hwConfig.getUint("externalTemperatureLimit")),
              ledStripCount(hwConfig.getUint("ledRGBStripCount")),
              ledStripGlobalBrightness(hwConfig.getUint("ledRGBStripGlobalBrightness")),
              isExternalTemperatureSensorConnected(hwConfig.getBool("isExternalTemperatureSensorConnected"))
        {

        }

        /**
         * If this field is not 0, it indicates to the board, that a connected absolute encoder
         * of type AksIM-1 must be used, even if there is a AksIM-2-encoder is connected.
         * This will only work if an AksIm-1-encoder is connected
         */
        std::uint8_t alwaysUseAksIm1;

        /**
         * The temperature limit of the on-board temperature sensor.
         * The default value is 60°C and should only be changed with good reason.
         */
        std::uint8_t onBoardTemperatureLimit;

        /**
         * The temperature limit of the external temperature sensor. The motor control is
         * disabled and the motor stopped if the measured external temperature is higher than
         * this limit. The default value is 80°C and should only be changed with very good reason.
         */
        std::uint8_t externalTemperatureLimit;

        /**
         * Led count of connected led strip
         */
        std::uint8_t ledStripCount;

        /**
         * Valid values range from 0 (darkest) to 31 (brightest).
         */
        std::uint8_t ledStripGlobalBrightness;

        /**
         * @brief isExternalTemperatureSensorConnected
         * Should default to true
         */
        bool isExternalTemperatureSensorConnected;
    };

    struct HeadBoardDataConfig
    {
        HeadBoardDataConfig(hwconfig::DeviceConfig& hwConfig)
            : absoluteEncoder(hwConfig.getLinearConfig("absoluteEncoder")),
              relativeEncoder(hwConfig.getLinearConfig("relativeEncoderTicks")),
              motorDirection(hwConfig.getInt("motorDirection")),
              maxPwm(hwConfig.getInt("maxPWM"))
        {
        }

        hwconfig::types::LinearConfig absoluteEncoder;
        hwconfig::types::LinearConfig relativeEncoder;
        std::int16_t motorDirection;
        std::int16_t maxPwm;
    };
}
