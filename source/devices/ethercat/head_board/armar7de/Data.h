#pragma once

#include <array>

#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <armarx/control/ethercat/DataInterface.h>
#include <armarx/control/rt_filters/AverageFilter.h>

#include "Slave.h"
#include <devices/ethercat/head_board/armar7de/ErrorDecoder.h>
#include <devices/ethercat/head_board/armar7de/SlaveIO.h>
#include <devices/ethercat/head_board/armar7de/Config.h>


namespace devices::ethercat::head_board::armar7de
{

    template <typename T>
    class SlopeValue
    {
    public:
        SlopeValue()
        {
            lastInput = 0;
            value = 0;
        }

        void
        read(T currentValue, const IceUtil::Time& timeSinceLastIteration)
        {
            //            ARMARX_INFO << "Slope value read";
            value = (static_cast<double>(currentValue) - static_cast<double>(lastInput)) /
                    timeSinceLastIteration.toMicroSecondsDouble() * 1e6;
            //            ARMARX_INFO << VAROUT(timeSinceLastIteration.toMicroSecondsDouble());
            //            ARMARX_INFO << VAROUT(value);
            //            ARMARX_INFO << VAROUT(currentValue);
            //            ARMARX_INFO << VAROUT(lastInput);
            lastInput = currentValue;
        }
        double value;

    private:
        T lastInput;
    };

    class HeadBoardSensorValue :
        virtual public armarx::SensorValue1DoFRealActuator,
        virtual public armarx::SensorValue1DoFMotorPWM
    {
    public:
        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION

            /* position */
            std::uint16_t absoluteEncoderDetailedStatus;
        float absoluteEncoderTemperature;
        std::uint32_t absoluteEncoderTicks;
        std::uint8_t absoluteEncoderCrc;

        float relativePosition;
        std::int32_t relativeTicks;

        float filteredAcceleration;

        float onBoardTemparature;
        /* motorTemparature */

        std::uint16_t mainUpdateRate;

        // Status Bits
        bool emergencyStopStatus;
        bool aksim1ConnectedAtStartup;
        bool aksim2ConnectedAtStartup;
        std::uint8_t activeAbsoluteEncoder;
        bool onBoardTemperatureError;
        bool externalTemperatureError;
        bool onBoardTemperatureLimitReached;
        bool externalTemperatureLimitReached;

        /* motorPWM */


        static SensorValueInfo<HeadBoardSensorValue>
        GetClassMemberInfo()
        {
            SensorValueInfo<HeadBoardSensorValue> svi;
            svi.addBaseClass<SensorValue1DoFMotorPWM>();
            svi.addBaseClass<SensorValue1DoFRealActuator>();
            svi.addMemberVariable(&HeadBoardSensorValue::absoluteEncoderDetailedStatus,
                                  "absoluteEncoderDetailedStatus");
            svi.addMemberVariable(&HeadBoardSensorValue::absoluteEncoderTemperature,
                                  "absoluteEncoderTemperature");
            svi.addMemberVariable(&HeadBoardSensorValue::absoluteEncoderTicks,
                                  "absoluteEncoderTicks");
            svi.addMemberVariable(&HeadBoardSensorValue::absoluteEncoderCrc, "absoluteEncoderCrc");
            svi.addMemberVariable(&HeadBoardSensorValue::relativePosition, "relativePosition");
            svi.addMemberVariable(&HeadBoardSensorValue::relativeTicks, "relativeTicks");
            svi.addMemberVariable(&HeadBoardSensorValue::filteredAcceleration,
                                  "filteredAcceleration");
            svi.addMemberVariable(&HeadBoardSensorValue::onBoardTemparature, "onBoardTemparature");
            svi.addMemberVariable(&HeadBoardSensorValue::mainUpdateRate, "mainUpdateRate");
            svi.addMemberVariable(&HeadBoardSensorValue::emergencyStopStatus,
                                  "emergencyStopStatus");
            svi.addMemberVariable(&HeadBoardSensorValue::aksim1ConnectedAtStartup,
                                  "aksim1ConnectedAtStartup");
            svi.addMemberVariable(&HeadBoardSensorValue::aksim2ConnectedAtStartup,
                                  "aksim2ConnectedAtStartup");
            svi.addMemberVariable(&HeadBoardSensorValue::activeAbsoluteEncoder,
                                  "activeAbsoluteEncoder");
            svi.addMemberVariable(&HeadBoardSensorValue::onBoardTemperatureError,
                                  "onBoardTemperatureError");
            svi.addMemberVariable(&HeadBoardSensorValue::externalTemperatureError,
                                  "externalTemperatureError");
            svi.addMemberVariable(&HeadBoardSensorValue::onBoardTemperatureLimitReached,
                                  "onBoardTemperatureLimitReached");
            svi.addMemberVariable(&HeadBoardSensorValue::externalTemperatureLimitReached,
                                  "externalTemperatureLimitReached");
            return svi;
        }
    };


    class Data : public armarx::control::ethercat::DataInterface
    {

    public:
        Data(HeadBoardDataConfig& config,
             SlaveOut* sensorOUT,
             SlaveIn* sensorIN);

        // AbstractData interface
    public:
        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration) override;

        void rtWriteTargetValues(const IceUtil::Time&, const IceUtil::Time&) override;

    public:
        float getAbsoluteEncoderAngleInRad() const;

        std::uint16_t getAbsoluteEncoderDetailedStatus() const;

        std::uint8_t getAbsoluteEncoderTemperature() const;

        std::uint32_t getAbsoluteEncoderTicks() const;

        std::uint8_t getAbsoluteEncoderCrc() const;

        bool isAbsoluteEncoderCrcValid() const;

        float getRelativeEncoderPositionInRad() const;

        std::int32_t getRelativeEncoderTicks() const;

        std::int8_t getOnBoardTemperature() const;

        std::int8_t getExternalTemperature() const;

        std::uint16_t getMainUpdateRate() const;

        bool getEmergencyStopStatus() const;

        bool getAksIm1ConnectedAtStartup() const;

        bool getAksIm2ConnectedAtStartup() const;

        std::uint8_t getActiveAbsoluteEncoder() const;

        bool getOnBoardTemperatureError() const;

        bool getExternalTemperatureError() const;

        bool getOnBoardTemperatureLimitReached() const;

        bool getExternalTemperatureLimitReached() const;

        double getVelocity() const;

        double getAcceleration() const;

        float getFilteredAcceleration() const;

        void setMotorPwmValue(std::int16_t value);

        std::int16_t getMotorPwmValue() const;

        void setLedRgbStripControl(std::array<std::uint16_t, 20> value);

        std::uint16_t* getLedRgbStripControlPtr() const;

    private:
        SlaveOut* outputs;
        SlaveIn* inputs;

        ErrorDecoder errorDecoder;

        armarx::control::ethercat::LinearConvertedValue<std::uint32_t> absoluteEncoderAngle;
        float sanitizedAbsoluteEncoderAngle;
        std::uint16_t absoluteEncoderDetailedStatus;
        std::uint8_t absoluteEncoderTemperature;
        std::uint8_t absoluteEncoderCrc;
        bool absoluteEncoderCrcValid;
        armarx::control::ethercat::LinearConvertedValue<std::int32_t> relativeEncoder;
        std::int8_t onBoardTemperature;
        std::int8_t externalTemperature;
        std::uint16_t mainUpdateRate;
        bool emergencyStopStatus;
        bool aksim1ConnectedAtStartup;
        bool aksim2ConnectedAtStartup;
        std::uint8_t activeAbsoluteEncoder;
        bool onBoardTemperatureError;
        bool externalTemperatureError;
        bool onBoardTemperatureLimitReached;
        bool externalTemperatureLimitReached;

        SlopeValue<float> velocity;
        armarx::control::rt_filters::RtAverageFilter velocityFilter;
        SlopeValue<double> acceleration;
        float filteredAcceleration;
        armarx::control::rt_filters::RtAverageFilter accelerationFilter;

        float relativeEncoderConversionFactor;
        const uint supRelativeEncoderTicks = 2 << 16;
        int numOverflowsRelativeEncoderTicks = 0;
        int lastRelativeEncoderTicksZone = -1;

        std::int16_t motorPwmValue = 0;
        std::array<std::uint16_t, 20> ledRgbStripControl;

        std::int8_t convertTemperature(std::uint16_t* raw) const;

        HeadBoardDataConfig& config;
    };
} // namespace devices::ethercat::head_board::armar7de
