#include "Data.h"

#include <algorithm>

// Simox
#include <SimoxUtility/math/periodic/periodic_clamp.h>
#include <VirtualRobot/Robot.h>

// armarx
#include <RobotAPI/libraries/core/math/MathUtils.h>


namespace devices::ethercat::head_board::armar7de
{
    Data::Data(HeadBoardDataConfig& config,
               SlaveOut* outputs,
               SlaveIn* inputs) :
        outputs(outputs),
        inputs(inputs),
        errorDecoder(outputs, inputs),
        velocityFilter(10),
        accelerationFilter(5),
        config(config)
    {
        ARMARX_CHECK_EXPRESSION(outputs);
        ARMARX_CHECK_EXPRESSION(inputs);

        absoluteEncoderAngle.init(&(outputs->absoluteEncoderValue),
                                  config.absoluteEncoder);
        relativeEncoder.init(&(outputs->relativeEncoderTicks),
                             config.relativeEncoder);
    }

    void
    Data::rtReadSensorValues(const IceUtil::Time& /*sensorValuesTimestamp*/,
                             const IceUtil::Time& timeSinceLastIteration)
    {
        absoluteEncoderAngle.read();
        sanitizedAbsoluteEncoderAngle = simox::math::periodic_clamp(
            absoluteEncoderAngle.value, -static_cast<float>(M_PI), static_cast<float>(M_PI));
        absoluteEncoderDetailedStatus = outputs->absoluteEncoderStatus;
        absoluteEncoderTemperature = outputs->absoluteEncoderTemp;
        absoluteEncoderCrc = outputs->absoluteEncoderCRC;
        uint8_t crc = errorDecoder.calcAbsoluteEncoderCrc();
        absoluteEncoderCrcValid = (crc == outputs->absoluteEncoderCRC);
        relativeEncoder.read();
        onBoardTemperature = convertTemperature(&(outputs->onBoardTemperatureRaw));
        externalTemperature = convertTemperature(&(outputs->externalTemperatureRaw));
        mainUpdateRate = outputs->mainUpdateRate;
        emergencyStopStatus = errorDecoder.getEmergencyStopStatus();
        aksim1ConnectedAtStartup = (0x4000 & outputs->statusBits) ? true : false;
        aksim2ConnectedAtStartup = (0x2000 & outputs->statusBits) ? true : false;
        activeAbsoluteEncoder = (0x1800 & outputs->statusBits) >> 11;
        onBoardTemperatureError = (0x0400 & outputs->statusBits) ? true : false;
        externalTemperatureError = (0x0200 & outputs->statusBits) ? true : false;
        onBoardTemperatureLimitReached = (0x0100 & outputs->statusBits) ? true : false;
        externalTemperatureLimitReached = (0x0080 & outputs->statusBits) ? true : false;

        velocity.read(getRelativeEncoderPositionInRad(), timeSinceLastIteration);
        velocity.value =
            static_cast<double>(velocityFilter.update(static_cast<float>(velocity.value)));
        acceleration.read(getVelocity(), timeSinceLastIteration);
        filteredAcceleration = accelerationFilter.update(static_cast<float>(acceleration.value));
    }

    void
    Data::rtWriteTargetValues(const IceUtil::Time&, const IceUtil::Time&)
    {
        inputs->motorPwmValue = motorPwmValue;
        std::copy(std::begin(ledRgbStripControl),
                  std::end(ledRgbStripControl),
                  inputs->ledRgbStripControl);
    }

    float
    Data::getAbsoluteEncoderAngleInRad() const
    {
        return sanitizedAbsoluteEncoderAngle;
    }

    std::uint16_t
    Data::getAbsoluteEncoderDetailedStatus() const
    {
        return absoluteEncoderDetailedStatus;
    }

    std::uint8_t
    Data::getAbsoluteEncoderTemperature() const
    {
        return absoluteEncoderTemperature;
    }

    std::uint32_t
    Data::getAbsoluteEncoderTicks() const
    {
        return absoluteEncoderAngle.getRaw();
    }

    std::uint8_t
    Data::getAbsoluteEncoderCrc() const
    {
        return absoluteEncoderCrc;
    }

    bool
    Data::isAbsoluteEncoderCrcValid() const
    {
        return absoluteEncoderCrcValid;
    }

    float
    Data::getRelativeEncoderPositionInRad() const
    {
        return relativeEncoder.value;
    }

    std::int32_t
    Data::getRelativeEncoderTicks() const
    {
        return relativeEncoder.getRaw();
    }

    std::int8_t
    Data::getOnBoardTemperature() const
    {
        return onBoardTemperature;
    }

    std::int8_t
    Data::getExternalTemperature() const
    {
        return externalTemperature;
    }

    std::uint16_t
    Data::getMainUpdateRate() const
    {
        return mainUpdateRate;
    }

    bool
    Data::getEmergencyStopStatus() const
    {
        return emergencyStopStatus;
    }

    bool
    Data::getAksIm1ConnectedAtStartup() const
    {
        return aksim1ConnectedAtStartup;
    }

    bool
    Data::getAksIm2ConnectedAtStartup() const
    {
        return aksim2ConnectedAtStartup;
    }

    std::uint8_t
    Data::getActiveAbsoluteEncoder() const
    {
        return activeAbsoluteEncoder;
    }

    bool
    Data::getOnBoardTemperatureError() const
    {
        return onBoardTemperatureError;
    }

    bool
    Data::getExternalTemperatureError() const
    {
        return externalTemperatureError;
    }

    bool
    Data::getOnBoardTemperatureLimitReached() const
    {
        return onBoardTemperatureLimitReached;
    }

    bool
    Data::getExternalTemperatureLimitReached() const
    {
        return externalTemperatureLimitReached;
    }

    double
    Data::getVelocity() const
    {
        return velocity.value;
    }

    double
    Data::getAcceleration() const
    {
        return acceleration.value;
    }

    float
    Data::getFilteredAcceleration() const
    {
        return filteredAcceleration;
    }

    void
    Data::setMotorPwmValue(std::int16_t value)
    {
        motorPwmValue = config.motorDirection * std::clamp(value,
                                                    static_cast<std::int16_t>(-config.maxPwm),
                                                    static_cast<std::int16_t>(config.maxPwm));
    }

    std::int16_t
    Data::getMotorPwmValue() const
    {
        return config.motorDirection * motorPwmValue;
    }

    void
    Data::setLedRgbStripControl(std::array<std::uint16_t, 20> value)
    {
        ledRgbStripControl = value;
    }

    std::uint16_t*
    Data::getLedRgbStripControlPtr() const
    {
        return &(inputs->ledRgbStripControl[0]);
    }

    std::int8_t
    Data::convertTemperature(std::uint16_t* raw) const
    {
        std::uint8_t upper = *raw >> 8;
        std::uint8_t lower = *raw & 0xff;

        if ((upper & 0x10) == 0x10) // Temperature < 0 degrees Celcius
        {
            upper &= 0x0f;
            return static_cast<std::int8_t>(-(256 - (static_cast<std::int16_t>(upper * 16) +
                                                     static_cast<std::int16_t>(lower / 16))));
        }
        else // Temperature >= 0 degrees Celcius
        {
            return static_cast<std::int8_t>((upper * 16) + (lower / 16));
        }
    }

} // namespace devices::ethercat::head_board::armar7de
