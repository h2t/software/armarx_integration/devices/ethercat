#pragma once


// STD/STL
#include <cstdint>


namespace devices::ethercat::head_board::armar7de
{

    /**
     * @brief PDO mapping motorB->master
     */
    struct SlaveOut
    {
        // Absolute Encoder
        std::uint32_t absoluteEncoderValue;
        std::uint16_t absoluteEncoderStatus;
        std::uint8_t absoluteEncoderTemp;
        std::uint8_t absoluteEncoderCRC;

        // Relative Encoder
        std::int32_t relativeEncoderTicks;

        // Temperature
        std::uint16_t onBoardTemperatureRaw;
        std::uint16_t externalTemperatureRaw;

        // Board Diagnostic
        std::uint16_t mainUpdateRate;
        std::uint16_t statusBits;

    } __attribute__((__packed__));


    /**
     * @brief PDO mapping master->motorB
     */
    struct SlaveIn
    {
        // Motor PWM
        std::int16_t motorPwmValue;
        std::uint16_t pad1;

        // LED RGB Strip Control
        std::uint16_t ledRgbStripControl[20];
    } __attribute__((__packed__));

} // namespace devices::ethercat::head_board::armar7de
