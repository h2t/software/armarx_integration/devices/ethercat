/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <armarx/control/ethercat/SlaveInterface.h>

#include <devices/ethercat/head_board/armar7de/ErrorDecoder.h>
#include <devices/ethercat/head_board/armar7de/SlaveIO.h>
#include <devices/ethercat/head_board/armar7de/Config.h>


namespace devices::ethercat::head_board::armar7de
{

    class Slave : public armarx::control::ethercat::SlaveInterfaceWithIO<SlaveIn, SlaveOut>
    {

    public:
        Slave(const armarx::control::ethercat::SlaveIdentifier slaveIdentifier);

        // AbstractSlave interface

        static std::string getDefaultName();

        static bool
        isSlaveIdentifierAccepted(const armarx::control::ethercat::SlaveIdentifier& sid);

    public:
        void doMappings() override;
        bool prepareForRun() override;
        void execute() override;
        bool shutdown() override;
        void prepareForSafeOp() override;
        void prepareForOp() override;
        bool hasError() override;

        bool isEmergencyStopActive() const override;
        bool recoverFromEmergencyStop() override;
        
        /**
         * @brief Set the Config object containing the coe config
         * The values are set in slave as soon as it switches from pre-op to safe-op
         */
        void setConfig(HeadBoardConfig& config);

        void setIsExternalTemperatureSensorConnected(bool value);

    private:
        armarx::control::ethercat::Bus& bus;
        ErrorDecoder errorDecoder{nullptr, nullptr};

        // object is stack-allocated in Device class
        HeadBoardConfig* config = nullptr;

        void writeCoE();
    };

} // namespace devices::ethercat::head_board::armar7de
