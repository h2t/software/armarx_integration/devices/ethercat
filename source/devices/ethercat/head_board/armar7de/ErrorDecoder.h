#pragma once

#include <armarx/control/ethercat/SlaveInterface.h>

#include <devices/ethercat/head_board/armar7de/SlaveIO.h>

namespace devices::ethercat::head_board::armar7de
{
    enum AbsoluteEncoderStatus : std::uint16_t
    {
        // clang-format off
        ErrorMultiturnCounterMismatch =         0b1000000000000000,
        ErrorSignalAmplitudeTooHigh =           0b0100000000000000,
        WarningSignalAmplitudeTooHigh =         0b0010000000000000,
        ErrorMagneticSensor =                   0b0001000000000000,
        ErrorSensorReadingError =               0b0000100000000000,
        ErrorEncoderNotConfiguredProperly =     0b0000010000000000,
        ErrorPositionNotValid =                 0b0000001000000000,
        WarningEncoderNearOperationalLimits =   0b0000000100000000,
        WarningSignalAmplitudeTooHigh2 =        0b0000000010000000,
        WarningSignalAmplitudeLow =             0b0000000001000000,
        ErrorSignalLost =                       0b0000000000100000,
        WarningTemperature =                    0b0000000000010000,
        ErrorPowerSupplyError =                 0b0000000000001000,
        ErrorSystemError =                      0b0000000000000100,
        ErrorMagneticPatternError =             0b0000000000000010,
        ErrorAccelerationError =                0b0000000000000001
        // clang-format on
    };

    class ErrorDecoder
    {
    public:
        ErrorDecoder(SlaveOut* outputs, SlaveIn* inputs);

        std::uint16_t getAbsoluteEncoderStatus() const;

        bool absoluteEncoderErrorDetected(armarx::control::ethercat::SlaveInterface* slave,
                                          bool printError = false) const;

        bool getEmergencyStopStatus() const;

        std::uint8_t calcAbsoluteEncoderCrc() const;

    private:
        SlaveOut* outputs;
        SlaveIn* inputs;

        std::uint8_t crc8_4B(std::uint32_t bb) const;
    };
} // namespace devices::ethercat::head_board::armar7de
