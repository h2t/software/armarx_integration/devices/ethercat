/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Slave.h"

#include <ethercat.h>

#include <armarx/control/ethercat/Bus.h>

#include <devices/ethercat/common/h2t_devices.h>


namespace devices::ethercat::head_board::armar7de
{

    Slave::Slave(const armarx::control::ethercat::SlaveIdentifier slaveIdentifier) :
        SlaveInterfaceWithIO(slaveIdentifier), bus(armarx::control::ethercat::Bus::getBus())
    {
        setTag(this->slaveIdentifier.getName());
    }


    void
    Slave::doMappings()
    {
    }


    bool
    Slave::prepareForRun()
    {
        return true;
    }


    void
    Slave::execute()
    {
    }


    bool
    Slave::shutdown()
    {
        return true;
    }

    void
    Slave::prepareForSafeOp()
    {
        writeCoE();
    }

    void
    Slave::prepareForOp()
    {
        errorDecoder = ErrorDecoder(getOutputsPtr(), getInputsPtr());
    }


    bool
    Slave::hasError()
    {
        bool hasError = false;
        hasError |= errorDecoder.absoluteEncoderErrorDetected(this, true);

        if (((outputs->statusBits & 0x1800) >> 11) ==
            1) // If AksIM-1 is the active absolute encoder
        {
            std::uint8_t crc = errorDecoder.calcAbsoluteEncoderCrc();
            if (crc != outputs->absoluteEncoderCRC)
            {
                SLAVE_WARNING(getSlaveIdentifier(),
                              "Absolute Encoder CRC failed. Expected: %#02X, but got: %#02X",
                              outputs->absoluteEncoderCRC,
                              crc)
                    .deactivateSpam(1);
                hasError = true;
            }
        }

        if (((outputs->statusBits & 0x1800) >> 11) == 0)
        {
            SLAVE_WARNING(getSlaveIdentifier(), "No absolute encoder active").deactivateSpam(1);
            hasError = true;
        }

        if (outputs->statusBits & 0x0400)
        {
            SLAVE_WARNING(getSlaveIdentifier(), "On-Board Temperature Error").deactivateSpam(1);
            hasError = true;
        }

        if (config->isExternalTemperatureSensorConnected && (outputs->statusBits & 0x0200))
        {
            SLAVE_WARNING(getSlaveIdentifier(), "External Temperature Error").deactivateSpam(1);
            hasError = true;
        }

        if (outputs->statusBits & 0x0100)
        {
            SLAVE_ERROR(getSlaveIdentifier(), "On-Board Temperature Limit reached")
                .deactivateSpam(1);
            hasError = true;
        }

        if (config->isExternalTemperatureSensorConnected && (outputs->statusBits & 0x0080))
        {
            SLAVE_ERROR(getSlaveIdentifier(), "External Temperature Limit reached")
                .deactivateSpam(1);
            hasError = true;
        }

        return hasError;
    }

    bool
    Slave::isEmergencyStopActive() const
    {
        return errorDecoder.getEmergencyStopStatus();
    }

    bool
    Slave::recoverFromEmergencyStop()
    {
        return true;
    }


    std::string
    Slave::getDefaultName()
    {
        return "HeadBoardArmar7de";
    }


    bool
    Slave::isSlaveIdentifierAccepted(const armarx::control::ethercat::SlaveIdentifier& sid)
    {
        return sid.vendorID == devices::ethercat::common::H2TVendorId and sid.productCode == 8193;
    }

    void
    Slave::setConfig(HeadBoardConfig& config)
    {
        this->config = &config;
    }

    void
    Slave::writeCoE()
    {
        if (!config)
            return;

        if (!bus.writeSDOEntry(getSlaveNumber(), 0x8000, 1, config->alwaysUseAksIm1))
        {
            SLAVE_ERROR(getSlaveIdentifier(), "Could not write SDO Entry Always Use AksIM_1");
        }

        if (!bus.writeSDOEntry(getSlaveNumber(), 0x8000, 2, config->onBoardTemperatureLimit))
        {
            SLAVE_ERROR(getSlaveIdentifier(),
                        "Could not write SDO Entry On-Board Temperature Limit");
        }

        if (!bus.writeSDOEntry(getSlaveNumber(), 0x8000, 3, config->externalTemperatureLimit))
        {
            SLAVE_ERROR(getSlaveIdentifier(),
                        "Could not write SDO Entry External Temperature Limit");
        }

        if (!bus.writeSDOEntry(getSlaveNumber(), 0x8000, 4, config->ledStripCount))
        {
            SLAVE_ERROR(getSlaveIdentifier(), "Could not write SDO Entry LED RGB Strip Count");
        }

        if (!bus.writeSDOEntry(getSlaveNumber(), 0x8000, 5, config->ledStripGlobalBrightness))
        {
            SLAVE_ERROR(getSlaveIdentifier(),
                        "Could not write SDO Entry LED RGB Strip Global Brightness");
        }
    }

} // namespace devices::ethercat::head_board::armar7de
