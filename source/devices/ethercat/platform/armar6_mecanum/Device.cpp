/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Markus Swarowsky (markus dot swarowsky at student dot kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Device.h"

#include <ArmarXCore/core/logging/Logging.h>


namespace devices::ethercat::platform::armar6_mecanum
{

    Device::Device(hwconfig::DeviceConfig& hwConfig,
                   const VirtualRobot::RobotPtr&) :
        DeviceBase(hwConfig.getName()),
        ControlDevice(hwConfig.getName()),
        SensorDevice(hwConfig.getName()),
        DeviceInterface(hwConfig.getName()),
        dataPtr(nullptr),
        frontRightElmoSerial(
            hwConfig.getSlaveConfig("Elmo", "FrontRight").getIdentifier().getSerialNumber()),
        frontLeftElmoSerial(
            hwConfig.getSlaveConfig("Elmo", "FrontLeft").getIdentifier().getSerialNumber()),
        rearRightElmoSerial(
            hwConfig.getSlaveConfig("Elmo", "RearRight").getIdentifier().getSerialNumber()),
        rearLeftElmoSerial(
            hwConfig.getSlaveConfig("Elmo", "RearLeft").getIdentifier().getSerialNumber()),
        frontRightConfig(hwConfig.getSlaveConfig("Elmo", "FrontRight")),
        frontLeftConfig(hwConfig.getSlaveConfig("Elmo", "FrontLeft")),
        rearRightConfig(hwConfig.getSlaveConfig("Elmo", "RearRight")),
        rearLeftConfig(hwConfig.getSlaveConfig("Elmo", "RearLeft")),
        frontRightElmo(nullptr),
        frontLeftElmo(nullptr),
        rearRightElmo(nullptr),
        rearLeftElmo(nullptr)
    {
        platformControllerConfig = armarx::control::joint_controller::HolonomicPlatformControllerConfiguration::
            CreateHolonomicPlatformControllerConfigData(hwConfig.getControllerConfig("Platform"));
    }

    void
    Device::postSwitchToSafeOp()
    {
        if (!dataPtr)
        {
            //creating the data object
            dataPtr = std::make_shared<Data>(frontRightConfig,
                                             frontLeftConfig,
                                             rearRightConfig,
                                             rearLeftConfig,
                                             frontRightElmo->getOutputsPtr(),
                                             frontRightElmo->getInputsPtr(),
                                             frontLeftElmo->getOutputsPtr(),
                                             frontLeftElmo->getInputsPtr(),
                                             rearRightElmo->getOutputsPtr(),
                                             rearRightElmo->getInputsPtr(),
                                             rearLeftElmo->getOutputsPtr(),
                                             rearLeftElmo->getInputsPtr());

            //creating the joint controller
            velocityControllerPtr = std::make_shared<joint_controller::VelocityController>(
                dataPtr,platformControllerConfig);
            emergencyControllerPtr =
                std::make_shared<joint_controller::EmergencyController>(dataPtr);
            stopMovementControllerPtr =
                std::make_shared<joint_controller::StopMovementController>(dataPtr);

            addJointController(velocityControllerPtr.get());
            addJointController(emergencyControllerPtr.get());
            addJointController(stopMovementControllerPtr.get());
        }
    }

    void
    Device::postSwitchToOp()
    {
        //set the elmos initial to velocity mode, so platform don't moves at startup
        frontRightElmo->switchMode(common::elmo::gold::ElmoControlMode::VELOCITY);
        frontLeftElmo->switchMode(common::elmo::gold::ElmoControlMode::VELOCITY);
        rearRightElmo->switchMode(common::elmo::gold::ElmoControlMode::VELOCITY);
        rearLeftElmo->switchMode(common::elmo::gold::ElmoControlMode::VELOCITY);
    }


    const armarx::SensorValueBase*
    Device::getSensorValue() const
    {
        return &sensorValue;
    }


    void
    Device::rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                               const IceUtil::Time& timeSinceLastIteration)
    {
        if (dataPtr == nullptr)
        {
            DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                   "Holonmic platform has no data set, switch to SafeOP before");
        }

        dataPtr->rtReadSensorValues(sensorValuesTimestamp, timeSinceLastIteration);

        using Wheel = SensorValueHolonomicPlatformWithWheels;
        sensorValue.wheelActualCurrents(Wheel::FrontRight) =
            dataPtr->getFrontRightActualCurrentValue();
        sensorValue.wheelActualCurrents(Wheel::FrontLeft) =
            dataPtr->getFrontLeftActualCurrentValue();
        sensorValue.wheelActualCurrents(Wheel::RearRight) =
            dataPtr->getRearRightActualCurrentValue();
        sensorValue.wheelActualCurrents(Wheel::RearLeft) = dataPtr->getRearLeftActualCurrentValue();

        sensorValue.wheelActualVelocities(Wheel::FrontRight) =
            dataPtr->getFrontRightActualVelocityValue();
        sensorValue.wheelActualVelocities(Wheel::FrontLeft) =
            dataPtr->getFrontLeftActualVelocityValue();
        sensorValue.wheelActualVelocities(Wheel::RearRight) =
            dataPtr->getRearRightActualVelocityValue();
        sensorValue.wheelActualVelocities(Wheel::RearLeft) =
            dataPtr->getRearLeftActualVelocityValue();

        sensorValue.wheelTargetVelocities(Wheel::FrontRight) =
            dataPtr->getFrontRightTargetVelocity();
        sensorValue.wheelTargetVelocities(Wheel::FrontLeft) = dataPtr->getFrontLeftTargetVelocity();
        sensorValue.wheelTargetVelocities(Wheel::RearRight) = dataPtr->getRearRightTargetVelocity();
        sensorValue.wheelTargetVelocities(Wheel::RearLeft) = dataPtr->getRearLeftTargetVelocity();

        Eigen::Vector3f cartesianVelocities = mecanumPlatform.calcCartesianVelocity(
            Eigen::Vector4f(sensorValue.wheelActualVelocities(Wheel::FrontLeft),
                            sensorValue.wheelActualVelocities(Wheel::FrontRight),
                            sensorValue.wheelActualVelocities(Wheel::RearLeft),
                            sensorValue.wheelActualVelocities(Wheel::RearRight)));

        sensorValue.velocityX = cartesianVelocities(0);
        sensorValue.velocityY = cartesianVelocities(1);
        sensorValue.velocityRotation = cartesianVelocities(2);

        auto deltaSeconds = timeSinceLastIteration.toSecondsDouble();
        Eigen::Vector2f platformPosition(sensorValue.velocityX * deltaSeconds,
                                         sensorValue.velocityY * deltaSeconds);
        sensorValue.relativePositionRotation += sensorValue.velocityRotation * deltaSeconds;
        Eigen::Rotation2Df rot(sensorValue.relativePositionRotation);
        Eigen::Vector2f integratedPosition = rot * platformPosition;
        sensorValue.relativePositionX += integratedPosition(0);
        sensorValue.relativePositionY += integratedPosition(1);
    }


    void
    Device::rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration)
    {
        if (dataPtr == nullptr)
        {
            DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                   "Holonmic platform has no data set, switch to SafeOP before");
        }

        dataPtr->rtWriteTargetValues(sensorValuesTimestamp, timeSinceLastIteration);
    }


    armarx::control::ethercat::DeviceInterface::TryAssignResult
    Device::tryAssign(armarx::control::ethercat::SlaveInterface& slave)
    {
        namespace ecat = armarx::control::ethercat;
        using Result = DeviceInterface::TryAssignResult;

        Result result = ecat::tryAssignUsingSerialNo(slave, frontLeftElmo, frontLeftElmoSerial);
        if (result != Result::unknown)
        {
            return result;
        }

        result = ecat::tryAssignUsingSerialNo(slave, frontRightElmo, frontRightElmoSerial);
        if (result != Result::unknown)
        {
            return result;
        }

        result = ecat::tryAssignUsingSerialNo(slave, rearLeftElmo, rearLeftElmoSerial);
        if (result != Result::unknown)
        {
            return result;
        }

        result = ecat::tryAssignUsingSerialNo(slave, rearRightElmo, rearRightElmoSerial);
        if (result != Result::unknown)
        {
            return result;
        }

        return result;
    }

    armarx::control::ethercat::DeviceInterface::AllAssignedResult
    Device::onAllAssigned()
    {
        using Result = DeviceInterface::AllAssignedResult;

        if (frontLeftElmo and frontRightElmo and rearLeftElmo and rearRightElmo)
        {
            //setting the configuration parameter for the elmos
            frontRightElmo->setConfiguration(frontRightConfig);
            frontLeftElmo->setConfiguration(frontLeftConfig);
            rearRightElmo->setConfiguration(rearRightConfig);
            rearLeftElmo->setConfiguration(rearLeftConfig);

            return Result::ok;
        }

        return Result::slavesMissing;
    }

    std::string
    Device::getClassName() const
    {
        return "MecanumPlatform_Armar6";
    }


    std::vector<std::uint32_t>
    Device::getElmoSerials() const
    {
        return {frontRightElmoSerial, frontLeftElmoSerial, rearRightElmoSerial, rearLeftElmoSerial};
    }


    const DataPtr&
    Device::getDataPtr() const
    {
        return dataPtr;
    }


    std::uint16_t
    Device::getFrontRightElmoSlaveNumber() const
    {
        return frontRightElmo->getSlaveNumber();
    }


    std::uint16_t
    Device::getFrontLeftElmoSlaveNumber() const
    {
        return frontLeftElmo->getSlaveNumber();
    }


    std::uint16_t
    Device::getRearRightElmoSlaveNumber() const
    {
        return rearRightElmo->getSlaveNumber();
    }


    std::uint16_t
    Device::getRearLeftElmoSlaveNumber() const
    {
        return rearLeftElmo->getSlaveNumber();
    }


    std::string
    Device::getWheelName(std::uint32_t elmoSerial) const
    {
        if (elmoSerial == frontRightElmoSerial)
        {
            return "FrontRight";
        }

        if (elmoSerial == frontLeftElmoSerial)
        {
            return "FrontLeft";
        }

        if (elmoSerial == rearRightElmoSerial)
        {
            return "RearRight";
        }

        if (elmoSerial == rearLeftElmoSerial)
        {
            return "RearLeft";
        }

        DEVICE_FATAL_AND_THROW(
            rtGetDeviceName(), "No wheel wither serial number %u exists", elmoSerial);
    }

} // namespace devices::ethercat::platform::armar6_mecanum
