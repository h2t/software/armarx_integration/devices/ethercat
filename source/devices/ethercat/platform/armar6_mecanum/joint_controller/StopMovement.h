#pragma once


// armarx
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>

// robot_devices
#include <devices/ethercat/platform/armar6_mecanum/Data.h>


namespace devices::ethercat::platform::armar6_mecanum::joint_controller
{

    using StopMovementControllerPtr = std::shared_ptr<class StopMovementController>;

    class StopMovementController : public armarx::JointController
    {

    public:

        StopMovementController(DataPtr holoDataPtr);

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

        armarx::ControlTargetBase* getControlTarget() override;

    private:

        armarx::DummyControlTargetStopMovement target;
        DataPtr dataPtr;

    };

}  // namespace devices::ethercat::platform::armar6_mecanum::joint_controller
