/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Raphael Grimm (raphael dot grimm at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// STD/STL
#include <memory>

// armarx
#include <armarx/control/joint_controller/ControllerConfiguration.h>
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTargetHolonomicPlatformVelocity.h>

// robot_devices
#include <devices/ethercat/platform/armar6_mecanum/Armar6MecanumPlatform.h>
#include <devices/ethercat/platform/armar6_mecanum/Data.h>


namespace devices::ethercat::platform::armar6_mecanum::joint_controller
{

    using VelocityControllerPtr = std::shared_ptr<class VelocityController>;


    class LinearLimitedAccelerationController
    {

    private:

        float maxVelocity{0.F};
        float maxAcceleration{0.F};
        float maxDeceleration{0.F};
        float maxDeltaT{0.F};
        float currentValue{0.F};

    public:

        LinearLimitedAccelerationController(float maxVelocity, float maxAcceleration, float maxDeceleration, float maxDeltaT, float value)
            : maxVelocity(maxVelocity), maxAcceleration(maxAcceleration), maxDeceleration(maxDeceleration), maxDeltaT(maxDeltaT), currentValue(value)
        { }

        LinearLimitedAccelerationController() = default;

        void init(float maxVelocity, float maxAcceleration, float maxDeceleration, float maxDeltaT, float value);

        float update(float value, float dt);

    };


    class VelocityController : public armarx::JointController
    {

    public:

        VelocityController(DataPtr holoDataPtr, armarx::control::joint_controller::HolonomicPlatformControllerConfigurationPtr configDataPtr);

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

        armarx::ControlTargetBase* getControlTarget() override;

    private:

        armarx::ControlTargetHolonomicPlatformVelocity target;
        DataPtr dataPtr;

        Armar6MecanumPlatform mecanumPlatform;

        LinearLimitedAccelerationController limitXVelocityController;
        LinearLimitedAccelerationController limitYVelocityController;
        LinearLimitedAccelerationController limitAngelVelocityController;

    };

}  // namespace devices::ethercat::platform::armar6_mecanum::joint_controller
