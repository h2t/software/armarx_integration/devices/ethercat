#include "StopMovement.h"


namespace devices::ethercat::platform::armar6_mecanum::joint_controller
{

    StopMovementController::StopMovementController(DataPtr holoDataPtr) :
        dataPtr(holoDataPtr)
    {

    }

    void StopMovementController::rtRun(const IceUtil::Time& sensorValuesTimestamp,
                                       const IceUtil::Time& timeSinceLastIteration)
    {
        dataPtr->setFrontRightTargetVelocity(0.f);
        dataPtr->setFrontLeftTargetVelocity(0.f);
        dataPtr->setRearRightTargetVelocity(0.f);
        dataPtr->setRearLeftTargetVelocity(0.f);
    }

    armarx::ControlTargetBase* StopMovementController::getControlTarget()
    {
        return &target;
    }

}
