/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Markus Swarowsky (markus dot swarowsky at student dot kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <cstdint>

#include <VirtualRobot/Robot.h>

#include <RobotAPI/components/units/RobotUnit/Devices/ControlDevice.h>
#include <RobotAPI/components/units/RobotUnit/Devices/SensorDevice.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValueHolonomicPlatform.h>
#include <armarx/control/ethercat/DeviceInterface.h>

#include <devices/ethercat/common/elmo/gold/Slave.h>
#include <devices/ethercat/common/elmo/gold/Config.h>
#include <devices/ethercat/platform/armar6_mecanum/Armar6MecanumPlatform.h>
#include <devices/ethercat/platform/armar6_mecanum/Data.h>
#include <devices/ethercat/platform/armar6_mecanum/joint_controller/Emergency.h>
#include <devices/ethercat/platform/armar6_mecanum/joint_controller/StopMovement.h>
#include <devices/ethercat/platform/armar6_mecanum/joint_controller/Velocity.h>


namespace devices::ethercat::platform::armar6_mecanum
{
    namespace hwconfig = armarx::control::hardware_config;

    class SensorValueHolonomicPlatformWithWheels :
        virtual public armarx::SensorValueHolonomicPlatform
    {

    public:
        enum WheelIndex
        {
            FrontRight,
            FrontLeft,
            RearRight,
            RearLeft,
            WheelCount
        };

        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION

        Eigen::Vector4f wheelActualCurrents = Eigen::Vector4f::Zero();
        Eigen::Vector4f wheelActualVelocities = Eigen::Vector4f::Zero();
        Eigen::Vector4f wheelTargetVelocities = Eigen::Vector4f::Zero();

        static SensorValueInfo<SensorValueHolonomicPlatformWithWheels>
        GetClassMemberInfo()
        {
            SensorValueInfo<SensorValueHolonomicPlatformWithWheels> svi;
            svi.addBaseClass<SensorValueHolonomicPlatform>();
            svi.addMemberVariable(&SensorValueHolonomicPlatformWithWheels::wheelActualCurrents,
                                  "wheelActualCurrents")
                .setFieldNames({"FrontRightActualCurrent",
                                "FrontLeftActualCurrent",
                                "RearRightActualCurrent",
                                "RearLeftActualCurrent"})
                .setVariantReportFunction(
                    [](const IceUtil::Time& timestamp,
                       const SensorValueHolonomicPlatformWithWheels* ptr)
                    {
                        return std::map<std::string, VariantBasePtr>{
                            {"FrontRightActualCurrent",
                             new armarx::TimedVariant(ptr->wheelActualCurrents(FrontRight),
                                                      timestamp)},
                            {"FrontLeftActualCurrent",
                             new armarx::TimedVariant(ptr->wheelActualCurrents(FrontLeft),
                                                      timestamp)},
                            {"RearRightActualCurrent",
                             new armarx::TimedVariant(ptr->wheelActualCurrents(RearRight),
                                                      timestamp)},
                            {"RearLeftActualCurrent",
                             new armarx::TimedVariant(ptr->wheelActualCurrents(RearLeft),
                                                      timestamp)},
                        };
                    });

            svi.addMemberVariable(&SensorValueHolonomicPlatformWithWheels::wheelActualVelocities,
                                  "wheelActualVelocities")
                .setFieldNames({"FrontRightActualVelocity",
                                "FrontLeftActualVelocity",
                                "RearRightActualVelocity",
                                "RearLeftActualVelocity"})
                .setVariantReportFunction(
                    [](const IceUtil::Time& timestamp,
                       const SensorValueHolonomicPlatformWithWheels* ptr)
                    {
                        return std::map<std::string, VariantBasePtr>{
                            {"FrontRightActualVelocity",
                             new armarx::TimedVariant(ptr->wheelActualVelocities(FrontRight),
                                                      timestamp)},
                            {"FrontLeftActualVelocity",
                             new armarx::TimedVariant(ptr->wheelActualVelocities(FrontLeft),
                                                      timestamp)},
                            {"RearRightActualVelocity",
                             new armarx::TimedVariant(ptr->wheelActualVelocities(RearRight),
                                                      timestamp)},
                            {"RearLeftActualVelocity",
                             new armarx::TimedVariant(ptr->wheelActualVelocities(RearLeft),
                                                      timestamp)},
                        };
                    });

            svi.addMemberVariable(&SensorValueHolonomicPlatformWithWheels::wheelTargetVelocities,
                                  "wheelTargetVelocities")
                .setFieldNames({"FrontRightTargetVelocity",
                                "FrontLeftTargetVelocity",
                                "RearRightTargetVelocity",
                                "RearLeftTargetVelocity"})
                .setVariantReportFunction(
                    [](const IceUtil::Time& timestamp,
                       const SensorValueHolonomicPlatformWithWheels* ptr)
                    {
                        return std::map<std::string, VariantBasePtr>{
                            {"FrontRightTargetVelocity",
                             new armarx::TimedVariant(ptr->wheelTargetVelocities(FrontRight),
                                                      timestamp)},
                            {"FrontLeftTargetVelocity",
                             new armarx::TimedVariant(ptr->wheelTargetVelocities(FrontLeft),
                                                      timestamp)},
                            {"RearRightTargetVelocity",
                             new armarx::TimedVariant(ptr->wheelTargetVelocities(RearRight),
                                                      timestamp)},
                            {"RearLeftTargetVelocity",
                             new armarx::TimedVariant(ptr->wheelTargetVelocities(RearLeft),
                                                      timestamp)},
                        };
                    });
            return svi;
        }
    };


    class Device :
        public armarx::ControlDevice,
        public armarx::SensorDevice,
        public armarx::control::ethercat::DeviceInterface
    {

    public:
        Device(hwconfig::DeviceConfig& deviceConfig,
               const VirtualRobot::RobotPtr& robot);

        // DeviceInterface interface
        void postSwitchToSafeOp() override;
        void postSwitchToOp() override;
        TryAssignResult tryAssign(armarx::control::ethercat::SlaveInterface& slave) override;
        AllAssignedResult onAllAssigned() override;
        std::string getClassName() const override;

        // SensorDevice interface
        const armarx::SensorValueBase* getSensorValue() const override;
        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration) override;

        // ControlDevice interface
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                 const IceUtil::Time& timeSinceLastIteration) override;


        std::vector<std::uint32_t> getElmoSerials() const;

        const DataPtr& getDataPtr() const;

        std::uint16_t getFrontRightElmoSlaveNumber() const;
        std::uint16_t getFrontLeftElmoSlaveNumber() const;
        std::uint16_t getRearRightElmoSlaveNumber() const;
        std::uint16_t getRearLeftElmoSlaveNumber() const;

        std::string getWheelName(std::uint32_t elmoSerial) const;

    private:
        // The date and target object.
        DataPtr dataPtr;
        SensorValueHolonomicPlatformWithWheels sensorValue;

        Armar6MecanumPlatform mecanumPlatform;

        // The serials for the four elmos of the platform.
        const std::uint32_t frontRightElmoSerial;
        const std::uint32_t frontLeftElmoSerial;
        const std::uint32_t rearRightElmoSerial;
        const std::uint32_t rearLeftElmoSerial;

        // Elmo configuration
        const common::elmo::gold::ElmoVelocityConfig frontRightConfig, frontLeftConfig, rearRightConfig, rearLeftConfig;

        // All elmo pointers.
        common::elmo::gold::Slave* frontRightElmo;
        common::elmo::gold::Slave* frontLeftElmo;
        common::elmo::gold::Slave* rearRightElmo;
        common::elmo::gold::Slave* rearLeftElmo;

        armarx::control::joint_controller::HolonomicPlatformControllerConfigurationPtr platformControllerConfig;

        // Joint controller.
        joint_controller::VelocityControllerPtr velocityControllerPtr;
        joint_controller::EmergencyControllerPtr emergencyControllerPtr;
        joint_controller::StopMovementControllerPtr stopMovementControllerPtr;
    };

} // namespace devices::ethercat::platform::armar6_mecanum
