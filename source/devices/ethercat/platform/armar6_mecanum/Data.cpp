/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2017, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Markus Swarowsky (markus dot swarowsky at student dot kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Data.h"


namespace devices::ethercat::platform::armar6_mecanum
{

    Data::Data(const common::elmo::gold::ElmoVelocityDataConfig& frontRightConfig,
               const common::elmo::gold::ElmoVelocityDataConfig& frontLeftConfig,
               const common::elmo::gold::ElmoVelocityDataConfig& rearRightConfig,
               const common::elmo::gold::ElmoVelocityDataConfig& rearLeftConfig,
               common::elmo::gold::SlaveOut* frontRight_elmo_out, common::elmo::gold::SlaveIn* frontRight_elmo_in,
               common::elmo::gold::SlaveOut* frontLeft_elmo_out, common::elmo::gold::SlaveIn* frontLeft_elmo_in,
               common::elmo::gold::SlaveOut* rearRight_elmo_out, common::elmo::gold::SlaveIn* rearRight_elmo_in,
               common::elmo::gold::SlaveOut* rearLeft_elmo_out, common::elmo::gold::SlaveIn* rearLeft_elmo_in)
    {
        //sensor values
        frontRightVelocityValue.init(&frontRight_elmo_out->velocityActualValue, frontRightConfig.velocity);
        frontRightCurrentValue.init(&frontRight_elmo_out->currentActualValue, frontRightConfig.currentValue);

        frontLeftVelocityValue.init(&frontLeft_elmo_out->velocityActualValue, frontLeftConfig.velocity);
        frontLeftCurrentValue.init(&frontLeft_elmo_out->currentActualValue, frontLeftConfig.currentValue);

        rearRightVelocityValue.init(&rearRight_elmo_out->velocityActualValue, rearRightConfig.velocity);
        rearRightCurrentValue.init(&rearRight_elmo_out->currentActualValue, rearRightConfig.currentValue);

        rearLeftVelocityValue.init(&rearLeft_elmo_out->velocityActualValue, rearLeftConfig.velocity);
        rearLeftCurrentValue.init(&rearLeft_elmo_out->currentActualValue, rearLeftConfig.currentValue);

        //targets
        frontRightTargetVelocity.init(&frontRight_elmo_in->targetVelocity, frontRightConfig.targetVelocity);
        frontRightTargetAcceleration.init(&frontRight_elmo_in->profiledAcceleration, frontRightConfig.targetAcceleration, 5);
        frontRightTargetDeceleration.init(&frontRight_elmo_in->profiledDeceleration, frontRightConfig.targetDeceleration, 10);

        frontLeftTargetVelocity.init(&frontLeft_elmo_in->targetVelocity, frontLeftConfig.targetVelocity);
        frontLeftTargetAcceleration.init(&frontLeft_elmo_in->profiledAcceleration, frontLeftConfig.targetAcceleration, 5);
        frontLeftTargetDeceleration.init(&frontLeft_elmo_in->profiledDeceleration, frontLeftConfig.targetDeceleration, 10);

        rearRightTargetVelocity.init(&rearRight_elmo_in->targetVelocity, rearRightConfig.targetVelocity);
        rearRightTargetAcceleration.init(&rearRight_elmo_in->profiledAcceleration, rearRightConfig.targetAcceleration, 5);
        rearRightTargetDeceleration.init(&rearRight_elmo_in->profiledDeceleration, rearRightConfig.targetDeceleration, 10);

        rearLeftTargetVelocity.init(&rearLeft_elmo_in->targetVelocity, rearLeftConfig.targetVelocity);
        rearLeftTargetAcceleration.init(&rearLeft_elmo_in->profiledAcceleration, rearLeftConfig.targetAcceleration, 5);
        rearLeftTargetDeceleration.init(&rearLeft_elmo_in->profiledDeceleration, rearLeftConfig.targetDeceleration, 10);
    }


    void Data::rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        frontRightCurrentValue.read();
        frontRightVelocityValue.read();

        frontLeftCurrentValue.read();
        frontLeftVelocityValue.read();

        rearRightCurrentValue.read();
        rearRightVelocityValue.read();

        rearLeftCurrentValue.read();
        rearLeftVelocityValue.read();
    }


    void Data::rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        frontRightTargetVelocity.write();
        frontRightTargetAcceleration.write();
        frontRightTargetDeceleration.write();

        frontLeftTargetVelocity.write();
        frontLeftTargetAcceleration.write();
        frontLeftTargetDeceleration.write();

        rearRightTargetVelocity.write();
        rearRightTargetAcceleration.write();
        rearRightTargetDeceleration.write();

        rearLeftTargetVelocity.write();
        rearLeftTargetAcceleration.write();
        rearLeftTargetDeceleration.write();
    }


    float& Data::getFrontRightActualVelocityValue()
    {
        return frontRightVelocityValue.value;
    }


    float& Data::getFrontRightActualCurrentValue()
    {
        return frontRightCurrentValue.value;
    }


    float& Data::getFrontLeftActualVelocityValue()
    {
        return frontLeftVelocityValue.value;
    }


    float& Data::getFrontLeftActualCurrentValue()
    {
        return frontLeftCurrentValue.value;
    }


    float& Data::getRearRightActualVelocityValue()
    {
        return rearRightTargetVelocity.value;
    }


    float& Data::getRearRightActualCurrentValue()
    {
        return rearRightCurrentValue.value;
    }


    float& Data::getRearLeftActualVelocityValue()
    {
        return rearLeftVelocityValue.value;
    }


    float& Data::getRearLeftActualCurrentValue()
    {
        return rearLeftCurrentValue.value;
    }


    void Data::setFrontRightTargetVelocity(float target)
    {
        frontRightTargetVelocity.value = target;
    }


    void Data::setFrontLeftTargetVelocity(float target)
    {
        frontLeftTargetVelocity.value = target;
    }


    void Data::setRearRightTargetVelocity(float target)
    {
        rearRightTargetVelocity.value = target;
    }


    void Data::setRearLeftTargetVelocity(float target)
    {
        rearLeftTargetVelocity.value = target;
    }


    float Data::getFrontRightTargetVelocity() const
    {
        return frontRightTargetVelocity.value;
    }


    float Data::getFrontLeftTargetVelocity() const
    {
        return frontLeftTargetVelocity.value;
    }


    float Data::getRearRightTargetVelocity() const
    {
        return rearRightTargetVelocity.value;
    }


    float Data::getRearLeftTargetVelocity() const
    {
        return rearLeftTargetVelocity.value;
    }

}
