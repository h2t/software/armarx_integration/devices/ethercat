/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Armar6RT::ArmarXObjects::Armar6MecanumPlatform
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Armar6MecanumPlatform.h"


// STD/STL
#include <iostream>

// armarx
#include <ArmarXCore/core/logging/Logging.h>


namespace devices::ethercat::platform::armar6_mecanum
{

    Armar6MecanumPlatform::Armar6MecanumPlatform()
    {
        info.platformHalfWidth = 250;
        info.platformHalfHeight = 300;
        info.radius = 95.f;
    }


    Eigen::Vector4f Armar6MecanumPlatform::calcWheelVelocity(float vx, float vy, float vAngle)
    {
        Eigen::Vector4f wheelVelocities;
        wheelVelocities(0) = 1.0f / info.radius * (+ vx + vy - (info.platformHalfWidth + info.platformHalfHeight) * vAngle);
        wheelVelocities(1) = 1.0f / info.radius * (- vx + vy + (info.platformHalfWidth + info.platformHalfHeight) * vAngle);
        wheelVelocities(2) = 1.0f / info.radius * (- vx + vy - (info.platformHalfWidth + info.platformHalfHeight) * vAngle);
        wheelVelocities(3) = 1.0f / info.radius * (+ vx + vy + (info.platformHalfWidth + info.platformHalfHeight) * vAngle);

        return wheelVelocities;
    }


    Eigen::Vector3f Armar6MecanumPlatform::calcCartesianVelocity(Eigen::Vector4f const& wheelVelocities)
    {
        Eigen::Vector3f  result;
        result(0) = (wheelVelocities(0) - wheelVelocities(1) - wheelVelocities(2) + wheelVelocities(3)) * info.radius * 0.25f;
        result(1) = (wheelVelocities(0) + wheelVelocities(1) + wheelVelocities(2) + wheelVelocities(3)) * info.radius * 0.25f;
        result(2) = (-wheelVelocities(0) + wheelVelocities(1) - wheelVelocities(2) + wheelVelocities(3)) * info.radius / (4.0f * (info.platformHalfWidth + info.platformHalfHeight));

        return result;
    }


    const PlatformGeometryInfo& Armar6MecanumPlatform::getInfo() const
    {
        return info;
    }

}
