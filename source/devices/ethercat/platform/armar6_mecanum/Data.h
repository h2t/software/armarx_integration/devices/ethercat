/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Markus Swarowsky (markus dot swarowsky at student dot kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


// armarx
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <armarx/control/ethercat/DataInterface.h>

// robot_devices
#include <devices/ethercat/common/elmo/gold/Slave.h>
#include <devices/ethercat/common/elmo/gold/Config.h>


namespace devices::ethercat::platform::armar6_mecanum
{

    using DataPtr = std::shared_ptr<class Data>;


    class Data : public armarx::control::ethercat::DataInterface
    {

    public:

        Data(const common::elmo::gold::ElmoVelocityDataConfig& frontRightConfig,
             const common::elmo::gold::ElmoVelocityDataConfig& frontLeftConfig,
             const common::elmo::gold::ElmoVelocityDataConfig& rearRightConfig,
             const common::elmo::gold::ElmoVelocityDataConfig& rearLeftConfig,
             common::elmo::gold::SlaveOut* frontRight_elmo_out, common::elmo::gold::SlaveIn* frontRight_elmo_in,
             common::elmo::gold::SlaveOut* frontLeft_elmo_out, common::elmo::gold::SlaveIn* frontLeft_elmo_in,
             common::elmo::gold::SlaveOut* rearRight_elmo_out, common::elmo::gold::SlaveIn* rearRight_elmo_in,
             common::elmo::gold::SlaveOut* rearLeft_elmo_out, common::elmo::gold::SlaveIn* rearLeft_elmo_in);

        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

        float& getFrontRightActualVelocityValue();
        float& getFrontRightActualCurrentValue();

        float& getFrontLeftActualVelocityValue();
        float& getFrontLeftActualCurrentValue();

        float& getRearRightActualVelocityValue();
        float& getRearRightActualCurrentValue();

        float& getRearLeftActualVelocityValue();
        float& getRearLeftActualCurrentValue();

        void setFrontRightTargetVelocity(float target);

        void setFrontLeftTargetVelocity(float target);

        void setRearRightTargetVelocity(float target);

        void setRearLeftTargetVelocity(float target);
        float getFrontRightTargetVelocity() const;

        float getFrontLeftTargetVelocity() const;

        float getRearRightTargetVelocity() const;

        float getRearLeftTargetVelocity() const;

    private:

        using LinearConverted_int32 = armarx::control::ethercat::LinearConvertedValue<std::int32_t>;
        using LinearConverted_uint32 = armarx::control::ethercat::LinearConvertedValue<std::uint32_t>;
        using LinearConverted_int16 = armarx::control::ethercat::LinearConvertedValue<std::int16_t>;

        LinearConverted_int32 frontRightTargetVelocity;
        LinearConverted_uint32 frontRightTargetDeceleration;
        LinearConverted_uint32 frontRightTargetAcceleration;

        LinearConverted_int32 frontLeftTargetVelocity;
        LinearConverted_uint32 frontLeftTargetAcceleration;
        LinearConverted_uint32 frontLeftTargetDeceleration;

        LinearConverted_int32 rearRightTargetVelocity;
        LinearConverted_uint32 rearRightTargetAcceleration;
        LinearConverted_uint32 rearRightTargetDeceleration;

        LinearConverted_int32 rearLeftTargetVelocity;
        LinearConverted_uint32 rearLeftTargetAcceleration;
        LinearConverted_uint32 rearLeftTargetDeceleration;

        LinearConverted_int32 frontRightVelocityValue;
        LinearConverted_int16 frontRightCurrentValue;

        LinearConverted_int32 frontLeftVelocityValue;
        LinearConverted_int16 frontLeftCurrentValue;

        LinearConverted_int32 rearRightVelocityValue;
        LinearConverted_int16 rearRightCurrentValue;

        LinearConverted_int32 rearLeftVelocityValue;
        LinearConverted_int16 rearLeftCurrentValue;

    };

}  // namespace devices::ethercat::platform::armar6_mecanum
