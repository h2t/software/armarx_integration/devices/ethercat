/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Armar6RT::ArmarXObjects::Armar6MecanumPlatform
 * @author     Simon Ottenhaus ( simon dot ottenhaus at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

// TODO: deprecated. remove this class and use Simox implementation

// Eigen
#include <Eigen/Dense>


namespace devices::ethercat::platform::armar6_mecanum
{

    struct PlatformGeometryInfo
    {
        float platformHalfWidth;
        float platformHalfHeight;
        float radius = 0;
    };


    class Armar6MecanumPlatform
    {

    public:

        Armar6MecanumPlatform();

        /**
         * @brief Armar6MecanumPlatform::calcWheelVelocity
         * @param vx velocity in x direction
         * @param vy velocity in y direction
         * @param vAngle rotation al velocity
         * @see   Omnidirectional Mobile Robot – Design and Implementation
         * @return the velocities for each wheel
         *
         * wheel 0: left front
         * wheel 1: right front
         * wheel 2: rear left
         * wheel 3: rear right
         */
        Eigen::Vector4f calcWheelVelocity(float vx, float vy, float vAngle);

        Eigen::Vector3f calcCartesianVelocity(const Eigen::Vector4f& wheelVelocities);

        const PlatformGeometryInfo& getInfo() const;

    private:

        PlatformGeometryInfo info;

    };

}
