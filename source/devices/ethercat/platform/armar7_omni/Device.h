/**
  * This file is part of ArmarX.
  *
  * ArmarX is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License version 2 as
  * published by the Free Software Foundation.
  *
  * ArmarX is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program. If not, see <http://www.gnu.org/licenses/>.
  *
  * @author     Fabian Reister ( fabian dot reister at kit dot edu )
  * @date       2021
  * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
  *             GNU General Public License
  */

#pragma once

#include <cstdint>

#include <VirtualRobot/IK/platform/OmniWheelPlatformKinematics.h>
#include <VirtualRobot/Robot.h>

#include <RobotAPI/components/units/RobotUnit/Devices/ControlDevice.h>
#include <RobotAPI/components/units/RobotUnit/Devices/SensorDevice.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValueHolonomicPlatform.h>
#include <armarx/control/ethercat/DeviceInterface.h>

#include <devices/ethercat/common/elmo/gold/Slave.h>
#include <devices/ethercat/common/elmo/gold/Config.h>
#include <devices/ethercat/platform/armar7_omni/Data.h>
#include <devices/ethercat/platform/armar7_omni/joint_controller/Emergency.h>
#include <devices/ethercat/platform/armar7_omni/joint_controller/StopMovement.h>
#include <devices/ethercat/platform/armar7_omni/joint_controller/Velocity.h>

namespace devices::ethercat::platform::armar7_omni
{
    namespace hwconfig = armarx::control::hardware_config;

    class SensorValueHolonomicPlatformWithWheels : virtual public armarx::SensorValueHolonomicPlatform
    {

    public:
        enum WheelIndex
        {
            Front,
            RearLeft,
            RearRight
        };

        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION;

        Eigen::Vector3f wheelActualCurrents = Eigen::Vector3f::Zero();   // TODO private
        Eigen::Vector3f wheelActualVelocities = Eigen::Vector3f::Zero(); // TODO private
        Eigen::Vector3f wheelTargetVelocities = Eigen::Vector3f::Zero(); // TODO private

        static SensorValueInfo<SensorValueHolonomicPlatformWithWheels>
        GetClassMemberInfo()
        {
            SensorValueInfo<SensorValueHolonomicPlatformWithWheels> svi;
            svi.addBaseClass<SensorValueHolonomicPlatform>();

            // TODO all strings as constants
            svi.addMemberVariable(&SensorValueHolonomicPlatformWithWheels::wheelActualCurrents,
                                  "wheelActualCurrents")
            .setFieldNames(
            {"FrontActualCurrent", "RearLeftActualCurrent", "RearRightActualCurrent"})
            .setVariantReportFunction(
                [](const IceUtil::Time & timestamp,
                   const SensorValueHolonomicPlatformWithWheels * ptr)
            {
                return std::map<std::string, VariantBasePtr>
                {
                    {
                        "FrontActualCurrent",
                        new armarx::TimedVariant(ptr->wheelActualCurrents(Front), timestamp)
                    },
                    {
                        "RearLeftActualCurrent",
                        new armarx::TimedVariant(ptr->wheelActualCurrents(RearLeft), timestamp)
                    },
                    {
                        "RearRightActualCurrent",
                        new armarx::TimedVariant(ptr->wheelActualCurrents(RearRight), timestamp)
                    },
                };
            });

            svi.addMemberVariable(&SensorValueHolonomicPlatformWithWheels::wheelActualVelocities,
                                  "wheelActualVelocities")
            .setFieldNames(
            {"FrontActualVelocity", "RearLeftActualVelocity", "RearRightActualVelocity"})
            .setVariantReportFunction(
                [](const IceUtil::Time & timestamp,
                   const SensorValueHolonomicPlatformWithWheels * ptr)
            {
                return std::map<std::string, VariantBasePtr>
                {
                    {
                        "FrontActualVelocity",
                        new armarx::TimedVariant(ptr->wheelActualVelocities(Front), timestamp)
                    },
                    {
                        "RearLeftActualVelocity",
                        new armarx::TimedVariant(ptr->wheelActualVelocities(RearLeft), timestamp)
                    },
                    {
                        "RearRightActualVelocity",
                        new armarx::TimedVariant(ptr->wheelActualVelocities(RearRight), timestamp)
                    },
                };
            });

            svi.addMemberVariable(&SensorValueHolonomicPlatformWithWheels::wheelTargetVelocities,
                                  "wheelTargetVelocities")
            .setFieldNames(
            {"FrontTargetVelocity", "RearLeftTargetVelocity", "RearRightTargetVelocity"})
            .setVariantReportFunction(
                [](const IceUtil::Time & timestamp,
                   const SensorValueHolonomicPlatformWithWheels * ptr)
            {
                return std::map<std::string, VariantBasePtr>
                {
                    {
                        "FrontTargetVelocity",
                        new armarx::TimedVariant(ptr->wheelTargetVelocities(Front), timestamp)
                    },
                    {
                        "RearLeftTargetVelocity",
                        new armarx::TimedVariant(ptr->wheelTargetVelocities(RearLeft), timestamp)
                    },
                    {
                        "RearRightTargetVelocity",
                        new armarx::TimedVariant(ptr->wheelTargetVelocities(RearRight), timestamp)
                    },
                };
            });
            return svi;
        }
    };


    class Device :
        public armarx::ControlDevice,
        public armarx::SensorDevice,
        public armarx::control::ethercat::DeviceInterface
    {

    public:
        Device(hwconfig::DeviceConfig& hwConfig,
               const VirtualRobot::RobotPtr& robot);

        // DeviceInterface interface
        void postSwitchToSafeOp() override;
        void postSwitchToOp() override;
        DeviceInterface::TryAssignResult tryAssign(armarx::control::ethercat::SlaveInterface& slave) override;
        DeviceInterface::AllAssignedResult onAllAssigned() override;
        std::string getClassName() const override;

        // SensorDevice interface
        const armarx::SensorValueBase* getSensorValue() const override;
        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration) override;

        // ControlDevice interface
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                 const IceUtil::Time& timeSinceLastIteration) override;

    private:
        std::vector<std::uint32_t> getElmoSerials() const;

        DataPtr getDataPtr() const;

        std::uint16_t getFrontElmoSlaveNumber() const;
        std::uint16_t getRearLeftElmoSlaveNumber() const;
        std::uint16_t getRearRightElmoSlaveNumber() const;

        std::string getWheelName(std::uint32_t elmoSerial) const;

    private:
        // The date and target object.
        DataPtr dataPtr;
        SensorValueHolonomicPlatformWithWheels sensorValue;

        // The name of the platform.
        const std::string name;

        // The serials for the four elmos of the platform.
        const std::uint32_t frontElmoSerial;
        const std::uint32_t rearLeftElmoSerial;
        const std::uint32_t rearRightElmoSerial;

        // All elmo pointers.
        devices::ethercat::common::elmo::gold::Slave* frontElmo;
        devices::ethercat::common::elmo::gold::Slave* rearLeftElmo;
        devices::ethercat::common::elmo::gold::Slave* rearRightElmo;

        // Elmo hardware_config
        common::elmo::gold::ElmoVelocityConfig frontConfig, rearLeftConfig, rearRightConfig;

        armarx::control::joint_controller::HolonomicPlatformControllerConfigurationPtr platformControllerConfig;

        // Joint controller.
        joint_controller::VelocityControllerPtr holonomicPlatformVelocityControllerPtr;
        joint_controller::EmergencyControllerPtr holonomicPlatformEmergencyControllerPtr;
        joint_controller::StopMovementControllerPtr holonomicPlatformStopMovementControllerPtr;

        VirtualRobot::OmniWheelPlatformKinematics platformKinematics;
    };

} // namespace devices::ethercat::platform::armar7_omni
