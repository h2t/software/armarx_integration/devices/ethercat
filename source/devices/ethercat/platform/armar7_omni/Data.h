/**
  * This file is part of ArmarX.
  *
  * ArmarX is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License version 2 as
  * published by the Free Software Foundation.
  *
  * ArmarX is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program. If not, see <http://www.gnu.org/licenses/>.
  *
  * @author     Fabian Reister ( fabian dot reister at kit dot edu )
  * @date       2021
  * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
  *             GNU General Public License
  */


#pragma once


// ArmarX
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <armarx/control/ethercat/DataInterface.h>

// RobotDevices
#include <devices/ethercat/common/elmo/gold/Slave.h>
#include <devices/ethercat/common/elmo/gold/Config.h>


namespace devices::ethercat::platform::armar7_omni
{
    using DataPtr = std::shared_ptr<class Data>;

    class Data : public armarx::control::ethercat::DataInterface
    {

    public:
        Data(const common::elmo::gold::ElmoVelocityDataConfig& frontConfig,
             const common::elmo::gold::ElmoVelocityDataConfig& rearLeftConfig,
             const common::elmo::gold::ElmoVelocityDataConfig& rearRightConfig,
             common::elmo::gold::SlaveOut* front_elmo_out,
             common::elmo::gold::SlaveIn* front_elmo_in,
             common::elmo::gold::SlaveOut* rearLeft_elmo_out,
             common::elmo::gold::SlaveIn* rearLeft_elmo_in,
             common::elmo::gold::SlaveOut* rearRight_elmo_out,
             common::elmo::gold::SlaveIn* rearRight_elmo_in);

        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration) override;

        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                 const IceUtil::Time& timeSinceLastIteration) override;

        float& getFrontActualVelocityValue();
        float& getRearLeftActualVelocityValue();
        float& getRearRightActualVelocityValue();

        float& getFrontActualCurrentValue();
        float& getRearLeftActualCurrentValue();
        float& getRearRightActualCurrentValue();

        void setFrontTargetVelocity(float target);
        void setRearLeftTargetVelocity(float target);
        void setRearRightTargetVelocity(float target);

        float getFrontTargetVelocity() const;
        float getRearLeftTargetVelocity() const;
        float getRearRightTargetVelocity() const;

    private:
        armarx::control::ethercat::LinearConvertedValue<std::int32_t> frontTargetVelocity;
        armarx::control::ethercat::LinearConvertedValue<std::uint32_t> frontTargetDeceleration;
        armarx::control::ethercat::LinearConvertedValue<std::uint32_t> frontTargetAcceleration;

        armarx::control::ethercat::LinearConvertedValue<std::int32_t> rearLeftTargetVelocity;
        armarx::control::ethercat::LinearConvertedValue<std::uint32_t> rearLeftTargetAcceleration;
        armarx::control::ethercat::LinearConvertedValue<std::uint32_t> rearLeftTargetDeceleration;

        armarx::control::ethercat::LinearConvertedValue<std::int32_t> rearRightTargetVelocity;
        armarx::control::ethercat::LinearConvertedValue<std::uint32_t> rearRightTargetAcceleration;
        armarx::control::ethercat::LinearConvertedValue<std::uint32_t> rearRightTargetDeceleration;

        armarx::control::ethercat::LinearConvertedValue<std::int32_t> frontVelocityValue;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> frontCurrentValue;

        armarx::control::ethercat::LinearConvertedValue<std::int32_t> rearLeftVelocityValue;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> rearLeftCurrentValue;

        armarx::control::ethercat::LinearConvertedValue<std::int32_t> rearRightVelocityValue;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> rearRightCurrentValue;
    };

} // namespace devices::ethercat::platform::armar7_omni
