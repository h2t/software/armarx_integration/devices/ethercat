/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Raphael Grimm (raphael dot grimm at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "Velocity.h"

#include <Eigen/Core>

// ArmarX
#include <ArmarXCore/core/logging/Logging.h>

#include <RobotAPI/libraries/core/math/MathUtils.h>

#include <devices/ethercat/platform/armar7_omni/config.h>

// RobotDevices

namespace devices::ethercat::platform::armar7_omni::joint_controller
{

    constexpr float MAX_DELTA_DT = 0.002F;

    VelocityController::VelocityController(
        const DataPtr& holoDataPtr,
        armarx::control::joint_controller::HolonomicPlatformControllerConfigurationPtr configDataPtr) :
        target(), dataPtr(holoDataPtr), platformKinematics(ARMAR7_OMNI_PLATFORM_CONFIG)
    {
        //limitXVelocityController.init(500, 1500, 0.002f, 0);
        //limitYVelocityController.init(500, 1500, 0.002f, 0);
        //limitAngelVelocityController.init(0.8, 2, 0.002, 0);
        limitXVelocityController.init(configDataPtr->maxVelocity,
                                      configDataPtr->maxAcceleration,
                                      configDataPtr->maxDeceleration,
                                      MAX_DELTA_DT,
                                      0);
        limitYVelocityController.init(configDataPtr->maxVelocity,
                                      configDataPtr->maxAcceleration,
                                      configDataPtr->maxDeceleration,
                                      MAX_DELTA_DT,
                                      0);
        limitAngelVelocityController.init(configDataPtr->maxAngularVelocity,
                                          configDataPtr->maxAngularAcceleration,
                                          configDataPtr->maxAngularDeceleration,
                                          MAX_DELTA_DT,
                                          0);
    }

    void
    VelocityController::rtRun(const IceUtil::Time& sensorValuesTimestamp,
                              const IceUtil::Time& timeSinceLastIteration)
    {
        //    ARMARX_INFO << deactivateSpam(1) << VAROUT(target.velocityY);

        const float vxTarget = target.velocityX;
        const float vyTarget = target.velocityY;
        const float vAngleTarget = target.velocityRotation;

        //wait a second to give the wheels time to release breaks
        if (sensorValuesTimestamp.toSecondsDouble() > 0.0)
        {
            auto dt = static_cast<float>(timeSinceLastIteration.toSecondsDouble());

            float vx = limitXVelocityController.update(vxTarget, dt);
            float vy = limitYVelocityController.update(vyTarget, dt);
            float vAngle = limitAngelVelocityController.update(vAngleTarget, dt);

            const Eigen::Vector3f v =
                platformKinematics.calcWheelVelocity(Eigen::Vector3f{vx, vy, vAngle});
            dataPtr->setFrontTargetVelocity(v(0));
            dataPtr->setRearLeftTargetVelocity(v(1));
            dataPtr->setRearRightTargetVelocity(v(2));
        }
        else
        {
            dataPtr->setFrontTargetVelocity(0.0);
            dataPtr->setRearLeftTargetVelocity(0.0);
            dataPtr->setRearRightTargetVelocity(0.0);
        }
    }

    armarx::ControlTargetBase*
    VelocityController::getControlTarget()
    {
        return &target;
    }

    void
    LinearLimitedAccelerationController::init(float maxVelocity,
            float maxAcceleration,
            float maxDeceleration,
            float maxDeltaT,
            float value)
    {
        this->maxVelocity = maxVelocity;
        this->maxAcceleration = maxAcceleration;
        this->maxDeceleration = maxDeceleration;
        this->maxDeltaT = maxDeltaT;
        this->currentValue = value;
    }

    float
    LinearLimitedAccelerationController::update(float value, float dt)
    {
        dt = std::max(0.F, std::min(dt, maxDeltaT));
        float delta = value - currentValue;
        float sign = 1;
        if (currentValue <= 0)
        {
            delta = -delta;
            sign = -1;
        }
        delta = delta < 0 ? -std::min(maxDeceleration * dt, -delta)
                : std::min(maxAcceleration * dt, delta);
        delta = delta * sign;
        currentValue = currentValue + delta;
        currentValue = static_cast<float>(armarx::math::MathUtils::LimitTo(currentValue, maxVelocity));

        return currentValue;
    }

} // namespace devices::ethercat::platform::armar7_omni::joint_controller
