/**
  * This file is part of ArmarX.
  *
  * ArmarX is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License version 2 as
  * published by the Free Software Foundation.
  *
  * ArmarX is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program. If not, see <http://www.gnu.org/licenses/>.
  *
  * @author     Fabian Reister ( fabian dot reister at kit dot edu )
  * @date       2021
  * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
  *             GNU General Public License
  */
#pragma once

// STD/STL
#include <memory>

// ArmarX
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>

// RobotDevices
#include <devices/ethercat/platform/armar7_omni/Data.h>


namespace devices::ethercat::platform::armar7_omni::joint_controller
{

    using EmergencyControllerPtr = std::shared_ptr<class EmergencyController>;

    class EmergencyController : public armarx::JointController
    {

    public:
        EmergencyController(const DataPtr& holoDataPtr);

        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;

        armarx::ControlTargetBase* getControlTarget() override;

    private:
        armarx::DummyControlTargetEmergencyStop target;
        DataPtr dataPtr;
    };

} // namespace devices::ethercat::platform::armar7_omni::joint_controller
