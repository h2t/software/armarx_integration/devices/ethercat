/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Fabian Reister ( fabian dot reister at kit dot edu )
 * @date       2021
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Device.h"

#include <cmath>

#include <Eigen/Geometry>

#include <VirtualRobot/IK/platform/OmniWheelPlatformKinematics.h>

#include <ArmarXCore/core/logging/Logging.h>

#include <devices/ethercat/platform/armar7_omni/config.h>


namespace devices::ethercat::platform::armar7_omni
{

    VirtualRobot::OmniWheelPlatformKinematics::Params platformKinematicsFromConfig(hwconfig::DeviceConfig& hwConfig)
    {
        return VirtualRobot::OmniWheelPlatformKinematics::Params{
            .L = hwConfig.getFloat("bodyRadius"),
            .R = hwConfig.getFloat("wheelRadius"),
            .delta = hwConfig.getFloat("angularPositionFirstWheelDegrees"),
            .n = 1
        };
    }

    Device::Device(hwconfig::DeviceConfig& hwConfig,
                   const VirtualRobot::RobotPtr&) :
        DeviceBase(hwConfig.getName()),
        ControlDevice(hwConfig.getName()),
        SensorDevice(hwConfig.getName()),
        DeviceInterface(hwConfig.getName()),
        dataPtr(nullptr),
        frontElmoSerial(hwConfig.getSlaveConfig("Elmo", "Front").getIdentifier().getSerialNumber()),
        rearLeftElmoSerial(
            hwConfig.getSlaveConfig("Elmo", "RearLeft").getIdentifier().getSerialNumber()),
        rearRightElmoSerial(
            hwConfig.getSlaveConfig("Elmo", "RearRight").getIdentifier().getSerialNumber()),
        frontElmo(nullptr),
        rearLeftElmo(nullptr),
        rearRightElmo(nullptr),
        frontConfig(hwConfig.getSlaveConfig("Elmo", "Front")),
        rearLeftConfig(hwConfig.getSlaveConfig("Elmo", "RearLeft")),
        rearRightConfig(hwConfig.getSlaveConfig("Elmo", "RearRight")),
        platformKinematics(platformKinematicsFromConfig(hwConfig))
    {
        platformControllerConfig = armarx::control::joint_controller::HolonomicPlatformControllerConfiguration::
                CreateHolonomicPlatformControllerConfigData(hwConfig.getControllerConfig("Platform"));
    }


    void
    Device::postSwitchToSafeOp()
    {
        if (!dataPtr)
        {
            //creating the data object
            dataPtr = std::make_shared<Data>(frontConfig,
                                             rearLeftConfig,
                                             rearRightConfig,
                                             frontElmo->getOutputsPtr(),
                                             frontElmo->getInputsPtr(),
                                             rearLeftElmo->getOutputsPtr(),
                                             rearLeftElmo->getInputsPtr(),
                                             rearRightElmo->getOutputsPtr(),
                                             rearRightElmo->getInputsPtr());

            //creating the joint controller
            holonomicPlatformVelocityControllerPtr =
                std::make_shared<joint_controller::VelocityController>(
                    dataPtr,platformControllerConfig);
            holonomicPlatformEmergencyControllerPtr =
                std::make_shared<joint_controller::EmergencyController>(dataPtr);
            holonomicPlatformStopMovementControllerPtr =
                std::make_shared<joint_controller::StopMovementController>(dataPtr);

            addJointController(holonomicPlatformVelocityControllerPtr.get());
            addJointController(holonomicPlatformEmergencyControllerPtr.get());
            addJointController(holonomicPlatformStopMovementControllerPtr.get());
        }
    }

    void
    Device::postSwitchToOp()
    {
        //set the elmos initial to velocity mode, so platform don't moves at startup
        frontElmo->switchMode(common::elmo::gold::ElmoControlMode::VELOCITY);
        rearLeftElmo->switchMode(common::elmo::gold::ElmoControlMode::VELOCITY);
        rearRightElmo->switchMode(common::elmo::gold::ElmoControlMode::VELOCITY);
    }


    armarx::control::ethercat::DeviceInterface::TryAssignResult
    Device::tryAssign(armarx::control::ethercat::SlaveInterface& slave)
    {
        namespace ecat = armarx::control::ethercat;
        using Result = DeviceInterface::TryAssignResult;

        Result result = ecat::tryAssignUsingSerialNo(slave, frontElmo, frontElmoSerial);
        if (result != Result::unknown)
        {
            return result;
        }

        result = ecat::tryAssignUsingSerialNo(slave, rearLeftElmo, rearLeftElmoSerial);
        if (result != Result::unknown)
        {
            return result;
        }

        result = ecat::tryAssignUsingSerialNo(slave, rearRightElmo, rearRightElmoSerial);
        if (result != Result::unknown)
        {
            return result;
        }

        return result;
    }


    armarx::control::ethercat::DeviceInterface::AllAssignedResult
    Device::onAllAssigned()
    {
        using Result = DeviceInterface::AllAssignedResult;

        if (frontElmo and rearLeftElmo and rearRightElmo)
        {
            // Setting the configuration parameter for the Elmos.
            frontElmo->setConfiguration(frontConfig);
            rearLeftElmo->setConfiguration(rearLeftConfig);
            rearRightElmo->setConfiguration(rearRightConfig);

            return Result::ok;
        }

        return Result::slavesMissing;
    }

    std::string
    Device::getClassName() const
    {
        return "OmniwheelPlatform_Armar7";
    }


    const armarx::SensorValueBase*
    Device::getSensorValue() const
    {
        return &sensorValue;
    }


    void
    Device::rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                               const IceUtil::Time& timeSinceLastIteration)
    {
        if (dataPtr == nullptr)
        {
            DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                   "Omniwheel platform has no data set, switch to SafeOP before");
        }
        dataPtr->rtReadSensorValues(sensorValuesTimestamp, timeSinceLastIteration);

        using Wheel = SensorValueHolonomicPlatformWithWheels;
        sensorValue.wheelActualCurrents(Wheel::Front) = dataPtr->getFrontActualCurrentValue();
        sensorValue.wheelActualCurrents(Wheel::RearLeft) = dataPtr->getRearLeftActualCurrentValue();
        sensorValue.wheelActualCurrents(Wheel::RearRight) =
            dataPtr->getRearRightActualCurrentValue();

        sensorValue.wheelActualVelocities(Wheel::Front) = dataPtr->getFrontActualVelocityValue();
        sensorValue.wheelActualVelocities(Wheel::RearLeft) =
            dataPtr->getRearLeftActualVelocityValue();
        sensorValue.wheelActualVelocities(Wheel::RearRight) =
            dataPtr->getRearRightActualVelocityValue();

        sensorValue.wheelTargetVelocities(Wheel::Front) = dataPtr->getFrontTargetVelocity();
        sensorValue.wheelTargetVelocities(Wheel::RearLeft) = dataPtr->getRearLeftTargetVelocity();
        sensorValue.wheelTargetVelocities(Wheel::RearRight) = dataPtr->getRearRightTargetVelocity();

        Eigen::Vector3f cartesianVelocities = platformKinematics.calcCartesianVelocity(
            Eigen::Vector3f(sensorValue.wheelActualVelocities(Wheel::Front),
                            sensorValue.wheelActualVelocities(Wheel::RearLeft),
                            sensorValue.wheelActualVelocities(Wheel::RearRight)));

        sensorValue.velocityX = cartesianVelocities(0);
        sensorValue.velocityY = cartesianVelocities(1);
        sensorValue.velocityRotation = cartesianVelocities(2);

        auto deltaSeconds = timeSinceLastIteration.toSecondsDouble();
        Eigen::Vector2f platformPosition(static_cast<double>(sensorValue.velocityX) * deltaSeconds,
                                         static_cast<double>(sensorValue.velocityY) * deltaSeconds);
        sensorValue.relativePositionRotation +=
            sensorValue.velocityRotation * static_cast<float>(deltaSeconds);
        Eigen::Rotation2Df rot(sensorValue.relativePositionRotation);
        Eigen::Vector2f integratedPosition = rot * platformPosition;
        sensorValue.relativePositionX += integratedPosition(0);
        sensorValue.relativePositionY += integratedPosition(1);
    }


    void
    Device::rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration)
    {
        if (dataPtr == nullptr)
        {
            DEVICE_FATAL_AND_THROW(rtGetDeviceName(),
                                   "Omniwheel platform has no data set, switch to SafeOP before");
        }
        dataPtr->rtWriteTargetValues(sensorValuesTimestamp, timeSinceLastIteration);
    }


    std::vector<std::uint32_t>
    Device::getElmoSerials() const
    {
        return {frontElmoSerial, rearLeftElmoSerial, rearRightElmoSerial};
    }


    DataPtr
    Device::getDataPtr() const
    {
        return dataPtr;
    }


    std::uint16_t
    Device::getFrontElmoSlaveNumber() const
    {
        return frontElmo->getSlaveNumber();
    }


    std::uint16_t
    Device::getRearLeftElmoSlaveNumber() const
    {
        return rearLeftElmo->getSlaveNumber();
    }


    std::uint16_t
    Device::getRearRightElmoSlaveNumber() const
    {
        return rearRightElmo->getSlaveNumber();
    }


    std::string
    Device::getWheelName(std::uint32_t elmoSerial) const
    {
        if (elmoSerial == frontElmoSerial)
        {
            return "Front";
        }

        if (elmoSerial == rearLeftElmoSerial)
        {
            return "RearLeft";
        }

        if (elmoSerial == rearRightElmoSerial)
        {
            return "RearRight";
        }

        DEVICE_FATAL_AND_THROW(
            rtGetDeviceName(), "No wheel wither serial number %u exists", elmoSerial);
    }

} // namespace devices::ethercat::platform::armar7_omni
