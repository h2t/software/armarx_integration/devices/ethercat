/**
  * This file is part of ArmarX.
  *
  * ArmarX is free software; you can redistribute it and/or modify
  * it under the terms of the GNU General Public License version 2 as
  * published by the Free Software Foundation.
  *
  * ArmarX is distributed in the hope that it will be useful, but
  * WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program. If not, see <http://www.gnu.org/licenses/>.
  *
  * @author     Fabian Reister ( fabian dot reister at kit dot edu )
  * @date       2021
  * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
  *             GNU General Public License
  */

#include "Data.h"

namespace devices::ethercat::platform::armar7_omni
{

    Data::Data(const common::elmo::gold::ElmoVelocityDataConfig& frontConfig,
               const common::elmo::gold::ElmoVelocityDataConfig& rearLeftConfig,
               const common::elmo::gold::ElmoVelocityDataConfig& rearRightConfig,
               common::elmo::gold::SlaveOut* front_elmo_out,
               common::elmo::gold::SlaveIn* front_elmo_in,
               common::elmo::gold::SlaveOut* rearLeft_elmo_out,
               common::elmo::gold::SlaveIn* rearLeft_elmo_in,
               common::elmo::gold::SlaveOut* rearRight_elmo_out,
               common::elmo::gold::SlaveIn* rearRight_elmo_in)
    {
        //sensor values
        frontVelocityValue.init(&front_elmo_out->velocityActualValue,
                                frontConfig.velocity);
        frontCurrentValue.init(&front_elmo_out->currentActualValue,
                               frontConfig.currentValue);

        rearLeftVelocityValue.init(&rearLeft_elmo_out->velocityActualValue,
                                   rearLeftConfig.velocity);
        rearLeftCurrentValue.init(&rearLeft_elmo_out->currentActualValue,
                                  rearLeftConfig.currentValue);

        rearRightVelocityValue.init(&rearRight_elmo_out->velocityActualValue,
                                    rearRightConfig.velocity);
        rearRightCurrentValue.init(&rearRight_elmo_out->currentActualValue,
                                   rearRightConfig.currentValue);

        //targets
        frontTargetVelocity.init(&front_elmo_in->targetVelocity, frontConfig.targetVelocity);
        frontTargetAcceleration.init(
            &front_elmo_in->profiledAcceleration, frontConfig.targetAcceleration, 5);
        frontTargetDeceleration.init(
            &front_elmo_in->profiledDeceleration, frontConfig.targetDeceleration, 10);

        rearLeftTargetVelocity.init(&rearLeft_elmo_in->targetVelocity,
                                    rearLeftConfig.targetVelocity);
        rearLeftTargetAcceleration.init(
            &rearLeft_elmo_in->profiledAcceleration, rearLeftConfig.targetAcceleration, 5);
        rearLeftTargetDeceleration.init(
            &rearLeft_elmo_in->profiledDeceleration, rearLeftConfig.targetDeceleration, 10);

        rearRightTargetVelocity.init(&rearRight_elmo_in->targetVelocity,
                                     rearRightConfig.targetVelocity);
        rearRightTargetAcceleration.init(
            &rearRight_elmo_in->profiledAcceleration, rearRightConfig.targetAcceleration, 5);
        rearRightTargetDeceleration.init(
            &rearRight_elmo_in->profiledDeceleration, rearRightConfig.targetDeceleration, 10);
    }

    void
    Data::rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                             const IceUtil::Time& timeSinceLastIteration)
    {
        frontCurrentValue.read();
        frontVelocityValue.read();

        rearLeftCurrentValue.read();
        rearLeftVelocityValue.read();

        rearRightCurrentValue.read();
        rearRightVelocityValue.read();
    }

    void
    Data::rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                              const IceUtil::Time& timeSinceLastIteration)
    {
        frontTargetVelocity.write();
        frontTargetAcceleration.write();
        frontTargetDeceleration.write();

        rearLeftTargetVelocity.write();
        rearLeftTargetAcceleration.write();
        rearLeftTargetDeceleration.write();

        rearRightTargetVelocity.write();
        rearRightTargetAcceleration.write();
        rearRightTargetDeceleration.write();
    }

    float&
    Data::getFrontActualVelocityValue()
    {
        return frontVelocityValue.value;
    }

    float&
    Data::getRearLeftActualCurrentValue()
    {
        return rearLeftCurrentValue.value;
    }

    float&
    Data::getRearLeftActualVelocityValue()
    {
        return rearLeftVelocityValue.value;
    }

    float&
    Data::getFrontActualCurrentValue()
    {
        return frontCurrentValue.value;
    }

    float&
    Data::getRearRightActualVelocityValue()
    {
        return rearRightVelocityValue.value;
    }

    float&
    Data::getRearRightActualCurrentValue()
    {
        return rearRightCurrentValue.value;
    }

    void
    Data::setFrontTargetVelocity(float target)
    {
        frontTargetVelocity.value = target;
    }

    void
    Data::setRearLeftTargetVelocity(float target)
    {
        rearLeftTargetVelocity.value = target;
    }

    void
    Data::setRearRightTargetVelocity(float target)
    {
        rearRightTargetVelocity.value = target;
    }

    float
    Data::getFrontTargetVelocity() const
    {
        return frontTargetVelocity.value;
    }

    float
    Data::getRearLeftTargetVelocity() const
    {
        return rearLeftTargetVelocity.value;
    }

    float
    Data::getRearRightTargetVelocity() const
    {
        return rearRightTargetVelocity.value;
    }

} // namespace devices::ethercat::platform::armar7_omni
