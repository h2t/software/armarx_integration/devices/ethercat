#pragma once


#include <armarx/control/ethercat/SlaveInterface.h>

#include <devices/ethercat/common/sensor_board/armar7de/ErrorDecoder.h>
#include <devices/ethercat/common/sensor_board/armar7de/SlaveIO.h>

namespace devices::ethercat::common::sensor_board::armar7de
{
    class Slave : public armarx::control::ethercat::SlaveInterfaceWithIO<SlaveIn, SlaveOut>
    {

    public:
        Slave(const armarx::control::ethercat::SlaveIdentifier slaveIdentifier_);

        /**
         * @see AbstractSlave::doMappings
         */
        void doMappings() override;

        void prepareForSafeOp() override;
        void finishPreparingForSafeOp() override;

        void prepareForOp() override;
        void finishPreparingForOp() override;

        /**
         * @see AbstractSlave::prepare()
         */
        bool prepareForRun() override;

        /**
         * @see AbstractSlave::execute()
         */
        void execute() override;

        /**
         * @see AbstractSlave::shutdown()
         */
        bool shutdown() override;

        /**
         * @see AbstractSlave::hasError()
         * @return
         */
        bool hasError() override;

        bool isEmergencyStopActive() const override;
        bool recoverFromEmergencyStop() override;

        static bool
        isSlaveIdentifierAccepted(const armarx::control::ethercat::SlaveIdentifier& sid);

        static std::string getDefaultName();

        /**
         * @brief Read the CoE value over the ethercat bus.
         * Do not call during operation because it can take more than 1ms.
         */
        std::uint8_t readTorquePgaAndSps();

        /**
         * @brief Set torquePgaAndSps bitmap that is applied when switching to safe-op
         */
        void setTorquePgaAndSps(std::uint8_t torquePgaAndSps);

        /**
         * @brief Read the CoE value over the ethercat bus.
         * Do not call during operation because it can take more than 1ms.
         */
        std::uint16_t readTorqueAdcTempFrequency();

        /**
         * @brief Set torqueAdcTempFrequency that is applied when switching to safe-op
         */
        void setTorqueAdcTempFrequency(std::uint16_t torqueAdcTempFrequency);

        /**
         * @brief Read the CoE value over the ethercat bus.
         * Do not call during operation because it can take more than 1ms.
         */
        std::uint8_t readDisableImuCalibAccel();

        /**
         * @brief Set disableImuCalibAccel that is applied when switching to safe-op
         */
        void setDisableImuCalibAccel(std::uint8_t disableImuCalibAccel);

        /**
         * @brief Read the CoE value over the ethercat bus.
         * Do not call during operation because it can take more than 1ms.
         */
        std::uint8_t readDisableImuCalibGyro();

        /**
         * @brief Set disableImuCalibGyro that is applied when switching to safe-op
         */
        void setDisableImuCalibGyro(std::uint8_t disableImuCalibGyro);

        /**
         * @brief Read the CoE value over the ethercat bus.
         * Do not call during operation because it can take more than 1ms.
         */
        std::uint8_t readDisableImuCalibMag();

        /**
         * @brief Set disableImuCalibMag that is applied when switching to safe-op
         */
        void setDisableImuCalibMag(std::uint8_t disableImuCalibMag);

        /**
         * @brief Read the CoE value over the ethercat bus.
         * Do not call during operation because it can take more than 1ms.
         */
        std::uint8_t readLedStripCount();

        /**
         * @brief Set ledStripCount that is applied when switching to safe-op
         */
        void setLedStripCount(std::uint8_t ledStripCount);

    private:
        ErrorDecoder errorDecoder;
        armarx::control::ethercat::Bus& bus;

        std::uint8_t torquePgaAndSps;
        std::uint16_t torqueAdcTempFrequency;
        std::uint8_t disableImuCalibAccel;
        std::uint8_t disableImuCalibGyro;
        std::uint8_t disableImuCalibMag;
        std::uint8_t ledStripCount;

        void writeCoE();
    };

} // namespace devices::ethercat::common::sensor_board::armar7de
