#pragma once

#include <cstdint>
#include <armarx/control/hardware_config/SlaveConfig.h>

namespace devices::ethercat::common::sensor_board::armar7de
{
    namespace hwconfig = armarx::control::hardware_config;

    struct SensorBoardConfig
    {
        SensorBoardConfig(hwconfig::SlaveConfig& config);

        std::uint8_t torquePgaAndSps;
        std::uint16_t torqueAdcTempFrequency;
        bool disableCalibAccel;
        bool disableCalibGyro;
        bool disableCalibMag;
        std::uint8_t ledStripCount;

        // Conversion parameters
        hwconfig::types::LinearConfig voltage;
        hwconfig::types::ModularConvertedValueConfig absoluteEncoder;
        float torqueFactor;
        float torqueOffset;
    };
}
