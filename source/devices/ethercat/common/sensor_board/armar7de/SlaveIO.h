#pragma once


#include <cstdint>


namespace devices::ethercat::common::sensor_board::armar7de
{

    /**
     * @brief PDO mapping sensorB->master
     */
    struct SlaveOut
    {
        // Object Entry 0x6000
        // Board Diagnostics
        std::uint16_t StatusBits;
        std::uint16_t VoltageMonitor;
        std::uint16_t MainUpdateRate;
        std::uint16_t MotorTemperatureADCValue;

        // Torque ADC
        std::int32_t TorqueADCValue;
        std::int32_t TorqueADCTempValue;

        // Absolute Encoder
        std::uint32_t AbsoluteEncoderValue;
        std::uint16_t AbsoluteEncoderDetailedStatus;
        std::uint8_t AbsoluteEncoderTemperature;

        std::uint8_t pad1;

        // IMU
        std::int16_t IMUAccelerationX;
        std::int16_t IMUAccelerationY;
        std::int16_t IMUAccelerationZ;
        std::int16_t IMUOrientationW;
        std::int16_t IMUOrientationX;
        std::int16_t IMUOrientationY;
        std::int16_t IMUOrientationZ;
        /*
         * 2 bits IMU Accuracy Acceleration
         * 2 bits IMU Accuracy Orientation
         * 1 bit IMU Error Indicator
         * 3 bits padding
         */
        std::uint8_t IMUSingleBits;

        std::uint8_t pad2;
    } __attribute__((__packed__));


    /**
     * @brief PDO mapping master->sensorB
     */
    struct SlaveIn
    {
        // RGB LED control
        std::uint8_t RedChannel;
        std::uint8_t GreenChannel;
        std::uint8_t BlueChannel;

        /*
         * 1 bit IMU Include Magnetometer in Orientation
         * 1 bit IMU Include Gravity in Acceleration
         * 6 bits padding
         */
        std::uint8_t flags;
    } __attribute__((__packed__));

} // namespace robot_devices::ethercat::common::sensor_board::armar7de
