#pragma once

#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValueIMU.h>
#include <armarx/control/ethercat/DataInterface.h>

#include <devices/ethercat/common/sensor_board/armar7de/ErrorDecoder.h>
#include <devices/ethercat/common/sensor_board/armar7de/SlaveIO.h>
#include <devices/ethercat/common/sensor_board/armar7de/Config.h>

namespace devices::ethercat::common::sensor_board::armar7de
{
    /**
     * @brief The TorqueConvertedValue class converts the value of torque adc to the
     * actual torque in Nm.
     */
    class TorqueConvertedValue
    {
    public:
        TorqueConvertedValue()
        {
            raw = nullptr;
            offset = factor = 0;
        }

        void
        init(std::int32_t* raw, float factor_uV_to_Nm, float offset_in_uV, std::uint8_t torqueAdcPgaAndSps)
        {
            const auto rawAsInt = reinterpret_cast<std::uint64_t>(raw);
            ARMARX_CHECK_EXPRESSION((rawAsInt % alignof(std::int32_t)) == 0)
                << "\nThe alignment is wrong!\nIt has to be " << alignof(std::int32_t)
                << ", but the data is aligned with " << rawAsInt % alignof(std::max_align_t)
                << "!\nThis is an offset of " << (rawAsInt % alignof(std::int32_t))
                << " bytes!\nThe datatype is "
                << "std::int32_t"
                << "\nIts size is " << sizeof(std::int32_t) << "\nraw = " << raw
                << " bytes\nThe name is "
                << "torque Value";
            this->factor = factor_uV_to_Nm;
            this->offset = offset_in_uV;
            this->raw = raw;
            this->torqueAdcPgaAndSps = torqueAdcPgaAndSps;

            value = 0;
            read();
        }

        void
        read()
        {
            double torqueAdcValueNormalized =
                static_cast<double>((*raw) / (1 << ((torqueAdcPgaAndSps & 0b01110000) >> 4)));
            double torqueInuV = torqueAdcValueNormalized * 1000000 * v_ref / (1 << 23) - offset;
            value = torqueInuV * factor;
        }

        float value;

        std::int32_t
        getRaw() const
        {
            return *raw;
        }

        float
        getFactor() const
        {
            return factor;
        }

        float
        getOffset() const
        {
            return offset;
        }

    private:
        std::int32_t* raw;
        float offset, factor;
        std::uint8_t torqueAdcPgaAndSps;

        const float v_ref = 2.25;
    };

    class Data : public armarx::control::ethercat::DataInterface
    {
    public:
        Data(SensorBoardConfig config,
             SlaveOut* outputs,
             SlaveIn* inputs);

        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration) override;
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                 const IceUtil::Time& timeSinceLastIteration) override;

        /**
         * @brief The input voltage of the sensor-actor PCB.
         * This voltage also describes the mototr voltage.
         * Maximum is 58V
         */
        float getVoltage() const;

        /**
         * @brief Describes the frequency of the main loop on the sensor-actor-pcb in Hz.
         * This frequency does not necessarily correspond to the frequency of other sensor values.
         */
        std::uint16_t getMainUpdateRate() const;

        /**
         * @brief The sensor for this temperature is on the Motor-PCB and has direct contact with the stator of the motor.
         * @return the motor temperature
         */
        float getMotorTemperature() const;

        /**
         * @return the torque mesured by this sensorboard
         */
        float getActualTorque() const;

        /**
         * @return the temperature of the torque-ADC
         */
        float getTorqueAdcTemperature() const;

        /**
         * @return the actual joint angle in radians
         */
        float getActualPosition() const;

        /**
         * @return the temperature of the Absolute-Encoder-PCB in degrees Celsius
         */
        float getAbsoluteEncoderTemperature() const;

        /**
         * @return the detailed status of the absolute encoder
         */
        std::uint16_t getAbsoluteEncoderDetailedStatus() const;

        /**
         * @return the acceleration in x-, y- and z-direction relative to the pose of the IMU
         */
        const Eigen::Vector3f& getImuAcceleration() const;

        /**
         * @return a quaternion that describes the orientation of the IMU
         */
        const Eigen::Quaternionf& getImuOrientation() const;

        /**
         * @brief Get the accuracy of the imu acceleration.
         * This value is a 2-bit integer ranging from 0 (unreliable) to 3 (high accuracy).
         *
         * @return the accuracy of imu acceleration values
         */
        std::uint8_t getImuAccelerationAccuracy() const;

        /**
         * @brief Get the accuracy of the imu orientation.
         * This value is a 2-bit integer ranging from 0 (unreliable) to 3 (high accuracy).
         *
         * @return the accuracy of imu orientation values
         */
        std::uint8_t getImuOrientationAccuracy() const;

        std::int32_t getTorqueTicks() const;

        std::uint32_t getAbsoluteEncoderTicks() const;

        /**
         * @return whether a communication error with the motor temperature ADC has been detected
         */
        bool hasMotorTemperatureError() const;

        /**
         * Get the status of the relative encoder.
         * This value is a 2-bit integer with the following meanings:
         * 0: Encoder ok
         * 1: Encoder has no power
         * 2: Encoder has no signal
         * 3: Speed is too high
         *
         * @return relative encoder status
         */
        std::uint8_t getRelativeEncoderStatus() const;

        /**
         * @brief Indicates whether the hardware emergency stop signal is signaling an emergency
         * stop or not.
         *
         * @return true if the electrical emergency stop signal is low and false if the signal is high
         */
        bool getEmergencyStopStatus() const;

        /**
         * @return whether there is an error during communication with the IMU
         */
        bool hasImuError() const;

        std::uint16_t getTorqueAdcErrorCounter() const;

        enum class TemperatureType
        {
            MotorAdcTemperature,
            TorqueAdcTemperature,
            AbsoluteEncoderTemperature
        };

        bool hasTemperatureBitshiftOccured(TemperatureType type) const;

        void setRedLedIntensity(std::uint8_t redIntensity);

        void setGreenLedIntensity(std::uint8_t greenIntensity);

        void setBlueLedIntensity(std::uint8_t blueItensity);

        void setImuIncludeMagnetometerInOrientation(bool includeMagnetometer);

        void setImuIncludeGravityInAcceleration(bool includeGravity);


    private:
        SlaveOut* outputs;
        SlaveIn* inputs;

        ErrorDecoder errorDecoder;

        // Normal Slave inputs
        armarx::control::ethercat::LinearConvertedValue<std::uint16_t> voltage;
        std::uint16_t mainUpdateRate;
        float motorTemp;
        TorqueConvertedValue torque;
        float torqueAdcTemp;
        armarx::control::ethercat::ModularConvertedValue<std::uint32_t, std::int64_t> absoluteEncoder;
        float absEncoderTemp;
        Eigen::Vector3f imuAcceleration;
        Eigen::Quaternionf imuOrientation;
        std::uint8_t imuAccelerationAccuracy;
        std::uint8_t imuOrientationAccuracy;

        // Error and status inputs
        bool motorTemperatureError;
        std::uint8_t relativeEncoderStatus;
        bool emergencyStopStatus;
        bool imuError;

        // Slave outputs
        std::uint8_t red = 0;
        std::uint8_t green = 0;
        std::uint8_t blue = 0;
        bool includeMagnetometer = false;
        bool includeGravity = false;

        template <typename T>
        class TemperatureBitshiftDetector
        {
        public:
            enum class ShiftDirection
            {
                Left,
                Right,
                None
            };

            T
            update(T newValue)
            {
                if (lastValue != std::numeric_limits<T>::max())
                {
                    // new value has been bitshifted to the left and is therefore around twice
                    // as large as the last value. Checking against 1.5x of the last value is
                    // sufficent to detect such bitshifts while not getting false positives
                    if (newValue > lastValue * 1.5)
                    {
                        lastValueShiftDirection = ShiftDirection::Left;
                        // do not update lastValue
                    }
                    else if (newValue < lastValue / 1.5)
                    {
                        lastValueShiftDirection = ShiftDirection::Right;
                        // do not update lastValue
                    }
                    else
                    {
                        lastValueShiftDirection = ShiftDirection::None;
                        lastValue = newValue;
                    }
                }
                else
                {
                    lastValue = newValue;
                }
                return lastValue;
            }

            ShiftDirection
            getLastShiftDirection() const
            {
                return lastValueShiftDirection;
            }

        private:
            T lastValue = std::numeric_limits<T>::max();
            ShiftDirection lastValueShiftDirection = ShiftDirection::None;
        };

        TemperatureBitshiftDetector<float> motorTempBitshiftDetector;
        TemperatureBitshiftDetector<float> torqueAdcTempBitshiftDetector;
        TemperatureBitshiftDetector<float> absEncoderTempBitshiftDetector;

        const float v_ref = 2.25;

        float readAndConvertMotorTemperatureValue();

        float readAndConvertTorqueAdcTemperatureValue();

        float readAndConvertAbsEncoderTemperatureValue();

        /**
         * @brief Convert fix-point integer values to float
         * @param i the integer value
         * @param q the bitshift
         * @return the converted value
         */
        inline float
        intToQFloat(std::int16_t i, unsigned short int q)
        {
            return i * (1.f / static_cast<float>(1 << q));
        }

        void readAndConvertIMUAcceleration();

        void readAndConvertIMUOrientation();

        void readAndConvertIMUAccelerationAccuracy();

        void readAndConvertIMUOrientationAccuracy();

        void readAndConvertMotorTemperatureError();

        void readAndConvertRelativeEncoderStatus();

        void readAndConvertEmergencyStopStatus();

        void readAndConvertImuError();
    };
} // namespace devices::ethercat::common::sensor_board::armar7de
