#include "Slave.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <RobotAPI/components/units/RobotUnit/util/RtLogging.h>
#include <armarx/control/ethercat/Bus.h>

#include <devices/ethercat/common/h2t_devices.h>


namespace devices::ethercat::common::sensor_board::armar7de
{
    Slave::Slave(const armarx::control::ethercat::SlaveIdentifier slaveIdentifier_) :
        SlaveInterfaceWithIO(slaveIdentifier_),
        errorDecoder(nullptr, nullptr),
        bus(armarx::control::ethercat::Bus::getBus())
    {
        setTag(this->slaveIdentifier.getName());
    }


    void
    Slave::execute()
    {
    }


    bool
    Slave::prepareForRun()
    {
        return true;
    }


    bool
    Slave::shutdown()
    {
        return true;
    }


    void
    Slave::doMappings()
    {
    }


    bool
    Slave::hasError()
    {
        // Check for all errors and return false iff there are no errors
        bool hasError = false;

        // Print the reason for each error
        if (errorDecoder.motorTemperatureErrorDetected())
        {
            SLAVE_WARNING(getSlaveIdentifier(), "Motor temperature error detected")
                .deactivateSpam(1);
            hasError = true;
        }
        // Print these errors too
        hasError |= errorDecoder.relativeEncoderErrorDetected(this, true);
        hasError |= errorDecoder.absoluteEncoderErrorDetected(this, true);

        if (errorDecoder.imuErrorDetected())
        {
            SLAVE_WARNING(getSlaveIdentifier(), "Imu communication error detected")
                .deactivateSpam(1);
            hasError = true;
        }
        return hasError;
    }


    void
    Slave::prepareForSafeOp()
    {
        writeCoE();
    }


    void
    Slave::finishPreparingForSafeOp()
    {
    }


    void
    Slave::prepareForOp()
    {
        errorDecoder = ErrorDecoder(getOutputsPtr(), getInputsPtr());
    }


    void
    Slave::finishPreparingForOp()
    {
    }

    bool
    Slave::isEmergencyStopActive() const
    {
        return errorDecoder.getEmergencyStopStatus();
    }

    bool
    Slave::recoverFromEmergencyStop()
    {
        return true;
    }

    bool
    Slave::isSlaveIdentifierAccepted(const armarx::control::ethercat::SlaveIdentifier& sid)
    {
        const std::uint32_t correctProductCode = 0x3100;
        return sid.vendorID == H2TVendorId and sid.productCode == correctProductCode;
    }

    std::string
    Slave::getDefaultName()
    {
        return "Armar7deSensorBoard";
    }

    std::uint8_t
    Slave::readTorquePgaAndSps()
    {
        bool success = bus.readSDOEntry(getSlaveNumber(), 0x8000, 1, torquePgaAndSps);
        if (!success)
        {
            SLAVE_ERROR(getSlaveIdentifier(), "Could not read TorquePgaAndSps CoE entry");
        }
        return torquePgaAndSps;
    }

    void
    Slave::setTorquePgaAndSps(std::uint8_t torquePgaAndSps)
    {
        this->torquePgaAndSps = torquePgaAndSps;
    }

    std::uint16_t
    Slave::readTorqueAdcTempFrequency()
    {
        bool success = bus.readSDOEntry(getSlaveNumber(), 0x8000, 3, torqueAdcTempFrequency);
        if (!success)
        {
            SLAVE_ERROR(getSlaveIdentifier(), "Could not read TorqueAdcTempFrequency CoE entry");
        }
        return torqueAdcTempFrequency;
    }

    void
    Slave::setTorqueAdcTempFrequency(std::uint16_t torqueAdcTempFrequency)
    {
        if (torqueAdcTempFrequency < 1 || torqueAdcTempFrequency > 1000)
        {
            SLAVE_WARNING(getSlaveIdentifier(),
                          "Trying to set invalid TorqueAdcTempFrequency: %d",
                          +torqueAdcTempFrequency);
        }
        this->torqueAdcTempFrequency = torqueAdcTempFrequency;
    }

    std::uint8_t
    Slave::readDisableImuCalibAccel()
    {
        bool success = bus.readSDOEntry(getSlaveNumber(), 0x8000, 4, disableImuCalibAccel);
        if (!success)
        {
            SLAVE_ERROR(getSlaveIdentifier(), "Could not read DisableImuCalibAccel CoE entry");
        }
        return disableImuCalibAccel;
    }

    void
    Slave::setDisableImuCalibAccel(std::uint8_t disableImuCalibAccel)
    {
        if (disableImuCalibAccel != 0 && disableImuCalibAccel != 1)
        {
            SLAVE_WARNING(getSlaveIdentifier(),
                          "Trying to set DisableImuCalibAccel to an invalid value: %d",
                          +disableImuCalibAccel);
        }
        this->disableImuCalibAccel = disableImuCalibAccel;
    }

    std::uint8_t
    Slave::readDisableImuCalibGyro()
    {
        bool success = bus.readSDOEntry(getSlaveNumber(), 0x8000, 5, disableImuCalibGyro);
        if (!success)
        {
            SLAVE_ERROR(getSlaveIdentifier(), "Could not read DisableImuCalibGyro CoE entry");
        }
        return disableImuCalibGyro;
    }

    void
    Slave::setDisableImuCalibGyro(std::uint8_t disableImuCalibGyro)
    {
        if (disableImuCalibGyro != 0 && disableImuCalibGyro != 1)
        {
            SLAVE_WARNING(getSlaveIdentifier(),
                          "Trying to set DisableImuCalibGyro to an invalid value: %d",
                          +disableImuCalibGyro);
        }
        this->disableImuCalibGyro = disableImuCalibGyro;
    }

    std::uint8_t
    Slave::readDisableImuCalibMag()
    {
        bool success = bus.readSDOEntry(getSlaveNumber(), 0x8000, 6, disableImuCalibMag);
        if (!success)
        {
            SLAVE_ERROR(getSlaveIdentifier(), "Could not read DisableImuCalibMag CoE entry");
        }
        return disableImuCalibMag;
    }

    void
    Slave::setDisableImuCalibMag(std::uint8_t disableImuCalibMag)
    {
        if (disableImuCalibMag != 0 && disableImuCalibMag != 1)
        {
            SLAVE_WARNING(getSlaveIdentifier(),
                          "Trying to set DisableImuCalibMag to an invalid value: %d",
                          +disableImuCalibMag);
        }
        this->disableImuCalibMag = disableImuCalibMag;
    }

    std::uint8_t
    Slave::readLedStripCount()
    {
        bool success = bus.readSDOEntry(getSlaveNumber(), 0x8000, 7, ledStripCount);
        if (!success)
        {
            SLAVE_ERROR(getSlaveIdentifier(), "Could not read LedStripCount CoE entry");
        }
        return ledStripCount;
    }

    void
    Slave::setLedStripCount(std::uint8_t ledStripCount)
    {
        this->ledStripCount = ledStripCount;
    }

    void
    Slave::writeCoE()
    {
        // Write torquePgaAndSps
        bool success = bus.writeSDOEntry(getSlaveNumber(), 0x8000, 1, torquePgaAndSps);
        if (!success)
        {
            SLAVE_ERROR(getSlaveIdentifier(), "Could not write TorquePgaAndSps CoE entry");
        }

        // Write torqueAdcTempFrequency
        success = bus.writeSDOEntry(getSlaveNumber(), 0x8000, 3, torqueAdcTempFrequency);
        if (!success)
        {
            SLAVE_ERROR(getSlaveIdentifier(), "Could not write TorqueAdcTempFrequency CoE entry");
        }

        // Write disableImuCalibAccel
        success = bus.writeSDOEntry(getSlaveNumber(), 0x8000, 4, disableImuCalibAccel);
        if (!success)
        {
            SLAVE_ERROR(getSlaveIdentifier(), "Could not write DisableImuCalibAccel CoE entry");
        }

        // Write disableImuCalibGyro
        success = bus.writeSDOEntry(getSlaveNumber(), 0x8000, 5, disableImuCalibGyro);
        if (!success)
        {
            SLAVE_ERROR(getSlaveIdentifier(), "Could not write DisableImuCalibGyro CoE entry");
        }

        // Write disableImuCalibMag
        success = bus.writeSDOEntry(getSlaveNumber(), 0x8000, 6, disableImuCalibMag);
        if (!success)
        {
            SLAVE_ERROR(getSlaveIdentifier(), "Could not write DisableImuCalibMag CoE entry");
        }

        // Write ledStripCount
        success = bus.writeSDOEntry(getSlaveNumber(), 0x8000, 7, ledStripCount);
        if (!success)
        {
            SLAVE_ERROR(getSlaveIdentifier(), "Could not write LedStripCount CoE entry");
        }
    }


} // namespace devices::ethercat::common::sensor_board::armar7de
