#include "Data.h"

#include <SimoxUtility/math/periodic_clamp.h>

namespace devices::ethercat::common::sensor_board::armar7de
{
    Data::Data(SensorBoardConfig config,
               SlaveOut* outputs,
               SlaveIn* inputs) :
        outputs(outputs), inputs(inputs), errorDecoder(outputs, inputs)
    {
        ARMARX_CHECK_EXPRESSION(outputs);
        ARMARX_CHECK_EXPRESSION(inputs);

        voltage.init(&(outputs->VoltageMonitor), config.voltage);
        absoluteEncoder.init(&(outputs->AbsoluteEncoderValue),
                             config.absoluteEncoder);
        torque.init(&(outputs->TorqueADCValue),
                    config.torqueFactor,
                    config.torqueOffset,
                    config.torquePgaAndSps);
    }

    void
    Data::rtReadSensorValues(const IceUtil::Time& /*sensorValuesTimestamp*/,
                             const IceUtil::Time& /*timeSinceLastIteration*/)
    {
        voltage.read();
        mainUpdateRate = outputs->MainUpdateRate;
        motorTemp = motorTempBitshiftDetector.update(readAndConvertMotorTemperatureValue());
        torque.read();
        torqueAdcTemp =
            torqueAdcTempBitshiftDetector.update(readAndConvertTorqueAdcTemperatureValue());
        absoluteEncoder.read();
        absEncoderTemp =
            absEncoderTempBitshiftDetector.update(readAndConvertAbsEncoderTemperatureValue());
        readAndConvertIMUAcceleration();
        readAndConvertIMUOrientation();
        readAndConvertIMUAccelerationAccuracy();
        readAndConvertIMUOrientationAccuracy();
        readAndConvertMotorTemperatureError();
        readAndConvertRelativeEncoderStatus();
        readAndConvertEmergencyStopStatus();
        readAndConvertImuError();
    }

    void
    Data::rtWriteTargetValues(const IceUtil::Time& /*sensorValuesTimestamp*/,
                              const IceUtil::Time& /*timeSinceLastIteration*/)
    {
        inputs->RedChannel = red;
        inputs->GreenChannel = green;
        inputs->BlueChannel = blue;

        if (includeMagnetometer)
        {
            inputs->flags |= 0b00000001;
        }
        else
        {
            inputs->flags &= ~0b00000001;
        }

        if (includeGravity)
        {
            inputs->flags |= 0b00000010;
        }
        else
        {
            inputs->flags &= ~0b00000010;
        }
    }

    float
    Data::getVoltage() const
    {
        return voltage.value;
    }

    std::uint16_t
    Data::getMainUpdateRate() const
    {
        return mainUpdateRate;
    }

    float
    Data::getMotorTemperature() const
    {
        return motorTemp;
    }

    float
    Data::getActualTorque() const
    {
        return torque.value;
    }

    float
    Data::getTorqueAdcTemperature() const
    {
        return torqueAdcTemp;
    }

    float
    Data::getActualPosition() const
    {
        return absoluteEncoder.value;
    }

    float
    Data::getAbsoluteEncoderTemperature() const
    {
        return absEncoderTemp;
    }

    std::uint16_t
    Data::getAbsoluteEncoderDetailedStatus() const
    {
        return outputs->AbsoluteEncoderDetailedStatus;
    }

    const Eigen::Vector3f&
    Data::getImuAcceleration() const
    {
        return imuAcceleration;
    }

    const Eigen::Quaternionf&
    Data::getImuOrientation() const
    {
        return imuOrientation;
    }

    std::uint8_t
    Data::getImuAccelerationAccuracy() const
    {
        return imuAccelerationAccuracy;
    }

    std::uint8_t
    Data::getImuOrientationAccuracy() const
    {
        return imuOrientationAccuracy;
    }

    std::int32_t
    Data::getTorqueTicks() const
    {
        return torque.getRaw();
    }

    std::uint32_t
    Data::getAbsoluteEncoderTicks() const
    {
        return absoluteEncoder.getRaw();
    }

    bool
    Data::hasMotorTemperatureError() const
    {
        return motorTemperatureError;
    }

    std::uint8_t
    Data::getRelativeEncoderStatus() const
    {
        return relativeEncoderStatus;
    }

    bool
    Data::getEmergencyStopStatus() const
    {
        return emergencyStopStatus;
    }

    bool
    Data::hasImuError() const
    {
        return imuError;
    }

    bool
    Data::hasTemperatureBitshiftOccured(TemperatureType type) const
    {
        switch (type)
        {
            case TemperatureType::MotorAdcTemperature:
                return motorTempBitshiftDetector.getLastShiftDirection() !=
                       Data::TemperatureBitshiftDetector<float>::ShiftDirection::None;
            case TemperatureType::TorqueAdcTemperature:
                return torqueAdcTempBitshiftDetector.getLastShiftDirection() !=
                       Data::TemperatureBitshiftDetector<float>::ShiftDirection::None;
            case TemperatureType::AbsoluteEncoderTemperature:
                return absEncoderTempBitshiftDetector.getLastShiftDirection() !=
                       Data::TemperatureBitshiftDetector<float>::ShiftDirection::None;
        }
        return false;
    }


    void
    Data::setRedLedIntensity(std::uint8_t redIntensity)
    {
        this->red = redIntensity;
    }

    void
    Data::setGreenLedIntensity(std::uint8_t greenIntensity)
    {
        this->green = greenIntensity;
    }

    void
    Data::setBlueLedIntensity(std::uint8_t blueIntensity)
    {
        this->blue = blueIntensity;
    }

    void
    Data::setImuIncludeMagnetometerInOrientation(bool includeMagnetometer)
    {
        this->includeMagnetometer = includeMagnetometer;
    }

    void
    Data::setImuIncludeGravityInAcceleration(bool includeGravity)
    {
        this->includeGravity = includeGravity;
    }

    float
    Data::readAndConvertMotorTemperatureValue()
    {
        std::uint16_t adc = outputs->MotorTemperatureADCValue;
        if ((0x2000 & adc) == 0x2000) // Temperature is negative
        {
            return (adc - 16384) / 32.f;
        }
        else
        {
            return adc / 32.f;
        }
    }

    float
    Data::readAndConvertTorqueAdcTemperatureValue()
    {
        return 25.f +
               (static_cast<float>(outputs->TorqueADCTempValue) - ((1 << 23) * 0.118f) / v_ref) /
                   (v_ref / 0.00081f);
    }

    float
    Data::readAndConvertAbsEncoderTemperatureValue()
    {
        return static_cast<float>(outputs->AbsoluteEncoderTemperature);
    }

    void
    Data::readAndConvertIMUAcceleration()
    {
        imuAcceleration(0) = intToQFloat(outputs->IMUAccelerationX, 8);
        imuAcceleration(1) = intToQFloat(outputs->IMUAccelerationY, 8);
        imuAcceleration(2) = intToQFloat(outputs->IMUAccelerationZ, 8);
    }

    void
    Data::readAndConvertIMUOrientation()
    {
        imuOrientation.w() = intToQFloat(outputs->IMUOrientationW, 14);
        imuOrientation.x() = intToQFloat(outputs->IMUOrientationX, 14);
        imuOrientation.y() = intToQFloat(outputs->IMUOrientationY, 14);
        imuOrientation.z() = intToQFloat(outputs->IMUOrientationZ, 14);
    }

    void
    Data::readAndConvertIMUAccelerationAccuracy()
    {
        imuAccelerationAccuracy = (outputs->IMUSingleBits & 0b00000011);
    }

    void
    Data::readAndConvertIMUOrientationAccuracy()
    {
        imuOrientationAccuracy = (outputs->IMUSingleBits & 0b00001100) >> 2;
    }

    void
    Data::readAndConvertMotorTemperatureError()
    {
        motorTemperatureError = errorDecoder.motorTemperatureErrorDetected();
    }

    void
    Data::readAndConvertRelativeEncoderStatus()
    {
        relativeEncoderStatus = errorDecoder.getEncoderStatus();
    }

    void
    Data::readAndConvertEmergencyStopStatus()
    {
        emergencyStopStatus = errorDecoder.getEmergencyStopStatus();
    }

    void
    Data::readAndConvertImuError()
    {
        imuError = errorDecoder.imuErrorDetected();
    }
} // namespace devices::ethercat::common::sensor_board::armar7de
