armarx_add_library(common_sensor_board_armar7de
    SOURCES
    Config.cpp
        Data.cpp
        ErrorDecoder.cpp
        Slave.cpp
    HEADERS
        Config.h
        Data.h
        ErrorDecoder.h
        SlaveIO.h
        Slave.h
    DEPENDENCIES
        RobotAPIUnits
        armarx_control::ethercat
    DEPENDENCIES_LEGACY
        SOEM
)

target_compile_options(common_sensor_board_armar7de
    PRIVATE
        -Wall -Wextra -Wpedantic -Werror
)

if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    if(CMAKE_CXX_COMPILER_VERSION VERSION_GREATER_EQUAL "9")
        target_compile_options(common_sensor_board_armar7de
            PRIVATE
                -Wno-error=address-of-packed-member
        )
    endif()
endif()
