#include "Config.h"

namespace devices::ethercat::common::sensor_board::armar7de
{
    SensorBoardConfig::SensorBoardConfig(hwconfig::SlaveConfig& config)
        : torquePgaAndSps(config.getUint("TorquePgaAndSps")),
          torqueAdcTempFrequency(config.getUint("TorqueAdcTempFrequency")),
          disableCalibAccel(config.getBool("disableCalibAccel")),
          disableCalibGyro(config.getBool("disableCalibGyro")),
          disableCalibMag(config.getBool("disableCalibMag")),
          ledStripCount(config.getUint("ledStripCount")),
          voltage(config.getLinearConfig("voltage")),
          absoluteEncoder(config.getModularConvertedValueConfig("absoluteEncoder")),
          torqueFactor(config.getFloat("torqueFactorUVtoNm")),
          torqueOffset(config.getFloat("torqueOffsetInUV"))
    {
    }
}
