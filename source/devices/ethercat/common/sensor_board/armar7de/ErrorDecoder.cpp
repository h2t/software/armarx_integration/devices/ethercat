#include "ErrorDecoder.h"

#include <armarx/control/ethercat/ErrorReporting.h>
#include <armarx/control/ethercat/SlaveInterface.h>


namespace devices::ethercat::common::sensor_board::armar7de
{
    ErrorDecoder::ErrorDecoder(SlaveOut* outputs, SlaveIn* inputs) :
        outputs(outputs), inputs(inputs)
    {
    }

    bool
    ErrorDecoder::motorTemperatureErrorDetected() const
    {
        return (0x4000 & outputs->StatusBits) ? true : false;
    }

    RelativeEncoderStatus
    ErrorDecoder::getEncoderStatus() const
    {
        std::uint16_t relativeEncStatus = (0x3000 & outputs->StatusBits) >> 12;
        return static_cast<RelativeEncoderStatus>(relativeEncStatus);
    }

    bool
    ErrorDecoder::relativeEncoderErrorDetected(armarx::control::ethercat::SlaveInterface* slave,
                                               bool printError) const
    {
        RelativeEncoderStatus status = getEncoderStatus();

        if (printError)
        {
            switch (status)
            {
                case RelativeEncoderStatus::EncoderOK:
                    return false;
                case RelativeEncoderStatus::EncoderHasNoPower:
                    SLAVE_ERROR(slave->getSlaveIdentifier(), "Relative encoder has no power")
                        .deactivateSpam(1);
                    return true;
                case RelativeEncoderStatus::EncoderHasNoSignal:
                    SLAVE_ERROR(slave->getSlaveIdentifier(), "Relative encoder has no signal")
                        .deactivateSpam(1);
                    return true;
                case RelativeEncoderStatus::SpeedIsTooHigh:
                    SLAVE_ERROR(slave->getSlaveIdentifier(), "Relative encoder: speed is too high")
                        .deactivateSpam(1);
                    return true;
            }
        }

        return status != RelativeEncoderStatus::EncoderOK;
    }

    std::uint16_t
    ErrorDecoder::getAbsoluteEncoderStatus() const
    {
        return outputs->AbsoluteEncoderDetailedStatus;
    }

    bool
    ErrorDecoder::absoluteEncoderErrorDetected(armarx::control::ethercat::SlaveInterface* slave,
                                               bool printError) const
    {
        std::uint16_t errors = getAbsoluteEncoderStatus();

        if (printError)
        {
            if (errors & AbsoluteEncoderStatus::ErrorAccelerationError)
            {
                SLAVE_ERROR(
                    slave->getSlaveIdentifier(),
                    "Absolute encoder: Acceleration error. The position "
                    "data changed too fast. A stray magnetic field is present or metal particles "
                    "are present between the readhead and the ring")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::ErrorEncoderNotConfiguredProperly)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Absolute encoder: ERROR - encoder not "
                            "configured properly")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::ErrorMagneticPatternError)
            {
                SLAVE_ERROR(
                    slave->getSlaveIdentifier(),
                    "Absolute encoder: Magnetic pattern error. A stray "
                    "magnetic field is present or metal particles are present between the readhead "
                    "and the ring or radial positioning between the readhead and the ring is out "
                    "of tolerances.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::ErrorMagneticSensor)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Absolute encoder: Magnetic sensor. "
                            "Cycle power to the encoder.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::ErrorMultiturnCounterMismatch)
            {
                SLAVE_ERROR(
                    slave->getSlaveIdentifier(),
                    "Absolute encoder: Multiturn counter mismatch. Encoder "
                    "was rotated for more than ±90° during power-down. Cycle the power to clear "
                    "this error or apply new multiturn counter value.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::ErrorPositionNotValid)
            {
                ARMARX_RT_LOGF_WARNING("Slave '%s': Absolute encoder: If bit is set, "
                                       "position is not valid.",
                                       slave->getSlaveIdentifier().getNameAsCStr())
                    .deactivateSpam(30);
            }
            if (errors & AbsoluteEncoderStatus::ErrorPowerSupplyError)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Absolute encoder: Power supply error. The readhead "
                            "power supply voltage is out of specified range.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::ErrorSensorReadingError)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Absolute encoder: Sensor reading error, probably "
                            "caused by electrical interference, ground loop or RFI.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::ErrorSignalAmplitudeTooHigh)
            {
                SLAVE_ERROR(
                    slave->getSlaveIdentifier(),
                    "Absolute encoder: Signal amplitude too high. The "
                    "readhead is too close to the ring or an external magnetic field is present.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::ErrorSignalLost)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Absolute encoder: Signal lost. The readhead is out of "
                            "alignment with the ring or the ring is damaged.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::ErrorSystemError)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Absolute encoder: System error. "
                            "Malfunction inside the circuitry or inconsistent calibration "
                            "data is detected. To reset the System error bit try to cycle "
                            "the power supply while the rise time is shorter than 20 ms.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::WarningEncoderNearOperationalLimits)
            {
                ARMARX_RT_LOGF_WARNING(
                    "Slave '%s': Absolute encoder: If bit is set, encoder is near "
                    "operational limits. Position is valid. Resolution and / or accuracy might be "
                    "lower than specified.",
                    slave->getSlaveIdentifier().getNameAsCStr())
                    .deactivateSpam(30);
            }
            if (errors & AbsoluteEncoderStatus::WarningSignalAmplitudeLow)
            {
                SLAVE_WARNING(slave->getSlaveIdentifier(),
                              "Absolute encoder: Signal amplitude low. The distance "
                              "between the readhead and the ring is too large.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::WarningSignalAmplitudeTooHigh)
            {
                SLAVE_WARNING(
                    slave->getSlaveIdentifier(),
                    "Absolute encoder: Signal amplitude too high. The "
                    "readhead is too close to the ring or an external magnetic field is present.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::WarningSignalAmplitudeTooHigh2)
            {
                SLAVE_WARNING(
                    slave->getSlaveIdentifier(),
                    "Absolute encoder: Signal amplitude too high. The "
                    "readhead is too close to the ring or an external magnetic field is present.")
                    .deactivateSpam(1);
            }
            if (errors & AbsoluteEncoderStatus::WarningTemperature)
            {
                SLAVE_WARNING(slave->getSlaveIdentifier(),
                              "Absolute encoder: Temperature. The "
                              "readhead temperature is out of specified range.")
                    .deactivateSpam(1);
            }
        }

        // Return true iff there are errors
        return (errors &
                (AbsoluteEncoderStatus::ErrorAccelerationError |
                 AbsoluteEncoderStatus::ErrorEncoderNotConfiguredProperly |
                 AbsoluteEncoderStatus::ErrorMagneticPatternError |
                 AbsoluteEncoderStatus::ErrorMagneticSensor |
                 AbsoluteEncoderStatus::ErrorMultiturnCounterMismatch |
                 AbsoluteEncoderStatus::ErrorPositionNotValid |
                 AbsoluteEncoderStatus::ErrorPowerSupplyError |
                 AbsoluteEncoderStatus::ErrorSensorReadingError |
                 AbsoluteEncoderStatus::ErrorSignalAmplitudeTooHigh |
                 AbsoluteEncoderStatus::ErrorSignalLost | AbsoluteEncoderStatus::ErrorSystemError))
                   ? true
                   : false;
    }

    bool
    ErrorDecoder::imuErrorDetected() const
    {
        return ((outputs->IMUSingleBits & 0b00010000) >> 4) ? true : false;
    }

    bool
    ErrorDecoder::getEmergencyStopStatus() const
    {
        ARMARX_CHECK_NOT_NULL(outputs);
        return (0x0800 & outputs->StatusBits) ? true : false;
    }

} // namespace devices::ethercat::common::sensor_board::armar7de
