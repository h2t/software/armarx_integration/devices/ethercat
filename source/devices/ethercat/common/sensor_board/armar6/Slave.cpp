#include "Slave.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <RobotAPI/components/units/RobotUnit/util/RtLogging.h>
#include <armarx/control/ethercat/Bus.h>

#include <devices/ethercat/common/h2t_devices.h>
#include <devices/ethercat/common/sensor_board/armar6/ADS1263CRCHelper.h>
#include <devices/ethercat/sensor_actor_unit/armar6/Device.h>


#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof(*(arr)))


namespace devices::ethercat::common::sensor_board::armar6
{

    // Taken from Appendix 1 in https://i61wiki.itec.uka.de/redmine/projects/armar-6-sh/repository/absolute-encoder-aksim/changes/Dokumente/Technical_article_AksIm_encoder.pdf?rev=master
    //poly = 0x97
    static std::uint8_t tableCRC[256] = {
        0x00, 0x97, 0xB9, 0x2E, 0xE5, 0x72, 0x5C, 0xCB, 0x5D, 0xCA, 0xE4, 0x73, 0xB8, 0x2F, 0x01,
        0x96, 0xBA, 0x2D, 0x03, 0x94, 0x5F, 0xC8, 0xE6, 0x71, 0xE7, 0x70, 0x5E, 0xC9, 0x02, 0x95,
        0xBB, 0x2C, 0xE3, 0x74, 0x5A, 0xCD, 0x06, 0x91, 0xBF, 0x28, 0xBE, 0x29, 0x07, 0x90, 0x5B,
        0xCC, 0xE2, 0x75, 0x59, 0xCE, 0xE0, 0x77, 0xBC, 0x2B, 0x05, 0x92, 0x04, 0x93, 0xBD, 0x2A,
        0xE1, 0x76, 0x58, 0xCF, 0x51, 0xC6, 0xE8, 0x7F, 0xB4, 0x23, 0x0D, 0x9A, 0x0C, 0x9B, 0xB5,
        0x22, 0xE9, 0x7E, 0x50, 0xC7, 0xEB, 0x7C, 0x52, 0xC5, 0x0E, 0x99, 0xB7, 0x20, 0xB6, 0x21,
        0x0F, 0x98, 0x53, 0xC4, 0xEA, 0x7D, 0xB2, 0x25, 0x0B, 0x9C, 0x57, 0xC0, 0xEE, 0x79, 0xEF,
        0x78, 0x56, 0xC1, 0x0A, 0x9D, 0xB3, 0x24, 0x08, 0x9F, 0xB1, 0x26, 0xED, 0x7A, 0x54, 0xC3,
        0x55, 0xC2, 0xEC, 0x7B, 0xB0, 0x27, 0x09, 0x9E, 0xA2, 0x35, 0x1B, 0x8C, 0x47, 0xD0, 0xFE,
        0x69, 0xFF, 0x68, 0x46, 0xD1, 0x1A, 0x8D, 0xA3, 0x34, 0x18, 0x8F, 0xA1, 0x36, 0xFD, 0x6A,
        0x44, 0xD3, 0x45, 0xD2, 0xFC, 0x6B, 0xA0, 0x37, 0x19, 0x8E, 0x41, 0xD6, 0xF8, 0x6F, 0xA4,
        0x33, 0x1D, 0x8A, 0x1C, 0x8B, 0xA5, 0x32, 0xF9, 0x6E, 0x40, 0xD7, 0xFB, 0x6C, 0x42, 0xD5,
        0x1E, 0x89, 0xA7, 0x30, 0xA6, 0x31, 0x1F, 0x88, 0x43, 0xD4, 0xFA, 0x6D, 0xF3, 0x64, 0x4A,
        0xDD, 0x16, 0x81, 0xAF, 0x38, 0xAE, 0x39, 0x17, 0x80, 0x4B, 0xDC, 0xF2, 0x65, 0x49, 0xDE,
        0xF0, 0x67, 0xAC, 0x3B, 0x15, 0x82, 0x14, 0x83, 0xAD, 0x3A, 0xF1, 0x66, 0x48, 0xDF, 0x10,
        0x87, 0xA9, 0x3E, 0xF5, 0x62, 0x4C, 0xDB, 0x4D, 0xDA, 0xF4, 0x63, 0xA8, 0x3F, 0x11, 0x86,
        0xAA, 0x3D, 0x13, 0x84, 0x4F, 0xD8, 0xF6, 0x61, 0xF7, 0x60, 0x4E, 0xD9, 0x12, 0x85, 0xAB,
        0x3C};


    // use this function to calculate CRC from 32-bit number
    static std::uint8_t
    crc8_4B(std::uint32_t bb)
    {
        std::uint8_t crc;
        std::uint32_t t;
        t = (bb >> 24) & 0x000000FF;
        //    ARMARX_INFO << VAROUT(t);
        crc = ((bb >> 16) & 0x000000FF);
        t = crc ^ tableCRC[t];
        //    ARMARX_INFO << VAROUT(t);

        crc = ((bb >> 8) & 0x000000FF);
        t = crc ^ tableCRC[t];
        //    ARMARX_INFO << VAROUT(t);
        crc = (bb & 0x000000FF);
        t = crc ^ tableCRC[t];
        //    ARMARX_INFO << VAROUT(t);
        crc = tableCRC[t];
        //    ARMARX_INFO << VAROUT((int)crc);

        return crc;
    }


    Slave::Slave(const armarx::control::ethercat::SlaveIdentifier slaveIdentifier) :
        SlaveInterfaceWithIO(slaveIdentifier),
        sdoHelper(&armarx::control::ethercat::Bus::getBus(),
                  static_cast<std::uint16_t>(slaveIdentifier.slaveIndex),
                  slaveIdentifier.getName())
    {
        setTag(this->slaveIdentifier.getName());
    }


    void
    Slave::execute()
    {
        if (checkAbsEncoderReading)
        {
            std::uint32_t corruptedPositionEncoderData =
                (static_cast<std::uint32_t>(outputs->RawABSEncoderValueBytes[0]) << 24 |
                 static_cast<std::uint32_t>(outputs->RawABSEncoderValueBytes[1]) << 16 |
                 static_cast<std::uint32_t>(outputs->RawABSEncoderValueBytes[2]) << 8 |
                 static_cast<std::uint32_t>(outputs->RawABSEncoderValueBytes[3]));

            // Ok, here comes the crazy bits:
            // There is an error in the firmware of the actor units which produce a right shift in the data.
            //
            // Effects on the encoder data:
            //   This leads to the most significant (MSB) bit to be always zero (assumed, not proven).
            //   The least significant bit (LSB) has been shifted into the status bits.
            //   This is the reason that we have detected 50 % error bits previously.
            //   Effectively, we lost the LSB of the absolute encoder.
            //   You can see the effect in the hardware config (2^19 resolution)
            //   We could restore the full encoder value, but we will leave it as it is to prevent config changes.

            // For the CRC check, we need to restore the encoder data by
            //  - left shifting by 1 and
            //  - setting the LSB to 1 (reserved bit is always 1)
            std::uint32_t restoredPositionEncoderData = corruptedPositionEncoderData << 1 | 0x1u;

            // Effects on the CRC
            //   The LSB of the status bits (reserved, always 1) is shifted into the CRC's MSB.
            //   The least significant bit of the CRC is lost.
            //   To restore the CRC, we need to
            //    - left shift by 1 and
            //    - guess the LSB (two options)
            std::uint8_t corruptedCRC = outputs->RawABSEncoderValueCRC;
            std::uint8_t restoredCRC_0 = static_cast<std::uint8_t>(corruptedCRC << 1) | 0x0u;
            std::uint8_t restoredCRC_1 = static_cast<std::uint8_t>(corruptedCRC << 1) | 0x1u;

            std::uint8_t crc = crc8_4B(restoredPositionEncoderData);
            //            ARMARX_IMPORTANT << "Data: " << std::hex << restoredPositionEncoderData
            //                             << " CRC Ethercat 0: " << std::hex << (uint32_t)restoredCRC_0
            //                             << " CRC Ethercat 1: " << std::hex << (uint32_t)restoredCRC_1
            //                             << " CRC Computed: " << std::hex << (uint32_t)crc;

            if (crc != restoredCRC_0 && crc != restoredCRC_1)
            {
                stats.crcFailed += 1;
            }

            // Gather statistics
            if (stats.totalCount == UINT64_MAX)
            {
                stats = SensorBoardStatistics();
            }
            stats.totalCount += 1;
            for (std::uint32_t i = 0; i < ARRAY_SIZE(stats.bitCount); ++i)
            {
                std::uint32_t mask = 1 << i;
                stats.bitCount[i] += (restoredPositionEncoderData & mask) ? 1 : 0;
            }
        }

        std::uint8_t calcTorqueCRC = ADS1263CRCHelper::CalculateCRC8_Torque(outputs->TorqueValue);
        if (calcTorqueCRC != outputs->TorqueValueCRC)
        {
            SLAVE_WARNING(getSlaveIdentifier(),
                          "Torque CRC failed. Expected %#02X, but got %#02X",
                          outputs->TorqueValueCRC,
                          calcTorqueCRC)
                .deactivateSpam(1);
        }
        std::uint8_t calcTemperatureCRC =
            ADS1263CRCHelper::CalculateCRC8_Temperature(outputs->TorqueSensorTemperature);
        if (calcTemperatureCRC != outputs->TorqueSensorTemperatureCRC)
        {
            SLAVE_WARNING(getSlaveIdentifier(),
                          "Temperature CRC failed. Expected %#02X, but got %#02X",
                          outputs->TorqueSensorTemperatureCRC,
                          calcTemperatureCRC)
                .deactivateSpam(1);
        }
    }


    bool
    Slave::prepareForRun()
    {
        ARMARX_DEBUG << deactivateSpam(5) << "SensorBoard " << getSlaveIdentifier() << " is ready";
        return true;
    }


    bool
    Slave::shutdown()
    {
#if 0
        return true;
        sdoHelper.getImu().reset();
        auto calibrationData = sdoHelper.getImu().getCalibration();
        std::stringstream stream;
        for (auto element : calibrationData)
        {
            stream << element << " ";
        }
        ARMARX_IMPORTANT << "IMU Calibration: " << stream.str();
        ARMARX_INFO << "SensorBoard is shut down";
#endif
        if (!statsPrinted)
        {
            // Calculate statistics
            std::ostringstream message;
            message << "Statistics for " << getSlaveIdentifier().getName() << "\n";
            if (stats.totalCount > 0)
            {
                for (std::size_t i = 0; i < ARRAY_SIZE(stats.bitCount); ++i)
                {
                    double percentage = 100.0 * static_cast<double>(stats.bitCount[i]) /
                                        static_cast<double>(stats.totalCount);
                    const char* bitName = AbsoluteEncoderErrorBitNames[i];
                    message << "  " << bitName << ": " << percentage << "%\n";
                }
                double crcFailPercentage = 100.0 * static_cast<double>(stats.crcFailed) /
                                           static_cast<double>(stats.totalCount);
                message << "  CRC failed: " << crcFailPercentage << "%";
            }
            else
            {
                message << "No data to generate statistics (checkAbsEncoderReading = "
                        << checkAbsEncoderReading << ")";
            }
            ARMARX_INFO << message.str();

            ARMARX_INFO << "SensorBoard '" << getSlaveIdentifier().getName() << " is shut down";

            statsPrinted = true;
        }
        return true;
    }


    void
    Slave::doMappings()
    {
    }


    std::uint32_t
    Slave::getRawAbsValue()
    {
        std::uint32_t absEncoderWithStatus =
            ((static_cast<std::uint32_t>(outputs->RawABSEncoderValueBytes[0]) << 24 |
              static_cast<std::uint32_t>(outputs->RawABSEncoderValueBytes[1]) << 16 |
              static_cast<std::uint32_t>(outputs->RawABSEncoderValueBytes[2]) << 8 |
              static_cast<std::uint32_t>(outputs->RawABSEncoderValueBytes[3])) &
             0xFFFFF000);
        return absEncoderWithStatus >> 12;
    }


    const std::int16_t*
    Slave::getGearTemperatur()
    {
        return &outputs->GearboxTemperature;
    }


    const std::int16_t*
    Slave::getMotorTemperatur()
    {
        return &outputs->MotorTemperature;
    }


    const std::int32_t*
    Slave::getTorqueValue()
    {
        return &outputs->TorqueValue;
    }


    bool
    Slave::hasError()
    {
        return false;
    }


    bool
    Slave::getCheckAbsEncoderReading() const
    {
        return checkAbsEncoderReading;
    }


    void
    Slave::setCheckAbsEncoderReading(bool value)
    {
        //    ARMARX_IMPORTANT << "Setting checkAbsEncoderReading to " << checkAbsEncoderReading;
        checkAbsEncoderReading = value;
    }


    bool
    Slave::getUseTMP007() const
    {
        return useTMP007;
    }


    void
    Slave::setUseTMP007(bool value)
    {
        useTMP007 = value;
    }


    void
    Slave::prepareForSafeOp()
    {
        if (initTask.joinable())
        {
            ARMARX_WARNING << "Waiting for old initthread!";
            initTask.join();
        }
        preparingFinished = false;
        if (useIMU)
        {
            initTask = std::thread{
                [this]
                {
                    try
                    {
                        sdoHelper.getImu().setUseTMP007(useTMP007);
                        sdoHelper.getImu().reset();
                        sdoHelper.getImu().init();
                        ARMARX_INFO << getSlaveIdentifier()
                                    << "BNO055 online: " << sdoHelper.getImu().pingBNO055();
                        ARMARX_INFO << getSlaveIdentifier()
                                    << "TMP007 online: " << sdoHelper.getImu().pingTMP007();
                        if (IMUCalibrationData.size() > 0)
                        {
                            sdoHelper.getImu().setCalibration(IMUCalibrationData);
                        }
                        sdoHelper.getImu().start();
                    }
                    catch (...)
                    {
                        ARMARX_WARNING << "Sensorboard startup failed: "
                                       << armarx::GetHandledExceptionString();
                    }
                    preparingFinished = true;
                }};
        }
        else
        {
            ARMARX_DEBUG << "Usage of IMU disabled on this device!";
            preparingFinished = true;
        }
    }


    void
    Slave::finishPreparingForSafeOp()
    {
        if (initTask.joinable())
        {
            initTask.join();
        }
    }


    bool
    Slave::getUseIMU() const
    {
        return useIMU;
    }


    void
    Slave::setUseIMU(bool value)
    {
        useIMU = value;
    }


    void
    Slave::prepareForOp()
    {
        //    auto calibrationData = sdoHelper.getImu().calibrateSensor();
        //    std::stringstream stream;
        //    for (auto element : calibrationData)
        //    {
        //        stream << element << " ";
        //    }
        //    ARMARX_IMPORTANT << "IMU Calibration: " << stream.str();
    }


    void
    Slave::finishPreparingForOp()
    {
    }


    std::vector<std::uint16_t>
    Slave::getIMUCalibrationData() const
    {
        return IMUCalibrationData;
    }


    void
    Slave::setIMUCalibrationData(const std::vector<std::uint16_t>& value)
    {
        IMUCalibrationData = value;
    }


    std::string
    Slave::getDefaultName()
    {
        return "Armar6SensorBoard";
    }


    bool
    Slave::isSlaveIdentifierAccepted(const armarx::control::ethercat::SlaveIdentifier& sid)
    {
        // For the old sensor board, the serial number could not be encoded on the chip.
        // The product code was used instead.

        const std::vector<std::uint32_t> validProductCodes{
            0x102, 0x101, 0x203, 0x202, 0x206, 0x204, 0x105, 0x103, 0x106, 0x104, 0x201, 0x205,
            0x107, 0x108, 0x207, 0x208, 0x306, 0x307, 0x305, 0x303, 0x301, 0x304, 0x302};

        const bool productCodeMatches =
            std::find(validProductCodes.begin(), validProductCodes.end(), sid.productCode) !=
            validProductCodes.end();

        return sid.vendorID == H2TVendorId and sid.serialNumber == 0 /* or 1 */ and
               productCodeMatches;
    }

} // namespace devices::ethercat::common::sensor_board::armar6
