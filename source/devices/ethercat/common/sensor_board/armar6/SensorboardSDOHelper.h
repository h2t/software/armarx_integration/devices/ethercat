#pragma once


// STD/STL
#include <cstdint>
#include <vector>

// Ice
#include <IceUtil/Time.h>

// armarx
#include <ArmarXCore/core/logging/Logging.h>


namespace armarx::control::ethercat
{
    class Bus;
}


namespace devices::ethercat::common::sensor_board::armar6
{

    class IMUSDOHelper :
        public armarx::Logging
    {

    public:

        enum bno055_opmode_t :
            std::uint8_t
        {
            /* Operation mode settings*/
            OPERATION_MODE_CONFIG                                   = 0X00,
            OPERATION_MODE_ACCONLY                                  = 0X01,
            OPERATION_MODE_MAGONLY                                  = 0X02,
            OPERATION_MODE_GYRONLY                                  = 0X03,
            OPERATION_MODE_ACCMAG                                   = 0X04,
            OPERATION_MODE_ACCGYRO                                  = 0X05,
            OPERATION_MODE_MAGGYRO                                  = 0X06,
            OPERATION_MODE_AMG                                      = 0X07,
            OPERATION_MODE_IMUPLUS                                  = 0X08,
            OPERATION_MODE_COMPASS                                  = 0X09,
            OPERATION_MODE_M4G                                      = 0X0A,
            OPERATION_MODE_NDOF_FMC_OFF                             = 0X0B,
            OPERATION_MODE_NDOF                                     = 0X0C
        };

        enum sensorboard_command_t :
            std::uint8_t
        {
            COMMAND_NONE                              = 0x00,
            COMMAND_READ_STATUS                       = 0x01,
            COMMAND_SWITCH_MODE                       = 0x02,
            COMMAND_READ_CALIBRATION                  = 0x03,
            COMMAND_WRITE_CALIBRATION                 = 0x04,
            COMMAND_APPLY_REPORT_VECTOR_REGISTER      = 0x05,
            COMMAND_START_BNO055_CYCLIC_REPORTING     = 0x10,
            COMMAND_START_TMP007_CYCLIC_REPORTING     = 0x11,
            COMMAND_STOP_ALL_CYCLIC_REPORTING         = 0x12,
            COMMAND_START_CALIBRATION_STATUS_PRINTING = 0x20,
            COMMAND_STOP_CALIBRATION_STATUS_PRINTING  = 0x21,
            COMMAND_ENABLE_I2C_ISR                    = 0x30,
            COMMAND_DISABLE_I2C_ISR                   = 0x31,
            COMMAND_PING_BNO055                       = 0x40,
            COMMAND_PING_TMP007                       = 0x41,
            COMMAND_INIT_BNO055                       = 0x48
        };

        enum sensorboard_command_result_t :
            std::uint8_t
        {
            COMMAND_SUCCESS                            = 0x00,
            COMMAND_EXECUTING                          = 0x01,
            COMMAND_ERROR_ALREADY_REPORTING            = 0x10,
            COMMAND_ERROR_NOT_REPORTING                = 0x11,
            COMMAND_ERROR_INVALID_MODE                 = 0x12,
            COMMAND_ERROR_BUS_BUSY                     = 0x13,
            COMMAND_ERROR_ONLY_ALLOWED_IN_CONFIG_MODE  = 0x14,
            COMMAND_ERROR_CALIBRATION_PRINTING_ACTIVE  = 0x15,
            COMMAND_ERROR_NO_RESPONSE                  = 0x16,
            COMMAND_ERROR_INIT_FAILED                  = 0x17,
            COMMAND_ERROR_UNKNOWN_COMMAND              = 0xFF
        };

        IMUSDOHelper(armarx::control::ethercat::Bus* bus, std::uint16_t slaveNumber, std::string humanName);
        static std::string SensorBoardCommandToString(sensorboard_command_t command);
        static std::string SensorBoardCommandResultToString(sensorboard_command_result_t result);
        ~IMUSDOHelper() override {}

        sensorboard_command_result_t execCommand(sensorboard_command_t command, bool reportError = true, IceUtil::Time sleepTime = IceUtil::Time::seconds(0), int retries = 3) const;

        void reset() const;
        void init() const;
        void start() const;
        std::vector<std::uint16_t> calibrateSensor() const;
        std::vector<std::uint16_t> getCalibration() const;
        void setCalibration(std::vector<std::uint16_t> calibrationData);

        bool pingBNO055() const;
        bool pingTMP007() const;
        bool getUseTMP007() const;
        void setUseTMP007(bool value);

    private:

        armarx::control::ethercat::Bus* bus;
        std::uint16_t slaveNumber;
        std::string humanName;
        bool useTMP007 = true;

    };


    class SensorboardSDOHelper
    {

    public:

        SensorboardSDOHelper(armarx::control::ethercat::Bus* bus, std::uint16_t slaveNumber, std::string humanName);

        ~SensorboardSDOHelper() {}

        IMUSDOHelper& getImu();

    private:

        IMUSDOHelper imu;
        std::string humanName;

    };

}  // namespace devices::ethercat::common::sensor_board::v1
