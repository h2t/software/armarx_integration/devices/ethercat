#pragma once


#include <atomic>
#include <cstdint>
#include <memory>
#include <thread>

#include <armarx/control/ethercat/SlaveInterface.h>

#include <devices/ethercat/common/sensor_board/armar6/SensorboardSDOHelper.h>
#include <devices/ethercat/common/sensor_board/armar6/SlaveIO.h>


namespace devices::ethercat::common::sensor_board::armar6
{

    enum AbsoluteEncoderErrorBits
    {
        b0_Reserved,
        b1_Reserved,
        b2_AccelerationError,
        b3_MagneticPatternError,
        b4_SystemError,
        b5_PowerSupplyError,
        b6_TemperatureWarning,
        b7_SignalLostError,
        b8_SignalAmplitudeLowWarning,
        b9_SignalAmplitudeTooHighWarning,
        b10_Warning,
        b11_Error,

        AbsoluteEncoderErrorBit_Count
    };


    const char* const AbsoluteEncoderErrorBitNames[AbsoluteEncoderErrorBit_Count] = {
        "b0_Reserved",
        "b1_Reserved",
        "b2_AccelerationError",
        "b3_MagneticPatternError",
        "b4_SystemError",
        "b5_PowerSupplyError",
        "b6_TemperatureWarning",
        "b7_SignalLostError",
        "b8_SignalAmplitudeLowWarning",
        "b9_SignalAmplitudeTooHighWarning",
        "b10_Warning",
        "b11_Error"};


    struct SensorBoardStatistics
    {
        std::uint64_t bitCount[AbsoluteEncoderErrorBit_Count] = {};
        std::uint64_t crcFailed = 0;
        std::uint64_t totalCount = 0;
    };


    class Slave : public armarx::control::ethercat::SlaveInterfaceWithIO<SlaveIn, SlaveOut>
    {

    public:
        Slave(const armarx::control::ethercat::SlaveIdentifier slaveIdentifier);

        /**
         * @see AbstractSlave::doMappings
         */
        void doMappings() override;

        void prepareForOp() override;

        void finishPreparingForOp() override;

        /**
         * @see AbstractSlave::prepare()
         */
        bool prepareForRun() override;

        /**
         * @see AbstractSlave::execute()
         */
        void execute() override;

        /**
         * @see AbstractSlave::shutdown()
         */
        bool shutdown() override;

        /**
         * @see AbstractSlave::hasError()
         * @return
         */
        bool hasError() override;

        const std::int32_t* getTorqueValue();

        const std::int16_t* getMotorTemperatur();

        const std::int16_t* getGearTemperatur();

        std::uint32_t getRawAbsValue();

        bool getCheckAbsEncoderReading() const;
        void setCheckAbsEncoderReading(bool value);

        bool getUseTMP007() const;
        void setUseTMP007(bool value);

        std::vector<std::uint16_t> getIMUCalibrationData() const;
        void setIMUCalibrationData(const std::vector<std::uint16_t>& value);

        void prepareForSafeOp() override;
        void finishPreparingForSafeOp() override;
        bool getUseIMU() const;
        void setUseIMU(bool value);

        static std::string getDefaultName();

        static bool
        isSlaveIdentifierAccepted(const armarx::control::ethercat::SlaveIdentifier& sid);

    private:
        SensorboardSDOHelper sdoHelper;
        std::atomic<bool> preparingFinished;
        std::thread initTask;
        bool checkAbsEncoderReading = true;
        bool useTMP007 = true;
        bool useIMU = true;

        std::vector<std::uint16_t> IMUCalibrationData;

        SensorBoardStatistics stats;
        bool statsPrinted = false;
    };

} // namespace devices::ethercat::common::sensor_board::armar6
