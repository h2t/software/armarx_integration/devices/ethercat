#pragma once


#include <cstdint>


namespace devices::ethercat::common::sensor_board::armar6
{

    /**
     * @brief PDO mapping sensorB->master
     */
    struct SlaveOut
    {
        // Object Entry 0x6000
        //    u_int32_t RawABSEncoderValue;
        std::uint8_t RawABSEncoderValueBytes[4];
        std::uint8_t RawABSEncoderValueCRC;
        std::int16_t pad1;
        std::int8_t pad2;

        // Temperature Value Robodrive
        std::int16_t MotorTemperature;
        std::int16_t pad3;

        // Temperature Value Elmo
        std::int16_t ElmoTemperature;
        std::int16_t pad4;

        //Torque ADC
        std::int32_t TorqueValue;
        // Temperature ADC Value
        std::int32_t TorqueSensorTemperature;
        // Torque ADC CRC Value
        std::uint8_t TorqueValueCRC;
        // Temperature CRC ADC Value
        std::uint8_t TorqueSensorTemperatureCRC;

        std::int16_t pad5;

        std::int16_t IMUVector1[3];
        std::int16_t IMUVector2[3];
        std::int16_t IMUQuaternionW;
        std::int16_t IMUQuaternionX;
        std::int16_t IMUQuaternionY;
        std::int16_t IMUQuaternionZ;

        // Die Temperature (Infrared Sensor for gearbox)
        std::int16_t DieTemperature;     // T_DIE
        std::int16_t GearboxTemperature; // T_OBJ

        std::uint16_t SensorboardUpdateRate;
        std::uint16_t I2CUpdateRate;
    } __attribute__((__packed__));


    /**
     * @brief PDO mapping master->sensorB
     */
    struct SlaveIn
    {
        //Test LED
        std::uint8_t LEDSwitch;
        //24 bits padding
        std::uint16_t pad1;
        std::uint8_t pad2;
    } __attribute__((__packed__));

} // namespace devices::ethercat::common::sensor_board::armar6
