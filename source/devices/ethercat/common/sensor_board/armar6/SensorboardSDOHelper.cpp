#include "SensorboardSDOHelper.h"


// armarx
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <armarx/control/ethercat/Bus.h>


#define IMU_CALIBRATION_DATA_SIZE 11


namespace devices::ethercat::common::sensor_board::armar6
{

    SensorboardSDOHelper::SensorboardSDOHelper(armarx::control::ethercat::Bus* bus, std::uint16_t slaveNumber, std::string humanName) :
        imu(bus, slaveNumber, humanName),
        humanName(humanName)
    {

    }


    IMUSDOHelper& SensorboardSDOHelper::getImu()
    {
        return imu;
    }


    std::string IMUSDOHelper::SensorBoardCommandToString(IMUSDOHelper::sensorboard_command_t command)
    {
        std::string stringCommand;
        switch (command)
        {
            case COMMAND_NONE:
                stringCommand = "none";
                break;
            case COMMAND_READ_STATUS:
                stringCommand = "read status";
                break;
            case COMMAND_SWITCH_MODE:
                stringCommand = "switch mode";
                break;
            case COMMAND_READ_CALIBRATION:
                stringCommand = "read calibration";
                break;
            case COMMAND_WRITE_CALIBRATION:
                stringCommand = "write calibration";
                break;
            case COMMAND_APPLY_REPORT_VECTOR_REGISTER:
                stringCommand = "apply report vector register";
                break;
            case COMMAND_START_BNO055_CYCLIC_REPORTING:
                stringCommand = "start BNO055 cyclic reporting";
                break;
            case COMMAND_START_TMP007_CYCLIC_REPORTING:
                stringCommand = "start TMP007 cyclic reporting";
                break;
            case COMMAND_STOP_ALL_CYCLIC_REPORTING:
                stringCommand = "stop all cyclic reporting";
                break;
            case COMMAND_START_CALIBRATION_STATUS_PRINTING:
                stringCommand = "start calibration status printing";
                break;
            case COMMAND_STOP_CALIBRATION_STATUS_PRINTING:
                stringCommand = "stop calibration status printing";
                break;
            case COMMAND_ENABLE_I2C_ISR:
                stringCommand = "enable I2C ISR";
                break;
            case COMMAND_DISABLE_I2C_ISR:
                stringCommand = "disable I2C ISR";
                break;
            case COMMAND_PING_BNO055:
                stringCommand = "ping BNO055";
                break;
            case COMMAND_PING_TMP007:
                stringCommand = "ping TMP007";
                break;
            case COMMAND_INIT_BNO055:
                stringCommand = "init BNO055";
                break;
            default:
                stringCommand = "unknown command";
        }
        return stringCommand;
    }


    std::string IMUSDOHelper::SensorBoardCommandResultToString(IMUSDOHelper::sensorboard_command_result_t result)
    {
        std::string stringResult;
        switch (result)
        {
            case COMMAND_SUCCESS:
                stringResult = "success";
                break;
            case COMMAND_EXECUTING:
                stringResult = "executing";
                break;
            case COMMAND_ERROR_ALREADY_REPORTING:
                stringResult = "error already reporting";
                break;
            case COMMAND_ERROR_NOT_REPORTING:
                stringResult = "error not reporting";
                break;
            case COMMAND_ERROR_INVALID_MODE:
                stringResult = "error invalid mode";
                break;
            case COMMAND_ERROR_BUS_BUSY:
                stringResult = "error bus busy";
                break;
            case COMMAND_ERROR_ONLY_ALLOWED_IN_CONFIG_MODE:
                stringResult = "error only allowed in config mode";
                break;
            case COMMAND_ERROR_CALIBRATION_PRINTING_ACTIVE:
                stringResult = "error calibration printing active";
                break;
            case COMMAND_ERROR_NO_RESPONSE:
                stringResult = "error no response";
                break;
            case COMMAND_ERROR_INIT_FAILED:
                stringResult = "error init failed";
                break;
            case COMMAND_ERROR_UNKNOWN_COMMAND:
                stringResult = "error unknown command";
                break;
            default:
                stringResult = "unknown result";
        }
        return stringResult;
    }


    IMUSDOHelper::IMUSDOHelper(armarx::control::ethercat::Bus* bus, std::uint16_t slaveNumber, std::string humanName) :
        bus(bus),
        slaveNumber(slaveNumber),
        humanName(humanName)

    {
        setTag("IMUSDOHelper_" + humanName);
    }

    IMUSDOHelper::sensorboard_command_result_t IMUSDOHelper::execCommand(sensorboard_command_t command, bool reportError, IceUtil::Time sleepTime, int retries) const
    {
        ARMARX_DEBUG << "Executing IMU command: " << SensorBoardCommandToString(command);
        bus->writeSDOEntry(slaveNumber, 0x8000, 1, static_cast<std::uint8_t>(command));
        std::uint8_t value = 255;
        if (sleepTime.toSecondsDouble() > 0)
        {
            usleep(sleepTime.toMicroSeconds());
        }
        while (true)
        {
            int i = 0;
            while (!bus->readSDOEntry(slaveNumber, 0x8000, 1, value))
            {
                ARMARX_DEBUG << "Waiting for " << SensorBoardCommandToString(command);
                usleep(20000);
                i++;
                if (i > retries)
                {
                    throw armarx::LocalException("Could not execute command with ") << retries << " retries: " << SensorBoardCommandToString(command);
                }
            }
            if (value != static_cast<std::uint8_t>(command))
            {
                break;
            }
            ARMARX_DEBUG << deactivateSpam(1) << "Waiting for command completion - current value: 0x" << std::hex << (int)value;
        }

        if (value != COMMAND_NONE)
        {
            throw armarx::LocalException("Command value is not COMMAND_NONE after executing command - did someone write commands at the same time? current value: 0x") << std::hex << SensorBoardCommandResultToString(sensorboard_command_result_t(value));
        }
        bus->readSDOEntry(slaveNumber, 0x8000, 2, value);
        if (value != COMMAND_SUCCESS && reportError)
        {
            ARMARX_WARNING << std::hex << "command failed - command result: " << SensorBoardCommandResultToString(sensorboard_command_result_t(value));
        }

        return static_cast<sensorboard_command_result_t>(value);
    }


    void IMUSDOHelper::reset() const
    {
        auto cmd_result = execCommand(COMMAND_STOP_ALL_CYCLIC_REPORTING, false);
        if (cmd_result != COMMAND_ERROR_NOT_REPORTING && cmd_result != COMMAND_SUCCESS)
        {
            throw armarx::LocalException("resetting of IMU failed: result: 0x") << SensorBoardCommandResultToString(cmd_result);
        }
    }


    void IMUSDOHelper::init() const
    {
        execCommand(COMMAND_INIT_BNO055, true, IceUtil::Time::secondsDouble(3));
    }


    void IMUSDOHelper::start() const
    {

        bus->writeSDOEntry(slaveNumber, 0x8000, 3, static_cast<std::uint8_t>(OPERATION_MODE_NDOF));
        execCommand(COMMAND_SWITCH_MODE, true);
        if (useTMP007 && pingTMP007())
        {
            execCommand(COMMAND_START_TMP007_CYCLIC_REPORTING);
        }
        execCommand(COMMAND_START_BNO055_CYCLIC_REPORTING);
    }


    bool IMUSDOHelper::pingBNO055() const
    {
        auto value = execCommand(COMMAND_PING_BNO055, true);
        return (value == COMMAND_SUCCESS);

    }


    bool IMUSDOHelper::pingTMP007() const
    {
        auto value = execCommand(COMMAND_PING_TMP007, false);
        return (value == COMMAND_SUCCESS);
    }


    bool IMUSDOHelper::getUseTMP007() const
    {
        return useTMP007;
    }


    void IMUSDOHelper::setUseTMP007(bool value)
    {
        useTMP007 = value;
    }


    std::vector<std::uint16_t> IMUSDOHelper::calibrateSensor() const
    {
        execCommand(COMMAND_STOP_ALL_CYCLIC_REPORTING);

        std::uint8_t value;

        bus->writeSDOEntry(slaveNumber, 0x8000, 3, static_cast<std::uint8_t>(OPERATION_MODE_NDOF));
        execCommand(COMMAND_SWITCH_MODE, true);

        execCommand(COMMAND_START_CALIBRATION_STATUS_PRINTING, true, IceUtil::Time::secondsDouble(1.1));
        while (true)
        {
            bus->readSDOEntry(slaveNumber, 0x8000, 5, value);
            ARMARX_INFO << deactivateSpam(1) << "Still calibrating 0x" << std::hex << (int)value;
            if (value == 0xFF)
            {
                ARMARX_INFO << "Final calibration status: 0x" << std::hex << (int)value;
                break;
            }
            usleep(100000);
        }
        execCommand(COMMAND_STOP_CALIBRATION_STATUS_PRINTING);

        return getCalibration();
    }


    std::vector<std::uint16_t> IMUSDOHelper::getCalibration() const
    {
        bus->writeSDOEntry(slaveNumber, 0x8000, 3, static_cast<std::uint8_t>(OPERATION_MODE_CONFIG));
        execCommand(COMMAND_SWITCH_MODE, true);
        execCommand(COMMAND_READ_CALIBRATION);
        std::uint16_t value16;
        std::vector<std::uint16_t> result;
        for (std::uint8_t n = 1; n <= IMU_CALIBRATION_DATA_SIZE; n++)
        {
            bus->readSDOEntry(slaveNumber, 0x8001, n, value16);
            result.push_back(value16);
        }
        return result;
    }


    void IMUSDOHelper::setCalibration(std::vector<std::uint16_t> calibrationData)
    {
        ARMARX_CHECK_EQUAL(calibrationData.size(), IMU_CALIBRATION_DATA_SIZE);
        //        ARMARX_INFO << "Setting IMU calibration";
        bus->writeSDOEntry(slaveNumber, 0x8000, 3, static_cast<std::uint8_t>(OPERATION_MODE_CONFIG));
        execCommand(COMMAND_SWITCH_MODE, true);
        for (std::uint8_t n = 1; n <= IMU_CALIBRATION_DATA_SIZE; n++)
        {
            bus->writeSDOEntry(slaveNumber, 0x8001, n, calibrationData.at(n - 1));
        }
        execCommand(COMMAND_WRITE_CALIBRATION);
        std::vector<std::uint16_t> result;
        execCommand(COMMAND_READ_CALIBRATION);
        std::uint16_t value16;
        for (std::uint8_t n = 1; n <= IMU_CALIBRATION_DATA_SIZE; n++)
        {
            bus->readSDOEntry(slaveNumber, 0x8001, n, value16);
            result.push_back(value16);
        }
        ARMARX_VERBOSE << "Calibration: " << result;
        //        ARMARX_INFO << "Wrote IMU calibration";

        bus->writeSDOEntry(slaveNumber, 0x8000, 3, static_cast<std::uint8_t>(OPERATION_MODE_NDOF));
        execCommand(COMMAND_SWITCH_MODE, true);

        //        std::uint8_t value;
        //        execCommand(COMMAND_START_CALIBRATION_STATUS_PRINTING, true, IceUtil::Time::secondsDouble(1.1));
        //        IceUtil::Time start = IceUtil::Time::now();
        //        while ((IceUtil::Time::now() - start).toSecondsDouble() < 5)
        //        {
        //            bus->readSDO(slaveNumber, 0x8000, 5, value);
        //            ARMARX_INFO << deactivateSpam(1) << "Current calibration status 0x" << std::hex << (int)value;
        //            usleep(100000);
        //        }
        //        execCommand(COMMAND_STOP_CALIBRATION_STATUS_PRINTING);

    }

}  // namespace devices::ethercat::common::sensor_board::v1
