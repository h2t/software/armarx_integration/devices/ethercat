#pragma once

#include <list>

#include <armarx/control/hardware_config/SlaveConfig.h>

namespace devices::ethercat::common::sensor_board::armar6
{
    namespace hwconfig = armarx::control::hardware_config;
    struct SensorBoardConfig
    {
        SensorBoardConfig(hwconfig::SlaveConfig& slaveConfig)
            : checkAbsEncoderReadings(slaveConfig.getBool("CheckAbsEncoderReadings")),
              useTmp007(slaveConfig.getBool("UseTMP007")),
              useIMU(slaveConfig.getBool("UseIMU")),
              imuCalibrationData(slaveConfig.getUintList("IMUCalibration")),
              position(slaveConfig.getLinearConfig("position")),
              actualTorque(slaveConfig.getLinearConfig("actualTorque")),
              torqueTemperature(slaveConfig.getLinearConfig("torqueTemperature")),
              motorTemperature(slaveConfig.getLinearConfig("motorTemperature")),
              gearTemperature(slaveConfig.getLinearConfig("gearTemperature")),
              elmoTemperature(slaveConfig.getLinearConfig("elmoTemperature")),
              imuQuaternion(slaveConfig.getLinearConfig("imuQuaternion")),
              imuAcceleration(slaveConfig.getLinearConfig("imuAcceleration")),
              imuGyro(slaveConfig.getLinearConfig("imuGyro"))

        {
        }

        bool checkAbsEncoderReadings;
        bool useTmp007;
        bool useIMU;
        std::list<std::uint32_t> imuCalibrationData;

        /// Conversion Parameters
        hwconfig::types::LinearConfig position;
        hwconfig::types::LinearConfig actualTorque;
        hwconfig::types::LinearConfig torqueTemperature;
        hwconfig::types::LinearConfig motorTemperature;
        hwconfig::types::LinearConfig gearTemperature;
        hwconfig::types::LinearConfig elmoTemperature;
        hwconfig::types::LinearConfig imuQuaternion;
        hwconfig::types::LinearConfig imuAcceleration;
        hwconfig::types::LinearConfig imuGyro;
    };
}
