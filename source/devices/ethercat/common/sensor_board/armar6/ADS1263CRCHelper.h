#pragma once


// STD/STL
#include <cstdint>


namespace devices::ethercat::common::sensor_board::armar6
{

    class ADS1263CRCHelper
    {

    public:

        ADS1263CRCHelper();
        static std::uint8_t CalculateCRC8_ADS1263(std::uint8_t* bytes, std::size_t numBytes);

        static std::uint8_t CalculateCRC8_Torque(std::int32_t value);

        static std::uint8_t CalculateCRC8_Temperature(std::int32_t value);

    };

}  // namespace devices::ethercat::common::sensor_board::v1
