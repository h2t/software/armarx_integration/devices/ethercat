/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValueIMU.h>
#include <armarx/control/ethercat/DataInterface.h>
#include <armarx/control/rt_filters/AverageFilter.h>

#include "Config.h"
#include "Slave.h"


namespace VirtualRobot
{
    using RobotPtr = std::shared_ptr<class Robot>;
    using RobotNodePtr = std::shared_ptr<class RobotNode>;
} // namespace VirtualRobot


namespace devices::ethercat::common::imagine_board
{

    class SensorActorSensorValue : virtual public armarx::SensorValue1DoFRealActuator
    {
    public:
        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION std::int32_t targetPWM;
        float relativePosition;
        float velocityTicksPerMs;
        float absoluteEncoderVelocity;
        float absoluteEncoderTicks;
        std::int32_t maxPWM;
        std::int32_t minPWM;

        static SensorValueInfo<SensorActorSensorValue>
        GetClassMemberInfo()
        {
            SensorValueInfo<SensorActorSensorValue> svi;
            //            svi.addBaseClass<SensorValue1DoFActuatorMotorTemperature>();
            svi.addBaseClass<SensorValue1DoFRealActuator>();
            svi.addMemberVariable(&SensorActorSensorValue::targetPWM, "targetPWM");
            svi.addMemberVariable(&SensorActorSensorValue::relativePosition, "relativePosition");
            svi.addMemberVariable(&SensorActorSensorValue::maxPWM, "maxPWM");
            svi.addMemberVariable(&SensorActorSensorValue::minPWM, "minPWM");
            svi.addMemberVariable(&SensorActorSensorValue::velocityTicksPerMs,
                                  "velocityTicksPerMs");
            svi.addMemberVariable(&SensorActorSensorValue::absoluteEncoderVelocity,
                                  "absoluteEncoderVelocity");
            svi.addMemberVariable(&SensorActorSensorValue::absoluteEncoderTicks,
                                  "absoluteEncoderTicks");
            return svi;
        }
    };


    class SensorActorData
    {
    public:
        SensorActorData();
        void setTargetPWM(std::int32_t targetPWM);
        float getPosition() const;
        float getRelativePosition() const;
        float getCompensatedRelativePosition() const;
        float getVelocity() const;
        float getAcceleration() const;
        float getTorque() const;
        float getSoftLimitHi() const;
        float getSoftLimitLo() const;
        bool isLimitless() const;
        std::int32_t getTargetPWM() const;
        std::int32_t getVelocityTicks() const;

        std::int32_t getCurrentMinPWM() const;

        std::int32_t getCurrentMaxPWM() const;

        std::int32_t getSiblingControlActorIndex() const;

        bool getPositionControlEnabled() const;

        bool getVelocityControlEnabled() const;

        float getCurrentPWMBoundGradient() const;

        std::int32_t getCurrentPWMBoundOffset() const;

        float getParallelGripperDecouplingFactor() const;
        float getAbsoluteEncoderTicks() const;

        float getAbsoluteEncoderVelocity() const;

    private:
        std::uint32_t rawABSEncoderTicks;
        armarx::control::ethercat::LinearConvertedValue<std::int32_t> relativePosition;
        float adjustedRelativePosition;
        float relativePositionOffset = std::nan("");
        float parallelGripperDecouplingFactor = std::nanf("");
        armarx::control::ethercat::LinearConvertedValue<std::uint32_t> position;
        float sanitizedAbsolutePosition;
        armarx::control::ethercat::LinearConvertedValue<std::int32_t> velocity;
        armarx::control::rt_filters::RtAverageFilter velocityFilter;
        float absoluteEncoderVelocity = 0.0f;
        float acceleration;
        float lastAbsolutePosition = std::nanf("");
        armarx::control::ethercat::LinearConvertedValue<std::int32_t> torque;
        armarx::control::ethercat::LinearConvertedValue<std::int32_t> targetPWM;
        std::int32_t* velocityTicks;
        std::int32_t currentMaxPWM = 0;
        std::int32_t currentMinPWM = 0;
        std::uint32_t maxPWM;
        std::int32_t parallelControlEnabled = -1;
        VirtualRobot::RobotNodePtr robotNode;
        armarx::control::ethercat::LinearConvertedValue<std::int32_t> targetPWMPtr;
        bool positionControlEnabled = true;
        bool velocityControlEnabled = true;
        float currentPWMBoundGradient = 3.75;
        std::int32_t currentPWMBoundOffset = 1500;

        friend class Data;
    };
    using SensorActorDataPtr = std::shared_ptr<SensorActorData>;


    class Data : public armarx::control::ethercat::DataInterface
    {

    public:
        Data(std::list<ActorConfig> config,
             SlaveOut* sensorOUT,
             SlaveIn* sensorIN,
             VirtualRobot::RobotPtr robot);

        // AbstractData interface
    public:
        /// @brief This function is never called!
        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration) override;
        /// @brief This function is never called!
        void
        rtWriteTargetValues(const IceUtil::Time&, const IceUtil::Time&) override
        {
            std::terminate();
        }
        SensorActorDataPtr& getSensorActorData(size_t actorIndex);

        std::int8_t getIMUTemperature() const;

    public:
        SlaveOut* sensorOUT;
        SlaveIn* sensorIN;

        std::vector<SensorActorDataPtr> headActorData;
    };

} // namespace devices::ethercat::common::imagine_board
