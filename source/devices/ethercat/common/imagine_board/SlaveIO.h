#pragma once


// STD/STL
#include <cstdint>


namespace devices::ethercat::common::imagine_board
{

    /**
     * @brief PDO mapping sensorB->master
     */
    struct SlaveOut
    {
        std::uint8_t RawABSEncoderValueBytes[4];
        std::uint8_t RawABSEncoderValueCRC;
        std::int16_t pad1;
        std::int8_t pad2;

        std::uint8_t RawABSEncoder2ValueBytes[4];
        std::uint8_t RawABSEncoder2ValueCRC;
        std::int16_t pad3;
        std::int8_t pad4;

        std::int16_t IMUVector1[3];
        std::int16_t IMUVector2[3];
        std::int16_t IMUQuaternionW;
        std::int16_t IMUQuaternionX;
        std::int16_t IMUQuaternionY;
        std::int16_t IMUQuaternionZ;
        std::int8_t IMUTemperature;

        std::int16_t pad5;
        std::int8_t pad6;

        std::int32_t motor1_current_pos;
        std::int32_t motor1_current_speed; // ticks pro milliseconds
        std::int32_t motor1_current_torque;
        std::int32_t motor2_current_pos;
        std::int32_t motor2_current_speed;
        std::int32_t motor2_current_torque;
        std::int32_t motor3_current_pos;
        std::int32_t motor3_current_speed;
        std::int32_t motor3_current_torque;

    } __attribute__((__packed__));


    /**
     * @brief PDO mapping master->sensorB
     */
    struct SlaveIn
    {
        std::uint16_t LED_PG15;
        std::uint16_t LED_2;
        std::uint16_t LED_3;
        std::uint16_t LED_4;

        std::int32_t motor1_target_pwm;
        std::int32_t motor1_target_speed;
        std::int32_t motor1_target_torque;
        std::int32_t motor2_target_pwm;
        std::int32_t motor2_target_speed;
        std::int32_t motor2_target_torque;
        std::int32_t motor3_target_pwm;
        std::int32_t motor3_target_speed;
        std::int32_t motor3_target_torque;
        std::int32_t motor4_target_pwm;
    } __attribute__((__packed__));

}
