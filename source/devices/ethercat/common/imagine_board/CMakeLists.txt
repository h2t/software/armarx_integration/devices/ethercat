armarx_add_library(common_imagine_board
    SOURCES
        Slave.cpp
        Data.cpp
        SensorValue.cpp
    HEADERS
        Config.h
        SlaveIO.h
        Slave.h
        Data.h
        SensorValue.h
    DEPENDENCIES
        RobotAPIUnits
        armarx_control::ethercat
        armarx_control::rt_filters
    DEPENDENCIES_LEGACY
        SOEM
)

target_compile_options(common_imagine_board
    PRIVATE
        -Wall -Wextra -Wpedantic -Werror
)

if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    if(CMAKE_CXX_COMPILER_VERSION VERSION_GREATER_EQUAL "9")
        target_compile_options(common_imagine_board
            PRIVATE
                -Wno-error=address-of-packed-member
        )
    endif()
endif()
