#pragma once

#include <armarx/control/hardware_config/DeviceConfig.h>

namespace devices::ethercat::common::imagine_board
{
    namespace hwconfig = armarx::control::hardware_config;
    struct ActorConfig
    {
        ActorConfig(hwconfig::DeviceConfigBase& actorConfig)
            : name(actorConfig.getName()),
              connector(actorConfig.getUint("connector")),
              enabled(actorConfig.getBool("enabled")),
              positionControlEnabled(actorConfig.getBool("PositionControlEnabled")),
              velocityControlEnabled(actorConfig.getBool("VelocityControlEnabled")),
              pwm(actorConfig.getLinearConfig("pwm")),
              maxPwm(actorConfig.getUint("maxPWM")),
              position(actorConfig.getLinearConfig("position")),
              relativePosition(actorConfig.getLinearConfig("relativePosition")),
              velocity(actorConfig.getLinearConfig("velocity")),
              torque(actorConfig.getLinearConfig("torque")),
              parallelGripperDecouplingFactor(actorConfig.getFloat("ParallelGripperDecouplingFactor")),
              parallelControlSiblingIndex(actorConfig.getInt("ParallelControlSiblingIndex")),
              currentPWMBoundGradient(actorConfig.getFloat("CurrentPWMBoundGradient")),
              currentPWMBoundOffset(actorConfig.getInt("CurrentPWMBoundOffset"))
        {
        }

        std::string name;
        std::uint32_t connector;
        bool enabled;

        bool positionControlEnabled;
        bool velocityControlEnabled;

        /// Conversion
        hwconfig::types::LinearConfig pwm;
        std::uint32_t maxPwm;
        hwconfig::types::LinearConfig position;
        hwconfig::types::LinearConfig relativePosition;
        hwconfig::types::LinearConfig velocity;
        hwconfig::types::LinearConfig torque;
        float parallelGripperDecouplingFactor;
        std::int32_t parallelControlSiblingIndex;
        float currentPWMBoundGradient;
        std::int32_t currentPWMBoundOffset;
    };
}
