/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Slave.h"

#include <ethercat.h>

#include <armarx/control/ethercat/Bus.h>

#include <devices/ethercat/common/h2t_devices.h>
#include <devices/ethercat/head/armar6/Device.h>


namespace devices::ethercat::common::imagine_board
{

    Slave::Slave(const armarx::control::ethercat::SlaveIdentifier slaveIdentifier) :
        SlaveInterfaceWithIO(slaveIdentifier)
    {
        setTag(this->slaveIdentifier.getName());
    }


    void
    Slave::doMappings()
    {
    }


    bool
    Slave::prepareForRun()
    {
        return true;
    }


    void
    Slave::execute()
    {
    }


    bool
    Slave::shutdown()
    {
        return true;
    }


    void
    Slave::prepareForOp()
    {
    }


    bool
    Slave::hasError()
    {
        return false;
    }


    std::string
    Slave::getDefaultName()
    {
        return "ImagineBoard_SensorActorAddon";
    }


    bool
    Slave::isSlaveIdentifierAccepted(const armarx::control::ethercat::SlaveIdentifier& sid)
    {
        // For the imagine boards, the serial number could not be encoded on the chip.
        // The product code was used instead.

        const std::vector<std::uint32_t> validProductCodes{
            0x1000,
            0x1001,
            0x1002,
            0x1003,
            0x1004,
            0x1005,
            0x1006,
            0x1007
        };

        const bool productCodeMatches =
            std::find(validProductCodes.begin(), validProductCodes.end(), sid.productCode) !=
            validProductCodes.end();

        return sid.vendorID == devices::ethercat::common::H2TVendorId
                and sid.serialNumber == 0 and productCodeMatches;
    }

} // namespace devices::ethercat::common::imagine_board
