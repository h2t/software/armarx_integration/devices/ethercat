/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Data.h"


// Simox
#include <VirtualRobot/Robot.h>

// armarx
#include <RobotAPI/components/units/RobotUnit/util/RtLogging.h>
#include <RobotAPI/libraries/core/math/MathUtils.h>


namespace devices::ethercat::common::imagine_board
{
    Data::Data(std::list<ActorConfig> config,
               SlaveOut* sensorOUT,
               SlaveIn* sensorIN,
               VirtualRobot::RobotPtr robot) :
        sensorOUT(sensorOUT), sensorIN(sensorIN)
    {
        ARMARX_CHECK_EXPRESSION(sensorOUT);
        ARMARX_CHECK_EXPRESSION(sensorIN);
        headActorData.resize(3);
        for (auto motorConfig : config)
        {
            ARMARX_IMPORTANT << "Creating actor data class for " << motorConfig.name << " at index "
                             << motorConfig.connector;
            auto initSensorActorData =
                [&](int* position, int* velocity, int* torque, int* targetPWM)
            {
                headActorData.at(motorConfig.connector).reset(new SensorActorData);
                headActorData.at(motorConfig.connector)
                    ->targetPWMPtr.init(targetPWM,
                                        motorConfig.pwm,
                                        std::nanf("1"),
                                        true,
                                        "targetPWMPtr");
                headActorData.at(motorConfig.connector)->maxPWM = motorConfig.maxPwm;
                headActorData.at(motorConfig.connector)
                    ->position.init(&headActorData.at(motorConfig.connector)->rawABSEncoderTicks,
                                    motorConfig.position,
                                    std::nanf("1"),
                                    true,
                                    "position");
                headActorData.at(motorConfig.connector)
                    ->relativePosition.init(position,
                                            motorConfig.relativePosition,
                                            std::nanf("1"),
                                            true,
                                            "relativePosition");
                headActorData.at(motorConfig.connector)
                    ->velocity.init(velocity,
                                    motorConfig.velocity,
                                    std::nanf("1"),
                                    true,
                                    "velocity");
                headActorData.at(motorConfig.connector)
                    ->torque.init(torque,
                                  motorConfig.torque,
                                  std::nanf("1"),
                                  true,
                                  "torque");
                headActorData.at(motorConfig.connector)->robotNode = robot->getRobotNode(motorConfig.name);
                headActorData.at(motorConfig.connector)->velocityTicks = velocity;
                headActorData.at(motorConfig.connector)->positionControlEnabled = motorConfig.positionControlEnabled;
                headActorData.at(motorConfig.connector)->velocityControlEnabled = motorConfig.velocityControlEnabled;

                headActorData.at(motorConfig.connector)->parallelGripperDecouplingFactor = motorConfig.parallelGripperDecouplingFactor;
                headActorData.at(motorConfig.connector)->parallelControlEnabled = motorConfig.parallelControlSiblingIndex;
                headActorData.at(motorConfig.connector)->currentPWMBoundGradient = motorConfig.currentPWMBoundGradient;
                headActorData.at(motorConfig.connector)->currentPWMBoundOffset = motorConfig.currentPWMBoundOffset;
            };
            switch (motorConfig.connector)
            {
                case 0:
                    initSensorActorData(&sensorOUT->motor1_current_pos,
                                        &sensorOUT->motor1_current_speed,
                                        &sensorOUT->motor1_current_torque,
                                        &sensorIN->motor1_target_pwm);
                    break;
                case 1:
                    initSensorActorData(&sensorOUT->motor2_current_pos,
                                        &sensorOUT->motor2_current_speed,
                                        &sensorOUT->motor2_current_torque,
                                        &sensorIN->motor2_target_pwm);
                    break;
                case 2:
                    initSensorActorData(&sensorOUT->motor3_current_pos,
                                        &sensorOUT->motor3_current_speed,
                                        &sensorOUT->motor3_current_torque,
                                        &sensorIN->motor3_target_pwm);
                    break;
                default:
                    throw armarx::LocalException("Motor index out of range: ") << motorConfig.connector;
            }
        }
    }

    void
    Data::rtReadSensorValues(const IceUtil::Time& /*sensorValuesTimestamp*/,
                             const IceUtil::Time& timeSinceLastIteration)
    {
        int i = 0;
        float dt = static_cast<float>(timeSinceLastIteration.toSecondsDouble());
        for (auto& ptr : headActorData)
        {
            if (!ptr)
            {
                ++i;
                continue;
            }
            SensorActorData& d = *ptr;


            d.relativePosition.read();

            if (d.getSiblingControlActorIndex() >= 0)
            {
                auto& d2 =
                    headActorData.at(static_cast<unsigned int>(d.getSiblingControlActorIndex()));
                d.adjustedRelativePosition =
                    d.relativePosition.value +
                    d2->relativePosition.value * d.parallelGripperDecouplingFactor;
            }
            else
            {
                d.adjustedRelativePosition = d.relativePosition.value;
            }

            if (std::isnan(d.relativePositionOffset)) // initialize on first run
            {
                d.relativePositionOffset = -d.relativePosition.value + d.position.value;
            }

            if (i > 1)
            {
                d.rawABSEncoderTicks = 0;
            }
            else
            {
                std::uint8_t* raw_abs_encoder_value_bytes =
                    i == 0 ? sensorOUT->RawABSEncoderValueBytes
                           : sensorOUT->RawABSEncoder2ValueBytes;
                d.rawABSEncoderTicks =
                    ((static_cast<std::uint32_t>(raw_abs_encoder_value_bytes[0]) << 24 |
                      static_cast<std::uint32_t>(raw_abs_encoder_value_bytes[1]) << 16 |
                      static_cast<std::uint32_t>(raw_abs_encoder_value_bytes[2]) << 8 |
                      static_cast<std::uint32_t>(raw_abs_encoder_value_bytes[3])) &
                     0xFFFFF000) >>
                    12;
                if ((raw_abs_encoder_value_bytes[2] & 0b00001000) ||
                    (raw_abs_encoder_value_bytes[2] & 0b00000100))
                {
                    // Error or Warning bit is set
                    std::uint8_t error_and_warnings =
                        static_cast<std::uint8_t>((raw_abs_encoder_value_bytes[2] & 0b00000011)
                                                  << 6u) |
                        static_cast<std::uint8_t>((raw_abs_encoder_value_bytes[3] & ~0b00000011) >>
                                                  2u);
                    ARMARX_RT_LOGF_ERROR("%s: Absolute Encoder Error and Warning bits: %b",
                                         d.robotNode->getName().c_str(),
                                         error_and_warnings).deactivateSpam (1);
                }
            }

            if (i > 1)
            {
                d.position.value = d.adjustedRelativePosition;
            }
            else
            {
                d.position.read();
            }
            d.sanitizedAbsolutePosition = armarx::math::MathUtils::angleModPI(d.position.value);

            //ARMARX_RT_LOGF_INFO("position %d, relative position: %d", (int)d.rawABSEncoderTicks, d.relativePosition.value).deactivateSpam(0.5);
            if (!std::isnan(d.lastAbsolutePosition))
            {
                d.absoluteEncoderVelocity =
                    armarx::math::MathUtils::AngleDelta(d.lastAbsolutePosition,
                                                        d.sanitizedAbsolutePosition) /
                    static_cast<float>(timeSinceLastIteration.toSecondsDouble());
            }
            d.lastAbsolutePosition = d.sanitizedAbsolutePosition;
            float oldVelocity = d.velocity.value;
            d.velocity.read();
            d.velocity.value = d.velocityFilter.update(d.velocity.value);
            d.torque.read();
            d.currentMaxPWM = static_cast<std::int32_t>(
                std::round(static_cast<float>(*d.velocityTicks) * d.currentPWMBoundGradient +
                           static_cast<float>(d.currentPWMBoundOffset)));
            d.currentMinPWM = static_cast<std::int32_t>(
                std::round(static_cast<float>(*d.velocityTicks) * d.currentPWMBoundGradient -
                           static_cast<float>(d.currentPWMBoundOffset)));
            d.acceleration = (d.velocity.value - oldVelocity) / dt;
            //            d.acceleration.read();
            //            d.gravityTorque.read();
            //            d.motorCurrent.read();
            //            d.motorTemperature.read();
            i++;
        }
    }


    SensorActorDataPtr&
    Data::getSensorActorData(size_t actorIndex)
    {
        ARMARX_CHECK_LESS(actorIndex, headActorData.size());
        ARMARX_CHECK_EXPRESSION(headActorData.at(actorIndex)) << actorIndex;
        return headActorData.at(actorIndex);
    }

    SensorActorData::SensorActorData() : velocityFilter(10)
    {
    }

    void
    SensorActorData::setTargetPWM(int32_t targetPWM)
    {
        targetPWM = std::clamp(targetPWM, currentMinPWM, currentMaxPWM);
        targetPWM = std::clamp(
            targetPWM, -static_cast<std::int32_t>(maxPWM), static_cast<std::int32_t>(maxPWM));

        targetPWMPtr.value = static_cast<float>(targetPWM);
        targetPWMPtr.write();
        //        ARMARX_RT_LOGF_INFO(" pwm: %d, raw pwm: %d, factor: %.f", targetPWMPtr.value, targetPWMPtr.getRaw(), targetPWMPtr.getFactor());
    }

    float
    SensorActorData::getPosition() const
    {
        return sanitizedAbsolutePosition;
    }

    float
    SensorActorData::getRelativePosition() const
    {
        return adjustedRelativePosition;
    }

    float
    SensorActorData::getCompensatedRelativePosition() const
    {
        return adjustedRelativePosition + relativePositionOffset;
    }

    float
    SensorActorData::getVelocity() const
    {
        return velocity.value;
    }

    float
    SensorActorData::getAcceleration() const
    {
        return acceleration;
    }

    float
    SensorActorData::getTorque() const
    {
        return torque.value;
    }

    float
    SensorActorData::getSoftLimitHi() const
    {
        return robotNode->getJointLimitHigh();
    }

    float
    SensorActorData::getSoftLimitLo() const
    {
        return robotNode->getJointLimitLo();
    }

    bool
    SensorActorData::isLimitless() const
    {
        return robotNode->isLimitless();
    }

    std::int32_t
    SensorActorData::getTargetPWM() const
    {
        return static_cast<std::int32_t>(targetPWMPtr.value);
    }

    std::int32_t
    SensorActorData::getVelocityTicks() const
    {
        return *velocityTicks;
    }

    std::int32_t
    SensorActorData::getCurrentMinPWM() const
    {
        return currentMinPWM;
    }

    std::int32_t
    SensorActorData::getCurrentMaxPWM() const
    {
        return currentMaxPWM;
    }

    std::int32_t
    SensorActorData::getSiblingControlActorIndex() const
    {
        return parallelControlEnabled;
    }

    bool
    SensorActorData::getPositionControlEnabled() const
    {
        return positionControlEnabled;
    }

    bool
    SensorActorData::getVelocityControlEnabled() const
    {
        return velocityControlEnabled;
    }

    float
    SensorActorData::getCurrentPWMBoundGradient() const
    {
        return currentPWMBoundGradient;
    }

    std::int32_t
    SensorActorData::getCurrentPWMBoundOffset() const
    {
        return currentPWMBoundOffset;
    }

    float
    SensorActorData::getParallelGripperDecouplingFactor() const
    {
        return parallelGripperDecouplingFactor;
    }

    float
    SensorActorData::getAbsoluteEncoderVelocity() const
    {
        return absoluteEncoderVelocity;
    }

    float
    SensorActorData::getAbsoluteEncoderTicks() const
    {
        return static_cast<float>(rawABSEncoderTicks);
    }

    std::int8_t
    Data::getIMUTemperature() const
    {
        return sensorOUT->IMUTemperature;
    }

} // namespace robot_devices::ethercat::common::imagine_board
