#pragma once


#include <armarx/control/ethercat/DeviceInterface.h>


namespace devices::ethercat::common
{

    static constexpr std::uint32_t H2TVendorId = 0x7bc;


    /**
     * Older H2T devices were flashed with the serial number being encoded as product code
     * due to limitations at that time.
     *
     * This function assigns devices similarly to `tryAssignUsingSerialNo` but accounting for this
     * trait.
     *
     * @see armarx::control::ethercat::tryAssignUsingSerialNo
     */
    template <typename SlaveI, typename Slave>
    [[nodiscard]] armarx::control::ethercat::DeviceInterface::TryAssignResult
    tryAssignLegacyH2TDevice(SlaveI& slave, Slave*& concreteSlave, std::uint32_t serialNo)
    {
        static_assert(std::is_base_of<SlaveI, Slave>::value);

        using Result = armarx::control::ethercat::DeviceInterface::TryAssignResult;

        if (Slave::isSlaveIdentifierAccepted(slave.getSlaveIdentifier()))
        {
            Slave* upcastedSlave = dynamic_cast<Slave*>(&slave);

            if (upcastedSlave and upcastedSlave->getSlaveIdentifier().productCode == serialNo)
            {
                // Ensures that concreteSlave was not already assigned. Dynamic cast prevents errors with
                // uninitialized pointers which are not nullptr.
                if (concreteSlave == nullptr or dynamic_cast<SlaveI*>(concreteSlave) == nullptr)
                {
                    concreteSlave = upcastedSlave;
                    return Result::assigned;
                }

                return Result::alreadyAssigned;
            }
        }

        // Unknown slave.
        return Result::unknown;
    }

} // namespace devices::ethercat::common
