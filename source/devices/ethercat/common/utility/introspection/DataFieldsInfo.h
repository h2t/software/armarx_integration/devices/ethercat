/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Stefan Reither (stefan dot reither at kit dot edu
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/components/units/RobotUnit/util/introspection/DataFieldsInfo.h>

namespace simox::color
{
    struct Color;
}

namespace armarx::introspection
{
    template <>
    struct DataFieldsInfo<simox::color::Color, void> : DataFieldsInfoBase<simox::color::Color>
    {
        using DataFieldsInfoBase<simox::color::Color>::GetDataFieldAs;
        static std::size_t GetNumberOfFields();
        static void
        GetDataFieldAs(std::size_t i, const simox::color::Color& field, std::uint64_t& out);
        static const std::type_info& GetDataFieldType(std::size_t i);
        static std::map<std::string, VariantBasePtr> ToVariants(const simox::color::Color& value,
                                                                const std::string& name,
                                                                const IceUtil::Time& timestamp,
                                                                const std::string& frame,
                                                                const std::string& agent);
    };
} // namespace armarx::introspection
