/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Stefan Reither (stefan dot reither at kit dot edu
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DataFieldsInfo.h"

#include <SimoxUtility/color/Color.h>

namespace armarx::introspection
{
    std::size_t
    DataFieldsInfo<simox::Color, void>::GetNumberOfFields()
    {
        return 1;
    }

    void
    DataFieldsInfo<simox::Color, void>::GetDataFieldAs(std::size_t /*i*/,
                                                       const simox::color::Color& field,
                                                       std::uint64_t& out)
    {
        out =
            (static_cast<std::uint64_t>(field.r) << 24 | static_cast<std::uint64_t>(field.g) << 16 |
             static_cast<std::uint64_t>(field.b) << 8 | static_cast<std::uint64_t>(field.a));
    }

    const std::type_info&
    DataFieldsInfo<simox::Color, void>::GetDataFieldType(std::size_t /*i*/)
    {
        return typeid(std::uint64_t);
    }

    std::map<std::string, VariantBasePtr>
    DataFieldsInfo<simox::Color, void>::ToVariants(const simox::Color& value,
                                                   const std::string& name,
                                                   const IceUtil::Time& timestamp,
                                                   const std::string& frame,
                                                   const std::string& agent)
    {
        ARMARX_CHECK_EXPRESSION(frame.empty() && agent.empty())
            << "There is no framed version for simox::Color";
        return {{name,
                 {new TimedVariant{(static_cast<std::uint64_t>(value.r) << 24 |
                                    static_cast<std::uint64_t>(value.g) << 16 |
                                    static_cast<std::uint64_t>(value.b) << 8 |
                                    static_cast<std::uint64_t>(value.a)),
                                   timestamp}}}};
    }

} // namespace armarx::introspection
