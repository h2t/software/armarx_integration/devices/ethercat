#include <bitset>
#include <exception>
#include <memory>

#include <ArmarXCore/core/logging/Logging.h>
#include <RobotAPI/components/units/RobotUnit/util/RtLogging.h>
#include <armarx/control/ethercat/Bus.h>

#include "Mappings.h"
#include <devices/ethercat/common/elmo/gold/Slave.h>


namespace devices::ethercat::common::elmo::gold
{
    //  master -> slave
    static constexpr std::uint16_t RX_MAPPING_INDEX = 0x1C12;
    //  slave -> master
    static constexpr std::uint16_t TX_MAPPING_INDEX = 0x1C13;

    static constexpr std::uint32_t ELMO_VENDOR_ID = 0x9A;

    static constexpr std::uint16_t DS402_MASK_1 = 0x4F;
    static constexpr std::uint16_t DS402_MASK_2 = 0x6F;

    static constexpr std::int8_t ELMO_MODE_OF_OPERATION_CURRENT_CONTROL = 4;
    static constexpr std::int8_t ELMO_MODE_OF_OPERATION_VELOCITY = 3;

    // This has to be set at startup before the mapping.
    static constexpr std::int8_t ELMO_MODE_OF_OPERATION_AT_START_UP = 8;
    static constexpr std::int8_t ELMO_MODE_OF_OPERATION_POSITION = 1;
    static constexpr std::int8_t ELMO_MODE_OF_OPERATION_DISABLE = 0;

    // Quick stop motions   - 0: Disable drive function
    //                      - 2: Slow down on quick-stop ramp and then disable the drive
    //                      - 5: Slow down on slow-down ramp and stay in QUICK STOP
    static constexpr std::int16_t ELMO_QUICK_STOP_OPERATION = 5;

    // Mappings
    constexpr mapping_struct rx_mapping = createRXMapping();
    constexpr mapping_struct rx2_mapping = createRX2Mapping();
    constexpr mapping_struct tx_mapping = createTXMapping();
    constexpr mapping_struct tx2_mapping = createTX2Mapping();


    Slave::Slave(const armarx::control::ethercat::SlaveIdentifier sid) :
        SlaveInterfaceWithIO(sid),
        lastModeSet(ELMO_MODE_OF_OPERATION_AT_START_UP),
        quickStopActive(false),
        ratedTorque(0),
        ratedCurrent(0)
    {
        setTag(this->slaveIdentifier.getName());
    }


    void
    Slave::execute()
    {
        inputs->polarity = 0; //polarity
        if (inputs->maxRatedCurrentTarget > maxRatedCurrent)
        {
            float currentFactor = float(this->ratedCurrent) * 0.001f;
            ARMARX_IMPORTANT << deactivateSpam(5) << "Max rated current was set too high ("
                             << inputs->maxRatedCurrentTarget * currentFactor << ") - limiting to "
                             << maxRatedCurrent * currentFactor << " from the config";
            inputs->maxRatedCurrentTarget = maxRatedCurrent;
        }
    }


    bool
    Slave::checkEmergencyStopOnBus() const
    {
        /// Actor: emergency stop off:  00000000001111110000000000000000
        /// emergency stop on:          00000000001111110000000000001000

        ///  Torso/Wheels:
        ///  emergency stop off:        00000000000000000000000000001000
        ///  emergency stop on:         00000000000000000000000000000000

        DS402State currentState = getState();
        //    ctrl::ethercat::EtherCAT& bus = ctrl::ethercat::EtherCAT::getBus();

        //    ARMARX_INFO << deactivateSpam(1) << (int)currentState;
        switch (currentState)
        {
            case DS402State::NOT_READY_TO_SWITCH_ON:
            case DS402State::READY_TO_SWITCH_ON:
            case DS402State::SWITCH_ON_DISABLED:
            case DS402State::SWITCH_ON:
            case DS402State::FAULT_REACTION_ACTIVE:
            case DS402State::FAULT:
            {
                //            u_int16_t value16;
                std::uint32_t value32;
                //            bus.readSDO(getSlaveNumber(), 0x60FD, 0, value32);
                //            ARMARX_IMPORTANT << deactivateSpam(1, to_string(value32)) << "stop check: 0x60FD: " << std::bitset<32>(value32);
                value32 = outputs->digitalInputs;
                ARMARX_DEBUG << deactivateSpam(1, armarx::to_string(value32))
                             << getSlaveIdentifier().getName()
                             << ": State: " << DS402StateToString(currentState)
                             << " - digital inputs: " << std::bitset<32>(outputs->digitalInputs);
                auto maskedValue = value32 & ELMO_DIGITAL_INPUTS_INTERLOCK;
                //            bus.readSDO(getSlaveNumber(), 0x20FD, 0, value32);
                //            ARMARX_IMPORTANT << deactivateSpam(1, to_string(value32)) << "0x20FD: " << std::bitset<32>(value32);
                //            bus.readSDO(getSlaveNumber(), 0x2086, 0, value32);
                // 0x2086: no emergency:        00000000000001010000000000100000
                // 0x2086: emergency:           00000000000000010000000000000000
                //            auto maskedValue = value32 &  0b00000000000001000000000000100000;

                //            ARMARX_IMPORTANT << deactivateSpam(1, to_string(value32)) << "0x60FD: " << std::bitset<32>(value32) << " masked value: " << std::bitset<32>(maskedValue);
                if ((!maskedValue) == STO_On_State)
                {
                    ARMARX_VERBOSE << deactivateSpam(1, getSlaveIdentifier().getName())
                                   << getSlaveIdentifier().getName() << ": Emergency stop active";
                    return true;
                }
            }
            default:
                // this prevents warnings for unhandled cases:
                // OPERATION_ENABLE
                // QUICK_STOP_ACTIVE
                // DEFAULT_ERROR
                break;
        }

        return false;
    }


    bool
    Slave::isEmergencyStopActive() const
    {
        return checkEmergencyStopOnBus();
    }


    bool
    Slave::recoverFromEmergencyStop()
    {
        DS402State state = getState();
        switch (state)
        {
            case DS402State::FAULT:
                ARMARX_RT_LOGF_DEBUG("Clearing Emergency Stop").deactivateSpam(1);
                performTransitionFivteen();
                break;
            case DS402State::SWITCH_ON_DISABLED:
                ARMARX_RT_LOGF_DEBUG("Transition Two").deactivateSpam(1);
                performTransitionTwo();
                break;
            case DS402State::READY_TO_SWITCH_ON:
                ARMARX_RT_LOGF_DEBUG("Transition Three").deactivateSpam(1);
                performTransitionThreeStar();
                break;
            case DS402State::SWITCH_ON:
                performTransitionFour();
                ARMARX_RT_LOGF_DEBUG("Clearing Emergency Stop DONE").deactivateSpam(1);
                break;
            case DS402State::OPERATION_ENABLE:
                if (!isEmergencyStopActive())
                {
                    return true;
                }
                break;
            default:
                break;
        }

        return false;
    }


    void
    Slave::setSTO_OnState(bool state)
    {
        STO_On_State = state;
    }


    bool
    Slave::getSTO_OnState() const
    {
        return STO_On_State;
    }


    void
    Slave::setOutputPDO(void* ptr)
    {
        checkForElmoErrors();
        SlaveInterfaceWithIO::setOutputPDO(ptr);
    }


    void
    Slave::setInputPDO(void* ptr)
    {
        checkForElmoErrors();
        SlaveInterfaceWithIO::setInputPDO(ptr);
    }


    void
    Slave::prepareForSafeOp()
    {
        ARMARX_DEBUG << slaveIdentifier.getName() << " prepareForSafeOp()";

        auto state = getState();

        ARMARX_VERBOSE << deactivateSpam(1, getSlaveIdentifier().getName())
                       << getSlaveIdentifier().getName()
                       << " Preparing for safe op: current statemachine state: "
                       << std::string(DS402StateToString(state));
        if (DS402State::FAULT == state)
        {
            //on start up we plot the error and then try to reset it
            checkForElmoErrors(true);

            ARMARX_INFO << deactivateSpam(1, getSlaveIdentifier().getName())
                        << getSlaveIdentifier().getName()
                        << " Fault at start up try to reset before Safe-Op";
            sendFaultReset();
        }

        ARMARX_DEBUG << slaveIdentifier.getName() << " finished prepareForSafeOp()";
    }

    bool
    Slave::prepareForRun()
    {
        DS402State state = getState();
        ARMARX_VERBOSE << deactivateSpam(3, getSlaveIdentifier().getName())
                       << getSlaveIdentifier().getName() << " Current state of state machine: "
                       << std::string(DS402StateToString(state));
        if (state == DS402State::SWITCH_ON_DISABLED)
        {
            performTransitionTwo();
            return false;
        }

        if (DS402State::READY_TO_SWITCH_ON == state)
        {
            //go to switched on
            performTransitionThreeStar();
            return false;
        }

        if (DS402State::SWITCH_ON == state)
        {
            //go to operation enabled
            performTransitionFour();
            return false;
        }

        if (DS402State::FAULT == state)
        {
            //on start up we plot the error and then try to reset it
            SLAVE_WARNING(getSlaveIdentifier(), "Fault at startup, trying to reset elmo");
            checkForElmoErrors(true);

            performTransitionFivteen();

            return false;
        }

        if (DS402State::OPERATION_ENABLE == state)
        {
            //setting start up mode
            //inputs->controlMode = ELMO_MODE_OF_OPERATION_VELOCITY;
            return true;
        }
        else
        {
            ARMARX_INFO << "Current UNHANDLED State: " << std::string(DS402StateToString(state));
            return false;
        }
    }


    bool
    Slave::shutdown()
    {
        //stopping the motor
        //sendHalt();
        //off from Operation mode
        DS402State currentState = getState();

        if (DS402State::OPERATION_ENABLE == currentState)
        {
            //bring it to switched on
            performTransitionFive();
            return false;
        }

        if (DS402State::SWITCH_ON == currentState)
        {
            //bring to ready to switch on
            performTransitionSix();
            return false;
        }

        if (DS402State::READY_TO_SWITCH_ON == currentState)
        {
            //bring to switch on disabled
            performTransitionSeven();
            return false;
        }

        if (DS402State::FAULT == currentState || DS402State::QUICK_STOP_ACTIVE == currentState)
        {
            ARMARX_DEBUG << "Already in Fault";
            //if there is a fault during shut down we can't do anything.... so lets continue
            return true;
        }

        if (DS402State::SWITCH_ON_DISABLED == currentState)
        {
            ARMARX_INFO << deactivateSpam(5) << "elmo " << getSlaveNumber() << " is shutdown";
            return true;
        }
        else
        {
            return false;
        }
    }

    void
    Slave::doMappings()
    {
        ARMARX_DEBUG << slaveIdentifier.getName() << " doMappings()";

        armarx::control::ethercat::Bus& bus = armarx::control::ethercat::Bus::getBus();

        //write zero to all pdos that are not used
        //elmo -> master
        bus.writeSDOEntry(getSlaveNumber(), 0x1A08, 0, static_cast<std::uint16_t>(0), true);
        //master -> elmo
        bus.writeSDOEntry(getSlaveNumber(), 0x1608, 0, static_cast<std::uint16_t>(0), true);

        //write pdo-mapping in 0x1c12
        //We use complete access on the dictionary entry (write buffer last argument is true) and write the entry for the
        //mapping and the length of the array simultaneously
        //master -> elmo
        //0x1A07 is the first of the manual rx pdo mapping objects
        bus.writeSDOByteBuffer(getSlaveNumber(),
                               0x1607,
                               0,
                               sizeof(mapping_struct),
                               reinterpret_cast<const std::uint8_t*>(&rx_mapping),
                               true);
        bus.writeSDOByteBuffer(getSlaveNumber(),
                               0x1608,
                               0,
                               sizeof(mapping_struct),
                               reinterpret_cast<const std::uint8_t*>(&rx2_mapping),
                               true);
        bus.writeSDOByteBuffer(getSlaveNumber(),
                               RX_MAPPING_INDEX,
                               0,
                               rx_mapping_buf.size(),
                               rx_mapping_buf.data(),
                               true);

        //same for the other direction pdos. For explanation see 1c12
        //elmo -> master
        //0x1A07 is the first of the manual tx pdo mapping objects
        bus.writeSDOByteBuffer(getSlaveNumber(),
                               0x1A07,
                               0,
                               sizeof(mapping_struct),
                               reinterpret_cast<const std::uint8_t*>(&tx_mapping),
                               true);
        bus.writeSDOByteBuffer(getSlaveNumber(),
                               0x1A08,
                               0,
                               sizeof(mapping_struct),
                               reinterpret_cast<const std::uint8_t*>(&tx2_mapping),
                               true);
        bus.writeSDOByteBuffer(getSlaveNumber(),
                               TX_MAPPING_INDEX,
                               0,
                               tx_mapping_buf.size(),
                               tx_mapping_buf.data(),
                               true);

        //these entries are necessary to write according to the elmo specification
        //opmode
        bus.writeSDOEntry(getSlaveNumber(), 0x6060, 0, ELMO_MODE_OF_OPERATION_AT_START_UP);
        lastModeSet = ELMO_MODE_OF_OPERATION_AT_START_UP;

        //set targets
        bus.writeSDOEntry(getSlaveNumber(), 0x60FF, 0, static_cast<std::int32_t>(0));
        bus.writeSDOEntry(getSlaveNumber(), 0x607A, 0, static_cast<std::int32_t>(0));
        bus.writeSDOEntry(getSlaveNumber(), 0x6071, 0, static_cast<std::int16_t>(0));

        //cycle time
        bus.writeSDOEntry(getSlaveNumber(), 0x60C2, 1, static_cast<std::int8_t>(2));
        //disable complete access (CA) for soem for this slave, otherwise soem will not be able to bring the elmo into op mode:
        bus.deactivateCOECA(getSlaveNumber());

        //set smooth slow dow for halt signal
        bus.writeSDOEntry(getSlaveNumber(), 0x605D, 0, static_cast<std::int16_t>(1));
        //setting the quick stop motions
        bus.writeSDOEntry(getSlaveNumber(), 0x605A, 0, ELMO_QUICK_STOP_OPERATION);

        //Some values had to set so set correct targets
        //set rated torque (6076)
        bus.writeSDOEntry(getSlaveNumber(), 0x6076, 0, ratedTorque);
        //set total current
        bus.writeSDOEntry(getSlaveNumber(), 0x6075, 0, ratedCurrent);
        //set max current
        bus.writeSDOEntry(getSlaveNumber(), 0x6073, 0, maxRatedCurrent);

        //check for old errors of the elmo:
        //Read the Abort connection option code, number of errors and their detailed description
        ARMARX_INFO << deactivateSpam(2, this->getSlaveIdentifier().getName())
                    << "Checking for old errors in the elmo:";
        checkForElmoErrors(true);

#ifdef ELMO_DEBUG_MODE
        //DEBUG reading values form position mode
        std::int32_t minPos, maxPos;
        bus.readSDOEntry(getSlaveNumber(), 0x607D, 1, minPos);
        bus.readSDOEntry(getSlaveNumber(), 0x607D, 2, maxPos);
        ARMARX_DEBUG << "minPos: " << minPos << " maxPos: " << maxPos;

        //0x608F position encoder resolution
        //0x6090 velocity encoder resolution
        std::uint32_t encoder, motorrev;
        std::int32_t offset;
        std::int16_t motionProf;
        //bus.writeSDO(getSlaveNumber(), 524288 , 0x608f, 2);
        //bus.writeSDO(getSlaveNumber(), 524288 , 0x6090, 2);
        //bus.writeSDO(getSlaveNumber(), 10000, 0x607C, 0);
        bus.readSDOEntry(getSlaveNumber(), 0x608f, 1, encoder);
        bus.readSDOEntry(getSlaveNumber(), 0x608f, 2, motorrev);
        bus.readSDOEntry(getSlaveNumber(), 0x607C, 0, offset);
        bus.readSDOEntry(getSlaveNumber(), 0x6086, 0, motionProf);
        ARMARX_DEBUG << "encoder: " << encoder << " motorrev: " << motorrev << " offset: " << offset
                     << " motion: " << motionProf;
#endif
        ARMARX_DEBUG << slaveIdentifier.getName() << " finished doMappings()";
    }


    bool
    Slave::sendControlWord(ControlWord controlWord, bool forceSDOMode)
    {
        armarx::control::ethercat::Bus& bus = armarx::control::ethercat::Bus::getBus();
        if (bus.getPDOValidity() == armarx::control::ethercat::PDOValidity::Both && !forceSDOMode)
        {
            inputs->controlWord = controlWord;
            return true;
        }
        else
        {
            return bus.writeSDOEntry(getSlaveNumber(), 0x6040, 0, controlWord.value);
        }
    }

    void
    Slave::checkForElmoErrors(bool clearErrorCount)
    {
        armarx::control::ethercat::Bus& bus = armarx::control::ethercat::Bus::getBus();

        std::uint8_t numErrors = 0;
        if (!bus.readSDOEntry(getSlaveNumber(), 0x1003, 0, numErrors) || numErrors == 0)
        {
            return;
        }

        std::uint16_t currentErrorCode;
        if (numErrors >= 1)
        {
            if (bus.readSDOEntry(getSlaveNumber(), 0x603F, 0, currentErrorCode) &&
                currentErrorCode != 0)
            {
                // Here it is okay to use stringstream because reading from
                // SDO is not realtime-capable anyways

                std::stringstream ss;
                ss << "Elmo error: 0x" << std::hex << std::uppercase << currentErrorCode << std::dec
                   << " - " << std::string(EMCYErrorCodeToString(currentErrorCode));
                SLAVE_ERROR(getSlaveIdentifier(), ss.str().c_str());
            }
        }

        // There are only old errors. We increase the numer of errors by one
        // to also read the newest old errors.
        if (currentErrorCode == 0)
        {
            numErrors++;
        }


        // Print collection of previous elmo errors
        if (numErrors > 1)
        {

            std::stringstream ss;
            ss << "There are " << +(numErrors - 1) << " old errors at 0x1003:0: ";

            std::uint32_t errorField;
            for (std::uint8_t i = 1; i < numErrors; i++)
            {
                bus.readSDOEntry(getSlaveNumber(), 0x1003, i, errorField);
                ss << "\n\t0x1003:" << +i << " - 0x" << std::hex << std::uppercase << errorField
                   << std::dec << " - " << std::string(EMCYErrorCodeToString(errorField));
            }
            SLAVE_WARNING(getSlaveIdentifier(), ss.str().c_str());
        }

        std::int16_t value;
        if (bus.readSDOEntry(getSlaveNumber(), 0x6007, 0, value))
        {
            switch (value)
            {
                case 0:
                    break;
                case 1:
                    SLAVE_WARNING(this->slaveIdentifier,
                                  "Malfunction:\n Motor is off (MO=0) and motor failure code is "
                                  "0x800. The failure is reported and possibly "
                                  "activates an AUTOERR routine similar to "
                                  "other failures (MF command 603F:8130)")
                        .deactivateSpam(0.5f);
                    break;
                case 2:
                    ARMARX_RT_LOGF_VERBOSE("Device control command “Disable_voltage”:\n "
                                           "Motor is off (MO=0), but no failure indication "
                                           "is set (MF=0).")
                        .deactivateSpam(1.f);
                    break;
                case 3:
                    ARMARX_RT_LOGF_VERBOSE("Device control command “Quick_stop”:\n "
                                           "ST command is executed. Its action depends "
                                           "on unit mode (UM).")
                        .deactivateSpam(1.f);
                    break;
                default:
                    SLAVE_ERROR(getSlaveIdentifier(),
                                "Unexpected value in register 0x6007 - unclear how to handle this "
                                "situation.");
            }
        }

        if (clearErrorCount)
        {
            bus.writeSDOEntry(getSlaveNumber(), 0x1003, 0, static_cast<std::uint16_t>(0));
        }
    }


    void
    Slave::sendShutdown()
    {
        union ControlWord con;
        con.value = 0;
        con.control.quickStop = 1;
        con.control.enableVoltage = 1;

        if (!sendControlWord(con))
        {
            ARMARX_WARNING << "couldn't send shudown to elmo: " << getSlaveNumber();
        }
    }


    void
    Slave::sendSwitchOn1()
    {
        union ControlWord con;
        con.value = 0;
        con.control.quickStop = 1;
        con.control.enableVoltage = 1;
        con.control.switchOn = 1;

        if (!sendControlWord(con))
        {
            ARMARX_WARNING << "couldn't send switch on to elmo: " << getSlaveNumber();
        }
    }

    void
    Slave::sendDisableVoltage()
    {
        union ControlWord con;
        con.value = 0;
        con.control.enableVoltage = 0;

        if (!sendControlWord(con))
        {
            ARMARX_WARNING << "couldn't send disable voltage to elmo: " << getSlaveNumber();
        }
    }

    void
    Slave::sendQuickStop()
    {
        union ControlWord con;
        con.value = 0;
        con.control.enableVoltage = 1;

        if (!sendControlWord(con))
        {
            ARMARX_WARNING << "couldn't send quick stop to elmo: " << getSlaveNumber();
        }
    }

    void
    Slave::sendDisableOperation()
    {
        union ControlWord con;
        con.value = 0;
        con.control.quickStop = 1;
        con.control.enableVoltage = 1;
        con.control.switchOn = 1;

        if (!sendControlWord(con))
        {
            ARMARX_WARNING << "couldn't send disable operation to elmo: " << getSlaveNumber();
        }
    }

    void
    Slave::sendEnableOperation()
    {
        union ControlWord con;
        con.value = 0;
        con.control.quickStop = 1;
        con.control.enableVoltage = 1;
        con.control.switchOn = 1;
        con.control.enableOperation = 1;
        con.control.halt = 0;

        if (!sendControlWord(con))
        {
            ARMARX_WARNING << "couldn't send enable operation to elmo: " << getSlaveNumber();
        }
    }

    void
    Slave::sendFaultReset()
    {
        ARMARX_VERBOSE << deactivateSpam(2) << "send fault reset to:\n"
                       << getSlaveIdentifier().toMinimalString() << std::endl;
        union ControlWord con;
        con.value = 0;
        con.control.faultReset = 1;

        if (!sendControlWord(con))
        {
            ARMARX_WARNING << "couldn't send fault reset to elmo: " << getSlaveNumber();
        }
    }

    void
    Slave::sendHalt()
    {
        union ControlWord con;
        con.value = 0;
        con.control.quickStop = outputs->statusWord.status.quickStop;
        con.control.enableVoltage = outputs->statusWord.status.voltageEnabled;
        con.control.switchOn = outputs->statusWord.status.switchedOn;
        con.control.enableOperation = outputs->statusWord.status.opEnabled;
        con.control.halt = 1;

        if (!sendControlWord(con))
        {
            ARMARX_WARNING << "couldn't send halt to elmo:" << getSlaveNumber();
        }
    }

    void
    Slave::performTransitionTwo()
    {
        ARMARX_DEBUG << deactivateSpam(1, getSlaveIdentifier().getName())
                     << getSlaveIdentifier().getName() << " DS 402 StateTransition 2";
        sendShutdown();
    }

    void
    Slave::performTransitionThreeStar()
    {
        ARMARX_DEBUG << deactivateSpam(1, getSlaveIdentifier().getName())
                     << getSlaveIdentifier().getName() << " DS 402 StateTransition 3*";
        sendSwitchOn1();
    }

    void
    Slave::performTransitionFour()
    {
        ARMARX_DEBUG << deactivateSpam(1, getSlaveIdentifier().getName())
                     << getSlaveIdentifier().getName() << " DS 402 StateTransition 4";

        sendEnableOperation();
    }

    void
    Slave::performTransitionFive()
    {
        ARMARX_DEBUG << deactivateSpam(1, getSlaveIdentifier().getName())
                     << getSlaveIdentifier().getName() << " DS 402 StateTransition 5";

        sendDisableOperation();
    }

    void
    Slave::performTransitionSix()
    {
        ARMARX_DEBUG << deactivateSpam(1, getSlaveIdentifier().getName())
                     << getSlaveIdentifier().getName() << " DS 402 StateTransition 6";

        sendShutdown();
    }

    void
    Slave::performTransitionSeven()
    {
        ARMARX_DEBUG << deactivateSpam(1, getSlaveIdentifier().getName())
                     << getSlaveIdentifier().getName() << " DS 402 StateTransition 7";

        sendDisableVoltage();
    }

    void
    Slave::performTransitionEight()
    {
        ARMARX_DEBUG << deactivateSpam(1, getSlaveIdentifier().getName())
                     << getSlaveIdentifier().getName() << " DS 402 StateTransition 8";

        sendShutdown();
    }

    void
    Slave::performTransitionNine()
    {
        ARMARX_DEBUG << deactivateSpam(1, getSlaveIdentifier().getName())
                     << getSlaveIdentifier().getName() << " DS 402 StateTransition 9";

        sendDisableVoltage();
    }

    void
    Slave::performTransitionTen()
    {
        ARMARX_DEBUG << deactivateSpam(1, getSlaveIdentifier().getName())
                     << getSlaveIdentifier().getName() << " DS 402 StateTransition 10";

        sendQuickStop();
    }

    void
    Slave::performTransitionEleven()
    {
        ARMARX_DEBUG << deactivateSpam(1, getSlaveIdentifier().getName())
                     << getSlaveIdentifier().getName() << " DS 402 StateTransition 11";

        sendQuickStop();
    }

    void
    Slave::performTransitionTwelve()
    {
        ARMARX_DEBUG << deactivateSpam(1, getSlaveIdentifier().getName())
                     << getSlaveIdentifier().getName() << " DS 402 StateTransition 12";

        sendDisableVoltage();
    }

    void
    Slave::performTransitionFivteen()
    {
        ARMARX_DEBUG << deactivateSpam(1, getSlaveIdentifier().getName())
                     << getSlaveIdentifier().getName() << " DS 402 StateTransition 15";

        sendFaultReset();
    }

    void
    Slave::performTransitionSixteen()
    {
        ARMARX_DEBUG << deactivateSpam(1, getSlaveIdentifier().getName())
                     << getSlaveIdentifier().getName() << " DS 402 StateTransition 16";

        sendEnableOperation();
    }

    DS402State
    convertToState(std::uint16_t value)
    {
        //#define STATE_DEBUG
        //check for the status
        if ((value & DS402_MASK_1) == static_cast<int>(DS402State::NOT_READY_TO_SWITCH_ON))
        {
#ifdef ELMO_DEBUG_MODE
            ARMARX_INFO << deactivateSpam(1) << "Elmo::getState() - not ready to switch on"
                        << std::endl;
#endif
            return DS402State::NOT_READY_TO_SWITCH_ON;
        }
        else if ((value & DS402_MASK_1) == static_cast<int>(DS402State::SWITCH_ON_DISABLED))
        {
#ifdef ELMO_DEBUG_MODE
            ARMARX_INFO << deactivateSpam(1) << "Elmo::getState() - switch on disabled"
                        << std::endl;
#endif
            return DS402State::SWITCH_ON_DISABLED;
        }
        else if ((value & DS402_MASK_2) == static_cast<int>(DS402State::READY_TO_SWITCH_ON))
        {
#ifdef ELMO_DEBUG_MODE
            ARMARX_INFO << deactivateSpam(1) << "Elmo::getState() - ready to switch on"
                        << std::endl;
#endif
            return DS402State::READY_TO_SWITCH_ON;
        }
        else if ((value & DS402_MASK_2) == static_cast<int>(DS402State::SWITCH_ON))
        {
#ifdef ELMO_DEBUG_MODE
            ARMARX_INFO << deactivateSpam(1) << "Elmo::getState() - switch on" << std::endl;
#endif
            return DS402State::SWITCH_ON;
        }
        else if ((value & DS402_MASK_2) == static_cast<int>(DS402State::OPERATION_ENABLE))
        {
#ifdef ELMO_DEBUG_MODE
            ARMARX_INFO << deactivateSpam(1) << "Elmo::getState() - operation enable" << std::endl;
#endif
            return DS402State::OPERATION_ENABLE;
        }
        else if ((value & DS402_MASK_2) == static_cast<int>(DS402State::QUICK_STOP_ACTIVE))
        {
#ifdef ELMO_DEBUG_MODE
            ARMARX_INFO << deactivateSpam(1) << "Elmo::getState() - quick stop active" << std::endl;
#endif

            return DS402State::QUICK_STOP_ACTIVE;
        }
        else if ((value & DS402_MASK_1) == static_cast<int>(DS402State::FAULT_REACTION_ACTIVE))
        {
            return DS402State::FAULT_REACTION_ACTIVE;
        }
        else if ((value & DS402_MASK_1) == static_cast<int>(DS402State::FAULT))
        {
            return DS402State::FAULT;
        }
        ARMARX_ERROR << "default state very bad!!! StateValue: " << std::hex << value << std::dec
                     << std::endl;
        return DS402State::DEFAULT_ERROR;
    }

    DS402State
    Slave::getState() const
    {
        StatusWord statusWord;
        //get the status word via pdo or sdo
        armarx::control::ethercat::Bus& bus = armarx::control::ethercat::Bus::getBus();
        if (bus.getPDOValidity() == armarx::control::ethercat::PDOValidity::OnlyInputs ||
            bus.getPDOValidity() == armarx::control::ethercat::PDOValidity::Both)
        {
            //pdo
            statusWord = outputs->statusWord;
        }
        else
        {
            //sdo
            bus.readSDOEntry(getSlaveNumber(), 0x6041, 0, statusWord.value);
        }

        return convertToState(statusWord.value);
    }

    bool
    Slave::switchMode(ElmoControlMode mode)
    {
        //reading current mode so in case we just set the same mode again
        std::int8_t modeToSet = outputs->displayModeOfOperation;
        switch (mode)
        {
            case ElmoControlMode::CURRENT:
                ARMARX_RT_LOGF_VERBOSE("Switching to current mode");
                modeToSet = ELMO_MODE_OF_OPERATION_CURRENT_CONTROL;
                break;
            case ElmoControlMode::VELOCITY:
                ARMARX_RT_LOGF_VERBOSE("Switching to velocity mode");
                modeToSet = ELMO_MODE_OF_OPERATION_VELOCITY;
                break;
            case ElmoControlMode::DEFAULT_MODE:
                //stay in old mode
                //modeToSet = 0;
                break;
            case ElmoControlMode::POSITION:
                ARMARX_RT_LOGF_VERBOSE("Switching to position mode");
                modeToSet = ELMO_MODE_OF_OPERATION_POSITION;
                //setting position to absolute position
                inputs->controlWord.control.opModeSpecific6 = 0;
                break;
        }

        inputs->controlMode = modeToSet;
        lastModeSet = modeToSet;
        inputs->controlWord.control.halt = 0;
        return true;
    }

    ElmoControlMode
    Slave::getMode()
    {
        std::int8_t currentMode = outputs->displayModeOfOperation;
        switch (currentMode)
        {
            case ELMO_MODE_OF_OPERATION_CURRENT_CONTROL:
                return ElmoControlMode::CURRENT;
            case ELMO_MODE_OF_OPERATION_VELOCITY:
                return ElmoControlMode::VELOCITY;
            case ELMO_MODE_OF_OPERATION_POSITION:
                return ElmoControlMode ::POSITION;
            default:
                return ElmoControlMode::DEFAULT_MODE;
        }
    }

    bool
    Slave::handleErrors()
    {
        return true;
    }

    bool
    Slave::hasError()
    {
        DS402State currentState = getState();
        //we want the elmo always in the Operational Mode or quick stop enable
        switch (currentState)
        {
            case DS402State::NOT_READY_TO_SWITCH_ON:
            case DS402State::READY_TO_SWITCH_ON:
            case DS402State::SWITCH_ON_DISABLED:
            case DS402State::SWITCH_ON:
            case DS402State::FAULT_REACTION_ACTIVE:
            case DS402State::FAULT:
                return true;
            case DS402State::DEFAULT_ERROR:
                return true;
            case DS402State::OPERATION_ENABLE:
                return getMode() != static_cast<ElmoControlMode>(lastModeSet);
            case DS402State::QUICK_STOP_ACTIVE:
                return !quickStopActive;
        }
        return false;
    }

    void
    Slave::activateQuickStop()
    {
        performTransitionEleven();
        quickStopActive = true;
    }

    bool
    Slave::decativateQuickStop()
    {
        quickStopActive = false;
        //TODO bring elmo into OP again if it is not in quick stop, at the moment this is done via clearErrors
        if (DS402State::QUICK_STOP_ACTIVE == getState())
        {
            performTransitionSixteen();
            return true;
        }
        return true;
    }

    void
    Slave::prepareForOp()
    {
        //Set the mode of operation to Velocity mode to prevent jumping or hopping of the motor
        switchMode(ElmoControlMode::VELOCITY);

        inputs->targetVelocity = 0;
        inputs->targetTorque = 0;
        inputs->maxRatedCurrentTarget = maxRatedCurrent;
        inputs->targetPosition = outputs->relativeEncoderActualValue;
    }

    void
    Slave::setRatedTorque(std::uint32_t ratedTorque)
    {
        Slave::ratedTorque = ratedTorque;
    }

    void
    Slave::setRatedCurrent(std::uint32_t ratedCurrent)
    {
        Slave::ratedCurrent = ratedCurrent;
    }

    void
    Slave::setMaxRatedCurrent(std::uint16_t maxRatedCurrent)
    {
        maxRatedCurrent =
            std::min<std::uint16_t>(maxRatedCurrent, 50000); // max value of elmo is 50k
        ARMARX_DEBUG << "new max rated current is: " << maxRatedCurrent;
        this->maxRatedCurrent = maxRatedCurrent;
        if (inputs)
        {
            inputs->maxRatedCurrentTarget = maxRatedCurrent;
        }
    }

    void
    Slave::setConfiguration(const ElmoSlaveConfig& config)
    {
        setRatedCurrent(config.ratedCurrent);
        setRatedTorque(config.ratedTorque);
        setMaxRatedCurrent(config.maxCurrent);
        setSTO_OnState(config.stoOnState);
    }

    std::string
    Slave::getDefaultName()
    {
        return "ElmoGold";
    }

    bool
    Slave::isSlaveIdentifierAccepted(const armarx::control::ethercat::SlaveIdentifier& sid)
    {
        if (sid.vendorID == ELMO_VENDOR_ID)
        {
            return true;
        }

        return false;
    }

    const char*
    EMCYErrorCodeToString(std::uint32_t emcy)
    {
        std::map<std::uint32_t, const char*> emcyErrorMap = {
            {0x2340, "Short circuit"},
            {0x3120, "Under-voltage"},
            {0x3130, "AC fail, loss of phase"},
            {0x3310, "Over-voltage"},
            {0x4310, "Temperature: drive overheating"},
            {0x5280, "Gantry position error"},
            {0x5441, "Motor disabled by INHIBIT or ABORT"},
            {0x5442, "Motor disabled by switch 'additional abort motion'"},
            {0x6300, "RPDO failed"},
            {0x7121, "Motor stuck"},
            {0x7300, "Feedback error"},
            {0x7381, "Two digital Hall sensors were changed at the same time"},
            {0x7382, "Commutation process fail during motor on"},
            {0x8110, "CAN message lost (corrupted or overrun)"},
            {0x8130, "Heartbeat event"},
            {0x8140, "Recovered from bus off"},
            {0x8210, "Attempt to access a non-configured RPDO"},
            {0x8311,
             "Peak current has been exceeded. Possible reasons are drive malfunction or bad tuning "
             "of the current controller."},
            {0x8480,
             "Speed tracking error DV[2] - VX (for UM=2 or UM=4, 5) exceeded speed error limit "
             "ER[2]."},
            {0x8481, "Speed limit exceeded: VX<LL[2] or VX>HL[2]. (Compatibility only)"},
            {0x8611,
             "Position tracking error DV[3] - PX (UM=5) or DV[3] - PY (UM=4) exceeded position "
             "error limit ER[3]."},
            {0x8680,
             "Position limit exceeded: PX<LL[3] or PX>HL[3] (UM=5), or PY<LL[3] or PY>HL[3] "
             "(UM=4). (Compatibility only)"},
            {0xFF01, "Request by user program EMCY(N) function"},
            {0xFF02,
             "IP mode underflow or other (check document MAN-G-DS301 - (2.002) p.58 for details)"},
            {0xFF10, "Failed to start motor"},
            {0xFF20, "Safety Torque Off in use (Emergency Stop active)"},
            {0xFF40, "Gantry Slave Disabled"},
        };

        if (emcyErrorMap.count(emcy))
        {
            return emcyErrorMap.at(emcy);
        }
        else
        {
            return "Unknown EMCY error code";
        }
    }

    const char*
    DS402StateToString(DS402State state)
    {
        switch (state)
        {
            case DS402State::NOT_READY_TO_SWITCH_ON:
                return "NOT_READY_TO_SWITCH_ON";
            case DS402State::SWITCH_ON_DISABLED:
                return "SWITCH_ON_DISABLED";
            case DS402State::READY_TO_SWITCH_ON:
                return "READY_TO_SWITCH_ON";
            case DS402State::SWITCH_ON:
                return "SWITCH_ON";
            case DS402State::OPERATION_ENABLE:
                return "OPERATION_ENABLE";
            case DS402State::QUICK_STOP_ACTIVE:
                return "QUICK_STOP_ACTIVE";
            case DS402State::FAULT_REACTION_ACTIVE:
                return "FAULT_REACTION_ACTIVE";
            case DS402State::FAULT:
                return "FAULT";
            case DS402State::DEFAULT_ERROR:
                return "DEFAULT_ERROR";
        }
        return "Unknown DS402State";
    }

} // namespace devices::ethercat::common::elmo::gold
