#pragma once

#include <cstdint>

#include <armarx/control/hardware_config/SlaveConfig.h>
#include <armarx/control/hardware_config/Types.h>

namespace devices::ethercat::common::elmo::gold
{
    namespace hwconfig = armarx::control::hardware_config;

    struct ElmoSlaveConfig
    {
        ElmoSlaveConfig(hwconfig::SlaveConfig& config) :
            ratedTorque(config.getUint("RatedTorque")),
            ratedCurrent(config.getUint("RatedCurrent")),
            maxCurrent(config.getUint("MaxCurrent")),
            // TODO: Should this flag be inverted
            stoOnState(config.getBool("InvertedSTOValue"))
        {
        }
        std::uint32_t ratedTorque;
        std::uint32_t ratedCurrent;
        std::uint16_t maxCurrent;
        bool stoOnState;
    };

    /// This config object defines the configuration fields needed for velocity control
    struct ElmoVelocityDataConfig
    {
        ElmoVelocityDataConfig(hwconfig::SlaveConfig& config) :
            targetVelocity(config.getLinearConfig("velocity")),
            targetAcceleration(config.getLinearConfig("acceleration")),
            targetDeceleration(config.getLinearConfig("deceleration")),
            velocity(config.getLinearConfig("velocity")),
            currentValue(config.getLinearConfig("current"))
        {
        }
        hwconfig::types::LinearConfig targetVelocity;
        hwconfig::types::LinearConfig targetAcceleration;
        hwconfig::types::LinearConfig targetDeceleration;
        hwconfig::types::LinearConfig velocity;
        hwconfig::types::LinearConfig currentValue;
    };

    /// Combined config for use in Device
    struct ElmoVelocityConfig :
            public common::elmo::gold::ElmoSlaveConfig,
            public ElmoVelocityDataConfig
    {
        ElmoVelocityConfig(hwconfig::SlaveConfig& config) :
            common::elmo::gold::ElmoSlaveConfig(config),
            ElmoVelocityDataConfig(config)
        {
        }
    };

    /// This config object defines all configuration fields
    struct ElmoConfig : public ElmoSlaveConfig
    {
        ElmoConfig(hwconfig::SlaveConfig& config) :
            ElmoSlaveConfig(config),
            loLimit(config.getFloat("loLimit")),
            hiLimit(config.getFloat("hiLimit")),
            positionSoftLimitMargin(config.getFloat("positionSoftLimitMargin")),
            limitless(config.getBool("limitless")),
            velocity(config.getLinearConfig("velocity")),
            relPosition(config.getLinearConfig("relativePosition")),
            currentValue(config.getLinearConfig("current")),
            targetPosition(config.getLinearConfig("targetPosition")),
            targetCurrent(config.getLinearConfig("targetTorque")),
            targetVelocity(config.getLinearConfig("velocity")),
            targetAcceleration(config.getLinearConfig("acceleration")),
            targetDeceleration(config.getLinearConfig("deceleration"))
        {
        }

        // Limits
        float loLimit;
        float hiLimit;
        float positionSoftLimitMargin;
        bool limitless;

        // Conversion Parameters
        hwconfig::types::LinearConfig velocity;
        hwconfig::types::LinearConfig relPosition;
        hwconfig::types::LinearConfig currentValue;
        hwconfig::types::LinearConfig targetPosition;
        hwconfig::types::LinearConfig targetCurrent;
        hwconfig::types::LinearConfig targetVelocity;
        hwconfig::types::LinearConfig targetAcceleration;
        hwconfig::types::LinearConfig targetDeceleration;
    };
}
