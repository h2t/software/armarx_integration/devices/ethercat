#pragma once

#include <cstdint>
#include <memory>

namespace devices::ethercat::common::elmo::gold
{
    //Mapping
    /**
     * This collects all infos about one mapped entry.
     * Important is to first set the lower index bytes before the upper index bytes because the elmo will use LSB order.
     * Also be sure that this struct is packed because the data has to be directly next to each other, we don't want any padding by the compiler.
     * @see armarx::mapping_struct
     */
    struct mapping_entry
    {
        std::uint8_t sizeInBits = 0;
        std::uint8_t subIndex = 0;
        std::uint8_t lowerIndexByte = 0;
        std::uint8_t upperIndexByte = 0;
    };


    /**
     * This struct holds the complete buffer for one of the two mapping objects that can be configured manually.
     * These are the objects 0x1607and 1608 for master->slave for slave->master 0x1A07 and 0x1A08 are the right objects.
     * The struct is for use with union to generate the buffer for the indices mentioned above.
     * @see armarx::mapping_union
     */
    struct mapping_struct
    {
        /** the number of the entries that the elmo should consider for mapping*/
        std::uint8_t number_of_entries; // max number is 7 - 8th entry seems to be broken
        // first index is only 8 bit but buffer will take 16 bit
        std::uint8_t pad;
        //only 8 mapping entries are possible for elmos
        mapping_entry mapping_entry1;
        mapping_entry mapping_entry2;
        mapping_entry mapping_entry3;
        mapping_entry mapping_entry4;
        mapping_entry mapping_entry5;
        mapping_entry mapping_entry6;
        mapping_entry mapping_entry7;
        mapping_entry mapping_entry8;
    };


    //RxPDO written to index 0x1C12
    /** this is the buffer to map rx entries that is written to index 0x1C12
    * ATTENTION if you change this buffer you have to change the struct below as well!
    * with elmo only 4 PDO mapping entries are allowed see DS301 beginning at chapter 5.1
    * <data typ , lenght = 2 * nummber of entries>
     * @see armarx::ELMO_in_t
    */
    [[maybe_unused]] static constexpr std::array<unsigned char, 6> rx_mapping_buf{
        //number of entries we want to use for mapping
        {0x02,
         0x00,
         //the first of the two manual mapping entries
         0x07,
         0x16,
         //the second of the two manual mapping entries
         0x08,
         0x16}};


    /**
     * This creates the buffer for the mapping object. The buffer can be written into the 0x1607 or 0x1608 Object.
     * If you want to CHANGE THE MAPPING READ THIS:
     * One can use from 0 to 8 entries in the 8 entries of the struct.
     * If you add or delete an Entry it is very important that you also change the definition of the struct ELMO_in_t.
     * If this is not done, segmentation faults or very strange behavior will be just a question of time!
     * SO ALWAYS DO THAT!!!!
     * @see armarx::mapping_union
     * @see armarx::ELMO_in_t
     * @return mapping buffer for 0x1607 or 0x1608
     */
    constexpr mapping_struct
    createRXMapping()
    {
        mapping_struct mapping{};
        mapping.number_of_entries = 7;
        // name Datatype size(in Bits) Index(Subindex)
        //first entry
        // Target velocity DINT 32 0x60FF(0)
        mapping.mapping_entry1.sizeInBits = 32;
        mapping.mapping_entry1.subIndex = 0;
        mapping.mapping_entry1.lowerIndexByte = 0xFF;
        mapping.mapping_entry1.upperIndexByte = 0x60;
        //second entry
        // Target position DINT 32 0x607A()
        mapping.mapping_entry2.sizeInBits = 32;
        mapping.mapping_entry2.subIndex = 0;
        mapping.mapping_entry2.lowerIndexByte = 0x7A;
        mapping.mapping_entry2.upperIndexByte = 0x60;
        //third entry
        // profiledDeceleration UINT 32 0x6084
        mapping.mapping_entry3.sizeInBits = 32;
        mapping.mapping_entry3.subIndex = 0;
        mapping.mapping_entry3.lowerIndexByte = 0x84;
        mapping.mapping_entry3.upperIndexByte = 0x60;
        //fourth entry
        // profiledAcceleration UINT 32 0x6083
        mapping.mapping_entry4.sizeInBits = 32;
        mapping.mapping_entry4.subIndex = 0;
        mapping.mapping_entry4.lowerIndexByte = 0x83;
        mapping.mapping_entry4.upperIndexByte = 0x60;
        //fith entry
        // max current value
        mapping.mapping_entry5.sizeInBits = 16;
        mapping.mapping_entry5.subIndex = 0;
        mapping.mapping_entry5.lowerIndexByte = 0x73;
        mapping.mapping_entry5.upperIndexByte = 0x60;
        //sixth entry
        // Controlword UINT 16 0x6040(0)
        mapping.mapping_entry6.sizeInBits = 16;
        mapping.mapping_entry6.subIndex = 0;
        mapping.mapping_entry6.lowerIndexByte = 0x40;
        mapping.mapping_entry6.upperIndexByte = 0x60;
        //seventh entry
        // Target Tourque INT 16 0x6071(X)
        mapping.mapping_entry7.sizeInBits = 16;
        mapping.mapping_entry7.subIndex = 0;
        mapping.mapping_entry7.lowerIndexByte = 0x71;
        mapping.mapping_entry7.upperIndexByte = 0x60;
        //eighth entry
        // DO NOT USE 8th ENTRY - SEEMS BROKEN
        //    mapping.mapping_entry8.sizeInBits = 24;
        //    mapping.mapping_entry8.subIndex = 0;
        //    mapping.mapping_entry8.lowerIndexByte = 0x60;
        //    mapping.mapping_entry8.upperIndexByte = 0x60;

        return mapping;
    }

    constexpr mapping_struct
    createRX2Mapping()
    {
        mapping_struct mapping{};
        mapping.number_of_entries = 2;
        // name Datatype size(in Bits) Index(Subindex)
        //first entry

        // Mode of Operation SINT 8 0x6060(0)
        mapping.mapping_entry1.sizeInBits = 8;
        mapping.mapping_entry1.subIndex = 0;
        mapping.mapping_entry1.lowerIndexByte = 0x60;
        mapping.mapping_entry1.upperIndexByte = 0x60;

        // polarity UINT 8 0x607E(0)
        mapping.mapping_entry2.sizeInBits = 8;
        mapping.mapping_entry2.subIndex = 0;
        mapping.mapping_entry2.lowerIndexByte = 0x7E;
        mapping.mapping_entry2.upperIndexByte = 0x60;

        return mapping;
    }


    //TX elmo -> master mapping written to 0x1C13
    /** This is the buffer to map tx entries that is written to index 0x1C13
    * ATTENTION if you change this buffer you have to change the struct below also!!!!
    * OTHERWISE will get fucked up!!!
    * with elmo only 4 PDO mapping entries are allowed see DS301 begining caper  of 5.2
    * <data typ , lenght = 2 * nummber of entries>
     * @see armarx::ELMO_out_t
    */
    [[maybe_unused]] static constexpr std::array<unsigned char, 6> tx_mapping_buf{
        //number of entries we want to use for mapping
        {0x02,
         0x00,
         //the first of the two manual mapping entries
         0x07,
         0x1A,
         0x08,
         0x1A

        }};


    /**
     * This creates the buffer for the mapping object. The buffer can be written into the 0x1A07 or 0x1A08 Object.
     * If you want to CHANGE THE MAPPING READ THIS:
     * One can use from 0 to 8 entries in the 8 entries of the struct.
     * If you add or delete an Entry it is very important that you also change the definition of the struct ELMO_out_t.
     * If this is not done, segmentation faults or very strange behavior will be just a question of time!
     * SO ALWAYS DO THAT!!!!
     * @see armarx::mapping_union
     * @see armarx::ELMO_out_t
     * @return mapping buffer for 0x1607 or 0x1608
     */
    constexpr mapping_struct
    createTXMapping()
    {
        mapping_struct mapping{};
        mapping.number_of_entries = 7;
        // name Datatype size(in Bits) Index(Subindex)
        //first entry
        // position actual value DINT 32 0x6064
        mapping.mapping_entry1.sizeInBits = 32;
        mapping.mapping_entry1.subIndex = 0;
        mapping.mapping_entry1.lowerIndexByte = 0x64;
        mapping.mapping_entry1.upperIndexByte = 0x60;
        //second entry
        // velocity actual value DINT 32 0x606C
        mapping.mapping_entry2.sizeInBits = 32;
        mapping.mapping_entry2.subIndex = 0;
        mapping.mapping_entry2.lowerIndexByte = 0x6C;
        mapping.mapping_entry2.upperIndexByte = 0x60;
        //third entry
        // Elmo status register UINT 32 0x1002
        mapping.mapping_entry3.sizeInBits = 32;
        mapping.mapping_entry3.subIndex = 0;
        mapping.mapping_entry3.lowerIndexByte = 0x02;
        mapping.mapping_entry3.upperIndexByte = 0x10;
        //fourth entry
        // Digital Inputs u32Bit
        mapping.mapping_entry4.sizeInBits = 32;
        mapping.mapping_entry4.subIndex = 0;
        mapping.mapping_entry4.lowerIndexByte = 0xFD;
        mapping.mapping_entry4.upperIndexByte = 0x60;
        //fith entry
        // status Word UINT 16 0x6041(0)
        mapping.mapping_entry5.sizeInBits = 16;
        mapping.mapping_entry5.subIndex = 0;
        mapping.mapping_entry5.lowerIndexByte = 0x41;
        mapping.mapping_entry5.upperIndexByte = 0x60;
        //sixth entry
        // torque actual value INT 16 0x6077
        mapping.mapping_entry6.sizeInBits = 16;
        mapping.mapping_entry6.subIndex = 0;
        mapping.mapping_entry6.lowerIndexByte = 0x77;
        mapping.mapping_entry6.upperIndexByte = 0x60;
        //seventh entry
        // current actual value INT 16 0x6078
        mapping.mapping_entry7.sizeInBits = 16;
        mapping.mapping_entry7.subIndex = 0;
        mapping.mapping_entry7.lowerIndexByte = 0x78;
        mapping.mapping_entry7.upperIndexByte = 0x60;

        //eighth entry

        return mapping;
    }

    constexpr mapping_struct
    createTX2Mapping()
    {
        mapping_struct mapping{};
        mapping.number_of_entries = 2;
        // name Datatype size(in Bits) Index(Subindex)
        //first entry
        // display mode of operation SINT 8 0x6061
        mapping.mapping_entry1.sizeInBits = 8;
        mapping.mapping_entry1.subIndex = 0;
        mapping.mapping_entry1.lowerIndexByte = 0x61;
        mapping.mapping_entry1.upperIndexByte = 0x60;

        //second entry
        // display mode of operation SINT 8 0x6061
        mapping.mapping_entry2.sizeInBits = 8;
        mapping.mapping_entry2.subIndex = 0;
        mapping.mapping_entry2.lowerIndexByte = 0x61;
        mapping.mapping_entry2.upperIndexByte = 0x60;

        return mapping;
    }

} // namespace devices::ethercat::common::elmo::gold
