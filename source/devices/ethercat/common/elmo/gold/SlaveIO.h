#pragma once


// STD/STL
#include <cstdint>


namespace devices::ethercat::common::elmo::gold
{

    struct StatusWordStruct
    {
        std::uint8_t readyToSwitchOn : 1;
        std::uint8_t switchedOn : 1;
        std::uint8_t opEnabled : 1;
        std::uint8_t fault : 1;
        std::uint8_t voltageEnabled : 1;
        std::uint8_t quickStop : 1;
        std::uint8_t switchOnDisabled : 1;
        std::uint8_t warning : 1;
        std::uint8_t dontCare1 : 1;
        std::uint8_t remote : 1;
        std::uint8_t targetReached : 1;
        std::uint8_t internalLimitActive : 1;
        std::uint8_t opModeSpecific : 2;
        std::uint8_t dontCare2 : 2;
    };


    union StatusWord
    {
        StatusWordStruct status;
        std::uint16_t value;
    };


    struct ControlWordStruct
    {
        std::uint8_t switchOn : 1;
        std::uint8_t enableVoltage : 1;
        std::uint8_t quickStop : 1;
        std::uint8_t enableOperation : 1;
        std::uint8_t opModeSpecific4 : 1;
        std::uint8_t opModeSpecific5 : 1;
        std::uint8_t opModeSpecific6 : 1;
        std::uint8_t faultReset : 1;
        std::uint8_t halt : 1;
        std::uint8_t reserved : 7;
    };


    union ControlWord
    {
        ControlWordStruct control;
        std::uint16_t value;
    };


    /**
     * @brief PDO mapping master -> elmo
     * If this is changed you have to be sure you changed your mappings setting also!!
     * IMPORTANT this has to be packed, because SOEM gives us the data directly in this order without any paddings.
     * So we also don't want your compiler to add some padding. Otherwise it will cause strange behaviour.
     * @see armarx::createRXMapping()
     */
    struct SlaveIn
    {
        /** sets the target velocity in ticks/seconds */
        std::int32_t targetVelocity;
        /** sets the target position in ticks */
        std::int32_t targetPosition;
        /** sets the profiledDeceleration */
        std::uint32_t profiledDeceleration;
        /** sets the profiledAcceleration value */
        std::uint32_t profiledAcceleration;
        /** sets maxRatedCurrent for velocity/torque control */
        std::uint16_t maxRatedCurrentTarget;
        /** with this you control the DS402 state machine and some other stuff */
        ControlWord controlWord;
        /** sets actually the current */
        std::int16_t targetTorque;
        /** decides with target the elmo will take */
        std::int8_t controlMode;

        std::uint8_t polarity;
    } __attribute__((__packed__));
    //packed to prevent that the compile add some padding


    /** elmo -> master
     * If this is changed you have to be sure you changed your mapping setting also!!
     * IMPORTANT this has to be packed, because SOEM gives us the data directly in this order without any paddings.
     * So we also don't want your compiler to add some padding. Otherwise it will cause strange behaviour.
     * @see armarx::createTXMapping()
     */
    struct SlaveOut
    {
        /** position actual value in ticks from the relativ encoder */
        std::int32_t relativeEncoderActualValue;
        /** velocity actual value in ticks/second */
        std::int32_t velocityActualValue;
        /** elmo status register */
        std::uint32_t statusRegister;
        std::uint32_t digitalInputs;
        /** status word to check the state of the elmo */
        StatusWord statusWord;
        /** torque actual value in a strange format see DS 402 for more info */
        std::int16_t torqueActualValue;
        /** currentActualValue in a strange format see DS 402 for more info */
        std::int16_t currentActualValue;
        /** display mode of operation, this indicates which target the elmo will use */
        std::int8_t displayModeOfOperation;

        std::uint8_t pad1 = 0;
        // TODO: also map extra status register 0x2085?
    } __attribute__((__packed__));
    //packed to prevent that the compile add some padding

}
