#pragma once


// armarx
#include <RobotAPI/components/units/RobotUnit/Devices/ControlDevice.h>
#include <RobotAPI/components/units/RobotUnit/Devices/SensorDevice.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <armarx/control/ethercat/DataInterface.h>
#include <armarx/control/rt_filters/AverageFilter.h>
#include <armarx/control/rt_filters/FirFilter.h>

// robot_devices
#include <devices/ethercat/common/elmo/gold/Slave.h>
#include <devices/ethercat/common/elmo/gold/Config.h>


namespace devices::ethercat::common::elmo::gold
{

    class Data :
        public armarx::control::ethercat::DataInterface
    {

    public:

        Data(ElmoConfig elmo_config, SlaveOut* elmo_out, SlaveIn* elmo_in);

        // AbstractData interface

    public:

        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        armarx::ControlMode getActualControlMode();

        float getActualRelativePosition() const;

        float getActualVelocity() const;
        float getActualFilteredVelocity() const;

        void setTargetVelocity(float target);
        float getTargetVelocity() const;
        void setTargetPosition(float target);
        float getTargetPosition() const;
        void setTargetCurrent(float target);
        float getTargetCurrent() const;
        void setMaxTargetCurrent(float target);
        void resetMaxTargetCurrent();
        float getMaxDefaultTargetCurrent() const;
        float getMaxTargetCurrent() const;
        const float& getActualCurrent();

        const float& getActualAcceleration();

        float getSoftLimitHi();

        float getSoftLimitLo();

        bool isLimitLess() const;
        const float& getTargetAcceleration() const;

        void setTargetAcceleration(float value);
        const float& getTargetDeceleration() const;

        void setTargetDeceleration(float value);

        const armarx::JointStatus& getStatus();


    protected:

        float shiftAngleToLimits(float angle);
        void calculateAcceleration(float lastActualVelocity, const IceUtil::Time& now);
        void updateElmoState();

        SlaveOut* elmo_out;
        SlaveIn* elmo_in;

        armarx::control::ethercat::LinearConvertedValue<std::int32_t> targetVelocity;
        armarx::control::ethercat::LinearConvertedValue<std::uint32_t> targetAcceleration;
        armarx::control::ethercat::LinearConvertedValue<std::uint32_t> targetDeceleration;

        armarx::control::ethercat::LinearConvertedValue<std::int32_t> targetPosition;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> targetCurrent;
        float defaultMaxTargetCurrent = 0;
        armarx::control::ethercat::LinearConvertedValue<std::uint16_t> maxTargetCurrent;

        armarx::control::ethercat::LinearConvertedValue<std::int32_t> relPosition;
        armarx::control::ethercat::LinearConvertedValue<std::int32_t> velocity;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> currentValue;

        IceUtil::Time lastTimestamp;
        float calculatedActualAcceleration;

        armarx::control::rt_filters::RtAverageFilter velocityFilter;
        float filteredVelocity;

        armarx::JointStatus status;
        const bool STO_On_State;
        std::int8_t* controlModePtr;

        float positionLimitLo;
        float positionLimitHi;
        float limitCenter;
        float positionSoftLimitMargin = 0;
        bool limitLess = false;

    };

} // namespace armarx
