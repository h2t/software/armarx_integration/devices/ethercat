#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

#include <devices/ethercat/common/elmo/gold/Data.h>
#include <devices/ethercat/common/sensor_board/armar6/Slave.h>


namespace devices::ethercat::common::elmo::gold
{

    Data::Data(ElmoConfig elmo_config,
               SlaveOut* elmo_out,
               SlaveIn* elmo_in) :
        elmo_out(elmo_out), elmo_in(elmo_in), velocityFilter(20), STO_On_State(elmo_config.stoOnState)
    {
        ARMARX_CHECK_EXPRESSION(elmo_out);
        ARMARX_CHECK_EXPRESSION(elmo_in);

        velocity.init(&(elmo_out->velocityActualValue),
                      elmo_config.velocity,
                      std::nanf(""),
                      true,
                      "velocity");
        relPosition.init(&(elmo_out->relativeEncoderActualValue),
                         elmo_config.relPosition,
                         std::nanf(""),
                         true,
                         "relPosition");

        currentValue.init(&elmo_out->currentActualValue,
                          elmo_config.currentValue,
                          std::nanf(""),
                          true,
                          "currentValue");
        targetPosition.init(&elmo_in->targetPosition,
                            elmo_config.targetPosition,
                            std::nanf(""),
                            true,
                            "targetPosition");
        targetCurrent.init(&elmo_in->targetTorque,
                           elmo_config.targetCurrent,
                           std::nanf(""),
                           true,
                           "targetCurrent");
        defaultMaxTargetCurrent = elmo_config.maxCurrent;
        maxTargetCurrent.init(&elmo_in->maxRatedCurrentTarget,
                              0.001f * elmo_config.ratedCurrent,
                              0.0f,
                              defaultMaxTargetCurrent,
                              true,
                              "maxTargetCurrent");
        targetVelocity.init(&elmo_in->targetVelocity,
                            elmo_config.targetVelocity,
                            std::nanf(""),
                            true,
                            "targetVelocity");
        targetAcceleration.init(&elmo_in->profiledAcceleration,
                                elmo_config.targetAcceleration,
                                2,
                                true,
                                "targetAcceleration");
        targetDeceleration.init(&elmo_in->profiledDeceleration,
                                elmo_config.targetDeceleration,
                                5,
                                true,
                                "targetDeceleration");

        positionLimitLo = elmo_config.loLimit;
        positionLimitHi = elmo_config.hiLimit;

        positionSoftLimitMargin = elmo_config.positionSoftLimitMargin;
        this->limitLess = elmo_config.limitless;
        limitCenter = this->limitLess ? 0 : (positionLimitLo + positionLimitHi) / 2;

        controlModePtr = &elmo_out->displayModeOfOperation;
    }


    void
    Data::rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                             const IceUtil::Time& timeSinceLastIteration)
    {
        float lastFilteredActualVelocity = filteredVelocity;

        relPosition.read();
        velocity.read();

        filteredVelocity = velocityFilter.update(velocity.value);
        currentValue.read();

        updateElmoState();
        calculateAcceleration(lastFilteredActualVelocity, sensorValuesTimestamp);
    }


    void
    Data::rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                              const IceUtil::Time& timeSinceLastIteration)
    {
        targetPosition.write();
        targetCurrent.write();
        maxTargetCurrent.write();
        targetVelocity.write();
        targetAcceleration.write();
        targetDeceleration.write();
    }


    float
    Data::getActualRelativePosition() const
    {
        return relPosition.value;
    }


    float
    Data::getActualVelocity() const
    {
        return velocity.value;
    }


    float
    Data::getActualFilteredVelocity() const
    {
        return filteredVelocity;
    }


    void
    Data::setTargetVelocity(float target)
    {
        targetVelocity.value = target;
    }


    float
    Data::getTargetVelocity() const
    {
        return targetVelocity.value;
    }


    void
    Data::setTargetPosition(float target)
    {
        targetPosition.value = target;
    }


    float
    Data::getTargetPosition() const
    {
        return targetPosition.value;
    }


    void
    Data::setTargetCurrent(float target)
    {
        targetCurrent.value = target;
    }


    void
    Data::setMaxTargetCurrent(float target)
    {
        target = fabs(target);
        target = std::min(target, defaultMaxTargetCurrent);
        ARMARX_VERBOSE << "Setting new maxTarget Current to " << target;
        maxTargetCurrent.value = target;
    }


    void
    Data::resetMaxTargetCurrent()
    {
        ARMARX_VERBOSE << "Resetting new maxTarget Current to " << defaultMaxTargetCurrent;
        maxTargetCurrent.value = defaultMaxTargetCurrent;
    }


    float
    Data::getMaxDefaultTargetCurrent() const
    {
        return defaultMaxTargetCurrent;
    }


    float
    Data::getMaxTargetCurrent() const
    {
        return maxTargetCurrent.value;
    }


    float
    Data::getTargetCurrent() const
    {
        return targetCurrent.value;
    }


    const float&
    Data::getActualCurrent()
    {
        return currentValue.value;
    }


    const float&
    Data::getActualAcceleration()
    {
        return calculatedActualAcceleration;
    }


    float
    Data::getSoftLimitHi()
    {
        return limitLess ? positionLimitHi : positionLimitHi - positionSoftLimitMargin;
    }


    float
    Data::getSoftLimitLo()
    {
        return limitLess ? positionLimitLo : positionLimitLo + positionSoftLimitMargin;
    }


    const float&
    Data::getTargetAcceleration() const
    {
        return targetAcceleration.value;
    }


    void
    Data::setTargetAcceleration(float value)
    {
        targetAcceleration.value = value;
    }


    const float&
    Data::getTargetDeceleration() const
    {
        return targetDeceleration.value;
    }


    void
    Data::setTargetDeceleration(float value)
    {
        targetDeceleration.value = value;
    }


    const armarx::JointStatus&
    Data::getStatus()
    {
        return status;
    }


    float
    Data::shiftAngleToLimits(float angle)
    {
        if (angle > limitCenter + M_PI)
        {
            angle -= 2 * M_PI;
        }
        else if (angle < limitCenter - M_PI)
        {
            angle += 2 * M_PI;
        }
        return angle;
    }


    void
    Data::calculateAcceleration(float lastActualVelocity, const IceUtil::Time& now)
    {
        if (lastTimestamp.toMicroSecondsDouble() > 0)
        {
            IceUtil::Time timeDelta = now - lastTimestamp;

            if (timeDelta.toSecondsDouble() > 0)
            {
                calculatedActualAcceleration =
                    (filteredVelocity - lastActualVelocity) / timeDelta.toSecondsDouble();
            }
            else
            {
                calculatedActualAcceleration = 0;
            }
        }

        lastTimestamp = now;
    }


    void
    Data::updateElmoState()
    {
        status.enabled = elmo_out->statusWord.status.opEnabled;
        status.emergencyStop =
            (!(elmo_out->digitalInputs & ELMO_DIGITAL_INPUTS_INTERLOCK)) == STO_On_State;

        if (elmo_out->statusWord.status.opEnabled)
        {
            status.operation = armarx::eOnline;
        }
        else
        {
            status.operation = armarx::eOffline;
        }

        if (elmo_out->statusWord.status.fault)
        {
            status.error = armarx::eError;
        }
        else if (elmo_out->statusWord.status.warning)
        {
            status.error = armarx::eWarning;
        }
        else
        {
            status.error = armarx::eOk;
        }
    }


    bool
    Data::isLimitLess() const
    {
        return limitLess;
    }

} // namespace devices::ethercat::common::elmo::gold
