#pragma once


// STD/STL
#include <array>
#include <chrono>
#include <cstdint>
#include <memory>

// armarx
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/core/util/StringHelpers.h>
#include <armarx/control/ethercat/SlaveInterface.h>

#include <devices/ethercat/common/elmo/gold/Config.h>
#include <devices/ethercat/common/elmo/gold/SlaveIO.h>

namespace devices::ethercat::common::elmo::gold
{
    // Masks.
    static constexpr std::uint32_t ELMO_DIGITAL_INPUTS_INTERLOCK =
        0x8; // contains the emergency flag in field 0x60FD

    /**
     * Describes the states of the DS 402 State machine.
     * The indices coming from the status bits of the the Status Word.
     * So to check for the state just use the bit mask for the state as it is given for every state,
     * to identify if this state is the current one.
     * @see StatusWord
     */
    enum class DS402State : int
    {
        /** Use DS402_MASK_1 */
        NOT_READY_TO_SWITCH_ON = 0x00,
        /** Use DS402_MASK_1 */
        SWITCH_ON_DISABLED = 0x40,
        /** Use DS402_MASK_2 */
        READY_TO_SWITCH_ON = 0x21,
        /** Use DS402_MASK_2 */
        SWITCH_ON = 0x23,
        /** Use DS402_MASK_2 */
        OPERATION_ENABLE = 0x27,
        /** Use DS402_MASK_2 */
        QUICK_STOP_ACTIVE = 0x07,
        /** Use DS402_MASK_1 */
        FAULT_REACTION_ACTIVE = 0x0F,
        /** Use DS402_MASK_1 */
        FAULT = 0x08,
        /** This is just to indcate there is an internal error **/
        DEFAULT_ERROR = 0xFFFF
    };

    const char* DS402StateToString(DS402State state);

    const char* EMCYErrorCodeToString(std::uint32_t emcy);


    enum class ElmoControlMode : int
    {
        /** set target current to control elmo */
        CURRENT = 4,
        /** This mode is to indicate an error, there is no real default mode in ds 402 */
        DEFAULT_MODE = 0,
        /**Velocity mode */
        VELOCITY = 3,
        /** position mode */
        POSITION = 1
    };


    class Slave : public armarx::control::ethercat::SlaveInterfaceWithIO<SlaveIn, SlaveOut>
    {

    public:
        Slave(const armarx::control::ethercat::SlaveIdentifier sid);

        /**
         * Prepares the elmo to switch in EtherCAT OP-Mode. Here the PDO Mappings for the ELMO are set.
         * Also some stuff is configure here to make it possible to use the elmo.
         * @see AbstractSlave::doMappings()
         */
        void doMappings() override;

        void prepareForOp() override;

        /**
         * Brings the Elmo form state SWITCH ON DISABLED to state OPERATION ENABLE
         * @see DS402State
         * @see AbstractSlave::prepare()
         */
        bool prepareForRun() override;

        /**
         * @see AbstractSlave::execute()
         */
        void execute() override;

        /**
         * @see AbstractSlave::shutdown()
         */
        bool shutdown() override;

        void setInputPDO(void* ptr) override;

        void setOutputPDO(void* ptr) override;

        void prepareForSafeOp() override;

        void setRatedTorque(std::uint32_t ratedTorque);

        void setRatedCurrent(std::uint32_t ratedCurrent);
        void setMaxRatedCurrent(std::uint16_t maxRatedCurrent);

        void setConfiguration(const ElmoSlaveConfig& config);

        /**
         * This checks if the elmo ist in DS402 State fault or fault reaction. In this case there is something wrong with it.
         * @see AbstractSlave::hasError
         * @return true if there is a error/fault otherwise false
         */
        bool hasError() override;

        bool handleErrors() override;

        /**
         * Changes the control mode of the Elmo, it the mode was invalid then elmo stays in old mode.
         * @see armarx::ControlMode
         * @param mode
         * @return
         */
        bool switchMode(ElmoControlMode mode);

        /**
         * Reads the current control mode from the elmo
         * @return
         */
        ElmoControlMode getMode();

        void activateQuickStop();

        bool decativateQuickStop();

        /**
         * This returns the current State of the elmo.
         * The current PDO is used, so this only returns the last state received via PDO
         * @see StatusWord
         * @see DS402State
         * @return the current State of the Elmo
         */
        DS402State getState() const;

        bool checkEmergencyStopOnBus() const;
        bool isEmergencyStopActive() const override;
        bool recoverFromEmergencyStop() override;

        void setSTO_OnState(bool state);
        bool getSTO_OnState() const;

        static std::string getDefaultName();
        static bool
        isSlaveIdentifierAccepted(const armarx::control::ethercat::SlaveIdentifier& sid);

    private:
        /**
         * Sending shutdown command
         * @see DS 402 Guide Object 0x6040
         */
        void sendShutdown();

        /**
         * Sending switch on (option 1) command
         * This option set the enable operation bit to zero
         * @see DS 402 Guide Object 0x6040
         */
        void sendSwitchOn1();

        /**
         * Sending disable voltage command
         * @see DS 402 Guide Object 0x6040
         */
        void sendDisableVoltage();

        /**
         * Sending disable voltage command
         * @see DS 402 Guide Object 0x6040
         */
        void sendQuickStop();

        /**
         * Sending disable operation command
         * @see DS 402 Guide Object 0x6040
         */
        void sendDisableOperation();

        /**
         * Sending enable operation command
         * @see DS 402 Guide Object 0x6040
         */
        void sendEnableOperation();

        /**
         * Sending fault reset command, after the reset was send ensure that, the Fault Reset Bit is cleared.
         * Otherwise the next error may not be detected. For this you can send any other command.
         * @see DS 402 Guide Object 0x6040
         * @see armarx::performTransitionTwo()
         */
        void sendFaultReset();

        /**
         * Stops the motor, by only setting the halt bit.
         * When this method is called it assumes that that the output mapping is update so it can use the correct StatusWord.
         * @see DS 402 Guide Object 0x6040
         * @see DS 402 Guide Object 0x6041
         */
        void sendHalt();

        /**
         * Performs the DS 402 StateTransition 2
         * @see S 402 Guide Object 0x6040
         */
        void performTransitionTwo();

        /**
         * Performs the DS 402 StateTransition 3
         * @see S 402 Guide Object 0x6040
         */
        void performTransitionThreeStar();

        /**
         * Performs the DS 402 StateTransition 4
         * @see S 402 Guide Object 0x6040
         */
        void performTransitionFour();

        /**
         * Performs the DS 402 StateTransition 5
         * @see S 402 Guide Object 0x6040
         */
        void performTransitionFive();

        /**
         * Performs the DS 402 StateTransition 6
         * @see S 402 Guide Object 0x6040
         */
        void performTransitionSix();

        /**
         * Performs the DS 402 StateTransition 7
         * @see S 402 Guide Object 0x6040
         */
        void performTransitionSeven();

        /**
         * Performs the DS 402 StateTransition 8
         * @see S 402 Guide Object 0x6040
         */
        void performTransitionEight();

        /**
         * Performs the DS 402 StateTransition 9
         * @see S 402 Guide Object 0x6040
         */
        void performTransitionNine();

        /**
         * Performs the DS 402 StateTransition 10
         * @see S 402 Guide Object 0x6040
         */
        void performTransitionTen();

        /**
         * Performs the DS 402 StateTransition 11
         * @see S 402 Guide Object 0x6040
         */
        void performTransitionEleven();

        /**
         * Performs the DS 402 StateTransition 12
         * @see S 402 Guide Object 0x6040
         */
        void performTransitionTwelve();

        /**
         * Performs the DS 402 StateTransition 15
         * After this transition the the next transitions should be done quite fast to ensure the rest Bit is set to zero again.
         * @see S 402 Guide Object 0x6040
         */
        void performTransitionFivteen();

        /**
         * Performs the DS 402 StateTransition 16
         * @see S 402 Guide Object 0x6040
         */
        void performTransitionSixteen();

        /**
         * This sends the given ControlWord to the elmo. I
         * PDO's will used to be sure PDO update will be done after calling this.
         * Attention it will not check if the elmo performed the command for this check the StatusWord by yourself.
         * @param controlWord the ControlWord to send
         * @param forceSDOMode if true, SDO access is used to send this controlWord, regardless whether PDO communication
         * is working
         * @return true
         */
        bool sendControlWord(ControlWord controlWord, bool forceSDOMode = false);


        /**
         * This checks whether there have been any errors reported by the elmo.
         * It will first check the register at 0x1003:0 and if that register reports one or more
         * errors will read out the registers 0x6007:0 and 0x603F:0 and print appropriate messages
         * @param clearErrorCount whether to clear the error count register
         */
        void checkForElmoErrors(bool clearErrorCount = false);

        /** the last operation mode that was set */
        std::int8_t lastModeSet;

        bool quickStopActive;

        bool STO_On_State = false;

        ///the configuration parameters for the elmo
        std::uint32_t ratedTorque;
        std::uint32_t ratedCurrent;
        std::uint16_t maxRatedCurrent = 50000; // 50000 is elmo default
    };

} // namespace devices::ethercat::common::elmo::gold
