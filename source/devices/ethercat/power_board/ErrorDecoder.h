/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Jan Hausberg (jan dot hausberg at kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <devices/ethercat/power_board/SlaveIO.h>

namespace armarx::control::ethercat
{
    class SlaveInterface;
}

namespace devices::ethercat::power_board
{

    enum FailStatus1 : std::uint8_t
    {
        // clang-format off
        overCurrentDischarge65A1 = 0b00000001,
        overCurrentDischarge90A =  0b00000010,
        overChargeProtection =     0b00000100,
        overCurrentCharge45A =     0b00001000,
        dischargeTempError =       0b00010000,
        lowVoltageFS1 =            0b00100000,
        fullyChargeFS1 =           0b01000000,
        overCurrentDischarge200A = 0b10000000
        // clang-format on
    };

    enum FailStatus2 : std::uint8_t
    {
        // clang-format off
        overCurrentDischarge110A = 0b00000001,
        overCurrentCharge65A =     0b00000010,
        chargeTempError =          0b00000100,
        cellUnbalance =            0b00001000,
        overCharge =               0b00010000,
        deepDischarge =            0b00100000,
        fuseBlown =                0b01000000,
        fetUnControl =             0b10000000
        // clang-format on
    };

    enum FailStatus3 : std::uint8_t
    {
        // clang-format off
        selfTestClockFail =         0b00000001,
        selfTestRomFail =           0b00000010,
        selfTestRegisterFail =      0b00000100,
        selfTestPswRegisterFail =   0b00001000,
        selfTestStackRegisterFail = 0b00010000,
        selfTestCsRegisterFail =    0b00100000,
        selfTestEsRegisterFail =    0b01000000,
        selfTestRamFail =           0b10000000
        // clang-format on
    };

    enum LeaderBatteryStatus : std::uint8_t
    {
        active = 0x01,
        fullyChargeLSB = 0x03,
        lowVoltageLSB = 0x06,
        bmError = 0x09,
        protect = 0x10,
        permanentFailure = 0x20
    };

    enum LeaderAlarm1 : std::uint8_t
    {
        // clang-format off
        idSettingError =       0b00000001,
        bmCommunicationError = 0b00000010
        // clang-format on
    };

    enum LeaderAlarm2 : std::uint8_t
    {
        // clang-format off
        cellVoltageMeasurementFail =  0b00000001,
        temperatureMeasurementFail =  0b00000010,
        selfTestDataFlashBlock2Fail = 0b00000100,
        selfTestDataFlashBlock3Fail = 0b00001000,
        // clang-format on
    };

    class ErrorDecoder
    {
    public:
        ErrorDecoder(SlaveOut* outputs, SlaveIn* inputs);

        bool batteryCommunicationErrorDetected() const;

        std::uint8_t getFailStatus1() const;

        bool failStatus1ErrorDetected(armarx::control::ethercat::SlaveInterface* slave,
                                      bool printError = false) const;

        std::uint8_t getFailStatus2() const;

        bool failStatus2ErrorDetected(armarx::control::ethercat::SlaveInterface* slave,
                                      bool printError = false) const;

        std::uint8_t getFailStatus3() const;

        bool failStatus3ErrorDetected(armarx::control::ethercat::SlaveInterface* slave,
                                      bool printError = false) const;

        std::uint8_t getLeaderBatteryStatus() const;

        bool leaderBatteryStatusErrorDetected(armarx::control::ethercat::SlaveInterface* slave,
                                              bool printError = false) const;

        std::uint8_t getLeaderBMAlarm1() const;

        bool leaderBMAlarm1ErrorDetected(armarx::control::ethercat::SlaveInterface* slave,
                                         bool printError = false) const;

        std::uint8_t getLeaderBMAlarm2() const;

        bool leaderBMAlarm2ErrorDetected(armarx::control::ethercat::SlaveInterface* slave,
                                         bool printError = false) const;

    private:
        SlaveOut* outputs;
        SlaveIn* inputs;
    };
} // namespace devices::ethercat::power_board
