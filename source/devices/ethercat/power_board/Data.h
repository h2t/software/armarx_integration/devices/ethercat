/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Jan Hausberg (jan dot hausberg at kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValueIMU.h>
#include <armarx/control/ethercat/DataInterface.h>

#include <devices/ethercat/power_board/ErrorDecoder.h>
#include <devices/ethercat/power_board/SlaveIO.h>
#include <devices/ethercat/power_board/Config.h>

namespace devices::ethercat::power_board
{
    /**
     * @brief This class contains all sensor data, that should be given to armarx.
     * If, for example, some data should be displayed in the gui, then it is necessary that it
     * is included in this class.
     */
    class PowerBoardSensorValue : virtual public armarx::SensorValueBase
    /*
     * Inherit from each class in RobotAPI/components/units/RobotUnit/SensorValues that makes sense.
     * E.g. for a sensor actor unit armarx::SensorValue1DoFRealActuatorWithStatus
     * In that case there are already attributes defined. Do not forget to update them as well!
     */
    {
    public:
        PowerBoardSensorValue() = default;

        PowerBoardSensorValue(const PowerBoardSensorValue&) = default;

        PowerBoardSensorValue& operator=(const PowerBoardSensorValue&) = default;

        PowerBoardSensorValue(PowerBoardSensorValue&& o)
        {
            *this = o;
        }

        PowerBoardSensorValue&
        operator=(PowerBoardSensorValue&& o)
        {
            *this = o;
            return *this;
        }

        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION

            float voltage_battery;
        float voltage_sensor1;
        float voltage_sensor2;

        float current_battery;
        float current_sensor1;
        float current_sensor2;

        float temperature_batteryMax;
        float temperature_batteryMin;
        float temperature_batteryFETMax;
        float temperature_sensor1;
        float temperature_sensor2;

        float remainingCapacity;
        float fullChargeCapacity;

        std::uint8_t stateOfCharge;
        std::uint8_t numberConnectedBMs;
        std::uint16_t mainUpdateRate;
        bool batteryCommunicationError;
        bool batteryIsCharging;
        bool batteryIsLow;
        bool batteryHasError;


        static SensorValueInfo<PowerBoardSensorValue>
        GetClassMemberInfo()
        {
            SensorValueInfo<PowerBoardSensorValue> svi;

            svi.addMemberVariable(&PowerBoardSensorValue::voltage_battery, "voltage_battery");
            svi.addMemberVariable(&PowerBoardSensorValue::voltage_sensor1, "voltage_sensor1");
            svi.addMemberVariable(&PowerBoardSensorValue::voltage_sensor2, "voltage_sensor2");

            svi.addMemberVariable(&PowerBoardSensorValue::current_battery, "current_battery");
            svi.addMemberVariable(&PowerBoardSensorValue::current_sensor1, "current_sensor1");
            svi.addMemberVariable(&PowerBoardSensorValue::current_sensor2, "current_sensor2");

            svi.addMemberVariable(&PowerBoardSensorValue::temperature_batteryMax,
                                  "temperature_batteryMax");
            svi.addMemberVariable(&PowerBoardSensorValue::temperature_batteryMin,
                                  "temperature_batteryMin");
            svi.addMemberVariable(&PowerBoardSensorValue::temperature_batteryFETMax,
                                  "temperature_batteryFETMax");
            svi.addMemberVariable(&PowerBoardSensorValue::temperature_sensor1,
                                  "temperature_sensor1");
            svi.addMemberVariable(&PowerBoardSensorValue::temperature_sensor2,
                                  "temperature_sensor2");

            svi.addMemberVariable(&PowerBoardSensorValue::remainingCapacity, "remainingCapacity");
            svi.addMemberVariable(&PowerBoardSensorValue::fullChargeCapacity, "fullChargeCapacity");

            svi.addMemberVariable(&PowerBoardSensorValue::stateOfCharge, "stateOfCharge");
            svi.addMemberVariable(&PowerBoardSensorValue::numberConnectedBMs, "numberConnectedBMs");
            svi.addMemberVariable(&PowerBoardSensorValue::mainUpdateRate, "mainUpdateRate");
            svi.addMemberVariable(&PowerBoardSensorValue::batteryCommunicationError,
                                  "batteryCommunicationError");
            svi.addMemberVariable(&PowerBoardSensorValue::batteryIsCharging, "batteryIsCharging");
            svi.addMemberVariable(&PowerBoardSensorValue::batteryIsLow, "batteryIsLow");
            svi.addMemberVariable(&PowerBoardSensorValue::batteryHasError, "batteryHasError");

            return svi;
        }
    };


    class Data : public armarx::control::ethercat::DataInterface
    {
    public:
        Data(const DataConfig& config,
             SlaveOut* outputs,
             SlaveIn* inputs);

        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration) override;
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                 const IceUtil::Time& timeSinceLastIteration) override;

        bool getBatteryCommunicationError();

        std::uint8_t getBatteryStatus();

        /**
         * @brief Describes the frequency of the main loop on the sensor-actor-pcb in Hz.
         * This frequency does not necessarily correspond to the frequency of other sensor values.
         */
        std::uint16_t getMainUpdateRate();

        float getTemperature1Converted();

        float getTemperature2Converted();

        std::uint16_t getCurrentMonitor1();

        float getCurrent1();

        std::uint16_t getCurrentMonitor2();

        float getCurrent2();

        std::uint16_t getVoltageMonitor1();

        float getVoltage1();

        std::uint16_t getVoltageMonitor2();

        float getVoltage2();

        std::uint8_t getNumberConnectedBm();

        std::uint8_t getAbsoluteStateOfChargeMinInPct();

        std::uint8_t getRelativeStateOfChargeMinInPct();

        std::uint8_t getStateOfHealthMinInPct();

        float getFetTemperatureMaxInDegC();

        float getAverageCurrentInA();

        float getChargingCurrentInA();

        float getVoltageMaxInV();

        std::uint8_t getFailStatus1();

        std::uint8_t getFailStatus2();

        std::uint8_t getFailStatus3();

        std::uint8_t getLeaderBatteryStatus();

        float getDesignCapacity();

        float getFullChargeCapacity();

        float getRemainingCapacity();

        std::uint8_t getLeaderBmAlarm1();

        std::uint8_t getLeaderBmAlarm2();

        float getBmVoltageMax();

        float getBmVoltageMin();

        float getBmTemperatureMax();

        float getBmTemperatureMin();

        std::uint8_t getBmVoltageMaxId();

        std::uint8_t getBmVoltageMinId();

        std::uint8_t getBmTemperatureMaxId();

        std::uint8_t getBmTemperatureMinId();

        float getBmCurrentMax();

        float getBmCurrentMin();

        float getCellVoltageMax();

        float getCellVoltageMin();

        std::uint8_t getBmCurrentMaxId();

        std::uint8_t getBmCurrentMinId();

        std::uint8_t getCellVoltageMaxId();

        std::uint8_t getCellVoltageMinId();

        std::uint16_t* getLedRgbStrip1ControlPtr();

        std::uint16_t* getLedRgbStrip2ControlPtr();

    private:
        SlaveOut* outputs;
        SlaveIn* inputs;

        ErrorDecoder errorDecoder;

        // Normal Slave inputs

        bool batteryCommunicationError;
        std::uint8_t batteryStatus;
        std::uint16_t mainUpdateRate;
        float temperature1Converted;
        float temperature2Converted;

        armarx::control::ethercat::LinearConvertedValue<std::uint16_t> current1;
        armarx::control::ethercat::LinearConvertedValue<std::uint16_t> current2;
        armarx::control::ethercat::LinearConvertedValue<std::uint16_t> voltage1;
        armarx::control::ethercat::LinearConvertedValue<std::uint16_t> voltage2;

        armarx::control::ethercat::LinearConvertedValue<std::uint16_t> fetTemperatureMax;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> averageCurrent;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> chargingCurrent;
        armarx::control::ethercat::LinearConvertedValue<std::uint16_t> voltageMax;
        std::uint8_t failStatus1;
        std::uint8_t failStatus2;
        std::uint8_t failStatus3;
        std::uint8_t leaderBatteryStatus;
        armarx::control::ethercat::LinearConvertedValue<std::uint16_t> designCapacity;
        armarx::control::ethercat::LinearConvertedValue<std::uint16_t> fullChargeCapacity;
        armarx::control::ethercat::LinearConvertedValue<std::uint16_t> remainingCapacity;
        std::uint8_t leaderBmAlarm1;
        std::uint8_t leaderBmAlarm2;
        armarx::control::ethercat::LinearConvertedValue<std::uint16_t> bmVoltageMax;
        armarx::control::ethercat::LinearConvertedValue<std::uint16_t> bmVoltageMin;
        armarx::control::ethercat::LinearConvertedValue<std::uint16_t> bmTemperatureMax;
        armarx::control::ethercat::LinearConvertedValue<std::uint16_t> bmTemperatureMin;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> bmCurrentMax;
        armarx::control::ethercat::LinearConvertedValue<std::int16_t> bmCurrentMin;
        armarx::control::ethercat::LinearConvertedValue<std::uint16_t> cellVoltageMax;
        armarx::control::ethercat::LinearConvertedValue<std::uint16_t> cellVoltageMin;

        // Conversion parameters
        float ntcBeta;

        float convertTemperature(std::uint16_t temperature);
    };

    using DataPtr = std::shared_ptr<Data>;
} // namespace devices::ethercat::power_board
