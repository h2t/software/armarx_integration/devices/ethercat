/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Armar6RT::ArmarXObjects::Armar6IceKinematicUnit
 * @author     Markus Swarowsky ( markus dot swarowsky at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <VirtualRobot/Nodes/RobotNode.h>

#include <RobotAPI/components/units/RobotUnit/BasicControllers.h>
#include <RobotAPI/components/units/RobotUnit/Devices/ControlDevice.h>
#include <RobotAPI/components/units/RobotUnit/Devices/SensorDevice.h>
#include <armarx/control/ethercat/DeviceInterface.h>
#include <armarx/control/ethercat/SlaveIdentifier.h>

#include <devices/ethercat/power_board/Data.h>
#include <devices/ethercat/power_board/LedControlData.h>
#include <devices/ethercat/power_board/Slave.h>
#include <devices/ethercat/power_board/Config.h>


namespace devices::ethercat::power_board
{

    namespace joint_controller
    {
        using LedStopMovementControllerPtr = std::shared_ptr<class LedStopMovementController>;
        using LedEmergencyStopControllerPtr = std::shared_ptr<class LedEmergencyStopController>;
        using LedColorControllerPtr = std::shared_ptr<class LedColorController>;
    } // namespace joint_controller


    class Device : public armarx::SensorDevice, public armarx::control::ethercat::DeviceInterface
    {

    public:
        class LedDevice :
            public armarx::ControlDevice,
            public armarx::control::ethercat::DeviceInterface::SubDeviceInterface
        {
            friend class Device;

        public:
            LedDevice(const std::string& deviceName);

            void init(Device* device, LedControlData* ledControlDataPtr);

        protected:
            Device* device;
            LedControlData* ledControlDataPtr;
            joint_controller::LedEmergencyStopControllerPtr emergencyController;
            joint_controller::LedStopMovementControllerPtr stopMovementController;
            joint_controller::LedColorControllerPtr colorController;

            // ControlDevice interface
            void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                     const IceUtil::Time& timeSinceLastIteration) override;
        };

        using LedDevicePtr = std::shared_ptr<LedDevice>;

        Device(armarx::control::hardware_config::DeviceConfig& hwConfig,
               VirtualRobot::RobotPtr const& robot);
        ~Device() override
        {
        }

        DeviceInterface::TryAssignResult
        tryAssign(armarx::control::ethercat::SlaveInterface& slave) override;

        DeviceInterface::AllAssignedResult onAllAssigned() override;

        std::string getClassName() const override;

        void postSwitchToSafeOp() override;

        void postSwitchToOp() override;

        // SensorDevice interface
        const armarx::SensorValueBase* getSensorValue() const override;
        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration) override;

        std::shared_ptr<Data> getData() const;

        const std::string& getJointName() const;

        const LedDevicePtr& getLedStrip1Device() const;

        const LedDevicePtr& getLedStrip2Device() const;

    private:
        void updateSensorValueStruct();

        ///The data and target object
        std::shared_ptr<Data> dataPtr;
        /// The data object for copying to non-rt part
        PowerBoardSensorValue sensorValue;

        Slave* slavePtr;

        const armarx::control::ethercat::SlaveIdentifier slaveIdentifier;

        std::unique_ptr<LedControlData> ledStrip1ControlDataPtr;
        std::unique_ptr<LedControlData> ledStrip2ControlDataPtr;
        LedDevicePtr ledStrip1Device;
        LedDevicePtr ledStrip2Device;

        SlaveConfig slaveConf;
        DeviceConfig deviceConf;
        DataConfig dataConf;
    };

} // namespace devices::ethercat::power_board
