#include "LedEmergencyStop.h"

namespace devices::ethercat::power_board::joint_controller
{
    LedEmergencyStopController::LedEmergencyStopController()
    {}
    void LedEmergencyStopController::rtRun(
        const IceUtil::Time&, const IceUtil::Time&)
    {
    }

    armarx::ControlTargetBase* LedEmergencyStopController::getControlTarget()
    {
        return &target;
    }

    void LedEmergencyStopController::rtPreActivateController()
    {
    }
} // namespace devices::ethercat::power_board::joint_controller
