#pragma once

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>

#include "../ControlTargets.h"
#include "../LedControlData.h"

namespace devices::ethercat::power_board::joint_controller
{
    class LedColorController : public armarx::JointController
    {
    public:
        LedColorController(LedControlData* data);
        armarx::ControlTargetBase* getControlTarget() override;
        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;

    private:
        LedControlData* data;
        ControlTargetPowerBoardLedColor target;
    };
    using LedColorControllerPtr = std::shared_ptr<LedColorController>;
} // namespace devices::ethercat::power_board::joint_controller
