#pragma once

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>

namespace devices::ethercat::power_board::joint_controller
{
    class LedEmergencyStopController : public armarx::JointController
    {
    public:
        LedEmergencyStopController();
    private:
        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;
        armarx::ControlTargetBase* getControlTarget() override;

        void rtPreActivateController() override;
    private:
        armarx::DummyControlTargetEmergencyStop target;
    };
    
    using LedEmergencyStopControllerPtr =
        std::shared_ptr<LedEmergencyStopController>;

} // namespace devices::ethercat::power_board::joint_controller
