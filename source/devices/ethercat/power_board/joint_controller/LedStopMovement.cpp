#include "LedStopMovement.h"

namespace devices::ethercat::power_board::joint_controller
{
    LedStopMovementController::LedStopMovementController()
    {}

    void LedStopMovementController::rtRun(
        const IceUtil::Time&, const IceUtil::Time&)
    {
    }

    armarx::ControlTargetBase* LedStopMovementController::getControlTarget()
    {
        return &target;
    }

    void LedStopMovementController::rtPreActivateController()
    {
    }
} // namespace devices::ethercat::power_board::joint_controller
