#include "LedColor.h"

namespace devices::ethercat::power_board::joint_controller
{
    LedColorController::LedColorController(LedControlData* data) : data(data)
    {
        ARMARX_CHECK_NOT_NULL(data);
    }

    armarx::ControlTargetBase*
    LedColorController::getControlTarget()
    {
        return &target;
    }

    void
    LedColorController::rtRun(const IceUtil::Time&, const IceUtil::Time&)
    {
        std::array<std::uint16_t, 40> ledRgbStripControl;
        for (size_t i = 0; i < ledRgbStripControl.size(); ++i)
        {
            ledRgbStripControl[i] = static_cast<std::uint16_t>(
                (static_cast<std::uint16_t>(target.colors[i].r & 0b11111000) << 8) |
                (static_cast<std::uint16_t>(target.colors[i].g & 0b11111100) << 3) |
                (static_cast<std::uint16_t>(target.colors[i].b) >> 3));
        }
        data->setLedRgbStripControl(ledRgbStripControl);
    }
} // namespace devices::ethercat::power_board::joint_controller
