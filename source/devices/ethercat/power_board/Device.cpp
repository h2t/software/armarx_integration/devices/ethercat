/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Armar6RT::ArmarXObjects::Armar6IceKinematicUnit
 * @author     Markus Swarowsky ( markus dot swarowsky at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Device.h"

#include <VirtualRobot/Robot.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/util/StringHelperTemplates.h>

#include "joint_controller/LedColor.h"
#include "joint_controller/LedEmergencyStop.h"
#include "joint_controller/LedStopMovement.h"
#include <devices/ethercat/power_board/Slave.h>


namespace devices::ethercat::power_board
{

    Device::Device(armarx::control::hardware_config::DeviceConfig& hwConfig,
                   const VirtualRobot::RobotPtr& /*robot*/) :
        DeviceBase(hwConfig.getName()),
        SensorDevice(hwConfig.getName()),
        DeviceInterface(hwConfig.getName()),
        dataPtr(nullptr),
        slavePtr(nullptr),
        slaveIdentifier(hwConfig.getOnlySlaveConfig().getIdentifier()),
        ledStrip1ControlDataPtr(nullptr),
        ledStrip2ControlDataPtr(nullptr),
        ledStrip1Device(nullptr),
        ledStrip2Device(nullptr),
        slaveConf(hwConfig.getOnlySlaveConfig()),
        deviceConf(hwConfig),
        dataConf(hwConfig.getOnlySlaveConfig())
    {
    }


    armarx::control::ethercat::DeviceInterface::TryAssignResult
    Device::tryAssign(armarx::control::ethercat::SlaveInterface& slave)
    {
        namespace ecat = armarx::control::ethercat;
        using Result = DeviceInterface::TryAssignResult;

        Result result = ecat::tryAssignUsingSerialNo(slave, slavePtr, slaveIdentifier.serialNumber);

        return result;
    }


    armarx::control::ethercat::DeviceInterface::AllAssignedResult
    Device::onAllAssigned()
    {
        using Result = DeviceInterface::AllAssignedResult;

        if (slavePtr)
        {
            // Initialize power board.
            {
                slavePtr->setConfig(slaveConf);
            }

            ledStrip1Device.reset(new LedDevice(getDeviceName() + "LedStrip1"));
            // ARMARX_INFO << VAROUT(ledStrip1Device->getDeviceName());
            ledStrip2Device.reset(new LedDevice(getDeviceName() + "LedStrip2"));
            subDevices.push_back(ledStrip1Device);
            subDevices.push_back(ledStrip2Device);

            return Result::ok;
        }

        return Result::slavesMissing;
    }

    std::string
    Device::getClassName() const
    {
        return "PowerBoard";
    }

    void
    Device::postSwitchToSafeOp()
    {
        if (!dataPtr)
        {
            dataPtr = std::make_shared<Data>(
                dataConf, slavePtr->getOutputsPtr(), slavePtr->getInputsPtr());

            ledStrip1ControlDataPtr =
                std::make_unique<LedControlData>(dataPtr->getLedRgbStrip1ControlPtr());
            ledStrip2ControlDataPtr =
                std::make_unique<LedControlData>(dataPtr->getLedRgbStrip2ControlPtr());
            ledStrip1Device->init(this, ledStrip1ControlDataPtr.get());
            ledStrip2Device->init(this, ledStrip2ControlDataPtr.get());
        }
        ARMARX_CHECK_NOT_NULL(dataPtr);
        ARMARX_CHECK_NOT_NULL(ledStrip1ControlDataPtr);
        ARMARX_CHECK_NOT_NULL(ledStrip2ControlDataPtr);
    }

    void
    Device::postSwitchToOp()
    {
    }


    void
    Device::updateSensorValueStruct()
    {
        sensorValue.voltage_battery =
            static_cast<float>(deviceConf.batteryModulesInSerial) *
            ((dataPtr->getBmVoltageMax() + dataPtr->getBmVoltageMin()) / 2.f);
        sensorValue.voltage_sensor1 = dataPtr->getVoltage1();
        sensorValue.voltage_sensor2 = dataPtr->getVoltage2();

        sensorValue.current_battery =
            static_cast<float>(deviceConf.batteryModulesInParallel) *
            ((dataPtr->getBmCurrentMax() + dataPtr->getBmCurrentMin()) / 2.f);
        sensorValue.current_sensor1 = dataPtr->getCurrent1();
        sensorValue.current_sensor2 = dataPtr->getCurrent2();

        sensorValue.temperature_batteryMax = dataPtr->getBmTemperatureMax();
        sensorValue.temperature_batteryMin = dataPtr->getBmTemperatureMin();
        sensorValue.temperature_batteryFETMax = dataPtr->getFetTemperatureMaxInDegC();
        sensorValue.temperature_sensor1 = dataPtr->getTemperature1Converted();
        sensorValue.temperature_sensor2 = dataPtr->getTemperature2Converted();

        sensorValue.remainingCapacity = dataPtr->getRemainingCapacity() *
                                        static_cast<float>(deviceConf.batteryModulesInSerial) *
                                        static_cast<float>(deviceConf.batteryModulesInParallel);
        sensorValue.fullChargeCapacity = dataPtr->getFullChargeCapacity() *
                                         static_cast<float>(deviceConf.batteryModulesInSerial) *
                                         static_cast<float>(deviceConf.batteryModulesInParallel);

        sensorValue.stateOfCharge = dataPtr->getAbsoluteStateOfChargeMinInPct();
        sensorValue.numberConnectedBMs = dataPtr->getNumberConnectedBm();
        sensorValue.mainUpdateRate = dataPtr->getMainUpdateRate();
        sensorValue.batteryCommunicationError = dataPtr->getBatteryCommunicationError();
        sensorValue.batteryIsCharging = dataPtr->getBatteryStatus() == 2;
        sensorValue.batteryIsLow = dataPtr->getBatteryStatus() == 1;
        sensorValue.batteryHasError = dataPtr->getBatteryStatus() == 3;

        //        // Board Diagnostics
        //        sensorValue.batteryCommunicationError = dataPtr->getBatteryCommunicationError();
        //        sensorValue.batteryStatus = dataPtr->getBatteryStatus();
        //        sensorValue.mainUpdateRate = dataPtr->getMainUpdateRate();
        //        sensorValue.temperature1Converted = dataPtr->getTemperature1Converted();
        //        sensorValue.temperature2Converted = dataPtr->getTemperature2Converted();

        //        // Power Sensing
        //        sensorValue.currentMonitor1 = dataPtr->getCurrentMonitor1();
        //        sensorValue.current1 = dataPtr->getCurrent1();
        //        sensorValue.currentMonitor2 = dataPtr->getCurrentMonitor2();
        //        sensorValue.current2 = dataPtr->getCurrent2();
        //        sensorValue.voltageMonitor1 = dataPtr->getVoltageMonitor1();
        //        sensorValue.voltage1 = dataPtr->getVoltage1();
        //        sensorValue.voltageMonitor2 = dataPtr->getVoltageMonitor2();
        //        sensorValue.voltage2 = dataPtr->getVoltage2();

        //        // Battery Management Info
        //        sensorValue.numberConnectedBm = dataPtr->getNumberConnectedBm();
        //        sensorValue.absoluteStateOfChargeMin = dataPtr->getAbsoluteStateOfChargeMinInPct();
        //        sensorValue.relativeStateOfChargeMin = dataPtr->getRelativeStateOfChargeMinInPct();
        //        sensorValue.stateOfHealthMin = dataPtr->getStateOfHealthMinInPct();
        //        sensorValue.fetTemperatureMax = dataPtr->getFetTemperatureMaxInDegC();
        //        sensorValue.averageCurrent = dataPtr->getAverageCurrentInA();
        //        sensorValue.chargingCurrent = dataPtr->getChargingCurrentInA();
        //        sensorValue.voltageMax = dataPtr->getVoltageMaxInV();
        //        sensorValue.failStatus1 = dataPtr->getFailStatus1();
        //        sensorValue.failStatus2 = dataPtr->getFailStatus2();
        //        sensorValue.failStatus3 = dataPtr->getFailStatus3();
        //        sensorValue.leaderBatteryStatus = dataPtr->getLeaderBatteryStatus();
        //        sensorValue.designCapacity = dataPtr->getDesignCapacity();
        //        sensorValue.fullChargeCapacity = dataPtr->getFullChargeCapacity();
        //        sensorValue.remainingCapacity = dataPtr->getRemainingCapacity();
        //        sensorValue.leaderBmAlarm1 = dataPtr->getLeaderBmAlarm1();
        //        sensorValue.leaderBmAlarm2 = dataPtr->getLeaderBmAlarm2();
        //        sensorValue.bmVoltageMax = dataPtr->getBmVoltageMax();
        //        sensorValue.bmVoltageMin = dataPtr->getBmVoltageMin();
        //        sensorValue.bmTemperatureMax = dataPtr->getBmTemperatureMax();
        //        sensorValue.bmTemperatureMin = dataPtr->getBmTemperatureMin();
        //        sensorValue.bmVoltageMaxId = dataPtr->getBmTemperatureMaxId();
        //        sensorValue.bmVoltageMinId = dataPtr->getBmVoltageMinId();
        //        sensorValue.bmTemperatureMaxId = dataPtr->getBmTemperatureMaxId();
        //        sensorValue.bmTemperatureMinId = dataPtr->getBmTemperatureMinId();
        //        sensorValue.bmCurrentMax = dataPtr->getBmCurrentMax();
        //        sensorValue.bmCurrentMin = dataPtr->getBmCurrentMin();
        //        sensorValue.cellVoltageMax = dataPtr->getCellVoltageMax();
        //        sensorValue.cellVoltageMin = dataPtr->getCellVoltageMin();
        //        sensorValue.bmCurrentMaxId = dataPtr->getBmCurrentMaxId();
        //        sensorValue.bmCurrentMinId = dataPtr->getBmCurrentMinId();
        //        sensorValue.cellVoltageMaxId = dataPtr->getCellVoltageMaxId();
        //        sensorValue.cellVoltageMinId = dataPtr->getCellVoltageMinId();
    }


    std::shared_ptr<Data>
    Device::getData() const
    {
        if (dataPtr == nullptr)
        {
            throw armarx::LocalException("Joint has no data set, call initData(...) before");
        }

        return dataPtr;
    }


    const std::string&
    Device::getJointName() const
    {
        return getDeviceName();
    }


    const Device::LedDevicePtr&
    Device::getLedStrip1Device() const
    {
        return ledStrip1Device;
    }


    const Device::LedDevicePtr&
    Device::getLedStrip2Device() const
    {
        return ledStrip2Device;
    }


    void
    Device::rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                               const IceUtil::Time& timeSinceLastIteration)
    {
        if (dataPtr == nullptr)
        {
            throw armarx::LocalException("Joint has no data set, call initData(...) before");
        }
        dataPtr->rtReadSensorValues(sensorValuesTimestamp, timeSinceLastIteration);

        float maxTemperature = 70;

        if (dataPtr->getTemperature1Converted() > maxTemperature)
        {
            DEVICE_WARNING(rtGetDeviceName(),
                           "Temperature 1 is high! %f°C",
                           dataPtr->getTemperature1Converted())
                .deactivateSpam(10);
        }

        if (dataPtr->getTemperature2Converted() > maxTemperature)
        {
            DEVICE_WARNING(rtGetDeviceName(),
                           "Temperature 2 is high! %f°C",
                           dataPtr->getTemperature1Converted())
                .deactivateSpam(10);
        }

        if (dataPtr->getFetTemperatureMaxInDegC() > maxTemperature)
        {

            DEVICE_WARNING(rtGetDeviceName(),
                           "FET Temperature Max is high! %f°C",
                           dataPtr->getTemperature1Converted())
                .deactivateSpam(10);
        }

        if (dataPtr->getBmTemperatureMax() > maxTemperature)
        {
            DEVICE_WARNING(rtGetDeviceName(),
                           "BM Max Temperature is high! %f°C (BM #%u)",
                           dataPtr->getBmTemperatureMax(),
                           dataPtr->getBmTemperatureMaxId())
                .deactivateSpam(10);
        }

        if (dataPtr->getBmTemperatureMin() > maxTemperature)
        {
            DEVICE_WARNING(rtGetDeviceName(),
                           "BM Min Temperature is high! %f°C (BM #%u)",
                           dataPtr->getBmTemperatureMin(),
                           dataPtr->getBmTemperatureMinId())
                .deactivateSpam(10);
        }

        updateSensorValueStruct();
    }


    const armarx::SensorValueBase*
    Device::getSensorValue() const
    {
        return &sensorValue;
    }


    Device::LedDevice::LedDevice(const std::string& deviceName) :
        DeviceBase(deviceName),
        ControlDevice(deviceName),
        DeviceInterface::SubDeviceInterface(deviceName)
    {
    }

    void
    Device::LedDevice::init(Device* device, LedControlData* ledControlDataPtr)
    {
        this->device = device;
        this->ledControlDataPtr = ledControlDataPtr;

        emergencyController.reset(new joint_controller::LedEmergencyStopController());
        stopMovementController.reset(new joint_controller::LedStopMovementController());
        colorController.reset(new joint_controller::LedColorController(ledControlDataPtr));

        addJointController(emergencyController.get());
        addJointController(stopMovementController.get());
        addJointController(colorController.get());
    }

    void
    Device::LedDevice::rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                           const IceUtil::Time& timeSinceLastIteration)
    {
        if (ledControlDataPtr == nullptr)
        {
            throw armarx::LocalException("LED has no data set, call initData(...) before");
        }

        ledControlDataPtr->rtWriteTargetValues(sensorValuesTimestamp, timeSinceLastIteration);
    }

} // namespace devices::ethercat::power_board
