#pragma once

#include <armarx/control/hardware_config/SlaveConfig.h>
#include <armarx/control/hardware_config/DeviceConfig.h>

namespace devices::ethercat::power_board
{
    struct SlaveConfig
    {
        SlaveConfig(armarx::control::hardware_config::SlaveConfig& hwConfig)
            : ledStrip1Count(hwConfig.getUint("ledRGBStrip1Count")),
              ledStrip2Count(hwConfig.getUint("ledRGBStrip2Count")),
              ledStrip1GlobalBrightness(hwConfig.getUint("ledRGBStrip1GlobalBrightness")),
              ledStrip2GlobalBrightness(hwConfig.getUint("ledRGBStrip2GlobalBrightness"))
        {
        }

        std::uint16_t ledStrip1Count;
        std::uint16_t ledStrip2Count;
        std::uint16_t ledStrip1GlobalBrightness;
        std::uint16_t ledStrip2GlobalBrightness;
    };

    struct DeviceConfig
    {
        DeviceConfig(armarx::control::hardware_config::DeviceConfig& hwConfig)
            : batteryModulesInParallel(hwConfig.getUint("batteryModulesInParallel")),
              batteryModulesInSerial(hwConfig.getUint("batteryModulesInSerial"))
        {
        }

        std::uint32_t batteryModulesInParallel;
        std::uint32_t batteryModulesInSerial;
    };

    struct DataConfig
    {
        DataConfig(armarx::control::hardware_config::Config& hwConfig)
            : current(hwConfig.getLinearConfig("current")),
              voltage(hwConfig.getLinearConfig("voltage")),
              batteryCurrent(hwConfig.getLinearConfig("batteryCurrent")),
              batteryVoltage(hwConfig.getLinearConfig("batteryVoltage")),
              batteryTemperature(hwConfig.getLinearConfig("batteryTemperature")),
              batteryCapacity(hwConfig.getLinearConfig("batteryCapacity")),
              ntcBeta(hwConfig.getFloat("ntcBeta"))
        {
        }

        armarx::control::hardware_config::types::LinearConfig current;
        armarx::control::hardware_config::types::LinearConfig voltage;
        armarx::control::hardware_config::types::LinearConfig batteryCurrent;
        armarx::control::hardware_config::types::LinearConfig batteryVoltage;
        armarx::control::hardware_config::types::LinearConfig batteryTemperature;
        armarx::control::hardware_config::types::LinearConfig batteryCapacity;
        float ntcBeta;
    };
}
