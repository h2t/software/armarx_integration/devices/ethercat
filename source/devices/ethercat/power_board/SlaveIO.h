/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Jan Hausberg (jan dot hausberg at kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <cstdint>


namespace devices::ethercat::power_board
{
    /*
     * Here belong the PDO mappings for ethercat.
     * The attribute packed is important.
     */

    /**
     * @brief PDO mapping sensorB->master
     */
    struct SlaveOut
    {
        // Object Entry 0x6000
        // Board Diagnostics
        std::uint16_t statusBits;
        std::uint16_t mainUpdateRate;
        std::uint16_t temperature1;
        std::uint16_t temperature2;

        // Object Entry 0x6001
        // Power Sensing
        std::uint16_t currentMonitor1;
        std::uint16_t currentMonitor2;
        std::uint16_t voltageMonitor1;
        std::uint16_t voltageMonitor2;

        // Object Entry 0x6002
        // Battery Management Info
        std::uint8_t numberConnectedBm;
        std::uint8_t absoluteStateOfChargeMin;
        std::uint8_t relativeStateOfChargeMin;
        std::uint8_t stateOfHealthMin;
        std::uint16_t fetTemperatureMax;
        std::int16_t averageCurrent;
        std::int16_t chargingCurrent;
        std::uint16_t voltageMax;
        std::uint8_t failStatus1;
        std::uint8_t failStatus2;
        std::uint8_t failStatus3;
        std::uint8_t leaderBatteryStatus;
        std::uint16_t designCapacity;
        std::uint16_t fullChargeCapacity;
        std::uint16_t remainingCapacity;
        std::uint8_t leaderBmAlarm1;
        std::uint8_t leaderBmAlarm2;
        std::uint16_t bmVoltageMax;
        std::uint16_t bmVoltageMin;
        std::uint16_t bmTemperatureMax;
        std::uint16_t bmTemperatureMin;
        std::uint8_t bmVoltageMaxId;
        std::uint8_t bmVoltageMinId;
        std::uint8_t bmTemperatureMaxId;
        std::uint8_t bmTemperatureMinId;
        std::int16_t bmCurrentMax;
        std::int16_t bmCurrentMin;
        std::uint16_t cellVoltageMax;
        std::uint16_t cellVoltageMin;
        std::uint8_t bmCurrentMaxId;
        std::uint8_t bmCurrentMinId;
        std::uint8_t cellVoltageMaxId;
        std::uint8_t cellVoltageMinId;

    } __attribute__((__packed__));


    /**
     * @brief PDO mapping master->sensorB
     */
    struct SlaveIn
    {
        // LED control
        // Object Entry 0x7000
        std::uint16_t ledRgbStrip1Control[40];
        // Object Entry 0x7001
        std::uint16_t ledRgbStrip2Control[40];
    } __attribute__((__packed__));

} // namespace devices::ethercat::power_board
