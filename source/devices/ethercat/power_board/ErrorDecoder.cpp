/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Jan Hausberg (jan dot hausberg at kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "ErrorDecoder.h"

#include <RobotAPI/components/units/RobotUnit/util/RtLogging.h>
#include <armarx/control/ethercat/SlaveInterface.h>

namespace devices::ethercat::power_board
{
    ErrorDecoder::ErrorDecoder(SlaveOut* outputs, SlaveIn* inputs) :
        outputs(outputs), inputs(inputs)
    {
    }

    bool
    ErrorDecoder::batteryCommunicationErrorDetected() const
    {
        return (0x08000 & outputs->statusBits) ? true : false;
    }

    std::uint8_t
    ErrorDecoder::getFailStatus1() const
    {
        return outputs->failStatus1;
    }

    bool
    ErrorDecoder::failStatus1ErrorDetected(armarx::control::ethercat::SlaveInterface* slave,
                                           bool printError) const
    {
        std::uint8_t errors = getFailStatus1();

        if (printError)
        {
            if (errors & FailStatus1::overCurrentDischarge65A1)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Battery - fail status 1: Over current (discharge) detection (65A) "
                            "(recoverable).")
                    .deactivateSpam(1);
            }
            if (errors & FailStatus1::overCurrentDischarge90A)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Battery - fail status 1: Over current (discharge) detection (90A) "
                            "(recoverable).")
                    .deactivateSpam(1);
            }
            if (errors & FailStatus1::overChargeProtection)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Battery - fail status 1: Over charge protection (non-recoverable).")
                    .deactivateSpam(1);
            }
            if (errors & FailStatus1::overCurrentCharge45A)
            {
                SLAVE_ERROR(
                    slave->getSlaveIdentifier(),
                    "Battery - fail status 1: Over current (charge) detection (45A) (recoverable).")
                    .deactivateSpam(1);
            }
            if (errors & FailStatus1::dischargeTempError)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Battery - fail status 1: Over temperature (discharge) detection "
                            "(recoverable).")
                    .deactivateSpam(1);
            }
            if (errors & FailStatus1::lowVoltageFS1)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Battery - fail status 1: Low voltage detection (recoverable).")
                    .deactivateSpam(1);
            }
            if (errors & FailStatus1::fullyChargeFS1)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Battery - fail status 1: Fully charge detection (recoverable).")
                    .deactivateSpam(1);
            }
            if (errors & FailStatus1::overCurrentDischarge200A)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Battery - fail status 1: Over current (discharge) detection (200A) "
                            "(recoverable).")
                    .deactivateSpam(1);
            }
        }

        // Return true if there are errors
        return (errors & 0b11111111) ? true : false;
    }

    std::uint8_t
    ErrorDecoder::getFailStatus2() const
    {
        return outputs->failStatus2;
    }

    bool
    ErrorDecoder::failStatus2ErrorDetected(armarx::control::ethercat::SlaveInterface* slave,
                                           bool printError) const
    {
        std::uint8_t errors = getFailStatus2();

        if (printError)
        {
            if (errors & FailStatus2::overCurrentDischarge110A)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Battery - fail status 2: Over current (discharge) detection (110A) "
                            "(recoverable).")
                    .deactivateSpam(1);
            }
            if (errors & FailStatus2::overCurrentCharge65A)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Battery - fail status 2: Over current (charge) detection (65A) "
                            "(recoverable).")
                    .deactivateSpam(1);
            }
            if (errors & FailStatus2::chargeTempError)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Battery - fail status 2: Over temperature (charge) detection "
                            "(recoverable).")
                    .deactivateSpam(1);
            }
            if (errors & FailStatus2::cellUnbalance)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Battery - fail status 2: Cell unbalance detection (200A) "
                            "(recoverable).")
                    .deactivateSpam(1);
            }
            if (errors & FailStatus2::overCharge)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Battery - fail status 2: Over charge (3.65V/cell) (recoverable).")
                    .deactivateSpam(1);
            }
            if (errors & FailStatus2::deepDischarge)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Battery - fail status 2: Deep discharge (non-recoverable).")
                    .deactivateSpam(1);
            }
            if (errors & FailStatus2::fuseBlown)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Battery - fail status 2: Fuse blown (non-recoverable).")
                    .deactivateSpam(1);
            }
            if (errors & FailStatus2::fetUnControl)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Battery - fail status 2: FET un-control (non-recoverable).")
                    .deactivateSpam(1);
            }
        }

        // Return true if there are errors
        return (errors & 0b11111111) ? true : false;
    }

    std::uint8_t
    ErrorDecoder::getFailStatus3() const
    {
        return outputs->failStatus3;
    }

    bool
    ErrorDecoder::failStatus3ErrorDetected(armarx::control::ethercat::SlaveInterface* slave,
                                           bool printError) const
    {
        std::uint8_t errors = getFailStatus3();

        if (printError)
        {
            if (errors & FailStatus3::selfTestClockFail)
            {
                SLAVE_ERROR(
                    slave->getSlaveIdentifier(),
                    "Battery - fail status 3: BM error - Self-Test clock Fail (recoverable).")
                    .deactivateSpam(1);
            }
            if (errors & FailStatus3::selfTestRomFail)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Battery - fail status 3: BM error - Self-Test ROM Fail (recoverable).")
                    .deactivateSpam(1);
            }
            if (errors & FailStatus3::selfTestRegisterFail)
            {
                SLAVE_ERROR(
                    slave->getSlaveIdentifier(),
                    "Battery - fail status 3: BM error - Self-Test register Fail (recoverable).")
                    .deactivateSpam(1);
            }
            if (errors & FailStatus3::selfTestPswRegisterFail)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Battery - fail status 3: BM error - Self-Test PSW register Fail "
                            "(recoverable).")
                    .deactivateSpam(1);
            }
            if (errors & FailStatus3::selfTestStackRegisterFail)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Battery - fail status 3: BM error - Self-Test stack register Fail "
                            "(recoverable).")
                    .deactivateSpam(1);
            }
            if (errors & FailStatus3::selfTestCsRegisterFail)
            {
                SLAVE_ERROR(
                    slave->getSlaveIdentifier(),
                    "Battery - fail status 3: BM error - Self-Test CS register Fail (recoverable).")
                    .deactivateSpam(1);
            }
            if (errors & FailStatus3::selfTestEsRegisterFail)
            {
                SLAVE_ERROR(
                    slave->getSlaveIdentifier(),
                    "Battery - fail status 3: BM error - Self-Test ES register Fail (recoverable).")
                    .deactivateSpam(1);
            }
            if (errors & FailStatus3::selfTestRamFail)
            {
                SLAVE_ERROR(
                    slave->getSlaveIdentifier(),
                    "Battery - fail status 3: BM error - Self-Test RAM Fail/DF fail (recoverable).")
                    .deactivateSpam(1);
            }
        }

        // Return true if there are errors
        return (errors & 0b11111111) ? true : false;
    }

    std::uint8_t
    ErrorDecoder::getLeaderBatteryStatus() const
    {
        return outputs->leaderBatteryStatus;
    }

    bool
    ErrorDecoder::leaderBatteryStatusErrorDetected(armarx::control::ethercat::SlaveInterface* slave,
                                                   bool printError) const
    {
        std::uint8_t status = getLeaderBatteryStatus();

        if (printError)
        {
            if (status == LeaderBatteryStatus::active)
            {
                ARMARX_RT_LOGF_VERBOSE("PowerBoard - leader battery status: Active")
                    .deactivateSpam(60);
            }
            if (status == LeaderBatteryStatus::fullyChargeLSB)
            {
                ARMARX_RT_LOGF_VERBOSE(
                    "PowerBoard - leader battery status: Fully charge (If any one "
                    "BM is in fully charge mode)")
                    .deactivateSpam(60);
            }
            if (status == LeaderBatteryStatus::lowVoltageLSB)
            {
                ARMARX_RT_LOGF_WARNING(
                    "PowerBoard - leader battery status: Low voltage (over discharge)")
                    .deactivateSpam(60);
            }
            if (status == LeaderBatteryStatus::bmError)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Battery - leader battery status: BM error: "
                            "transition condition: "
                            "Fail Status1(except bit6)≠0 or Fail Status2≠0 or "
                            "Fail Status3≠0 or BM communication error (max.90sec)")
                    .deactivateSpam(1);
            }
            if (status == LeaderBatteryStatus::protect)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(), "Battery - leader battery status: Protect")
                    .deactivateSpam(1);
            }
            if (status == LeaderBatteryStatus::permanentFailure)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Battery - leader battery status: Permanent Failure")
                    .deactivateSpam(1);
            }
        }

        // Return true if there are errors
        return (status == LeaderBatteryStatus::bmError || status == LeaderBatteryStatus::protect ||
                status == LeaderBatteryStatus::permanentFailure)
                   ? true
                   : false;
    }

    std::uint8_t
    ErrorDecoder::getLeaderBMAlarm1() const
    {
        return outputs->leaderBmAlarm1;
    }

    bool
    ErrorDecoder::leaderBMAlarm1ErrorDetected(armarx::control::ethercat::SlaveInterface* slave,
                                              bool printError) const
    {
        std::uint8_t errors = getLeaderBMAlarm1();

        if (printError)
        {
            if (errors & LeaderAlarm1::idSettingError)
            {
                SLAVE_ERROR(slave->getSlaveIdentifier(),
                            "Battery - leader BM Alarm 1: ID setting error. "
                            "The process of BM ID allocation did not finish successfully. "
                            "Leader BM might have failed to identify all of the connected "
                            "member BM correctly. "
                            "So, please open (set to High) Module_ON pin, then set Module "
                            "ON pin for Low again to reassign the ID.")
                    .deactivateSpam(1);
            }
            if (errors & LeaderAlarm1::bmCommunicationError)
            {
                SLAVE_ERROR(
                    slave->getSlaveIdentifier(),
                    "Battery - leader BM Alarm 1: BM communication error. "
                    "Communication error between BMs is detected. "
                    "After 30~90 seconds, transition to BM error and turn off the battery output. "
                    "So, please check the connection of Connector 1 and Connector 2 between each "
                    "BMs.")
                    .deactivateSpam(1);
            }
        }

        // Return true if there are errors
        return (errors & (LeaderAlarm1::idSettingError | LeaderAlarm1::bmCommunicationError))
                   ? true
                   : false;
    }

    std::uint8_t
    ErrorDecoder::getLeaderBMAlarm2() const
    {
        return outputs->leaderBmAlarm2;
    }

    bool
    ErrorDecoder::leaderBMAlarm2ErrorDetected(armarx::control::ethercat::SlaveInterface* slave,
                                              bool printError) const
    {
        std::uint8_t errors = getLeaderBMAlarm2();

        if (printError)
        {
            if (errors & LeaderAlarm2::cellVoltageMeasurementFail)
            {
                SLAVE_ERROR(
                    slave->getSlaveIdentifier(),
                    "Battery - leader BM Alarm 2: Cell voltage measurement Fail. "
                    "Failure of voltage measurement system is detected. "
                    "An error may have occurred in the microcomputer A/D converter or data flash. "
                    "If the alarm is continued, please consider to replace the BM.")
                    .deactivateSpam(1);
            }
            if (errors & LeaderAlarm2::temperatureMeasurementFail)
            {
                SLAVE_ERROR(
                    slave->getSlaveIdentifier(),
                    "Battery - leader BM Alarm 2: temperature measurement Fail. "
                    "Failure of temperature measurement system is detected. "
                    "An error may have occurred in the microcomputer A/D converter or data flash. "
                    "If the alarm is continued, please consider to replace the BM.")
                    .deactivateSpam(1);
            }
            if (errors & LeaderAlarm2::selfTestDataFlashBlock2Fail)
            {
                SLAVE_ERROR(
                    slave->getSlaveIdentifier(),
                    "Battery - leader BM Alarm 2: Self-Test Data Flash block2 Fail. "
                    "Checksum of Data Flash block2 is broken. "
                    "An error may have occurred in the microcomputer A/D converter or data flash. "
                    "If the alarm is continued, please consider to replace the BM.")
                    .deactivateSpam(1);
            }
            if (errors & LeaderAlarm2::selfTestDataFlashBlock3Fail)
            {
                SLAVE_ERROR(
                    slave->getSlaveIdentifier(),
                    "Battery - leader BM Alarm 2: Self-Test Data Flash block3 Fail. "
                    "Checksum of Data Flash block3 is broken. "
                    "An error may have occurred in the microcomputer A/D converter or data flash. "
                    "If the alarm is continued, please consider to replace the BM.")
                    .deactivateSpam(1);
            }
        }

        // Return true if there are errors
        return (errors & (LeaderAlarm2::cellVoltageMeasurementFail |
                          LeaderAlarm2::temperatureMeasurementFail |
                          LeaderAlarm2::selfTestDataFlashBlock2Fail |
                          LeaderAlarm2::selfTestDataFlashBlock3Fail))
                   ? true
                   : false;
    }

} // namespace devices::ethercat::power_board
