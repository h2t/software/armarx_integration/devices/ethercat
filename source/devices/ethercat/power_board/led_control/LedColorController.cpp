#include "LedColorController.h"

#include <fstream>
#include <iostream>

#include <SimoxUtility/color/interpolation.h>
#include <SimoxUtility/json.h>
#include <SimoxUtility/json/eigen_conversion.h>
#include <SimoxUtility/math/distance/angle_between.h>
#include <SimoxUtility/math/periodic/periodic_diff.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/exceptions/local/FileIOException.h>
#include <ArmarXCore/core/logging/Logging.h>


namespace devices::ethercat::power_board
{
    std::ostream&
    operator<<(std::ostream& out, const LedColorController::LedMapping::Led& led)
    {
        out << "Idx: " << led.index << " angle (deg): " << led.angle
            << " position: (x: " << led.position.x() << " y: " << led.position.y()
            << " z: " << led.position.z() << ")";
        out << " posOnStrips: " << led.strip << ":" << led.posOnStrip;
        return out;
    }

    std::ostream&
    operator<<(std::ostream& out, const LedColorController::LedMapping::Region& region)
    {
        out << "Name: " << region.name << " LEDs: [";
        unsigned int i = 0;
        for (const auto& led : region.leds)
        {
            out << led->index;
            if (++i < region.leds.size())
            {
                out << ", ";
            }
        }
        out << "]";
        return out;
    }

    bool
    operator==(const LedColorController::LedMapping::Led& a,
               const LedColorController::LedMapping::Led& b)
    {
        return simox::math::is_equal(a.index, b.index) && simox::math::is_equal(a.angle, b.angle) &&
               simox::math::is_equal(a.position, b.position) &&
               simox::math::is_equal(a.strip, b.strip) &&
               simox::math::is_equal(a.posOnStrip, b.posOnStrip);
    }

    void
    from_json(const simox::json::json& j, LedColorController::LedMapping::Led& led)
    {
        led.index = j["Index"];
        if (j.count("Angle"))
        {
            ARMARX_CHECK_IS_NULL(j.count("Position"))
                << "Either an Angle or a Position must be defined for each LED. Never both at the "
                   "same time.";
            led.angle = j["Angle"];
            led.position = Eigen::Vector3f(0, 0, 0);
        }
        else if (j.count("Position"))
        {
            ARMARX_CHECK_IS_NULL(j.count("Angle"))
                << "Either an Angle or a Position must be defined for each LED. Never both at the "
                   "same time.";
            j["Position"].get_to(led.position);

            // Calc angle
            float angle = simox::math::angle_between_vec3f_vec3f(
                Eigen::Vector3f(0, 1, 0),
                Eigen::Vector3f(led.position.x(), led.position.y(), 0),
                Eigen::Vector3f(0, 0, 1));
            led.angle = (angle >= 0) ? angle * 180 / M_PI : 360 + angle * 180 / M_PI;
            if (led.angle == -0)
                led.angle = 0;
            ARMARX_CHECK_GREATER_EQUAL(led.angle, 0);
            ARMARX_CHECK_LESS(led.angle, 360);
        }
        led.strip = j["Strip"];
        led.posOnStrip = j["PosOnStrip"];
    }

    void
    from_json(const simox::json::json& j, LedColorController::LedMapping& mapping)
    {
        mapping.ledCountPerStrip.clear();
        mapping.leds.clear();
        mapping.angleInDegreeToLed.clear();
        mapping.indexToLed.clear();

        mapping.stripCount = j["StripCount"];
        for (auto& c : j["LEDsPerStrip"])
        {
            mapping.ledCountPerStrip.push_back(c["LEDCount"]);
            mapping.highestLEDPosPerStrip.push_back(0);
        }
        ARMARX_CHECK_EQUAL(mapping.stripCount, mapping.ledCountPerStrip.size())
            << "The amount of entries in 'LEDsPerStrip' does not correspond to the value in "
               "'StripCount'";

        for (auto& elem : j["LEDs"])
        {
            LedColorController::LedMapping::Led led;
            elem.get_to(led);
            mapping.leds.push_back(led);
        }
        ARMARX_CHECK_EQUAL(
            mapping.leds.size(),
            std::accumulate(mapping.ledCountPerStrip.begin(), mapping.ledCountPerStrip.end(), 0u))
            << "The amount of specified LEDs does not equal the sum of the entries in "
               "'LEDsPerStrip'";


        for (const auto& led : mapping.leds)
        {
            ARMARX_CHECK_EXPRESSION(!mapping.indexToLed.count(led.index))
                << "Every index must be unique! Following index occurs atleast twice: "
                << led.index;
            mapping.indexToLed[led.index] = &led;
            ARMARX_CHECK_EXPRESSION(!mapping.angleInDegreeToLed.count(led.angle))
                << "The angle of each LED must be unique! Following LED has the same angle as "
                   "another LED: "
                << led;
            mapping.angleInDegreeToLed[led.angle] = &led;

            mapping.highestLEDPosPerStrip.at(led.strip) =
                std::max(mapping.highestLEDPosPerStrip.at(led.strip), led.posOnStrip);
        }

        // Verify that the indexes are starting at 0 and include every whole number up to the highest index
        int lastIndex = -1;
        for (const auto& [index, led] : mapping.indexToLed)
        {
            ARMARX_CHECK_EQUAL(lastIndex + 1, static_cast<int>(index))
                << "The indexes must begin at 0 and continue upwards by including every whole "
                   "number until the max index. Index "
                << lastIndex - 1 << " is missing.";
            lastIndex++;
        }

        // Parse regions if some have been defined in the json
        if (j.count("Regions"))
        {
            for (const auto& r : j["Regions"])
            {
                LedColorController::LedMapping::Region region;
                region.name = r["Name"];
                for (const auto& i : r["LED-Indices"])
                {
                    ARMARX_CHECK_EXPRESSION(mapping.indexToLed.count(i))
                        << "LED with index " << i << " has not be defined";
                    region.leds.push_back(mapping.indexToLed.at(i));
                }
                mapping.regions.push_back(region);
            }
        }
    }

    LedColorController::LedColorController(std::istream& ledMappingString,
                                           LedStripsColorsControllerInterfacePrx proxy)
    {
        m_proxy = proxy;

        m_mapping = LedMapping::createLedMappingFromIStream(ledMappingString);
        initLedColors();
    }

    LedColorController::LedColorController(const std::filesystem::path& ledMappingFilePath,
                                           LedStripsColorsControllerInterfacePrx proxy)
    {
        m_proxy = proxy;

        if (!std::filesystem::exists(ledMappingFilePath))
        {
            throw armarx::exceptions::local::FileIOException(ledMappingFilePath.string());
        }

        std::ifstream ifs;
        try
        {
            ifs.open(ledMappingFilePath);
            m_mapping = LedMapping::createLedMappingFromIStream(ifs);
        }
        catch (const std::ios_base::failure& e)
        {
            std::stringstream ss;
            ss << "Opening or parsing LED mapping file failed: \n" << e.what();
            throw simox::json::error::IOError(ss.str());
        }
        initLedColors();
    }

    const std::vector<simox::Color>&
    LedColorController::getColorsOfStrip(unsigned int index) const
    {
        ARMARX_CHECK_LESS(index, m_mapping.stripCount)
            << "Trying to get colors of strip at index which does not exist.";
        return m_ledColors.at(index);
    }

    bool
    LedColorController::setColor(const simox::Color color)
    {
        return setColorGradientBasedOnIndex(0, m_mapping.indexToLed.rbegin()->first, color, color);
    }

    bool
    LedColorController::setColorBasedOnIndex(unsigned int startIndex,
                                             unsigned int endIndex,
                                             const simox::Color color)
    {
        ARMARX_DEBUG << "Setting color from index " << startIndex << " to index " << endIndex
                     << " to " << color;
        return setColorGradientBasedOnIndex(startIndex, endIndex, color, color);
    }

    bool
    LedColorController::setColorBasedOnAngle(float startAngleInDegree,
                                             float endAngleInDegree,
                                             const simox::Color color)
    {
        return setColorGradientBasedOnAngle(startAngleInDegree, endAngleInDegree, color, color);
    }

    bool
    LedColorController::setColorBasedOnRegion(const LedMapping::Region& region,
                                              const simox::Color color)
    {
        for (const auto& led : region.leds)
        {
            setColor(*led, color);
        }
        return true;
    }

    bool
    LedColorController::setColorGradientBasedOnIndex(unsigned int startIndex,
                                                     unsigned int endIndex,
                                                     const simox::Color startColor,
                                                     const simox::Color endColor)
    {
        if (!m_mapping.indexToLed.count(startIndex))
        {
            ARMARX_WARNING << "StartIndex not part of configured LEDs";
            return false;
        }

        if (!m_mapping.indexToLed.count(endIndex))
        {
            ARMARX_WARNING << "EndIndex not part of configured LEDs";
            return false;
        }

        auto it = m_mapping.indexToLed.find(startIndex);
        auto endIt = ++m_mapping.indexToLed.find(endIndex);
        if (endIt == m_mapping.indexToLed.end())
        {
            endIt = m_mapping.indexToLed.begin();
        }
        do
        {
            setColor(*it->second,
                     calcGradientBasedOnIndex(startColor,
                                              startIndex,
                                              endColor,
                                              endIndex,
                                              it->first,
                                              m_mapping.indexToLed.rbegin()->first));
            it++;
            if (it == m_mapping.indexToLed.end())
            {
                // Rollover to beginning
                it = m_mapping.indexToLed.begin();
            }
        } while (it != endIt);

        return true;
    }

    bool
    LedColorController::setColorGradientBasedOnAngle(float startAngleInDegree,
                                                     float endAngleInDegree,
                                                     const simox::Color startColor,
                                                     const simox::Color endColor)
    {
        startAngleInDegree = simox::math::periodic_clamp(startAngleInDegree, 0.f, 360.f);
        endAngleInDegree = simox::math::periodic_clamp(endAngleInDegree, 0.f, 360.f);

        auto it = m_mapping.getIteratorToNextLedAtAngle(startAngleInDegree);
        if (it == m_mapping.getIteratorToNextLedAtAngle(endAngleInDegree))
        {
            return false;
        }

        auto endIt = m_mapping.getIteratorToPreviousLedAtAngle(endAngleInDegree);
        endIt++;
        if (endIt == m_mapping.angleInDegreeToLed.end())
        {
            endIt = m_mapping.angleInDegreeToLed.begin();
        }

        while (it != endIt)
        {
            setColor(*it->second,
                     calcGradientBasedOnAngle(
                         startColor, startAngleInDegree, endColor, endAngleInDegree, it->first));
            it++;
            if (it == m_mapping.angleInDegreeToLed.end())
            {
                // Rollover to beginning
                it = m_mapping.angleInDegreeToLed.begin();
            }
        }

        return true;
    }

    void
    LedColorController::resetColors()
    {
        for (const auto& led : m_mapping.leds)
        {
            setColor(led, simox::Color::black());
        }
    }

    simox::Color
    LedColorController::getColorAtIndex(unsigned int index) const
    {
        auto led = m_mapping.indexToLed.at(index);
        return m_ledColors.at(led->strip).at(led->posOnStrip);
    }

    simox::Color
    LedColorController::getColorAtAngle(float angle) const
    {
        auto prevLed = m_mapping.getIteratorToPreviousLedAtAngle(angle)->second;
        auto nextLed = m_mapping.getIteratorToNextLedAtAngle(angle)->second;

        auto colorPrev = m_ledColors.at(prevLed->strip).at(prevLed->posOnStrip);
        auto colorNext = m_ledColors.at(nextLed->strip).at(nextLed->posOnStrip);

        // The provided angle is exactly the angle of one of the defined LEDs
        if (*prevLed == *nextLed)
        {
            return colorPrev;
        }

        float diffToPrev = simox::math::periodic_diff(angle, prevLed->angle, 0.f, 360.f);
        float diffToNext = simox::math::periodic_diff(nextLed->angle, angle, 0.f, 360.f);

        return (diffToPrev <= diffToNext) ? colorPrev : colorNext;
    }

    const std::map<unsigned int, const LedColorController::LedMapping::Led*>&
    LedColorController::getIndexToLedMapping() const
    {
        return this->m_mapping.indexToLed;
    }

    const std::vector<LedColorController::LedMapping::Region>&
    LedColorController::getRegions() const
    {
        return m_mapping.regions;
    }

    void
    LedColorController::printLeds() const
    {
        std::stringstream ss;
        ss << "LedColorController with " << m_mapping.stripCount << " strips:\n";

        for (const auto& [angle, led] : m_mapping.angleInDegreeToLed)
        {
            ss << *led;
            ss << '\n';
        }
        ARMARX_INFO << ss.str();
    }


    LedColorController::LedMapping
    LedColorController::LedMapping::createLedMappingFromIStream(std::istream& is)
    {
        LedMapping mapping;
        try
        {
            simox::json::json mappingJson = simox::json::read(is);
            mappingJson.get_to(mapping);
        }
        catch (const simox::json::error::JsonError& e)
        {
            ARMARX_WARNING << "Parsing LED mapping file failed: \n" << e.what();
        }

        return mapping;
    }

    const std::map<float, const LedColorController::LedMapping::Led*>::const_iterator
    LedColorController::LedMapping::getIteratorToPreviousLedAtAngle(float angle) const
    {
        auto prev = angleInDegreeToLed.upper_bound(angle);
        if (prev == angleInDegreeToLed.begin())
        {
            return angleInDegreeToLed.crbegin().base();
        }
        else
        {
            return (--prev);
        }
    }

    const std::map<float, const LedColorController::LedMapping::Led*>::const_iterator
    LedColorController::LedMapping::getIteratorToNextLedAtAngle(float angle) const
    {
        auto next = angleInDegreeToLed.lower_bound(angle);
        if (next == angleInDegreeToLed.end())
        {
            return angleInDegreeToLed.begin();
        }
        else
        {
            return next;
        }
    }


    void
    LedColorController::setColor(LedMapping::Led led, simox::Color color)
    {
        m_ledColors.at(led.strip).at(led.posOnStrip) = color;
    }

    simox::Color
    LedColorController::calcGradientBasedOnAngle(simox::Color color1,
                                                 float angle1,
                                                 simox::Color color2,
                                                 float angle2,
                                                 float targetAngle)
    {
        ARMARX_CHECK_LESS(angle1, 360.f);
        ARMARX_CHECK_LESS(angle2, 360.f);
        ARMARX_CHECK_LESS(targetAngle, 360.f);

        if (angle1 < angle2)
        {
            if (angle1 > targetAngle || angle2 < targetAngle)
            {
                throw std::runtime_error("calcGradientBasedOnAngle: targetAngle (" +
                                         std::to_string(targetAngle) + ") is not between angle1 (" +
                                         std::to_string(angle1) + ") and angle2 (" +
                                         std::to_string(angle2) + ")");
            }

            float t = (targetAngle - angle1) / (angle2 - angle1);
            return simox::color::interpol::linear(t, color1, color2);
        }
        else if (simox::math::is_equal(angle1, angle2))
        {
            return color1;
        }
        else
        {
            if (targetAngle < angle1 && targetAngle > angle2)
            {
                throw std::runtime_error("calcGradientBasedOnAngle: targetAngle (" +
                                         std::to_string(targetAngle) + ") is not between angle1 (" +
                                         std::to_string(angle1) + ") and angle2 (" +
                                         std::to_string(angle2) + ")");
            }

            auto diff_periodic = [](float a, float b) { return (a <= b) ? b - a : 360.f - a + b; };

            float angle1_to_target = diff_periodic(angle1, targetAngle);
            float angle1_to_angle2 = diff_periodic(angle1, angle2);

            float t = angle1_to_target / angle1_to_angle2;
            return simox::color::interpol::linear(t, color1, color2);
        }
    }

    simox::Color
    LedColorController::calcGradientBasedOnIndex(simox::Color color1,
                                                 unsigned int index1,
                                                 simox::Color color2,
                                                 unsigned int index2,
                                                 unsigned int targetIndex,
                                                 unsigned int maxIndex)
    {
        ARMARX_CHECK_LESS_EQUAL(index1, maxIndex);
        ARMARX_CHECK_LESS_EQUAL(index2, maxIndex);
        ARMARX_CHECK_LESS_EQUAL(targetIndex, maxIndex);

        if (index1 < index2)
        {
            if (index1 > targetIndex || index2 < targetIndex)
            {
                throw std::runtime_error("calcGradientBasedOnIndex: targetIndex (" +
                                         std::to_string(targetIndex) + ") is not between index1 (" +
                                         std::to_string(index1) + ") and index2 (" +
                                         std::to_string(index2) + ")");
            }

            float t =
                static_cast<float>(targetIndex - index1) / static_cast<float>(index2 - index1);
            return simox::color::interpol::linear(t, color1, color2);
        }
        else if (index1 == index2)
        {
            return color1;
        }
        else
        {
            if (targetIndex < index1 && targetIndex > index2)
            {
                throw std::runtime_error("calcGradientBasedOnIndex: targetIndex (" +
                                         std::to_string(targetIndex) + ") is not between index1 (" +
                                         std::to_string(index1) + ") and index2 (" +
                                         std::to_string(index2) + ")");
            }

            auto diff = [](unsigned int a, unsigned int b, unsigned int max)
            { return (a <= b) ? b - a : (max + 1) - a + b; };

            unsigned int index1_to_target = diff(index1, targetIndex, maxIndex);
            unsigned int index1_to_index2 = diff(index1, index2, maxIndex);

            float t = static_cast<float>(index1_to_target) / static_cast<float>(index1_to_index2);
            return simox::color::interpol::linear(t, color1, color2);
        }
    }

    void
    LedColorController::initLedColors()
    {
        m_ledColors.resize(m_mapping.stripCount);
        for (unsigned int i = 0; i < m_mapping.stripCount; i++)
        {
            m_ledColors.at(i).resize(m_mapping.highestLEDPosPerStrip.at(i) + 1);
        }

        printLeds();
    }

    void
    LedColorController::commitColorDataToRtController() const
    {
        if (m_proxy)
        {
            for (unsigned int i = 0; i < m_ledColors.size(); i++)
            {
                std::vector<armarx::viz::data::Color> colors;
                colors.reserve(m_ledColors.at(i).size());
                for (const auto& c : m_ledColors.at(i))
                {
                    colors.emplace_back(255, c.r, c.g, c.b);
                }
                m_proxy->setColorsForLEDStrip(static_cast<int>(i), colors);
            }
        }
    }


} // namespace devices::ethercat::power_board
