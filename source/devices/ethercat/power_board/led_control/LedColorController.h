#pragma once

#include <filesystem>
#include <map>

#include <SimoxUtility/color/Color.h>
#include <SimoxUtility/math/compare/is_equal.h>

#include <devices/ethercat/power_board/njoint_controller/LedStripsColorsInterface.h>

namespace devices::ethercat::power_board
{
    class LedColorController
    {
    public:
        struct LedMapping
        {
            unsigned int stripCount;
            std::vector<unsigned int> ledCountPerStrip;
            std::vector<unsigned int> highestLEDPosPerStrip;

            struct Led
            {
                unsigned int index;
                // angle in degree when looking at the LEDs from the top (in direction of the
                // negative z-axis). In a right-handed coordinate system a positiv angle corresponds
                // to a counterclock-wise rotation.
                float angle;
                // position in the root frame of the robot
                Eigen::Vector3f position;
                unsigned int strip;
                unsigned int posOnStrip;
            };

            std::vector<Led> leds;

            std::map<unsigned int, const Led*> indexToLed;
            std::map<float, const Led*> angleInDegreeToLed;

            struct Region
            {
                std::string name;
                std::vector<const Led*> leds;
            };

            std::vector<Region> regions;

            static LedMapping createLedMappingFromIStream(std::istream& is);

            const std::map<float, const Led*>::const_iterator
            getIteratorToPreviousLedAtAngle(float angle) const;
            const std::map<float, const Led*>::const_iterator
            getIteratorToNextLedAtAngle(float angle) const;
        };

        LedColorController(std::istream& ledMappingString,
                           LedStripsColorsControllerInterfacePrx proxy = nullptr);
        LedColorController(const std::filesystem::path& ledMappingFilePath,
                           LedStripsColorsControllerInterfacePrx proxy = nullptr);

        const std::vector<simox::Color>& getColorsOfStrip(unsigned int index) const;

        bool setColor(const simox::Color color);

        bool setColorBasedOnIndex(unsigned int startIndex,
                                  unsigned int endIndex,
                                  const simox::Color color);

        bool setColorBasedOnAngle(float startAngleInDegree,
                                  float endAngleInDegree,
                                  const simox::Color color);

        bool setColorBasedOnRegion(const LedMapping::Region& region, const simox::Color color);

        bool setColorGradientBasedOnIndex(unsigned int startIndex,
                                          unsigned int endIndex,
                                          const simox::Color startColor,
                                          const simox::Color endColor);

        bool setColorGradientBasedOnAngle(float startAngleInDegree,
                                          float endAngleInDegree,
                                          const simox::Color startColor,
                                          const simox::Color endColor);


        void resetColors();

        simox::Color getColorAtIndex(unsigned int index) const;
        simox::Color getColorAtAngle(float angle) const;

        const std::map<unsigned int, const LedMapping::Led*>& getIndexToLedMapping() const;
        const std::vector<LedMapping::Region>& getRegions() const;

        void commitColorDataToRtController() const;

    private:
        std::vector<std::vector<simox::Color>> m_ledColors;

        LedMapping m_mapping;

        LedStripsColorsControllerInterfacePrx m_proxy;

        void printLeds() const;

        void setColor(LedMapping::Led led, simox::Color color);
        static simox::Color calcGradientBasedOnAngle(simox::Color color1,
                                                     float angle1,
                                                     simox::Color color2,
                                                     float angle2,
                                                     float targetAngle);
        static simox::Color calcGradientBasedOnIndex(simox::Color color1,
                                                     unsigned int index1,
                                                     simox::Color color2,
                                                     unsigned int index2,
                                                     unsigned int targetIndex,
                                                     unsigned int maxIndex);

        void initLedColors();
    };

    std::ostream& operator<<(std::ostream& out, const LedColorController::LedMapping::Led& led);

    std::ostream& operator<<(std::ostream& out,
                             const LedColorController::LedMapping::Region& region);

} // namespace devices::ethercat::power_board
