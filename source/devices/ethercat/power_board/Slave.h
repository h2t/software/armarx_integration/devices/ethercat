/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Jan Hausberg (jan dot hausberg at kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <armarx/control/ethercat/SlaveInterface.h>

#include <devices/ethercat/power_board/ErrorDecoder.h>
#include <devices/ethercat/power_board/SlaveIO.h>
#include <devices/ethercat/power_board/Config.h>

namespace devices::ethercat::power_board
{
    class Slave : public armarx::control::ethercat::SlaveInterfaceWithIO<SlaveIn, SlaveOut>
    {

    public:
        Slave(const armarx::control::ethercat::SlaveIdentifier slaveIdentifier);

        /**
         * @see AbstractSlave::doMappings
         */
        void doMappings() override;

        void prepareForSafeOp() override;
        void finishPreparingForSafeOp() override;

        void prepareForOp() override;
        void finishPreparingForOp() override;

        /**
         * @see AbstractSlave::prepare()
         */
        bool prepareForRun() override;

        /**
         * @see AbstractSlave::execute()
         */
        void execute() override;

        /**
         * @see AbstractSlave::shutdown()
         */
        bool shutdown() override;

        /**
         * @see AbstractSlave::hasError()
         * @return
         */
        bool hasError() override;

        bool isEmergencyStopActive() const override;
        bool recoverFromEmergencyStop() override;

        static bool
        isSlaveIdentifierAccepted(const armarx::control::ethercat::SlaveIdentifier& sid);

        static std::string getDefaultName();

        /**
         * @brief Read the CoE value ledRgbStrip1Count over the ethercat bus.
         * Do not call during operation because it can take more than 1ms.
         */
        std::uint8_t readLedRgbStrip1Count();

        /**
         * @brief Read the CoE value ledRgbStrip2Count over the ethercat bus.
         * Do not call during operation because it can take more than 1ms.
         */
        std::uint8_t readLedRgbStrip2Count();

        /**
         * @brief Read the CoE value ledRgbStrip1GlobalBrightness over the ethercat bus.
         * Do not call during operation because it can take more than 1ms.
         */
        std::uint8_t readLedRgbStrip1GlobalBrightness();

        /**
         * @brief Read the CoE value ledRgbStrip2GlobalBrightness over the ethercat bus.
         * Do not call during operation because it can take more than 1ms.
         */
        std::uint8_t readLedRgbStrip2GlobalBrightness();

        /**
         * @brief Set ledRgbStrip1Count that is applied when switching to safe-op
         */
        void setLedRgbStrip1Count(std::uint8_t ledRgbStripCount);

        /**
         * @brief Set ledRgbStrip2Count that is applied when switching to safe-op
         */
        void setLedRgbStrip2Count(std::uint8_t ledRgbStripCount);

        /**
         * @brief Set ledRgbStrip1GlobalBrightness that is applied when switching to safe-op
         */
        void setLedRgbStrip1GlobalBrightness(std::uint8_t ledRgbStripGlobalBrightness);

        /**
         * @brief Set ledRgbStrip2GlobalBrightness that is applied when switching to safe-op
         */
        void setLedRgbStrip2GlobalBrightness(std::uint8_t ledRgbStripGlobalBrightness);

        void setConfig(SlaveConfig& config);

    private:
        ErrorDecoder errorDecoder;
        armarx::control::ethercat::Bus& bus;

        /*
         * Keep a local copy of CoE values that are set using the setter.
         * These CoE entries are sent over ethercat in 'prepareForSafeOp()'.
         * The type could also be optional in case the default value of the slave
         * should not be overwritten.
         */
        std::uint8_t ledRgbStrip1Count;
        std::uint8_t ledRgbStrip2Count;
        std::uint8_t ledRgbStrip1GlobalBrightness;
        std::uint8_t ledRgbStrip2GlobalBrightness;

        // Method to bundle CoE writes
        void writeCoE();
    };

} // namespace devices::ethercat::power_board
