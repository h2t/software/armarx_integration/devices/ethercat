#pragma once

#include <memory>

#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>

#include "../led_control/LedColorController.h"
#include <devices/ethercat/power_board/ControlTargets.h>
#include <devices/ethercat/power_board/njoint_controller/LedStripsColorsInterface.h>


namespace devices::ethercat::power_board
{
    TYPEDEF_PTRS_HANDLE(LedStripsColorsController);

    class LedStripsColorsControllerControlData
    {
    public:
        std::vector<simox::Color> ledStrip1_colors;
        std::vector<simox::Color> ledStrip2_colors;
    };


    class LedStripsColorsController :
        public armarx::NJointControllerWithTripleBuffer<LedStripsColorsControllerControlData>,
        public LedStripsColorsControllerInterface

    {

    public:
        using ConfigPtrT = LedStripsColorsControllerConfigPtr;
        LedStripsColorsController(armarx::RobotUnit* prov,
                                  LedStripsColorsControllerConfigPtr config,
                                  const VirtualRobot::RobotPtr& r);

        // NJointControllerInterface interface

    public:
        std::string
        getClassName(const Ice::Current&) const override
        {
            return "NJointPowerBoardLedStripsColorsController";
        }

        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;

        static armarx::WidgetDescription::WidgetPtr
        GenerateConfigDescription(const VirtualRobot::RobotPtr&,
                                  const std::map<std::string, armarx::ConstControlDevicePtr>&,
                                  const std::map<std::string, armarx::ConstSensorDevicePtr>&);

        static LedStripsColorsControllerConfigPtr
        GenerateConfigFromVariants(const armarx::StringVariantBaseMap& values);

    private:
        ControlTargetPowerBoardLedColor* ledStrip0ControlTarget;
        ControlTargetPowerBoardLedColor* ledStrip1ControlTarget;

        std::unique_ptr<LedColorController> ledColorMapping;

        void copyColorDataToTripleBuffer();

    public:
        armarx::WidgetDescription::StringWidgetDictionary
        getFunctionDescriptions(const Ice::Current&) const override;
        void callDescribedFunction(const std::string& name,
                                   const armarx::StringVariantBaseMap& valueMap,
                                   const Ice::Current& = Ice::emptyCurrent) override;

        void setColorsForLEDStrip(Ice::Int stripID,
                                  const armarx::viz::data::ColorSeq& colors,
                                  const Ice::Current& = Ice::emptyCurrent) override;

    protected:
        void rtPreActivateController() override;
    };
} // namespace devices::ethercat::power_board
