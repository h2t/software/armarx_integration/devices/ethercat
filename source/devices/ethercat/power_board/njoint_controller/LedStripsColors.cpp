#include <devices/ethercat/power_board/njoint_controller/LedStripsColors.h>


namespace devices::ethercat::power_board
{

    armarx::NJointControllerRegistration<LedStripsColorsController>
        registrationControllerNJointPowerBoardController(
            "NJointPowerBoardLedStripsColorsController");


    LedStripsColorsController::LedStripsColorsController(armarx::RobotUnit* prov,
                                                         LedStripsColorsControllerConfigPtr config,
                                                         const VirtualRobot::RobotPtr&)
    {
        ARMARX_CHECK_EXPRESSION(prov);
        auto ledStrip1DeviceName = config->deviceName + "LedStrip1";
        armarx::ControlTargetBase* ct =
            useControlTarget(ledStrip1DeviceName, ctrl_modes::PowerBoardLedColor);
        ARMARX_CHECK_EXPRESSION(ct);

        ARMARX_CHECK_EXPRESSION(ct->asA<ControlTargetPowerBoardLedColor>());
        ledStrip0ControlTarget = ct->asA<ControlTargetPowerBoardLedColor>();

        auto ledStrip2DeviceName = config->deviceName + "LedStrip2";
        ct = useControlTarget(ledStrip2DeviceName, ctrl_modes::PowerBoardLedColor);
        ARMARX_CHECK_EXPRESSION(ct);

        ARMARX_CHECK_EXPRESSION(ct->asA<ControlTargetPowerBoardLedColor>());
        ledStrip1ControlTarget = ct->asA<ControlTargetPowerBoardLedColor>();

        if (!config->ledMappingPath.empty())
        {
            ledColorMapping =
                std::make_unique<LedColorController>(std::filesystem::path(config->ledMappingPath));
        }
    }


    void
    LedStripsColorsController::rtPreActivateController()
    {
    }


    void
    LedStripsColorsController::rtRun(const IceUtil::Time& /*sensorValuesTimestamp*/,
                                     const IceUtil::Time& /*timeSinceLastIteration*/)
    {
        rtUpdateControlStruct();

        std::copy_n(rtGetControlStruct().ledStrip1_colors.begin(),
                    rtGetControlStruct().ledStrip1_colors.size(),
                    ledStrip0ControlTarget->colors.begin());
        std::copy_n(rtGetControlStruct().ledStrip2_colors.begin(),
                    rtGetControlStruct().ledStrip2_colors.size(),
                    ledStrip1ControlTarget->colors.begin());
    }

    armarx::WidgetDescription::WidgetPtr
    LedStripsColorsController::GenerateConfigDescription(
        const VirtualRobot::RobotPtr&,
        const std::map<std::string, armarx::ConstControlDevicePtr>&,
        const std::map<std::string, armarx::ConstSensorDevicePtr>&)
    {
        using namespace armarx::WidgetDescription;
        HBoxLayoutPtr layout = new HBoxLayout;

        LabelPtr label = new Label;
        label->text = "control device name";
        layout->children.emplace_back(label);

        LineEditPtr edit = new LineEdit;
        edit->name = "deviceName";
        edit->defaultValue = "PowerBoard";
        layout->children.emplace_back(edit);

        LineEditPtr editMappingFile = new LineEdit;
        editMappingFile->name = "mappingFile";
        editMappingFile->defaultValue = "[Path to file with led mapping]";
        layout->children.emplace_back(editMappingFile);
        return layout;
    }

    LedStripsColorsControllerConfigPtr
    LedStripsColorsController::GenerateConfigFromVariants(
        const armarx::StringVariantBaseMap& values)
    {
        return new LedStripsColorsControllerConfig{values.at("deviceName")->getString(),
                                                   values.at("mappingFile")->getString()};
    }

    void
    LedStripsColorsController::copyColorDataToTripleBuffer()
    {
        if (ledColorMapping)
        {
            LockGuardType guard{controlDataMutex};
            getWriterControlStruct().ledStrip1_colors = ledColorMapping->getColorsOfStrip(0);
            getWriterControlStruct().ledStrip2_colors = ledColorMapping->getColorsOfStrip(1);
            writeControlStruct();
        }
    }


    armarx::WidgetDescription::StringWidgetDictionary
    LedStripsColorsController::getFunctionDescriptions(const Ice::Current&) const
    {
        using namespace armarx::WidgetDescription;

        HBoxLayoutPtr hbox2 = new HBoxLayout;
        {
            hbox2->children.emplace_back(new Label(false, "StartIndex"));
            hbox2->children.emplace_back(new IntSpinBox(false, "StartIndexSpin", 0, 5, 0));
        }
        {
            hbox2->children.emplace_back(new Label(false, "EndIndex"));
            hbox2->children.emplace_back(new IntSpinBox(false, "EndIndexSpin", 0, 5, 0));
        }
        {
            hbox2->children.emplace_back(new Label(false, "red"));
            IntSliderPtr slider = new IntSlider(false, "redSlider", 0, 255, 0);
            hbox2->children.emplace_back(slider);
        }
        {
            hbox2->children.emplace_back(new Label(false, "green"));
            IntSliderPtr slider = new IntSlider(false, "greenSlider", 0, 255, 0);
            hbox2->children.emplace_back(slider);
        }
        {
            hbox2->children.emplace_back(new Label(false, "blue"));
            IntSliderPtr slider = new IntSlider(false, "blueSlider", 0, 255, 0);
            hbox2->children.emplace_back(slider);
        }

        HBoxLayoutPtr hbox3 = new HBoxLayout;
        {
            hbox3->children.emplace_back(new Label(false, "StartIndex"));
            hbox3->children.emplace_back(new IntSpinBox(false, "StartIndexSpin", 0, 5, 0));
        }
        {
            hbox3->children.emplace_back(new Label(false, "EndIndex"));
            hbox3->children.emplace_back(new IntSpinBox(false, "EndIndexSpin", 0, 5, 0));
        }
        {
            hbox3->children.emplace_back(new Label(false, "red_start"));
            IntSliderPtr slider = new IntSlider(false, "red_startSlider", 0, 255, 0);
            hbox3->children.emplace_back(slider);
        }
        {
            hbox3->children.emplace_back(new Label(false, "green_start"));
            IntSliderPtr slider = new IntSlider(false, "green_startSlider", 0, 255, 0);
            hbox3->children.emplace_back(slider);
        }
        {
            hbox3->children.emplace_back(new Label(false, "blue_start"));
            IntSliderPtr slider = new IntSlider(false, "blue_startSlider", 0, 255, 0);
            hbox3->children.emplace_back(slider);
        }
        {
            hbox3->children.emplace_back(new Label(false, "red_end"));
            IntSliderPtr slider = new IntSlider(false, "red_endSlider", 0, 255, 0);
            hbox3->children.emplace_back(slider);
        }
        {
            hbox3->children.emplace_back(new Label(false, "green_end"));
            IntSliderPtr slider = new IntSlider(false, "green_endSlider", 0, 255, 0);
            hbox3->children.emplace_back(slider);
        }
        {
            hbox3->children.emplace_back(new Label(false, "blue_end"));
            IntSliderPtr slider = new IntSlider(false, "blue_endSlider", 0, 255, 0);
            hbox3->children.emplace_back(slider);
        }

        return {{"ColorBasedOnIndex", hbox2}, {"ColorGradientBasedOnIndex", hbox3}};
    }


    void
    LedStripsColorsController::callDescribedFunction(const std::string& name,
                                                     const armarx::StringVariantBaseMap& valueMap,
                                                     const Ice::Current&)
    {
        if (ledColorMapping)
        {
            if (name == "ColorBasedOnIndex")
            {
                ledColorMapping->resetColors();
                ledColorMapping->setColorBasedOnIndex(
                    static_cast<unsigned int>(valueMap.at("StartIndexSpin")->getInt()),
                    static_cast<unsigned int>(valueMap.at("EndIndexSpin")->getInt()),
                    simox::Color(valueMap.at("redSlider")->getInt(),
                                 valueMap.at("greenSlider")->getInt(),
                                 valueMap.at("blueSlider")->getInt()));
            }
            else if (name == "ColorGradientBasedOnIndex")
            {
                ledColorMapping->resetColors();
                ledColorMapping->setColorGradientBasedOnIndex(
                    static_cast<unsigned int>(valueMap.at("StartIndexSpin")->getInt()),
                    static_cast<unsigned int>(valueMap.at("EndIndexSpin")->getInt()),
                    simox::Color(valueMap.at("red_startSlider")->getInt(),
                                 valueMap.at("green_startSlider")->getInt(),
                                 valueMap.at("blue_startSlider")->getInt()),
                    simox::Color(valueMap.at("red_endSlider")->getInt(),
                                 valueMap.at("green_endSlider")->getInt(),
                                 valueMap.at("blue_endSlider")->getInt()));
            }
            else
            {
                ARMARX_WARNING << "Unknown function name called: " << name;
            }

            copyColorDataToTripleBuffer();
        }
    }

    void
    LedStripsColorsController::setColorsForLEDStrip(Ice::Int stripID,
                                                    const armarx::viz::data::ColorSeq& colors,
                                                    const Ice::Current&)
    {
        ARMARX_CHECK(stripID == 0 || stripID == 1) << "Only valid values for stripID are 0 and 1";
        LockGuardType guard{controlDataMutex};
        auto* targetList = (stripID == 0) ? &getWriterControlStruct().ledStrip1_colors
                                          : &getWriterControlStruct().ledStrip2_colors;
        targetList->resize(colors.size());
        for (unsigned int i = 0; i < colors.size(); i++)
        {
            armarx::viz::data::Color color = colors.at(i);
            targetList->at(i) = simox::Color(color.r, color.g, color.b);
        }
        writeControlStruct();
    }

} // namespace devices::ethercat::power_board
