#pragma once

#include <array>

#include <armarx/control/ethercat/DataInterface.h>


namespace devices::ethercat::power_board
{
    class LedControlData : public armarx::control::ethercat::DataInterface
    {

    public:
        LedControlData(std::uint16_t* target);

        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration) override;
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                 const IceUtil::Time& timeSinceLastIteration) override;

        void setLedRgbStripControl(std::array<std::uint16_t, 40> value);

    private:
        std::uint16_t* target;
        std::array<std::uint16_t, 40> ledRgbStripControl;

    };
    using LedControlDataPtr =
        std::shared_ptr<LedControlData>;
} // namespace devices::ethercat::power_board
