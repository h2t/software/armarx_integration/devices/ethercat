#define BOOST_TEST_MODULE Devices::Ethercat::PowerBoard

#define ARMARX_BOOST_TEST

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <SimoxUtility/color/interpolation.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <devices/ethercat/Test.h>
#include <devices/ethercat/power_board/led_control/LedColorController.h>

using namespace devices::ethercat::power_board;

std::istringstream
getTestJson_withAngles()
{
    std::string sampleJson = R""""(
{
    "StripCount": 2,
    "LEDsPerStrip": [
        {
            "LEDCount": 4
        },
        {
            "LEDCount": 1
        }
    ],
    "LEDs": [
        {
            "Index": 0,
            "Angle": 0.0,
            "Strip": 0,
            "PosOnStrip": 0
        },
        {
            "Index": 1,
            "Angle": 72.0,
            "Strip": 0,
            "PosOnStrip": 1
        },
        {
            "Index": 2,
            "Angle": 144.0,
            "Strip": 0,
            "PosOnStrip": 2
        },
        {
            "Index": 3,
            "Angle": 216.0,
            "Strip": 0,
            "PosOnStrip": 3
        },
        {
            "Index": 4,
            "Angle": 288.0,
            "Strip": 1,
            "PosOnStrip": 5
        }
    ]
}
    )"""";

    std::istringstream is(sampleJson);
    return is;
}

std::istringstream
getTestJson_withPositions()
{
    std::string sampleJson = R""""(
{
    "StripCount": 1,
    "LEDsPerStrip": [
        {
            "LEDCount": 4
        }
    ],
    "LEDs": [
        {
            "Index": 0,
            "Position": {"x": 0.0, "y": 5.0, "z": 0.0},
            "Strip": 0,
            "PosOnStrip": 0
        },
        {
            "Index": 1,
            "Position": {"x": -5.0, "y": 5.0, "z": 0.0},
            "Strip": 0,
            "PosOnStrip": 1
        },
        {
            "Index": 2,
            "Position": {"x": 5.0, "y": -5.0, "z": 0.0},
            "Strip": 0,
            "PosOnStrip": 2
        },
        {
            "Index": 3,
            "Position": {"x": -5.0, "y": -5.0, "z": 0.0},
            "Strip": 0,
            "PosOnStrip": 3
        }
    ]
}
    )"""";

    std::istringstream is(sampleJson);
    return is;
}

BOOST_AUTO_TEST_CASE(defaultTest)
{
    BOOST_CHECK_EQUAL(true, true);
}

BOOST_AUTO_TEST_CASE(parsingTest)
{
    auto is = getTestJson_withAngles();
    LedColorController::LedMapping mapping =
        LedColorController::LedMapping::createLedMappingFromIStream(is);

    BOOST_CHECK_EQUAL(mapping.stripCount, 2);
    BOOST_CHECK_EQUAL(mapping.ledCountPerStrip.at(0), 4);
    BOOST_CHECK_EQUAL(mapping.ledCountPerStrip.at(1), 1);

    BOOST_CHECK_EQUAL(mapping.highestLEDPosPerStrip.at(0), 3);
    BOOST_CHECK_EQUAL(mapping.highestLEDPosPerStrip.at(1), 5);

    BOOST_CHECK_EQUAL(mapping.angleInDegreeToLed.at(216.f)->index, 3);
    BOOST_CHECK_EQUAL(mapping.angleInDegreeToLed.at(216.f)->strip, 0);

    BOOST_CHECK_EQUAL(mapping.angleInDegreeToLed.at(288.f)->index, 4);
    BOOST_CHECK_EQUAL(mapping.angleInDegreeToLed.at(288.f)->strip, 1);

    BOOST_CHECK_EQUAL(mapping.indexToLed.at(1)->posOnStrip, 1);
    BOOST_CHECK_EQUAL(mapping.indexToLed.at(1)->strip, 0);

    BOOST_CHECK_EQUAL(mapping.indexToLed.at(4)->posOnStrip, 5);
    BOOST_CHECK_EQUAL(mapping.indexToLed.at(4)->strip, 1);
}

BOOST_AUTO_TEST_CASE(parsingTest_withPositions)
{
    auto is = getTestJson_withPositions();
    LedColorController::LedMapping mapping =
        LedColorController::LedMapping::createLedMappingFromIStream(is);

    BOOST_CHECK_EQUAL(mapping.stripCount, 1);
}

BOOST_AUTO_TEST_CASE(parsingTest_withPositions_withPrint)
{
    auto is = getTestJson_withPositions();
    LedColorController colorController(is);
    BOOST_CHECK_EQUAL(true, true);
}


BOOST_AUTO_TEST_CASE(colorVectorSizesTest)
{
    auto is = getTestJson_withAngles();
    LedColorController colorController(is);

    BOOST_CHECK_EQUAL(colorController.getColorsOfStrip(0).size(), 4);
    BOOST_CHECK_EQUAL(colorController.getColorsOfStrip(1).size(), 6);
    BOOST_CHECK_THROW(colorController.getColorsOfStrip(2).size(), armarx::LocalException);
}

BOOST_AUTO_TEST_CASE(colorSettingBasedOnIndexTest)
{
    auto is = getTestJson_withAngles();
    LedColorController colorController(is);

    colorController.setColorBasedOnIndex(0, 0, simox::Color::green());

    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(4), simox::Color::black());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(0), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(1), simox::Color::black());

    colorController.resetColors();
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(0), simox::Color::black());

    colorController.setColorBasedOnIndex(1, 2, simox::Color::green());

    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(0), simox::Color::black());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(1), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(2), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(3), simox::Color::black());

    colorController.setColorBasedOnIndex(2, 3, simox::Color::red());

    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(0), simox::Color::black());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(1), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(2), simox::Color::red());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(3), simox::Color::red());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(4), simox::Color::black());

    colorController.resetColors();
    colorController.setColorBasedOnIndex(3, 1, simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(0), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(1), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(2), simox::Color::black());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(3), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(4), simox::Color::green());

    colorController.resetColors();
    colorController.setColorBasedOnIndex(3, 4, simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(0), simox::Color::black());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(1), simox::Color::black());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(2), simox::Color::black());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(3), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(4), simox::Color::green());
}

BOOST_AUTO_TEST_CASE(colorGradientSettingBasedOnIndexTest)
{
    auto is = getTestJson_withAngles();
    LedColorController colorController(is);

    colorController.setColorGradientBasedOnIndex(0, 1, simox::Color::green(), simox::Color::red());

    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(0), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(1), simox::Color::red());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(2), simox::Color::black());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(3), simox::Color::black());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(4), simox::Color::black());

    colorController.resetColors();
    colorController.setColorGradientBasedOnIndex(0, 2, simox::Color::green(), simox::Color::red());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(0), simox::Color::green());
    BOOST_CHECK_EQUAL(
        colorController.getColorAtIndex(1),
        simox::color::interpol::linear(0.5, simox::Color::green(), simox::Color::red()));
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(2), simox::Color::red());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(3), simox::Color::black());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(4), simox::Color::black());

    colorController.resetColors();
    colorController.setColorGradientBasedOnIndex(3, 2, simox::Color::green(), simox::Color::red());
    BOOST_CHECK_EQUAL(
        colorController.getColorAtIndex(0),
        simox::color::interpol::linear(2.f / 4.f, simox::Color::green(), simox::Color::red()));
    BOOST_CHECK_EQUAL(
        colorController.getColorAtIndex(1),
        simox::color::interpol::linear(3.f / 4.f, simox::Color::green(), simox::Color::red()));
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(2), simox::Color::red());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(3), simox::Color::green());
    BOOST_CHECK_EQUAL(
        colorController.getColorAtIndex(4),
        simox::color::interpol::linear(1.f / 4.f, simox::Color::green(), simox::Color::red()));

    colorController.resetColors();
    colorController.setColorGradientBasedOnIndex(4, 0, simox::Color::green(), simox::Color::red());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(0), simox::Color::red());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(1), simox::Color::black());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(2), simox::Color::black());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(3), simox::Color::black());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(4), simox::Color::green());

    colorController.resetColors();
    colorController.setColorGradientBasedOnIndex(3, 0, simox::Color::green(), simox::Color::red());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(0), simox::Color::red());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(1), simox::Color::black());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(2), simox::Color::black());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(3), simox::Color::green());
    BOOST_CHECK_EQUAL(
        colorController.getColorAtIndex(4),
        simox::color::interpol::linear(1.f / 2.f, simox::Color::green(), simox::Color::red()));
}

BOOST_AUTO_TEST_CASE(getColorAtAngleTest)
{
    auto is = getTestJson_withAngles();
    LedColorController colorController(is);

    colorController.setColorBasedOnIndex(0, 1, simox::Color::green());

    // The first two leds are now green. They are at angle 0.0f and 72.0f.
    // the previous led is at angle 288.0f and the next led is at angle 144.0f
    BOOST_CHECK_EQUAL(colorController.getColorAtAngle(0.0f), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtAngle(30.0f), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtAngle(71.0f), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtAngle(72.0f), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtAngle((144.f + 72.0f) / 2 - 1), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtAngle((144.f + 72.0f) / 2), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtAngle((144.f + 72.0f) / 2 + 1), simox::Color::black());

    colorController.resetColors();
    colorController.setColorBasedOnIndex(4, 1, simox::Color::green());

    BOOST_CHECK_EQUAL(colorController.getColorAtAngle((288.f + 216.0f) / 2), simox::Color::black());
    BOOST_CHECK_EQUAL(colorController.getColorAtAngle((288.f + 216.0f) / 2 + 1),
                      simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtAngle(288.0f), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtAngle(0.0f), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtAngle(72.0f), simox::Color::green());

    BOOST_CHECK_EQUAL(colorController.getColorAtAngle(5000.0f), colorController.getColorAtAngle(360.0f));
}

BOOST_AUTO_TEST_CASE(colorSettingBasedOnAngleTest)
{
    auto is = getTestJson_withAngles();
    LedColorController colorController(is);

    colorController.setColorBasedOnAngle(0.f, 90.f, simox::Color::green());

    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(0), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(1), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(2), simox::Color::black());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(3), simox::Color::black());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(4), simox::Color::black());

    BOOST_CHECK_EQUAL(colorController.getColorAtAngle(0.0f), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtAngle(71.0f), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtAngle(72.0f), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtAngle((144.f + 72.0f) / 2), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtAngle((144.f + 72.0f) / 2 + 1), simox::Color::black());

    colorController.resetColors();
    colorController.setColorBasedOnAngle(0.f, 0.f, simox::Color::green());

    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(4), simox::Color::black());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(0), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(1), simox::Color::black());

    BOOST_CHECK_EQUAL(colorController.getColorAtAngle(0.0f), simox::Color::green());

    colorController.resetColors();
    colorController.setColorBasedOnAngle(360.f, 0.f, simox::Color::green());

    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(4), simox::Color::black());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(0), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(1), simox::Color::black());

    BOOST_CHECK_EQUAL(colorController.getColorAtAngle(0.0f), simox::Color::green());

    colorController.resetColors();
    colorController.setColorBasedOnAngle(0.f, 360.f, simox::Color::green());

    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(4), simox::Color::black());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(0), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(1), simox::Color::black());

    BOOST_CHECK_EQUAL(colorController.getColorAtAngle(0.0f), simox::Color::green());

    colorController.resetColors();
    colorController.setColorBasedOnAngle(0.f, 359.9999f, simox::Color::green());

    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(0), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(1), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(2), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(3), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(4), simox::Color::green());
}

BOOST_AUTO_TEST_CASE(colorGradientSettingBasedOnAngleTest)
{
    auto is = getTestJson_withAngles();
    LedColorController colorController(is);

    colorController.setColorGradientBasedOnAngle(
        0.f, 72.f, simox::Color::green(), simox::Color::red());

    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(4), simox::Color::black());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(0), simox::Color::green());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(1), simox::Color::red());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(2), simox::Color::black());

    colorController.resetColors();
    colorController.setColorGradientBasedOnAngle(
        20.0f, 100.f, simox::Color::green(), simox::Color::red());

    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(0), simox::Color::black());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(1),
                      simox::color::interpol::linear((72.f - 20.0f) / (100.f - 20.f),
                                                     simox::Color::green(),
                                                     simox::Color::red()));
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(2), simox::Color::black());

    colorController.resetColors();
    colorController.setColorGradientBasedOnAngle(
        260.0f, 100.f, simox::Color::green(), simox::Color::red());

    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(3), simox::Color::black());
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(4),
                      simox::color::interpol::linear(
                          (288.f - 260.0f) / (200.f), simox::Color::green(), simox::Color::red()));
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(0),
                      simox::color::interpol::linear(
                          (360.f - 260.0f) / (200.f), simox::Color::green(), simox::Color::red()));
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(1),
                      simox::color::interpol::linear(((360.f + 72.f) - 260.0f) / (200.f),
                                                     simox::Color::green(),
                                                     simox::Color::red()));
    BOOST_CHECK_EQUAL(colorController.getColorAtIndex(3), simox::Color::black());
}
