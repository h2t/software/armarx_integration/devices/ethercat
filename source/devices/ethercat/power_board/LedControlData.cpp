#include "LedControlData.h"

namespace devices::ethercat::power_board
{
    LedControlData::LedControlData(std::uint16_t* target) : target{target}
    {
        ARMARX_TRACE;
        ARMARX_CHECK_NOT_NULL(target);

        ledRgbStripControl.fill(0);
    }

    void
    LedControlData::rtReadSensorValues(const IceUtil::Time& /*sensorValuesTimestamp*/,
                                       const IceUtil::Time& /*timeSinceLastIteration*/)
    {
    }

    void
    LedControlData::rtWriteTargetValues(const IceUtil::Time& /*sensorValuesTimestamp*/,
                                        const IceUtil::Time& /*timeSinceLastIteration*/)
    {
        std::copy(std::begin(ledRgbStripControl), std::end(ledRgbStripControl), target);
    }

    void
    LedControlData::setLedRgbStripControl(std::array<std::uint16_t, 40> value)
    {
        ledRgbStripControl = value;
    }
} // namespace devices::ethercat::power_board
