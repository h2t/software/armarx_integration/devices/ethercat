/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Jan Hausberg (jan dot hausberg at kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Slave.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <RobotAPI/components/units/RobotUnit/util/RtLogging.h>
#include <armarx/control/ethercat/Bus.h>

#include <devices/ethercat/common/h2t_devices.h>


namespace devices::ethercat::power_board
{
    Slave::Slave(const armarx::control::ethercat::SlaveIdentifier slaveIdentifier_) :
        SlaveInterfaceWithIO(slaveIdentifier_),
        errorDecoder(nullptr, nullptr),
        bus(armarx::control::ethercat::Bus::getBus())
    {
        setTag(this->slaveIdentifier.getName());
    }


    void
    Slave::execute()
    {
    }


    bool
    Slave::prepareForRun()
    {
        return true;
    }


    bool
    Slave::shutdown()
    {
        return true;
    }


    void
    Slave::doMappings()
    {
    }


    bool
    Slave::hasError()
    {
        // Check for all errors and return false if there are no errors
        bool hasError = false;

        // Print the reason for each error
        if (errorDecoder.batteryCommunicationErrorDetected())
        {
            //            ARMARX_RT_LOGF_ERROR("PowerBoard: battery communication error detected");
            //            hasError = true;
        }

        // Print these errors too
        hasError |= errorDecoder.failStatus1ErrorDetected(this, true) |
                    errorDecoder.failStatus2ErrorDetected(this, true) |
                    errorDecoder.failStatus3ErrorDetected(this, true) |
                    errorDecoder.leaderBatteryStatusErrorDetected(this, true) |
                    errorDecoder.leaderBMAlarm1ErrorDetected(this, true) |
                    errorDecoder.leaderBMAlarm2ErrorDetected(this, true);

        return hasError;
    }


    void
    Slave::prepareForSafeOp()
    {
        writeCoE();
    }


    void
    Slave::finishPreparingForSafeOp()
    {
    }


    void
    Slave::prepareForOp()
    {
        // Here the inputs and outputs pointers are initialized
        errorDecoder = ErrorDecoder(getOutputsPtr(), getInputsPtr());
    }


    void
    Slave::finishPreparingForOp()
    {
    }

    bool
    Slave::isEmergencyStopActive() const
    {
        // This needs to be implemented, if the slave supports emergency stop detection.
        return false;
    }

    bool
    Slave::recoverFromEmergencyStop()
    {
        return true;
    }

    bool
    Slave::isSlaveIdentifierAccepted(const armarx::control::ethercat::SlaveIdentifier& sid)
    {
        // TO DO
        // Product Code is different for each type of slave
        // The Slave Identifier can for example be obtained using EtherKITten
        const std::uint32_t correctProductCode = 0x2010;
        return sid.vendorID == common::H2TVendorId and sid.productCode == correctProductCode;
    }

    std::string
    Slave::getDefaultName()
    {
        return "PowerBoard";
    }

    // Read led rgb strip 1 count - only debugging function
    std::uint8_t
    Slave::readLedRgbStrip1Count()
    {
        bool success = bus.readSDOEntry(getSlaveNumber(), 0x8000, 1, ledRgbStrip1Count);
        if (!success)
        {
            SLAVE_ERROR(getSlaveIdentifier(), "Could not read ledRgbStrip1Count CoE entry");
        }
        return ledRgbStrip1Count;
    }

    // Read led rgb strip 2 count - only debugging function
    std::uint8_t
    Slave::readLedRgbStrip2Count()
    {
        bool success = bus.readSDOEntry(getSlaveNumber(), 0x8000, 2, ledRgbStrip2Count);
        if (!success)
        {
            SLAVE_ERROR(getSlaveIdentifier(), "Could not read ledRgbStrip2Count CoE entry");
        }
        return ledRgbStrip2Count;
    }

    // Read led rgb strip 1 global brightness - only debugging function
    std::uint8_t
    Slave::readLedRgbStrip1GlobalBrightness()
    {
        bool success = bus.readSDOEntry(getSlaveNumber(), 0x8000, 3, ledRgbStrip1GlobalBrightness);
        if (!success)
        {
            SLAVE_ERROR(getSlaveIdentifier(),
                        "Could not read ledRgbStrip1GlobalBrightness CoE entry");
        }
        return ledRgbStrip1GlobalBrightness;
    }

    // Read led rgb strip 2 global brightness - only debugging function
    std::uint8_t
    Slave::readLedRgbStrip2GlobalBrightness()
    {
        bool success = bus.readSDOEntry(getSlaveNumber(), 0x8000, 4, ledRgbStrip2GlobalBrightness);
        if (!success)
        {
            SLAVE_ERROR(getSlaveIdentifier(),
                        "Could not read ledRgbStrip2GlobalBrightness CoE entry");
        }
        return ledRgbStrip2GlobalBrightness;
    }

    void
    Slave::setLedRgbStrip1Count(std::uint8_t ledRgbStripCount)
    {
        ledRgbStrip1Count = ledRgbStripCount;
    }

    void
    Slave::setLedRgbStrip2Count(std::uint8_t ledRgbStripCount)
    {
        ledRgbStrip2Count = ledRgbStripCount;
    }

    void
    Slave::setLedRgbStrip1GlobalBrightness(std::uint8_t ledRgbStripGlobalBrightness)
    {
        ledRgbStrip1GlobalBrightness = ledRgbStripGlobalBrightness;
    }

    void
    Slave::setLedRgbStrip2GlobalBrightness(std::uint8_t ledRgbStripGlobalBrightness)
    {
        ledRgbStrip2GlobalBrightness = ledRgbStripGlobalBrightness;
    }

    void Slave::setConfig(SlaveConfig& config)
    {
        setLedRgbStrip1Count(config.ledStrip1Count);
        setLedRgbStrip2Count(config.ledStrip2Count);
        setLedRgbStrip1GlobalBrightness(config.ledStrip1GlobalBrightness);
        setLedRgbStrip2GlobalBrightness(config.ledStrip2GlobalBrightness);
    }

    void
    Slave::writeCoE()
    {
        // Write ledRgbStrip1Count
        bool success = bus.writeSDOEntry(getSlaveNumber(), 0x8000, 1, ledRgbStrip1Count);
        if (!success)
        {
            SLAVE_ERROR(getSlaveIdentifier(), "Could not write ledRgbStrip1Count CoE entry");
        }

        // Write ledRgbStrip2Count
        success = bus.writeSDOEntry(getSlaveNumber(), 0x8000, 2, ledRgbStrip2Count);
        if (!success)
        {
            SLAVE_ERROR(getSlaveIdentifier(), "Could not write ledRgbStrip2Count CoE entry");
        }

        // Write ledRgbStrip1GlobalBrightness
        success = bus.writeSDOEntry(getSlaveNumber(), 0x8000, 3, ledRgbStrip1GlobalBrightness);
        if (!success)
        {
            SLAVE_ERROR(getSlaveIdentifier(),
                        "Could not write ledRgbStrip1GlobalBrightness CoE entry");
        }

        // Write ledRgbStrip2GlobalBrightness
        success = bus.writeSDOEntry(getSlaveNumber(), 0x8000, 4, ledRgbStrip2GlobalBrightness);
        if (!success)
        {
            SLAVE_ERROR(getSlaveIdentifier(),
                        "Could not write ledRgbStrip2GlobalBrightness CoE entry");
        }
    }


} // namespace devices::ethercat::power_board
