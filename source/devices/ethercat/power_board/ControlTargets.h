#pragma once


#include <SimoxUtility/color/Color.h>

#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTargetBase.h>

#include <devices/ethercat/common/utility/introspection/DataFieldsInfo.h>

namespace devices::ethercat::power_board
{

    namespace ctrl_modes
    {
        static const std::string PowerBoardLedColor = "ControlMode_PowerBoardLEDColor";
    }


    class ControlTargetPowerBoardLedColor : public armarx::ControlTargetBase
    {

    public:
        std::array<simox::Color, 40> colors;

        ControlTargetPowerBoardLedColor() = default;
        ControlTargetPowerBoardLedColor(const ControlTargetPowerBoardLedColor&) = default;
        ControlTargetPowerBoardLedColor(ControlTargetPowerBoardLedColor&&) = default;
        ControlTargetPowerBoardLedColor(std::array<simox::Color, 40> colors)
        {
            this->colors = colors;
        }
        ControlTargetPowerBoardLedColor&
        operator=(const ControlTargetPowerBoardLedColor&) = default;
        ControlTargetPowerBoardLedColor& operator=(ControlTargetPowerBoardLedColor&&) = default;

        const std::string&
        getControlMode() const override
        {
            return ctrl_modes::PowerBoardLedColor;
        }

        void
        reset() override
        {
            colors.fill(simox::Color::black());
        }

        bool
        isValid() const override
        {
            return true;
        }

        DETAIL_ControlTargetBase_DEFAULT_METHOD_IMPLEMENTATION

            static ControlTargetInfo<ControlTargetPowerBoardLedColor>
            GetClassMemberInfo()
        {
            ControlTargetInfo<ControlTargetPowerBoardLedColor> cti;
            //            cti.addMemberVariable(&ControlTargetPowerBoardLedColor::colors, "colors");
            return cti;
        }
    };

} // namespace devices::ethercat::power_board
