armarx_add_ice_library(power_board_interfaces
    SLICE_FILES
        njoint_controller/LedStripsColorsInterface.ice
    DEPENDENCIES
        ArmarXCoreInterfaces
        ArmarXGuiInterfaces
        RobotAPIInterfaces
)

armarx_add_library(power_board
    SOURCES
        Device.cpp
        Data.cpp
        ErrorDecoder.cpp
        LedControlData.cpp
        Slave.cpp
        joint_controller/LedColor.cpp
        joint_controller/LedEmergencyStop.cpp
        joint_controller/LedStopMovement.cpp
        njoint_controller/LedStripsColors.cpp
        led_control/LedColorController.cpp
    HEADERS
        Config.h
        ControlTargets.h
        Device.h
        Data.h
        ErrorDecoder.h
        LedControlData.h
        Slave.h
        SlaveIO.h
        joint_controller/LedColor.h
        joint_controller/LedEmergencyStop.h
        joint_controller/LedStopMovement.h
        njoint_controller/LedStripsColors.h
        led_control/LedColorController.h
    DEPENDENCIES
        RobotAPIUnits
        armarx_control::ethercat
        armarx_control::joint_controller
        RemoteGui
        devices_ethercat::power_board_interfaces
        SimoxUtility
        devices_ethercat::common_utility
        ArVizCoin
    DEPENDENCIES_LEGACY
        SOEM
)

target_compile_options(power_board
    PRIVATE
        -Wall -Wextra -Wpedantic -Werror
)

if(CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    if(CMAKE_CXX_COMPILER_VERSION VERSION_GREATER_EQUAL "9")
        target_compile_options(power_board
                PRIVATE
                -Wno-error=address-of-packed-member
                )
    endif()
endif()

add_subdirectory(test)
