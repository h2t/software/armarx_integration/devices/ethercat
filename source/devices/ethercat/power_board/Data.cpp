/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Jan Hausberg (jan dot hausberg at kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Data.h"

namespace devices::ethercat::power_board
{
    Data::Data(const DataConfig& config,
               SlaveOut* outputs,
               SlaveIn* inputs) :
        outputs(outputs), inputs(inputs), errorDecoder(outputs, inputs)
    {
        ARMARX_CHECK_EXPRESSION(outputs);
        ARMARX_CHECK_EXPRESSION(inputs);

        // Initialize LinearConvertedValues
        current1.init(&(outputs->currentMonitor1), config.current);
        current2.init(&(outputs->currentMonitor2), config.current);
        voltage1.init(&(outputs->voltageMonitor1), config.voltage);
        voltage2.init(&(outputs->voltageMonitor2), config.voltage);

        fetTemperatureMax.init(&(outputs->fetTemperatureMax), config.batteryTemperature);
        averageCurrent.init(&(outputs->averageCurrent), config.batteryCurrent);
        chargingCurrent.init(&(outputs->chargingCurrent), config.batteryCurrent);
        voltageMax.init(&(outputs->voltageMax), config.batteryVoltage);
        designCapacity.init(&(outputs->designCapacity), config.batteryCapacity);
        fullChargeCapacity.init(&(outputs->fullChargeCapacity), config.batteryCapacity);
        remainingCapacity.init(&(outputs->remainingCapacity), config.batteryCapacity);
        bmVoltageMax.init(&(outputs->bmVoltageMax), config.batteryVoltage);
        bmVoltageMin.init(&(outputs->bmVoltageMin), config.batteryVoltage);
        bmTemperatureMax.init(&(outputs->bmTemperatureMax), config.batteryTemperature);
        bmTemperatureMin.init(&(outputs->bmTemperatureMin), config.batteryTemperature);
        bmCurrentMax.init(&(outputs->bmCurrentMax), config.batteryCurrent);
        bmCurrentMin.init(&(outputs->bmCurrentMin), config.batteryCurrent);
        cellVoltageMax.init(&(outputs->cellVoltageMax), config.batteryVoltage);
        cellVoltageMin.init(&(outputs->cellVoltageMin), config.batteryVoltage);

        ntcBeta = config.ntcBeta;
    }

    void
    Data::rtReadSensorValues(const IceUtil::Time& /*sensorValuesTimestamp*/,
                             const IceUtil::Time& /*timeSinceLastIteration*/)
    {
        batteryCommunicationError = errorDecoder.batteryCommunicationErrorDetected();
        batteryStatus = (0x6000 & outputs->statusBits) >> 13;
        mainUpdateRate = outputs->mainUpdateRate;
        temperature1Converted = convertTemperature(outputs->temperature1);
        temperature2Converted = convertTemperature(outputs->temperature2);

        current1.read();
        current2.read();
        voltage1.read();
        voltage2.read();

        fetTemperatureMax.read();
        averageCurrent.read();
        chargingCurrent.read();
        voltageMax.read();
        failStatus1 = errorDecoder.getFailStatus1();
        failStatus2 = errorDecoder.getFailStatus2();
        failStatus3 = errorDecoder.getFailStatus3();
        leaderBatteryStatus = errorDecoder.getLeaderBatteryStatus();
        designCapacity.read();
        fullChargeCapacity.read();
        remainingCapacity.read();
        leaderBmAlarm1 = errorDecoder.getLeaderBMAlarm1();
        leaderBmAlarm2 = errorDecoder.getLeaderBMAlarm2();
        bmVoltageMax.read();
        bmVoltageMin.read();
        bmTemperatureMax.read();
        bmTemperatureMin.read();
        bmCurrentMax.read();
        bmCurrentMin.read();
        cellVoltageMax.read();
        cellVoltageMin.read();
    }

    void
    Data::rtWriteTargetValues(const IceUtil::Time& /*sensorValuesTimestamp*/,
                              const IceUtil::Time& /*timeSinceLastIteration*/)
    {
    }

    bool
    Data::getBatteryCommunicationError()
    {
        return batteryCommunicationError;
    }

    std::uint8_t
    Data::getBatteryStatus()
    {
        return batteryStatus;
    }

    std::uint16_t
    Data::getMainUpdateRate()
    {
        return mainUpdateRate;
    }

    float
    Data::getTemperature1Converted()
    {
        return temperature1Converted;
    }

    float
    Data::getTemperature2Converted()
    {
        return temperature2Converted;
    }

    std::uint16_t
    Data::getCurrentMonitor1()
    {
        return current1.getRaw();
    }

    float
    Data::getCurrent1()
    {
        return current1.value;
    }

    std::uint16_t
    Data::getCurrentMonitor2()
    {
        return current2.getRaw();
    }

    float
    Data::getCurrent2()
    {
        return current2.value;
    }

    std::uint16_t
    Data::getVoltageMonitor1()
    {
        return voltage1.getRaw();
    }

    float
    Data::getVoltage1()
    {
        return voltage1.value;
    }

    std::uint16_t
    Data::getVoltageMonitor2()
    {
        return voltage2.getRaw();
    }

    float
    Data::getVoltage2()
    {
        return voltage2.value;
    }

    std::uint8_t
    Data::getNumberConnectedBm()
    {
        return outputs->numberConnectedBm;
    }

    std::uint8_t
    Data::getAbsoluteStateOfChargeMinInPct()
    {
        return outputs->absoluteStateOfChargeMin;
    }

    std::uint8_t
    Data::getRelativeStateOfChargeMinInPct()
    {
        return outputs->relativeStateOfChargeMin;
    }

    std::uint8_t
    Data::getStateOfHealthMinInPct()
    {
        return outputs->stateOfHealthMin;
    }

    float
    Data::getFetTemperatureMaxInDegC()
    {
        return fetTemperatureMax.value;
    }

    float
    Data::getAverageCurrentInA()
    {
        return -averageCurrent.value;
    }

    float
    Data::getChargingCurrentInA()
    {
        return chargingCurrent.value;
    }

    float
    Data::getVoltageMaxInV()
    {
        return voltageMax.value;
    }

    std::uint8_t
    Data::getFailStatus1()
    {
        return failStatus1;
    }

    std::uint8_t
    Data::getFailStatus2()
    {
        return failStatus2;
    }

    std::uint8_t
    Data::getFailStatus3()
    {
        return failStatus3;
    }

    std::uint8_t
    Data::getLeaderBatteryStatus()
    {
        return leaderBatteryStatus;
    }

    std::uint8_t
    Data::getLeaderBmAlarm1()
    {
        return leaderBmAlarm1;
    }

    std::uint8_t
    Data::getLeaderBmAlarm2()
    {
        return leaderBmAlarm2;
    }

    float
    Data::getDesignCapacity()
    {
        return designCapacity.value;
    }

    float
    Data::getFullChargeCapacity()
    {
        return fullChargeCapacity.value;
    }

    float
    Data::getRemainingCapacity()
    {
        return remainingCapacity.value;
    }

    float
    Data::getBmVoltageMax()
    {
        return bmVoltageMax.value;
    }

    float
    Data::getBmVoltageMin()
    {
        return bmVoltageMin.value;
    }

    float
    Data::getBmTemperatureMax()
    {
        return bmTemperatureMax.value;
    }

    float
    Data::getBmTemperatureMin()
    {
        return bmTemperatureMin.value;
    }

    std::uint8_t
    Data::getBmVoltageMaxId()
    {
        return outputs->bmVoltageMaxId;
    }

    std::uint8_t
    Data::getBmVoltageMinId()
    {
        return outputs->bmVoltageMinId;
    }

    std::uint8_t
    Data::getBmTemperatureMaxId()
    {
        return outputs->bmTemperatureMaxId;
    }

    std::uint8_t
    Data::getBmTemperatureMinId()
    {
        return outputs->bmTemperatureMinId;
    }

    float
    Data::getBmCurrentMax()
    {
        return -bmCurrentMax.value;
    }

    float
    Data::getBmCurrentMin()
    {
        return -bmCurrentMin.value;
    }

    float
    Data::getCellVoltageMax()
    {
        return cellVoltageMax.value;
    }

    float
    Data::getCellVoltageMin()
    {
        return cellVoltageMin.value;
    }

    std::uint8_t
    Data::getBmCurrentMaxId()
    {
        return outputs->bmCurrentMaxId;
    }

    std::uint8_t
    Data::getBmCurrentMinId()
    {
        return outputs->bmCurrentMinId;
    }

    std::uint8_t
    Data::getCellVoltageMaxId()
    {
        return outputs->cellVoltageMaxId;
    }

    std::uint8_t
    Data::getCellVoltageMinId()
    {
        return outputs->cellVoltageMinId;
    }

    std::uint16_t*
    Data::getLedRgbStrip1ControlPtr()
    {
        return &(inputs->ledRgbStrip1Control[0]);
    }

    std::uint16_t*
    Data::getLedRgbStrip2ControlPtr()
    {
        return &(inputs->ledRgbStrip2Control[0]);
    }


    float
    Data::convertTemperature(std::uint16_t temperature)
    {
        return 1.0f / ((std::log((static_cast<float>(2 << 13) / static_cast<float>(temperature)) -
                                 1.f) /
                        ntcBeta) +
                       (1.0f / 298.15f)) -
               273.15f;
    }

} // namespace devices::ethercat::power_board
