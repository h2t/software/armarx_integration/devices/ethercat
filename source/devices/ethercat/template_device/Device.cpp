/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Niklas Arlt (ujrwq at student dot kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Device.h"

#include <VirtualRobot/Robot.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/util/StringHelperTemplates.h>
#include <armarx/control/joint_controller/ControllerConfiguration.h>

#include "joint_controller/LedColor.h"
#include <devices/ethercat/common/h2t_devices.h>
#include <devices/ethercat/template_device/Slave.h>


namespace devices::ethercat::template_device
{

    Device::Device(armarx::control::hardware_config::DeviceConfig& hwConfig,
                   const VirtualRobot::RobotPtr& robot) :
        DeviceBase(hwConfig.getName()),
        ControlDevice(hwConfig.getName()),
        SensorDevice(hwConfig.getName()),
        DeviceInterface(hwConfig.getName()),
        robotNode(robot->getRobotNode(hwConfig.getName())),
        dataPtr(nullptr),
        // note that the names defined in the getters of hwConfig correspond to xml tags in the HardwareConfig
        slaveIdentifier(hwConfig.getSlaveConfig("Slave1").getIdentifier()),
        slavePtr(nullptr),
        slaveConfig(hwConfig.getSlaveConfig("Slave1"))
    {
        using namespace armarx::control::joint_controller;

        ARMARX_CHECK_NOT_NULL(robotNode);

        // No controller configuration that has to be initialized
        // This would be a good place to do so

        enabled = hwConfig.getBool("Enabled");
        ARMARX_INFO << "TemplateDevice " << getDeviceName() << " enabled: " << enabled;


        // We require that a Led subdevice is defined
        hwConfig.getSubDeviceConfigsWithType("Led");
    }


    armarx::control::ethercat::DeviceInterface::TryAssignResult
    Device::tryAssign(armarx::control::ethercat::SlaveInterface& slave)
    {
        namespace ecat = armarx::control::ethercat;
        using Result = DeviceInterface::TryAssignResult;

        Result result = ecat::tryAssignUsingSerialNo(slave, slavePtr, slaveIdentifier.serialNumber);
        if (result != Result::unknown)
        {
            return result;
        }

        return result;
    }


    armarx::control::ethercat::DeviceInterface::AllAssignedResult
    Device::onAllAssigned()
    {
        using Result = DeviceInterface::AllAssignedResult;

        if (slavePtr)
        {
            // Initialize Slave1
            {
                // note that the nodes defined here correspond to xml tags in the HardwareConfig
                std::uint8_t ledStripCount = slaveConfig.ledCount;
                slavePtr->setLedStripCount(ledStripCount);
            }

            // Slave2 does not have CoE entries that have to be filled here

            return Result::ok;
        }

        return Result::slavesMissing;
    }

    std::string
    Device::getClassName() const
    {
        return "TemplateDevice";
    }

    void
    Device::postSwitchToSafeOp()
    {
        if (!dataPtr)
        {
            dataPtr = std::make_shared<Data>(slaveConfig, slavePtr->getOutputsPtr(), slavePtr->getInputsPtr());
            // with data set we can create the joint controllers ...
            ledColorController = std::make_shared<joint_controller::LedColorController>(dataPtr);
            ledStopMovementController =
                std::make_shared<joint_controller::LedStopMovementController>();
            ledEmergencyController =
                std::make_shared<joint_controller::LedEmergencyStopController>();

            // Add all joint controllers
            addJointController(ledEmergencyController.get());
            addJointController(ledStopMovementController.get());
            addJointController(ledColorController.get());
        }
        ARMARX_CHECK_NOT_NULL(dataPtr);
    }

    void
    Device::postSwitchToOp()
    {
    }


    void
    Device::updateSensorValueStruct()
    {
        sensorValue.temperature = dataPtr->getTemperature();
        sensorValue.anotherSensorValue = dataPtr->getAnotherSensorValue();
        sensorValue.sensorValueWithNonLinearConversion =
            dataPtr->getSensorValueWithNonLinearConversion();
    }


    DataPtr
    Device::getData() const
    {
        if (dataPtr == nullptr)
        {
            throw armarx::LocalException(
                "Template Device has no data set, call initData(...) before");
        }

        return dataPtr;
    }


    armarx::control::ethercat::SlaveIdentifier
    Device::getSlaveIdentifier() const
    {
        return slaveIdentifier;
    }


    void
    Device::rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                               const IceUtil::Time& timeSinceLastIteration)
    {
        // Read new data from slaves
        if (dataPtr == nullptr)
        {
            throw armarx::LocalException(
                "TemplateDevice has no data set, call initData(...) before");
        }
        dataPtr->rtReadSensorValues(sensorValuesTimestamp, timeSinceLastIteration);

        // Check for invalid / critical data and print messages on the log decribing the problem
        if (dataPtr->getTemperature() > 70)
        {
            ARMARX_WARNING << deactivateSpam(10) << "Temperature is high! "
                           << dataPtr->getTemperature() << " °C";
        }

        updateSensorValueStruct();
    }


    void
    Device::rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration)
    {
        if (dataPtr == nullptr)
        {
            throw armarx::LocalException(
                "TemplateDevice has no data set, call initData(...) before");
        }
        dataPtr->rtWriteTargetValues(sensorValuesTimestamp, timeSinceLastIteration);
    }


    const armarx::SensorValueBase*
    Device::getSensorValue() const
    { // Here the SensorValue struct is given to armarx
        return &sensorValue;
    }


    std::uint16_t
    Device::getSlaveNumber() const
    {
        if (slavePtr == nullptr)
        {
            throw armarx::LocalException(
                "Template_Device isn't initialized, call init(...) before");
        }
        return slavePtr->getSlaveNumber();
    }


} // namespace devices::ethercat::template_device
