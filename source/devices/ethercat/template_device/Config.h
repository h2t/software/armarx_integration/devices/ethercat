#pragma once

#include <armarx/control/hardware_config/SlaveConfig.h>

namespace devices::ethercat::template_device
{
    namespace hwconfig = armarx::control::hardware_config;

    /**
     * @brief The Slave1DataConfig struct represents the HardwareConfig data used in the Data class
     */
    struct Slave1DataConfig
    {
        Slave1DataConfig(hwconfig::SlaveConfig& hwConfig)
            : anotherSensor(hwConfig.getLinearConfig("anotherSensor"))
        {
        }

        hwconfig::types::LinearConfig anotherSensor;
    };

    /**
     * @brief The Slave1SlaveConfig struct represents the HardwareConfig data used in the Slave class
     */
    struct Slave1SlaveConfig
    {
        Slave1SlaveConfig(hwconfig::SlaveConfig& hwConfig)
            : ledCount(static_cast<uint8_t>(hwConfig.getUint("ledCount")))
        {
        }

        std::uint8_t ledCount;
    };

    /**
     * @brief The Slave1CompleteConfig struct contains all configuration options that are required
     * and allowed in the HardwareConfig of Slave1. It can be devided into a SlaveConfig part and
     * a DataConfig part.
     */
    struct Slave1CompleteConfig : public Slave1DataConfig, public Slave1SlaveConfig
    {
        Slave1CompleteConfig(hwconfig::SlaveConfig& hwConfig)
            : Slave1DataConfig(hwConfig),
              Slave1SlaveConfig(hwConfig)
        {
        }
    };
}
