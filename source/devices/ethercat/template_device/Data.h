/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Niklas Arlt (ujrwq at student dot kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <ArmarXCore/core/rapidxml/wrapper/DefaultRapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValueIMU.h>
#include <armarx/control/ethercat/DataInterface.h>

#include <devices/ethercat/template_device/Config.h>
#include <devices/ethercat/template_device/ErrorDecoder.h>
#include <devices/ethercat/template_device/SlaveIO.h>

namespace devices::ethercat::template_device
{
    /**
     * @brief This class contains all sensor data, that should be given to armarx.
     * If, for example, some data should be displayed in the gui, then it is necessary that it
     * is included in this class.
     */
    class SensorValue : virtual public armarx::SensorValueBase
    /*
     * Inherit from each class in RobotAPI/components/units/RobotUnit/SensorValues that makes sense.
     * E.g. for a sensor actor unit armarx::SensorValue1DoFRealActuatorWithStatus
     * In that case there are already attributes defined. Do not forget to update them as well!
     */
    {
    public:
        SensorValue() = default;

        SensorValue(const SensorValue&) = default;

        SensorValue& operator=(const SensorValue&) = default;

        SensorValue(SensorValue&& o)
        {
            *this = o;
        }

        SensorValue&
        operator=(SensorValue&& o)
        {
            *this = o;
            return *this;
        }

        DETAIL_SensorValueBase_DEFAULT_METHOD_IMPLEMENTATION

            // Sensor values
            std::int16_t temperature;
        float anotherSensorValue;
        double sensorValueWithNonLinearConversion;

        static SensorValueInfo<SensorValue>
        GetClassMemberInfo()
        {
            SensorValueInfo<SensorValue> svi;

            // Add members of superclasses that are not SensorValueBase e.g.
            // svi.addBaseClass<armarx::SensorValue1DoFMotorPWM>();

            // Add new members
            svi.addMemberVariable(&SensorValue::temperature, "temperature");
            svi.addMemberVariable(&SensorValue::anotherSensorValue, "anotherSensorValue");
            svi.addMemberVariable(&SensorValue::sensorValueWithNonLinearConversion,
                                  "sensorValueWithNonLinearConversion");

            return svi;
        }
    };


    class Data : public armarx::control::ethercat::DataInterface
    {
    public:
        Data(const Slave1DataConfig& config,
             SlaveOut* outputs,
             SlaveIn* inputs);

        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration) override;
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                 const IceUtil::Time& timeSinceLastIteration) override;

        /**
         * @brief Describes the frequency of the main loop on the sensor-actor-pcb in Hz.
         * This frequency does not necessarily correspond to the frequency of other sensor values.
         */
        std::uint16_t getMainUpdateRate();

        std::int16_t getTemperature();

        float getAnotherSensorValue();

        double getSensorValueWithNonLinearConversion();

        void setRedLedIntensity(std::uint8_t redIntensity);

        void setGreenLedIntensity(std::uint8_t greenIntensity);

        void setBlueLedIntensity(std::uint8_t blueIntensity);

    private:
        SlaveOut* outputs;
        SlaveIn* inputs;

        ErrorDecoder errorDecoder;

        // Normal Slave inputs

        // temperature does not need a conversion and therefore does not need an attribute

        // std::uint8_t is the type of the raw data in SlaveIO
        armarx::control::ethercat::LinearConvertedValue<std::uint8_t> anotherSensorValue;

        std::uint16_t mainUpdateRate;

        double sensorValueWithNonLinearConversion;

        // Slave outputs
        // Cache values until they are written on the bus in rtWriteTargetValues
        std::uint8_t red = 0;
        std::uint8_t green = 0;
        std::uint8_t blue = 0;

        void readAndConvertSensorValueWithNonLinearConversion();
    };

    using DataPtr = std::shared_ptr<Data>;
} // namespace devices::ethercat::template_device
