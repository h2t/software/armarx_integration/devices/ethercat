/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Niklas Arlt (ujrwq at student dot kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Data.h"

namespace devices::ethercat::template_device
{
    Data::Data(const Slave1DataConfig& config,
               SlaveOut* outputs,
               SlaveIn* inputs) :
        outputs(outputs), inputs(inputs), errorDecoder(outputs, inputs)
    {
        ARMARX_CHECK_EXPRESSION(outputs);
        ARMARX_CHECK_EXPRESSION(inputs);

        // Initialize LinearConvertedValues
        /*
         * LinearConvertedValue caches converted values and uses the attributes a and offset for
         * the conversion.
         */
        anotherSensorValue.init(&(outputs->anotherSensorValue),
                                config.anotherSensor);
    }

    void
    Data::rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                             const IceUtil::Time& timeSinceLastIteration)
    {
        // Convert all values that should be converted and cache the results

        // For LinearConvertedValues simply call read().
        anotherSensorValue.read();

        // No conversion needed
        mainUpdateRate = outputs->MainUpdateRate;

        // SomeSensorValue also does not need a conversion but is not cached
    }

    void
    Data::rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                              const IceUtil::Time& timeSinceLastIteration)
    {
        /* Write new values that have been set on the bus.
         * Ignore unused variables warning. Often the parameters to this function are not used.
         * timeSinceLastIteration can be used to calculate the current rate of change of a value.
         */
        inputs->RedChannel = red;
        inputs->GreenChannel = green;
        inputs->BlueChannel = blue;
    }

    std::int16_t
    Data::getTemperature()
    {
        return outputs->temperature;
    }


    std::uint16_t
    Data::getMainUpdateRate()
    {
        return mainUpdateRate;
    }


    float
    Data::getAnotherSensorValue()
    {
        // For LinearConvertedValue the converted float value can be retrieved like this
        return anotherSensorValue.value;
    }

    double
    Data::getSensorValueWithNonLinearConversion()
    {
        return sensorValueWithNonLinearConversion;
    }


    void
    Data::setRedLedIntensity(std::uint8_t redIntensity)
    {
        this->red = redIntensity;
    }

    void
    Data::setGreenLedIntensity(std::uint8_t greenIntensity)
    {
        this->green = greenIntensity;
    }

    void
    Data::setBlueLedIntensity(std::uint8_t blueIntensity)
    {
        this->blue = blueIntensity;
    }

    void
    Data::readAndConvertSensorValueWithNonLinearConversion()
    {
        // Convert the value and cache the result
        sensorValueWithNonLinearConversion =
            300.0 + std::log(outputs->sensorValueWithNonLinearConversion);
    }

} // namespace devices::ethercat::template_device
