/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Niklas Arlt (ujrwq at student dot kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <VirtualRobot/Nodes/RobotNode.h>

#include <RobotAPI/components/units/RobotUnit/BasicControllers.h>
#include <RobotAPI/components/units/RobotUnit/Devices/ControlDevice.h>
#include <RobotAPI/components/units/RobotUnit/Devices/SensorDevice.h>
#include <armarx/control/ethercat/DeviceInterface.h>
#include <armarx/control/ethercat/SlaveIdentifier.h>

#include <devices/ethercat/template_device/Data.h>
#include <devices/ethercat/template_device/Slave.h>
#include <devices/ethercat/template_device/joint_controller/LedColor.h>
#include <devices/ethercat/template_device/joint_controller/LedEmergencyStop.h>
#include <devices/ethercat/template_device/joint_controller/LedStopMovement.h>


namespace devices::ethercat::template_device
{

    namespace joint_controller
    {
        using LedColorControllerPtr = std::shared_ptr<class LedColorController>;
    } // namespace joint_controller


    /*
     * This class inherits from ControlDevice because there are Slave inputs on some
     * slaves (to control the leds) and inherits from SensorDevice because there are Slave
     * outputs.
     * It is also necessary to inherit from DeviceInterface for all devices.
     *
     * This device only provides a simple controller for rgb leds.
     * In more complex devices, like for example sensor-actor-units there would be other
     * (and more) controllers. It can be useful to copy controllers from similar devices.
     */
    class Device :
        public armarx::ControlDevice,
        public armarx::SensorDevice,
        public armarx::control::ethercat::DeviceInterface
    {

    public:
        Device(armarx::control::hardware_config::DeviceConfig& hwConfig,
               VirtualRobot::RobotPtr const& robot);
        ~Device() override
        {
        }

        DeviceInterface::TryAssignResult
        tryAssign(armarx::control::ethercat::SlaveInterface& slave) override;

        DeviceInterface::AllAssignedResult onAllAssigned() override;

        std::string getClassName() const override;

        void postSwitchToSafeOp() override;

        void postSwitchToOp() override;

        // SensorDevice interface
        const armarx::SensorValueBase* getSensorValue() const override;
        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration) override;

        // ControlDevice interface
        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                 const IceUtil::Time& timeSinceLastIteration) override;

        DataPtr getData() const;

        armarx::control::ethercat::SlaveIdentifier getSlaveIdentifier() const;

        std::uint16_t getSlaveNumber() const;

        bool
        isEnabled() const
        {
            return this->enabled;
        }

    private:
        void updateSensorValueStruct();

        VirtualRobot::RobotNodePtr robotNode;

        ///The data and target object
        DataPtr dataPtr;
        /// The data object for copying to non-rt part
        SensorValue sensorValue;

        /// Whether or not this device should be started on the bus
        bool enabled = false;

        ///bus devices - serial form config and pointer to actual bus slave
        const armarx::control::ethercat::SlaveIdentifier slaveIdentifier;

        /// Slave pointers
        template_device::Slave* slavePtr;

        Slave1CompleteConfig slaveConfig;

        ///the joint controller
        joint_controller::LedColorControllerPtr ledColorController;
        std::shared_ptr<joint_controller::LedEmergencyStopController> ledEmergencyController;
        std::shared_ptr<joint_controller::LedStopMovementController> ledStopMovementController;
    };

} // namespace devices::ethercat::template_device
