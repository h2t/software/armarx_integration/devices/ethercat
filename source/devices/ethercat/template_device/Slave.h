/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Niklas Arlt (ujrwq at student dot kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <armarx/control/ethercat/SlaveInterface.h>

#include <devices/ethercat/template_device/ErrorDecoder.h>
#include <devices/ethercat/template_device/SlaveIO.h>

namespace devices::ethercat::template_device
{
    /*
     * In this example we take a look at a very simple slave.
     * The slave has the pdo data that is specified in SlaveIO
     * and only one CoE entry: 'ledStripCount'.
     */

    class Slave : public armarx::control::ethercat::SlaveInterfaceWithIO<SlaveIn, SlaveOut>
    {

    public:
        Slave(const armarx::control::ethercat::SlaveIdentifier slaveIdentifier);

        /**
         * @see AbstractSlave::doMappings
         */
        void doMappings() override;

        void prepareForSafeOp() override;
        void finishPreparingForSafeOp() override;

        void prepareForOp() override;
        void finishPreparingForOp() override;

        /**
         * @see AbstractSlave::prepare()
         */
        bool prepareForRun() override;

        /**
         * @see AbstractSlave::execute()
         */
        void execute() override;

        /**
         * @see AbstractSlave::shutdown()
         */
        bool shutdown() override;

        /**
         * @see AbstractSlave::hasError()
         * @return
         */
        bool hasError() override;

        bool isEmergencyStopActive() const override;
        bool recoverFromEmergencyStop() override;

        static bool
        isSlaveIdentifierAccepted(const armarx::control::ethercat::SlaveIdentifier& sid);

        static std::string getDefaultName();

        /**
         * @brief Read the CoE value over the ethercat bus.
         * Do not call during operation because it can take more than 1ms.
         */
        std::uint8_t readLedStripCount();

        /**
         * @brief Set ledStripCount that is applied when switching to safe-op
         */
        void setLedStripCount(std::uint8_t ledStripCount);

    private:
        ErrorDecoder errorDecoder;
        armarx::control::ethercat::Bus& bus;

        /*
         * Keep a local copy of CoE values that are set using the setter.
         * These CoE entries are sent over ethercat in 'prepareForSafeOp()'.
         * The type could also be optional in case the default value of the slave
         * should not be overwritten.
         */
        std::uint8_t ledStripCount;

        // Method to bundle CoE writes
        void writeCoE();
    };

} // namespace devices::ethercat::template_device
