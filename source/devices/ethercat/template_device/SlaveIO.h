/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Niklas Arlt (ujrwq at student dot kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <cstdint>


namespace devices::ethercat::template_device
{
    /*
     * Here belong the PDO mappings for ethercat.
     * The attribute packed is important.
     */

    /**
     * @brief PDO mapping sensorB->master
     */
    struct SlaveOut
    {
        // Object Entry 0x6000
        // Board Diagnostics
        std::uint16_t StatusBits;
        std::uint16_t MainUpdateRate;

        // Sensors
        std::int16_t temperature;
        std::uint8_t anotherSensorValue;
        std::uint8_t pad1; // Don't forget the paddings

        std::uint32_t sensorValueWithNonLinearConversion;
    } __attribute__((__packed__));


    /**
     * @brief PDO mapping master->sensorB
     */
    struct SlaveIn
    {
        // LED control
        std::uint8_t RedChannel;
        std::uint8_t GreenChannel;
        std::uint8_t BlueChannel;
        std::uint8_t pad1; // This struct also needs padding
    } __attribute__((__packed__));

} // namespace devices::ethercat::template_device
