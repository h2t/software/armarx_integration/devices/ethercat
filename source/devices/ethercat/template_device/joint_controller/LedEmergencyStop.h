/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Niklas Arlt (ujrwq at student dot kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>

namespace devices::ethercat::template_device::joint_controller
{
    /**
     * @brief Necessary EmergencyStopController for the Led
     */
    class LedEmergencyStopController : public armarx::JointController
    {
    public:
        LedEmergencyStopController();

    private:
        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;
        armarx::ControlTargetBase* getControlTarget() override;

        void rtPreActivateController() override;

    private:
        armarx::DummyControlTargetEmergencyStop target;
    };

    using LedEmergencyStopControllerPtr = std::shared_ptr<LedEmergencyStopController>;

} // namespace devices::ethercat::template_device::joint_controller
