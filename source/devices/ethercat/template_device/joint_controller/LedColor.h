/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Niklas Arlt (ujrwq at student dot kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>

#include "../ControlTargets.h"
#include "../Data.h"

namespace devices::ethercat::template_device::joint_controller
{
    /**
     * @brief This is a very simple controller taken from head_board_armar7de (and modified).
     * It allows setting a rgb color for a led (strip).
     */
    class LedColorController : public armarx::JointController
    {
    public:
        LedColorController(DataPtr data);
        armarx::ControlTargetBase* getControlTarget() override;
        void rtRun(const IceUtil::Time& sensorValuesTimestamp,
                   const IceUtil::Time& timeSinceLastIteration) override;

    private:
        DataPtr data;
        ControlTargetLedColor target;
    };
    using LedColorControllerPtr = std::shared_ptr<LedColorController>;
} // namespace devices::ethercat::template_device::joint_controller
