/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Niklas Arlt (ujrwq at student dot kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "LedColor.h"

namespace devices::ethercat::template_device::joint_controller
{
    LedColorController::LedColorController(DataPtr data) : data(data)
    {
        ARMARX_CHECK_NOT_NULL(data);
    }

    armarx::ControlTargetBase*
    LedColorController::getControlTarget()
    {
        return &target;
    }

    void
    LedColorController::rtRun(const IceUtil::Time&, const IceUtil::Time&)
    {
        data->setRedLedIntensity(target.r);
        data->setGreenLedIntensity(target.g);
        data->setBlueLedIntensity(target.b);
    }
} // namespace devices::ethercat::template_device::joint_controller
