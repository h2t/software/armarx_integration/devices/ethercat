/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Niklas Arlt (ujrwq at student dot kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Slave.h"

#include <ArmarXCore/core/logging/Logging.h>
#include <RobotAPI/components/units/RobotUnit/util/RtLogging.h>
#include <armarx/control/ethercat/Bus.h>

#include <devices/ethercat/common/h2t_devices.h>


namespace devices::ethercat::template_device
{
    Slave::Slave(const armarx::control::ethercat::SlaveIdentifier slaveIdentifier_) :
        SlaveInterfaceWithIO(slaveIdentifier_),
        errorDecoder(nullptr, nullptr),
        bus(armarx::control::ethercat::Bus::getBus())
    {
        setTag(this->slaveIdentifier.getName());
    }


    void
    Slave::execute()
    {
    }


    bool
    Slave::prepareForRun()
    {
        return true;
    }


    bool
    Slave::shutdown()
    {
        return true;
    }


    void
    Slave::doMappings()
    {
    }


    bool
    Slave::hasError()
    {
        // Check for all errors and return false iff there are no errors
        bool hasError = false;

        // Print the reason for each error
        if (errorDecoder.someErrorDetected())
        {
            ARMARX_RT_LOGF_ERROR("Template Device slave 1: some error detected");
            hasError = true;
        }

        // Print these errors too
        hasError |= errorDecoder.relativeEncoderErrorDetected(true);

        return hasError;
    }


    void
    Slave::prepareForSafeOp()
    {
        writeCoE();
    }


    void
    Slave::finishPreparingForSafeOp()
    {
    }


    void
    Slave::prepareForOp()
    {
        // Here the inputs and outputs pointers are initialized
        errorDecoder = ErrorDecoder(getOutputsPtr(), getInputsPtr());
    }


    void
    Slave::finishPreparingForOp()
    {
    }

    bool
    Slave::isEmergencyStopActive() const
    {
        // This needs to be implemented, if the slave supports emergency stop detection.
        return false;
    }

    bool
    Slave::recoverFromEmergencyStop()
    {
        return true;
    }

    bool
    Slave::isSlaveIdentifierAccepted(const armarx::control::ethercat::SlaveIdentifier& sid)
    {
        // Product Code is different for each type of slave
        // The Slave Identifier can for example be obtained using EtherKITten
        const std::uint32_t correctProductCode = 0x4500;
        return sid.vendorID == common::H2TVendorId and sid.productCode == correctProductCode;
    }

    std::string
    Slave::getDefaultName()
    {
        return "TemplateDeviceSlave1";
    }

    // Read led strip count - only debugging function
    std::uint8_t
    Slave::readLedStripCount()
    {
        bool success = bus.readSDOEntry(getSlaveNumber(), 0x8000, 7, ledStripCount);
        if (!success)
        {
            ARMARX_ERROR << "Template Device slave 1: Could not read LedStripCount CoE entry"
                         << std::endl;
        }
        return ledStripCount;
    }

    void
    Slave::setLedStripCount(std::uint8_t ledStripCount)
    {
        this->ledStripCount = ledStripCount;
    }

    void
    Slave::writeCoE()
    {
        // Write ledStripCount
        bool success = bus.writeSDOEntry(getSlaveNumber(), 0x8000, 7, ledStripCount);
        if (!success)
        {
            ARMARX_ERROR << "Template Device slave 1: Could not write LedStripCount CoE entry"
                         << std::endl;
        }
    }


} // namespace devices::ethercat::template_device
