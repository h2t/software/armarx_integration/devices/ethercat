/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Niklas Arlt (ujrwq at student dot kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "ErrorDecoder.h"

#include <RobotAPI/components/units/RobotUnit/util/RtLogging.h>

namespace devices::ethercat::template_device
{
    ErrorDecoder::ErrorDecoder(SlaveOut* outputs, SlaveIn* inputs) :
        outputs(outputs), inputs(inputs)
    {
    }

    bool
    ErrorDecoder::someErrorDetected() const
    {
        return (0x8000 & outputs->StatusBits) ? true : false;
    }

    RelativeEncoderStatus
    ErrorDecoder::getEncoderStatus() const
    {
        std::uint16_t relativeEncStatus = (0x3000 & outputs->StatusBits) >> 12;
        return static_cast<RelativeEncoderStatus>(relativeEncStatus);
    }

    bool
    ErrorDecoder::relativeEncoderErrorDetected(bool printError) const
    {
        RelativeEncoderStatus status = getEncoderStatus();

        if (printError)
        {
            switch (status)
            {
                case RelativeEncoderStatus::EncoderOK:
                    return false;
                case RelativeEncoderStatus::EncoderHasNoPower:
                    ARMARX_RT_LOGF_ERROR("Template Device - relative encoder has no power");
                    return true;
                case RelativeEncoderStatus::EncoderHasNoSignal:
                    ARMARX_RT_LOGF_ERROR("Template Device - relative encoder has no signal");
                    return true;
                case RelativeEncoderStatus::SpeedIsTooHigh:
                    ARMARX_RT_LOGF_ERROR("Template Device - relative encoder: speed is too high");
                    return true;
            }
        }

        return status != RelativeEncoderStatus::EncoderOK;
    }

} // namespace devices::ethercat::template_device
