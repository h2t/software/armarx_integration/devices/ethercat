/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2022, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    armarx_integration::devices::ethercat
 * @author     Niklas Arlt (ujrwq at student dot kit dot edu)
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once


#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTargetBase.h>


namespace devices::ethercat::template_device
{
    /**
     * This namespace contains all control mode strings for all ControlTargets
     * that are not already defined in RobotAPI.
     */
    namespace ctrl_modes
    {
        /* specify control modes that are not already included in
         * RobotAPI/components/units/RobotUnit/ControlTargets
         */
        static const std::string LedColor = "ControlMode_LEDColor";
    } // namespace ctrl_modes

    /**
     * @brief ControlTarget for the led color
     */
    class ControlTargetLedColor : public armarx::ControlTargetBase
    {

    public:
        std::uint8_t r;
        std::uint8_t g;
        std::uint8_t b;
        ControlTargetLedColor() = default;
        ControlTargetLedColor(const ControlTargetLedColor&) = default;
        ControlTargetLedColor(ControlTargetLedColor&&) = default;
        ControlTargetLedColor(std::uint8_t r, std::uint8_t g, std::uint8_t b)
        {
            this->r = r;
            this->g = g;
            this->b = b;
        }
        ControlTargetLedColor& operator=(const ControlTargetLedColor&) = default;
        ControlTargetLedColor& operator=(ControlTargetLedColor&&) = default;

        const std::string&
        getControlMode() const override
        {
            return ctrl_modes::LedColor;
        }

        void
        reset() override
        {
            r = 0;
            g = 0;
            b = 0;
        }

        bool
        isValid() const override
        {
            return true;
        }

        DETAIL_ControlTargetBase_DEFAULT_METHOD_IMPLEMENTATION

            static ControlTargetInfo<ControlTargetLedColor>
            GetClassMemberInfo()
        {
            ControlTargetInfo<ControlTargetLedColor> cti;
            cti.addMemberVariable(&ControlTargetLedColor::r, "r");
            cti.addMemberVariable(&ControlTargetLedColor::g, "g");
            cti.addMemberVariable(&ControlTargetLedColor::b, "b");
            return cti;
        }
    };

} // namespace devices::ethercat::template_device
