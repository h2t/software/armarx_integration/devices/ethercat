/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <atomic>

#include <armarx/control/hardware_config/Config.h>

#include <RobotAPI/libraries/core/PIDController.h>
#include <RobotAPI/components/units/RobotUnit/BasicControllers.h>


namespace devices::ethercat::head::armar6
{

    using PWMVelocityControllerConfigurationPtr = std::shared_ptr<class PWMVelocityControllerConfiguration>;
    using PWMVelocityControllerConfigurationCPtr = std::shared_ptr<const PWMVelocityControllerConfiguration>;

    namespace hwconfig = armarx::control::hardware_config;

    class PWMVelocityControllerConfiguration : public armarx::RampedAccelerationVelocityControllerConfiguration
    {

    public:

        PWMVelocityControllerConfiguration() = default;
        static PWMVelocityControllerConfigurationCPtr CreatePWMVelocityControllerConfigData(hwconfig::Config& hwConfig);
        float p;
        float i;
        float d;
        float maxIntegral;
        float feedforwardVelocityToPWMFactor;
        float feedforwardTorqueToPWMFactor;
        float PWMDeadzone;
        float velocityUpdatePercent;
        float conditionalIntegralErrorTreshold;
        bool feedForwardMode;

    };


    class PWMVelocityController
    {

    public:

        PWMVelocityController(PWMVelocityControllerConfigurationCPtr velocityControllerConfigDataPtr);
        double run(IceUtil::Time const& deltaT, double currentVelocity, double targetVelocity, double gravityTorque);
        void reset(double currentVelocity);

        PWMVelocityControllerConfigurationCPtr config;

        armarx::PIDControllerPtr pid;
        std::atomic<double> lastActualVelocity;

    };

}  // namespace devices::ethercat::head_board::v1
