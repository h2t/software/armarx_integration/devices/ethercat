/*
     * This file is part of ArmarX.
     *
     * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
     *
     * ArmarX is free software; you can redistribute it and/or modify
     * it under the terms of the GNU General Public License version 2 as
     * published by the Free Software Foundation.
     *
     * ArmarX is distributed in the hope that it will be useful, but
     * WITHOUT ANY WARRANTY; without even the implied warranty of
     * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
     * GNU General Public License for more details.
     *
     * You should have received a copy of the GNU General Public License
     * along with this program. If not, see <http://www.gnu.org/licenses/>.
     *
     * @package    ArmarX
     * @author     Mirko Waechter( mirko.waechter at kit dot edu)
     * @date       2018
     * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
     *             GNU General Public License
     */


#pragma once


#include <RobotAPI/components/units/RobotUnit/Devices/ControlDevice.h>
#include <RobotAPI/components/units/RobotUnit/Devices/SensorDevice.h>
#include <armarx/control/ethercat/DeviceInterface.h>
#include <armarx/control/ethercat/SlaveIdentifier.h>

#include <devices/ethercat/common/imagine_board/Data.h>
#include <devices/ethercat/common/imagine_board/SensorValue.h>

#include <devices/ethercat/head/armar6/Config.h>

namespace devices::ethercat::common::imagine_board
{
    class Data;
    class SensorActorData;
    class Slave;
} // namespace devices::ethercat::common::imagine_board


namespace devices::ethercat::head::armar6::joint_controller
{
    using StopMovementControllerPtr = std::shared_ptr<class StopMovementController>;
    using EmergencyStopControllerPtr = std::shared_ptr<class EmergencyStopController>;
    using PWMVelocityControllerPtr = std::shared_ptr<class PWMVelocityController>;
    using PWMPositionControllerPtr = std::shared_ptr<class PWMPositionController>;
    using PWMZeroTorqueControllerPtr = std::shared_ptr<class PWMZeroTorqueController>;
    using ParallelGripperPositionControllerPtr =
        std::shared_ptr<class ParallelGripperPositionController>;
    using ParallelGripperVelocityControllerPtr =
        std::shared_ptr<class ParallelGripperVelocityController>;
    using PWMControllerPtr = std::shared_ptr<class PWMController>;
} // namespace devices::ethercat::head::armar6::joint_controller


namespace devices::ethercat::head::armar6
{

    using PWMVelocityControllerConfigurationPtr =
        std::shared_ptr<class PWMVelocityControllerConfiguration>;
    namespace hwconfig = armarx::control::hardware_config;

    class Device :
        public armarx::SensorDevice,
        // public ControlDevice,
        public armarx::control::ethercat::DeviceInterface
    {

    public:
        class ActorRobotUnitDevice :
                public DeviceInterface::SubDeviceInterface,
                public armarx::ControlDevice,
                public armarx::SensorDevice
        {
            friend class Device;

            // SensorDevice interface
        public:
            ActorRobotUnitDevice(size_t connectorIndex,
                                 const std::string& deviceName,
                                 VirtualRobot::RobotNodePtr robotNode);
            const armarx::SensorValueBase*
            getSensorValue() const override
            {
                return &sensorValue;
            }

            void init(Device* dev,
                      common::imagine_board::Data* headActorDataPtr,
                      ActorConfigWithController& config);

        protected:
            Device* board;
            //            ImagineBoardDataPtr dataPtr;
            VirtualRobot::RobotNodePtr robotNode;
            size_t actorIndex;
            common::imagine_board::SensorActorDataPtr headActorDataPtr;

            joint_controller::EmergencyStopControllerPtr emergencyController;
            joint_controller::StopMovementControllerPtr stopMovementController;
            joint_controller::PWMVelocityControllerPtr velocityController;
            joint_controller::PWMPositionControllerPtr positionController;
            joint_controller::PWMZeroTorqueControllerPtr zeroTorqueController;
            joint_controller::PWMControllerPtr pwmController;

            /// The data object for copying to non-rt part
            common::imagine_board::SensorActorSensorValue sensorValue;

            // SensorDevice interface
        public:
            void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                    const IceUtil::Time& timeSinceLastIteration) override;

            // ControlDevice interface
        public:
            void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp,
                                     const IceUtil::Time& timeSinceLastIteration) override;
            const VirtualRobot::RobotNodePtr& getRobotNode() const;
            common::imagine_board::SensorActorDataPtr getSensorActorDataPtr() const;
        };

        using ActorRobotUnitDevicePtr = std::shared_ptr<ActorRobotUnitDevice>;

        Device(hwconfig::DeviceConfig& config,
               VirtualRobot::RobotPtr const& robot);

        // DeviceInterface interface
        void postSwitchToSafeOp() override;
        TryAssignResult tryAssign(armarx::control::ethercat::SlaveInterface& slave) override;
        AllAssignedResult onAllAssigned() override;
        std::string getClassName() const override;

        // SensorDevice interface
        const armarx::SensorValueBase* getSensorValue() const override;
        void rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                                const IceUtil::Time& timeSinceLastIteration) override;

        // ControlDevice interface
        //    public:
        //        void rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override;

        const armarx::control::ethercat::SlaveIdentifier& getSlaveIdentifier() const;
        size_t getActorIndex(const std::string& actorName);

    private:
        VirtualRobot::RobotPtr robot;
        std::unique_ptr<common::imagine_board::Data> dataPtr;
        common::imagine_board::Slave* slave;
        common::imagine_board::SensorValue sensorValue;
        armarx::control::ethercat::SlaveIdentifier slaveIdentifier;
        PWMVelocityControllerConfigurationPtr velocityControllerConfigDataPtr;
        std::vector<ActorConfigWithController> actorConfigs;
    };

} // namespace devices::ethercat::head::armar6
