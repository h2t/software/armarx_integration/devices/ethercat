#pragma once

#include <armarx/control/hardware_config/DeviceConfig.h>

#include <devices/ethercat/head/armar6/joint_controller/PWMPosition.h>
#include <devices/ethercat/head/armar6/joint_controller/PWMVelocity.h>
#include <devices/ethercat/head/armar6/joint_controller/ZeroTorque.h>

#include <devices/ethercat/common/imagine_board/Config.h>

namespace devices::ethercat::head::armar6
{
    struct ActorConfigWithController : public common::imagine_board::ActorConfig
    {
        ActorConfigWithController(armarx::control::hardware_config::DeviceConfigBase& actorConfig)
            : common::imagine_board::ActorConfig(actorConfig)
        {
            positionControllerCfg = joint_controller::PWMPositionControllerConfiguration::
                CreatePWMPositionControllerConfigData(
                        actorConfig.getControllerConfig("JointHeadPWMPosition"));
            velocityControllerCfg =
                PWMVelocityControllerConfiguration::CreatePWMVelocityControllerConfigData(
                    actorConfig.getControllerConfig("JointHeadPWMVelocity"));
            zeroTorqueControllerCfg =
                joint_controller::PWMZeroTorqueControllerConfiguration::CreateConfigData(
                    actorConfig.getControllerConfig("JointPWMZeroTorque"));
        }

        std::shared_ptr<const joint_controller::PWMPositionControllerConfiguration> positionControllerCfg;
        // TODO: Why not the controller in joint_controllers? Why is there another one?
        std::shared_ptr<const PWMVelocityControllerConfiguration> velocityControllerCfg;
        std::shared_ptr<const joint_controller::PWMZeroTorqueControllerConfiguration> zeroTorqueControllerCfg;
    };
}
