#pragma once

#include <RobotAPI/components/units/RobotUnit/NJointControllers/NJointController.h>

namespace devices::ethercat::head_board::v1::njoint_controller
{

    TYPEDEF_PTRS_HANDLE(PWMPassThroughControllerConfig);


    class PWMPassThroughControllerConfig :
        virtual public armarx::NJointControllerConfig
    {

    public:

        PWMPassThroughControllerConfig(const std::string& deviceName) : deviceName{deviceName} {}
        std::string deviceName;

    };


    TYPEDEF_PTRS_HANDLE(PWMPassThroughController);


    class PWMPassThroughController :
        virtual public armarx::NJointController
    {

    public:

        using ConfigPtrT = PWMPassThroughControllerConfigPtr;

        static armarx::WidgetDescription::WidgetPtr GenerateConfigDescription(const VirtualRobot::RobotPtr& robot,
                const std::map<std::string, armarx::ConstControlDevicePtr>&,
                const std::map<std::string, armarx::ConstSensorDevicePtr>&);

        static ConfigPtrT GenerateConfigFromVariants(const armarx::StringVariantBaseMap& values);

        armarx::WidgetDescription::StringWidgetDictionary getFunctionDescriptions(const Ice::Current&) const override;
        void callDescribedFunction(const std::string& name, const armarx::StringVariantBaseMap& valueMap, const Ice::Current&) override;

        inline PWMPassThroughController(
            armarx::RobotUnitPtr prov,
            const PWMPassThroughControllerConfigPtr& cfg,
            const VirtualRobot::RobotPtr&);

        inline void rtRun(const IceUtil::Time&, const IceUtil::Time&) override
        {
            *target = control;
        }

        inline void rtPreActivateController() override
        {

        }

        inline std::string getClassName(const Ice::Current& = Ice::emptyCurrent) const override
        {
            return "NJointHeadPWMPassThroughController";
        }

        void set(std::int32_t val)
        {
            control = val;
        }

    protected:

        std::atomic<std::int32_t> control {0};
        std::int32_t* target {nullptr};

    };

}  // namespace devices::ethercat::head_board::v1::njoint_controller
