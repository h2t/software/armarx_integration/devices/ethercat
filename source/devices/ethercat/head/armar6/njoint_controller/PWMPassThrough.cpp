#include "PWMPassThroughController.h"


// robot_devices
#include "robot_devices/ethercat/head_board/v1/ControlTargets.h"


namespace devices::ethercat::head_board::v1::njoint_controller
{

    armarx::NJointControllerRegistration<PWMPassThroughController> registrationControllerPWMPassThroughController("NJointHeadPWMPassThroughController");


    armarx::WidgetDescription::WidgetPtr PWMPassThroughController::GenerateConfigDescription(const VirtualRobot::RobotPtr& robot, const std::map<std::string, armarx::ConstControlDevicePtr>&, const std::map<std::string, armarx::ConstSensorDevicePtr>&)
    {
        using namespace armarx::WidgetDescription;
        HBoxLayoutPtr layout = new HBoxLayout;

        LabelPtr label = new Label;
        label->text = "control device name";
        layout->children.emplace_back(label);

        LineEditPtr edit = new LineEdit;
        edit->name = "deviceName";
        layout->children.emplace_back(edit);
        return layout;
    }


    PWMPassThroughControllerConfigPtr PWMPassThroughController::GenerateConfigFromVariants(
        const armarx::StringVariantBaseMap& values)
    {
        return new PWMPassThroughControllerConfig
        {
            values.at("deviceName")->getString()
        };
    }


    armarx::WidgetDescription::StringWidgetDictionary PWMPassThroughController::getFunctionDescriptions(const Ice::Current&) const
    {
        using namespace armarx::WidgetDescription;
        IntSpinBoxPtr spin = new IntSpinBox;
        spin->defaultValue = 0;
        spin->min = -1000;
        spin->max = 1000;
        spin->name = "pwm";
        return {{"SetPWM", spin}};
    }


    void PWMPassThroughController::callDescribedFunction(const std::string& name, const armarx::StringVariantBaseMap& valueMap, const Ice::Current&)
    {
        if (name == "SetPWM")
        {
            set(valueMap.at("pwm")->getInt());
        }
        else
        {
            ARMARX_WARNING << "Unknown function name called: " << name;
        }
    }


    PWMPassThroughController::PWMPassThroughController(
        armarx::RobotUnitPtr prov,
        const PWMPassThroughControllerConfigPtr& cfg,
        const VirtualRobot::RobotPtr&)
    {
        armarx::ControlTargetBase* ct = useControlTarget(cfg->deviceName, ctrl_modes::HeadPWM);
        ARMARX_CHECK_EXPRESSION(ct->isA<ControlTargetHeadPWM>());
        target = &(ct->asA<ControlTargetHeadPWM>()->pwm);
    }

}  // namespace devices::ethercat::head_board::v1::njoint_controller
