#pragma once


#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTargetBase.h>


namespace devices::ethercat::head::armar6
{

    namespace ctrl_modes
    {
        static const std::string HeadPWM = "ControlMode_HeadPWM";
    }


    class ControlTargetHeadPWM :
        public armarx::ControlTargetBase
    {

    public:

        std::int32_t pwm = 0;
        ControlTargetHeadPWM() = default;
        ControlTargetHeadPWM(const ControlTargetHeadPWM&) = default;
        ControlTargetHeadPWM(ControlTargetHeadPWM&&) = default;
        ControlTargetHeadPWM(std::int32_t val) : pwm{val} {}
        ControlTargetHeadPWM& operator=(const ControlTargetHeadPWM&) = default;
        ControlTargetHeadPWM& operator=(ControlTargetHeadPWM&&) = default;

        ControlTargetHeadPWM& operator=(std::int32_t val)
        {
            pwm = val;
            return *this;
        }

        const std::string& getControlMode() const override
        {
            return ctrl_modes::HeadPWM;
        }

        void reset() override
        {
            pwm = std::numeric_limits<int>::min();
        }

        bool isValid() const override
        {
            return pwm != std::numeric_limits<int>::min();
        }

        DETAIL_ControlTargetBase_DEFAULT_METHOD_IMPLEMENTATION

        static ControlTargetInfo<ControlTargetHeadPWM> GetClassMemberInfo()
        {
            ControlTargetInfo<ControlTargetHeadPWM> cti;
            cti.addMemberVariable(&ControlTargetHeadPWM::pwm, "pwm");
            return cti;
        }

    };

}
