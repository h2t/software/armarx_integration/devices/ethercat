#include "PWMPosition.h"

// armarx
#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/observers/filters/rtfilters/AverageFilter.h>

#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetBuilder.h>

#include <RobotAPI/libraries/core/PIDController.h>
#include <RobotAPI/components/units/RobotUnit/RobotUnitModules/RobotUnitModuleDevices.h>

// robot_devices
#include <devices/ethercat/common/imagine_board/Data.h>
#include <devices/ethercat/head/armar6/Device.h>

namespace devices::ethercat::head::armar6::joint_controller
{

    PWMPositionController::PWMPositionController(const std::string& deviceName, Device* board,
            common::imagine_board::SensorActorDataPtr jointData,
            PWMPositionControllerConfigurationCPtr positionControllerConfigDataPtr) : JointController(),
        config(positionControllerConfigDataPtr),
        //    controller(positionControllerConfigDataPtr),
        target(), board(board), deviceName(deviceName)
    {
        actorIndex = board->getActorIndex(deviceName);
        sensorValue = std::dynamic_pointer_cast<Device::ActorRobotUnitDevice>(board->getSubDevices().at(actorIndex))->getSensorValue()->asA<armarx::SensorValue1DoFActuator>();
        ARMARX_CHECK_EXPRESSION(sensorValue) << deviceName;
        dataPtr = jointData;

        posController.desiredDeceleration = static_cast<double>(positionControllerConfigDataPtr->maxDecelerationRad);
        posController.desiredJerk = 100;
        posController.maxDt = static_cast<double>(positionControllerConfigDataPtr->maxDt);
        posController.maxV = static_cast<double>(positionControllerConfigDataPtr->maxVelocityRad);
        posController.p = 4;
        posController.phase2SwitchDistance =  0.1;
        ARMARX_CHECK_GREATER_EQUAL(jointData->getSoftLimitHi(), jointData->getSoftLimitLo());
        //    controller.positionLimitHiHard = dataPtr->getHardLimitHi();
        //    posController.positionLimitHi = jointData->getSoftLimitHi();
        //    controller.positionLimitLoHard = dataPtr->getHardLimitLo();
        //    posController.positionLimitLo = jointData->getSoftLimitLo();
        //    posController.pControlPosErrorLimit = 0.02f;
        //    posController.pid->Kp = posController.calculateProportionalGain();
        //    ARMARX_IMPORTANT << "position Kp " << posController.pid->Kp;

        this->isLimitless = jointData->isLimitless();
        pidPosController.reset(new armarx::PIDController(positionControllerConfigDataPtr->p, positionControllerConfigDataPtr->i, positionControllerConfigDataPtr->d));
        pidPosController->maxIntegral = static_cast<double>(positionControllerConfigDataPtr->maxIntegral);
        pidPosController->differentialFilter.reset(new armarx::rtfilters::AverageFilter(10));
    }


    PWMPositionController::~PWMPositionController() noexcept(true)
    {
        stopRequested = true;
        try
        {
            threadHandle.join();
        }
        catch (...)
        {

        }
    }


    void PWMPositionController::rtRun(const IceUtil::Time& /*sensorValuesTimestamp*/, const IceUtil::Time& timeSinceLastIteration)
    {
        if (target.isValid())
        {
            auto currentPosition = dataPtr->getPosition();

            if (isLimitless)
            {
                ARMARX_RT_LOGF_WARNING("Position controller not implemented for limitless joints").deactivateSpam(10);
                return;
            }
            else
            {
                //            posController.currentPosition =  currentPosition;
            }
            posController.currentV = lastTargetVelocity;
            posController.dt = timeSinceLastIteration.toSecondsDouble();
            posController.setTargetPosition(static_cast<double>(target.position));
            //        ARMARX_CHECK_EXPRESSION(posController.validParameters());
            auto r = posController.run();
            posController.currentPosition = r.position;
            posController.currentAcc = r.acceleration;
            double newVel = r.velocity;
            //        double newVel = posController.p * (posController.targetPosition - posController.currentPosition);
            //        newVel = math::MathUtils::LimitTo(newVel, posController.maxV);
            //            ARMARX_INFO << deactivateSpam(1) << VAROUT(newVel);
            if (std::isnan(newVel))
            {
                newVel = 0;
            }
            pidPosController->update(timeSinceLastIteration.toSecondsDouble(), static_cast<double>(currentPosition), r.position);
            auto targetPWM = pidPosController->getControlValue();
            //        auto targetPWM = static_cast<int>(controller.run(timeSinceLastIteration, dataPtr->getVelocity(), newVel));
            newVel = std::clamp(newVel, -posController.maxV, posController.maxV);

            float torqueFF = config->feedforwardTorqueToPWMFactor * -sensorValue->gravityTorque;
            targetPWM += torqueFF;
            targetPWM += newVel * config->feedforwardVelocityToPWMFactor;

            //        ARMARX_INFO << deactivateSpam(0.1) << VAROUT(pidPosController->previousError) << VAROUT(r.acceleration)
            //                    << VAROUT(target.position) << VAROUT(targetPWM) << VAROUT(pidPosController->Kp) << VAROUT(pidPosController->Ki)
            //                    << VAROUT(torqueFF);

            if (std::isnan(targetPWM))
            {
                ARMARX_ERROR << deactivateSpam(1) << "Target PWM of " << getParent().getDeviceName() << " is NaN!";
                targetPWM = 0.0f;
            }
            float updateRatio = 0.3;
            this->targetPWM = (1.0 - updateRatio) * this->targetPWM + updateRatio * targetPWM;
            float pwmDiff = std::abs(dataPtr->getTargetPWM() - targetPWM);
            //        ARMARX_INFO << deactivateSpam(0.3) << VAROUT(pwmDiff) << VAROUT(dataPtr->getTargetPWM()) << VAROUT(targetPWM) << VAROUT(dataPtr->getVelocity());
            if (pwmDiff > 5 || dataPtr->getVelocity() > 0.01) // avoid jittering when standing still
            {
                //            ARMARX_INFO << deactivateSpam(0.0, std::to_string(targetPWM)) << "Setting new targetPWM to" << targetPWM << " diff: " << pwmDiff << " vel: " << dataPtr->getVelocity();
                dataPtr->setTargetPWM(targetPWM);
            }

            this->targetPWM = targetPWM;
            lastTargetVelocity = newVel;
            //        auto name = getParent().getDeviceName().c_str();
            //        ARMARX_RT_LOGF_INFO("%s: position: %.f, target position: %.f, targetvelocity: %.f, target PWM: %d", name,
            //                            currentPosition, targetPosition, newVel, targetPWM).deactivateSpam(1);
            //        ARMARX_INFO << deactivateSpam(1) << VAROUT(name) << VAROUT(currentPosition) << VAROUT(targetPosition) << VAROUT(newVel) << VAROUT(targetPWM);


        }
        else
        {
            ARMARX_ERROR << deactivateSpam(1) << "invalid target set for actor " << getParent().getDeviceName();
        }
    }


    armarx::ControlTargetBase* PWMPositionController::getControlTarget()
    {
        return &target;
    }


    void PWMPositionController::rtPreActivateController()
    {
        targetPWM = 0.0;
        lastTargetVelocity = static_cast<double>(dataPtr->getVelocity());
        posController.reset();
        posController.currentAcc = static_cast<double>(dataPtr->getAcceleration());
        posController.currentPosition = static_cast<double>(dataPtr->getPosition());
        posController.currentV = static_cast<double>(dataPtr->getVelocity());
        pidPosController->reset();
        //    controller.reset(dataPtr->getVelocity());
    }


    void PWMPositionController::rtPostDeactivateController()
    {
        //    ARMARX_RT_LOGF_INFO("Setting PWM to 0");
        //    dataPtr->setTargetPWM(0);
    }


    armarx::StringVariantBaseMap PWMPositionController::publish(const armarx::DebugDrawerInterfacePrx& /*draw*/, const armarx::DebugObserverInterfacePrx& /*observer*/) const
    {
        if (!remoteGui)
        {
            threadHandle = armarx::Application::getInstance()->getThreadPool()->runTask([this]
            {
                std::string guiTabName;
                while (!stopRequested)
                {
                    armarx::ManagedIceObjectPtr object;
                    ARMARX_IMPORTANT << deactivateSpam(1) << "Trying to get parent";
                    try
                    {
                        object = armarx::ManagedIceObjectPtr::dynamicCast(getParent().getOwner());
                        ARMARX_CHECK_EXPRESSION(object);
                        remoteGui = object->getProxy<armarx::RemoteGuiInterfacePrx>("RemoteGuiProvider", false, "", false);
                        if (!remoteGui)
                        {
                            continue;
                        }
                        ARMARX_IMPORTANT << deactivateSpam(1) << "Got Proxy";
                        guiTabName = getParent().getDeviceName() + getControlMode();
                        break;
                    }
                    catch (...)
                    {
                        sleep(1);
                    }
                }

                if (remoteGui)
                {
                    ARMARX_IMPORTANT << "Creating GUI " << guiTabName;
                    using namespace armarx::RemoteGui;

                    auto vLayout = makeVBoxLayout();

                    {
                        WidgetPtr KpLabel = makeTextLabel("Kp: ");

                        WidgetPtr KpSlider = makeFloatSlider("KpSlider")
                                             .min(0.0f).max(pidPosController->Kp * 5)
                                             .value(pidPosController->Kp);
                        WidgetPtr KpLabelValue = makeTextLabel(std::to_string(pidPosController->Kp * 5));
                        WidgetPtr line = makeHBoxLayout()
                                         .children({KpLabel, KpSlider, KpLabelValue});

                        vLayout.addChild(line);
                    }

                    {
                        WidgetPtr KiLabel = makeTextLabel("Ki: ");
                        WidgetPtr KiSlider = makeFloatSlider("KiSlider")
                                             .min(0.0f).max(pidPosController->Ki * 5)
                                             .value(pidPosController->Ki);
                        WidgetPtr KiLabelValue = makeTextLabel(std::to_string(pidPosController->Ki * 5));

                        WidgetPtr line = makeHBoxLayout()
                                         .children({KiLabel, KiSlider, KiLabelValue});

                        vLayout.addChild(line);
                    }

                    {
                        WidgetPtr KdLabel = makeTextLabel("Kd: ");
                        WidgetPtr KdSlider = makeFloatSlider("KdSlider")
                                             .min(-10.0f * pidPosController->Kd).max(10.0f * pidPosController->Kd)
                                             .steps(1000)
                                             .value(pidPosController->Kd);
                        WidgetPtr KdLabelValue = makeTextLabel(std::to_string(pidPosController->Kd * 10));

                        WidgetPtr line = makeHBoxLayout()
                                         .children({KdLabel, KdSlider, KdLabelValue});

                        vLayout.addChild(line);
                        vLayout.addChild(new VSpacer);
                    }

                    //        WidgetPtr spin = makeFloatSpinBox("KpSpin")
                    //                         .min(0.0f).max(2.0f)
                    //                         .steps(20).decimals(2)
                    //                         .value(0.4f);

                    WidgetPtr groupBox = makeGroupBox("GroupBox")
                                         .label("Group")
                                         .child(vLayout);

                    remoteGui->createTab(guiTabName, groupBox);

                    while (!stopRequested)
                    {
                        armarx::RemoteGui::TabProxy tab(remoteGui, guiTabName);
                        tab.receiveUpdates();
                        //                    this->controller.pid->Kp = tab.getValue<float>("KpSlider").get();
                        //                    this->controller.pid->Ki = tab.getValue<float>("KiSlider").get();
                        //                    this->controller.pid->Kd = tab.getValue<float>("KdSlider").get();
                        pidPosController->Kp = tab.getValue<float>("KpSlider").get();
                        pidPosController->Ki = tab.getValue<float>("KiSlider").get();
                        pidPosController->Kd = tab.getValue<float>("KdSlider").get();
                        usleep(100000);
                    }
                }

            });
        }
        return {{"lastTargetVelocity", new armarx::Variant(lastTargetVelocity.load())},
            {"targetPosition", new armarx::Variant(posController.currentPosition)}, // position of profile generator is target position
            {"posError", new armarx::Variant(posController.getTargetPosition() - posController.currentPosition)},
            {"pidError", new armarx::Variant(pidPosController->previousError)},
            //        {"filteredVelocity", new Variant(controller.lastActualVelocity.load())},
            {"pidIntegralCV", new armarx::Variant(pidPosController->integral * static_cast<double>(pidPosController->Ki))},
            {"pidIntegral", new armarx::Variant(pidPosController->integral)},
            {"pidPropCV", new armarx::Variant(pidPosController->previousError * static_cast<double>(pidPosController->Kp))},
            {"pidDiffCV", new armarx::Variant(pidPosController->derivative * static_cast<double>(pidPosController->Kd))},
            //        {"pospidIntegralCV", new Variant(posController.pid->integral * posController.pid->Ki)},
            //        {"pospidIntegral", new Variant(posController.pid->integral)},
            //        {"pospidPropCV", new Variant(posController.pid->previousError * posController.pid->Kp)},
            //        {"pospidDiffCV", new Variant(posController.pid->derivative * posController.pid->Kd)},
            //        {"pidUsed", new Variant(posController.getCurrentlyPIDActive())},
            {"desiredPWM", new armarx::Variant(targetPWM.load())}
        };
    }

    PWMPositionControllerConfigurationCPtr PWMPositionControllerConfiguration::CreatePWMPositionControllerConfigData(armarx::control::hardware_config::Config& hwConfig)
    {
        PWMPositionControllerConfiguration configData;

        configData.maxVelocityRad = hwConfig.getFloat("maxVelocityRad");
        configData.maxAccelerationRad = hwConfig.getFloat("maxAccelerationRad");
        configData.maxDecelerationRad = hwConfig.getFloat("maxDecelerationRad");
        configData.maxDt = hwConfig.getFloat("maxDt");
        configData.p = hwConfig.getFloat("p");
        configData.i = hwConfig.getFloat("i");
        configData.d = hwConfig.getFloat("d");
        configData.maxIntegral = hwConfig.getFloat("maxIntegral");
        configData.feedforwardVelocityToPWMFactor = hwConfig.getFloat("feedforwardVelocityToPWMFactor");
        configData.feedforwardTorqueToPWMFactor = hwConfig.getFloat("feedforwardTorqueToPWMFactor");
        configData.PWMDeadzone = hwConfig.getFloat("PWMDeadzone");
        configData.velocityUpdatePercent = hwConfig.getFloat("velocityUpdatePercent");
        configData.conditionalIntegralErrorTreshold = hwConfig.getFloat("conditionalIntegralErrorTreshold");
        configData.feedForwardMode = hwConfig.getBool("FeedForwardMode");

        return std::make_shared<PWMPositionControllerConfiguration>(configData);
    }

}  // namespace devices::ethercat::head_board::v1::joint_controller
