#include "PWM.h"


// robot_devices
#include <devices/ethercat/common/imagine_board/Data.h>
#include <devices/ethercat/head/armar6/Device.h>

namespace devices::ethercat::head::armar6::joint_controller
{

    PWMController::PWMController(
        const std::string&,
        Device*,
        common::imagine_board::SensorActorDataPtr jointData
    ) : dataPtr(jointData)
    {
    }


    void PWMController::rtRun(const IceUtil::Time&, const IceUtil::Time&)
    {
        if (target.isValid())
        {
            dataPtr->setTargetPWM(target.pwm);
        }
        else
        {
            ARMARX_ERROR << "invalid target set for actor";
        }
    }


    armarx::ControlTargetBase* PWMController::getControlTarget()
    {
        return &target;
    }

}  // namespace devices::ethercat::head_board::v1::joint_controller
