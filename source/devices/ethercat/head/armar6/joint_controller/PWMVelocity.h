#pragma once

// armarx
#include <ArmarXCore/core/services/tasks/ThreadPool.h>
#include <ArmarXGui/interface/RemoteGuiInterface.h>
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/SensorValues/SensorValue1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/BasicControllers.h>

// robot_devices
#include <devices/ethercat/head/armar6/PWMVelocity.h>


namespace armarx
{
    using PIDControllerPtr = std::shared_ptr<class PIDController>;
}


namespace devices::ethercat::common::imagine_board
{
    using SensorActorDataPtr = std::shared_ptr<class SensorActorData>;
}


namespace devices::ethercat::head::armar6
{
    class Device;
    using PWMVelocityControllerConfigurationCPtr = std::shared_ptr<const class PWMVelocityControllerConfiguration>;
}

namespace devices::ethercat::head::armar6::joint_controller
{

    class PWMVelocityController :
        public armarx::JointController
    {

    public:

        PWMVelocityController(const std::string& deviceName,
                              Device* board,
                              common::imagine_board::SensorActorDataPtr jointData,
                              PWMVelocityControllerConfigurationCPtr velocityControllerConfigDataPtr);
        ~PWMVelocityController() noexcept(true);

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override ;
        armarx::ControlTargetBase* getControlTarget() override;

        armarx::StringVariantBaseMap publish(const armarx::DebugDrawerInterfacePrx& draw, const armarx::DebugObserverInterfacePrx& observer) const override;

        void rtPreActivateController() override;
        void rtPostDeactivateController() override;

    protected:

        PWMVelocityControllerConfigurationCPtr config;
        devices::ethercat::head::armar6::PWMVelocityController controller;
        armarx::VelocityControllerWithRampedAccelerationAndPositionBounds velController;

        armarx::ControlTarget1DoFActuatorVelocity target;

        std::atomic<float> lastTargetVelocity, lastTargetAcceleration;
        bool isLimitless;

        common::imagine_board::SensorActorDataPtr dataPtr;
        Device* board;
        const std::string deviceName;
        size_t actorIndex = 0;
        mutable armarx::RemoteGuiInterfacePrx remoteGui;
        bool stopRequested = false;
        mutable armarx::ThreadPool::Handle threadHandle;
        const armarx::SensorValue1DoFActuator* sensorValue;

    };

}  // namespace devices::ethercat::head_board::v1::joint_controller
