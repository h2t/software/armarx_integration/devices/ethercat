#pragma once


// armarx
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/BasicControllers.h>

#include <armarx/control/hardware_config/Config.h>


namespace devices::ethercat::common::imagine_board
{
    using SensorActorDataPtr = std::shared_ptr<class SensorActorData>;
}

namespace devices::ethercat::head::armar6
{
    class Device;
}


namespace devices::ethercat::head::armar6::joint_controller
{

    using PWMZeroTorqueControllerConfigurationPtr = std::shared_ptr<class PWMZeroTorqueControllerConfiguration>;
    using PWMZeroTorqueControllerConfigurationCPtr = std::shared_ptr<const class PWMZeroTorqueControllerConfiguration>;


    class PWMZeroTorqueControllerConfiguration
    {

    public:

        PWMZeroTorqueControllerConfiguration() {}
        static PWMZeroTorqueControllerConfigurationCPtr CreateConfigData(armarx::control::hardware_config::Config& hwConfig);
        float feedforwardVelocityToPWMFactor;
        float PWMDeadzone;

    };


    using JointPWMZeroTorqueControllerPtr = std::shared_ptr<class PWMZeroTorqueController>;


    class PWMZeroTorqueController :
        public armarx::JointController
    {

    public:

        PWMZeroTorqueController(const std::string& deviceName,
                                Device* board,
                                common::imagine_board::SensorActorDataPtr jointData,
                                PWMZeroTorqueControllerConfigurationCPtr config);
        ~PWMZeroTorqueController() noexcept(true);

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override ;
        armarx::ControlTargetBase* getControlTarget() override;

        void rtPreActivateController() override;
        void rtPostDeactivateController() override;

    private:

        PWMZeroTorqueControllerConfigurationCPtr config;
        armarx::ControlTarget1DoFActuatorZeroTorque target;

        std::atomic<double> lastTargetVelocity;
        bool isLimitless;

        common::imagine_board::SensorActorDataPtr dataPtr;
        Device* board;
        const std::string deviceName;
        size_t actorIndex = 0;

    };

}
