#include "ZeroTorque.h"


// robot_devices
#include <devices/ethercat/head/armar6/Device.h>

namespace devices::ethercat::head::armar6::joint_controller
{

    PWMZeroTorqueControllerConfigurationCPtr PWMZeroTorqueControllerConfiguration::CreateConfigData(armarx::control::hardware_config::Config& hwConfig)
    {
        PWMZeroTorqueControllerConfiguration configData;

        configData.feedforwardVelocityToPWMFactor = hwConfig.getFloat("feedforwardVelocityToPWMFactor");
        configData.PWMDeadzone = hwConfig.getFloat("PWMDeadzone");

        return std::make_shared<PWMZeroTorqueControllerConfiguration>(configData);
    }


    PWMZeroTorqueController::PWMZeroTorqueController(const std::string& deviceName,
            Device* board,
            common::imagine_board::SensorActorDataPtr jointData,
            PWMZeroTorqueControllerConfigurationCPtr config) : JointController(),
        config(config), target(), board(board), deviceName(deviceName)
    {
        actorIndex = board->getActorIndex(deviceName);
        dataPtr = jointData;

        this->isLimitless = jointData->isLimitless();
    }


    PWMZeroTorqueController::~PWMZeroTorqueController() noexcept(true)
    {
    }


    void PWMZeroTorqueController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        if (target.isValid())
        {
            float targetPWM = dataPtr->getVelocity() * config->feedforwardVelocityToPWMFactor;
            targetPWM += armarx::math::MathUtils::Sign(dataPtr->getVelocity()) * config->PWMDeadzone;
            //        targetPWM = math::MathUtils::LimitTo(targetPWM, 1500);
            dataPtr->setTargetPWM(targetPWM);

            //            ARMARX_RT_LOGF_INFO("target velocity: %.3f, current velocity: %.3f, target pwm: %d, kp: %.3f ki: %f, kd: %f, max acc: %.3f",
            //                                target.velocity, dataPtr->getVelocity(), targetPWM, pid->Kp, pid->Ki, pid->Kd, controller.acceleration).deactivateSpam(1);


        }
        else
        {
            ARMARX_ERROR << "invalid target set for actor";
        }
    }


    armarx::ControlTargetBase* PWMZeroTorqueController::getControlTarget()
    {
        return &target;
    }


    void PWMZeroTorqueController::rtPreActivateController()
    {
        lastTargetVelocity = dataPtr->getVelocity();
        //    controller.reset(dataPtr->getVelocity());
    }


    void PWMZeroTorqueController::rtPostDeactivateController()
    {
        ARMARX_RT_LOGF_INFO("Setting PWM to 0");
        dataPtr->setTargetPWM(0);
    }

}
