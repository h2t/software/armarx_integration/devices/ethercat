#pragma once


// armarx
#include <RobotAPI/components/units/RobotUnit/JointControllers/JointController.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTargetBase.h>

// robot_devices
#include <devices/ethercat/head/armar6/ControlTargets.h>


namespace devices::ethercat::common::imagine_board
{
    using SensorActorDataPtr = std::shared_ptr<class SensorActorData>;
}

namespace devices::ethercat::head::armar6
{
    class Device;
}

namespace devices::ethercat::head::armar6::joint_controller
{
    using PWMControllerPtr = std::shared_ptr<class PWMController> ;

    class PWMController : public armarx::JointController
    {

    public:

        PWMController(const std::string&, Device*, common::imagine_board::SensorActorDataPtr jointData);

        void rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration) override ;
        armarx::ControlTargetBase* getControlTarget() override;

    protected:

        ControlTargetHeadPWM target;
        common::imagine_board::SensorActorDataPtr dataPtr;

    };

}  // namespace devices::ethercat::head_board::v1::joint_controller
