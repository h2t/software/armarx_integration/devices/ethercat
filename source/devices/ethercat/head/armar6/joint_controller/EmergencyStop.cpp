#include "EmergencyStop.h"


// armarx
#include <RobotAPI/libraries/core/PIDController.h>

// robot_devices
#include <devices/ethercat/common/imagine_board/Data.h>


namespace devices::ethercat::head::armar6::joint_controller
{

    EmergencyStopController::EmergencyStopController(common::imagine_board::SensorActorDataPtr dataPtr) :
        dataPtr(dataPtr)
    {
        pid.reset(new armarx::PIDController(0, 5000, 0));
        pid->maxIntegral = 0.1;
    }


    void EmergencyStopController::rtRun(const IceUtil::Time& /*sensorValuesTimestamp*/, const IceUtil::Time& timeSinceLastIteration)
    {
        pid->update(timeSinceLastIteration.toSecondsDouble(), static_cast<double>(dataPtr->getVelocity()), 0.0);
        double targetPwm = pid->getControlValue();
        if (std::isnan(targetPwm))
        {
            targetPwm = 0.0;
        }
        dataPtr->setTargetPWM(static_cast<std::int32_t>(targetPwm));
        //        lastPWM *= 0.9997;
        //        dataPtr->setTargetPWM(lastPWM);
    }


    armarx::ControlTargetBase* EmergencyStopController::getControlTarget()
    {
        return &target;
    }


    void EmergencyStopController::rtPreActivateController()
    {
        pid->reset();
        //        ARMARX_INFO << "Stopping gripper!";
        lastPWM = static_cast<float>(std::clamp(dataPtr->getTargetPWM(), -500, 500));
    }

}  // namespace devices::ethercat::head_board::v1::joint_controller
