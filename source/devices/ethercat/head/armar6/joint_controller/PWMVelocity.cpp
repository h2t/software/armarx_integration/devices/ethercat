#include "PWMVelocity.h"


// armarx
#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/core/application/Application.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetBuilder.h>
#include <ArmarXGui/libraries/RemoteGui/WidgetProxy.h>
#include <RobotAPI/components/units/RobotUnit/RobotUnitModules/RobotUnitModuleDevices.h>
#include <RobotAPI/libraries/core/PIDController.h>

#include <devices/ethercat/common/imagine_board/Data.h>
#include <devices/ethercat/head/armar6/Device.h>

namespace devices::ethercat::head::armar6::joint_controller
{

    PWMVelocityController::PWMVelocityController(const std::string& deviceName,
            Device* board,
            common::imagine_board::SensorActorDataPtr jointData,
            PWMVelocityControllerConfigurationCPtr velocityControllerConfigDataPtr) : JointController(),
        config(velocityControllerConfigDataPtr),
        controller(velocityControllerConfigDataPtr),
        target(), board(board), deviceName(deviceName)
    {
        actorIndex = board->getActorIndex(deviceName);
        sensorValue = std::dynamic_pointer_cast<Device::ActorRobotUnitDevice>(board->getSubDevices().at(actorIndex))->getSensorValue()->asA<armarx::SensorValue1DoFActuator>();
        ARMARX_CHECK_EXPRESSION(sensorValue) << deviceName;
        dataPtr = jointData;

        //    velController.acceleration = velocityControllerConfigDataPtr->maxAccelerationRad;
        velController.deceleration = velocityControllerConfigDataPtr->maxDecelerationRad;
        velController.jerk = velocityControllerConfigDataPtr->jerk;
        velController.maxDt = velocityControllerConfigDataPtr->maxDt;
        velController.maxV = velocityControllerConfigDataPtr->maxVelocityRad;
        velController.directSetVLimit = velocityControllerConfigDataPtr->directSetVLimit;
        ARMARX_CHECK_GREATER_EQUAL(jointData->getSoftLimitHi(), jointData->getSoftLimitLo());
        //    controller.positionLimitHiHard = dataPtr->getHardLimitHi();
        velController.positionLimitHiSoft = jointData->getSoftLimitHi();
        //    controller.positionLimitLoHard = dataPtr->getHardLimitLo();
        velController.positionLimitLoSoft = jointData->getSoftLimitLo();
        this->isLimitless = jointData->isLimitless();
    }


    PWMVelocityController::~PWMVelocityController() noexcept(true)
    {
        stopRequested = true;
        try
        {
            threadHandle.join();
        }
        catch (...)
        {

        }
    }


    void PWMVelocityController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        if (target.isValid())
        {
            auto currentPosition = dataPtr->getPosition();
            if (isLimitless)
            {
                velController.currentPosition = velController.positionLimitHiSoft - (velController.positionLimitHiSoft - velController.positionLimitLoSoft) * 0.5;
                //                ARMARX_INFO << VAROUT(velController.currentPosition) << VAROUT(velController.positionLimitLoSoft) << VAROUT(velController.positionLimitHiSoft);
            }
            else
            {
                velController.currentPosition =  currentPosition;
            }
            velController.currentV = lastTargetVelocity;
            velController.currentAcc = lastTargetAcceleration;
            velController.dt = timeSinceLastIteration.toSecondsDouble();
            velController.targetV = target.velocity;
            auto r = velController.run();
            double newVel = r.velocity;
            double newAcc = r.acceleration;

            //            ARMARX_INFO << deactivateSpam(1) << VAROUT(newVel) << VAROUT(target.velocity);
            if (std::isnan(newVel))
            {
                newVel = 0;
                newAcc = 0;
            }
            //            float newVel = target.velocity;
            if ((currentPosition > velController.positionLimitHiSoft && target.velocity > 0)
                || (currentPosition < velController.positionLimitLoSoft && target.velocity < 0))
            {
                newVel = 0;
                newAcc = 0;
                ARMARX_INFO << deactivateSpam(1) << "Breaking now at " << dataPtr->getPosition() << " pwm: " << dataPtr->getTargetPWM();
            }

            auto targetPWM = static_cast<int>(controller.run(timeSinceLastIteration, dataPtr->getVelocity(), newVel, sensorValue->gravityTorque));
            dataPtr->setTargetPWM(targetPWM);

            lastTargetVelocity = newVel;
            lastTargetAcceleration = newAcc;

            //            ARMARX_RT_LOGF_INFO("target velocity: %.3f, current velocity: %.3f, target pwm: %d, kp: %.3f ki: %f, kd: %f, max acc: %.3f",
            //                                target.velocity, dataPtr->getVelocity(), targetPWM, pid->Kp, pid->Ki, pid->Kd, controller.acceleration).deactivateSpam(1);
        }
        else
        {
            ARMARX_ERROR << "invalid target set for actor";
        }
    }


    armarx::ControlTargetBase* PWMVelocityController::getControlTarget()
    {
        return &target;
    }


    void PWMVelocityController::rtPreActivateController()
    {
        lastTargetVelocity = dataPtr->getVelocity();
        lastTargetAcceleration = dataPtr->getAcceleration();
        controller.reset(static_cast<double>(dataPtr->getVelocity()));
    }


    void PWMVelocityController::rtPostDeactivateController()
    {
        //    ARMARX_RT_LOGF_INFO("Setting PWM to 0");
        //    dataPtr->setTargetPWM(0);
    }


    armarx::StringVariantBaseMap PWMVelocityController::publish(const armarx::DebugDrawerInterfacePrx& /*draw*/, const armarx::DebugObserverInterfacePrx& /*observer*/) const
    {
        if (!remoteGui && !threadHandle.isValid())
        {
            threadHandle = armarx::Application::getInstance()->getThreadPool()->runTask([this]
            {
                std::string guiTabName;
                while (!stopRequested)
                {
                    armarx::ManagedIceObjectPtr object;
                    ARMARX_IMPORTANT << deactivateSpam(1) << "Trying to get parent";
                    try
                    {
                        object = armarx::ManagedIceObjectPtr::dynamicCast(getParent().getOwner());
                        ARMARX_CHECK_EXPRESSION(object);
                        remoteGui = object->getProxy<armarx::RemoteGuiInterfacePrx>("RemoteGuiProvider", false, "", false);
                        if (!remoteGui)
                        {
                            return;
                        }
                        ARMARX_IMPORTANT << deactivateSpam(1) << "Got Proxy";
                        guiTabName = getParent().getDeviceName() + getControlMode();
                        break;
                    }
                    catch (...)
                    {
                        armarx::handleExceptions();
                        sleep(1);
                    }
                }
                if (remoteGui)
                {
                    ARMARX_IMPORTANT << "Creating GUI " << guiTabName;
                    using namespace armarx::RemoteGui;

                    auto vLayout = makeVBoxLayout();

                    {
                        WidgetPtr KpLabel = makeTextLabel("Kp: ");

                        WidgetPtr KiSlider = makeFloatSlider("KpSlider")
                                             .min(0.0f).max(5000.0f)
                                             .value(config->p);
                        WidgetPtr line = makeHBoxLayout()
                                         .children({KpLabel, KiSlider});

                        vLayout.addChild(line);

                    }

                    {
                        WidgetPtr KiLabel = makeTextLabel("Ki: ");
                        WidgetPtr KiSlider = makeFloatSlider("KiSlider")
                                             .min(0.0f).max(50000.0f)
                                             .value(config->i);

                        WidgetPtr line = makeHBoxLayout()
                                         .children({KiLabel, KiSlider});

                        vLayout.addChild(line);

                    }

                    {
                        WidgetPtr KdLabel = makeTextLabel("Kd: ");
                        WidgetPtr KdSlider = makeFloatSlider("KdSlider")
                                             .min(0.0f).max(50.0f)
                                             .steps(100)
                                             .value(config->d);

                        WidgetPtr line = makeHBoxLayout()
                                         .children({KdLabel, KdSlider});

                        vLayout.addChild(line);
                        vLayout.addChild(new VSpacer);
                    }

                    //        WidgetPtr spin = makeFloatSpinBox("KpSpin")
                    //                         .min(0.0f).max(2.0f)
                    //                         .steps(20).decimals(2)
                    //                         .value(0.4f);

                    WidgetPtr groupBox = makeGroupBox("GroupBox")
                                         .label("Group")
                                         .child(vLayout);

                    remoteGui->createTab(guiTabName, groupBox);

                    while (!stopRequested)
                    {
                        armarx::RemoteGui::TabProxy tab(remoteGui, guiTabName);
                        tab.receiveUpdates();
                        this->controller.pid->Kp = tab.getValue<float>("KpSlider").get();
                        this->controller.pid->Ki = tab.getValue<float>("KiSlider").get();
                        this->controller.pid->Kd = tab.getValue<float>("KdSlider").get();
                        usleep(100000);
                    }
                }
            });
        }
        return
        {
            {"lastTargetVelocity", new armarx::Variant(lastTargetVelocity.load())},
            {"lastTargetAcceleration", new armarx::Variant(lastTargetAcceleration.load())},
            {"filteredVelocity", new armarx::Variant(controller.lastActualVelocity.load())},
            {"pidIntegralCV", new armarx::Variant(controller.pid->integral * controller.pid->Ki)},
            {"pidPropCV", new armarx::Variant(controller.pid->previousError * controller.pid->Kp)},
            {"pidDiffCV", new armarx::Variant(controller.pid->derivative * controller.pid->Kd)}

        };
    }

}  // namespace devices::ethercat::head_board::v1::joint_controller
