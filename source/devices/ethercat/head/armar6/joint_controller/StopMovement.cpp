#include "StopMovement.h"


// robot_devices
#include <devices/ethercat/common/imagine_board/Data.h>


namespace devices::ethercat::head::armar6::joint_controller
{

    StopMovementController::StopMovementController(common::imagine_board::SensorActorDataPtr dataPtr) :
        dataPtr(dataPtr)
    {
        pid.reset(new armarx::PIDController(0, 5000, 0));
        pid->maxIntegral = 0.1;
    }


    void StopMovementController::rtRun(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    {
        pid->update(timeSinceLastIteration.toSecondsDouble(), dataPtr->getVelocity(), 0.0);
        float targetPwm = pid->getControlValue();

        if (std::isnan(targetPwm))
        {
            targetPwm = 0.0f;
        }

        dataPtr->setTargetPWM(targetPwm);
    }


    armarx::ControlTargetBase* StopMovementController::getControlTarget()
    {
        return &target;
    }


    void StopMovementController::rtPreActivateController()
    {
        pid->reset();
    }

}  // namespace devices::ethercat::head_board::v1::joint_controller
