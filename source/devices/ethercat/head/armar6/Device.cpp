/**
     * This file is part of ArmarX.
     *
     * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
     *
     * ArmarX is free software; you can redistribute it and/or modify
     * it under the terms of the GNU General Public License version 2 as
     * published by the Free Software Foundation.
     *
     * ArmarX is distributed in the hope that it will be useful, but
     * WITHOUT ANY WARRANTY; without even the implied warranty of
     * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
     * GNU General Public License for more details.
     *
     * You should have received a copy of the GNU General Public License
     * along with this program. If not, see <http://www.gnu.org/licenses/>.
     *
     * @package    ArmarX
     * @author     Mirko Waechter( mirko.waechter at kit dot edu)
     * @date       2018
     * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
     *             GNU General Public License
     */


#include "Device.h"

#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/Robot.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/logging/Logging.h>

#include "joint_controller/EmergencyStop.h"
#include "joint_controller/PWM.h"
#include "joint_controller/PWMPosition.h"
#include "joint_controller/PWMVelocity.h"
#include "joint_controller/StopMovement.h"
#include "joint_controller/ZeroTorque.h"
#include <devices/ethercat/common/h2t_devices.h>
#include <devices/ethercat/common/imagine_board/Slave.h>


namespace devices::ethercat::head::armar6
{

    Device::Device(hwconfig::DeviceConfig& config,
                   const VirtualRobot::RobotPtr& robot) :
        DeviceBase(config.getName()),
        SensorDevice(config.getName()),
        //        ControlDevice(config.getName()),
        DeviceInterface(config.getName()),
        robot(robot),
        slave(nullptr),
        slaveIdentifier(config.getOnlySlaveConfig().getIdentifier())
    {
        slaveIdentifier.setName(config.getName());
        auto actorHwConfigs = config.getSubDeviceConfigsWithType("Actor");
        ARMARX_VERBOSE << "found " << actorHwConfigs.size() << " actors";
        for (hwconfig::DeviceConfigBase& motorHwConfig : actorHwConfigs)
        {
            ActorConfigWithController actorConfig{motorHwConfig};
            actorConfigs.push_back(actorConfig);

            if (actorConfig.enabled)
            {
                ARMARX_VERBOSE << "Found motor configuration for connector index "
                               << actorConfig.connector;
                //                auto SensorActorData = dataPtr->getSensorActorData(connectorIndex);
                //                ARMARX_CHECK_EXPRESSION(SensorActorData) << name;
                auto robotNode = robot->getRobotNode(actorConfig.name);
                ARMARX_CHECK_EXPRESSION(robotNode) << actorConfig.name;
                ARMARX_INFO << "Creating actor class for " << actorConfig.name;
                Device::ActorRobotUnitDevicePtr ptr =
                    std::make_shared<Device::ActorRobotUnitDevicePtr::element_type>(
                        actorConfig.connector, actorConfig.name, robotNode);
                subDevices.push_back(ptr);
            }
            else
            {
                ARMARX_INFO << "motor at Index " << actorConfig.connector << " disabled";
            }
        }
    }


    armarx::control::ethercat::DeviceInterface::TryAssignResult
    Device::tryAssign(armarx::control::ethercat::SlaveInterface& slave)
    {
        using Result = DeviceInterface::TryAssignResult;

        Result result = ethercat::common::tryAssignLegacyH2TDevice(
            slave, this->slave, slaveIdentifier.productCode);
        if (result != Result::unknown)
        {
            return result;
        }

        return result;

        return TryAssignResult::unknown;
    }


    armarx::control::ethercat::DeviceInterface::AllAssignedResult
    Device::onAllAssigned()
    {
        using Result = DeviceInterface::AllAssignedResult;

        if (slave)
        {
            return Result::ok;
        }

        return Result::slavesMissing;
    }


    std::string
    Device::getClassName() const
    {
        return "HeadBoard";
    }


    void
    Device::postSwitchToSafeOp()
    {
        if (!dataPtr)
        {
            dataPtr.reset(new common::imagine_board::Data(/* copy list and slice the objects to get rid of controller configurations */
                                                          std::list<common::imagine_board::ActorConfig>(actorConfigs.begin(), actorConfigs.end()),
                                                          slave->getOutputsPtr(),
                                                          slave->getInputsPtr(),
                                                          robot));

            for (auto& actorConfig : actorConfigs)
            {
                if (actorConfig.enabled)
                {
                    size_t i = getActorIndex(actorConfig.name);
                    std::dynamic_pointer_cast<Device::ActorRobotUnitDevice>(subDevices.at(i))
                        ->init(this, dataPtr.get(), actorConfig);
                }
            }
        }

        ARMARX_CHECK_NOT_NULL(dataPtr);
    }


    const armarx::SensorValueBase*
    Device::getSensorValue() const
    {
        return &sensorValue;
    }


    const armarx::control::ethercat::SlaveIdentifier&
    Device::getSlaveIdentifier() const
    {
        return slaveIdentifier;
    }


    void
    Device::rtReadSensorValues(const IceUtil::Time& sensorValuesTimestamp,
                               const IceUtil::Time& timeSinceLastIteration)
    {
        dataPtr->rtReadSensorValues(sensorValuesTimestamp, timeSinceLastIteration);

        // TODO: read IMU
    }


    //    void head_board::rtWriteTargetValues(const IceUtil::Time& sensorValuesTimestamp, const IceUtil::Time& timeSinceLastIteration)
    //    {
    //        // TODO: write LED targets
    //    }


    size_t
    Device::getActorIndex(const std::string& actorName)
    {
        size_t i = 0;

        for (auto& a : subDevices)
        {
            ActorRobotUnitDevicePtr actor = std::dynamic_pointer_cast<ActorRobotUnitDevice>(a);
            if (actor && actor->getRobotNode()->getName() == actorName)
            {
                return i;
            }
            i++;
        }

        throw armarx::LocalException()
            << "Could not find actor with name: " << actorName << "\nactors:\n"
            << ARMARX_STREAM_PRINTER
        {
            for (auto& a : subDevices)
            {
                ActorRobotUnitDevicePtr actor = std::dynamic_pointer_cast<ActorRobotUnitDevice>(a);
                if (actor)
                {
                    out << actor->getDeviceName();
                }
                else
                {
                    out << "Cannot dynamic cast subDevice to ActorRobotUnitDevice";
                }
            }
        };
    }


    Device::ActorRobotUnitDevice::ActorRobotUnitDevice(size_t connectorIndex,
                                                       const std::string& deviceName,
                                                       VirtualRobot::RobotNodePtr robotNode) :
        DeviceBase(deviceName),
        DeviceInterface::SubDeviceInterface(deviceName),
        ControlDevice(deviceName),
        SensorDevice(deviceName),
        actorIndex(connectorIndex)
    {
        ARMARX_CHECK_EXPRESSION(robotNode) << deviceName;
        this->robotNode = robotNode;
        ARMARX_INFO << deviceName << " actor created";
    }


    void
    Device::ActorRobotUnitDevice::init(Device* dev,
                                       common::imagine_board::Data* dataPtr,
                                       ActorConfigWithController& config)
    {
        ARMARX_INFO << "Init " << dev->getDeviceName();

        this->board = dev;
        this->headActorDataPtr = dataPtr->getSensorActorData(actorIndex);
        emergencyController.reset(new joint_controller::EmergencyStopController(headActorDataPtr));
        addJointController(emergencyController.get());
        stopMovementController.reset(
            new joint_controller::StopMovementController(headActorDataPtr));
        addJointController(stopMovementController.get());

        if (headActorDataPtr->getVelocityControlEnabled())
        {
            velocityController.reset(new joint_controller::PWMVelocityController(
                getDeviceName(), dev, headActorDataPtr, config.velocityControllerCfg));
            addJointController(velocityController.get());
        }
        else
        {
            ARMARX_VERBOSE << "Velocity Control disabled for " << getDeviceName();
        }
        zeroTorqueController.reset(new joint_controller::PWMZeroTorqueController(
            getDeviceName(), dev, headActorDataPtr, config.zeroTorqueControllerCfg));
        addJointController(zeroTorqueController.get());
        if (headActorDataPtr->getPositionControlEnabled())
        {
            positionController.reset(new joint_controller::PWMPositionController(
                getDeviceName(), dev, headActorDataPtr, config.positionControllerCfg));
            addJointController(positionController.get());
        }
        else
        {
            ARMARX_VERBOSE << "Position Control disabled for " << getDeviceName();
        }
        pwmController.reset(
            new joint_controller::PWMController(getDeviceName(), dev, headActorDataPtr));
        addJointController(pwmController.get());
    }


    common::imagine_board::SensorActorDataPtr
    Device::ActorRobotUnitDevice::getSensorActorDataPtr() const
    {
        return headActorDataPtr;
    }


    const VirtualRobot::RobotNodePtr&
    Device::ActorRobotUnitDevice::getRobotNode() const
    {
        return robotNode;
    }


    void
    Device::ActorRobotUnitDevice::rtReadSensorValues(const IceUtil::Time& /*sensorValuesTimestamp*/,
                                                     const IceUtil::Time& /*timeSinceLastIteration*/)
    {
        sensorValue.position = headActorDataPtr->getPosition();
        sensorValue.relativePosition = headActorDataPtr->getRelativePosition();
        sensorValue.velocity = headActorDataPtr->getVelocity();
        sensorValue.absoluteEncoderVelocity = headActorDataPtr->getAbsoluteEncoderVelocity();
        sensorValue.absoluteEncoderTicks = headActorDataPtr->getAbsoluteEncoderTicks();
        sensorValue.targetPWM = headActorDataPtr->getTargetPWM();
        sensorValue.motorCurrent = static_cast<float>(headActorDataPtr->getTargetPWM());
        sensorValue.minPWM = headActorDataPtr->getCurrentMinPWM();
        sensorValue.maxPWM = headActorDataPtr->getCurrentMaxPWM();
        sensorValue.velocityTicksPerMs = static_cast<float>(headActorDataPtr->getVelocityTicks());
        sensorValue.torque =
            0; //estimateTorque(sensorValue.velocityTicksPerMs, sensorValue.targetPWM);
    }


    void
    Device::ActorRobotUnitDevice::rtWriteTargetValues(const IceUtil::Time& /*sensorValuesTimestamp*/,
                                                      const IceUtil::Time& /*timeSinceLastIteration*/)
    {
        const uint16_t led = std::chrono::duration_cast<std::chrono::seconds>(
                                 std::chrono::high_resolution_clock::now().time_since_epoch())
                                 .count() %
                             2;
        board->dataPtr->sensorIN->LED_2 = led;
        board->dataPtr->sensorIN->LED_3 = led;
        board->dataPtr->sensorIN->LED_4 = led;
        board->dataPtr->sensorIN->LED_PG15 = led;
    }

} // namespace devices::ethercat::head::armar6
